module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
    ecmaFeatures: {
      legacyDecorators: true,
      experimentalDecorators: true
    }
  },
  plugins: [
    'ember'
  ],
  extends: [
    'eslint:recommended',
    'plugin:ember/recommended'
  ],
  env: {
    browser: true
  },
  rules: {
    'ember/no-classic-components': 'off',
    'ember/no-classic-classes': 'off',
    'ember/require-tagless-components': 'off',
    'ember/no-actions-hash': 'off',
    'ember/classic-decorator-no-classic-methods': 'off',
    'ember/no-side-effects': 'off',
    'ember/no-component-lifecycle-hooks': 'off',
    'ember/no-computed-properties-in-native-classes': 'warn'
  },
  overrides: [
    // node files
    {
      files: [
        '.eslintrc.js',
        '.template-lintrc.js',
        'ember-cli-build.js',
        'testem.js',
        'blueprints/*/index.js',
        'config/**/*.js',
        'lib/*/index.js',
        'server/**/*.js'
      ],
      parserOptions: {
        sourceType: 'script'
      },
      env: {
        browser: false,
        node: true
      },
      plugins: ['node'],
      rules: {
        'node/no-unpublished-require': 'off',
        'ember/no-classic-components': 'off',
        'ember/no-classic-classes': 'off',
        'ember/require-tagless-components': 'off',
        'ember/no-actions-hash': 'off'
      }
    }
  ]
};
