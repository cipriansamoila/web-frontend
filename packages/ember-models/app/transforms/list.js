import DS from 'ember-data';
import { A } from '@ember/array';

export default DS.Transform.extend({
  deserialize(serialized) {
    return A(serialized);
  },

  serialize(deserialized) {
    return deserialized;
  }
});
