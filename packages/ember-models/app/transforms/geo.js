import DS from 'ember-data';
import { A } from '@ember/array';

export default DS.Transform.extend({
  deserialize(serialized) {
    return Object.create({
      type: serialized.type,
      coordinates: A(serialized.coordinates)
    });
  },

  serialize(deserialized) {
    return {
      type: deserialized.type,
      coordinates: deserialized.coordinates
    };
  }
});
