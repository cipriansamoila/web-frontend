gis-collective-ember-models
==============================================================================

[Short description of the addon.]

Installation
------------------------------------------------------------------------------

```
ember install gis-collective-ember-models
```


Usage
------------------------------------------------------------------------------

[Longer description of how to use the addon in apps.]


License
------------------------------------------------------------------------------

This project is licensed under the [MIT License](LICENSE.md).
