import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import Polygon from 'ol/geom/Polygon';

describe('Integration | Component | map/view', function() {
  setupRenderingTest();

  it('renders nothing', async function() {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`{{map/view}}`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('should set the zoom', async function() {
    this.set('zoom', 10);

    await render(hbs`<Map::Plain as |map|>
      <Map::View @map={{map}} @zoom={{zoom}} />
    </Map::Plain>`);

    const map = this.element.querySelector(".map").olMap;
    const view = map.getView();
    expect(view.getZoom()).to.equal(10);

    this.set('zoom', 15);
    expect(view.getZoom()).to.equal(15);
  });

  it('should set the center', async function() {
    this.set('center', [13, 14]);

    await render(hbs`<Map::Plain as |map|>
      <Map::View @map={{map}} @center={{center}} />
    </Map::Plain>`);

    const map = this.element.querySelector(".map").olMap;
    const view = map.getView();

    expect(view.getCenter()).to.deep.equal([13, 14]);

    this.set('center', [0, 0]);
    expect(view.getCenter()).to.deep.equal([0, 0]);
  });

  it('should set the mercator center', async function() {
    this.set('centerM', [13, 14]);

    await render(hbs`<Map::Plain as |map|>
      <Map::View @map={{map}} @centerM={{centerM}} />
    </Map::Plain>`);

    const map = this.element.querySelector(".map").olMap;
    const view = map.getView();

    expect(view.getCenter().map(a => parseInt(a)).join(",")).to.equal("1447153,1574216");

    this.set('centerM', [0, 0]);
    expect(view.getCenter().map(a => parseInt(a)).join(",")).to.equal("0,-7");
  });

  it('should set the extent', async function() {
    this.set('extent', undefined);

    await render(hbs`<Map::Plain as |map|>
      <Map::View @map={{map}} @extent={{extent}} />
    </Map::Plain>`);

    const map = this.element.querySelector(".map").olMap;
    const view = map.getView();

    expect(view.calculateExtent().map(a => parseInt(a)).join(",")).to.equal("-3047,0,3047,0");

    this.set("extent", [1000,2000,1000,2000]);
    expect(view.calculateExtent().map(a => parseInt(a)).join(",")).to.equal("999,2000,1000,2000");
  });


  it('should set the extent from geometry', async function() {
    this.set('polygon', new Polygon([
      [
        [1000.0, 0.0],
        [2000.0, 0.0],
        [2000.0, 1000.0],
        [1000.0, 1000.0],
        [1000.0, 0.0]
      ]
    ]));

    await render(hbs`<Map::Plain as |map|>
      <Map::View @map={{map}} @extent={{polygon}} />
    </Map::Plain>`);

    const map = this.element.querySelector(".map").olMap;
    const view = map.getView();

    expect(view.calculateExtent().map(a => parseInt(a)).join(",")).to.equal("-20036008,500,20039008,500");
  });

  it('should set the mercator extent', async function() {
    this.set('extentM', undefined);

    await render(hbs`<Map::Plain as |map|>
      <Map::View @map={{map}} @extentM={{extentM}} />
    </Map::Plain>`);

    const map = this.element.querySelector(".map").olMap;
    const view = map.getView();

    expect(view.calculateExtent().map(a => parseInt(a)).join(",")).to.equal("-3047,0,3047,0");

    this.set("extentM", [12,51,13,52]);
    expect(view.calculateExtent().map(a => parseInt(a)).join(",")).to.equal("-18646014,6710709,21429001,6710709");
  });
});
