import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

describe('Integration | Component | map/layer/vector-tiles', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`{{map/layer/vector-tiles}}`);

    expect(this.element.textContent.trim()).to.equal('');

    // Template block usage:
    await render(hbs`
      {{#map/layer/vector-tiles}}{{/map/layer/vector-tiles}}
    `);

    expect(this.element.textContent.trim()).to.equal('');
  });
});
