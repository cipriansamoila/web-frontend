import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import Map from "ol/Map";

describe('Integration | Component | map/layer/vector', function() {
  setupRenderingTest();

  it('renders nothing when the map is not set', async function() {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`{{map/layer/vector}}`);
    expect(this.element.textContent.trim()).to.equal('');

    await render(hbs`{{#map/layer/vector}}test{{/map/layer/vector}}`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders', async function() {
    this.set('map1', new Map());
    await render(hbs`{{#map/layer/vector map=map as |layer| }}-{{layer}}-{{/map/layer/vector}}`);
    expect(this.element.textContent.trim()).to.equal('-[object Object]-');
  });

  it('creates a new layer in the map', async function() {
    this.set('map1', new Map());

    await render(hbs`{{#map/layer/vector map=map1}}{{/map/layer/vector}}`);
    expect(this.map1.getLayers().getLength()).to.equal(1);
  });

  it('removes the layer when the component is removed', async function() {
    const map = new Map();
    this.set('map2', map);
    this.set('show', true);

    await render(hbs`{{#if show}}{{map/layer/vector map=map2}}{{/if}}`);
    expect(map.getLayers().getLength()).to.equal(1);

    this.set('show', false);
    expect(map.getLayers().getLength()).to.equal(0);
  });
});
