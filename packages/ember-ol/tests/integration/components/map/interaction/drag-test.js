import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import Vector from "ol/layer/Vector";
import Map from "ol/Map";

describe('Integration | Component | map/interaction/drag', function() {
  setupRenderingTest();

  it('renders nothing when map and layer is not set', async function() {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`{{map/interaction/drag}}`);

    expect(this.element.textContent.trim()).to.equal('');

    // Template block usage:
    await render(hbs`
      {{#map/interaction/drag}}
        template block text
      {{/map/interaction/drag}}
    `);

    expect(this.element.textContent.trim()).to.equal('');
  });


  it('renders', async function() {
    const map = new Map();
    this.set('map', map);
    this.set('layer', new Vector({
      renderMode: "vector"
    }));

    // Template block usage:
    await render(hbs`
      {{#map/interaction/drag map=map layer=layer as | interaction features |}}
        {{interaction}}-{{features}}
      {{/map/interaction/drag}}
    `);

    expect(this.element.textContent.trim()).to.equal('[object Object]-');
  });

  it('creates a new interaction in the map', async function() {
    const map = new Map();
    this.set('map1', map);
    this.set('layer1', new Vector({
      renderMode: "vector"
    }));

    const interactionCount = map.getInteractions().getLength();

    await render(hbs`{{#map/interaction/drag map=map1 layer=layer1}}{{/map/interaction/drag}}`);
    expect(this.map1.getInteractions().getLength()).to.equal(interactionCount + 1);
  });

  it('removes the interaction when the component is removed', async function() {
    const map = new Map();
    this.set('layer2', new Vector({
      renderMode: "vector"
    }));

    this.set('map2', map);
    this.set('show', true);
    const interactionCount = map.getInteractions().getLength();

    await render(hbs`{{#if show}}{{map/interaction/drag map=map2 layer=layer2}}{{/if}}`);
    expect(map.getInteractions().getLength()).to.equal(interactionCount + 1);

    this.set('show', false);
    expect(map.getInteractions().getLength()).to.equal(interactionCount);
  });
});
