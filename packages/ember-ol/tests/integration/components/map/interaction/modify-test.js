import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import Map from "ol/Map";
import Collection from 'ol/Collection';

describe('Integration | Component | map/interaction/modify', function() {
  setupRenderingTest();

  it('renders nothing', async function() {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`{{map/interaction/modify}}`);

    expect(this.element.textContent.trim()).to.equal('');

    // Template block usage:
    await render(hbs`
      {{#map/interaction/modify}}{{/map/interaction/modify}}
    `);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders nothing when map and features are set', async function() {
    const map = new Map();
    this.set('map', map);
    this.set('features', new Collection());

    // Template block usage:
    await render(hbs`
      {{#map/interaction/modify map=map features=features as | interaction features |}}
        {{interaction}}-{{features}}
      {{/map/interaction/modify}}
    `);

    expect(this.element.textContent.trim()).to.equal('-');
  });

  it('creates a new interaction in the map', async function() {
    const map = new Map();
    this.set('map1', map);
    this.set('features1', new Collection());

    const interactionCount = map.getInteractions().getLength();

    await render(hbs`{{#map/interaction/modify map=map1 features=features1}}{{/map/interaction/modify}}`);
    expect(this.map1.getInteractions().getLength()).to.equal(interactionCount + 1);
  });

  it('removes the interaction when the component is removed', async function() {
    const map = new Map();
    this.set('features2', new Collection());

    this.set('map2', map);
    this.set('show', true);
    const interactionCount = map.getInteractions().getLength();

    await render(hbs`{{#if show}}{{map/interaction/modify map=map2 features=features2}}{{/if}}`);
    expect(map.getInteractions().getLength()).to.equal(interactionCount + 1);

    this.set('show', false);
    expect(map.getInteractions().getLength()).to.equal(interactionCount);
  });
});
