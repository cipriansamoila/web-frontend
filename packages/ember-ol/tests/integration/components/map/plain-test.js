import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, waitFor } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

describe('Integration | Component | map/plain', function() {
  setupRenderingTest();

  it('renders', async function() {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<Map::Plain />`);
    expect(this.element.textContent.trim()).to.equal('');

    // Template block usage:
    await render(hbs`<Map::Plain></Map::Plain>`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('attaches the ol object to the element', async function() {
    // Template block usage:
    await render(hbs`<Map::Plain></Map::Plain>`);
    expect(this.element.querySelector(".map").olMap).to.not.be.undefined;
  });

  it('can be disabled', async function() {
    this.set("disabled", false);

    await render(hbs`<Map::Plain @disabled={{disabled}}/>`);
    expect([...this.element.classList]).to.not.contain('disabled');

    this.set("disabled", true);
    await waitFor(".disabled");
  });
});
