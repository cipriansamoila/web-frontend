import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import VectorSource from 'ol/source/Vector';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | map/features/geojson/point', function() {
  setupRenderingTest();

  it('renders', async function() {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<Map::Features::Geojson::Point />`);

    expect(this.element.textContent.trim()).to.equal('');

    // Template block usage:
    await render(hbs`
      <Map::Features::Geojson::Point>
        template block text
      </Map::Features::Geojson::Point>
    `);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('adds a line to the source', async function () {
    this.set('visible', true);
    this.set('source', new VectorSource());
    this.set('value', {
      "type": "Point",
      "coordinates": [10, 20]
    });

    expect(this.source.getFeatures().length).to.equal(0);

    await render(hbs`{{#if visible}}{{map/features/geojson/point source=source value=value}}{{/if}}`);
    expect(this.source.getFeatures().length).to.equal(1);
    expect(JSON.stringify(this.source.getFeatures()[0].getGeometry().transform("EPSG:3857", "EPSG:4326").getCoordinates())).to.equal(JSON.stringify([10, 20]));

    this.set('visible', false);
    expect(this.source.getFeatures().length).to.equal(0);
  });
});
