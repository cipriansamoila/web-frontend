import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import VectorSource from 'ol/source/Vector';

describe('Integration | Component | map/features/geojson/line', function () {
  setupRenderingTest();

  it('renders nothing', async function () {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`{{map/features/geojson/line}}`);

    expect(this.element.textContent.trim()).to.equal('');

    // Template block usage:
    await render(hbs`
      {{#map/features/geojson/line}}
        template block text
      {{/map/features/geojson/line}}
    `);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('adds a line to the source', async function () {
    this.set('visible', true);
    this.set('source', new VectorSource());
    this.set('value', {
      "type": "LineString",
      "coordinates":
        [
          [10.0, 0.0],
          [11.0, 0.0],
          [11.0, 1.0],
          [10.0, 1.0],
          [10.0, 0.0]
        ]
    });

    expect(this.source.getFeatures().length).to.equal(0);

    await render(hbs`{{#if visible}}{{map/features/geojson/line source=source value=value}}{{/if}}`);
    expect(this.source.getFeatures().length).to.equal(1);
    expect(JSON.stringify(this.source.getFeatures()[0].getGeometry().transform("EPSG:3857", "EPSG:4326").getCoordinates())).to.equal(JSON.stringify(
      [[10, 0], [11, 0], [11, 1], [10, 1], [10, 0]]
    ));

    this.set('visible', false);
    expect(this.source.getFeatures().length).to.equal(0);
  });
});
