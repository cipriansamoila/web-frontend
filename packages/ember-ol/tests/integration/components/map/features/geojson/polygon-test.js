import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import VectorSource from 'ol/source/Vector';

describe('Integration | Component | map/features/geojson/polygon', function () {
  setupRenderingTest();

  it('renders nothing', async function () {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`{{map/features/geojson/polygon}}`);

    expect(this.element.textContent.trim()).to.equal('');

    // Template block usage:
    await render(hbs`
      {{#map/features/geojson/polygon}}
        template block text
      {{/map/features/geojson/polygon}}
    `);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('adds a polygon to the source', async function () {
    this.set('visible', true);
    this.set('source', new VectorSource());
    this.set('value', {
      "type": "Polygon",
      "coordinates": [
        [
          [100.0, 0.0],
          [101.0, 0.0],
          [101.0, 1.0],
          [100.0, 1.0],
          [100.0, 0.0]
        ]
      ]
    });

    expect(this.source.getFeatures().length).to.equal(0);

    await render(hbs`{{#if visible}}{{map/features/geojson/polygon source=source value=value}}{{/if}}`);
    expect(this.source.getFeatures().length).to.equal(1);
    expect(JSON.stringify(this.source.getFeatures()[0].getGeometry().transform("EPSG:3857", "EPSG:4326").getCoordinates())).to.equal(JSON.stringify([[
      [100.0, 0.0],
      [101.0, 0.0],
      [101.0, 1.0],
      [100.0, 1.0],
      [100.0, 0.0]
    ]]));

    this.set('visible', false);
    expect(this.source.getFeatures().length).to.equal(0);
  });
});
