import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

describe('Integration | Helper | nice-distance', function() {
  setupRenderingTest();

  // Replace this with your real tests.
  it('renders', async function() {
    this.set('location1', {coordinates: [0, 0]});
    this.set('location2', [1, 1]);

    await render(hbs`{{nice-distance location1 location2}}`);

    expect(this.element.textContent.trim()).to.equal('157.25km');
  });
});
