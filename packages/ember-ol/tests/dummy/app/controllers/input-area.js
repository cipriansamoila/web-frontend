import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
  box: true,
  mode: computed("box", function() {
    return this.box ? "box" : "free";
  }),

  strValue: computed("value", function() {
    return JSON.stringify(this.value);
  }),

  actions: {
    change(value) {
      this.set("value", value);
    },

    setValue() {
      this.set("value", {
        "type": "Polygon",
        "coordinates": [
          [
            [100.0, 0.0], [101.0, 0.0], [101.0, 1.0],
            [100.0, 1.0], [100.0, 0.0]
          ]
        ]
      });
    }
  }
});
