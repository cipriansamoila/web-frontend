import Controller from '@ember/controller';
import { action } from '@ember/object';

export default class MapController extends Controller {
  disabled = false
  mode = "free"

  get osm() {
    return { layers: [{
      type: "Open Street Maps"
    }]};
  }

  @action
  polygonChange(value) {
    this.set("value", value);
  }
}
