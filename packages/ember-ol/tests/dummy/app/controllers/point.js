import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class PointController extends Controller {
  @tracked disabled = false
  @tracked zoom = 10
  @tracked center = [13.36486816406251, 52.54963546276437]

  get osm() {
    return { layers: [{
      type: "Open Street Maps"
    }]};
  }

  @action
  changeCenter() {

  }

  @action
  pointChange(value) {
    this.set("value", value);
  }
}
