import Service from "ember-ol/services/ol";

import { inject as service } from "@ember/service";

export default Service.extend({
  fastboot: service(),
});
