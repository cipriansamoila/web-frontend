import { helper } from '@ember/component/helper';
import { getDistance } from "ol/sphere";


export function niceDistance(params/*, hash*/) {
  let value;

  if(!params[0] || !params[1]) {
    return "";
  }

  const center = params[1];

  if(params[0].coordinates) {
    value = getDistance(params[0].coordinates, center);
  } else {
    value = parseInt(params[0]);
  }

  if(value < 1000) {
    value = Math.round(value * 100) / 100;
    return `${value}m`;
  }

  value = value / 1000;
  value = Math.round(value * 100) / 100;

  if(!value) {
    return `unknown distance`;
  }

  return `${value}km`;
}

export default helper(niceDistance);
