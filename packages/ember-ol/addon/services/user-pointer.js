import Service from '@ember/service';
import { inject as service } from '@ember/service';
import { later } from '@ember/runloop';
import { computed } from '@ember/object';

import Point from "ol/geom/Point";
import VectorSource from "ol/source/Vector";
import Vector from "ol/layer/Vector";
import Feature from "ol/Feature";
import { fromLonLat } from "ol/proj";
import { unByKey } from "ol/Observable";
import { getVectorContext } from 'ol/render';

export default Service.extend({
  vectorSource: null,
  vectorLayer: null,
  duration: 5000,

  featureStyle: service(),
  ol: service(),
  accuracy: 0,

  position: computed("_position", "_position.[]", {
    get() {
      return this._position;
    },

    set(key, value) {
      if(!value) {
        this.set("_position", [0,0]);
        this.clear();
        return this._position;
      }

      if(value[0] == 0 && value[1] == 0) {
        this.set("_position", [0,0]);
        this.clear();
        return this._position;
      }

      if(!this.ol.map) {
        this.set("_position", [0,0]);
        later(() => {
          this.setPosition(value);
        }, 1000);

        return this._position;
      }

      this.set("_position", value.slice());
      var geom = new Point(fromLonLat(value));
      if(!this.feature) {
        this.setupFeature(geom);
        return this._position;
      }

      this.feature.setGeometry(geom);
      this.feature.changed();

      return this._position;
    }

  }),

  setPosition: function(value) {
    this.set("position", value);
  },

  animate(event) {
    if(!this.feature) {
      return;
    }
    const map = this.ol.map;
    const vectorContext = getVectorContext(event);

    var frameState = event.frameState;
    var flashGeom = this.feature.getGeometry().clone();
    var elapsed = frameState.time - this.start;
    var elapsedRatio = (elapsed % this.duration) / this.duration;

    const index = parseInt(elapsedRatio * 100);

    const userPointerStyle = this.featureStyle.userPointer(index);
    const accuracyStyle = this.featureStyle.accuracyRadius(this.accuracy);
    const animatedStyle = this.featureStyle.userPointerRadius(index, this.accuracy);

    vectorContext.setStyle(accuracyStyle);
    vectorContext.drawGeometry(flashGeom);

    userPointerStyle.forEach(element => {
      vectorContext.setStyle(element);
      vectorContext.drawGeometry(flashGeom);
    });

    vectorContext.setStyle(animatedStyle);
    vectorContext.drawGeometry(flashGeom);

    // tell OpenLayers to continue postcompose animation
    map.render();
  },

  setupFeature: function(geom) {
    this.set("feature", new Feature(geom));
    this.feature.setId("user-pointer");

    let style = this.featureStyle.userPointer();
    this.feature.setStyle(style);
    this.feature.changed();

    if(!this.map.olMap) {
      return;
    }

    if(!this.vectorSource) {
      this.initLayer();
    }

    this.vectorSource.addFeature(this.feature);

    this.start = new Date().getTime();
    this.listenerKey = this.vectorLayer.on("postrender", (event) => {
      this.animate(event)
    });
  },

  initLayer() {
    if(this.vectorSource && this.vectorLayer) {
      return;
    }

    this.set("vectorSource", new VectorSource({}));
    this.set("vectorLayer", new Vector({
      source: this.vectorSource,
      renderMode: 'vector',
      opacity: 0.99
    }));

    this.map.olMap.addLayer(this.vectorLayer);
  },

  clear() {
    if(this.vectorLayer) {
      this.map.olMap.removeLayer(this.vectorLayer);
      this.set("vectorLayer", null);
    }

    if(this.vectorSource) {
      this.vectorSource.clear();
    }

    this.set("vectorSource", null);
    this.set("feature", null);

    unByKey(this.listenerKey);
  }
});
