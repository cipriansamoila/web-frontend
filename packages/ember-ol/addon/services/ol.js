import Service from "@ember/service";

import { inject as service } from "@ember/service";
import { later, cancel } from "@ember/runloop";
import { toPng } from "html-to-image";
import Map from "ol/Map";
import View from "ol/View";
import * as olInteraction from "ol/interaction";

export default Service.extend({
  fastboot: service(),

  screenshot() {
    return new Promise((resolve, reject) => {
      const exportOptions = {
        filter: function (element) {
          return element.className
            ? element.className.indexOf("ol-control") === -1
            : true;
        },
      };

      this.map.once("rendercomplete", () => {
        toPng(this.map.getTargetElement(), exportOptions)
          .then(resolve)
          .catch(reject);
      });

      this.map.renderSync();
    });
  },

  onMapResize(map) {
    map.updateSize();
  },

  spyResizeWithTimer(element, map) {
    this.set("oldW", element.offsetWidth);
    this.set("oldH", element.offsetHeight);
    cancel(this.resizeTimer);
    const _this = this;

    function sizeCheck() {
      const resizeTimer = later(() => {
        sizeCheck();
      }, 1000);

      _this.set("resizeTimer", resizeTimer);

      if (
        _this.oldW == element.offsetWidth &&
        _this.oldH == element.offsetHeight
      ) {
        return;
      }

      _this.set("oldW", element.offsetWidth);
      _this.set("oldH", element.offsetHeight);

      _this.onMapResize(map);
    }

    const resizeTimer = later(() => {
      sizeCheck();
    }, 1000);

    _this.set("resizeTimer", resizeTimer);
  },

  spyResize(element, map) {
    try {
      if (this.sizeObserver) {
        this.sizeObserver.disconnect();
      }

      this.set(
        "sizeObserver",
        new ResizeObserver(() => {
          this.onMapResize(map);
        })
      );

      this.sizeObserver.observe(element);
    } catch (err) {
      return this.spyResizeWithTimer(element, map);
    }
  },

  insert: function (element) {
    const map = this.createMap();
    this.set("map", map);

    map.setTarget(element);
    this.spyResize(element, map);

    return map;
  },

  createMap() {
    return new Map({
      controls: [],
      loadTilesWhileAnimating: true,
      loadTilesWhileInteracting: true,
      view: new View({
        zoom: 1,
        center: [0, 0],
      }),
      interactions: olInteraction.defaults({
        doubleClickZoom: false,
      }),
    });
  },
});
