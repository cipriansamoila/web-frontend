import Service from '@ember/service';

import Tile from "ol/layer/Tile";
import VectorTile from "ol/layer/VectorTile";
import MVT from "ol/format/MVT";

import OSM from "ol/source/OSM";
import Stamen from "ol/source/Stamen";
import TileArcGISRest from "ol/source/TileArcGISRest";
import TileWMS from "ol/source/TileWMS";
import XYZ from "ol/source/XYZ";
import BingMaps from "ol/source/BingMaps";
import VectorTileSource from "ol/source/VectorTile";

import { createXYZ } from "ol/tilegrid";
import stylefunction from 'ol-mapbox-style/dist/stylefunction';

export default Service.extend({

  toOl(layer) {
    if(layer.type == "Open Street Maps") {
      return this.toOSMLayer();
    }

    if(layer.type == "WMS") {
      return this.toWMSLayer(layer.options);
    }

    if(layer.type == "XYZ") {
      return this.toXYZLayer(layer.options);
    }

    if(layer.type == "Stamen") {
      return this.toStamenLayer(layer.options);
    }

    if(layer.type == "VectorTile") {
      return this.toVectorTileLayer(layer.options);
    }

    if(layer.type == "ArcGIS MapServer") {
      return this.toArcGISMapServer(layer.options);
    }

    if(layer.type == "Bing Maps") {
      return this.toBingLayer(layer.options);
    }
  },

  toOSMLayer() {
    return new Tile({
      source: new OSM(),
    });
  },

  toStamenLayer(options) {
    return new Tile({
      source: new Stamen({
        layer: options.layer,
      })
    });
  },

  toArcGISMapServer(options) {
    return new Tile({
      source: new TileArcGISRest({
        url: options.url,
      })
    });
  },

  toWMSLayer(options) {
    return new Tile({
      source: new TileWMS({
        url: options.url,
        serverType: options["server type"],
        params: {
          "LAYERS": options["layers"],
          "STYLES": options["styles"]
        },
        gutter: options["gutter"]
      })
    });
  },

  toXYZLayer(options) {
    return new Tile({
      source: new XYZ({
        url: options.url,
        tilePixelRatio: options["tilePixelRatio"]
      })
    });
  },

  toBingLayer(options) {
    return new Tile({
      source: new BingMaps({
        key: options.key,
        imagerySet: options["imagerySet"]
      })
    });
  },

  toVectorTileLayer(options) {
    const tilegrid = createXYZ({ tileSize: 512 });

    const layer = new VectorTile({
      declutter: true,
      source: new VectorTileSource({
        format: new MVT(),
        url: options.url,
        tileGrid: tilegrid,
      })
    });

    fetch(options.style).then(function(response) {
      response.json().then(function(glStyle) {
        const keys = Object.keys(glStyle.sources);
        stylefunction(layer, glStyle, keys[0]);
      });
    });

    return layer;
  }
});
