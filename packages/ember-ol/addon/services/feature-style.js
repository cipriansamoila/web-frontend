import Service from '@ember/service';

import { inject as service } from '@ember/service';

import Style from "ol/style/Style";
import Icon from "ol/style/Icon";
import Circle from "ol/style/Circle";
import Fill from "ol/style/Fill";
import Stroke from "ol/style/Stroke";
import TextStyle from "ol/style/Text";
import { easeOut } from "ol/easing";

export default Service.extend({
  ol: service(),
  styles: null,
  oldAccuracy: 0,

  init() {
    this._super(...arguments);
    this.set("styles", {});
  },

  marker() {
    if(!this.markerCache) {
      this.set("markerCache", new Style({
        image: new Icon(({
          anchor: [0.5, 1],
          anchorXUnits: 'fraction',
          anchorYUnits: 'fraction',
          src: "/assets/img/marker.svg"
        }))
      }));
    }

    return this.markerCache;
  },

  icon(path, setStyle, color) {
    if(!setStyle) {
      return this.missingStyle();
    }

    const key = `icon.${path}.${color}`;

    if(this.styles[key]) {
      return this.styles[key];
    }

    const localStyle = {
      opacity: setStyle.opacity || 1,
      scale: setStyle.scale || 1,
      overlayOpacity: setStyle.overlayOpacity || 0.5,
      overlayVisible: setStyle.overlayVisible || true,
      overlayLineColor: color || setStyle.overlayLineColor || "#999999"
    }

    if(!this.styles[path]) {
      this.styles[path] = new Style({
        image: new Icon(({
          src: path + "/sm",
          scale: localStyle.scale,
          opacity: localStyle.opacity
        }))
      });
    }

    if(localStyle.overlayVisible) {
      this.styles[key] = [ this.default(localStyle.overlayLineColor, localStyle.overlayOpacity), this.styles[path] ];
    } else {
      this.styles[key] = [ this.styles[path] ];
    }

    return this.styles[key];
  },

  default(color, opacity) {
    if(!color) {
      color = '#999999';
    }

    if(!opacity) {
      opacity = 0.5;
    }

    let defaultCache = this.get("defaultCache" + color + opacity);

    if(!this.defaultCache) {
      defaultCache = new Style({
        image: new Circle({
          radius: 15,
          stroke: new Stroke({
            color: color,
            width: 1
          }),
          fill: new Fill({
            color: 'rgba(255,255,255,' + opacity + ')'
          })
        })
      });

      this.set("defaultCache" + color, defaultCache);
    }

    return defaultCache;
  },

  group(text) {
    var level = parseInt(text);
    var fileLevel = "";

    if(level > 0 && level < 10) {
      fileLevel = 0;
    } else if(level >= 10 && level < 30) {
      fileLevel = 1;
    } else if(level >= 30 && level < 50) {
      fileLevel = 2;
    } else if(level >= 50 && level < 100) {
      fileLevel = 3;
    } else if(level >= 100 && level < 200) {
      fileLevel = 4;
    } else if(level >= 200 && level < 500) {
      fileLevel = 5;
    } else if(level >= 500 && level < 1000) {
      fileLevel = 6;
    } else if(level >= 1000) {
      fileLevel = 7;
      text = parseInt(level / 1000) + "k"
    }

    const key = `group.${fileLevel}`;

    if(!this.styles[key]) {
      this.set("groupCache", new Style({
        image: new Icon(({
          anchor: [0.5, 0.5],
          anchorXUnits: 'fraction',
          anchorYUnits: 'fraction',
          src: `/img/hex${fileLevel}.svg`,
          scale: 0.25
        }))
      }));
    }

    var textStyle = new Style({
      text: new TextStyle({
        font: 'bold 11px "LatoWeb", "sans-serif"',
        text: text,
        fill: new Fill({
          color: '#FFFFFF'
        })
      })
    });

    return [ textStyle, this.groupCache ];
  },

  selected() {
    var style = new Style({
      image: new Circle({
        radius: 3,
        fill: new Fill({
          color: '#333333'
        })
      })
    });

    return style;
  },

  blank() {
    var frame = new Style({
      image: new Circle({
        radius: 10,
        fill: new Fill({
          color: '#28a745'
        })
      })
    });

    this.set("pointerRadius", {});

    return [this.default('#28a745', 1), frame];
  },

  missingStyle() {
    var frame = new Style({
      image: new Circle({
        radius: 10,
        fill: new Fill({
          color: '#aaaaaa'
        })
      })
    });

    this.set("pointerRadius", {});

    return [this.default('#aaaaaa', 1), frame];
  },

  userPointer() {
    if(!this.innerUserCircle) {
      this.set("innerUserCircle", new Style({
        image: new Circle({
          radius: 8,
          fill: new Fill({
            color: '#0088a3'
          })
        })
      }));
    }

    if(!this.outerCircle1) {
      this.set("outerCircle1", new Style({
        image: new Circle({
          radius: 20,
          fill: new Fill({
            color: 'rgba(0,0,0,0.05)'
          })
        })
      }));
    }

    if(!this.outerCircle2) {
      this.set("outerCircle2", new Style({
        image: new Circle({
          radius: 18,
          fill: new Fill({
            color: 'rgba(0,0,0,0.1)'
          })
        })
      }));
    }

    return [ this.outerCircle1, this.outerCircle2, this.default("#ffffff", 1), this.innerUserCircle ];
  },

  accuracyRadius(meters) {
    const map = this.ol.map;
    const resolution = map.getView().getResolution();

    if(this.accuracyStyle && (meters == this.oldAccuracy && resolution == this.oldResolution)) {
      return this.accuracyStyle;
    }

    this.set("oldAccuracy", meters);
    this.set("oldResolution", resolution);

    var radius = parseInt(meters / resolution);

    if(radius > 1000) {
      radius = 1000;
    }

    var style = new Style({
      image: new Circle({
        radius: radius,
        fill: new Stroke({
          color: 'rgba(0, 136, 163, 0.1)'
        }),
        stroke: new Stroke({
          color: 'rgba(0, 136, 163, 0.5)',
          width: 1
        })
      })
    });

    this.set("accuracyStyle", style);

    return style;
  },

  userPointerRadius(index, meters) {
    var pointerRadius = this.pointerRadius;

    if(!pointerRadius) {
      pointerRadius = {};
    }

    if(pointerRadius[index]) {
      return pointerRadius[index];
    }

    var color = meters <= 10 ? 'rgb(0, 136, 163)' : 'rgb(163, 0, 27)';

    if(index <= 50) {
      index *= 2;
    } else {
      index = 100 - index;
    }

    const elapsedRatio = (index / 100);

    var radius = easeOut(elapsedRatio) * 3 + 7;
    var style = new Style({
      image: new Circle({
        radius: radius,
        fill: new Stroke({
          color: color
        })
      })
    });

    pointerRadius[index] = style;
    this.set("pointerRadius", pointerRadius);

    return style;
  }
});
