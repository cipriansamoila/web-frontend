import Component from "@ember/component";
import { computed } from "@ember/object";
import { htmlSafe } from "@ember/template";

export default Component.extend({
  classNames: ["base-map-select"],

  covers: computed(
    'list.@each.{cover,isLoaded}', 'selected.id',
    function () {
      if (!this.list) {
        return [];
      }

      const selectedId = this.selected ? this.selected.id : null;

      return this.list
        .filter((a) => a.id != selectedId)
        .map((a) => a.get("cover"));
    }
  ),

  styles: computed("covers", "covers.@each.isLoaded", function () {
    return this.covers.map((a) => {
      return {
        cover: a,
        style: htmlSafe(`background-image: url(` + a.get("picture") + `/md);`),
      };
    });
  }),

  actions: {
    select(coverId) {
      if (this.onSelect) {
        const selected = this.list.filter((a) => a.get("cover.id") == coverId);
        this.onSelect(selected.pop());
      }
    },
  },
});
