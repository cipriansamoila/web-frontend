import Component from '../lib/base';
import { computed } from '@ember/object';
import { Modify } from 'ol/interaction';

export default Component.extend({
  setupOl() {
    this.destroyOl();

    if(!this._features || !this._map) {
      return;
    }

    this.set("modify", new Modify({
      features: this._features
    }));

    this._map.addInteraction(this.modify);
    this.set("currentInteractions", [ this.modify ])

    this.modify.on("modifyend", (ev) => {
      this.triggerChange(ev);
    });
  },

  triggerChange(ev) {
    if(this.onChange) {
      this.onChange(ev);
    }
  },

  features: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      return this.proxySet(name, value);
    }
  })
});
