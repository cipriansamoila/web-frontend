import Component from '../lib/base';
import { computed } from '@ember/object';
import Select from 'ol/interaction/Select';

export default Component.extend({
  setupOl() {
    this.destroyOl();

    if(!this._layer || !this._map) {
      return;
    }

    this.set("select", new Select({
      layers: [ this._layer ]
    }));

    this._map.addInteraction(this.select);
    this.set("currentInteractions", [ this.select ])
  },

  features: computed("select", function() {
    if(!this.select) {
      return null;
    }

    return this.select.getFeatures();
  }),

  layer: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      return this.proxySet(name, value);
    }
  })
});
