import Component from '../lib/base';
import Drag from '../lib/Drag'
import { computed } from '@ember/object';

export default Component.extend({
  setupOl() {
    this.destroyOl();

    if(!this._layer || !this._map) {
      return;
    }

    this.set("drag", new Drag({
      layer: this._layer
    }));

    this._map.addInteraction(this.drag);
    this.set("currentInteractions", [ this.drag ])
  },

  layer: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      return this.proxySet(name, value);
    }
  })
});
