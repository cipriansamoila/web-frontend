import Component from '../lib/base';
import { computed } from '@ember/object';
import Draw from 'ol/interaction/Draw';

export default Component.extend({
  setupOl() {
    this.destroyOl();
    if(!this._source || !this._type || !this._map) {
      return;
    }

    this.set("draw", new Draw({
      source: this.source,
      type: this.type,
      geometryFunction: this._geometryFunction
    }));

    this._map.addInteraction(this.draw);
    this.set("currentInteractions", [ this.draw ]);

    this.draw.on("drawend", (ev) => {
      this.triggerDrawEnd(ev);
    });
  },

  triggerDrawEnd(ev) {
    if(this.onDrawEnd) {
      this.onDrawEnd(ev);
    }
  },

  source: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      return this.proxySet(name, value);
    }
  }),

  type: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      return this.proxySet(name, value);
    }
  }),

  geometryFunction: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      return this.proxySet(name, value);
    }
  })
});
