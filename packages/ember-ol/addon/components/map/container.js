import Component from '@ember/component';

export default Component.extend({
  classNames: ["map-inner-container"],
  classNameBindings: ["top", "left", "bottom", "right"],
  top: false,
  left: false,
  bottom: false,
  right: false
});
