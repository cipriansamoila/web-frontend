import Pointer from "ol/interaction/Pointer";

export default class Drag extends Pointer {

  constructor(source) {
    super();
    this.source = source;
    this.metaType = "Drag";

    /**
     * @type {ol.Pixel}
     * @private
     */
    this.coordinate_ = null;

    /**
     * @type {string|undefined}
     * @private
     */
    this.cursor_ = 'pointer';

    /**
     * @type {ol.Feature}
     * @private
     */
    this.feature_ = null;

    /**
     * @type {string|undefined}
     * @private
     */
    this.previousCursor_ = undefined;
  }

  /**
   * @param {ol.MapBrowserEvent} evt Map browser event.
   * @return {boolean} `true` to start the drag sequence.
   */
  handleDownEvent(evt) {
    let feature = evt.map.forEachFeatureAtPixel(evt.pixel, (feature) => { return feature; });

    if(feature && this.source && !this.source.hasFeature(feature)) {
      feature = null;
    }

    if (feature) {
      this.coordinate_ = evt.coordinate;
      this.feature_ = feature;
    }

    return !!feature && typeof(feature.getId()) != "number";
  }

  /**
   * @param {ol.MapBrowserEvent} evt Map browser event.
   */
  handleDragEvent(evt) {
    var deltaX = evt.coordinate[0] - this.coordinate_[0];
    var deltaY = evt.coordinate[1] - this.coordinate_[1];

    var geometry = this.feature_.getGeometry();
    geometry.translate(deltaX, deltaY);

    this.coordinate_[0] = evt.coordinate[0];
    this.coordinate_[1] = evt.coordinate[1];
  }

  /**
   * @param {ol.MapBrowserEvent} evt Event.
   */
  handleMoveEvent(evt) {
    if (this.cursor_) {
      var map = evt.map;
      var feature = map.forEachFeatureAtPixel(evt.pixel,
          function(feature) {
            return feature;
          });
      var element = evt.map.getTargetElement();
      if (feature) {
        if (element.style.cursor != this.cursor_) {
          this.previousCursor_ = element.style.cursor;
          element.style.cursor = this.cursor_;
        }
      } else if (this.previousCursor_ !== undefined) {
        element.style.cursor = this.previousCursor_;
        this.previousCursor_ = undefined;
      }
    }
  }

  /**
   * @return {boolean} `false` to stop the drag sequence.
   */
  handleUpEvent() {
    this.coordinate_ = null;
    this.feature_ = null;
  }
}