import Pointer from "ol/interaction/Pointer";

export default class FeatureSelect extends Pointer {
  layer = null

  constructor() {
    super();
    this.coordinate_ = null;
  }

  handleMoveEvent() {
    this.moved_ = this.down_;
  }

  stopDown() {
    return false;
  }

  handleDownEvent() {
    this.down_ = true;
    return true;
  }

  layerFilter(layer) {
    if(this.layer == null) {
      return true;
    }

    return layer == this.layer;
  }

  handleUpEvent(evt) {
    this.down_ = false;

    if(this.moved_) {
      return false;
    }

    if(evt.originalEvent.button > 0) {
      return false;
    }

    var map = evt.map;

    var feature = map.forEachFeatureAtPixel(evt.pixel, function(feature) { return feature; }, {
      layerFilter: (layer) => {
        return this.layerFilter(layer);
      },

      hitTolerance: 4
    });

    this.coordinate_ = evt.coordinate;
    this.feature_ = feature;

    if(!this.moved_) {
      this.notify(this.feature_);
    }

    return true;
  }
}
