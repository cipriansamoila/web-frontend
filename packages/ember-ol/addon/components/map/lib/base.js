import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  tagName: "",

  setupOl() {
    if(this.isDestroyed) {
      return;
    }

    this.destroyOl();
  },

  destroyOl() {
    if(this.isDestroyed) {
      return;
    }

    if(!this._map) {
      return;
    }

    if(this.currentLayers) {
      this.currentLayers.forEach((item) => {
        this._map.removeLayer(item);
      });
    }

    if(this.currentInteractions) {
      this.currentInteractions.forEach((item) => {
        this._map.removeInteraction(item);
      });
    }
  },

  willClearRender() {
    this.destroyOl();
  },

  willRemove() {
    this.destroyOl();
  },

  proxySet(name, value) {
    if(this.isDestroyed) {
      return;
    }

    if(value == this.get("_" + name) && this._map) {
      return value;
    }

    this.set("_" + name, value);

    this.setupOl();

    return value;
  },

  proxyGet(name) {
    if(this.isDestroyed) {
      return;
    }

    this.get("_" + name);
  },

  proxyPropertyGet(name) {
    if(this.isDestroyed) {
      return;
    }

    if(!this.view) {
      return;
    }

    const key = "get" + name.charAt(0).toUpperCase() + name.slice(1);

    return this.view[key]();
  },

  proxyPropertySet(name, value) {
    if(this.isDestroyed) {
      return;
    }

    if(!this.view) {
      this.setPending(name, value);
      return value;
    }

    const key = "set" + name.charAt(0).toUpperCase() + name.slice(1);
    this.view[key](value);

    return value;
  },

  setPending(name, value) {
    if(!this.pending) {
      this.set("pending", {});
    }

    this.pending[name] = value;
  },

  applyPending(view) {
    if(!this.pending) {
      return;
    }

    Object.keys(this.pending).forEach((key) => {
      view.set(key, this.pending[key], false);
    });
  },

  map: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      return this.proxySet(name, value);
    }
  })
});
