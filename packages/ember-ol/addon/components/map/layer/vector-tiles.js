import Component from './base';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';
import EmberObject from '@ember/object';

import MVT from "ol/format/MVT";
import VectorTile from "ol/layer/VectorTile";
import VectorTileSource from "ol/source/VectorTile";
import Style from "ol/style/Style";
import Fill from "ol/style/Fill";
import Stroke from "ol/style/Stroke";
import TextStyle from "ol/style/Text";
import FeatureSelect from '../lib/FeatureSelect';
import { debounce } from '@ember/runloop';
import { all } from 'rsvp';

export default Component.extend({
  layer: service(),
  featureStyle: service(),
  tagName: "",

  init() {
    this._super();
    this.set("iconCache", EmberObject.create({}));
  },

  setupOl() {
    this.destroyOl();

    if(!this._url || !this._map) {
      return;
    }

    const mapLayers = this._map.getLayers();
    const result = this.getVectorLayer(this._url, this._bearer);

    mapLayers.push(result);
    this.set("currentLayers", [result]);
  },

  getVectorLayer(url) {
    const source = {
      format: new MVT(),
      url: url
    }

    source.tileLoadFunction = (tile, url) => {
      tile.setLoader((extent, resolution, projection) => {
        const xhr = new XMLHttpRequest();
        xhr.open("GET", url);
        xhr.responseType = "arraybuffer";

        if(this._bearer) {
          xhr.setRequestHeader("Authorization", this._bearer)
        }

        xhr.onload = function () {
          const format = tile.getFormat();

          tile.setFeatures(format.readFeatures(xhr.response, {
            extent: extent,
            featureProjection: projection
          }));
        };

        xhr.send();
      });
    }

    const layer = new VectorTile({
      source: new VectorTileSource(source),
      declutter: false,
      renderMode: "hybrid",
      zIndex: 9999,
      style: (feature) => {
        return this.style(feature);
      }
    });

    return layer;
  },

  style(feature) {
    const properties = feature.getProperties();

    if(properties.layer == "debug" && !properties.name && this.debugTiles) {
      return new Style({
        stroke: new Stroke({
          color: [0, 0, 0, 0.6],
          width: 1,
          lineDash: [10, 10],
          lineDashOffset: 10
        })
      });
    }

    if(properties.layer == "debug" && properties.name && this.debugTiles) {
      return new Style({
        text: new TextStyle({
          font: 'bold 11px "LatoWeb", "sans-serif"',
          text: properties.name,
          fill: new Fill({
            color: '#000000'
          })
        })
      });
    }

    if(properties.layer == "groups") {
      return this.featureStyle.group(properties.count + "");
    }

    if(properties.layer == "points") {
      const icon = properties.icon;
      const featureId = properties._id;

      const isPublished = properties.isPublished == "true";

      if(this.selectedFeatureId == featureId) {
        return this.featureStyle.selected();
      }

      if(!icon || icon == "") {
        return this.featureStyle.default("#cc0033");
      }

      return this.getIconStyle(icon, isPublished, false);
    }
  },

  renderIcons() {
    const layer = this.currentLayers[0];
    layer.changed();
  },

  loadMissingIcons() {
    const missingIds = Object.keys(this.iconCache).filter(a => this.iconCache.get(a) === null);
    if(missingIds.length == 0) {
      return;
    }

    const promises = missingIds.map(a => this.loadIcon(a));

    all(promises).then((result) => {
      result.forEach(icon => {
        this.iconCache.set(icon.get("id"), icon);
      });

      const cancel = debounce(this, "renderIcons", 500);
      this.set("cancelIconRender", cancel);
    });
  },

  debounceIconLoad(iconId) {
    this.iconCache.set(iconId, null);

    if(this.loadIcon) {
      const cancel = debounce(this, "loadMissingIcons", 150);
      this.set("cancelIconLoad", cancel);
    }

    return [ this.featureStyle.default() ];
  },

  getIconStyle(iconId, isPublished, hasIssues) {
    const icon = this.iconCache.get(iconId);

    if(!icon) {
      return this.debounceIconLoad(iconId);
    }

    var color;
    if(!isPublished) {
      color = "#fffff";
    }

    if(hasIssues) {
      color = "#cc0033";
    }
    const iconSetStyle = icon.get("iconSet.style");
    const style = this.featureStyle.icon(icon.image.value, iconSetStyle, color);

    return style;
  },

  didInsertElement() {
    if(!this._map) {
      return;
    }

    this.set("featureSelect", new FeatureSelect());

    this._map.addInteraction(this.featureSelect);

    this.featureSelect.notify = (feature) => {
      if(this.onSelect) {
        this.onSelect(feature);
      }
    };
  },

  willDestroyElement() {
    if(!this._map) {
      return;
    }

    this._map.addInteraction(this.featureSelect);
  },

  url: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      return this.proxySet(name, value);
    }
  }),

  bearer: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      return this.proxySet(name, value);
    }
  })
});
