import Component from '../lib/base';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
  layer: service(),
  tagName: "div",
  classNames: [ "d-none" ],

  setupOl() {
    this.destroyOl();

    if(!this._value || !this._value.layers || !this._value.layers.length || !this._map) {
      return;
    }

    const mapLayers = this._map.getLayers();
    const result = [];

    this._value.layers.slice().reverse().forEach((layer) => {
      const olLayer = this.layer.toOl(layer);
      result.push(olLayer);
      mapLayers.insertAt(0, olLayer);
    });

    this.set("currentLayers", result);
  },

  value: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      return this.proxySet(name, value);
    }
  }),

  style: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      return this.proxySet(name, value);
    }
  }),

  map: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      return this.proxySet(name, value);
    }
  })
});
