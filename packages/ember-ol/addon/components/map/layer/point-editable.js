import Component from './base';
import { computed } from '@ember/object';
import VectorSource from "ol/source/Vector";
import Vector from "ol/layer/Vector";
import Feature from "ol/Feature";
import { fromLonLat, transform } from "ol/proj";
import Point from "ol/geom/Point";
import Drag from "../lib/Drag";
import { inject as service } from '@ember/service';

export default Component.extend({
  featureStyle: service(),

  setupOl() {
    this.destroyOl();

    if(!this.feature || !this._map) {
      return;
    }

    const vectorSource = new VectorSource({});
    this.set("vectorSource", vectorSource);
    this.vectorSource.addFeature(this.feature);

    const vectorLayer = new Vector({
      source: vectorSource,
      renderMode: "vector"
    });

    this._map.addLayer(vectorLayer);
    this.set("currentLayers", [ vectorLayer ]);

    this.set("drag", new Drag(vectorSource));

    this._map.addInteraction(this.drag);

    this.set("_changeGeometry", () => {
      const position = transform(this.feature.getGeometry().getCoordinates(), 'EPSG:3857', 'EPSG:4326');

      if(this.onPositionUpdate) {
        this.onPositionUpdate(position);
      }
    });

    this.feature.on("change", this._changeGeometry);
  },

  destroyOl() {
    this._super();
    this._map.removeInteraction(this.drag);

    if(this.feature && this._changeGeometry) {
      this.feature.un("change", this._changeGeometry);
    }
  },

  iconStyle: computed(function() {
    return this.featureStyle.marker();
  }),

  position: computed({
    get(name) {
      return this.proxyGet(name);
    },

    set(name, value) {
      const geom = new Point(fromLonLat(value.slice()));

      if(!this.feature) {
        this.set("feature", new Feature());
        this.feature.setStyle(this.iconStyle);
      }

      this.feature.setGeometry(geom);

      return this.proxySet(name, value.slice());
    }
  })
});
