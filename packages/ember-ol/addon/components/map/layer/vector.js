import Component from './base';
import VectorSource from "ol/source/Vector";
import Vector from "ol/layer/Vector";

export default Component.extend({
  prepare() {
    this.set("vectorSource", new VectorSource({}));
    this.set("vectorLayer", new Vector({
      source: this.vectorSource,
      renderMode: "vector",
      useSpatialIndex: true,
      style: this._style
    }));

    this.vectorLayer.on("change", () => {
      if(this.onChange) {
        this.onChange(this.vectorLayer);
      }
    });
  },

  setupOl() {
    if(!this.vectorLayer) {
      this.prepare();
    }

    this.destroyOl();

    if(!this._map) {
      return;
    }

    this._map.getLayers().push(this.vectorLayer);
    this.set("currentLayers", [ this.vectorLayer ]);
  }
});
