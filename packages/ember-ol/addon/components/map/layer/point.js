import Component from './base';
import { computed } from '@ember/object';
import VectorSource from "ol/source/Vector";
import Vector from "ol/layer/Vector";
import Feature from "ol/Feature";
import { fromLonLat } from "ol/proj";
import Point from "ol/geom/Point";
import { inject as service } from '@ember/service';

export default Component.extend({
  featureStyle: service(),
  classNames: ["ol-point"],

  setupOl() {
    this.destroyOl();

    if(!this.feature || !this._map) {
      return;
    }

    const vectorSource = new VectorSource({});
    this.set("vectorSource", vectorSource);
    this.vectorSource.addFeature(this.feature);

    const vectorLayer = new Vector({
      source: vectorSource,
      renderMode: "vector"
    });

    this._map.addLayer(vectorLayer);

    this.set("currentLayers", [ vectorLayer ]);
  },

  iconStyle: computed("_icon", "_icon.isLoaded", "_iconSet", "_iconSet.isLoaded", function() {
    const icon = this._icon;
    const iconSet = this._iconSet;

    if(!icon || !iconSet) {
      return this.featureStyle.blank();
    }

    return this.featureStyle.icon(icon.get("image"), iconSet);
  }),

  position: computed({
    get(name) {
      return this.proxyGet(name);
    },

    set(name, value) {
      if(!value || !value.length) {
        return;
      }

      const geom = new Point(fromLonLat(value));

      if(!this.feature) {
        this.set("feature", new Feature());
        this.feature.setStyle(this.iconStyle);
      }

      this.feature.setGeometry(geom);

      return this.proxySet(name, value);
    }
  }),

  icon: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      this.proxySet(name, value);

      if(this.feature) {
        this.feature.setStyle(this.iconStyle);
      }

      return value;
    }
  }),

  iconSet: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      this.proxySet(name, value);

      if(this.feature) {
        this.feature.setStyle(this.iconStyle);
      }

      return value;
    }
  })
});
