import Component from './base';
import { computed } from '@ember/object';
import VectorSource from "ol/source/Vector";
import Vector from "ol/layer/Vector";
import { inject as service } from '@ember/service';
import { createBox } from 'ol/interaction/Draw';

export default Component.extend({
  featureStyle: service(),
  _disabled: false,
  _mode: "box",

  setupOl() {
    this.destroyOl();

    if(!this._map) {
      return;
    }

    const vectorSource = new VectorSource({});
    this.set("vectorSource", vectorSource);

    if(this.feature) {
      this.vectorSource.addFeature(this.feature);
    }

    const vectorLayer = new Vector({
      source: vectorSource,
      renderMode: "vector"
    });

    this._map.addLayer(vectorLayer);

    this.set("currentLayers", [ vectorLayer ]);
  },

  mode: computed("disabled", {
    get() {
      return this.proxyGet(...arguments);
    },

    set() {
      return this.proxySet(...arguments);
    }
  }),

  hasValue: computed("value", "value.coordinates.[]", function() {
    if(!this.value || !this.value.coordinates || this.value.coordinates.length == 0) {
      return false;
    }

    return true;
  }),

  value: computed({
    get() {
      return this.proxyGet(...arguments);
    },

    set() {
      return this.proxySet(...arguments);
    }
  }),

  focus: computed({
    get() {
      return this.proxyGet(...arguments);
    },

    set() {
      return this.proxySet(...arguments);
    }
  }),

  disabled: computed("mode", {
    get() {
      return this.proxyGet(...arguments);
    },

    set() {
      return this.proxySet(...arguments);
    }
  }),

  updateFocus(extent) {
    if(this._focus && this._map && extent) {
      try {
        this._map.getView().fit(extent);
        // eslint-disable-next-line no-empty
      } catch(err) { }
    }
  },

  triggerChange(coordinates) {
    if(this.onChange) {
      this.onChange({
        coordinates,
        type: "Polygon"
      });
    }
  },

  clearInteractions() {
    if(this.draw) {
      this._map.removeInteraction(this.draw);
      this.set("draw", null);
    }

    if(this.select) {
      this._map.removeInteraction(this.select);
      this.set("select", null);
    }

    if(this.modify) {
      this._map.removeInteraction(this.modify);
      this.set("modify", null);
    }
  },

  createBox: computed(function() {
    return createBox();
  }),

  actions: {
    layerChange(layer) {
      this.updateFocus(layer.getSource().getExtent());
    },

    featureChange(ev) {
      const coordinates = ev.features.item(0).getGeometry().transform("EPSG:3857", "EPSG:4326").getCoordinates();
      this.triggerChange(coordinates);
    },

    onNewPolygon(ev) {
      const coordinates = ev.feature.getGeometry().transform("EPSG:3857", "EPSG:4326").getCoordinates();
      this.triggerChange(coordinates);
    }
  }
});
