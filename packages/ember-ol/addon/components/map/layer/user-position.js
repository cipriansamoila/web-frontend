import Component from './base';
import { computed } from '@ember/object';
import VectorSource from "ol/source/Vector";
import Vector from "ol/layer/Vector";
import Feature from "ol/Feature";
import { fromLonLat } from "ol/proj";
import Point from "ol/geom/Point";
import { inject as service } from '@ember/service';
import { unByKey } from "ol/Observable";

export default Component.extend({
  featureStyle: service(),
  duration: 4000,

  setupOl() {
    this.destroyOl();

    if(!this.feature || !this._map) {
      return;
    }

    const vectorSource = new VectorSource({});
    this.set("vectorSource", vectorSource);
    this.vectorSource.addFeature(this.feature);


    const vectorLayer = new Vector({
      source: vectorSource,
      renderMode: "vector"
    });

    this._map.addLayer(vectorLayer);

    this.set("currentLayers", [ vectorLayer ]);
  },

  destroyOl() {
    unByKey(this.listenerKey);
    this._super();
  },

  position: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      if(!value || !value.length || !value[0] || !value[1]) {
        return this.proxySet(name, [0, 0]);
      }

      const geom = new Point(fromLonLat(value));

      if(!this.feature) {
        this.set("feature", new Feature());
        this.feature.setStyle(this.featureStyle.userPointer());
      }

      this.feature.setGeometry(geom);

      return this.proxySet(name, value);
    }
  })
});
