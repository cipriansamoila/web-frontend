import Component from '../map/lib/base';
import { computed } from '@ember/object';
import { transform } from "ol/proj";
import { transformExtent } from "ol/proj";
import { createCenterConstraint } from 'ol/View';

const _property = {
  get(name) {
    return this.proxyPropertyGet(name);
  },
  set(name, value) {
    return this.proxyPropertySet(name, value);
  }
};

export default Component.extend({
  zoom: computed("view", _property),
  center: computed("view", _property),
  constrainRotation: computed("view", _property),
  enableRotation: computed("view", _property),
  constrainOnlyCenter: computed("view", _property),
  smoothExtentConstraint: computed("view", _property),
  maxResolution: computed("view", _property),
  minResolution: computed("view", _property),
  maxZoom: computed("view", _property),
  minZoom: computed("view", _property),
  multiWorld: computed("view", _property),
  constrainResolution: computed("view", _property),
  smoothResolutionConstraint: computed("view", _property),
  projection: computed("view", _property),
  rotation: computed("view", _property),
  zoomFactor: computed("view", _property),

  centerM: computed("center", {
    get() {
      return transform(this.proxyPropertyGet("center"), 'EPSG:3857', 'EPSG:4326');
    },

    set(name, value) {
      if(!value) {
        return [0, 0];
      }

      const newValue = transform(value, 'EPSG:4326', 'EPSG:3857');

      return this.proxyPropertySet("center", newValue);
    }
  }),

  constraintExtent: computed("view", {
    get() {
      if(!this.view || !this.map) {
        return;
      }

      return null;
    },

    set(name, value) {
      if(!this.view || !this.map) {
        return value;
      }

      const constraints = this.map.getView().getConstraints();
      constraints["center"] = createCenterConstraint({ extent: value });

      return this.map.getView().set("extent", value);
    }
  }),

  extent: computed("view", {
    get() {
      if(!this.view || !this.map) {
        return;
      }

      return this.map.getView().calculateExtent(this.map.getSize());
    },

    set(name, value) {
      if(!this.view || !this.map) {
        return value;
      }

      let options = {};

      if(this.padding) {
        options.padding = this.padding;
      }

      if(value) {
        this.view.fit(value, options);
      }

      return this.map.getView().calculateExtent(this.map.getSize());
    }
  }),

  extentM: computed("extent", {
    get() {
      const extent = this._map.getView().calculateExtent(this._map.getSize());
      return transformExtent(extent, "EPSG:3857", "EPSG:4326");
    },

    set(name, value) {
      if(!value) {
        return;
      }

      value = transformExtent(value, "EPSG:4326", "EPSG:3857");

      let options = {};

      if(this.padding) {
        options.padding = this.padding;
      }

      this.view.fit(value, options);

      const extent = this.map.getView().calculateExtent(this.map.getSize());
      return transformExtent(extent, "EPSG:3857", "EPSG:4326");
    }
  }),

  setupOl() {
    this.destroyOl();

    if(!this._map) {
      return;
    }

    const view = this._map.getView();
    this.applyPending(view);

    this.set("_changeExtent", () => {
      if (this.isDestroyed) {
        return;
      }

      this.notifyPropertyChange("extent");
      this.notifyPropertyChange("extentM");

      if(this.onChangeExtent) {
        this.onChangeExtent(this.get("extentM"));
      }
    });
    this._map.on("moveend", this._changeExtent);

    this.set("_changeCenter", () => {
      if(this.onChangeCenter) {
        this.onChangeCenter(view);
      }
    });
    view.on("change:center", this._changeCenter);

    this.set("_changeResolution", () => {
      if(this.onChangeResolution) {
        this.onChangeResolution(view);
      }
    });
    view.on("change:resolution", this._changeResolution);

    this.set("_changeRotation", () => {
      if(this.onChangeRotation) {
        this.onChangeRotation(view);
      }
    });
    view.on("change:rotation", this._changeRotation);

    this.set("_change", () => {
      if(this.onChange) {
        this.onChange(view);
      }
    });
    view.on("change", this._change);

    this.set("view", view);
  },

  destroyOl() {
    this._super();

    if(this.view) {
      this.view.un("change:center", this._changeCenter);
      this.view.un("change:resolution", this._changeResolution);
      this.view.un("change:rotation", this._changeRotation);
      this.view.un("change", this._change);
    }
  }
});
