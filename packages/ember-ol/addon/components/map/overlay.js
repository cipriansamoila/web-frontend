import Component from '@ember/component';
import Overlay from "ol/Overlay";
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ["map-overlay"],

  position: computed({
    get() {
      return this._position;
    },

    set(key, value) {
      if(this.overlay) {
        this.overlay.setPosition(value);
      }

      return value;
    }
  }),

  didRender: function() {
    const overlay = new Overlay({
      element: this.element,
      stopEvent: true,
      insertFirst: true,
      positioning: "bottom-center",
      autoPan: true
    });

    this.set("overlay", overlay);

    try {
      this.map.addOverlay(this.overlay);
      this.overlay.setPosition(this.position);
    // eslint-disable-next-line no-empty
    } catch(err) { }
  },

  willDestroy: function() {
    this.map.removeOverlay(this.overlay);
  },

  willRender: function() {
    this.map.removeOverlay(this.overlay);
  }
});
