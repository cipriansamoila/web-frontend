import Component from '../../lib/base';
import { computed } from '@ember/object';
import MultiPolygon from 'ol/geom/MultiPolygon';
import Feature from "ol/Feature";

export default Component.extend({
  setupOl() {
    this.destroyOl();

    if(!this.feature || !this._source) {
      return;
    }

    this._source.addFeature(this.feature);
  },

  destroyOl() {
    this._super();

    if(this.source) {
      let exists;
      this.source.forEachFeature((item) => {
        exists = exists || item == this.feature;
      });

      if(exists) {
        this.source.removeFeature(this.feature);
      }
    }
  },

  value: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      if(!this.feature) {
        this.set("feature", new Feature());
      }

      const geometry = new MultiPolygon(value.coordinates?.slice?.() ?? []);
      geometry.transform("EPSG:4326", "EPSG:3857");

      this.feature.setGeometry(geometry);

      return this.proxySet(name, value);
    }
  }),

  source: computed({
    get(name) {
      return this.proxyGet(name);
    },
    set(name, value) {
      return this.proxySet(name, value);
    }
  })
});
