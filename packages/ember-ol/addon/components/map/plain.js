import { inject as service } from "@ember/service";
import { fromLonLat, transformExtent, transform } from "ol/proj";
import Component from "@glimmer/component";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class MapPlainComponent extends Component {
  @service ol;
  @tracked disabled = false;
  @tracked firstZoom = 14;
  @tracked map;

  get viewbox() {
    if (!this.map) {
      return null;
    }

    var extent = this.map.getView().calculateExtent(this.map.getSize());
    extent = transformExtent(extent, "EPSG:3857", "EPSG:4326");

    if (extent.filter((a) => isNaN(a)).length > 0) {
      return null;
    }

    return extent.join(",");
  }

  set viewbox(value) {
    if (!this.map) {
      return;
    }

    let currentExtent = this.map.getView().calculateExtent(this.map.getSize());

    try {
      value = value.split(",");
      value = value.map((a) => parseFloat(a));
    } catch (err) {
      value = value.map((a) => parseFloat(a));
    }

    const transformedValue = transformExtent(value, "EPSG:4326", "EPSG:3857");

    currentExtent = transformExtent(currentExtent, "EPSG:3857", "EPSG:4326");

    const order = 1000000;
    const a = currentExtent.map((a) => Math.round(a * order) / order).join("|");
    const b = value.map((a) => Math.round(a * order) / order).join("|");

    if (a != b) {
      this.map.getView().fit(transformedValue, this.map.getSize());
    }
  }

  get zoom() {
    if (!this.map) {
      return null;
    }

    return this.map.getView().getZoom();
  }

  set zoom(value) {
    this.map.getView?.().setZoom?.(value);
  }

  @action
  async setupMapElement(element) {
    this.mapElement = element;

    this.map = await this.ol.insert(this.mapElement);
    if (this.isDestroyed || this.isDestroying) {
      return;
    }

    this.element.olMap = this.map;

    let useCenter = true;

    if (this.initialExtent) {
      this.map.getView().set("extent", this.initialExtent);
    } else {
      this.map.getView().set("extent", null);
    }

    if (this.firstViewbox) {
      this.viewbox = this.args.firstViewbox;
      useCenter = false;
    }

    if (useCenter) {
      this.map.getView().setZoom(this.firstZoom);

      if (this.firstCenter) {
        this.map.getView().setCenter(fromLonLat(this.firstCenter));
      }
    }

    this._moveend = () => {
      // eslint-disable-next-line no-self-assign
      this.viewbox = this.viewbox;

      if (this.onCenterMove) {
        const center = transform(
          this.map.getView().getCenter(),
          "EPSG:3857",
          "EPSG:4326"
        );
        this.onCenterMove(center);
      }
    };

    this.map.on("moveend", this._moveend);
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.map?.un?.("moveend", this._moveend);
  }
}
