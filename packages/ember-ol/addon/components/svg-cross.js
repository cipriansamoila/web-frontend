import Component from "@ember/component";
import { computed } from "@ember/object";
import { htmlSafe } from "@ember/template";

export default Component.extend({
  classNames: ["cross", "d-flex", "justify-content-center"],
  lineStyle:
    "opacity:1;vector-effect:none;fill-opacity:1;fill-rule:nonzero;stroke:none;stroke-width:0.24582477;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:0",

  c1: computed('color1', 'lineStyle', function () {
    return htmlSafe(`${this.lineStyle};fill:${this.color1}`);
  }),

  c2: computed('color2', 'lineStyle', function () {
    return htmlSafe(`${this.lineStyle};fill:${this.color2}`);
  }),
});
