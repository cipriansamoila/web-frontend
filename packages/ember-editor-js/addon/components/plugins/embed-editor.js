import Embed from "@editorjs/embed";

export default class EmbedEditor extends Embed {
  constructor() {
    super(...arguments);
  }

  rendered() {
    if (
      !this.element ||
      !this.element.parentElement ||
      !this.element.parentElement.parentElement
    ) {
      return;
    }

    this.element.parentElement.parentElement.classList.add("is-embed");
  }
}
