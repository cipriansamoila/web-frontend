import sanitizeHtml from 'sanitize-html';

export default class TrayEditor {
  constructor({ data }) {
    this.data = data;
  }

  static get sanitize() {
    return {
      br: true,
      b: true,
      a: {
        href: true,
      },
      i: true,
    };
  }

  render() {
    const container = document.createElement('div');
    container.classList.add('page-col-tray', 'editor');

    this.titleInput = document.createElement('div');
    this.titleInput.innerHTML = sanitizeHtml(this.data?.title) ?? 'Lorem ipsum';
    this.titleInput.contentEditable = true;

    const titleIcon = document.createElement('div');
    titleIcon.innerHTML = `<svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="minus" class="svg-inline--fa fa-minus fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512"><path fill="currentColor" d="M416 208H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"></path></svg>`;
    titleIcon.style.float = 'right';
    titleIcon.style.height = '1em';

    const title = document.createElement('div');
    title.classList.add('title');
    title.appendChild(titleIcon);
    title.appendChild(this.titleInput);

    this.paragraph = document.createElement('p');
    this.paragraph.classList.add('paragraph');
    this.paragraph.innerHTML =
      sanitizeHtml(this.data?.paragraph) ??
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.';
    this.paragraph.contentEditable = true;

    container.appendChild(title);
    container.appendChild(this.paragraph);

    return container;
  }

  save() {
    return {
      title: this.titleInput.innerHTML,
      paragraph: this.paragraph.innerHTML,
    };
  }

  static get toolbox() {
    return {
      title: 'Tray',
      icon: `<svg
      aria-hidden="true"
      focusable="false"
      data-prefix="far"
      data-icon="window-maximize"
      class="svg-inline--fa fa-window-maximize fa-w-16"
      role="img"
      viewBox="0 0 512 512"
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      xmlns:svg="http://www.w3.org/2000/svg">
      <path
     fill="currentColor"
     d="M 365.89691,137.64948 H 146.10309 c -14.00129,0 -25.36083,11.35954 -25.36083,25.36082 v 185.9794 c 0,14.00128 11.35954,25.36082 25.36083,25.36082 h 219.79382 c 14.00129,0 25.36082,-11.35954 25.36082,-25.36082 V 163.0103 c 0,-14.00128 -11.35953,-25.36082 -25.36082,-25.36082 z m 0,208.17011 c 0,1.74356 -1.42654,3.17011 -3.1701,3.17011 H 149.27319 c -1.74356,0 -3.1701,-1.42655 -3.1701,-3.17011 V 222.18556 h 219.79382 z"
     id="path2"
     style="stroke-width:0.528351" />
   </svg>`,
    };
  }
}
