import Component from '@glimmer/component';
import EditorJS from '@editorjs/editorjs';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';
import Header from '@editorjs/header';
import List from '@editorjs/list';
import Delimiter from '@editorjs/delimiter';
import Quote from '@editorjs/quote';
import Table from '@editorjs/table';
import EmbedEditor from './plugins/embed-editor';
import TrayEditor from './plugins/tray-editor';
import ImageTool from '@editorjs/image';
import { later } from '@ember/runloop';

export default class ManageEditorComponent extends Component {
  elementId = `editor-js-${guidFor(this)}`;

  @tracked editor = null;
  @tracked isReady = false;

  changed() {
    this.editor
      ?.save?.()
      .then((outputData) => {
        this.args.onChange?.(outputData);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  get value() {
    if (!this.args.value) {
      return {};
    }

    if (this.args.value.blocks) {
      return this.args.value;
    }

    return {};
  }

  @action
  setup() {
    const tools = {
      tray: {
        class: TrayEditor,
        inlineToolbar: true,
      },
      header: Header,
      list: {
        class: List,
        inlineToolbar: true,
      },
      delimiter: Delimiter,
      quote: {
        class: Quote,
        inlineToolbar: true,
      },
      table: {
        class: Table,
      },
      embed: {
        class: EmbedEditor,
        config: {
          services: {
            codepen: true,
            coub: true,
            imgur: true,
            gfycat: true,
            'twitch-video': true,
            'twitch-channel': true,
            vimeo: true,
            youtube: true,
            twitter: true,
            instagram: true,
          },
        },
      },
    };

    if (this.args.onPictureUpload) {
      tools.image = {
        class: ImageTool,
        config: {
          uploader: {
            uploadByFile: (file) => {
              return this.args.onPictureUpload(file, 'file');
            },
            uploadByUrl: (url) => {
              return this.args.onPictureUpload(url, 'url');
            },
          },
        },
      };
    }

    this.editor = new EditorJS({
      holder: this.elementId,
      minHeight: 0,
      logLevel: 'ERROR',
      tools,
      onReady: () => {
        later(() => {
          this.isReady = true;
          this.args.onReady?.(this.editor);
        }, 1000);
      },
      onChange: () => {
        this.changed();
      },
      data: this.value,
    });
  }

  willDestroy() {
    super.willDestroy(...arguments);

    try {
      this.editor?.destroy?.();
    } catch (err) {
      console.log(err);
    }
  }
}
