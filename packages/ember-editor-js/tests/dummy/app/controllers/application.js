import Controller from "@ember/controller";
import { action } from "@ember/object";
import { tracked } from "@glimmer/tracking";

export default class ApplicationController extends Controller {
  @tracked testValue = {
    time: 1627205220404,
    blocks: [
      {
        id: "9mYl_3pCJK",
        type: "header",
        data: {
          text: "Editor.js",
          level: 2,
        },
      },
      {
        id: "Smd82_odWB",
        type: "paragraph",
        data: {
          text: "Hey. Meet the new Editor. On this page you can see it in action — try to edit this text.",
        },
      },
      {
        id: "urN4pX0XiM",
        type: "header",
        data: {
          text: "Key features",
          level: 3,
        },
      },
      {
        id: "5Nw8B09_UC",
        type: "list",
        data: {
          style: "unordered",
          items: [
            "It is a block-styled editor",
            "It returns clean data output in JSON",
            "Designed to be extendable and pluggable with a simple API",
          ],
        },
      },
      {
        type: "tray",
        data: {
          title: "See what's on the menu",
          paragraph:
            "• Browse is your gateway to all of the Open Green Maps, and the thousands of featured places charted by local people, all around the world. • Our logo takes you to the World View. • OGM2 responds to the shape of the device or computer it’s being viewed on.",
          "link-title": "More questions? there's an FAQ",
          link: "help.faq",
        },
      },
      {
        id: "Smd82_odWB",
        type: "paragraph",
        data: {
          text: "Hey. Meet the new Editor. On this page you can see it in action — try to edit this text.",
        },
      },
    ],
  };

  get serializedValue() {
    return JSON.stringify(this.testValue, null, " ");
  }

  @action
  updateTestValue(value) {
    this.testValue = value;
  }
}
