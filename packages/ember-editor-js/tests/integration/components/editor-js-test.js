import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, waitFor, fillIn, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe.skip('Integration | Component | editor-js', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<EditorJs />`);
    expect(this.element.querySelector('.codex-editor')).to.exist;
  });

  it('renders markdown text', async function () {
    this.set('value', '## title\n\nsome text\n');

    await render(hbs`<EditorJs @value={{this.value}} />`);

    await waitFor('.editor-is-ready', { timeout: 2000 });

    expect(this.element.querySelector('h2').textContent).to.equal('title');
    expect(this.element.querySelector('.ce-paragraph').textContent).to.equal(
      'some text'
    );
  });

  it('triggers on change when the content is changed', async function () {
    this.timeout(10000);

    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'Editor.js',
            level: 2,
          },
        },
        {
          type: 'paragraph',
          data: {
            text: 'Hey. Meet the new Editor.',
          },
        },
      ],
    });

    let newValue;
    this.set('change', (value) => {
      newValue = value;
    });

    await render(
      hbs`<EditorJs @value={{this.value}} @onChange={{this.change}}/>`
    );

    await waitFor('.editor-is-ready', { timeout: 2000 });

    await click('h2');
    await fillIn('h2', ' --- new text');
    await click('.codex-editor');

    await waitUntil(() => newValue);

    expect(newValue.blocks).to.deep.equal([
      { type: 'header', data: { text: ' --- new text', level: 2 } },
      { type: 'paragraph', data: { text: 'Hey. Meet the new Editor.' } },
    ]);
  });
});
