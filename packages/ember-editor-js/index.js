'use strict';

module.exports = {
  name: require('./package').name,
  included: function (/* app */) {
    this._super.included.apply(this, arguments);
  },
  options: {
    babel: {
      plugins: [require.resolve('ember-auto-import/babel-plugin')],
    },
  },
};
