import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | help-tooltip', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<HelpTooltip>message</HelpTooltip>`);
    expect(this.element.querySelector('.fa-question-circle')).to.exist;
    expect(this.element.querySelector('.content').textContent.trim()).to.equal(
      'message'
    );
  });
});
