import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | visibility-icon', function() {
  setupRenderingTest();

  it('renders nothing', async function() {
    await render(hbs`<VisibilityIcon />`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector(".icon-not-published")).not.to.exist;
    expect(this.element.querySelector(".icon-pending")).not.to.exist;
  });

  it('renders nothing when value is true', async function() {
    await render(hbs`<VisibilityIcon @value={{true}}/>`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector(".icon-not-published")).not.to.exist;
    expect(this.element.querySelector(".icon-pending")).not.to.exist;
  });

  it('renders nothing when value is 1', async function() {
    await render(hbs`<VisibilityIcon @value={{1}}/>`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector(".icon-not-published")).not.to.exist;
    expect(this.element.querySelector(".icon-pending")).not.to.exist;
  });

  it('renders the private icon when the value is false', async function() {
    await render(hbs`<VisibilityIcon @value={{false}}/>`);
    expect(this.element.querySelector(".icon-not-published")).to.exist;
  });

  it('renders the private icon when the value is 0', async function() {
    await render(hbs`<VisibilityIcon @value={{0}}/>`);
    expect(this.element.querySelector(".icon-not-published")).to.exist;
  });

  it('renders the pending icon when the value is -1', async function() {
    await render(hbs`<VisibilityIcon @value={{-1}}/>`);
    expect(this.element.querySelector(".icon-pending")).to.exist;
  });
});
