import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | user-account-form', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<UserAccountForm />`);
    expect(this.element.textContent.trim()).to.contain('Here are some info about your account');
  });
});
