import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | user-profile-form', function() {
  setupRenderingTest();

  it('renders all profile fields', async function() {
    await render(hbs`<UserProfileForm />`);

    const labels = [...this.element.querySelectorAll("label") ].map(a => a.textContent.trim());

    expect(labels).to.deep.equal([
      'Status message',
      'Show the contributions calendar',
      'Show private contributions',
      'Salutation',
      'Title',
      'First name',
      'Last name',
      'Skype',
      'Linkedin',
      'Twitter',
      'Website url',
      'Location',
      'Job title',
      'Organization',
      'Bio']
    );

    expect(this.element.querySelector(".btn-profile-update")).to.have.attribute("disabled", "");
  });

  it('shows a enabled update button when it has dirty attributes', async function() {
    this.set("profile", {
      hasDirtyAttributes: true
    });

    await render(hbs`<UserProfileForm @profile={{this.profile}}/>`);

    expect(this.element.querySelector(".btn-profile-update")).not.to.have.attribute("disabled", "");
  });

  it('triggers on update event when the update button is pressed', async function() {
    this.set("profile", {
      hasDirtyAttributes: true
    });

    let updated;

    this.set("update", () => {
      updated = true;
    });

    await render(hbs`<UserProfileForm @profile={{this.profile}} @onUpdate={{this.update}}/>`);

    await click(".btn-profile-update");

    expect(updated).to.equal(true);
  });

  it('shows an detailed location field when detailedLocation is true', async function() {
    this.set("profile", {
      hasDirtyAttributes: true
    });

    await render(hbs`<UserProfileForm @profile={{this.profile}} @detailedLocation={{true}}/>`);
    const labels = [...this.element.querySelectorAll("label") ].map(a => a.textContent.trim());

    expect(labels).to.deep.equal([
      'Status message',
      'Show the contributions calendar',
      'Show private contributions',
      'Salutation',
      'Title',
      'First name',
      'Last name',
      'Skype',
      'Linkedin',
      'Twitter',
      'Website url',
      "Country",
      "Province/State",
      "City",
      "Postal code",
      'Job title',
      'Organization',
      'Bio']
    );
  });
});
