import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | image-sphere', function() {
  setupRenderingTest();

  it('renders nothing', async function() {
    await render(hbs`<ImageSphere />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders nothing when is not 360', async function() {
    this.set("picture", {
      picture: "some picture"
    });
    await render(hbs`<ImageSphere @picture={{this.picture}} />`);
    expect(this.element.querySelector("img")).not.to.exist;
    expect(this.element.querySelector(".picture-360-view")).not.to.exist;
  });

  it('does not render an image if it is a 360 image', async function() {
    this.set("picture", {
      picture: "some picture",
      is360: true
    });
    await render(hbs`<ImageSphere @picture={{this.picture}} @alt="something"/>`);

    expect(this.element.querySelector("img")).not.to.exist;
    expect(this.element.querySelector(".picture-360-view")).to.exist;
  });
});
