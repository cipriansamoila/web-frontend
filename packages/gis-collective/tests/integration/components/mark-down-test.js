import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

describe('mark-down', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<MarkDown/>`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('render http link', async function () {
    this.set('value1', 'http://test.com');

    await render(hbs`<MarkDown @value={{this.value1}} />`);

    expect(this.element.firstChild.innerHTML.trim()).to.equal(
      `<p><a href="http://test.com" class="unsecure">http://test.com</a></p>`
    );
  });

  it('render http link with a target attr', async function () {
    this.set('value1', 'http://test.com');

    await render(hbs`<MarkDown @value={{this.value1}} @target="_blank" />`);

    expect(this.element.querySelector('a')).to.exist;

    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      'http://test.com'
    );
    expect(this.element.querySelector('a')).to.have.attribute(
      'class',
      'unsecure'
    );
  });

  it('render https link', async function () {
    this.set('value2', 'https://test.com');

    await render(hbs`<MarkDown @value={{this.value2}} />`);

    expect(this.element.querySelector('a')).to.exist;

    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      'https://test.com'
    );
    expect(this.element.querySelector('a')).to.have.attribute(
      'class',
      'secure'
    );
  });

  it('render local link', async function () {
    this.set('value3', '[/local](/local)');

    await render(hbs`<MarkDown @value={{this.value3}} />`);

    expect(this.element.firstChild.innerHTML.trim()).to.equal(
      `<p><a href="/local" class="secure">/local</a></p>`
    );

    expect(this.element.querySelector('a')).to.exist;

    expect(this.element.querySelector('a')).to.have.attribute('href', '/local');
    expect(this.element.querySelector('a')).to.have.attribute(
      'class',
      'secure'
    );
  });

  it('render short youtube links as embedded videos', async function () {
    this.set('value4', '[Making a Map](https://youtu.be/D2ZXjnAwM6o)');

    await render(hbs`<MarkDown @value={{this.value4}} />`);

    expect(this.element.querySelector('a')).not.to.exist;
    expect(this.element.querySelector('iframe')).to.exist;

    expect(this.element.querySelector('iframe')).to.have.attribute(
      'src',
      'https://www.youtube.com/embed/D2ZXjnAwM6o'
    );
    expect(this.element.querySelector('iframe')).to.have.attribute(
      'frameborder',
      '0'
    );
    expect(this.element.querySelector('iframe')).to.have.attribute(
      'allow',
      'autoplay; encrypted-media; picture-in-picture'
    );
    expect(this.element.querySelector('iframe')).to.have.attribute(
      'class',
      'youtube'
    );
  });

  it('render tags link', async function () {
    this.set('value4', 'some text #tag1 #tag2 other text');

    await render(hbs`<MarkDown @value={{this.value4}} />`);

    expect(this.element.firstChild.innerHTML.trim()).to.equal(
      `<p>some text <a href="/test/tag1" class="secure">#tag1</a> <a href="/test/tag2" class="secure">#tag2</a> other text</p>`
    );
  });

  it('render tags link at the end of the line', async function () {
    this.set('value4', 'some text #tag1 #tag2\nother text');

    await render(hbs`<MarkDown @value={{this.value4}} />`);

    expect(this.element.firstChild.innerHTML.trim()).to.equal(
      `<p>some text <a href="/test/tag1" class="secure">#tag1</a> <a href="/test/tag2" class="secure">#tag2</a>\nother text</p>`
    );
  });

  it('escapes the text', async function () {
    this.set(
      'value5',
      `<p>&nbsp;</p><center>
      <iframe style="border: 1px solid #CCC; border-width: 1px; margin-bottom: 5px; max-width: 100%;"
      src="//www.slideshare.net/slideshow/embed_code/key/d8zvZuHpklPOvj" width="510" height="420"
      frameborder="0" marginwidth="0" marginheight="0" scrolling="no" allowfullscreen=""></iframe>
      <div style="margin-bottom: 5px;"><strong>
      <a title="SDGs and Green Map Icons v2 1-18"
      href="//www.slideshare.net/wendybrawer/sdgs-and-green-map-icons-v2-118" target="_blank"
      rel="noopener">SDGs and Green Map Icons v2 1-18</a> </strong> from
      <strong><a href="//www.slideshare.net/wendybrawer" target="_blank" rel="noopener">Green Map System</a>
      </strong></div></center>`
    );

    await render(hbs`<MarkDown @value={{this.value5}} />`);

    expect(this.element.firstChild.innerHTML.trim()).to.equal(
      `<p>SDGs and Green Map Icons v2 1-18  from\n      Green Map System</p>`
    );
  });
});
