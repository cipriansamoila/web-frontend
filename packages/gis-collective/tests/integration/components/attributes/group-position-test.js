import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/group-position', function() {
  setupRenderingTest();

  it('renders nothing when the position is not set', async function() {
    await render(hbs`<Attributes::GroupPosition />`);

    expect(this.element.querySelector(".attributes-position")).not.to.exist;
    expect(this.element.textContent.trim()).to.contain('position');
  });

  it('zooms to the extent when is set', async function() {
    this.set("position", {
      coordinates: [0, 0]
    });

    this.set("extent", {
      type: "Polygon",
      coordinates: [[[1, 2], [3, 4], [4, 5.1], [4.5, 5], [1, 2]]]
    });

    await render(hbs`<Attributes::GroupPosition @position={{this.position}} @extent={{this.extent}}/>`);
    await waitUntil(() => this.element.querySelector(".map").olMap);

    const map = this.element.querySelector(".map").olMap;
    const center = map.getView().getCenter();

    expect(center).to.deep.equal([306128.5996815023, 395582.3980045552]);
  });
});
