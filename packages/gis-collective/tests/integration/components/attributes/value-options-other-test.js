import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, fillIn, blur } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/value-options-other', function () {
  setupRenderingTest();

  it('adds the other option', async function () {
    this.set('options', 'map1,map2');
    await render(
      hbs`<Attributes::ValueOptionsOther @options={{this.options}}/>`
    );

    await click('.btn-add');

    const items = this.element.querySelectorAll('.list-group-item');
    const values = [...items].map((a) => a.textContent.trim());

    expect(values).to.deep.equal(['map1', 'map2', 'Other']);
  });

  it('displays an input field when selecting the Other option', async function () {
    this.set('options', 'map1,map2');
    await render(
      hbs`<Attributes::ValueOptionsOther @options={{this.options}}/>`
    );

    await click('.btn-add');
    const otherItem = Array.from(
      this.element.querySelectorAll('.list-group-item')
    )
      .slice(-1)
      .pop();
    await click(otherItem);

    expect(this.element.querySelector('input')).to.exist;
    expect(
      this.element.querySelector('.input-group-text').textContent
    ).to.contain('Please specify if you selected “Other”.');
  });

  it('triggers the on change event when an option is selected', async function () {
    this.set('options', 'map1,map2');

    let changedValue;
    this.set('change', (newValue) => {
      changedValue = newValue;
    });

    await render(
      hbs`<Attributes::ValueOptionsOther @onChange={{this.change}} @options={{this.options}}/>`
    );

    await click('.btn-add');
    await click('.list-group-item');

    expect(changedValue).equal('map1');
  });

  it('triggers the on change event with an empty string when the Other option is selected', async function () {
    this.set('options', 'map1,map2');

    let changedValue;
    this.set('change', (newValue) => {
      changedValue = newValue;
    });

    await render(
      hbs`<Attributes::ValueOptionsOther @onChange={{this.change}} @options={{this.options}}/>`
    );

    await click('.btn-add');

    const otherItem = Array.from(
      this.element.querySelectorAll('.list-group-item')
    )
      .slice(-1)
      .pop();
    await click(otherItem);

    expect(changedValue).equal('');
  });

  it('triggers the input update event when Other is selected and an input provided', async function () {
    this.set('options', 'map1,map2');

    let changedValue;
    this.set('change', (newValue) => {
      changedValue = newValue;
    });

    await render(
      hbs`<Attributes::ValueOptionsOther @onChange={{this.change}} @options={{this.options}}/>`
    );
    await click('.btn-add');

    const otherItem = Array.from(
      this.element.querySelectorAll('.list-group-item')
    )
      .slice(-1)
      .pop();
    await click(otherItem);

    await fillIn('input', 'my custom input');
    await blur('input');

    expect(changedValue).equal('my custom input');
  });

  it('shows the string value as selected when the options are opened', async function () {
    this.set('options', 'map1,map2');

    await render(
      hbs`<Attributes::ValueOptionsOther @value="map1" @options={{this.options}}/>`
    );

    await click('.btn-change');

    const activeElements = this.element.querySelectorAll(
      '.list-group-item.active'
    );

    expect(activeElements.length).to.equal(1);
    expect(activeElements[0]).to.contain.text('map1');
  });

  it('renders the Other input as selected when an empty value is passed', async function () {
    this.set('options', 'map1,map2');

    await render(
      hbs`<Attributes::ValueOptionsOther @value="" @options={{this.options}}/>`
    );

    await click('.btn-change');

    const activeElements = this.element.querySelectorAll(
      '.list-group-item.active'
    );

    expect(activeElements.length).to.equal(1);
    expect(activeElements[0]).to.contain.text('Other');
    expect(this.element.querySelector('.list-group-item input')).to.exist;
    expect(this.element.querySelector('.list-group-item input').value).to.equal(
      ''
    );
  });

  it('renders the string value from the Other input', async function () {
    this.set('options', 'map1,map2');

    await render(
      hbs`<Attributes::ValueOptionsOther @value="other value" @options={{this.options}}/>`
    );

    await click('.btn-change');

    const activeElements = this.element.querySelectorAll(
      '.list-group-item.active'
    );

    expect(activeElements.length).to.equal(1);
    expect(activeElements[0]).to.contain.text('Other');
    expect(this.element.querySelector('.list-group-item input')).to.exist;
    expect(
      this.element.querySelector('.list-group-item input').value
    ).to.contain('other value');
  });

  it('does not render a selected item when no value is passed', async function () {
    this.set('options', 'map1,map2');

    await render(
      hbs`<Attributes::ValueOptionsOther @options={{this.options}}/>`
    );

    expect(this.element.querySelector('.btn-change')).to.not.exist;
  });
});
