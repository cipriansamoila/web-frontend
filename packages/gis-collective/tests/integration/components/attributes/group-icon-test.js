import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

describe('Integration | Component | attributes/group-icon', function() {
  setupRenderingTest();

  it('renders nothing by default', async function() {
    // Template block usage:
    await render(hbs`
      <Attributes::GroupIcon>
        template block text
      </Attributes::GroupIcon>
    `);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('should select the icon when it is matched', async function () {
    this.set("icons1", [{
      name: "test"
    }]);

    await render(hbs`
      <Attributes::GroupIcon @icons={{icons1}} @group="test" as |icon|>
        <p>{{icon.name}}</p>
      </Attributes::GroupIcon>
    `);

    expect(this.element.querySelectorAll("p").length).to.equal(1);
    expect(this.element.querySelector("p").textContent.trim()).to.have.equal("test");
  });

  it('should match the group when the case is different', async function () {
    this.set("icons2", [{
      name: "teST"
    }]);

    await render(hbs`
      <Attributes::GroupIcon @icons={{icons2}} @group="TEST" as |icon|>
        <p>{{icon.name}}</p>
      </Attributes::GroupIcon>
    `);

    expect(this.element.querySelectorAll("p").length).to.equal(1);
    expect(this.element.querySelector("p").textContent.trim()).to.have.equal("teST");
  });

  it('should not match the group when the name is different', async function () {
    this.set("icons3", [{
      name: "teST"
    }]);

    await render(hbs`
      <Attributes::GroupIcon @icons={{icons3}} @group="other" as |icon|>
        <p>{{icon.name}}</p>
      </Attributes::GroupIcon>
    `);

    expect(this.element.querySelectorAll("p").length).to.equal(0);
  });

  it('should match the group by the otherNames', async function () {
    this.set("icons4", [{
      name: "teST",
      otherNames: ["otherTest"]
    }]);

    await render(hbs`
      <Attributes::GroupIcon @icons={{icons4}} @group="OtherTest" as |icon|>
        <p>{{icon.name}}</p>
      </Attributes::GroupIcon>
    `);

    expect(this.element.querySelectorAll("p").length).to.equal(1);
    expect(this.element.querySelector("p").textContent.trim()).to.have.equal("teST");
  });
});
