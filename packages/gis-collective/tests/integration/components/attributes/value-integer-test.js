import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, typeIn } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import waitUntil from '@ember/test-helpers/wait-until';

describe('Integration | Component | attributes/value-integer', function () {
  setupRenderingTest();

  it('renders an empty field by default', async function () {
    await render(hbs`<Attributes::ValueInteger />`);

    expect(this.element.querySelector('.value-integer')).to.exist;
  });

  it('renders the integer value when it is set', async function () {
    await render(hbs`<Attributes::ValueInteger @value={{12}} />`);

    expect(this.element.querySelector('.value-integer input').value).to.equal(
      '12'
    );
  });

  it('allows users to set digits', async function () {
    let changedValue;
    this.set('onChange', (newValue) => {
      changedValue = newValue;
    });

    await render(hbs`<Attributes::ValueInteger @onChange={{this.onChange}}/>`);

    await typeIn('input', '32');

    expect(changedValue).equal(32);
  });

  it('does not allow input of non-digits', async function () {
    let changedValue;
    this.set('onChange', (newValue) => {
      changedValue = newValue;
    });

    await render(hbs`<Attributes::ValueInteger @onChange={{this.onChange}}/>`);
    await typeIn('input', 'text');

    expect(changedValue).to.be.undefined;
  });

  it('sets the numeric updated value', async function () {
    this.set('value', 1);

    await render(hbs`<Attributes::ValueInteger @value={{this.value}} />`);
    await typeIn('input', '11');

    this.set('value', 2);

    await waitUntil(
      () =>
        this.element.querySelector('.value-integer input').value.indexOf('1') ==
        -1
    );

    expect(this.element.querySelector('.value-integer input').value).to.equal(
      '2'
    );
  });
});
