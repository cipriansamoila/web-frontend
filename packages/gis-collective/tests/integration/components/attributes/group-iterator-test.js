import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

describe('Integration | Component | attributes/group-iterator', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`
      <Attributes::GroupIterator as |field|>
        <p>{{field.key}}:{{field.value}}:{{field.type}}:{{field.options}}:{{field.isValid}}</p>
      </Attributes::GroupIterator>
    `);

    expect(this.element.querySelectorAll("p").length).to.equal(0);
  });

  it('renders the attributes when there is no icon', async function () {
    this.set("attributes1", {
      "item1": "value1",
      "item2": "value2",
      "item3": "value3"
    })

    await render(hbs`
      <Attributes::GroupIterator @attributes={{attributes1}} as |field|>
        <p>{{field.key}}:{{field.value}}:{{field.type}}:{{field.options}}:{{field.isValid}}</p>
      </Attributes::GroupIterator>
    `);

    expect(this.element.querySelectorAll("p").length).to.equal(3);
    expect(this.element.querySelectorAll("p")[0]).to.have.text("item1:value1:short text::true");
    expect(this.element.querySelectorAll("p")[1]).to.have.text("item2:value2:short text::true");
    expect(this.element.querySelectorAll("p")[2]).to.have.text("item3:value3:short text::true");
  });

  it('renders the attributes when there is an icon', async function () {
    this.set("attributes2", {
      "item1": "value1",
      "item2": "value2",
      "item3": "value3"
    });

    this.set("icons", [{
      name: "test",
      attributes: [{
        "name": "item1",
        "type": "short text"
      }, {
        "name": "item2",
        "type": "long text",
        "options": ""
      }, {
        "name": "item3",
        "type": "integer",
        "options": ""
      }]
    }]);

    await render(hbs`
      <Attributes::GroupIterator @icons={{icons}} @group="test" @attributes={{attributes2}} as |field|>
        <p>{{field.key}}:{{field.value}}:{{field.type}}:{{field.options}}:{{field.isValid}}</p>
      </Attributes::GroupIterator>
    `);

    expect(this.element.querySelectorAll("p").length).to.equal(3);
    expect(this.element.querySelectorAll("p")[0]).to.have.text("item1:value1:short text::true");
    expect(this.element.querySelectorAll("p")[1]).to.have.text("item2:value2:long text::true");
    expect(this.element.querySelectorAll("p")[2]).to.have.text("item3:value3:integer::false");
  });

  it('should match icons ignoring the group case', async function () {
    this.set("attributes3", {
      "item1": "value1",
      "item2": "value2",
      "item3": "value3"
    });

    this.set("icons2", [{
      name: "TEST",
      attributes: [{
        "name": "item1",
        "type": "short text"
      }, {
        "name": "item2",
        "type": "long text",
        "options": ""
      }, {
        "name": "item3",
        "type": "integer",
        "options": ""
      }]
    }]);

    await render(hbs`
      <Attributes::GroupIterator @icons={{icons2}} @group="test" @attributes={{attributes3}} as |field|>
        <p>{{field.key}}:{{field.value}}:{{field.type}}:{{field.options}}:{{field.isValid}}</p>
      </Attributes::GroupIterator>
    `);

    expect(this.element.querySelectorAll("p").length).to.equal(3);
    expect(this.element.querySelectorAll("p")[0]).to.have.text("item1:value1:short text::true");
    expect(this.element.querySelectorAll("p")[1]).to.have.text("item2:value2:long text::true");
    expect(this.element.querySelectorAll("p")[2]).to.have.text("item3:value3:integer::false");
  });

  it('should match icons ignoring the group trailing ws', async function () {
    this.set("attributes4", {
      "item1": "value1",
      "item2": "value2",
      "item3": "value3"
    });

    this.set("icons3", [{
      name: " TEST",
      attributes: [{
        "name": "item1",
        "type": "short text"
      }, {
        "name": "item2",
        "type": "long text",
        "options": ""
      }, {
        "name": "item3",
        "type": "integer",
        "options": ""
      }]
    }]);

    await render(hbs`
      <Attributes::GroupIterator @icons={{icons3}} @group="test " @attributes={{attributes4}} as |field|>
        <p>{{field.key}}:{{field.value}}:{{field.type}}:{{field.options}}:{{field.isValid}}</p>
      </Attributes::GroupIterator>
    `);

    expect(this.element.querySelectorAll("p").length).to.equal(3);
    expect(this.element.querySelectorAll("p")[0]).to.have.text("item1:value1:short text::true");
    expect(this.element.querySelectorAll("p")[1]).to.have.text("item2:value2:long text::true");
    expect(this.element.querySelectorAll("p")[2]).to.have.text("item3:value3:integer::false");
  });

  it('should match icons matching the group with otherNames field', async function () {
    this.set("attributes5", {
      "item1": "value1",
      "item2": "value2",
      "item3": "value3"
    });

    this.set("icons4", [{
      name: "TEST",
      otherNames: ["otherTest"],

      attributes: [{
        "name": "item1",
        "type": "short text"
      }, {
        "name": "item2",
        "type": "long text",
        "options": ""
      }, {
        "name": "item3",
        "type": "integer",
        "options": ""
      }]
    }]);

    await render(hbs`
      <Attributes::GroupIterator @icons={{icons4}} @group="otherTest" @attributes={{attributes5}} as |field|>
        <p>{{field.key}}:{{field.value}}:{{field.type}}:{{field.options}}:{{field.isValid}}</p>
      </Attributes::GroupIterator>
    `);

    expect(this.element.querySelectorAll("p").length).to.equal(3);
    expect(this.element.querySelectorAll("p")[0]).to.have.text("item1:value1:short text::true");
    expect(this.element.querySelectorAll("p")[1]).to.have.text("item2:value2:long text::true");
    expect(this.element.querySelectorAll("p")[2]).to.have.text("item3:value3:integer::false");
  });

  it('should ignore the key case and trailing ws', async function () {
    this.set("attributes6", {
      " item1": "value1",
      "item2": "value2",
      "item3": "value3"
    });

    this.set("icons6", [{
      name: "test",
      attributes: [{
        "name": "Item1",
        "type": "short text"
      }, {
        "name": "Item2 ",
        "type": "long text",
        "options": ""
      }, {
        "name": " Item3",
        "type": "integer",
        "options": ""
      }]
    }]);

    await render(hbs`
      <Attributes::GroupIterator @icons={{icons6}} @group="test" @attributes={{attributes6}} as |field|>
        <p>{{field.key}}:{{field.niceKey}}:{{field.value}}:{{field.type}}:{{field.options}}:{{field.isValid}}</p>
      </Attributes::GroupIterator>
    `);

    expect(this.element.querySelectorAll("p").length).to.equal(3);
    expect(this.element.querySelectorAll("p")[0]).to.have.text(" item1:Item1:value1:short text::true");
    expect(this.element.querySelectorAll("p")[1]).to.have.text("item2:Item2:value2:long text::true");
    expect(this.element.querySelectorAll("p")[2]).to.have.text("item3:Item3:value3:integer::false");
  });

  it('should iterate over a list of attributes', async function () {
    this.set("attributes7", [
      { key: "item1", value: "value1" },
      { key: "item2", value: "value2" },
      { key: "item3", value: "value3" }
    ]);

    this.set("icons7", [{
      name: "test",
      attributes: [{
        "name": "Item1",
        "type": "short text"
      }, {
        "name": "Item2 ",
        "type": "long text",
        "options": ""
      }, {
        "name": " Item3",
        "type": "integer",
        "options": ""
      }]
    }]);

    await render(hbs`
      <Attributes::GroupIterator @icons={{icons7}} @group="test" @attributes={{attributes7}} as |field|>
        <p>{{field.key}}:{{field.value}}:{{field.type}}:{{field.options}}:{{field.isValid}}</p>
      </Attributes::GroupIterator>
    `);

    expect(this.element.querySelectorAll("p").length).to.equal(3);
    expect(this.element.querySelectorAll("p")[0]).to.have.text("item1:value1:short text::true");
    expect(this.element.querySelectorAll("p")[1]).to.have.text("item2:value2:long text::true");
    expect(this.element.querySelectorAll("p")[2]).to.have.text("item3:value3:integer::false");
  });

  it('should iterate over the icon attributes if they are not set', async function () {
    this.set("attributes8", []);

    this.set("icons8", [{
      name: "test",
      attributes: [{
        "name": "Item1",
        "type": "short text"
      }, {
        "name": "Item2 ",
        "type": "long text",
        "options": ""
      }, {
        "name": " Item3",
        "type": "integer",
        "options": ""
      }]
    }]);

    await render(hbs`
      <Attributes::GroupIterator @icons={{icons8}} @group="test" @attributes={{attributes8}} as |field|>
        <p>{{field.key}}:{{field.niceKey}}:{{field.value}}:{{field.type}}:{{field.options}}:{{field.isValid}}</p>
      </Attributes::GroupIterator>
    `);

    expect(this.element.querySelectorAll("p").length).to.equal(3);
    expect(this.element.querySelectorAll("p")[0]).to.have.text("Item1:Item1::short text::true");
    expect(this.element.querySelectorAll("p")[1]).to.have.text("Item2 :Item2::long text::true");
    expect(this.element.querySelectorAll("p")[2]).to.have.text(" Item3:Item3::integer::true");
  });

  it('should iterate the icon attributes and then over the provided one', async function () {
    this.set("attributes9", {
      "other1": 1,
      "other2": 2
    });

    this.set("icons9", [{
      name: "test",
      attributes: [{
        "name": "Item1",
        "type": "short text"
      }, {
        "name": "Item2 ",
        "type": "long text",
        "options": ""
      }, {
        "name": " Item3",
        "type": "integer",
        "options": ""
      }]
    }]);

    await render(hbs`
      <Attributes::GroupIterator @icons={{icons9}} @group="test" @attributes={{attributes9}} as |field|>
        <p>{{field.niceKey}}:{{field.value}}:{{field.type}}:{{field.options}}:{{field.isValid}}:{{field.isIconAttribute}}</p>
      </Attributes::GroupIterator>
    `);

    expect(this.element.querySelectorAll("p").length).to.equal(5);
    expect(this.element.querySelectorAll("p")[0]).to.have.text("Item1::short text::true:true");
    expect(this.element.querySelectorAll("p")[1]).to.have.text("Item2::long text::true:true");
    expect(this.element.querySelectorAll("p")[2]).to.have.text("Item3::integer::true:true");
    expect(this.element.querySelectorAll("p")[3]).to.have.text("other1:1:short text::true:false");
    expect(this.element.querySelectorAll("p")[4]).to.have.text("other2:2:short text::true:false");
  });
});
