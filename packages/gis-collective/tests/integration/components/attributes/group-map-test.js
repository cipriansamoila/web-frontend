import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, waitUntil, click, fillIn } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/group-map', function () {
  setupRenderingTest();

  it('zooms to the extent when is set', async function () {
    this.set('position', {
      coordinates: [0, 0],
    });

    this.set('extent', {
      type: 'Polygon',
      coordinates: [
        [
          [1, 2],
          [3, 4],
          [4, 5.1],
          [4.5, 5],
          [1, 2],
        ],
      ],
    });

    await render(
      hbs`<Attributes::GroupPosition @position={{this.position}} @extent={{this.extent}}/>`
    );
    await waitUntil(() => this.element.querySelector('.map').olMap);

    const map = this.element.querySelector('.map').olMap;
    const center = map.getView().getCenter();

    expect(center).to.deep.equal([306128.5996815023, 395582.3980045552]);
  });

  it('shows the select button', async function () {
    await render(hbs`<Attributes::GroupMap />`);
    expect(this.element.querySelector('.btn-edit')).to.exist;
  });

  it('triggers a change when the edit button is pressed', async function () {
    let position;
    let details;

    this.set('change', function (p, d) {
      position = p;
      details = d;
    });

    this.set('position', { type: 'Point', coordinates: [1, 2] });

    await render(
      hbs`<Attributes::GroupMap @position={{this.position}} @onChange={{this.change}} />`
    );
    await click('.btn-edit');

    expect(position).to.deep.equal({
      type: 'Point',
      coordinates: [1, 2],
    });
    expect(details).to.deep.equal({ type: 'manual' });

    await click('.btn-dismiss');
    expect(position.toJSON()).to.deep.equal({
      type: 'Point',
      coordinates: [1, 2],
    });

    expect(details.toJSON()).to.deep.equal({
      capturedUsingGPS: false,
      altitude: 0,
      accuracy: 1000,
      altitudeAccuracy: 1000,
    });
  });

  it('allows to search for geocoding records', async function () {
    let position;
    let details;
    let term;

    this.set('change', function (p, d) {
      position = p;
      details = d;
    });

    this.set('search', function (t) {
      term = t;
    });

    this.set('position', { type: 'Point', coordinates: [1, 2] });

    await render(
      hbs`<Attributes::GroupMap @position={{this.position}} @onChange={{this.change}} @onSearch={{this.search}} />`
    );
    await click('.btn-edit');
    await fillIn('.input-search', 'term');
    await click('.btn-search');

    expect(term).to.equal('term');
    expect(position).to.deep.equal({
      type: 'Point',
      coordinates: [1, 2],
    });
    expect(details).to.deep.equal({ type: 'manual' });
  });

  it('can set the center to a result', async function () {
    let position;
    let details;
    let term;

    this.set('change', function (p, d) {
      position = p;
      details = d;
    });

    this.set('search', function (t) {
      term = t;
    });

    this.set('searchResults', [
      {
        name: 'address 2',
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [3.86, 36.03],
              [31.28, 36.03],
              [31.28, 48.92],
              [3.86, 48.92],
              [3.86, 36.03],
            ],
          ],
        },
      },
    ]);

    await render(
      hbs`<Attributes::GroupMap @searchResults={{this.searchResults}} @onChange={{this.change}} @onSearch={{this.search}} />`
    );
    await click('.btn-edit');
    await fillIn('.input-search', 'term');
    await click('.btn-search');
    await click('.list-group-item');

    expect(term).to.be.null;

    await click('.btn-submit');

    expect(position.toJSON()).to.deep.equal({
      type: 'Point',
      coordinates: [-36, 28.958943313139144],
    });
    expect(details.toJSON()).to.deep.equal({
      altitude: 0,
      accuracy: 1000,
      altitudeAccuracy: 1000,
      type: 'manual',
    });
  });
});
