import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, fillIn, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import PageElements from '../../../helpers/page-elements';

describe('Integration | Component | attributes/group-position-details', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<Attributes::GroupPositionDetails />`);

    expect(
      this.element.querySelector('.card-header').textContent.trim()
    ).to.equal('How do you want to select the location?');
  });

  it('allows to change the position type', async function () {
    let value;

    this.set('onChange', (_, v) => {
      value = v;
    });

    await render(
      hbs`<Attributes::GroupPositionDetails @onChange={{this.onChange}}/>`
    );

    await click('.value-options .btn-add');
    await click('.value-options .list-group-item');

    expect(value.type).to.equal('gps');
  });

  it('renders the type value', async function () {
    this.set('details', {
      type: 'gps',
    });

    await render(
      hbs`<Attributes::GroupPositionDetails @details={{this.details}}/>`
    );

    expect(
      this.element.querySelector('.value-options .name-list').textContent.trim()
    ).to.equal('use my current location');
  });

  describe('when there is gps type', async function () {
    let details;
    let position;

    beforeEach(async function () {
      this.set('position', { type: 'Point', coordinates: [1, 2] });

      this.set('details', {
        type: 'gps',
        altitude: 3,
        accuracy: 4,
        altitudeAccuracy: 5,
      });

      details = null;
      position = null;

      this.set('change', (p, d) => {
        details = d;
        position = p;

        this.set('position', position);
        this.set('details', details);
      });

      this.set('positionService', {
        latitude: 0,
        longitude: 0,
        accuracy: 0,
        altitude: 0,
        altitudeAccuracy: 0,
      });

      await render(
        hbs`<Attributes::GroupPositionDetails
          @positionService={{this.positionService}}
          @position={{this.position}}
          @details={{this.details}}
          @onChange={{this.change}} />`
      );

      await click('.btn-expand');
    });

    it('renders gps location attributes and values', async function () {
      const attributesLabels = this.element.querySelectorAll(
        '.attribute-value .input-group-text'
      );

      const attributesValues = this.element.querySelectorAll(
        '.attribute-value .value-read-only'
      );

      expect(attributesLabels).to.have.length(6);
      expect(attributesLabels[0].textContent.trim()).to.equal('location mode');
      expect(attributesLabels[1].textContent.trim()).to.equal('longitude');
      expect(attributesLabels[2].textContent.trim()).to.equal('latitude');
      expect(attributesLabels[3].textContent.trim()).to.equal('altitude');
      expect(attributesLabels[4].textContent.trim()).to.equal('accuracy');
      expect(attributesLabels[5].textContent.trim()).to.equal(
        'altitude accuracy'
      );

      expect(attributesValues).to.have.length(5);
      expect(attributesValues[0].textContent.trim()).to.equal('1');
      expect(attributesValues[1].textContent.trim()).to.equal('2');
      expect(attributesValues[2].textContent.trim()).to.equal('3');
      expect(attributesValues[3].textContent.trim()).to.equal('4');
      expect(attributesValues[4].textContent.trim()).to.equal('5');
    });

    it('updates the values using a timer', async function () {
      this.set('positionService.longitude', 11);
      this.set('positionService.latitude', 12);
      this.set('positionService.altitude', 13);
      this.set('positionService.accuracy', 14);
      this.set('positionService.altitudeAccuracy', 15);

      const attributesValues = this.element.querySelectorAll(
        '.attribute-value .value-read-only'
      );

      await waitUntil(() => position);

      expect(attributesValues[1].textContent.trim()).to.equal('12');
      expect(attributesValues[2].textContent.trim()).to.equal('13');
      expect(attributesValues[3].textContent.trim()).to.equal('14');
      expect(attributesValues[4].textContent.trim()).to.equal('15');
    });

    it('does not update the values using a timer after the type is switched to manual', async function () {
      await click('.value-options .btn-change');
      const options = this.element.querySelectorAll(
        '.value-options .list-group-item'
      );
      await click(options[1]);
      await click('.ember-text-field');

      this.set('positionService.longitude', 11);
      this.set('positionService.latitude', 12);
      this.set('positionService.altitude', 13);
      this.set('positionService.accuracy', 14);
      this.set('positionService.altitudeAccuracy', 15);

      const attributesValues = this.element.querySelectorAll(
        '.attribute-value .form-control'
      );

      await PageElements.wait(1200);

      expect(attributesValues[1].value).to.equal('2');
      expect(attributesValues[2].value).to.equal('3');
      expect(attributesValues[3].value).to.equal('4');
      expect(attributesValues[4].value).to.equal('5');
    });
  });

  describe('when there is manual type', async function () {
    let details;
    let position;

    beforeEach(async function () {
      this.set('position', { type: 'Point', coordinates: [1, 2] });

      this.set('details', {
        type: 'manual',
        altitude: 3,
        accuracy: 4,
        altitudeAccuracy: 5,
      });

      details = null;
      position = null;

      this.set('change', (p, d) => {
        details = d;
        position = p;
      });

      await render(
        hbs`<Attributes::GroupPositionDetails @position={{this.position}} @details={{this.details}} @onChange={{this.change}}/>`
      );
      await click('.btn-expand');
    });

    it('renders gps location attributes and values', async function () {
      const attributesLabels = this.element.querySelectorAll(
        '.attribute-value .input-group-text'
      );

      const attributesValues = this.element.querySelectorAll(
        '.attribute-value .form-control'
      );

      expect(attributesLabels).to.have.length(6);
      expect(attributesLabels[0].textContent.trim()).to.equal('location mode');
      expect(attributesLabels[1].textContent.trim()).to.equal('longitude');
      expect(attributesLabels[2].textContent.trim()).to.equal('latitude');
      expect(attributesLabels[3].textContent.trim()).to.equal('altitude');
      expect(attributesLabels[4].textContent.trim()).to.equal('accuracy');
      expect(attributesLabels[5].textContent.trim()).to.equal(
        'altitude accuracy'
      );

      expect(attributesValues).to.have.length(5);
      expect(attributesValues[0].value).to.equal('1');
      expect(attributesValues[1].value).to.equal('2');
      expect(attributesValues[2].value).to.equal('3');
      expect(attributesValues[3].value).to.equal('4');
      expect(attributesValues[4].value).to.equal('5');
    });

    it('allows to change the longitude', async function () {
      const inputs = this.element.querySelectorAll(
        '.attribute-value .form-control'
      );

      await fillIn(inputs[0], '10');
      await click(inputs[1]);

      expect(position.coordinates).to.deep.equal([10, 2]);
    });

    it('allows to change the latitude', async function () {
      const inputs = this.element.querySelectorAll(
        '.attribute-value .form-control'
      );

      await fillIn(inputs[1], '10');
      await click(inputs[0]);

      expect(position.coordinates).to.deep.equal([1, 10]);
    });

    it('allows to change the altitude', async function () {
      const inputs = this.element.querySelectorAll(
        '.attribute-value .form-control'
      );

      await fillIn(inputs[2], '10');
      await click(inputs[0]);

      expect(details).to.deep.equal({
        type: 'manual',
        altitude: 10,
        accuracy: 4,
        altitudeAccuracy: 5,
      });
    });

    it('allows to change the accuracy', async function () {
      const inputs = this.element.querySelectorAll(
        '.attribute-value .form-control'
      );

      await fillIn(inputs[3], '10');
      await click(inputs[0]);

      expect(details).to.deep.equal({
        type: 'manual',
        altitude: 3,
        accuracy: 10,
        altitudeAccuracy: 5,
      });
    });

    it('allows to change the altitudeAccuracy', async function () {
      const inputs = this.element.querySelectorAll(
        '.attribute-value .form-control'
      );

      await fillIn(inputs[4], '10');
      await click(inputs[0]);

      expect(details).to.deep.equal({
        type: 'manual',
        altitude: 3,
        accuracy: 4,
        altitudeAccuracy: 10,
      });
    });
  });

  describe('when there is address type', async function () {
    let details;
    let position;

    beforeEach(async function () {
      this.set('position', { type: 'Point', coordinates: [1, 2] });

      this.set('details', {
        type: 'address',
      });

      details = null;
      position = null;

      this.set('change', (p, d) => {
        details = d;
        position = p;
      });

      this.set('searchResults', [
        {
          name: 'address 1',
          geometry: {
            type: 'Point',
            coordinates: [13.433576, 52.495781],
          },
        },
        {
          name: 'address 2',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [3.86, 36.03],
                [31.28, 36.03],
                [31.28, 48.92],
                [3.86, 48.92],
                [3.86, 36.03],
              ],
            ],
          },
        },
      ]);
    });

    it('renders an empty search field', async function () {
      await render(
        hbs`<Attributes::GroupPositionDetails @position={{this.position}} @details={{this.details}} @onChange={{this.change}}/>`
      );

      const attributesLabels = this.element.querySelectorAll(
        '.attribute-value .input-group-text'
      );

      const attributesValues = this.element.querySelectorAll(
        '.attribute-value .form-control'
      );

      expect(attributesLabels).to.have.length(2);
      expect(attributesLabels[0].textContent.trim()).to.equal('location mode');
      expect(attributesLabels[1].textContent.trim()).to.equal(
        'search an address'
      );

      expect(attributesValues).to.have.length(1);
      expect(attributesValues[0].value).to.equal('');
    });

    it('shows the search results when they are set', async function () {
      await render(
        hbs`<Attributes::GroupPositionDetails
          @position={{this.position}}
          @details={{this.details}}
          @onChange={{this.change}}
          @searchResults={{this.searchResults}} />`
      );

      const options = this.element.querySelectorAll('.value-options');

      expect(options[1].querySelector('.btn-add').textContent.trim()).to.equal(
        'Select a value from the list'
      );
    });

    it('shows the selected search values', async function () {
      this.set('details', {
        type: 'address',
        address: 'address 1',
        searchTerm: 'address',
      });

      await render(
        hbs`<Attributes::GroupPositionDetails
          @position={{this.position}}
          @details={{this.details}}
          @onChange={{this.change}}
          @searchResults={{this.searchResults}} />`
      );

      const options = this.element.querySelectorAll('.value-options');
      expect(
        options[1].querySelector('.name-list').textContent.trim()
      ).to.equal('address 1');
      expect(
        this.element.querySelector('.attribute-value input').value
      ).to.equal('address');
    });

    it('triggers a change with the selected geometry', async function () {
      await render(
        hbs`<Attributes::GroupPositionDetails
          @position={{this.position}}
          @details={{this.details}}
          @onChange={{this.change}}
          @searchResults={{this.searchResults}} />`
      );

      await click('.btn-add');
      const options = this.element.querySelectorAll(
        '.value-options .list-group-item'
      );

      await click(options[0]);
      await click('.btn-hide');

      expect(details).to.deep.equal({
        type: 'address',
        address: 'address 1',
      });
      expect(position).to.deep.contain(this.searchResults[0].geometry);
    });

    it('converts a selected polygon to a point', async function () {
      await render(
        hbs`<Attributes::GroupPositionDetails
          @position={{this.position}}
          @details={{this.details}}
          @onChange={{this.change}}
          @searchResults={{this.searchResults}} />`
      );

      await click('.btn-add');
      const options = this.element.querySelectorAll(
        '.value-options .list-group-item'
      );

      await click(options[1]);

      expect(details).to.deep.equal({ type: 'address', address: 'address 2' });
      expect(position).to.deep.contain({
        type: 'Point',
        coordinates: [17.57, 42.475],
      });
    });

    it('triggers a change with the search term', async function () {
      await render(
        hbs`<Attributes::GroupPositionDetails
          @position={{this.position}}
          @details={{this.details}}
          @onChange={{this.change}}
          @searchResults={{this.searchResults}} />`
      );

      await fillIn('.attribute-value .form-control', 'search term');
      await click('.btn-add');

      expect(details).to.deep.equal({
        type: 'address',
        searchTerm: 'search term',
      });
      expect(position).to.deep.equal({ type: 'Point', coordinates: [1, 2] });
    });

    it('shows a loading icon while searching', async function () {
      let done;

      this.set('change', () => {
        return new Promise((resolve) => {
          done = resolve;
        });
      });

      await render(
        hbs`<Attributes::GroupPositionDetails
          @position={{this.position}}
          @details={{this.details}}
          @onChange={{this.change}}
          @searchResults={{this.searchResults}} />`
      );

      await fillIn('.attribute-value .form-control', 'search term');

      expect(this.element.querySelector('.fa-spin')).to.exist;
      done();

      await waitUntil(() => !this.element.querySelector('.fa-spin'));
    });
  });
});
