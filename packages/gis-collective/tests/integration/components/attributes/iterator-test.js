import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

describe('Integration | Component | attributes/iterator', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<Attributes::Iterator />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('should iterate through a list of keys', async function () {
    this.set('attributes1', {
      key1: {},
      key2: {},
    });

    await render(hbs`
      <Attributes::Iterator @attributes={{this.attributes1}} as |attribute|>
        <h1>{{attribute.name}}:{{attribute.stringPath}}:{{array attribute.path}}:{{attribute.type}}</h1>

        {{#each attribute.content.values as |item|}}
          <p>{{item.key}}:{{item.value}}:{{array item.path}}</p>
        {{/each}}
      </Attributes::Iterator>
    `);

    expect(this.element.querySelectorAll('h1').length).to.equal(2);
    expect(this.element.querySelectorAll('h1')[0]).to.have.text(
      'key1:key1:key1:attribute'
    );
    expect(this.element.querySelectorAll('h1')[1]).to.have.text(
      'key2:key2:key2:attribute'
    );

    expect(this.element.querySelectorAll('p').length).to.equal(0);
  });

  it('should iterate through 2 nested keys', async function () {
    this.set('attributes2', {
      key1: { key2: {} },
    });

    await render(hbs`
      <Attributes::Iterator @attributes={{this.attributes2}} as |attribute|>
        <h1>{{attribute.name}}:{{attribute.stringPath}}:{{array attribute.path}}:{{attribute.type}}</h1>
        {{#each attribute.content.values as |item|}}
          <p>{{item.key}}:{{item.value}}:{{array item.path}}</p>
        {{/each}}
      </Attributes::Iterator>
    `);

    expect(this.element.querySelectorAll('h1').length).to.equal(2);
    expect(this.element.querySelectorAll('h1')[0]).to.have.text(
      'key1:key1:key1:attribute'
    );
    expect(this.element.querySelectorAll('h1')[1]).to.have.text(
      'key2:key1.key2:key1,key2:attribute'
    );
    expect(this.element.querySelectorAll('p').length).to.equal(0);
  });

  it('should iterate through 3 nested keys', async function () {
    this.set('attributes3', {
      key1: { key2: { key3: {} } },
    });

    await render(hbs`
    <Attributes::Iterator @attributes={{this.attributes3}} as |attribute|>
    <h1>{{attribute.name}}:{{attribute.stringPath}}:{{array attribute.path}}:{{attribute.type}}</h1>

    {{#each attribute.content.values as |item|}}
    <p>{{item.key}}:{{item.value}}:{{array item.path}}</p>
    {{/each}}
    </Attributes::Iterator>
    `);

    expect(this.element.querySelectorAll('h1').length).to.equal(3);
    expect(this.element.querySelectorAll('h1')[0]).to.have.text(
      'key1:key1:key1:attribute'
    );
    expect(this.element.querySelectorAll('h1')[1]).to.have.text(
      'key2:key1.key2:key1,key2:attribute'
    );
    expect(this.element.querySelectorAll('h1')[2]).to.have.text(
      'key3:key1.key2.key3:key1,key2,key3:attribute'
    );

    expect(this.element.querySelectorAll('p').length).to.equal(0);
  });

  it('should yield non object items', async function () {
    this.set('attributes4', {
      key1: {
        item1: 'value1',
        item2: 'value2',
        item3: 'value3',
      },
    });

    await render(hbs`
      <Attributes::Iterator @attributes={{this.attributes4}} as |attribute|>
        <h1>{{attribute.name}}:{{attribute.stringPath}}:{{array attribute.path}}:{{attribute.type}}</h1>

        {{#each attribute.content.values as |item|}}
          <p>{{item.key}}:{{item.value}}:{{array item.path}}</p>
        {{/each}}
      </Attributes::Iterator>
    `);

    expect(this.element.querySelectorAll('h1').length).to.equal(1);
    expect(this.element.querySelectorAll('h1')[0]).to.have.text(
      'key1:key1:key1:attribute'
    );

    expect(this.element.querySelectorAll('p').length).to.equal(3);
    expect(this.element.querySelectorAll('p')[0]).to.have.text(
      'item1:value1:key1,item1'
    );
    expect(this.element.querySelectorAll('p')[1]).to.have.text(
      'item2:value2:key1,item2'
    );
    expect(this.element.querySelectorAll('p')[2]).to.have.text(
      'item3:value3:key1,item3'
    );
  });

  it('should yield nested non object items', async function () {
    this.set('attributes5', {
      key1: {
        key2: {
          item1: 'value1',
          item2: 'value2',
          item3: 'value3',
        },
      },
    });

    await render(hbs`
      <Attributes::Iterator @attributes={{this.attributes5}} as |attribute|>
        <h1>{{attribute.name}}:{{attribute.stringPath}}:{{array attribute.path}}:{{attribute.type}}</h1>

        {{#each attribute.content.values as |item|}}
          <p>{{item.key}}:{{item.value}}:{{array item.path}}</p>
        {{/each}}
      </Attributes::Iterator>
    `);

    expect(this.element.querySelectorAll('h1').length).to.equal(2);
    expect(this.element.querySelectorAll('h1')[0]).to.have.text(
      'key1:key1:key1:attribute'
    );
    expect(this.element.querySelectorAll('h1')[1]).to.have.text(
      'key2:key1.key2:key1,key2:attribute'
    );

    expect(this.element.querySelectorAll('p').length).to.equal(3);
    expect(this.element.querySelectorAll('p')[0]).to.have.text(
      'item1:value1:key1,key2,item1'
    );
    expect(this.element.querySelectorAll('p')[1]).to.have.text(
      'item2:value2:key1,key2,item2'
    );
    expect(this.element.querySelectorAll('p')[2]).to.have.text(
      'item3:value3:key1,key2,item3'
    );
  });

  it('should yield object root items as "other"', async function () {
    this.set('attributes6', {
      key1: {},
      key2: {},

      item1: 'value1',
      item2: 'value2',
      item3: 'value3',
    });

    await render(hbs`
      <Attributes::Iterator @attributes={{this.attributes6}} as |attribute|>
        <h1>{{attribute.name}}:{{attribute.stringPath}}:{{array attribute.path}}:{{attribute.type}}</h1>

        {{#each attribute.content.values as |item|}}
          <p>{{item.key}}:{{item.value}}:{{array item.path}}</p>
        {{/each}}
      </Attributes::Iterator>
    `);

    expect(this.element.querySelectorAll('h1').length).to.equal(3);
    expect(this.element.querySelectorAll('h1')[0]).to.have.text(
      'key1:key1:key1:attribute'
    );
    expect(this.element.querySelectorAll('h1')[1]).to.have.text(
      'key2:key2:key2:attribute'
    );
    expect(this.element.querySelectorAll('h1')[2]).to.have.text(
      'Other:::attributes'
    );

    expect(this.element.querySelectorAll('p').length).to.equal(3);
    expect(this.element.querySelectorAll('p')[0]).to.have.text(
      'item1:value1:item1'
    );
    expect(this.element.querySelectorAll('p')[1]).to.have.text(
      'item2:value2:item2'
    );
    expect(this.element.querySelectorAll('p')[2]).to.have.text(
      'item3:value3:item3'
    );
  });

  it('should iterate over arrays', async function () {
    this.set('attributes7', {
      key1: [
        {
          item1: 'value1',
          item2: 'value2',
          item3: 'value3',
        },
        {
          item4: 'value4',
          item5: 'value5',
          item6: 'value6',
        },
      ],
    });

    await render(hbs`
      <Attributes::Iterator @attributes={{this.attributes7}} as |attribute|>
        <h1>{{attribute.name}}:{{attribute.stringPath}}:{{array attribute.path}}:{{attribute.type}}</h1>

        {{#each attribute.content.values as |item|}}
          <p>{{item.key}}:{{item.value}}:{{array item.path}}</p>
        {{/each}}
      </Attributes::Iterator>
    `);

    expect(this.element.querySelectorAll('h1').length).to.equal(3);
    expect(this.element.querySelectorAll('h1')[0]).to.have.text(
      'key1:key1.0:key1,0:attribute'
    );
    expect(this.element.querySelectorAll('h1')[1]).to.have.text(
      'key1:key1.1:key1,1:attribute'
    );
    expect(this.element.querySelectorAll('h1')[2]).to.have.text(
      'key1:key1:key1:newArrayAttribute'
    );

    expect(this.element.querySelectorAll('p').length).to.equal(6);
    expect(this.element.querySelectorAll('p')[0]).to.have.text(
      'item1:value1:key1,0,item1'
    );
    expect(this.element.querySelectorAll('p')[1]).to.have.text(
      'item2:value2:key1,0,item2'
    );
    expect(this.element.querySelectorAll('p')[2]).to.have.text(
      'item3:value3:key1,0,item3'
    );

    expect(this.element.querySelectorAll('p')[3]).to.have.text(
      'item4:value4:key1,1,item4'
    );
    expect(this.element.querySelectorAll('p')[4]).to.have.text(
      'item5:value5:key1,1,item5'
    );
    expect(this.element.querySelectorAll('p')[5]).to.have.text(
      'item6:value6:key1,1,item6'
    );
  });

  it('should ignore null array values', async function () {
    this.set('attributes8', {
      key1: [
        null,
        {
          item4: 'value4',
          item5: 'value5',
          item6: 'value6',
        },
      ],
    });

    await render(hbs`
      <Attributes::Iterator @attributes={{this.attributes8}} as |attribute|>
        <h1>{{attribute.name}}:{{attribute.stringPath}}:{{array attribute.path}}:{{attribute.type}}</h1>

        {{#each attribute.content.values as |item|}}
          <p>{{item.key}}:{{item.value}}:{{array item.path}}</p>
        {{/each}}
      </Attributes::Iterator>
    `);

    expect(this.element.querySelectorAll('h1').length).to.equal(2);
    expect(this.element.querySelectorAll('h1')[0]).to.have.text(
      'key1:key1.1:key1,1:attribute'
    );
    expect(this.element.querySelectorAll('h1')[1]).to.have.text(
      'key1:key1:key1:newArrayAttribute'
    );

    expect(this.element.querySelectorAll('p').length).to.equal(3);
    expect(this.element.querySelectorAll('p')[0]).to.have.text(
      'item4:value4:key1,1,item4'
    );
    expect(this.element.querySelectorAll('p')[1]).to.have.text(
      'item5:value5:key1,1,item5'
    );
    expect(this.element.querySelectorAll('p')[2]).to.have.text(
      'item6:value6:key1,1,item6'
    );
  });

  it('should iterate over icons if the attributes are missing', async function () {
    this.set('attributes9', {});
    this.set('icons9', [
      {
        name: 'icon1',
        allowMany: false,
      },
      {
        name: 'icon2',
        allowMany: true,
      },
    ]);

    await render(hbs`
      <Attributes::Iterator @icons={{this.icons9}} @attributes={{this.attributes9}} as |attribute|>
        <h1>{{attribute.name}}:{{attribute.stringPath}}:{{array attribute.path}}:{{attribute.type}}</h1>
        {{#each attribute.content.values as |item|}}
          <p>{{item.key}}:{{item.value}}:{{array item.path}}</p>
        {{/each}}
      </Attributes::Iterator>
    `);

    expect(this.element.querySelectorAll('h1').length).to.equal(2);
    expect(this.element.querySelectorAll('h1')[0]).to.have.text(
      'icon1:icon1:icon1:newAttribute'
    );
    expect(this.element.querySelectorAll('h1')[1]).to.have.text(
      'icon2:icon2:icon2:newArrayAttribute'
    );
  });

  it('should fill the attribute values in the icon attributes', async function () {
    this.set('attributes10', {
      icon1: {
        key1: 1,
        key2: 2,
      },
      icon2: [
        {
          key3: 3,
          key4: 4,
        },
        {
          key5: 5,
          key6: 6,
        },
      ],
    });
    this.set('icons10', [
      {
        name: 'icon1',
        allowMany: false,
      },
      {
        name: 'icon2',
        allowMany: true,
      },
    ]);

    await render(hbs`
      <Attributes::Iterator @icons={{this.icons10}} @attributes={{this.attributes10}} as |attribute|>
        <h1>{{attribute.name}}:{{attribute.stringPath}}:{{array attribute.path}}:{{attribute.type}}</h1>

        {{#each attribute.content.values as |item|}}
          <p>{{item.key}}:{{item.value}}:{{array item.path}}</p>
        {{/each}}
      </Attributes::Iterator>
    `);

    expect(this.element.querySelectorAll('h1').length).to.equal(4);
    expect(this.element.querySelectorAll('h1')[0]).to.have.text(
      'icon1:icon1:icon1:attribute'
    );
    expect(this.element.querySelectorAll('h1')[1]).to.have.text(
      'icon2:icon2.0:icon2,0:attribute'
    );
    expect(this.element.querySelectorAll('h1')[2]).to.have.text(
      'icon2:icon2.1:icon2,1:attribute'
    );
    expect(this.element.querySelectorAll('h1')[3]).to.have.text(
      'icon2:icon2:icon2:newArrayAttribute'
    );

    expect(this.element.querySelectorAll('p').length).to.equal(6);
    expect(this.element.querySelectorAll('p')[0]).to.have.text(
      'key1:1:icon1,key1'
    );
    expect(this.element.querySelectorAll('p')[1]).to.have.text(
      'key2:2:icon1,key2'
    );
    expect(this.element.querySelectorAll('p')[2]).to.have.text(
      'key3:3:icon2,0,key3'
    );
    expect(this.element.querySelectorAll('p')[3]).to.have.text(
      'key4:4:icon2,0,key4'
    );
    expect(this.element.querySelectorAll('p')[4]).to.have.text(
      'key5:5:icon2,1,key5'
    );
    expect(this.element.querySelectorAll('p')[5]).to.have.text(
      'key6:6:icon2,1,key6'
    );
  });

  it('should not add null icon attributes as other', async function () {
    this.set('attributes11', {
      icon1: null,
      icon2: null,
    });

    this.set('icons11', [
      {
        name: 'icon1',
        allowMany: false,
      },
      {
        name: 'icon2',
        allowMany: true,
      },
    ]);

    await render(hbs`
      <Attributes::Iterator @icons={{this.icons11}} @attributes={{this.attributes11}} as |attribute|>
        <h1>{{attribute.name}}:{{attribute.stringPath}}:{{array attribute.path}}:{{attribute.type}}</h1>

        {{#each attribute.content.values as |item|}}
          <p>{{item.key}}:{{item.value}}:{{array item.path}}</p>
        {{/each}}
      </Attributes::Iterator>
    `);

    expect(this.element.querySelectorAll('h1').length).to.equal(2);
    expect(this.element.querySelectorAll('h1')[0]).to.have.text(
      'icon1:icon1:icon1:newAttribute'
    );
    expect(this.element.querySelectorAll('h1')[1]).to.have.text(
      'icon2:icon2:icon2:newArrayAttribute'
    );

    expect(this.element.querySelectorAll('p').length).to.equal(0);
  });
});
