import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/group-about', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Attributes::GroupAbout />`);
    expect(this.element.textContent.trim()).to.contain('about');
  });
});
