import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/group', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Attributes::Group />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('does not render the validation message when it is not present', async function() {
    this.set("group", {});

    await render(hbs`<Attributes::Group @group={{group}}/>`);

    expect(this.element.querySelector(".icon-validation-error")).not.to.exist;
  });

  it('renders the validation message when it is present', async function() {
    this.set("group", {
      validationMessage: "there is an error"
    });

    await render(hbs`<Attributes::Group @group={{group}}/>`);

    expect(this.element.querySelector(".icon-validation-error")).to.exist;
  });
});
