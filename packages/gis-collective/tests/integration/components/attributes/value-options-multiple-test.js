import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/value-options-multiple', function() {
  setupRenderingTest();

  it('renders a message without change button when no attribute is set', async function() {
    await render(hbs`<Attributes::ValueOptionsMultiple />`);

    expect(this.element.textContent.trim()).to.equal('there is nothing to choose');
    expect(this.element.querySelector(".btn")).not.to.exist;
  });

  it('renders a message without change button when the value is an empty string', async function() {
    await render(hbs`<Attributes::ValueOptionsMultiple @value={{array}}/>`);

    expect(this.element.textContent.trim()).to.equal('there is nothing to choose');
    expect(this.element.querySelector(".btn")).not.to.exist;
  });

  it('renders the string list value when is set', async function() {
    await render(hbs`<Attributes::ValueOptionsMultiple @value={{array "string1" "string2"}}/>`);

    expect(this.element.textContent.trim()).to.equal('string1, string2');
    expect(this.element.querySelector(".btn")).not.to.exist;
  });

  it('renders the name list when an object value list is set', async function() {
    this.set("value1", [{ id: 1, name:"map1" }, { id: 2, name:"map2" }]);
    await render(hbs`<Attributes::ValueOptionsMultiple @value={{this.value1}}/>`);

    expect(this.element.textContent.trim()).to.equal('map1, map2');
    expect(this.element.querySelector(".btn")).not.to.exist;
  });

  it('renders the add message when the value is not set but there are object options available', async function() {
    this.set("options1", [{ id: 1, name:"map1" }, { id: 2, name:"map2" }]);
    await render(hbs`<Attributes::ValueOptionsMultiple @options={{this.options1}} />`);

    expect(this.element.querySelector(".btn-change")).not.to.exist;
    expect(this.element.querySelector(".btn-add")).to.exist;
    expect(this.element.querySelector(".btn-add").textContent.trim()).to.equal('Select one or more values from the list');
  });

  it('renders the value and the change button when both values and options are set', async function() {
    this.set("value2", [{ id: 1, name:"map1" }, { id: 2, name:"map2" }]);
    this.set("options2", [{ id: 1, name:"map1" }, { id: 2, name:"map2" }]);
    await render(hbs`<Attributes::ValueOptionsMultiple @options={{this.options2}} @value={{this.value2}} />`);

    expect(this.element.querySelector(".name-list").textContent.trim()).to.equal('map1, map2');
    expect(this.element.querySelector(".btn-add")).not.to.exist;
    expect(this.element.querySelector(".btn-change")).to.exist;
    expect(this.element.querySelector(".btn-change").textContent.trim()).to.equal('change');
  });

  it('opens and closes the option list when btn change is clicked', async function() {
    this.set("value3", [{ id: 1, name:"map1" }, { id: 2, name:"map2" }]);
    this.set("options3", [{ id: 1, name:"map1" }, { id: 2, name:"map2" }]);

    await render(hbs`<Attributes::ValueOptionsMultiple @options={{this.options3}} @value={{this.value3}} />`);

    expect(this.element.querySelector(".list-group")).not.to.exist;
    expect(this.element.querySelector(".list-hide")).not.to.exist;

    await click(".btn-change");

    expect(this.element.querySelector(".btn-change")).not.to.exist;
    expect(this.element.querySelector(".btn-hide")).to.exist;

    expect(this.element.querySelector(".list-group")).to.exist;
    expect(this.element.querySelector(".list-group .list-group-item")).to.exist;

    await click(".btn-hide");

    expect(this.element.querySelector(".btn-change")).to.exist;
    expect(this.element.querySelector(".btn-hide")).not.to.exist;
    expect(this.element.querySelector(".list-group")).not.to.exist;
  });

  it('selects an option and puts it in a list', async function() {
    let selected;

    this.set("value4", []);
    this.set("options4", [{ id: 1, name:"map1" }, { id: 2, name:"map2" }]);
    this.set("selectedAction", (value) => {
      selected = value;
    });

    await render(hbs`<Attributes::ValueOptionsMultiple @options={{this.options4}} @value={{this.value4}} @onChange={{this.selectedAction}}/>`);

    await click(".btn-add");

    const options = this.element.querySelectorAll(".list-group-item");
    await click(options[0]);

    expect(selected).to.deep.equal([{ id: 1, name:"map1" }]);
  });

  it.skip('selects all options and puts them in a list', async function() {
    this.timeout(15000);
    let selected;

    this.set("value4", []);
    this.set("options4", [{ id: 1, name:"map1" }, { id: 2, name:"map2" }]);
    this.set("selectedAction", (value) => {
      selected = value;
      this.set("value4", value);
    });

    await render(hbs`<Attributes::ValueOptionsMultiple @options={{this.options4}} @value={{this.value4}} @onChange={{this.selectedAction}}/>`);

    await click(".btn-add");

    const _element = this.element;

    await click(".list-group-item-action.not-active");
    await click(".list-group-item-action.not-active");

    await waitUntil(async function() {
      if(_element.querySelector(".list-group-item-action.not-active")) {
        click(".list-group-item-action.not-active");
      }

      return selected.length == 2
    }, { timeout: 4000 });

    await waitUntil(() => this.element.querySelectorAll(".list-group-item.active").length == 2, { timeout: 4000 });
    const selectedOptions = this.element.querySelectorAll(".list-group-item.active");

    expect(selectedOptions.length).to.equal(2);

    expect(selected).to.deep.equal([{ id: 1, name:"map1" }, { id: 2, name:"map2" }]);
  });

  it('deselects an option and updates the value list', async function() {
    let selected;

    this.set("value5", [{ id: 1, name:"map1" }, { id: 2, name:"map2" }]);
    this.set("options5", [{ id: 1, name:"map1" }, { id: 2, name:"map2" }]);
    this.set("selectedAction5", (value) => {
      selected = value;
    });

    await render(hbs`<Attributes::ValueOptionsMultiple @options={{this.options5}} @value={{this.value5}} @onChange={{this.selectedAction5}}/>`);

    await click(".btn-change");

    const selectedOptions = this.element.querySelectorAll(".list-group-item.active");
    expect(selectedOptions.length).to.equal(2);

    await click(selectedOptions[1]);

    expect(selected).to.deep.equal([{ id: 1, name:"map1" }]);
  });


  it('deselects all options and updates the value list', async function() {
    let selected;

    this.set("value5", [{ id: 1, name:"map1" }, { id: 2, name:"map2" }]);
    this.set("options5", [{ id: 1, name:"map1" }, { id: 2, name:"map2" }]);
    this.set("selectedAction5", (value) => {
      selected = value;
      this.set("value5", value);
    });

    await render(hbs`<Attributes::ValueOptionsMultiple @options={{this.options5}} @value={{this.value5}} @onChange={{this.selectedAction5}}/>`);

    await click(".btn-change");

    let selectedOptions = this.element.querySelectorAll(".list-group-item.active");
    expect(selectedOptions.length).to.equal(2);

    await click(selectedOptions[0]);
    await waitUntil(() => selected.length == 1);


    selectedOptions = this.element.querySelectorAll(".list-group-item.active");
    await click(selectedOptions[0]);
    await waitUntil(() => selected.length == 0);

    expect(selected).to.deep.equal([]);
  });
});
