import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | attributes/value-boolean-required', function() {
  setupRenderingTest();

  it('does not render the unknown option', async function() {
    await render(hbs`<Attributes::ValueBooleanRequired />`);

    expect(this.element.querySelector(".btn-unknown")).not.to.exist;
    expect(this.element.querySelector(".btn-true")).not.to.have.class('btn-secondary');
    expect(this.element.querySelector(".btn-false")).not.to.have.class('btn-secondary');
  });
});
