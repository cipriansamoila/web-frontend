import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/member-table', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Manage::MemberTable />`);

    const colls = this.element.querySelectorAll("th");
    expect(colls[0].textContent.trim()).to.equal('Name');
    expect(colls[1].textContent.trim()).to.equal('Email');
    expect(colls[2].textContent.trim()).to.equal('Role');
    expect(colls[3].textContent.trim()).to.equal('');
  });

  describe("when there is a pending user", function() {
    beforeEach(function() {
      this.set("members", [{
        role: "owners",
        user: {
          id: "mail@asd.asd"
        }
      }]);
    });

    it("renders a row with the email", async function() {
      await render(hbs`<Manage::MemberTable @members={{this.members}}/>`);

      expect(this.element.querySelector(".full-name").textContent.trim()).to.eql("---");
      expect(this.element.querySelector(".email").textContent.trim()).to.eql("mail@asd.asd");
      expect(this.element.querySelector(".role").value).to.eql("owners");
    });
  });

  describe("when there is an user", function() {
    beforeEach(function() {
      this.set("members", [{
        role: "owners",
        user: {
          id: "1",
          email: "mail@asd.asd"
        }
      }]);
    });

    it("renders a row with the email", async function() {
      await render(hbs`<Manage::MemberTable @members={{this.members}}/>`);
      expect(this.element.querySelector(".full-name").textContent.trim()).to.eql("");
      expect(this.element.querySelector(".email").textContent.trim()).to.eql("mail@asd.asd");
      expect(this.element.querySelector(".role").value).to.eql("owners");
    });

    it("requests the profile and shows the name result", async function() {
      let id;
      this.set("requestProfile", (v) => {
        id = v;

        return {
          fullName: "full name"
        }
      })

      await render(hbs`<Manage::MemberTable @members={{this.members}} @requestProfile={{this.requestProfile}}/>`);

      expect(this.element.querySelector(".full-name").textContent.trim()).to.eql("full name");
      expect(id).to.eql("1");
    });

    it("allows deleting a record", async function() {
      let user;
      this.set("remove", (v) => {
        user = v;
      });

      await render(hbs`<Manage::MemberTable @members={{this.members}} @onRemove={{this.remove}}/>`);

      await click(".btn-danger");
      expect(user.id).to.eql("1");
    });

    it("allows updating the role", async function() {
      let user;
      let role;
      this.set("updateRole", (v, r) => {
        user = v;
        role = r;
      });

      await render(hbs`<Manage::MemberTable @members={{this.members}} @updateRole={{this.updateRole}}/>`);

      this.element.querySelector(".role").value = "guests";
      await triggerEvent('.role', 'change');

      expect(user.id).to.eql("1");
      expect(role).to.eql("guests");
    });

    it("renders the disabled fields when disabled is true", async function() {
      await render(hbs`<Manage::MemberTable @disabled={{true}} @members={{this.members}} @requestProfile={{this.requestProfile}}/>`);

      expect(this.element.querySelector(".role")).to.have.attribute("disabled", "");
      expect(this.element.querySelector(".btn-danger")).to.have.attribute("disabled", "");
    });
  });
});
