import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/toolbar/admin-all', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Manage::Toolbar::AdminAll />`);
    expect(this.element.textContent.trim()).to.equal('show all');
  });
});
