import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/toolbar/view-mode', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Manage::Toolbar::ViewMode />`);

    expect(this.element.textContent.trim()).to.equal('');

    expect(this.element.querySelector(".btn-card-columns")).not.to.exist;
    expect(this.element.querySelector(".btn-card-deck")).not.to.exist;
    expect(this.element.querySelector(".btn-list")).not.to.exist;
  });

  it('renders the card columns, card deck and list buttons', async function() {
    await render(hbs`<Manage::Toolbar::ViewMode @allowCardColumns={{true}} @allowCardDeck={{true}} @allowList={{true}} />`);

    expect(this.element.querySelector(".btn-card-columns")).to.exist;
    expect(this.element.querySelector(".btn-card-deck")).to.exist;
    expect(this.element.querySelector(".btn-list")).to.exist;
  });

  it('renders the card columns btn activated by default', async function() {
    await render(hbs`<Manage::Toolbar::ViewMode @allowCardColumns={{true}} @allowCardDeck={{true}} @allowList={{true}} />`);

    expect(this.element.querySelector(".btn-card-columns")).to.have.attribute("disabled", "");
    expect(this.element.querySelector(".btn-card-deck")).not.to.have.attribute("disabled", "");
    expect(this.element.querySelector(".btn-list")).not.to.have.attribute("disabled");
  });

  it('renders the list btn activated by default if it is set as default', async function() {
    await render(hbs`<Manage::Toolbar::ViewMode @allowCardColumns={{true}} @allowCardDeck={{true}} @allowList={{true}} @default="list"/>`);

    expect(this.element.querySelector(".btn-card-columns")).not.to.have.attribute("disabled");
    expect(this.element.querySelector(".btn-card-deck")).not.to.have.attribute("disabled");
    expect(this.element.querySelector(".btn-list")).to.have.attribute("disabled", "");
  });

  it('renders the list btn activated when the value is "list"', async function() {
    await render(hbs`<Manage::Toolbar::ViewMode @value="list" @allowCardColumns={{true}} @allowCardDeck={{true}} @allowList={{true}}  />`);

    expect(this.element.querySelector(".btn-card-columns")).not.to.have.attribute("disabled");
    expect(this.element.querySelector(".btn-card-deck")).not.to.have.attribute("disabled", "");
    expect(this.element.querySelector(".btn-list")).to.have.attribute("disabled", "");
  });

  it('renders the list btn activated when the value is "list"', async function() {
    await render(hbs`<Manage::Toolbar::ViewMode @value="card-deck" @allowCardColumns={{true}} @allowCardDeck={{true}} @allowList={{true}} />`);

    expect(this.element.querySelector(".btn-card-columns")).not.to.have.attribute("disabled");
    expect(this.element.querySelector(".btn-card-deck")).to.have.attribute("disabled", "");
    expect(this.element.querySelector(".btn-list")).not.to.have.attribute("disabled");
  });

  it('triggers the onChange with grid when the grid button is pressed', async function() {
    let value;

    this.set("changed", function(newValue) {
      value = newValue;
    });

    await render(hbs`<Manage::Toolbar::ViewMode @allowCardColumns={{true}} @allowCardDeck={{true}} @allowList={{true}}  @value="list" @onChange={{this.changed}}/>`);

    await click(".btn-card-deck");

    expect(value).to.equal("card-deck");
  });

  it('triggers the onChange with list when the list button is pressed', async function() {
    let value;

    this.set("changed", function(newValue) {
      value = newValue;
    });

    await render(hbs`<Manage::Toolbar::ViewMode @allowCardColumns={{true}} @allowCardDeck={{true}} @allowList={{true}} @value="grid" @onChange={{this.changed}}/>`);

    await click(".btn-list");

    expect(value).to.equal("list");
  });

  it('does not trigger the onChange with list when the list button is pressed and the value is list', async function() {
    let value;

    this.set("changed", function(newValue) {
      value = newValue;
    });

    await render(hbs`<Manage::Toolbar::ViewMode @allowCardColumns={{true}} @allowCardDeck={{true}} @allowList={{true}}  @value="list" @onChange={{this.changed}} />`);

    await click(".btn-list");

    expect(value).not.to.exist;
  });
});
