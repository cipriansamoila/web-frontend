import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/row-save-cancel', function() {
  setupRenderingTest();

  it('renders the save button by default', async function() {
    let isSave = false;

    this.set("save1", function() {
      isSave = true;
    })

    await render(hbs`<Manage::RowSaveCancel @onSave={{this.save1}} />`);

    expect(this.element.querySelector("button.btn-primary")).to.exist;
    expect(this.element.querySelector("button.btn-primary").textContent.trim()).to.equal("Save");
    expect(this.element.querySelector("button.btn-primary")).to.have.class("btn");
    expect(this.element.querySelector("button.btn-primary")).to.have.class("btn-submit");

    expect(this.element.querySelector("button.btn-secondary")).not.to.exist;

    await click(this.element.querySelector(".btn-primary"));
    expect(isSave).to.equal(true);
  });

  it('renders the cancel button when the action is present', async function() {
    let isCancel = false;

    this.set("cancel1", function() {
      isCancel = true;
    });

    await render(hbs`<Manage::RowSaveCancel @onCancel={{this.cancel1}}/>`);

    expect(this.element.querySelector("button.btn-primary")).to.exist;
    expect(this.element.querySelector("button.btn-primary").textContent.trim()).to.equal("Save");

    expect(this.element.querySelector("button.btn-secondary")).to.exist;
    expect(this.element.querySelector("button.btn-secondary")).to.have.class("btn");
    expect(this.element.querySelector("button.btn-secondary")).to.have.class("btn-cancel");
    expect(this.element.querySelector("button.btn-secondary").textContent.trim()).to.equal("Cancel");

    await click(this.element.querySelector(".btn-secondary"));
    expect(isCancel).to.equal(true);
  });

  it('renders the disabled save button when isDisabled is true', async function() {
    this.set("cancel2", function() { });

    await render(hbs`<Manage::RowSaveCancel @isDisabled={{true}} @onCancel={{this.cancel2}} />`);

    expect(this.element.querySelector("button.btn-primary")).to.exist;
    expect(this.element.querySelector("button.btn-primary")).to.have.class("disabled");
    expect(this.element.querySelector("button.btn-primary")).to.have.attribute("disabled", "");

    expect(this.element.querySelector("button.btn-secondary")).to.exist;
    expect(this.element.querySelector("button.btn-secondary")).to.have.class("disabled");
    expect(this.element.querySelector("button.btn-secondary")).to.have.attribute("disabled", "");
  });

  it('renders the disabled save button when the model is clean', async function() {
    this.set("model2", {
      hasDirtyAttributes: false
    });

    await render(hbs`<Manage::RowSaveCancel @model={{this.model2}} />`);

    expect(this.element.querySelector("button.btn-primary")).to.exist;
    expect(this.element.querySelector("button.btn-primary")).to.have.class("disabled");
    expect(this.element.querySelector("button.btn-primary")).to.have.attribute("disabled", "");
  });

  it('renders the enabled save button when the model is dirty', async function() {
    this.set("model3", {
      hasDirtyAttributes: true
    });

    await render(hbs`<Manage::RowSaveCancel @model={{this.model3}} />`);

    expect(this.element.querySelector("button.btn-primary")).to.exist;
    expect(this.element.querySelector("button.btn-primary")).not.to.have.attribute("disabled", "");
  });


  it('renders the enabled save button when the model is dirty and ignoreValidation is true', async function() {
    this.set("model4", {
      hasDirtyAttributes: false
    });

    await render(hbs`<Manage::RowSaveCancel @model={{this.model4}} @ignoreValidation={{true}}/>`);

    expect(this.element.querySelector("button.btn-primary")).to.exist;
    expect(this.element.querySelector("button.btn-primary")).not.to.have.attribute("disabled", "");
  });
});
