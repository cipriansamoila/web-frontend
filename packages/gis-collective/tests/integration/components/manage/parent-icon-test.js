import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/parent-icon', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Manage::ParentIcon />`);

    expect(this.element.querySelector(".container-group-small")).to.exist;
  });
});
