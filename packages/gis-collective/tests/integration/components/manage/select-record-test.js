import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/select-record', function () {
  setupRenderingTest();

  it('renders an error when the value is not set', async function () {
    await render(
      hbs`<Manage::SelectRecord @title="test" @emptyError="empty"/>`
    );

    expect(
      this.element.querySelector('.alert-info').textContent.trim()
    ).to.equal('empty');
  });

  it('renders the name when the value is set', async function () {
    this.set('value', {
      name: 'some name',
    });

    await render(
      hbs`<Manage::SelectRecord @title="test" @value={{this.value}}/>`
    );

    expect(this.element.querySelector('.alert-info')).not.to.exist;
    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'some name'
    );
  });

  it('can change the selected value', async function () {
    this.set('value', {
      id: '2',
      name: 'name2',
    });

    this.set('list', [
      {
        id: '1',
        name: 'name1',
      },
      {
        id: '2',
        name: 'name2',
      },
    ]);

    let value;
    this.set('save', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::SelectRecord
        @title="test"
        @editablePanel="test"
        @isMainValue={{true}}
        @onSave={{this.save}}
        @list={{this.list}}
        @value={{this.value}}/>`
    );

    this.element.querySelector('select').value = '1';
    await triggerEvent('select', 'change');

    await click('.btn-submit');

    expect(value.id).to.eql('1');
  });
});
