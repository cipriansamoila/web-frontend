import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../../helpers/test-server';
import { selectChoose, selectSearch } from 'ember-power-select/test-support';
import waitUntil from '@ember/test-helpers/wait-until';


describe('Integration | Component | manage/editors/user', function() {
  setupRenderingTest();

  let server;
  let user1;
  let user2;

  before(function () {
    server = new TestServer();
    user1 = server.testData.storage.addDefaultUser(false, '1');
    server.testData.storage.addDefaultProfile('1');

    user2 = server.testData.storage.addDefaultUser(false, '2');
    user2.username = "gedaiu2";
    server.testData.storage.addDefaultProfile('2');
  });

  after(function () {
    server.shutdown();
  });

  it('renders the values', async function () {
    this.set('value', user1);

    await render(hbs`<Manage::Editors::User @value={{this.value._id}} />`);

    expect(this.element.querySelector(".ember-power-select-selected-item").textContent.trim()).to.equal("gedaiu")
  });

  it('can change the values', async function () {
    this.set('value', user1);

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Manage::Editors::User @value={{this.value._id}} @onChange={{this.change}} />`);

    await selectSearch('.ember-power-select-trigger', "gedaiu");
    selectChoose('.ember-power-select-trigger', "gedaiu2");

    await waitUntil(() => value);

    expect(value).to.equal('2');
  });
});
