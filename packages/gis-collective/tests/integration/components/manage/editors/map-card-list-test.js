import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../../helpers/test-server';

describe('Integration | Component | manage/editors/mapCardList', function () {
  setupRenderingTest();
  let server;

  before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultTeam('1');
    server.testData.storage.addDefaultTeam('2');
  });

  after(function () {
    server.shutdown();
  });

  it('renders', async function () {
    this.set('value', { data: { query: { team: '' } } });

    await render(hbs`<Manage::Editors::MapCardList @value={{this.value}} />`);

    const options = this.element.querySelectorAll('.form-select option');

    expect(this.element.querySelector('.form-select').value).to.equal('all');
    expect(options).to.have.length(3);
    expect(options[0].textContent.trim()).to.equal('all teams');
    expect(options[1].textContent.trim()).to.equal('Open Green Map');
    expect(options[2].textContent.trim()).to.equal('Open Green Map');

    expect(options[0]).to.have.attribute('value', 'all');
    expect(options[1]).to.have.attribute('value', '1');
    expect(options[2]).to.have.attribute('value', '2');
  });

  it('allows changing the value', async function () {
    this.set('value', { data: { query: { team: '' } } });

    let value;
    this.set('onChange', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::Editors::MapCardList @value={{this.value}} @onChange={{this.onChange}} />`
    );

    this.element.querySelector('.form-select').value = '1';
    await triggerEvent('.form-select', 'change');

    expect(value).to.deep.equal({ query: { team: '1' }, model: 'map' });
  });

  it('allows changing to the `all` value', async function () {
    this.set('value', { data: { query: { team: '1' } } });

    let value;
    this.set('onChange', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::Editors::MapCardList @value={{this.value}} @onChange={{this.onChange}} />`
    );

    this.element.querySelector('.form-select').value = 'all';
    await triggerEvent('.form-select', 'change');

    expect(value).to.deep.equal({ query: {}, model: 'map' });
  });
});
