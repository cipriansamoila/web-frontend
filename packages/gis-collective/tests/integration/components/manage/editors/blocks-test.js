import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import {
  render,
  waitFor,
  fillIn,
  waitUntil,
  triggerEvent,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/editors/blocks', function () {
  setupRenderingTest();

  it('shows the blocks without other options', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title1',
            level: 1,
          },
        },
      ],
    });

    await render(hbs`<Manage::Editors::Blocks @value={{this.value}} />`);
    await waitFor('.editor-is-ready', { timeout: 3000 });

    expect(this.element.querySelectorAll('.header').length).to.equal(1);
    expect(this.element.querySelector('.ce-header').innerHTML.trim()).to.equal(
      'title1'
    );
  });

  it('shows the blocks and other options when they are wrapped with data', async function () {
    this.set('value', {
      data: {
        blocks: [
          {
            type: 'header',
            data: {
              text: 'title1',
              level: 1,
            },
          },
        ],
      },
    });

    await render(hbs`<Manage::Editors::Blocks @value={{this.value}} />`);
    await waitFor('.editor-is-ready', { timeout: 3000 });

    expect(this.element.querySelectorAll('.header').length).to.equal(2);

    expect(this.element.querySelector('.ce-header').innerHTML.trim()).to.equal(
      'title1'
    );
  });

  it('can change the value and alignment', async function () {
    this.set('value', {
      data: {
        alignment: 'center',
        lineHeight: 'sm',
        font: 'monospace',
        blocks: [
          {
            type: 'header',
            data: {
              text: 'title1',
              level: 1,
            },
          },
        ],
      },
    });

    let value;
    this.set('change', (newValue) => {
      value = newValue.blocks;
    });

    await render(
      hbs`<Manage::Editors::Blocks @value={{this.value}} @onChange={{this.change}} />`
    );
    await waitFor('.editor-is-ready', { timeout: 3000 });

    expect(this.element.querySelector('.article-alignment').value).to.equal(
      'center'
    );
    expect(this.element.querySelector('.article-line-height').value).to.equal(
      'sm'
    );
    expect(this.element.querySelector('.article-font').value).to.equal(
      'monospace'
    );

    await fillIn('h1', 'newValue');
    await waitUntil(() => value);

    this.element.querySelector('.article-alignment').value = 'start';
    await triggerEvent('.article-alignment', 'change');

    expect(value).to.deep.equal([
      { type: 'header', data: { level: 1, text: 'newValue' } },
    ]);
  });

  it('can change the value', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title1',
            level: 1,
          },
        },
      ],
    });

    let value;
    this.set('change', (newValue) => {
      value = newValue;

      value.time = 0;
      value.version = 0;
    });

    await render(
      hbs`<Manage::Editors::Blocks @value={{this.value}} @onChange={{this.change}} />`
    );
    await waitFor('.editor-is-ready', { timeout: 3000 });

    await fillIn('h1', 'newValue');

    await waitUntil(() => value);

    expect(value).to.deep.equal({
      time: 0,
      version: 0,
      blocks: [{ type: 'header', data: { level: 1, text: 'newValue' } }],
    });
  });
});
