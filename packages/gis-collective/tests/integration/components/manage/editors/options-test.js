import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/editors/options', function () {
  setupRenderingTest();

  it('renders a value', async function () {
    this.set('options', { values: ['value1', 'value2', 'value3'] });
    this.set('value', 'value1');

    await render(
      hbs`<Manage::Editors::Options @value={{this.value}} @options={{this.options}}/>`
    );

    expect(this.element.querySelector('select').value).to.equal('value1');
  });

  it('can change a value', async function () {
    this.set('options', { values: ['value1', 'value2', 'value3'] });
    this.set('value', 'value1');

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::Editors::Options @value={{this.value}} @options={{this.options}} @onChange={{this.change}}/>`
    );

    const select = this.element.querySelector('select');

    select.value = 'value2';
    await triggerEvent(select, 'change');

    expect(value).to.equal('value2');
  });
});
