import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import {
  render,
  typeIn,
  waitFor,
  fillIn,
  triggerEvent,
  click,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import PageElements from '../../../../helpers/page-elements';
import TestServer from '../../../../helpers/test-server';

describe('Integration | Component | manage/editors/banner', function () {
  setupRenderingTest();
  let server;
  let picture1;

  beforeEach(function () {
    server = new TestServer();
    picture1 = server.testData.storage.addDefaultPicture('1');

    server.post(`/mock-server/pictures`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: picture1,
        }),
      ];
    });
  });

  it('shows the values', async function () {
    this.set('value', {
      data: {
        heading: '1',
        headingPictureId: '2',
        subHeading: '3',
        description: '4',
        buttonLabel: '5',
        buttonLink: 'http://6.com',
        sidePictureId: '7',
        sidePictureMobile: 'before',
      },
    });

    await render(hbs`<Manage::Editors::Banner @value={{this.value}} />`);
    await waitFor('.editor-is-ready', { timeout: 3000 });

    expect(this.element.querySelector('.text-heading').value).to.equal('1');
    expect(this.element.querySelector('.text-subheading').value).to.equal('3');
    expect(
      this.element.querySelector('.ce-paragraph').textContent.trim()
    ).to.equal('4');
    expect(this.element.querySelector('.text-button-label').value).to.equal(
      '5'
    );
    expect(
      this.element.querySelector('.text-button-link .text-link').value
    ).to.equal('http://6.com');
    expect(this.element.querySelector('.side-picture-mobile').value).to.equal(
      'before'
    );
  });

  it('can change the values', async function () {
    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Manage::Editors::Banner @onChange={{this.change}} />`);
    await waitFor('.editor-is-ready', { timeout: 3000 });

    await typeIn('.text-heading', '1');
    await typeIn('.text-subheading', '2');
    await fillIn('.ce-paragraph', 'test article');
    await typeIn('.text-button-label', '3');

    await click('.chk-external-link');
    await typeIn('.text-button-link .text-link', '4');

    const blob = server.testData.create.pngBlob();
    await triggerEvent(
      ".heading-frame .input-picture [type='file']",
      'change',
      {
        files: [blob],
      }
    );

    await triggerEvent(
      ".side-picture-frame .input-picture [type='file']",
      'change',
      {
        files: [blob],
      }
    );

    this.element.querySelector('.side-picture-mobile').value = 'before';
    await triggerEvent('.side-picture-mobile', 'change');

    await PageElements.wait(1000);

    value.description.time = 0;
    value.description.version = 0;

    expect(value).to.deep.equal({
      heading: '1',
      subHeading: '2',
      description: {
        time: 0,
        blocks: [{ type: 'paragraph', data: { text: 'test article' } }],
        version: 0,
      },
      buttonLabel: '3',
      buttonLink: '4',
      sidePictureMobile: 'before',
      sidePicture: '1',
      headingPicture: '1',
    });
  });
});
