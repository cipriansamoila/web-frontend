import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/editors/feature-visibility', function () {
  setupRenderingTest();

  it('renders the value', async function () {
    await render(hbs`<Manage::Editors::FeatureVisibility @value={{1}} />`);
    expect(this.element.querySelector('.form-select').value).to.equal('1');
  });

  it('can change the value', async function () {
    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::Editors::FeatureVisibility @value={{0}} @onChange={{this.change}} />`
    );

    const select = this.element.querySelector(
      '.manage-editors-feature-visibility select'
    );

    select.value = '-1';
    await triggerEvent(select, 'change');

    expect(value).to.equal(-1);
  });
});
