import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, waitFor, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../../helpers/test-server';
import waitUntil from '@ember/test-helpers/wait-until';
import fillIn from '@ember/test-helpers/dom/fill-in';

describe('Integration | Component | manage/editors/pictures', function () {
  setupRenderingTest();

  let server;
  let picture1;
  let picture2;

  before(function () {
    server = new TestServer();

    picture1 = server.testData.create.picture('1');
    picture2 = server.testData.create.picture('2');

    server.testData.storage.addPicture(picture1);
    server.testData.storage.addPicture(picture2);

    server.post('/mock-server/pictures', (request) => {
      const pictureRequest = JSON.parse(request.requestBody);
      pictureRequest.picture['_id'] = '5cc8dc1038e882010061545a';

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(pictureRequest),
      ];
    });
  });

  after(function () {
    server.shutdown();
  });

  describe('when the value is a list', function () {
    it('renders a list of pictures', async function () {
      this.set('value', [picture1, picture2]);
      await render(hbs`<Manage::Editors::Pictures @value={{this.value}} />`);

      await waitFor('.image-list-input');

      expect(this.element.querySelector('img')).to.have.attribute(
        'src',
        'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/sm'
      );
    });

    it('can add a new picture', async function () {
      this.set('value', [picture1, picture2]);

      let value;
      this.set('change', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::Editors::Pictures @value={{this.value}} @onChange={{this.change}} />`
      );

      await waitFor('.image-list-input');

      const blob = server.testData.create.pngBlob();
      await triggerEvent(".image-list-input[type='file']", 'change', {
        files: [blob],
      });

      await waitUntil(() => value);

      expect(value).to.have.length(3);
    });

    it('does not show the style options', async function () {
      this.set('value', [picture1, picture2]);

      await render(
        hbs`<Manage::Editors::Pictures @onChange={{this.change}} />`
      );

      await waitFor('.image-list-input');

      expect(this.element.querySelector('.mobile-top-padding')).not.to.exist;
    });
  });

  describe('when the value is a page col', function () {
    it('renders a list of pictures', async function () {
      this.set('value', { data: { records: [picture1, picture2] } });
      await render(hbs`<Manage::Editors::Pictures @value={{this.value}} />`);

      await waitFor('.image-list-input');

      expect(this.element.querySelector('img')).to.have.attribute(
        'src',
        'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/sm'
      );
    });

    it('can add a new picture', async function () {
      this.set('value', { data: { records: [picture1, picture2] } });

      let value;
      this.set('change', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::Editors::Pictures @value={{this.value}} @onChange={{this.change}} />`
      );

      await waitFor('.image-list-input');

      const blob = server.testData.create.pngBlob();
      await triggerEvent(".image-list-input[type='file']", 'change', {
        files: [blob],
      });

      await waitUntil(() => value);

      expect(value.ids).to.have.length(3);
    });

    it('can change the paddings', async function () {
      this.set('value', { data: { records: [picture1, picture2] } });

      let value;
      this.set('change', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::Editors::Pictures @value={{this.value}} @onChange={{this.change}} />`
      );

      await waitFor('.image-list-input');

      this.element.querySelector('.mobile-top-padding').value = '1';
      await triggerEvent('.mobile-top-padding', 'change');

      this.element.querySelector('.tablet-start-padding').value = '2';
      await triggerEvent('.tablet-start-padding', 'change');

      this.element.querySelector('.desktop-end-padding').value = '3';
      await triggerEvent('.desktop-end-padding', 'change');

      await waitUntil(() => value);
      expect(value).to.deep.equal({
        ids: ['1', '2'],
        model: 'picture',
        options: ['pt-1', 'ps-md-2', 'pe-lg-3'],
        containerOptions: [],
        heightSm: undefined,
        heightMd: undefined,
        heightLg: undefined,
      });
    });

    it('can change the alignment', async function () {
      this.set('value', { data: { records: [picture1, picture2] } });

      let value;
      this.set('change', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::Editors::Pictures @value={{this.value}} @onChange={{this.change}} />`
      );

      await waitFor('.image-list-input');

      this.element.querySelector('.mobile-alignment').value = 'start';
      await triggerEvent('.mobile-alignment', 'change');

      this.element.querySelector('.tablet-alignment').value = 'end';
      await triggerEvent('.tablet-alignment', 'change');

      this.element.querySelector('.desktop-alignment').value = 'center';
      await triggerEvent('.desktop-alignment', 'change');

      await waitUntil(() => value);
      expect(value).to.deep.equal({
        ids: ['1', '2'],
        model: 'picture',
        options: [],
        containerOptions: ['text-start', 'text-md-end', 'text-lg-center'],
        heightSm: undefined,
        heightMd: undefined,
        heightLg: undefined,
      });
    });

    it('can change the height', async function () {
      this.set('value', {
        data: {
          records: [picture1, picture2],
          heightSm: 1,
          heightMd: 2,
          heightLg: 3,
        },
      });

      let value;
      this.set('change', (v) => {
        value = v;
      });

      await render(
        hbs`<Manage::Editors::Pictures @value={{this.value}} @onChange={{this.change}} />`
      );

      await waitFor('.image-list-input');

      expect(this.element.querySelector('.mobile-height').value).to.equal('1');
      expect(this.element.querySelector('.tablet-height').value).to.equal('2');
      expect(this.element.querySelector('.desktop-height').value).to.equal('3');

      await fillIn('.mobile-height', '100');
      await fillIn('.tablet-height', '200');
      await fillIn('.desktop-height', '300');

      await waitUntil(() => value);
      expect(value).to.deep.equal({
        ids: ['1', '2'],
        model: 'picture',
        options: [],
        containerOptions: [],
        heightSm: 100,
        heightMd: 200,
        heightLg: 300,
      });
    });
  });
});
