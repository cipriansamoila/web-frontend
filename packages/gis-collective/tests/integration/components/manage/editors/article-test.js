import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../../helpers/test-server';

describe('Integration | Component | manage/editors/article', function () {
  setupRenderingTest();

  let server;

  before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle('1');
    server.testData.storage.addDefaultArticle('2');
  });

  after(function () {
    server.shutdown();
  });

  it('renders', async function () {
    this.set('selectedArticle', { data: { id: '1' } });

    await render(
      hbs`<Manage::Editors::Article @value={{this.selectedArticle}} />`
    );

    expect(this.element.querySelector('.form-select').value).to.eql('1');
  });
});
