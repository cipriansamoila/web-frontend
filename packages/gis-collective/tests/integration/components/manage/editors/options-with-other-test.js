import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import typeIn from '@ember/test-helpers/dom/type-in';

describe('Integration | Component | manage/editors/options-with-other', function () {
  setupRenderingTest();

  it('renders an option', async function () {
    this.set('options', { values: ['value1', 'value2', 'value3'] });
    this.set('value', 'value1');

    await render(
      hbs`<Manage::Editors::OptionsWithOther @value={{this.value}} @options={{this.options}}/>`
    );

    expect(this.element.querySelector('select').value).to.equal('value1');
  });

  it('renders a custom value', async function () {
    this.set('options', { values: ['value1', 'value2', 'value3'] });
    this.set('value', 'custom value');

    await render(
      hbs`<Manage::Editors::OptionsWithOther @value={{this.value}} @options={{this.options}}/>`
    );

    expect(this.element.querySelector('select').value).to.equal('Other');
    expect(this.element.querySelector('input').value).to.equal('custom value');
  });

  it('can change the option', async function () {
    this.set('options', { values: ['value1', 'value2', 'value3'] });
    this.set('value', 'value1');

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::Editors::OptionsWithOther @value={{this.value}} @options={{this.options}} @onChange={{this.change}}/>`
    );

    const select = this.element.querySelector('select');

    select.value = 'value2';
    await triggerEvent(select, 'change');

    expect(value).to.equal('value2');
  });

  it('can change the custom value', async function () {
    this.set('options', { values: ['value1', 'value2', 'value3'] });
    this.set('value', 'value1');

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::Editors::OptionsWithOther @value={{this.value}} @options={{this.options}} @onChange={{this.change}}/>`
    );

    const select = this.element.querySelector('select');

    select.value = 'Other';
    await triggerEvent(select, 'change');
    await typeIn('input', 'this is a custom option');

    expect(value).to.equal('this is a custom option');
  });
});
