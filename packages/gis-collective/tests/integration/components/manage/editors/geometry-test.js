import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, waitUntil, typeIn, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/editors/geometry', function () {
  setupRenderingTest();

  it('renders an input geometry component', async function () {
    await render(hbs`<Manage::Editors::Geometry />`);
    expect(this.element.querySelector('.input-geometry')).to.exist;
  });

  it('triggers a change event when the geometry is updated', async function () {
    this.set('value', { type: 'Point', coordinates: [1, 2] });

    let newValue;
    this.set('change', (v) => {
      this.value = v;
      newValue = v;
    });

    await render(
      hbs`<Manage::Editors::Geometry @value={{this.value}} @onChange={{this.change}}/>`
    );
    await waitUntil(() => this.element.querySelector('.map').olMap);
    await click('.btn-paste');

    const editor = this.element.querySelector('.input-geo-json').editor;
    editor.setMode('text');

    this.element.querySelector('.jsoneditor-text').value = '';
    await typeIn(
      '.jsoneditor-text',
      `{ "type": "Point", "coordinates": [11, 12] }`
    );

    expect(newValue).to.deep.equal({ type: 'Point', coordinates: [11, 12] });
  });
});
