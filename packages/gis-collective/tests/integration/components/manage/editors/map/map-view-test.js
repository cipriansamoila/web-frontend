import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../../../helpers/test-server';

describe('Integration | Component | manage/editors/map/map-view', function () {
  setupRenderingTest();
  let server;

  before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultMap('1');
    server.testData.storage.addDefaultMap('2');
  });

  after(function () {
    server.shutdown();
  });

  it('allows selecting a new value', async function () {
    this.set('value', { data: { query: { id: '' } } });

    let value;
    this.set('onChange', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::Editors::Map::MapView @value={{this.value}} @onChange={{this.onChange}} />`
    );

    this.element.querySelector('.form-select').value = '2';
    await triggerEvent('.form-select', 'change');

    expect(value).to.deep.equal({ id: '2', model: 'map' });
  });

  it('shows the selected value', async function () {
    this.set('value', { data: { id: '2' } });

    await render(hbs`<Manage::Editors::Map::MapView @value={{this.value}} />`);

    expect(this.element.querySelector('.form-select').value).to.equal('2');
  });
});
