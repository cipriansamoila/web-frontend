import { expect } from 'chai';
import { describe, it, before, after, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../../helpers/test-server';

describe('Integration | Component | manage/editors/picture', function () {
  setupRenderingTest();

  let picture;
  let server;
  let receivedPicture;

  before(function () {
    server = new TestServer();

    receivedPicture = null;
    server.post('/mock-server/pictures', (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      receivedPicture.picture._id = '1';

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedPicture),
      ];
    });

    picture = server.testData.storage.addDefaultPicture();
  });

  beforeEach(function () {
    receivedPicture = null;
  });

  after(function () {
    server.shutdown();
  });

  it('renders the picture editor', async function () {
    this.set('value', {
      data: {
        id: picture._id,
      },
    });
    await render(hbs`<Manage::Editors::Picture @value={{this.value}}/>`);

    await waitUntil(
      () =>
        this.element
          .querySelector('.input-picture .image')
          .attributes.getNamedItem('style').value
    );

    expect(
      this.element.querySelector('.input-picture .image')
    ).to.have.attribute(
      'style',
      `background-image: url('https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/md?rnd=undefined')`
    );
  });

  it('allows to set picture', async function () {
    let value;
    this.set('onChange', (v) => {
      value = v;
    });
    await render(hbs`<Manage::Editors::Picture @onChange={{this.onChange}}/>`);

    const blob = server.testData.create.pngBlob();
    await triggerEvent("input[type='file']", 'change', { files: [blob] });

    await waitUntil(() => value);
    expect(value).to.deep.equal({ id: '1', model: 'picture', options: [] });
  });

  it('allows to set paddings', async function () {
    let value;
    this.set('onChange', (v) => {
      value = v;
    });
    await render(hbs`<Manage::Editors::Picture @onChange={{this.onChange}}/>`);

    this.element.querySelector('.mobile-top-padding').value = '1';
    await triggerEvent('.mobile-top-padding', 'change');

    this.element.querySelector('.tablet-start-padding').value = '2';
    await triggerEvent('.tablet-start-padding', 'change');

    this.element.querySelector('.desktop-end-padding').value = '3';
    await triggerEvent('.desktop-end-padding', 'change');

    await waitUntil(() => value);
    expect(value).to.deep.equal({
      id: undefined,
      model: 'picture',
      options: ['pt-1', 'ps-md-2', 'pe-lg-3'],
    });
  });
});
