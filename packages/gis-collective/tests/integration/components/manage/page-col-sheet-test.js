import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import click from '@ember/test-helpers/dom/click';

describe('Integration | Component | manage/page-col-sheet', function () {
  setupRenderingTest();

  it('renders a hidden sheet', async function () {
    await render(hbs`<Manage::PageColSheet />`);
    expect(
      this.element.querySelector('.offcanvas-body').textContent.trim()
    ).to.equal('');

    expect(this.element.querySelector('.offcanvas')).not.to.have.class('show');
  });

  it('renders a visible sheet when @show is true', async function () {
    await render(hbs`<Manage::PageColSheet @show={{true}}/>`);

    expect(this.element.querySelector('.offcanvas')).to.have.class('show');
  });

  it('allows setting the col to null', async function () {
    let type = '';
    this.set('onChange', (v, t) => {
      type = t;
    });
    await render(
      hbs`<Manage::PageColSheet @show={{true}} @allowTypeChange={{true}} @type="banner" @onChange={{this.onChange}}/>`
    );

    await click('.btn-none');
    expect(this.element.querySelector('.btn-none')).to.have.class(
      'btn-secondary'
    );

    await click('.btn-set');

    expect(type).to.be.false;
  });

  it('closes the sheet when the close button is pressed', async function () {
    await render(hbs`<Manage::PageColSheet @show={{true}}/>`);

    await click('.btn-close');

    expect(this.element.querySelector('.offcanvas')).not.to.have.class('show');
  });

  it('allows editing the blocks content', async function () {
    let value;
    this.set('onChange', (v) => {
      value = v.blocks;
    });

    await render(
      hbs`<Manage::PageColSheet @show={{true}} @type="blocks" @onChange={{this.onChange}}/>`
    );
    await waitFor('.editor-is-ready', { timeout: 5000 });

    await click('.add-paragraph-link');

    await click('.btn-set');

    expect(value).deep.equal([
      { type: 'paragraph', data: { text: 'New paragraph' } },
    ]);
    expect(this.element.querySelector('.offcanvas')).not.to.have.class('show');
  });
});
