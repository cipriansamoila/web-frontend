import { expect } from 'chai';
import { describe, it, beforeEach, afterEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';

describe('Integration | Component | manage/slide-rows', function () {
  setupRenderingTest();

  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle('1');
    server.testData.storage.addDefaultPicture('2');
    server.testData.storage.addDefaultArticle('3');
    server.testData.storage.addDefaultPicture('4');
  });

  afterEach(function () {
    server.shutdown();
  });

  it('can add a new row when there is nothing set', async function () {
    let rows;
    this.set('onSave', (value) => {
      rows = value;
    });

    await render(
      hbs`<Manage::SlideRows @title="test" @editablePanel="test" @onSave={{this.onSave}}/>`
    );

    expect(this.element.querySelectorAll('.page-row')).to.have.length(0);
    await click('.btn-add-row');
    expect(this.element.querySelectorAll('.page-row')).to.have.length(1);

    await click('.btn-submit');

    expect(rows.map((a) => a.toJSON())).to.deep.equal([
      { container: 0, col: 0, row: 0, type: 'article', data: {} },
      { container: 0, col: 1, row: 0, type: 'picture', data: {} },
    ]);
  });

  it('can add a two new rows when there is nothing set', async function () {
    let rows;
    this.set('onSave', (value) => {
      rows = value;
    });

    await render(
      hbs`<Manage::SlideRows @title="test" @editablePanel="test" @onSave={{this.onSave}}/>`
    );

    expect(this.element.querySelectorAll('.page-row')).to.have.length(0);
    await click('.btn-add-row');
    await click('.btn-add-row');
    expect(this.element.querySelectorAll('.page-row')).to.have.length(2);

    await click('.btn-submit');

    expect(rows.map((a) => a.toJSON())).to.deep.equal([
      { container: 0, col: 0, row: 0, type: 'article', data: {} },
      { container: 0, col: 1, row: 0, type: 'picture', data: {} },
      { container: 0, col: 0, row: 1, type: 'article', data: {} },
      { container: 0, col: 1, row: 1, type: 'picture', data: {} },
    ]);
  });

  it('can delete the first row when there are two', async function () {
    let rows;
    this.set('onSave', (value) => {
      rows = value;
    });
    this.set('value', [
      { col: 0, row: 0, type: 'article', data: { id: '1' } },
      { col: 1, row: 0, type: 'picture', data: { id: '2' } },
      { col: 0, row: 1, type: 'article', data: { id: '3' } },
      { col: 1, row: 1, type: 'picture', data: { id: '4' } },
    ]);

    await render(
      hbs`<Manage::SlideRows @title="test" @editablePanel="test" @value={{this.value}} @onSave={{this.onSave}}/>`
    );

    expect(this.element.querySelectorAll('.page-row')).to.have.length(2);
    await click('.btn-delete-row');
    expect(this.element.querySelectorAll('.page-row')).to.have.length(1);

    await click('.btn-submit');

    expect(rows).to.deep.equal([
      { col: 0, row: 0, type: 'article', data: { id: '3' } },
      { col: 1, row: 0, type: 'picture', data: { id: '4' } },
    ]);
  });

  it('renders two rows in the view mode', async function () {
    this.set('value', [
      { col: 0, row: 0, type: 'article', data: { id: '1' } },
      { col: 1, row: 0, type: 'picture', data: { id: '2' } },
      { col: 0, row: 1, type: 'article', data: { id: '3' } },
      { col: 1, row: 1, type: 'picture', data: { id: '4' } },
    ]);

    await render(hbs`<Manage::SlideRows @title="test" @value={{this.value}}/>`);
    expect(this.element.querySelectorAll('.page-row')).to.have.length(2);
  });
});
