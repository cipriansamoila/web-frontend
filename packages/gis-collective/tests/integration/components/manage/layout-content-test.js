import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';
import click from '@ember/test-helpers/dom/click';
import PageCol from 'ogm/transforms/page-col-list';

describe('Integration | Component | manage/layout-content', function () {
  setupRenderingTest();

  let server;

  before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle('1');
    server.testData.storage.addDefaultPicture('2');
    server.testData.storage.addDefaultArticle('3');
    server.testData.storage.addDefaultPicture('4');
  });

  after(function () {
    server.shutdown();
  });

  it('renders two rows in the view mode', async function () {
    this.set('value', [
      { col: 0, row: 0, type: 'article', data: { id: '1' } },
      { col: 1, row: 0, type: 'picture', data: { id: '2' } },
      { col: 0, row: 1, type: 'article', data: { id: '3' } },
      { col: 1, row: 1, type: 'picture', data: { id: '4' } },
    ]);

    this.set('layout', [
      {
        rows: [
          {
            cols: [{}, {}],
          },
          {
            cols: [{}, {}],
          },
        ],
      },
    ]);

    await render(
      hbs`<Manage::LayoutContent @title="test" @layout={{this.layout}} @value={{this.value}}/>`
    );

    await waitUntil(
      () =>
        this.element.querySelector('img').attributes.getNamedItem('src')?.value
    );

    expect(this.element.querySelectorAll('.row')).to.have.length(2);

    const articleContent = this.element.querySelectorAll('.article-preview');
    expect(articleContent[0].textContent).to.contain('some content');
    expect(articleContent[1].textContent).to.contain('some content');

    const pictureContent = this.element.querySelectorAll('img');
    expect(pictureContent[0]).to.have.attribute(
      'src',
      'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/sm'
    );
    expect(pictureContent[1]).to.have.attribute(
      'src',
      'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/sm'
    );
  });

  it('renders two rows in the edit mode', async function () {
    this.set('value', [
      { col: 0, row: 0, type: 'article', data: { id: '1' } },
      { col: 1, row: 0, type: 'picture', data: { id: '2' } },
      { col: 0, row: 1, type: 'article', data: { id: '3' } },
      { col: 1, row: 1, type: 'picture', data: { id: '4' } },
    ]);

    this.set('layout', [
      {
        rows: [
          {
            cols: [{}, {}],
          },
          {
            cols: [{}, {}],
          },
        ],
      },
    ]);

    await render(
      hbs`<Manage::LayoutContent @title="test" @editablePanel="test" @layout={{this.layout}} @value={{this.value}}/>`
    );

    await waitUntil(
      () =>
        this.element.querySelector('.image').attributes.getNamedItem('style')
          ?.value
    );
    expect(this.element.querySelectorAll('.row')).to.have.length(3);

    const typeSelectors = this.element.querySelectorAll('.page-col-type');
    expect(typeSelectors[0].value).to.equal('article');
    expect(typeSelectors[1].value).to.equal('picture');
    expect(typeSelectors[2].value).to.equal('article');
    expect(typeSelectors[3].value).to.equal('picture');
  });

  it('can edit the cols', async function () {
    let col1 = new PageCol();
    col1.col = 0;
    col1.row = 0;
    col1.type = 'article';
    col1.data = { id: '1' };

    let col2 = new PageCol();
    col2.col = 1;
    col2.row = 0;
    col2.type = 'article';
    col2.data = { id: '1' };

    this.set('value', [col1, col2]);

    this.set('layout', [
      {
        rows: [
          {
            cols: [{}, {}],
          },
        ],
      },
    ]);

    let value;
    this.set('save', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::LayoutContent
        @title="test"
        @editablePanel="test"
        @layout={{this.layout}}
        @value={{this.value}}
        @onSave={{this.save}} />`
    );

    let typeSelectors = this.element.querySelectorAll('.page-col-id');
    typeSelectors[0].value = '3';
    await triggerEvent(typeSelectors[0], 'change');

    typeSelectors = this.element.querySelectorAll('.page-col-id');
    typeSelectors[1].value = '1';
    await triggerEvent(typeSelectors[1], 'change');

    await click('.btn-submit');

    expect(value.map((a) => a.toJSON())).to.deep.equal([
      {
        container: 0,
        col: 0,
        row: 0,
        type: 'article',
        data: { id: '3', model: 'article' },
      },
      {
        container: 0,
        col: 1,
        row: 0,
        type: 'article',
        data: { id: '1', model: 'article' },
      },
    ]);
  });

  it('can edit the cols when the value is empty', async function () {
    this.set('value', []);

    this.set('layout', [
      {
        rows: [
          {
            cols: [{}, {}],
          },
        ],
      },
    ]);

    let value;
    this.set('save', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::LayoutContent
        @title="test"
        @editablePanel="test"
        @layout={{this.layout}}
        @value={{this.value}}
        @onSave={{this.save}} />`
    );

    let typeSelectors = this.element.querySelectorAll('.page-col-type');
    typeSelectors[0].value = 'article';
    await triggerEvent(typeSelectors[0], 'change');

    typeSelectors = this.element.querySelectorAll('.page-col-id');
    typeSelectors[0].value = '3';
    await triggerEvent(typeSelectors[0], 'change');

    typeSelectors = this.element.querySelectorAll('.page-col-type');
    typeSelectors[1].value = 'article';
    await triggerEvent(typeSelectors[1], 'change');

    typeSelectors = this.element.querySelectorAll('.page-col-id');
    typeSelectors[1].value = '1';
    await triggerEvent(typeSelectors[1], 'change');

    await click('.btn-submit');

    expect(value.map((a) => a.toJSON())).to.deep.equal([
      {
        container: 0,
        col: 0,
        row: 0,
        type: 'article',
        data: { id: '3', model: 'article' },
      },
      {
        container: 0,
        col: 1,
        row: 0,
        type: 'article',
        data: { id: '1', model: 'article' },
      },
    ]);
  });
});
