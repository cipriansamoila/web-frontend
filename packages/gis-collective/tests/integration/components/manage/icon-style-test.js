import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, fillIn, waitUntil, blur } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/icon-style', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Manage::IconStyle />`);

    expect(this.element.querySelector(".title").textContent.trim()).to.equal('styles');
    expect(this.element.querySelector(".btn-edit")).to.exist;
  });

  it('renders a message when it has the default style', async function() {
    this.set("value", {
      hasCustomStyle: false
    });

    await render(hbs`<Manage::IconStyle @value={{this.value}} />`);

    expect(this.element.querySelector(".col-site")).not.to.exist;
    expect(this.element.querySelector(".col-line")).not.to.exist;
    expect(this.element.querySelector(".col-polygon")).not.to.exist;

    expect(this.element.querySelector(".alert").textContent.trim()).to.equal("This icon is using the icon set style.");
  });

  it('renders all custom border color values in the view mode', async function() {
    this.set("value", {
      hasCustomStyle: true,
      types: {
        "polygonBorderMarker": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue1" },
        "lineMarker": { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue2" },
        "polygonMarker": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue3" },
        "polygon": { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue4" },
        "site": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue5" },
        "line": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue6" },
        "polygonBorder": { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue7" }
      }
    });

    await render(hbs`<Manage::IconStyle @value={{this.value}} />`);

    expect(this.element.querySelector(".col-site").textContent).to.contain("border color: blue5");
    expect(this.element.querySelector(".col-line-marker").textContent).to.contain("border color: blue2");
    expect(this.element.querySelector(".col-polygon-marker").textContent).to.contain("border color: blue3");
    expect(this.element.querySelector(".col-line").textContent).to.contain("border color: blue6");
    expect(this.element.querySelector(".col-polygon").textContent).to.contain("border color: blue4");
  });

  it('renders all values in the edit mode', async function() {
    this.set("value", {
      hasCustomStyle: true,
      types: {
        "polygonBorderMarker": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue1" },
        "lineMarker": { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue2" },
        "polygonMarker": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue3" },
        "polygon": { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue4" },
        "site": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue5" },
        "line": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue6" },
        "polygonBorder": { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue7" }
      }
    });

    await render(hbs`<Manage::IconStyle @value={{this.value}} @editablePanel="styles" />`);

    const colorList = this.element.querySelectorAll(".icon-style-border-color");

    expect(colorList[0].value).to.equal("blue5");
    expect(colorList[1].value).to.equal("#fff");
    expect(colorList[2].value).to.equal("blue2");
    expect(colorList[3].value).to.equal("blue6");
    expect(colorList[4].value).to.equal("#fff");
    expect(colorList[5].value).to.equal("blue3");
    expect(colorList[6].value).to.equal("blue4");
    expect(colorList[7].value).to.equal("#fff");
  });

  it('can change from the default style to the custom one', async function() {
    this.set("value", {
      hasCustomStyle: false
    });

    await render(hbs`<Manage::IconStyle @value={{this.value}} @editablePanel="styles" />`);


    expect(this.element.querySelector(".col-site")).not.to.exist;
    expect(this.element.querySelector(".col-line-marker")).not.to.exist;
    expect(this.element.querySelector(".col-polygon-border-marker")).not.to.exist;
    expect(this.element.querySelector(".col-polygon-marker")).not.to.exist;

    expect(this.element.querySelector(".col-line")).not.to.exist;
    expect(this.element.querySelector(".col-polygon")).not.to.exist;
    expect(this.element.querySelector(".col-polygon-border")).not.to.exist;

    await click(".check-has-custom-style");

    expect(this.element.querySelector(".col-site")).to.exist;

    expect(this.element.querySelector(".col-line")).to.exist;
    expect(this.element.querySelector(".col-line-marker")).to.exist;

    expect(this.element.querySelector(".col-polygon-marker")).to.exist;
    expect(this.element.querySelector(".col-polygon")).to.exist;
  });

  it('resets the value when cancel is pressed', async function() {
    this.set("value", {
      hasCustomStyle: true,
      types: {
        "polygonBorderMarker": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue1" },
        "lineMarker": { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue2" },
        "polygonMarker": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue3" },
        "polygon": { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue4" },
        "site": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue5" },
        "line": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue6" },
        "polygonBorder": { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue7" }
      }
    });

    this.set("title", "styles")
    this.set("cancel", () => {
      this.set("title", "");
    });

    this.set("edit", (v) => {
      this.set("title", v);
    });

    await render(hbs`<Manage::IconStyle @value={{this.value}} @editablePanel={{this.title}} @onEdit={{this.edit}} @onCancel={{this.cancel}} />`);

    await fillIn(".col-site .icon-style-border-color", "new color");
    await click(".icon-style-border-width");

    await click(".col-site .icon-style-border-color");
    await click(".icon-style-border-width");

    await click(".btn-cancel");
    expect(this.title).to.equal("");

    await click(".btn-edit");
    expect(this.title).to.equal("styles");

    const colorList = this.element.querySelectorAll(".icon-style-border-color");

    expect(colorList[0].value).to.equal("blue5");
    expect(colorList[1].value).to.equal("#fff");
    expect(colorList[2].value).to.equal("blue2");
    expect(colorList[3].value).to.equal("blue6");
    expect(colorList[4].value).to.equal("#fff");
    expect(colorList[5].value).to.equal("blue3");
    expect(colorList[6].value).to.equal("blue4");
  });

  it('saves the value when save is pressed', async function() {
    this.set("value", {
      hasCustomStyle: true,
      types: {
        "polygonBorderMarker": { isVisible: true, "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue1" },
        "lineMarker": { isVisible: true, "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue2" },
        "polygonMarker": { isVisible: true, "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue3" },
        "site": { isVisible: true, "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue5" },
        "polygon": { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue4" },
        "line": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue6" },
        "polygonBorder": { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue7" }
      }
    });

    this.set("title", "styles");
    this.set("save", (v) => {
      this.set("title", "");
      this.set("value", v);
    });

    await render(hbs`<Manage::IconStyle @value={{this.value}} @editablePanel={{this.title}} @onSave={{this.save}} />`);

    await fillIn(".col-site .icon-style-border-color", "new color");
    await blur('.col-site .icon-style-border-color');
    await click(".icon-style-border-width");

    await click(".btn-submit");

    await waitUntil(() => this.title == "");

    expect(this.value.hasCustomStyle).to.deep.equal(true);

    expect(this.value.types.site).to.deep.contain({
      isVisible: true,
      backgroundColor: "white",
      size: 21,
      shape: "circle",
      borderWidth: 1,
      borderColor: "new color" });
  });
});
