import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/list-table', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Manage::ListTable />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders one column with one item', async function() {
    this.set("header", "name,description");
    this.set("columns", [[{name: "name", description: "description", canEdit: true}]]);

    await render(hbs`<Manage::ListTable @isEditable={{true}} @header={{this.header}} @columns={{this.columns}} as | section |>
      {{#if section.isRow}}
      <td>{{section.item.name}}</td>
      <td>{{section.item.description}}</td>
      {{/if}}
    </Manage::ListTable>`);

    const headers = this.element.querySelectorAll("thead th");
    expect(headers.length).to.equal(4);
    expect(headers[0].textContent.trim()).to.equal("");
    expect(headers[1].textContent.trim()).to.equal("Name");
    expect(headers[2].textContent.trim()).to.equal("Description");
    expect(headers[3].textContent.trim()).to.equal("");

    const cells = this.element.querySelectorAll("tbody td");
    expect(cells.length).to.equal(4);
    expect(cells[1].textContent.trim()).to.equal("name");
    expect(cells[2].textContent.trim()).to.equal("description");

    /// the first cell should contain a checkbox
    expect(cells[0].querySelector("input[type=checkbox]")).to.exist;

    /// the last cell should contain a dropdown
    expect(cells[3].querySelector(".dropdown-toggle")).to.exist;
  });

  it('renders an enabled select all checkbox when selectAll is "enabled"', async function() {
    this.set("header", "name,description");
    this.set("columns", [[{name: "name", description: "description", canEdit: true}]]);

    await render(hbs`<Manage::ListTable @isEditable={{true}} @header={{this.header}} @columns={{this.columns}} @selectAll="enabled">
    </Manage::ListTable>`);

    expect(this.element.querySelector(".select-all input")).to.have.attribute("disabled", null);
  });

  it('renders a disabled select all checkbox when selectAll is "disabled"', async function() {
    this.set("header", "name,description");
    this.set("columns", [[{name: "name", description: "description", canEdit: true}]]);

    await render(hbs`<Manage::ListTable @isEditable={{true}} @header={{this.header}} @columns={{this.columns}} @selectAll="disabled">
    </Manage::ListTable>`);

    expect(this.element.querySelector(".select-all input")).to.have.attribute("disabled", "");
  });

  it('does not render a select all checkbox when selectAll is not set', async function() {
    this.set("header", "name,description");
    this.set("columns", [[{name: "name", description: "description", canEdit: true}]]);

    await render(hbs`<Manage::ListTable @isEditable={{true}} @header={{this.header}} @columns={{this.columns}}>
    </Manage::ListTable>`);

    expect(this.element.querySelector(".select-all input")).not.to.exist;
  });

  it('should delete an item', async function() {
    let deletedItem;
    this.set("header", "name,description");
    this.set("columns", [[{name: "name", description: "description", canEdit: true}]]);
    this.set("delete", function(item) {
      deletedItem = item;
    })

    await render(hbs`<Manage::ListTable @isEditable={{true}} @header={{this.header}} @columns={{this.columns}} @onDelete={{this.delete}} as | section |>
      {{#if section.isRow}}
      <td>{{section.item.name}}</td>
      <td>{{section.item.description}}</td>
      {{/if}}
    </Manage::ListTable>`);

    await click(".btn.dropdown-toggle");
    await click(".btn-delete");

    expect(deletedItem.name).to.deep.equal("name");
    expect(deletedItem.description).to.deep.equal("description");
  });

  it('should select an item', async function() {
    let selected;
    this.set("header", "name,description");
    this.set("columns", [[{id: 1, name: "name", description: "description", canEdit: true}]]);
    this.set("select", function(list) {
      selected = list;
    })

    await render(hbs`<Manage::ListTable @isEditable={{true}} @header={{this.header}} @columns={{this.columns}} @onSelect={{this.select}} as | section |>
      {{#if section.isRow}}
      <td>{{section.item.name}}</td>
      <td>{{section.item.description}}</td>
      {{/if}}
    </Manage::ListTable>`);

    await click("tbody .chk-select");

    expect(selected.length).to.equal(1);
    expect(selected[0]).to.equal(1);
  });

  it('should select all items when the select all checkbox is pressed', async function() {
    let selected;
    this.set("header", "name,description");
    this.set("columns", [[
      {id: 1, name: "name", description: "description", canEdit: true},
      {id: 2, name: "name", description: "description", canEdit: true} ]]);
    this.set("select", function(list) {
      selected = list;
    })

    await render(hbs`<Manage::ListTable @isEditable={{true}} @header={{this.header}} @columns={{this.columns}} @selectAll="enabled" @onSelect={{this.select}} as | section |>
      {{#if section.isRow}}
        <td>{{section.item.name}}</td>
        <td>{{section.item.description}}</td>
      {{/if}}
    </Manage::ListTable>`);

    await click("thead .chk-select");

    expect(selected).to.equal("all");
  });

  it('should select all items when selectedIds is "all"', async function() {
    this.set("header", "name,description");
    this.set("columns", [[
      {id: 1, name: "name", description: "description", canEdit: true},
      {id: 2, name: "name", description: "description", canEdit: true} ]]);
    this.set("select", function() {})

    await render(hbs`<Manage::ListTable @isEditable={{true}} @selectedIds="all" @header={{this.header}} @columns={{this.columns}} @selectAll="enabled" @onSelect={{this.select}} as | section |>
      {{#if section.isRow}}
        <td>{{section.item.name}}</td>
        <td>{{section.item.description}}</td>
      {{/if}}
    </Manage::ListTable>`);

    const allCheckboxes = this.element.querySelectorAll(".chk-select");
    expect(allCheckboxes[0].checked).to.equal(true);
    expect(allCheckboxes[1].checked).to.equal(true);
    expect(allCheckboxes[2].checked).to.equal(true);
  });

  it('should deselect a checkbox when select all is true', async function() {
    let selected;
    this.set("header", "name,description");
    this.set("columns", [[
      {id: 1, name: "name", description: "description", canEdit: true},
      {id: 2, name: "name", description: "description", canEdit: true} ]]);
    this.set("select", function(list) {
      selected = list;
    });

    await render(hbs`<Manage::ListTable @isEditable={{true}} @selectedIds="all" @header={{this.header}} @columns={{this.columns}} @selectAll="enabled" @onSelect={{this.select}} as | section |>
      {{#if section.isRow}}
        <td>{{section.item.name}}</td>
        <td>{{section.item.description}}</td>
      {{/if}}
    </Manage::ListTable>`);

    await click("thead .chk-select");
    await click("tbody .chk-select");

    expect(selected).to.deep.equal([2]);

    const allCheckboxes = this.element.querySelectorAll(".chk-select");
    expect(allCheckboxes[0].checked).to.equal(false);
    expect(allCheckboxes[1].checked).to.equal(false);
    expect(allCheckboxes[2].checked).to.equal(true);
  });

  it('should deselect all items when all ids are selected', async function() {
    let selected;
    this.set("header", "name,description");
    this.set("columns", [[
      {id: 1, name: "name", description: "description", canEdit: true},
      {id: 2, name: "name", description: "description", canEdit: true} ]]);
    this.set("select", function(list) {
      selected = list;
    })

    await render(hbs`<Manage::ListTable @selectedIds="all" @isEditable={{true}} @header={{this.header}} @columns={{this.columns}} @selectAll="enabled" @onSelect={{this.select}} as | section |>
      {{#if section.isRow}}
        <td>{{section.item.name}}</td>
        <td>{{section.item.description}}</td>
      {{/if}}
    </Manage::ListTable>`);

    await click("thead .chk-select");

    expect(selected).to.deep.equal([]);
  });

  it('should render the selected item and allow deselection', async function() {
    let selected;
    this.set("header", "name,description");
    this.set("columns", [[{id: 1, name: "name", description: "description", canEdit: true}]]);
    this.set("select", function(list) {
      selected = list;
    });

    this.set("selectedIds", [ 1 ]);

    await render(hbs`<Manage::ListTable @isEditable={{true}} @header={{this.header}} @columns={{this.columns}} @selectedIds={{this.selectedIds}} @onSelect={{this.select}} as | section |>
      {{#if section.isRow}}
      <td>{{section.item.name}}</td>
      <td>{{section.item.description}}</td>
      {{/if}}
    </Manage::ListTable>`);

    expect(this.element.querySelector("tbody .chk-select").checked).to.equal(true);
    await click("tbody .chk-select");

    expect(selected.length).to.equal(0);
  });

  it('should deselect the checkbox when the selected ids is changed to an empty list', async function() {
    let selected;
    this.set("header", "name,description");
    this.set("columns", [[{id: 1, name: "name", description: "description", canEdit: true}]]);
    this.set("select", function(list) {
      selected = list;
    });

    this.set("selectedIds", [ 1 ]);

    await render(hbs`<Manage::ListTable @isEditable={{true}} @header={{this.header}} @columns={{this.columns}} @selectedIds={{this.selectedIds}} @onSelect={{this.select}} as | section |>
      {{#if section.isRow}}
      <td>{{section.item.name}}</td>
      <td>{{section.item.description}}</td>
      {{/if}}
    </Manage::ListTable>`);

    this.set("selectedIds", [ ]);

    expect(this.element.querySelector("tbody .chk-select").checked).to.equal(false);
    await click("tbody .chk-select");

    expect(selected.length).to.equal(1);
  });
});
