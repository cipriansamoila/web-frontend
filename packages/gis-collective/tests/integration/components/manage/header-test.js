import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/header', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Manage::Header />`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector("h1")).not.to.exist;
  });

  it('renders the title if it is set', async function() {
    await render(hbs`<Manage::Header @title="title"/>`);

    expect(this.element.textContent.trim()).to.equal('title');
    expect(this.element.querySelector("h1").textContent.trim()).to.equal("title");
  });

  it('renders the body if it is set', async function() {
    await render(hbs`<Manage::Header>
      some content
    </Manage::Header>`);

    expect(this.element.textContent.trim()).to.equal('some content');
  });
});
