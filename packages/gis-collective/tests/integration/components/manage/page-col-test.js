import { expect } from 'chai';
import { describe, it, beforeEach, afterEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';
import { waitUntil, click, typeIn } from '@ember/test-helpers';

describe('Integration | Component | manage/page-col', function () {
  setupRenderingTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle('1');
    server.testData.storage.addDefaultArticle('2');
    server.testData.storage.addDefaultPicture('1');
  });

  afterEach(function () {
    server.shutdown();
  });

  it('renders an article column', async function () {
    this.set('value', {
      type: 'article',
      data: {
        id: '1',
      },
    });

    this.set('allArticles', [
      {
        id: '1',
        title: 'title1',
      },
      {
        id: '2',
        title: 'title2',
      },
    ]);

    await render(
      hbs`<Manage::PageCol @value={{this.value}} @allArticles={{this.allArticles}}/>`
    );

    expect(this.element.querySelector('.page-col-type').value).to.equal(
      'article'
    );
    expect(this.element.querySelector('.page-col-id').value).to.equal('1');
  });

  it('renders a picture column', async function () {
    this.set('value', {
      type: 'picture',
      data: {
        id: '1',
        model: 'picture',
      },
    });

    await render(hbs`<Manage::PageCol @value={{this.value}} />`);
    await waitUntil(
      () =>
        this.element.querySelector('.image').attributes.getNamedItem('style')
          .value != ''
    );

    expect(this.element.querySelector('.page-col-type').value).to.equal(
      'picture'
    );
    expect(this.element.querySelector('.input-picture')).to.exist;
    expect(this.element.querySelector('.page-col-id')).not.to.exist;
    expect(this.element.querySelector('.image')).to.have.attribute(
      'style',
      "background-image: url('https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/md?rnd=undefined')"
    );
  });

  it('can change the selected article', async function () {
    this.set('value', {
      type: 'article',
      data: {
        id: '1',
        model: 'article',
      },
    });

    this.set('allArticles', [
      {
        id: '1',
        title: 'title1',
      },
      {
        id: '2',
        title: 'title2',
      },
    ]);

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::PageCol @value={{this.value}} @allArticles={{this.allArticles}} @onChange={{this.change}}/>`
    );

    const select = this.element.querySelector('.page-col-id');
    select.value = '2';
    await triggerEvent(select, 'change');

    expect(this.element.querySelector('.page-col-type').value).to.equal(
      'article'
    );
    expect(this.element.querySelector('.page-col-id').value).to.equal('2');
    expect(value).to.deep.contain({
      col: 0,
      row: 0,
      type: 'article',
      data: {
        id: '2',
        model: 'article',
      },
    });
  });

  it('can change the editor type', async function () {
    this.set('value', {
      type: 'article',
      data: {
        id: '1',
      },
    });

    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(
      hbs`<Manage::PageCol @value={{this.value}} @onChange={{this.change}}/>`
    );

    const select = this.element.querySelector('.page-col-type');
    select.value = 'picture';
    await triggerEvent(select, 'change');

    expect(this.element.querySelector('.page-col-type').value).to.equal(
      'picture'
    );
    expect(this.element.querySelector('.page-col-id')).not.to.exist;
    expect(value).to.deep.contain({
      col: 0,
      row: 0,
      type: 'picture',
      data: {},
    });
  });

  it('can edit a banner column', async function () {
    let value;
    this.set('change', (v) => {
      value = v;
    });

    this.set('value', {
      type: 'banner',
      data: {},
    });

    await render(
      hbs`<Manage::PageCol @value={{this.value}} @onChange={{this.change}}/>`
    );

    await typeIn('.text-heading', '1');
    await typeIn('.text-subheading', '2');
    await typeIn('.text-button-label', '3');

    await click('.chk-external-link');
    await typeIn('.text-button-link .text-link', '4');

    expect(value).to.deep.contain({
      col: 0,
      row: 0,
      type: 'banner',
      data: {
        heading: '1',
        subHeading: '2',
        description: { blocks: [] },
        buttonLabel: '3',
        buttonLink: '4',
        sidePictureMobile: undefined,
        headingPicture: undefined,
        sidePicture: undefined,
      },
    });
  });
});
