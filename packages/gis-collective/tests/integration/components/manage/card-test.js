import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/card', function() {
  setupRenderingTest();

  it('renders a private card with no options', async function() {
    await render(hbs`<Manage::Card @title="title" @isPublic={{false}} />`);

    expect(this.element.querySelector(".title").textContent.trim()).to.equal('title');
    expect(this.element.querySelector(".btn")).not.to.exist;
    expect(this.element.querySelector(".badge").textContent.trim()).to.equal('private');
    expect(this.element.querySelector(".btn-extend")).not.to.exist;
  });

  it('renders a public card with no options', async function() {
    await render(hbs`<Manage::Card @title="title" @isPublic={{true}} />`);

    expect(this.element.querySelector(".title").textContent.trim()).to.equal('title');
    expect(this.element.querySelector(".badge").textContent.trim()).to.equal('public');
    expect(this.element.querySelector(".btn-extend")).not.to.exist;
  });

  it('renders the options', async function() {
    await render(hbs`<Manage::Card @title="title" @hasOptions={{true}} as | type |>
      {{#if (eq type "options")}}
        <div class="option-list"></div>
      {{/if}}
    </Manage::Card>`);

    expect(this.element.querySelector(".label")).not.to.exist;
    expect(this.element.querySelector(".options")).to.exist;
    expect(this.element.querySelector(".option-list")).to.exist;
  });

  it('renders the label when is set', async function() {
    await render(hbs`<Manage::Card @title="title" @label="label" as | type |>
      {{#if (eq type "options")}}
        <div class="option-list"></div>
      {{/if}}
    </Manage::Card>`);

    expect(this.element.querySelector(".label").textContent.trim()).to.equal("label");
  });

  it("triggers the on click event when the title is pressed", async function() {
    let pressed;
    this.set("onClick", () => {
      pressed = true;
    });

    await render(hbs`<Manage::Card @title="title" @onClick={{this.onClick}} />`);

    await click(".title");
    expect(pressed).to.be.true;
  });
});
