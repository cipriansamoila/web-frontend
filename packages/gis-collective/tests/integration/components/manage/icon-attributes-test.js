import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, triggerEvent, fillIn } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { move } from 'ember-drag-sort/utils/trigger';

describe('Integration | Component | manage/icon-attributes', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Manage::IconAttributes />`);

    expect(this.element.querySelector(".title").textContent.trim()).to.equal('attributes');
    expect(this.element.querySelector(".btn-edit")).to.exist;
  });

  it('triggers the edit action', async function() {
    let label;

    this.set("edit", function(value) {
      label = value;
    });

    await render(hbs`<Manage::IconAttributes @onEdit={{this.edit}}/>`);

    await click(this.element.querySelector(".btn-edit"));

    expect(label).to.equal('attributes');
  });

  it('triggers the cancel action', async function() {
    let isCancel;

    this.set("cancel", function() {
      isCancel = true;
    });

    await render(hbs`<Manage::IconAttributes @editablePanel="attributes" @onCancel={{this.cancel}}/>`);

    await click(this.element.querySelector(".btn-cancel"));

    expect(isCancel).to.equal(true);
  });

  it('triggers the save action', async function() {
    let value;

    this.set("save", function(v) {
      value = v;
    });

    await render(hbs`<Manage::IconAttributes @editablePanel="attributes" @onSave={{this.save}}/>`);

    await click(this.element.querySelector(".btn-submit"));

    expect(value).to.deep.equal([]);
  });

  it('does not add an attribute if cancel is triggered', async function() {
    this.set("value", [
      {name: "attribute1", type: "short text"},
      {name: "attribute2", type: "long text"}
    ]);

    this.set("panel", "attributes");

    this.set("cancel", () => {
      this.set("panel", "");
    });

    await render(hbs`<Manage::IconAttributes @value={{this.value}} @editablePanel={{this.panel}} @onCancel={{this.cancel}}/>`);

    await click(this.element.querySelector(".btn-add-attribute"));
    await click(this.element.querySelector(".btn-cancel"));

    expect(this.value).to.deep.equal([
      { name: "attribute1", type: "short text" },
      { name: "attribute2", type: "long text" }
    ]);
  });

  it('adds an attribute if save is triggered', async function() {
    this.set("value", [
      {name: "attribute1", type: "short text", from: {}},
      {name: "attribute2", type: "long text", from: {}}
    ]);

    this.set("panel", "");

    this.set("save", (v) => {
      this.set("value", v);
    });

    this.set("edit", () => {
      this.set("panel", "attributes");
    });

    await render(hbs`<Manage::IconAttributes @value={{this.value}} @editablePanel={{this.panel}} @onSave={{this.save}} @onEdit={{this.edit}}/>`);

    await click(this.element.querySelector(".btn-edit"));
    await click(this.element.querySelector(".btn-add-attribute"));
    await click(this.element.querySelector(".btn-submit"));

    expect(this.value).to.deep.equal([
      { name: "attribute1", displayName: '', help: "", type: "short text", options: '', isPrivate: false, isRequired: false, isInherited: false, from: {} },
      { name: "attribute2", displayName: '', help: "",type: "long text", options: '', isPrivate: false, isRequired: false, isInherited: false, from: {} },
      { name: "new attribute", displayName: '', help: "", type: "short text", options: '', isPrivate: false, isRequired: false, isInherited: false, from: {} }
    ]);
  });

  it('renders 2 attributes in view mode', async function() {
    this.set("attributes", [
      {name: "attribute1", type: "short text"},
      {name: "attribute2", type: "long text"}
    ]);

    await render(hbs`<Manage::IconAttributes @value={{this.attributes}}/>`);


    const names = this.element.querySelectorAll(".input-icon-attribute .attribute-name");
    const types = this.element.querySelectorAll(".input-icon-attribute .attribute-type");

    expect(names.length).to.equal(2);
    expect(names[0].textContent.trim()).to.equal("attribute1");
    expect(names[1].textContent.trim()).to.equal("attribute2");

    expect(types.length).to.equal(2);
    expect(types[0].textContent.trim()).to.equal("short text");
    expect(types[1].textContent.trim()).to.equal("long text");
  });

  it('renders 2 attributes in edit mode', async function() {
    this.set("attributes", [
      {name: "attribute1", type: "short text"},
      {name: "attribute2", type: "long text"}
    ]);

    this.set("panel", "");

    this.set("edit", (v) => {
      this.set("panel", v);
    });

    await render(hbs`<Manage::IconAttributes @value={{this.attributes}} @onEdit={{this.edit}} @editablePanel={{this.panel}}/>`);

    await click(".btn-edit");

    const names = this.element.querySelectorAll(".input-attribute-name");
    const types = this.element.querySelectorAll(".input-icon-attribute select");

    expect(names.length).to.equal(2);
    expect(names[0].value).to.equal("attribute1");
    expect(names[1].value).to.equal("attribute2");

    expect(types.length).to.equal(2);
    expect(types[0].value).to.equal("short text");
    expect(types[1].value).to.equal("long text");
  });

  it('updates an attribute if save is triggered', async function() {
    this.set("value", [
      {name: "attribute1", displayName: "", help: "", type: "short text"}
    ]);

    this.set("panel", "");

    this.set("save", (v) => {
      this.set("value", v);
    });

    this.set("edit", () => {
      this.set("panel", "attributes");
    });

    await render(hbs`<Manage::IconAttributes @value={{this.value}} @editablePanel={{this.panel}} @onSave={{this.save}} @onEdit={{this.edit}}/>`);

    await click(this.element.querySelector(".btn-edit"));

    await fillIn(".input-attribute-name", "new name");
    await fillIn(".input-attribute-display-name", "new display name");
    await fillIn(".input-attribute-help", "help message");

    this.element.querySelector(".input-icon-attribute select").value = "options";

    await triggerEvent(".input-icon-attribute select", "change");
    await fillIn(".input-attribute-options", "option1,option2");

    await click(this.element.querySelector(".input-is-attribute-private"));

    await click(this.element.querySelector(".btn-submit"));

    expect(this.value).to.deep.equal([
      { name: "new name", displayName: 'new display name', help: "help message", type: "options", options: 'option1,option2', isPrivate: true, isRequired: false, isInherited: false, from: {} }
    ]);
  });

  it('deletes an attribute if delete is triggered', async function() {
    this.set("value", [
      {name: "attribute1", displayName: "", help: "", type: "short text"}
    ]);

    this.set("panel", "");

    this.set("save", (v) => {
      this.set("value", v);
    });

    this.set("edit", () => {
      this.set("panel", "attributes");
    });

    await render(hbs`<Manage::IconAttributes @value={{this.value}} @editablePanel={{this.panel}} @onSave={{this.save}} @onEdit={{this.edit}}/>`);

    await click(this.element.querySelector(".btn-edit"));
    await click(this.element.querySelector(".btn-delete"));
    await click(this.element.querySelector(".btn-submit"));

    expect(this.value).to.deep.equal([]);
  });

  it('can sort 2 attributes', async function() {
    this.set("attributes", [
      {name: "attribute1", type: "short text"},
      {name: "attribute2", type: "long text"}
    ]);

    this.set("panel", "");

    this.set("edit", (v) => {
      this.set("panel", v);
    });

    this.set("save", (v) => {
      this.set("value", v);
    });

    await render(hbs`<Manage::IconAttributes @onSave={{this.save}} @value={{this.attributes}} @onEdit={{this.edit}} @editablePanel={{this.panel}}/>`);
    await click(".btn-edit");

    const list = document.querySelector('.dragSortList')
    await move(list, 0, list, 1, false, ".handler");

    await click(this.element.querySelector(".btn-submit"));
    expect(this.value).to.deep.equal([
      { name: 'attribute2',
        displayName: '',
        help: '',
        type: 'long text',
        options: '',
        isPrivate: false,
        isInherited: false,
        isRequired: false,
        from: {} },
      { name: 'attribute1',
        displayName: '',
        help: '',
        type: 'short text',
        options: '',
        isPrivate: false,
        isInherited: false,
        isRequired: false,
        from: {} }
    ]);
  });

  it('renders the isPrivate toggle', async function() {
    this.set("attributes", [
      {name: "attribute1", type: "short text"},
      {name: "attribute2", type: "long text"}
    ]);

    this.set("panel", "");

    this.set("edit", (v) => {
      this.set("panel", v);
    });

    await render(hbs`<Manage::IconAttributes @value={{this.attributes}} @onEdit={{this.edit}} @editablePanel={{this.panel}}/>`);

    await click(".btn-edit");

    const isPrivateToggles = this.element.querySelectorAll(".input-is-attribute-private");
    expect(isPrivateToggles.length).to.equal(2);
  });

  it('displays the attribute as private if isPrivate is true', async function() {
    this.set("attributes", [
      {name: "attribute1", type: "short text", isPrivate: true},
      {name: "attribute2", type: "long text"}
    ]);

    this.set("panel", "");

    this.set("edit", (v) => {
      this.set("panel", v);
    });

    await render(hbs`<Manage::IconAttributes @value={{this.attributes}} @onEdit={{this.edit}} @editablePanel={{this.panel}}/>`);

    await click(".btn-edit");
    const isPrivateToggles = this.element.querySelectorAll(".input-is-attribute-private");

    expect(isPrivateToggles[0].checked).to.equal(true);
  });

  it("displays disabled fields for inherited attributes", async function() {
    this.set("attributes", [
      {name: "attribute1", type: "short text", isPrivate: true, isInherited: true},
      {name: "attribute2", type: "long text"}
    ]);

    this.set("icon", {
      id: "icon-id"
    });

    this.set("panel", "");

    this.set("edit", (v) => {
      this.set("panel", v);
    });

    await render(hbs`<Manage::IconAttributes @icon={{this.icon}} @value={{this.attributes}} @onEdit={{this.edit}} @editablePanel={{this.panel}}/>`);

    expect(this.element.querySelectorAll(".icon-inherited")).to.have.length(1);
    await click(".btn-edit");

    const nameInputs = this.element.querySelectorAll(".input-attribute-name");
    expect(nameInputs[0]).to.have.attribute("disabled", "");
    expect(nameInputs[1]).to.have.attribute("disabled", null);

    const deleteBtns = this.element.querySelectorAll(".btn-delete");
    expect(deleteBtns[0]).to.have.attribute("disabled", "");
    expect(deleteBtns[1]).to.have.attribute("disabled", null);
  });
});
