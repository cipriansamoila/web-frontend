import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/actions-selected', function() {
  setupRenderingTest();

  it('renders "nothing selected" when the list of selected items is empty', async function() {
    this.set("selected", []);

    await render(hbs`<Manage::ActionsSelected @selected={{this.selected}}/>`);
    expect(this.element.querySelector(".count-message").textContent.trim()).to.equal('nothing selected');
  });

  it('renders "one selected item" when the list of selected items has one element', async function() {
    this.set("selected", [""]);

    await render(hbs`<Manage::ActionsSelected @selected={{this.selected}}/>`);
    expect(this.element.querySelector(".count-message").textContent.trim()).to.equal('one selected item');
  });

  it('renders "2 selected items" when the list of selected items has two elements', async function() {
    this.set("selected", ["", ""]);

    await render(hbs`<Manage::ActionsSelected @selected={{this.selected}}/>`);
    expect(this.element.querySelector(".count-message").textContent.trim()).to.equal('2 selected items');
  });

  it('renders "all items are selected" when the list of selected items is "all"', async function() {
    this.set("selected", "all");

    await render(hbs`<Manage::ActionsSelected @selected={{this.selected}}/>`);
    expect(this.element.querySelector(".count-message").textContent.trim()).to.equal('all items are selected');
  });

  it('should show the delete button when the event is set', async function() {
    let list = [];

    this.set("selected", ["1", "2"]);
    this.set("delete", function(items) {
      list = items.slice();
    });

    await render(hbs`<Manage::ActionsSelected @selected={{this.selected}} @onDelete={{this.delete}}/>`);
    expect(this.element.querySelector(".btn-danger.btn-delete")).to.exist;

    await click(".btn-delete");

    expect(list).to.deep.equal(["1", "2"]);
  });

  it('should show a spinner while waiting to delete', async function() {
    this.set("selected", ["1", "2"]);
    this.set("delete", function() {
      return { then() {}, catch() {} }
    });

    await render(hbs`<Manage::ActionsSelected @selected={{this.selected}} @onDelete={{this.delete}}/>`);
    expect(this.element.querySelector(".btn-danger.btn-delete")).to.exist;

    await click(".btn-delete");

    expect(this.element.querySelector(".btn-delete")).to.have.attribute("disabled", "");
    expect(this.element.querySelector(".btn-delete .fa-spinner")).to.exist;
    expect(this.element.querySelector(".btn-delete .fa-trash")).not.to.exist;
  });

  it('should hide spinner on delete success', async function() {
    this.set("selected", ["1", "2"]);
    this.set("delete", function() {
      return { then(a) { a(); } }
    });

    await render(hbs`<Manage::ActionsSelected @selected={{this.selected}} @onDelete={{this.delete}}/>`);
    expect(this.element.querySelector(".btn-danger.btn-delete")).to.exist;

    await click(".btn-delete");

    expect(this.element.querySelector(".btn-delete")).not.to.have.attribute("disabled", "");
    expect(this.element.querySelector(".btn-delete .fa-spinner")).not.to.exist;
    expect(this.element.querySelector(".btn-delete .fa-trash")).to.exist;
  });

  it('should call onClearSelection on delete success', async function() {
    this.set("selected", ["1", "2"]);
    this.set("delete", function() {
      return { then(a) { a(); } }
    });

    let cleared;
    this.set("clearSelection", function() {
      cleared = true;
    });

    await render(hbs`<Manage::ActionsSelected @selected={{this.selected}} @onClearSelection={{this.clearSelection}} @onDelete={{this.delete}}/>`);
    expect(this.element.querySelector(".btn-danger.btn-delete")).to.exist;

    await click(".btn-delete");
    expect(cleared).to.equal(true);
  });

  it('should call onClearSelection when delete does not return a promise', async function() {
    this.set("selected", ["1", "2"]);
    this.set("delete", function() { });

    let cleared;
    this.set("clearSelection", function() {
      cleared = true;
    });

    await render(hbs`<Manage::ActionsSelected @selected={{this.selected}} @onClearSelection={{this.clearSelection}} @onDelete={{this.delete}}/>`);
    expect(this.element.querySelector(".btn-danger.btn-delete")).to.exist;

    await click(".btn-delete");
    expect(cleared).to.equal(true);
  });

  it('should call onClearSelection when the user clicks the clear button', async function() {
    this.set("selected", ["1", "2"]);

    let cleared;
    this.set("clearSelection", function() {
      cleared = true;
    });

    await render(hbs`<Manage::ActionsSelected @selected={{this.selected}} @onClearSelection={{this.clearSelection}}/>`);

    await click(".btn-clear");
    expect(cleared).to.equal(true);
  });

  it('should not render the clear button when the selected list is empty', async function() {
    this.set("selected", []);
    this.set("clearSelection", function() { });

    await render(hbs`<Manage::ActionsSelected @selected={{this.selected}} @onClearSelection={{this.clearSelection}}/>`);
    expect(this.element.querySelector(".btn-clear")).not.to.exist;
  });

  it('should not render the clear button when the action is not set', async function() {
    this.set("selected", ["1", "2"]);

    await render(hbs`<Manage::ActionsSelected @selected={{this.selected}} @onClearSelection={{this.clearSelection}}/>`);
    expect(this.element.querySelector(".btn-clear")).not.to.exist;
  });

  it('should hide spinner on delete failure', async function() {
    this.set("selected", ["1", "2"]);
    this.set("delete", function() {
      return { then(a, b) { b(); } }
    });

    await render(hbs`<Manage::ActionsSelected @selected={{this.selected}} @onDelete={{this.delete}}/>`);
    expect(this.element.querySelector(".btn-danger.btn-delete")).to.exist;

    await click(".btn-delete");

    expect(this.element.querySelector(".btn-delete")).not.to.have.attribute("disabled", "");
    expect(this.element.querySelector(".btn-delete .fa-spinner")).not.to.exist;
    expect(this.element.querySelector(".btn-delete .fa-trash")).to.exist;
  });

  describe('when actions are defined', function() {

    this.beforeEach(function() {
      this.set("selected", ["1", "2"]);
      this.set("actions", [{
        name: "action1",
        key:"action-1",
        icon: "map",
        class: "btn-success"
      },{
        name: "action2",
        key:"action-2",
        icon: "user",
        class: "btn-warning"
      }]);
    });

    it('should render the custom actions', async function() {
      await render(hbs`<Manage::ActionsSelected @actions={{this.actions}} @selected={{this.selected}} @onDelete={{this.delete}}/>`);

      expect(this.element.querySelector(".btn-success.btn-action-1")).to.exist;
      expect(this.element.querySelector(".btn-success.btn-action-1").textContent.trim()).to.equal("action1");
      expect(this.element.querySelector(".btn-success.btn-action-1 .fa-map")).to.exist;

      expect(this.element.querySelector(".btn-warning.btn-action-2")).to.exist;
      expect(this.element.querySelector(".btn-warning.btn-action-2").textContent.trim()).to.equal("action2");
      expect(this.element.querySelector(".btn-warning.btn-action-2 .fa-user")).to.exist;
    });

    it('should trigger action 1', async function() {
      let action;
      let items;

      this.set("onAction", function(a, b) {
        action = a;
        items = b;
      });

      await render(hbs`<Manage::ActionsSelected @actions={{this.actions}} @onAction={{this.onAction}} @selected={{this.selected}} @onDelete={{this.delete}}/>`);
      await click(this.element.querySelector(".btn-action-1"));

      expect(action).to.equal("action-1");
      expect(items).to.deep.equal(["1", "2"]);
    });

    it('should trigger action 2', async function() {
      let action;
      let items;

      this.set("onAction", function(a, b) {
        action = a;
        items = b;
      });

      await render(hbs`<Manage::ActionsSelected @actions={{this.actions}} @onAction={{this.onAction}} @selected={{this.selected}} @onDelete={{this.delete}}/>`);
      await click(this.element.querySelector(".btn-action-2"));

      expect(action).to.equal("action-2");
      expect(items).to.deep.equal(["1", "2"]);
    });

    it('should show spinner while waiting the action', async function() {
      this.set("onAction", function() {
        return { then() {} }
      });

      await render(hbs`<Manage::ActionsSelected @actions={{this.actions}} @onAction={{this.onAction}} @selected={{this.selected}} @onDelete={{this.delete}}/>`);
      await click(this.element.querySelector(".btn-action-1"));

      expect(this.element.querySelector(".btn-action-1")).to.have.attribute("disabled", "");
      expect(this.element.querySelector(".btn-action-1 .fa-spinner")).to.exist;
      expect(this.element.querySelector(".btn-action-1 .fa-map")).not.to.exist;

      expect(this.element.querySelector(".btn-action-2")).to.have.attribute("disabled", "");
      expect(this.element.querySelector(".btn-action-2 .fa-spinner")).not.to.exist;
      expect(this.element.querySelector(".btn-action-2 .fa-user")).to.exist;
    });

    it('should hide spinner on action success', async function() {
      this.set("onAction", function() {
        return { then(a) { a(); } }
      });

      await render(hbs`<Manage::ActionsSelected @actions={{this.actions}} @onAction={{this.onAction}} @selected={{this.selected}} @onDelete={{this.delete}}/>`);
      await click(this.element.querySelector(".btn-action-1"));

      await waitUntil(() => !this.element.querySelector(".btn-action-1 .fa-spinner"));

      expect(this.element.querySelector(".btn-action-1")).not.to.have.attribute("disabled", "");
      expect(this.element.querySelector(".btn-action-1 .fa-spinner")).not.to.exist;
      expect(this.element.querySelector(".btn-action-1 .fa-map")).to.exist;

      expect(this.element.querySelector(".btn-action-2")).not.to.have.attribute("disabled", "");
    });
  });
});
