import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

describe('Integration | Component | manage/group-buttons', function() {
  setupRenderingTest();

  it('renders', async function() {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`<Manage::GroupButtons />`);

    expect(this.element.textContent.trim()).to.equal('Edit');

    // Template block usage:
    await render(hbs`
      <Manage::GroupButtons>
        template block text
      </Manage::GroupButtons>
    `);

    expect(this.element.textContent.trim()).to.equal('Edit');
  });
});
