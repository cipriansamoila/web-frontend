import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/toggle', function() {
  setupRenderingTest();

  it('renders an active toggle by default', async function() {
    await render(hbs`<Manage::Toggle @title="Is public" />`);

    expect(this.element.querySelector(".form-switch")).to.exist;
    expect(this.element.querySelector(".form-check-label").textContent.trim()).to.equal("Is public");
    expect(this.element.querySelector(".fa-spinner")).to.not.exist;
    expect(this.element.querySelector(".btn-switch")).to.not.have.attribute("disabled");
    expect(this.element.querySelector(".container-group")).to.have.class("container-group-is-public");
  });

  it('displays a loading indicator when saving a change', async function() {
    await render(hbs`<Manage::Toggle @title="title" @savingField="title" />`);

    expect(this.element.querySelector(".fa-spinner")).to.exist;
  });

  it('makes the switch enabled when the value is true', async function() {
    await render(hbs`<Manage::Toggle @value={{true}} @title="title" @id="something" />`);

    expect(this.element.querySelector(".btn-switch")).to.have.attribute("checked", "true");
  });

  it('disables the checkbox while saving', async function() {
    await render(hbs`<Manage::Toggle @title="title" @savingField="title" />`);

    expect(this.element.querySelector(".btn-switch")).to.have.attribute("disabled", "");
  });

  it('disables the checkbox when disabled attribute is true', async function() {
    await render(hbs`<Manage::Toggle @title="title" @disabled={{true}} />`);

    expect(this.element.querySelector(".btn-switch")).to.have.attribute("disabled", "");
  });

  it('triggers onSave when changing the toggle value to true', async function() {
    let value;

    this.set("save", function(newValue) {
      value = newValue;
    });

    await render(hbs`<div class="outside"></div> <Manage::Toggle @value={{false}} @title="title" @onSave={{this.save}} />`);
    await click(".btn-switch");

    expect(value).to.equal(true);
  });

  it('triggers onSave when changing the toggle value to false', async function() {
    let value;

    this.set("save", function(newValue) {
      value = newValue;
    });

    await render(hbs`<div class="outside"></div> <Manage::Toggle @value={{true}} @title="title" @onSave={{this.save}} @id="something"/>`);
    await click(".btn-switch");

    expect(value).to.equal(false);
  });

  it('updates the toggle value when clicking on the toggle label', async function() {
    let value;

    this.set("save", function(newValue) {
      value = newValue;
    });

    await render(hbs`<div class="outside"></div> <Manage::Toggle @value={{true}} @title="title" @onSave={{this.save}} @id="something" />`);
    await click(".form-check-label");

    expect(value).to.equal(false);
  });
});
