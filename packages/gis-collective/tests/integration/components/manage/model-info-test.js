import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupIntl } from 'ember-intl/test-support';

describe('Integration | Component | manage/model-info', function () {
  let hooks = setupRenderingTest();
  setupIntl(hooks);

  it('renders', async function () {
    await render(hbs`<Manage::ModelInfo />`);

    expect(this.element.textContent.trim()).to.contain('original author');
    expect(this.element.textContent.trim()).to.contain('edit');
    expect(this.element.textContent.trim()).to.contain('last change');
  });
});
