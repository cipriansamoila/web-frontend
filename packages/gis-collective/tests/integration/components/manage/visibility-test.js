import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

describe('Integration | Component | manage/visibility', function () {
  setupRenderingTest();

  it('renders the is default section when hasIsDefault = true', async function () {
    await render(hbs`<Manage::Visibility @hasIsDefault={{true}} />`);

    expect(this.element.textContent.trim()).to.contain('team');
    expect(this.element.textContent.trim()).to.contain('edit');
    expect(this.element.textContent.trim()).to.contain('is public');
    expect(this.element.textContent.trim()).to.contain('is default');
  });

  it('does not render the is default section when hasIsDefault = false', async function () {
    await render(hbs`<Manage::Visibility @hasIsDefault={{false}} />`);

    expect(this.element.textContent.trim()).to.contain('team');
    expect(this.element.textContent.trim()).to.contain('edit');
    expect(this.element.textContent.trim()).to.contain('is public');
    expect(this.element.textContent.trim()).not.to.contain('is default');
  });

  it('should show an alert when the model has no team', async function () {
    await render(hbs`<Manage::Visibility />`);

    expect(
      this.element
        .querySelector('.container-group-team .alert-danger')
        .textContent.trim()
    ).to.equal(
      'This record has no team. You must set one in order to have a valid ownership.'
    );
  });

  it('should hide the alert when the model has a team', async function () {
    this.set('model', {
      visibility: {
        team: {
          name: 'name',
        },
      },
    });

    await render(hbs`<Manage::Visibility @model={{this.model}}/>`);

    expect(this.element.querySelector('.container-group-team .alert-danger'))
      .not.to.exist;
  });
});
