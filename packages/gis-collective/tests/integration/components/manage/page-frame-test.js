import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | manage/page-frame', function () {
  setupRenderingTest();

  it('renders the header and body', async function () {
    await render(
      hbs`<Manage::PageFrame as | isHeader |>{{isHeader}}</Manage::PageFrame>`
    );

    expect(
      this.element.querySelector('.page-frame-header').textContent.trim()
    ).to.equal('true');
    expect(
      this.element.querySelector('.page-frame-body').textContent.trim()
    ).to.equal('false');
  });
});
