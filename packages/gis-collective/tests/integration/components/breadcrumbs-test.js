import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | breadcrumbs', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Breadcrumbs />`);

    expect(this.element.textContent.trim()).to.equal('');

    // Template block usage:
    await render(hbs`
      <Breadcrumbs>
        template block text
      </Breadcrumbs>
    `);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a list of breadcrumbs with links', async function() {
    this.set("value", [
      { model: "model", route: "route", text: "text1" },
      { model: "model", route: "route", text: "text2" }
    ]);

    await render(hbs`<Breadcrumbs @value={{this.value}}/>`);

    const items = this.element.querySelectorAll(".breadcrumb-item");

    expect(this.element.querySelector(".breadcrumb-item.active")).not.to.exist;
    expect(items.length).to.equal(2);
    expect(items[0].textContent.trim()).to.equal("text1");
    expect(items[1].textContent.trim()).to.equal("text2");
  });

  it('renders a list of breadcrumbs without links', async function() {
    this.set("value", [
      { text: "text1" },
      { text: "text2" }
    ]);

    await render(hbs`<Breadcrumbs @value={{this.value}}/>`);

    const items = this.element.querySelectorAll(".breadcrumb-item.active");

    expect(items.length).to.equal(2);
    expect(items[0].textContent.trim()).to.equal("text1");
    expect(items[1].textContent.trim()).to.equal("text2");
  });
});
