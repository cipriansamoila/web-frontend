import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | modal-info', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<ModalInfo />`);

    expect(this.element.querySelector(".modal-content .modal-header")).to.exist;
    expect(this.element.querySelector(".modal-content .modal-header .modal-title")).to.exist;
    expect(this.element.querySelector(".modal-content .modal-body")).to.exist;
    expect(this.element.querySelector(".modal-content .modal-footer")).to.exist;
  });
});
