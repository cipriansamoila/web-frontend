import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | offcanvas', function () {
  setupRenderingTest();

  it('renders a hidden offcanvas', async function () {
    await render(hbs`<Offcanvas />`);
    expect(
      this.element.querySelector('.offcanvas-body').textContent.trim()
    ).to.equal('');

    expect(this.element.querySelector('.offcanvas')).not.to.have.class('show');
  });

  it('renders a visible sheet when @show is true', async function () {
    await render(hbs`<Offcanvas @show={{true}}/>`);

    expect(this.element.querySelector('.offcanvas')).to.have.class('show');
  });

  it('closes the sheet when the close button is pressed', async function () {
    await render(hbs`<Offcanvas @show={{true}}/>`);

    await click('.btn-close');

    expect(this.element.querySelector('.offcanvas')).not.to.have.class('show');
  });
});
