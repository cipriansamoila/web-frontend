import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | map/controls', function() {
  setupRenderingTest();

  it('renders nothing by default', async function() {
    // Set any properties with this.set('myProperty', 'value');
    // Handle any actions with this.set('myAction', function(val) { ... });

    await render(hbs`{{map/controls}}`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the location button when onLocalize is set', async function() {
    this.timeout(5000);
    let localizeCalled = false;

    this.set('localize', () => {
      localizeCalled = true;
    });

    await render(hbs`
    <Map::Plain as |map|>
      <Map::Controls @map={{map}} @onLocalize={{localize}} />
    </Map::Plain>`);

    await waitFor(".btn-localize");

    await click(this.element.querySelector(".btn-localize"));

    expect(localizeCalled).to.equal(true);

    this.set("localize", false);
    expect(this.element.querySelector(".btn-localize")).not.to.exist;
  });

  it('renders the error location button when onLocalize throws an error', async function() {
    this.timeout(5000);
    this.set('localize', () => {
      throw new Error("asd");
    });

    await render(hbs`
    <Map::Plain as |map|>
      <Map::Controls @map={{map}} @onLocalize={{localize}} />
    </Map::Plain>`);

    await waitFor(".btn-localize");

    expect(this.element.querySelector(".btn-localize.btn-danger")).not.to.exist;
    await click(this.element.querySelector(".btn-localize"));
    expect(this.element.querySelector(".btn-localize.btn-danger")).to.exist;
  });


  it('removes the error state when the localization is a success', async function() {
    this.timeout(5000);
    let i = 0;

    this.set('localize', () => {
      if(i == 0) {
        i++;
        throw new Error("asd");
      }
    });

    await render(hbs`
    <Map::Plain as |map|>
      <Map::Controls @map={{map}} @onLocalize={{localize}} />
    </Map::Plain>`);

    await waitFor(".btn-localize");

    await click(this.element.querySelector(".btn-localize"));
    expect(this.element.querySelector(".btn-localize.btn-danger")).to.exist;

    await click(this.element.querySelector(".btn-localize"));
    expect(this.element.querySelector(".btn-localize.btn-danger")).not.to.exist;
  });
});
