import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | map/context-menu', function() {
  setupRenderingTest();

  it('renders nothing by default', async function() {
    await render(hbs`<Map::ContextMenu />`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the component when isVisible is true', async function() {
    await render(hbs`<Map::ContextMenu @isVisible={{true}} />`);

    expect(this.element.querySelector(".map-context-menu")).to.exist;
  });

  it('renders the options when they are set', async function() {
    await render(hbs`<Map::ContextMenu @isVisible={{true}} @options="option1,option2"/>`);

    expect(this.element.querySelectorAll(".dropdown-item")[0].textContent.trim()).to.equal("Option1");
    expect(this.element.querySelectorAll(".dropdown-item")[1].textContent.trim()).to.equal("Option2");
  });

  it('renders the style with the position when it is visible and it has coordinates', async function() {
    await render(hbs`<Map::ContextMenu @isVisible={{true}} @x="10" @y="20"/>`);

    expect(this.element.querySelector(".map-context-menu").attributes.getNamedItem("style").value.trim()).to.equal("top: 20px; left: 10px;");
  });

  it('triggers an action when an option is selected', async function() {
    let selectedOption = "";

    this.set("action", (option) => {
      selectedOption = option;
    });

    await render(hbs`<Map::ContextMenu @isVisible={{true}} @x="10" @y="20" @options="option1,option2" @onSelect={{this.action}}/>`);
    await click(this.element.querySelectorAll(".dropdown-item")[1]);

    expect(selectedOption).to.equal("option2");
  });


  it('triggers an action with lon lat when the map is set', async function() {
    let selectedOption = "";
    let lon = 0;
    let lat = 0;

    this.set("action", (option, selectedLon, selectedLat) => {
      selectedOption = option;
      lon = selectedLon;
      lat = selectedLat;
    });

    this.set("map", {
      getCoordinateFromPixel() {
        return [100000, 200000];
      }
    });

    await render(hbs`<Map::ContextMenu @map={{this.map}} @isVisible={{true}} @x="10" @y="20" @options="option1,option2" @onSelect={{this.action}}/>`);
    await click(this.element.querySelectorAll(".dropdown-item")[1]);

    expect(selectedOption).to.equal("option2");
    expect(lon).to.equal(0.8983152841195214);
    expect(lat).to.equal(1.7963362120992912);
  });
});
