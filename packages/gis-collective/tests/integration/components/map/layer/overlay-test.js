import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | map/layer/overlay', function () {
  setupRenderingTest();

  it('renders', async function () {
    let overlay;
    this.set('map', {
      addOverlay(v) {
        overlay = v;
      },
    });

    this.set('position', [1, 2]);

    await render(hbs`
      <Map::Layer::Overlay @map={{this.map}} @positioning="bottom-right" @position={{this.position}}>
        template block text
      </Map::Layer::Overlay>
    `);

    expect(overlay).to.exist;
    expect(overlay.getElement().textContent.trim()).to.equal(
      'template block text'
    );
    expect(overlay.getPositioning()).to.equal('bottom-right');
    expect(overlay.getPosition()).to.deep.equal([
      111319.49079327358, 222684.20850554318,
    ]);
  });
});
