import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | map/marker/group', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Map::Marker::Group />`);
    expect(this.element.textContent.trim()).to.equal('');
  });


  it('renders the number of pending feature count', async function() {
    this.set("feature", {
      getProperties() {
        return {
          pendingCount: 1
        }
      }
    });

    await render(hbs`<Map::Marker::Group @feature={{this.feature}} />`);
    expect(this.element.textContent.trim()).to.equal('There is a pending feature');
  });
});
