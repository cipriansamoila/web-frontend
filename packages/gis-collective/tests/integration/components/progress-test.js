import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | progress', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Progress />`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector(".progress-bar")).not.to.exist;
  });

  it('renders a simple progress bar when the max and primary attributes are set', async function() {
    await render(hbs`<Progress @max=100 @primary="30"/>`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector(".progress-bar")).to.exist;
    expect(this.element.querySelector(".bg-success, .bg-info, .bg-warning, .bg-danger, .progress-bar-striped, .progress-bar-animated")).not.to.exist;

    expect(this.element.querySelector(".progress-bar")).to.have.attribute('style', 'width: 30%');
  });

  it('renders the right progress size', async function() {
    await render(hbs`<Progress @max=1 @primary=0.5/>`);
    expect(this.element.textContent.trim()).to.equal('');

    expect(this.element.querySelector(".progress-bar")).to.have.attribute('style', 'width: 50%');
    expect(this.element.querySelector(".progress-bar")).to.have.attribute('aria-valuemax', '1');
    expect(this.element.querySelector(".progress-bar")).to.have.attribute('aria-valuemin', '0');
    expect(this.element.querySelector(".progress-bar")).to.have.attribute('aria-valuenow', '0.5');
  });

  it('renders an animated primary progress', async function() {
    await render(hbs`<Progress @max=1 @primary=0.5 @animated=true/>`);
    expect(this.element.textContent.trim()).to.equal('');

    expect(this.element.querySelector(".progress-bar")).to.have.class('progress-bar-animated');
  });

  it('renders a striped primary progress', async function() {
    await render(hbs`<Progress @max=1 @primary=0.5 @striped=true/>`);
    expect(this.element.textContent.trim()).to.equal('');

    expect(this.element.querySelector(".progress-bar")).to.have.class('progress-bar-striped');
  });

  it('renders a simple success progress bar when the max and success attributes are set', async function() {
    await render(hbs`<Progress @max=100 @success="30"/>`);
    expect(this.element.textContent.trim()).to.equal('');

    expect(this.element.querySelector(".progress-bar.bg-success")).to.exist;
    expect(this.element.querySelector(".bg-info, .bg-warning, .bg-danger, .progress-bar-striped, .progress-bar-animated")).not.to.exist;

    expect(this.element.querySelector(".progress-bar")).to.have.attribute('style', 'width: 30%');
  });

  it('renders a simple danger progress bar when the max and success attributes are set', async function() {
    await render(hbs`<Progress @max=100 @danger="30"/>`);
    expect(this.element.textContent.trim()).to.equal('');

    expect(this.element.querySelector(".progress-bar.bg-danger")).to.exist;
    expect(this.element.querySelector(".bg-info, .bg-warning, .bg-success, .progress-bar-striped, .progress-bar-animated")).not.to.exist;

    expect(this.element.querySelector(".progress-bar")).to.have.attribute('style', 'width: 30%');
  });

  it('renders a simple warning progress bar when the max and success attributes are set', async function() {
    await render(hbs`<Progress @max=100 @warning="30"/>`);
    expect(this.element.textContent.trim()).to.equal('');

    expect(this.element.querySelector(".progress-bar.bg-warning")).to.exist;
    expect(this.element.querySelector(".bg-info, .bg-danger, .bg-success, .progress-bar-striped, .progress-bar-animated")).not.to.exist;

    expect(this.element.querySelector(".progress-bar")).to.have.attribute('style', 'width: 30%');
  });

  it('renders a simple info progress bar when the max and success attributes are set', async function() {
    await render(hbs`<Progress @max=100 @info="30"/>`);
    expect(this.element.textContent.trim()).to.equal('');

    expect(this.element.querySelector(".progress-bar.bg-info")).to.exist;
    expect(this.element.querySelector(".bg-warning, .bg-danger, .bg-success, .progress-bar-striped, .progress-bar-animated")).not.to.exist;

    expect(this.element.querySelector(".progress-bar")).to.have.attribute('style', 'width: 30%');
  });
});
