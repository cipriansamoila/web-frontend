import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | layout', function () {
  setupRenderingTest();

  it('renders the cols in the right container', async function () {
    this.set('containers', [
      {
        rows: [
          {
            cls: 'cls1',
            options: [],
            cols: [{ cls: 'cls2', type: '', options: [] }],
          },
        ],
        options: ['container-fluid', 'pb-5'],
        cls: 'container-fluid pb-5',
      },
      {
        rows: [{ options: [], cols: [{ type: '', options: [] }] }],
        options: ['pb-5'],
      },
    ]);

    await render(
      hbs`<Layout @containers={{this.containers}} as |value|>{{value.container}}-{{value.row}}-{{value.col}}</Layout>`
    );

    expect(this.element.querySelector('.container-fluid.pb-5')).to.exist;
    expect(this.element.querySelector('.row.cls1')).to.exist;
    expect(this.element.querySelector('.col.cls2')).to.exist;
    expect(
      this.element.querySelectorAll('.col')[0].textContent.trim()
    ).to.equal('0-0-0');
    expect(
      this.element.querySelectorAll('.col')[1].textContent.trim()
    ).to.equal('1-0-0');
  });
});
