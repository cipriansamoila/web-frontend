import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import waitUntil from '@ember/test-helpers/wait-until';

describe('Integration | Component | timer', function () {
  setupRenderingTest();

  it('renders', async function () {
    let value = 1;

    this.set('interval', 100);
    this.set('tick', () => {
      value++;
    });

    render(hbs`<Timer @interval={{this.interval}} @onTick={{this.tick}} />`);

    await waitUntil(() => {
      return value >= 3;
    });

    this.set('interval', 0);
  });
});
