import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | native-scrollbar', function () {
  setupRenderingTest();

  it('renders nothing when there is no argument', async function () {
    await render(hbs`<NativeScrollbar />`);
    expect(this.element.querySelector('.inner')).not.to.exist;
  });

  it('renders the scroll on top when the visible items are less then the total', async function () {
    await render(hbs`<NativeScrollbar @visible={{2}} @total={{4}} />`);
    expect(this.element.querySelector('.inner')).to.have.attribute(
      'style',
      'height: 200%'
    );
  });
});
