import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import click from '@ember/test-helpers/dom/click';

describe('Integration | Component | bs-col-widths', function () {
  setupRenderingTest();

  it('renders dashes when the options are not set', async function () {
    await render(hbs`<BsColWidths />`);

    expect(
      this.element.querySelector('.bs-col-width-desktop').textContent.trim()
    ).to.equal('-');
    expect(
      this.element.querySelector('.bs-col-width-tablet').textContent.trim()
    ).to.equal('-');
    expect(
      this.element.querySelector('.bs-col-width-mobile').textContent.trim()
    ).to.equal('-');
  });

  it('renders the mobile size when is set', async function () {
    this.set('options', ['col-2']);
    await render(hbs`<BsColWidths @options={{this.options}} />`);

    expect(
      this.element.querySelector('.bs-col-width-desktop').textContent.trim()
    ).to.equal('-');
    expect(
      this.element.querySelector('.bs-col-width-tablet').textContent.trim()
    ).to.equal('-');
    expect(
      this.element.querySelector('.bs-col-width-mobile').textContent.trim()
    ).to.equal('2');
  });

  it('renders the tablet size when is set', async function () {
    this.set('options', ['col-md-2']);
    await render(hbs`<BsColWidths @options={{this.options}} />`);

    expect(
      this.element.querySelector('.bs-col-width-desktop').textContent.trim()
    ).to.equal('-');
    expect(
      this.element.querySelector('.bs-col-width-tablet').textContent.trim()
    ).to.equal('2');
    expect(
      this.element.querySelector('.bs-col-width-mobile').textContent.trim()
    ).to.equal('-');
  });

  it('renders the desktop size when is set', async function () {
    this.set('options', ['col-lg-2']);
    await render(hbs`<BsColWidths @options={{this.options}} />`);

    expect(
      this.element.querySelector('.bs-col-width-desktop').textContent.trim()
    ).to.equal('2');
    expect(
      this.element.querySelector('.bs-col-width-tablet').textContent.trim()
    ).to.equal('-');
    expect(
      this.element.querySelector('.bs-col-width-mobile').textContent.trim()
    ).to.equal('-');
  });

  it('does not show any size as active by default', async function () {
    await render(hbs`<BsColWidths @options={{this.options}} />`);

    expect(this.element.querySelector('.bs-col-width-desktop.active')).not.to
      .exist;
    expect(this.element.querySelector('.bs-col-width-tablet.active')).not.to
      .exist;
    expect(this.element.querySelector('.bs-col-width-mobile.active')).not.to
      .exist;
  });

  it('activates the desktop size when size is desktop', async function () {
    await render(hbs`<BsColWidths @options={{this.options}} @size="desktop"/>`);

    expect(this.element.querySelector('.bs-col-width-desktop.active')).to.exist;
    expect(this.element.querySelector('.bs-col-width-tablet.active')).not.to
      .exist;
    expect(this.element.querySelector('.bs-col-width-mobile.active')).not.to
      .exist;
  });

  it('activates the tablet size when size is tablet', async function () {
    await render(hbs`<BsColWidths @options={{this.options}} @size="tablet"/>`);

    expect(this.element.querySelector('.bs-col-width-desktop.active')).not.to
      .exist;
    expect(this.element.querySelector('.bs-col-width-tablet.active')).to.exist;
    expect(this.element.querySelector('.bs-col-width-mobile.active')).not.to
      .exist;
  });

  it('activates the mobile size when size is mobile', async function () {
    await render(hbs`<BsColWidths @options={{this.options}} @size="mobile"/>`);

    expect(this.element.querySelector('.bs-col-width-desktop.active')).not.to
      .exist;
    expect(this.element.querySelector('.bs-col-width-tablet.active')).not.to
      .exist;
    expect(this.element.querySelector('.bs-col-width-mobile.active')).to.exist;
  });

  it('triggers onClick event when the component is clicked', async function () {
    let clicked;
    this.set('click', () => {
      clicked = true;
    });

    await render(hbs`<BsColWidths @onClick={{this.click}}/>`);
    await click('.bs-col-widths');

    expect(clicked).to.equal(true);
  });
});
