import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

describe('Integration | Component | language-selector', function() {
  setupRenderingTest();

  it('gets a list of languages with name and locale', async function() {
    this.set("languages1", [{name: "English", locale: "en-US"}, {name: "Romanian", locale: "ro-RO"}]);
    await render(hbs`<LanguageSelector @languages={{this.languages1}} />`);

    var dropdownItems = this.element.querySelectorAll(".dropdown-item");
    expect(this.element.querySelector(".dropdown-language")).to.exist;
    expect(dropdownItems.length).to.equal(2);
    expect(dropdownItems[0].textContent.trim()).to.equal('English');
    expect(dropdownItems[1].textContent.trim()).to.equal('Romanian');
  });

  it('displays the selected language', async function() {
    this.set("languages3", [{name: "English", locale: "en-US"}, {name: "Romanian", locale: "ro-RO"}]);

    await render(hbs`<LanguageSelector @languages={{this.languages3}} @selectedLocale="en-US"/>`);

    expect(this.element.querySelector(".dropdown-toggle").textContent.trim()).to.contain("English");
  });
});
