import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import {
  render,
  click,
  triggerEvent,
  waitUntil,
  typeIn,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { transform } from 'ol/proj';

let testGpxTrack = `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<gpx xmlns="http://www.topografix.com/GPX/1/1" creator="Maps 3D" version="1.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
<trk>
<name>Maps3D 2020-05-31 04-45pm</name>
<trkseg>
<trkpt lat="52.482009" lon="13.407045"><ele>48.080788</ele><time>2020-05-31T11:32:26Z</time></trkpt>
<trkpt lat="52.482077" lon="13.407251"><ele>48.009930</ele><time>2020-05-31T11:33:31Z</time></trkpt>
</trkseg></trk></gpx>`;

let testGpxRoute = `<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
<rte>
<rtept lat="12.0000000000000" lon="11.0000000000000"/>
<rtept lat="14.0000000000000" lon="13.0000000000000"/>
<rtept lat="16.0000000000000" lon="15.0000000000000"/>
<rtept lat="18.0000000000000" lon="17.0000000000000"/>
</rte></gpx>`;

let testGpxTrackAndRoute = `<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
<rte>
<rtept lat="12.0000000000000" lon="11.0000000000000"/>
<rtept lat="14.0000000000000" lon="13.0000000000000"/>
<rtept lat="16.0000000000000" lon="15.0000000000000"/>
<rtept lat="18.0000000000000" lon="17.0000000000000"/>
</rte><trk>
<name>Maps3D 2020-05-31 04-45pm</name>
<trkseg>
<trkpt lat="52.482009" lon="13.407045"><ele>48.080788</ele><time>2020-05-31T11:32:26Z</time></trkpt>
<trkpt lat="52.482077" lon="13.407251"><ele>48.009930</ele><time>2020-05-31T11:33:31Z</time></trkpt>
</trkseg></trk></gpx>`;

let testGpxEmpty = `<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<gpx xmlns="http://www.topografix.com/GPX/1/1" version="1.1" xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
</gpx>`;

let testJsonTrack = `{
  "type": "LineString",
  "coordinates": [
      [102.0, 0.0],
      [103.0, 1.0],
      [104.0, 0.0],
      [105.0, 1.0]]}`;

let testJsonMultiLine = `{
  "type": "MultiLineString",
  "coordinates": [[
      [102.0, 0.0],
      [103.0, 1.0],
      [104.0, 0.0],
      [105.0, 1.0]]]}`;

let testJsonMultiPoint = `{
  "type": "MultiPoint",
  "coordinates": [
      [100.0, 0.0],
      [101.0, 1.0]
  ]}`;

let testJsonMultiPolygon = `{
  "type": "MultiPolygon",
  "coordinates": [[[
    [180.0, 40.0], [180.0, 50.0], [170.0, 50.0], [170.0, 40.0], [180.0, 40.0]
  ]],[[
    [-170.0, 40.0], [-170.0, 50.0], [-180.0, 50.0], [-180.0, 40.0], [-170.0, 40.0]
  ]]]}`;

let testFeaturePolygon = `{
    "type": "Feature",
    "bbox": [-10.0, -10.0, 10.0, 10.0],
    "geometry": {
        "type": "Polygon",
        "coordinates": [[
            [-10.0, -10.0],
            [10.0, -10.0],
            [10.0, 10.0],
            [-10.0, -10.0]
        ]]}}`;

let testPolygon = `{
"type": "Polygon",
"coordinates": [[
    [-10.0, -10.0],
    [10.0, -10.0],
    [10.0, 10.0],
    [-10.0, -10.0]
]]}`;

let testMultiPolygon = `{
  "type": "MultiPolygon",
  "coordinates": [[[
    [-10.0, -20.0],
    [10.0, -10.0],
    [10.0, 10.0],
    [-10.0, -10.0]
  ]]]
}`;

let testFeatureWithoutGeometryPolygon = `{
  "type": "Feature",
  "bbox": [-10.0, -10.0, 10.0, 10.0]}`;

describe('Integration | Component | input/geometry', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<Input::Geometry />`);

    expect(this.element.querySelector('.btn-point')).to.exist;
    expect(this.element.querySelector('.btn-line')).to.exist;
    expect(this.element.querySelector('.btn-polygon')).to.exist;
  });

  it('selects the point button by default', async function () {
    await render(hbs`<Input::Geometry />`);

    expect(this.element.querySelector('.btn-point')).to.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-point')).to.not.have.class(
      'btn-outline-secondary'
    );

    expect(this.element.querySelector('.btn-line')).to.not.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-line')).to.have.class(
      'btn-outline-secondary'
    );

    expect(this.element.querySelector('.btn-polygon')).to.not.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-polygon')).to.have.class(
      'btn-outline-secondary'
    );
  });

  it('selects the edit mode button by default', async function () {
    await render(hbs`<Input::Geometry />`);

    expect(this.element.querySelector('.btn-edit')).to.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-edit')).to.not.have.class(
      'btn-outline-secondary'
    );

    expect(this.element.querySelector('.btn-paste')).to.not.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-paste')).to.have.class(
      'btn-outline-secondary'
    );

    expect(this.element.querySelector('.btn-file')).to.not.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-file')).to.have.class(
      'btn-outline-secondary'
    );
  });

  it('selects the line button on click', async function () {
    this.set('value', null);
    this.set('handleChange', (value) => {
      this.set('value', value);
    });

    await render(
      hbs`<Input::Geometry @value={{this.value}} @onChange={{this.handleChange}}/>`
    );

    await click('.btn-line');

    expect(this.element.querySelector('.btn-point')).to.not.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-point')).to.have.class(
      'btn-outline-secondary'
    );

    expect(this.element.querySelector('.btn-line')).to.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-line')).to.not.have.class(
      'btn-outline-secondary'
    );

    expect(this.element.querySelector('.btn-polygon')).to.not.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-polygon')).to.have.class(
      'btn-outline-secondary'
    );
  });

  it('selects the polygon button on click', async function () {
    this.set('value', null);
    this.set('handleChange', (value) => {
      this.set('value', value);
    });

    await render(
      hbs`<Input::Geometry @value={{this.value}} @onChange={{this.handleChange}}/>`
    );

    await click('.btn-polygon');

    expect(this.element.querySelector('.btn-point')).to.not.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-point')).to.have.class(
      'btn-outline-secondary'
    );

    expect(this.element.querySelector('.btn-line')).to.not.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-line')).to.have.class(
      'btn-outline-secondary'
    );

    expect(this.element.querySelector('.btn-polygon')).to.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-polygon')).to.not.have.class(
      'btn-outline-secondary'
    );
  });

  it('selects the point button on click', async function () {
    this.set('value', { type: 'LineString', coordinates: [] });
    this.set('handleChange', (value) => {
      this.set('value', value);
    });

    await render(
      hbs`<Input::Geometry @value={{this.value}} @onChange={{this.handleChange}}/>`
    );

    await click('.btn-point');

    expect(this.element.querySelector('.btn-point')).to.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-point')).to.not.have.class(
      'btn-outline-secondary'
    );

    expect(this.element.querySelector('.btn-line')).to.not.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-line')).to.have.class(
      'btn-outline-secondary'
    );

    expect(this.element.querySelector('.btn-polygon')).to.not.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-polygon')).to.have.class(
      'btn-outline-secondary'
    );
  });

  it('does not change the map view when the editor type is changed', async function () {
    this.set('value', null);
    this.set('handleChange', (value) => {
      this.set('value', value);
    });

    await render(
      hbs`<Input::Geometry @value={{this.value}} @onChange={{this.handleChange}}/>`
    );

    const map = this.element.querySelector('.map').olMap;
    const view = map.getView();
    const initialExtent = JSON.stringify(view.calculateExtent());

    await click('.btn-line');
    const secondExtent = JSON.stringify(view.calculateExtent());

    expect(initialExtent).to.equal(secondExtent);
  });

  it('changes the point location when the map view is changed', async function () {
    this.set('value', { type: 'Point', coordinates: [1, 2] });
    this.set('handleChange', (value) => {
      this.set('value', value);
    });

    await render(
      hbs`<Input::Geometry @value={{this.value}} @onChange={{this.handleChange}}/>`
    );
    const map = this.element.querySelector('.map').olMap;
    map.getView().setCenter(transform([10, 20], 'EPSG:4326', 'EPSG:3857'));

    await click('.btn-point');
    expect(this.value).to.deep.equal({ type: 'Point', coordinates: [10, 20] });
  });

  it('resets the line by pressing the line button', async function () {
    this.set('value', {
      type: 'LineString',
      coordinates: [
        [1, 2],
        [3, 4],
      ],
    });
    this.set('handleChange', (value) => {
      this.set('value', value);
    });

    await render(
      hbs`<Input::Geometry @value={{this.value}} @onChange={{this.handleChange}}/>`
    );

    await click('.btn-line');
    expect(this.value).to.deep.equal({ type: 'LineString', coordinates: [] });
  });

  it('resets the polygon by pressing the polygon button', async function () {
    this.set('value', {
      type: 'Polygon',
      coordinates: [
        [1, 2],
        [3, 4],
        [5, 6],
        [1, 2],
      ],
    });
    this.set('handleChange', (value) => {
      this.set('value', value);
    });

    await render(
      hbs`<Input::Geometry @value={{this.value}} @onChange={{this.handleChange}}/>`
    );

    await click('.btn-polygon');
    expect(this.value).to.deep.equal({ type: 'Polygon', coordinates: [] });
  });

  it('selects the paste mode button on click', async function () {
    await render(hbs`<Input::Geometry />`);

    await click('.btn-paste');

    expect(this.element.querySelector('.btn-edit')).to.not.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-edit')).to.have.class(
      'btn-outline-secondary'
    );

    expect(this.element.querySelector('.btn-paste')).to.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-paste')).to.not.have.class(
      'btn-outline-secondary'
    );

    expect(this.element.querySelector('.btn-file')).to.not.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-file')).to.have.class(
      'btn-outline-secondary'
    );
  });

  it('selects the file mode button on click', async function () {
    await render(hbs`<Input::Geometry />`);

    await click('.btn-file');

    expect(this.element.querySelector('.btn-edit')).to.not.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-edit')).to.have.class(
      'btn-outline-secondary'
    );

    expect(this.element.querySelector('.btn-paste')).to.not.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-paste')).to.have.class(
      'btn-outline-secondary'
    );

    expect(this.element.querySelector('.btn-file')).to.have.class(
      'btn-success'
    );
    expect(this.element.querySelector('.btn-file')).to.not.have.class(
      'btn-outline-secondary'
    );
  });

  it('hides the geometry types when the paste mode is selected', async function () {
    await render(hbs`<Input::Geometry />`);

    await click('.btn-paste');
    expect(this.element.querySelector('.btn-group-operations')).not.to.exist;
  });

  it('hides the geometry types when the file mode is selected', async function () {
    await render(hbs`<Input::Geometry />`);

    await click('.btn-file');
    expect(this.element.querySelector('.btn-group-operations')).not.to.exist;
  });

  it('shows the geometry types when the edit mode is selected', async function () {
    await render(hbs`<Input::Geometry />`);

    await click('.btn-file');
    await click('.btn-edit');

    expect(this.element.querySelector('.btn-group-operations')).to.exist;
  });

  it('shows the file input when the file mode is selected', async function () {
    await render(hbs`<Input::Geometry />`);

    await click('.btn-file');

    expect(this.element.querySelector('input[type=file]')).to.exist;
    expect(this.element.querySelector('.input-geo-json')).not.to.exist;
    expect(this.element.querySelector('.map-container')).not.to.exist;
  });

  it('shows the geo json input when the paste mode is selected', async function () {
    await render(hbs`<Input::Geometry />`);

    await click('.btn-paste');

    expect(this.element.querySelector('.custom-file')).not.to.exist;
    expect(this.element.querySelector('.input-geo-json')).to.exist;
    expect(this.element.querySelector('.map-container')).not.to.exist;
  });

  it('shows the map when the edit mode is selected', async function () {
    await render(hbs`<Input::Geometry />`);

    await click('.btn-edit');

    expect(this.element.querySelector('.custom-file')).not.to.exist;
    expect(this.element.querySelector('.input-geo-json')).not.to.exist;
    expect(this.element.querySelector('.map-container')).to.exist;
  });

  describe('using the json editor', function () {
    it('shows the geometry value when it is set', async function () {
      this.set('value', testJsonTrack);

      await render(hbs`<Input::Geometry @value={{this.value}}/>`);

      await click('.btn-paste');

      const editor = this.element.querySelector('.input-geo-json').editor;
      editor.setMode('text');

      const value = JSON.parse(
        this.element.querySelector('.jsoneditor-text').value
      );

      expect(value).to.deep.equal(JSON.parse(testJsonTrack));
    });

    it('updates the geometry', async function () {
      let value;

      this.set('handleChange', (v) => {
        value = v;
      });

      await render(hbs`<Input::Geometry @onChange={{this.handleChange}}/>`);

      await click('.btn-paste');

      const editor = this.element.querySelector('.input-geo-json').editor;
      editor.setMode('text');

      this.element.querySelector('.jsoneditor-text').value = '';
      await typeIn('.jsoneditor-text', `{ "key": "value" }`);

      expect(value).to.deep.equal({ key: 'value' });
    });

    it('does not update the geometry while setting an invalid value', async function () {
      let value = 'not set';

      this.set('handleChange', (v) => {
        value = v;
      });

      await render(hbs`<Input::Geometry @onChange={{this.handleChange}}/>`);

      const map = this.element.querySelector('.map').olMap;
      map.getView().setCenter(transform([10, 20], 'EPSG:4326', 'EPSG:3857'));

      await click('.btn-paste');

      const editor = this.element.querySelector('.input-geo-json').editor;
      editor.setMode('text');

      this.element.querySelector('.jsoneditor-text').value = '';
      await typeIn('.jsoneditor-text', `{ "key": "value"`);

      expect(value).to.deep.equal({ type: 'Point', coordinates: [10, 20] });
    });
  });

  describe('using the map editor', async function () {
    it('should use a point at 0, 0 when the position is null', async function () {
      await render(hbs`<Input::Geometry />`);

      await waitUntil(() => this.element.querySelector('.map').olMap);
      const map = this.element.querySelector('.map').olMap;
      const view = map.getView();
      const center = view.getCenter();

      expect(center[0]).to.be.closeTo(0, 0.1);
      expect(center[1]).to.be.closeTo(0, 0.1);
      expect(view.getZoom()).to.be.closeTo(2, 0.5);
    });

    it('should start drawing a polygon when the polygon button is selected', async function () {
      this.set('handleChange', (v) => {
        this.set('value', v);
      });

      await render(
        hbs`<Input::Geometry @value={{this.value}} @onChange={{this.handleChange}}/>`
      );
      await waitUntil(() => this.element.querySelector('.map').olMap);

      await click('.btn-polygon');

      const map = this.element.querySelector('.map').olMap;
      expect(map.getLayers().item(0).getSource().getFeatures().length).to.equal(
        0
      );
      const interactions = [];
      map.getInteractions().forEach((b) => interactions.push(b));
      const interactionsName = interactions.map((a) => a.constructor.name);

      expect(interactionsName).contains('Draw');
      const drawInteraction = interactions.filter(
        (a) => a.constructor.name == 'Draw'
      );

      expect(drawInteraction[0].type_).equal('Polygon');
    });

    it('should start drawing a line when the line button is selected after the polygon was pressed', async function () {
      this.set('handleChange', (v) => {
        this.set('value', v);
      });

      await render(
        hbs`<Input::Geometry @value={{this.value}} @onChange={{this.handleChange}}/>`
      );
      await waitUntil(() => this.element.querySelector('.map').olMap);

      await click('.btn-polygon');
      await click('.btn-line');

      const map = this.element.querySelector('.map').olMap;
      expect(map.getLayers().item(0).getSource().getFeatures().length).to.equal(
        0
      );
      const interactions = [];
      map.getInteractions().forEach((b) => interactions.push(b));
      const interactionsName = interactions.map((a) => a.constructor.name);

      expect(interactionsName).contains('Draw');
      const drawInteraction = interactions.filter(
        (a) => a.constructor.name == 'Draw'
      );

      expect(drawInteraction[0].type_).equal('LineString');
    });

    it('should start moving a point when the point button is selected after the polygon was pressed', async function () {
      this.set('handleChange', (v) => {
        this.set('value', v);
      });

      await render(
        hbs`<Input::Geometry @value={{this.value}} @onChange={{this.handleChange}}/>`
      );
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;
      map.getView().setCenter(transform([10, 20], 'EPSG:4326', 'EPSG:3857'));
      map.getView().setZoom(12);

      await click('.btn-polygon');

      map.getView().setCenter(transform([30, 40], 'EPSG:4326', 'EPSG:3857'));
      await click('.btn-point');

      expect(this.value).to.deep.equal({
        type: 'Point',
        coordinates: [30, 40],
      });
    });

    it('should render a line in edit mode when the value is a LineString', async function () {
      this.set('value', JSON.parse(testJsonTrack));

      await render(hbs`<Input::Geometry @value={{this.value}}/>`);
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;
      const interactions = [];
      map.getInteractions().forEach((b) => interactions.push(b));
      const interactionsName = interactions.map((a) => a.constructor.name);

      expect(interactionsName).contains('Select');
      expect(interactionsName).contains('Modify');

      const features = map.getLayers().item(0).getSource().getFeatures();
      const coordinates = features[0]
        .getGeometry()
        .transform('EPSG:3857', 'EPSG:4326')
        .getCoordinates()
        .map((pair) => [parseInt(pair[0]), parseInt(pair[1])]);

      expect(coordinates).deep.equal([
        [101, 0],
        [103, 1],
        [104, 0],
        [105, 1],
      ]);
    });

    it('should render a multi line in edit mode when the value is a MultiLineString', async function () {
      this.set('value', JSON.parse(testJsonMultiLine));

      await render(hbs`<Input::Geometry @value={{this.value}}/>`);
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;
      const interactions = [];
      map.getInteractions().forEach((b) => interactions.push(b));
      const interactionsName = interactions.map((a) => a.constructor.name);

      expect(interactionsName).contains('Select');
      expect(interactionsName).contains('Modify');

      const features = map.getLayers().item(0).getSource().getFeatures();
      const coordinates = features[0]
        .getGeometry()
        .transform('EPSG:3857', 'EPSG:4326')
        .getCoordinates()
        .map((a) => a.map((pair) => [parseInt(pair[0]), parseInt(pair[1])]));

      expect(coordinates).deep.equal([
        [
          [101, 0],
          [103, 1],
          [104, 0],
          [105, 1],
        ],
      ]);
    });

    it('should render a polygon in edit mode when the value is a Polygon', async function () {
      this.set('value', JSON.parse(testPolygon));

      await render(hbs`<Input::Geometry @value={{this.value}}/>`);
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;
      const interactions = [];
      map.getInteractions().forEach((b) => interactions.push(b));
      const interactionsName = interactions.map((a) => a.constructor.name);

      expect(interactionsName).contains('Select');
      expect(interactionsName).contains('Modify');

      const features = map.getLayers().item(0).getSource().getFeatures();
      const coordinates = features[0]
        .getGeometry()
        .transform('EPSG:3857', 'EPSG:4326')
        .getCoordinates()
        .map((a) => a.map((pair) => [parseInt(pair[0]), parseInt(pair[1])]));

      expect(coordinates).deep.equal([
        [
          [-10, -10],
          [10, -10],
          [10, 10],
          [-10, -10],
        ],
      ]);
    });

    it('should render a MultiPolygon in edit mode when the value is a MultiPolygon', async function () {
      this.set('value', JSON.parse(testMultiPolygon));

      await render(hbs`<Input::Geometry @value={{this.value}}/>`);
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;
      const interactions = [];
      map.getInteractions().forEach((b) => interactions.push(b));
      const interactionsName = interactions.map((a) => a.constructor.name);

      expect(interactionsName).contains('Select');
      expect(interactionsName).contains('Modify');

      const features = map.getLayers().item(0).getSource().getFeatures();
      expect(features.length).to.equal(1);
    });

    it('renders the extent when it is set', async function () {
      this.set('value', JSON.parse(testPolygon));
      this.set('extent', {
        type: 'Polygon',
        coordinates: [
          [
            [-74.02053047199401, 40.69132569295246],
            [-73.8908520916848, 40.69132569295246],
            [-73.8908520916848, 40.82500472865084],
            [-74.02053047199401, 40.82500472865084],
            [-74.02053047199401, 40.69132569295246],
          ],
        ],
      });

      await render(
        hbs`<Input::Geometry @value={{this.value}} @extent={{this.extent}}/>`
      );
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;

      const features = map.getLayers().item(0).getSource().getFeatures();
      const coordinates = features[0]
        .getGeometry()
        .transform('EPSG:3857', 'EPSG:4326')
        .getCoordinates();

      const view = map.getView();

      expect(coordinates[0].length).to.deep.equal(5);
      expect(coordinates[0][0]).to.deep.equal([
        -74.02053047199401, 40.69132569295246,
      ]);

      const center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');

      expect(center[0]).to.be.closeTo(-73, 1);
      expect(center[1]).to.be.closeTo(40, 1);
      expect(view.getZoom()).to.be.closeTo(11, 1);
    });

    it('does not render the extent when it is not a polygon', async function () {
      this.set('value', JSON.parse(testPolygon));
      this.set('extent', {
        type: 'LineString',
        coordinates: [
          [
            [-74.02053047199401, 40.69132569295246],
            [-73.8908520916848, 40.69132569295246],
            [-73.8908520916848, 40.82500472865084],
            [-74.02053047199401, 40.82500472865084],
            [-74.02053047199401, 40.69132569295246],
          ],
        ],
      });

      await render(
        hbs`<Input::Geometry @value={{this.value}} @extent={{this.extent}}/>`
      );
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;

      const features = map.getLayers().item(0).getSource().getFeatures();
      const coordinates = features[0]
        .getGeometry()
        .transform('EPSG:3857', 'EPSG:4326')
        .getCoordinates();

      expect(coordinates[0].length).to.deep.equal(4);
    });
  });

  describe('selecting files', function () {
    beforeEach(async function () {
      this.set('handleChange', (value) => {
        this.set('value', value);
      });

      await render(
        hbs`<Input::Geometry @value={{this.value}} @onChange={{this.handleChange}}/>`
      );

      await click('.btn-file');
    });

    it('updates the value with a gpx track file when a valid file is selected', async function () {
      let blob = new Blob([testGpxTrack], { type: 'application/gpx+xml' });

      blob.name = 'file.gpx';

      await triggerEvent('input[type=file]', 'change', { files: [blob] });
      await waitUntil(() => this.value);

      expect(this.value).to.deep.equal({
        type: 'gpx.track',
        value: testGpxTrack,
      });
    });

    it('updates the value with a gpx track file when a valid file is selected', async function () {
      let blob = new Blob([testGpxTrackAndRoute], {
        type: 'application/gpx+xml',
      });

      blob.name = 'file.gpx';

      await triggerEvent('input[type=file]', 'change', { files: [blob] });
      await waitUntil(() => this.value);

      expect(this.value).to.deep.equal({
        type: 'gpx.track',
        value: testGpxTrackAndRoute,
      });
    });

    it('updates the value with a gpx route file when a valid file is selected', async function () {
      let blob = new Blob([testGpxRoute], { type: 'application/gpx+xml' });

      blob.name = 'file.gpx';

      await triggerEvent('input[type=file]', 'change', { files: [blob] });
      await waitUntil(() => this.value);

      expect(this.value).to.deep.equal({
        type: 'gpx.route',
        value: testGpxRoute,
      });
    });

    it('shows an error when the gpx has no track or route', async function () {
      let blob = new Blob([testGpxEmpty], { type: 'application/gpx+xml' });

      blob.name = 'file.gpx';

      await triggerEvent('input[type=file]', 'change', { files: [blob] });
      await waitUntil(() => this.value != '');

      expect(this.value).to.equal(null);
      expect(
        this.element.querySelector('.invalid-feedback').textContent.trim()
      ).to.equal('The gpx file must contain at least one route or track.');
    });

    it('updates the value with a json file when a valid file is selected', async function () {
      let blob = new Blob([testJsonTrack], { type: 'application/json' });

      blob.name = 'file.json';

      await triggerEvent('input[type=file]', 'change', { files: [blob] });
      await waitUntil(() => this.value);

      expect(this.value).to.deep.equal(JSON.parse(testJsonTrack));
    });

    it('updates the value with a json file containing a feature', async function () {
      let blob = new Blob([testFeaturePolygon], { type: 'application/json' });

      blob.name = 'file.json';

      await triggerEvent('input[type=file]', 'change', { files: [blob] });
      await waitUntil(() => this.value);

      expect(this.value).to.deep.equal(JSON.parse(testFeaturePolygon));
    });

    it('shows an error when the provided json is not valid', async function () {
      let blob = new Blob(['some invalid value'], {
        type: 'application/gpx+xml',
      });

      blob.name = 'file.json';

      await triggerEvent('input[type=file]', 'change', { files: [blob] });
      await waitUntil(() => this.value !== '');

      expect(this.value).to.equal(null);
      expect(
        this.element.querySelector('.invalid-feedback').textContent.trim()
      ).to.equal('The file contains an invalid Json');
    });

    it('shows an error when a multi point geojson is selected', async function () {
      let blob = new Blob([testJsonMultiPoint], { type: 'application/json' });

      blob.name = 'file.json';

      await triggerEvent('input[type=file]', 'change', { files: [blob] });
      await waitUntil(() => this.value !== '');

      expect(this.value).to.equal(null);
      expect(
        this.element.querySelector('.invalid-feedback').textContent.trim()
      ).to.equal(
        'The file contains a `MultiPoint` geometry. Only Point, LineString, MultiLineString and Polygon are allowed.'
      );
    });

    it('shows an error when a multi polygon geojson is selected', async function () {
      let blob = new Blob([testJsonMultiPolygon], { type: 'application/json' });

      blob.name = 'file.json';

      await triggerEvent('input[type=file]', 'change', { files: [blob] });
      await waitUntil(() => this.value !== '');

      expect(this.value).to.equal(null);
      expect(
        this.element.querySelector('.invalid-feedback').textContent.trim()
      ).to.equal(
        'The file contains a `MultiPolygon` geometry. Only Point, LineString, MultiLineString and Polygon are allowed.'
      );
    });

    it('shows an error when a multi polygon geojson is selected', async function () {
      let blob = new Blob([testJsonMultiPolygon], { type: 'application/json' });

      blob.name = 'file.json';

      await triggerEvent('input[type=file]', 'change', { files: [blob] });
      await waitUntil(() => this.value !== '');

      expect(this.value).to.equal(null);
      expect(
        this.element.querySelector('.invalid-feedback').textContent.trim()
      ).to.equal(
        'The file contains a `MultiPolygon` geometry. Only Point, LineString, MultiLineString and Polygon are allowed.'
      );
    });

    it('shows an error when an array json is selected', async function () {
      let blob = new Blob(['[]'], { type: 'application/json' });
      blob.name = 'file.json';

      await triggerEvent('input[type=file]', 'change', { files: [blob] });
      await waitUntil(() => this.value !== '');

      expect(this.value).to.equal(null);
      expect(
        this.element.querySelector('.invalid-feedback').textContent.trim()
      ).to.equal(
        'The file contains an invalid geometry. Only Point, LineString, MultiLineString and Polygon are allowed.'
      );
    });

    it('shows an error when a json feature without geometry is selected', async function () {
      let blob = new Blob([testFeatureWithoutGeometryPolygon], {
        type: 'application/json',
      });
      blob.name = 'file.json';

      await triggerEvent('input[type=file]', 'change', { files: [blob] });
      await waitUntil(() => this.value !== '');

      expect(this.value).to.equal(null);
      expect(
        this.element.querySelector('.invalid-feedback').textContent.trim()
      ).to.equal(
        'The file contains an invalid geometry. Only Point, LineString, MultiLineString and Polygon are allowed.'
      );
    });

    it('shows an error when a text file is selected', async function () {
      let blob = new Blob([testJsonMultiPolygon], { type: 'application/json' });

      blob.name = 'file.txt';

      await triggerEvent('input[type=file]', 'change', { files: [blob] });
      await waitUntil(() => this.value !== '');

      expect(this.value).to.equal(null);
      expect(
        this.element.querySelector('.invalid-feedback').textContent.trim()
      ).to.equal('You provided an unsupported file type.');
    });
  });
});
