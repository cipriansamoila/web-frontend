import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/image', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Input::Image />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the buttons when the picture is set', async function() {
    this.set("picture", {picture: ""});
    await render(hbs`<Input::Image @picture={{this.picture}}/>`);

    expect(this.element.querySelector(".btn-delete")).to.exist;
    expect(this.element.querySelector(".btn-rotate")).to.exist;
    expect(this.element.querySelector(".btn-enable-360")).to.exist;
  });

  it('does not render the buttons when the picture is set and is disabled', async function() {
    this.set("picture", {picture: ""});
    await render(hbs`<Input::Image @disabled={{true}} @picture={{this.picture}}/>`);

    expect(this.element.querySelector(".btn-delete")).not.to.exist;
    expect(this.element.querySelector(".btn-rotate")).not.to.exist;
    expect(this.element.querySelector(".btn-enable-360")).not.to.exist;
  });
});
