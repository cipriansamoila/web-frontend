import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/manage/mask', function() {
  setupRenderingTest();

  describe("in view mode", function() {
    it('renders', async function() {
      await render(hbs`<Input::Manage::Mask @title="title" />`);
      expect(this.element.textContent.trim()).to.contain('Features belonging to this map will not be masked.');
    });

    it('renders the map name when it is enabled', async function() {
      this.set("value", {
        isEnabled: true,
        map: {
          name: "custom map"
        }
      });

      await render(hbs`<Input::Manage::Mask @title="title" @value={{this.value}}/>`);
      expect(this.element.textContent.trim()).to.contain('Features belonging to this map will be masked with features from `custom map`.');
    });
  });

  describe("in edit mode", function() {
    it('renders a disabled switch when the mask is not enabled', async function() {
      await render(hbs`<Input::Manage::Mask @title="title" @isMainValue={{true}} @editablePanel="title" />`);

      expect(this.element.querySelector(".enable-map")).exist;
      expect(this.element.querySelector(".enable-map").checked).to.equal(false);
    });

    it('renders an enabled switch when the mask is enabled', async function() {
      this.set("value", {
        isEnabled: true,
        map: {
          name: "custom map"
        }
      });

      await render(hbs`<Input::Manage::Mask @value={{this.value}} @title="title" @isMainValue={{true}} @editablePanel="title" />`);

      expect(this.element.querySelector(".enable-map")).exist;
      expect(this.element.querySelector(".enable-map").checked).to.equal(true);
    });

    it('allows choosing a new map', async function() {
      this.set("value", {
        isEnabled: true,
        map: {
          id: "map-1",
          name: "custom map"
        }
      });

      this.set("maps", [
        {
          id: "map-1",
          name: "map 1"
        }, {
          id: "map-2",
          name: "map 2"
        }
      ]);

      await render(hbs`<Input::Manage::Mask @maps={{this.maps}} @value={{this.value}} @title="title" @isMainValue={{true}} @editablePanel="title" />`);

      expect(this.element.querySelector(".map-select").value).to.equal("map-1");

      this.element.querySelector("select").value = "map-2";
      await triggerEvent('select', 'change');

      expect(this.element.querySelector(".map-select").value).to.equal("map-2");
    });

    it('allows enabling a mask', async function() {
      this.set("value", { });

      this.set("maps", [
        {
          id: "map-1",
          name: "map 1"
        }, {
          id: "map-2",
          name: "map 2"
        }
      ]);

      let value;
      this.set("save", (v) => {
        value = v;
      });

      await render(hbs`<Input::Manage::Mask @maps={{this.maps}} @onSave={{this.save}} @value={{this.value}} @title="title" @isMainValue={{true}} @editablePanel="title" />`);

      await click(".enable-map");
      this.element.querySelector("select").value = "map-2";
      await triggerEvent('select', 'change');

      await click(".btn-submit");

      expect(value.isEnabled).to.equal(true);
      expect(value.mapId).to.equal("map-2");
    });
  });
});
