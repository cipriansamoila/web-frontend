import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/manage/select-with-visibility', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Input::Manage::SelectWithVisibility />`);
    expect(this.element.textContent.trim()).to.equal('');

    expect(this.element.querySelector(".btn-show-all-teams")).to.exist;
    expect(this.element.querySelector("select")).to.exist;
  });

  it('renders the provided list', async function() {
    this.set("list", ["1", "2", "3"]);

    await render(hbs`<Input::Manage::SelectWithVisibility @list={{this.list}} @value="1"/>`);

    expect(this.element.querySelectorAll("select option").length).to.equal(3);
    expect(this.element.querySelectorAll("select option")[0].textContent.trim()).to.equal("1");
    expect(this.element.querySelectorAll("select option")[1].textContent.trim()).to.equal("2");
    expect(this.element.querySelectorAll("select option")[2].textContent.trim()).to.equal("3");
    expect(this.element.querySelector("select").value).to.equal("1");
  });

  it('renders the eye icon when areAllShowing is true', async function() {
    await render(hbs`<Input::Manage::SelectWithVisibility @areAllShowing={{true}}/>`);

    expect(this.element.querySelector(".btn-show-all-teams .fa-eye")).to.exist;
    expect(this.element.querySelector(".btn-show-all-teams .fa-eye-slash")).not.to.exist;
  });

  it('renders the eye icon when areAllShowing is false', async function() {
    await render(hbs`<Input::Manage::SelectWithVisibility @areAllShowing={{false}}/>`);

    expect(this.element.querySelector(".btn-show-all-teams .fa-eye")).not.to.exist;
    expect(this.element.querySelector(".btn-show-all-teams .fa-eye-slash")).to.exist;
  });

  it('triggers the toggleAll action with false when areAllShowing is true', async function() {
    let result;

    this.set("action", (newValue) => {
      result = newValue;
    });

    await render(hbs`<Input::Manage::SelectWithVisibility @areAllShowing={{true}} @toggleAll={{this.action}}/>`);

    await click(".btn-show-all-teams");

    expect(result).to.equal(false);
  });

  it('triggers the toggleAll action with true when areAllShowing is false', async function() {
    let result;

    this.set("action", (newValue) => {
      result = newValue;
    });

    await render(hbs`<Input::Manage::SelectWithVisibility @areAllShowing={{false}} @toggleAll={{this.action}}/>`);

    await click(".btn-show-all-teams");

    expect(result).to.equal(true);
  });
});
