import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, fillIn, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/manage/license', function() {
  setupRenderingTest();

  it('renders the name and url when they are set', async function() {
    this.set("value", {
      name: "some license",
      url: "some url"
    });

    await render(hbs`<Input::Manage::License @isMainValue={{true}} @title="license" @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.contain("some license");
    expect(this.element.textContent.trim()).to.contain("-");
    expect(this.element.querySelector("a").textContent.trim()).to.contain("some url");
  });

  it('renders the name when url is not set', async function() {
    this.set("value", {
      name: "some license"
    });

    await render(hbs`<Input::Manage::License @isMainValue={{true}} @title="license" @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.contain("some license");
    expect(this.element.textContent.trim()).not.to.contain("-");
    expect(this.element.querySelector("a")).not.to.exist;
  });

  it('renders the url and "unknown" when the name is not set', async function() {
    this.set("value", {
      url: "some url"
    });

    await render(hbs`<Input::Manage::License @isMainValue={{true}} @title="license" @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.contain("unknown");
    expect(this.element.textContent.trim()).to.contain("-");
    expect(this.element.querySelector("a").textContent.trim()).to.contain("some url");
  });

  describe("in edit mode", function() {
    it("should fill the values in the input fields", async function() {
      this.set("value", {
        name: "some license",
        url: "some url"
      });

      await render(hbs`<Input::Manage::License @isMainValue={{true}} @editablePanel="license" @title="license" @value={{this.value}} />`);

      expect(this.element.querySelector(".input-license-name").value).to.equal("some license");
      expect(this.element.querySelector(".input-license-url").value).to.equal("some url");
    });

    it("should save the values on click on save field", async function() {
      this.set("value", {
        name: "some license",
        url: "some url"
      });

      let savedValue;

      this.set("save", (val) => {
        savedValue = val;
      })

      await render(hbs`<Input::Manage::License @onSave={{this.save}} @isMainValue={{true}} @editablePanel="license" @title="license" @value={{this.value}} />`);

      await fillIn(".input-license-name", "new name");
      await fillIn(".input-license-url", "new url");
      await click(".btn-submit");

      expect(savedValue).to.deep.equal({
        name: "new name",
        url: "new url"
      });
    });
  });
});
