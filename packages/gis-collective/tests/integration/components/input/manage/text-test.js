import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, fillIn, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/manage/text', function() {
  setupRenderingTest();

  describe("in the view mode", function() {
    it('renders the title', async function() {
      await render(hbs`<Input::Manage::Text @title="test" @isMainValue={{true}}/>`);
      expect(this.element.querySelector(".title").textContent.trim()).to.equal('test');
    });

    it('renders the value', async function() {
      await render(hbs`<Input::Manage::Text @title="test" @isMainValue={{true}} @value="the test value"/>`);
      expect(this.element.querySelector("p").textContent.trim()).to.equal('the test value');
    });

    it("renders an error message when the value is not set and the emptyError exists", async function() {
      await render(hbs`<Input::Manage::Text @title="test" @isMainValue={{true}} @emptyError="the error message"/>`);
      expect(this.element.querySelector(".alert").textContent.trim()).to.equal('the error message');
    });
  });

  describe("in edit mode", function() {
    it("should render an input text with the value", async function() {
      await render(hbs`<Input::Manage::Text @title="test" @editablePanel="test" @isMainValue={{true}} @value="the test value"/>`);
      expect(this.element.querySelector("input").value).to.equal('the test value');
    });

    it("should save a new value", async function() {
      let value;

      this.set("save", (a) => {
        value = a;
      });

      await render(hbs`<Input::Manage::Text @title="test" @editablePanel="test" @isMainValue={{true}} @value="the test value" @onSave={{this.save}}/>`);
      await fillIn('input', 'hello world');

      await click(".btn-submit");

      expect(value).to.equal("hello world");
    });
  });
});
