import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/manage/date', function() {
  setupRenderingTest();

  describe("in view mode", function() {
    it('renders "not set" by default', async function() {
      await render(hbs`<Input::Manage::Date @title="title"/>`);
      expect(this.element.querySelector(".value").textContent.trim()).to.equal('not set');
    });

    it('renders the date when it is set', async function() {
      this.set("value", new Date(Date.parse("2019-01-01T00:00:00")));

      await render(hbs`<Input::Manage::Date @title="title" @value={{this.value}}/>`);
      expect(this.element.querySelector(".value").textContent.trim()).to.equal('Tue Jan 01 2019');
    });
  });

  describe("in edit mode", function() {
    it('can disable the value', async function() {
      this.set("value", new Date(Date.parse("2019-01-01T00:00:00")));

      let value;
      this.set("save", (a) => {
        value = a;
      });

      await render(hbs`<Input::Manage::Date @title="title" @isMainValue={{true}} @onSave={{this.save}} @value={{this.value}} @editablePanel="title"/>`);
      await click("input[type=checkbox]");
      await click(".btn-submit");

      expect(value.getFullYear()).to.equal(0);
    });

    it('changes the year', async function() {
      this.set("value", new Date(Date.parse("2019-01-01T00:00:00")));

      let value;
      this.set("save", (a) => {
        value = a;
      });

      await render(hbs`<Input::Manage::Date @title="title" @isMainValue={{true}} @onSave={{this.save}} @value={{this.value}} @editablePanel="title"/>`);

      this.element.querySelector(".year").value = 2012;
      await triggerEvent(".year", "change");

      await click(".btn-submit");

      expect(value.getFullYear()).to.equal(2012);
    });

    it('resets the year after cancel is pressed', async function() {
      this.set("value", new Date(Date.parse("2019-01-01T00:00:00")));

      await render(hbs`<Input::Manage::Date @title="title" @isMainValue={{true}} @onSave={{this.save}} @value={{this.value}} @editablePanel="title"/>`);

      this.element.querySelector(".year").value = 2012;
      await triggerEvent(".year", "change");

      await click(".btn-cancel");
      expect(this.element.querySelector(".year").value).to.equal("2019");
    });
  });
});
