import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/manage/map-layer-list', function() {
  setupRenderingTest();

  describe("in view mode", function() {
    it('renders an alert message when a value is not set', async function() {
      await render(hbs`<Input::Manage::MapLayerList @title="test" @isMainValue={{true}} />`);
      expect(this.element.querySelector(".alert-danger").textContent.trim()).to.contain('There are no layers set.');
    });

    it('renders a list of layers', async function() {
      this.set("value", [
        { type: "Open Street Map", options: {} },
        { type: "Stamen", options: {} }
      ]);

      await render(hbs`<Input::Manage::MapLayerList @values={{this.value}} @title="test" @isMainValue={{true}} />`);
      expect(this.element.querySelector(".alert-danger")).not.to.exist;

      const values = this.element.querySelectorAll("li");

      expect(values[0].textContent.trim()).to.equal("Stamen (topmost)");
      expect(values[1].textContent.trim()).to.equal("Open Street Map");
    });
  });

  describe("in edit mode", function() {
    it("should show and add layer button", async function() {
      let value;

      this.set("onSave", (newValue) => {
        value = newValue;
      })

      await render(hbs`<Input::Manage::MapLayerList @onSave={{this.onSave}} @editablePanel="test" @title="test" @isMainValue={{true}} />`);

      expect(this.element.querySelector(".input-map-layer")).not.to.exist;

      await click(".btn-add-layer");

      expect(this.element.querySelector(".input-map-layer")).to.exist;

      this.element.querySelector(".select-layer-type").value = "Open Street Map";
      await triggerEvent('.select-layer-type', 'change');

      await click(".btn-submit");

      expect(value).to.deep.equal([{
        type: "Open Street Map",
        options: {}
      }]);
    });

    it("should delete a layer from the list", async function() {
      let value;

      this.set("onSave", (newValue) => {
        value = newValue;
      })

      this.set("values", [{
        type: "Open Street Maps",
        options: {}
      }]);

      await render(hbs`<Input::Manage::MapLayerList @values={{this.values}} @onSave={{this.onSave}} @editablePanel="test" @title="test" @isMainValue={{true}} />`);
      await click(".btn-delete");

      expect(this.element.querySelector(".input-map-layer")).not.to.exist;

      await click(".btn-submit");

      expect(value).to.deep.equal([]);
    });

    it("should move the layer up in the list", async function() {
      let value;

      this.set("onSave", (newValue) => {
        value = newValue;
      })

      this.set("values", [
        { type: "Open Street Maps", options: {} },
        { type: "Stamen", options: {} }
      ]);

      await render(hbs`<Input::Manage::MapLayerList @values={{this.values}} @onSave={{this.onSave}} @editablePanel="test" @title="test" @isMainValue={{true}} />`);

      await click(this.element.querySelectorAll(".btn-move-up")[1]);

      await click(".btn-submit");

      expect(value).to.deep.equal([
        { type: 'Stamen', options: {} },
        { type: 'Open Street Maps', options: {} }
      ]);
    });

    it("should move the layer down in the list", async function() {
      let value;

      this.set("onSave", (newValue) => {
        value = newValue;
      })

      this.set("values", [
        { type: "Open Street Maps", options: {} },
        { type: "Stamen", options: {} }
      ]);

      await render(hbs`<Input::Manage::MapLayerList @values={{this.values}} @onSave={{this.onSave}} @editablePanel="test" @title="test" @isMainValue={{true}} />`);

      await click(this.element.querySelectorAll(".btn-move-down")[0]);

      await click(".btn-submit");

      expect(value).to.deep.equal([
        { type: 'Stamen', options: {} },
        { type: 'Open Street Maps', options: {} }
      ]);
    });

    it("should update the layer options", async function() {
      let value;

      this.set("onSave", (newValue) => {
        value = newValue;
      })

      this.set("values", [
        { type: "Stamen", options: {} }
      ]);

      await render(hbs`<Input::Manage::MapLayerList @values={{this.values}} @onSave={{this.onSave}} @editablePanel="test" @title="test" @isMainValue={{true}} />`);

      await click(".btn-options .btn");
      await click(".btn-submit");

      expect(value).to.deep.equal([
        { type: 'Stamen', options: { layer: 'toner' } }
      ]);
    });
  });
});
