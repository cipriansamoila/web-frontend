import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';
import { setupIntl } from 'ember-intl/test-support';

describe('Integration | Component | input/manage/area', function() {
  let hooks = setupRenderingTest();
  setupIntl(hooks);

  it('renders', async function() {
    await render(hbs`<Input::Manage::Area />`);
    expect(this.element.textContent.trim()).to.contain('');
  });
});
