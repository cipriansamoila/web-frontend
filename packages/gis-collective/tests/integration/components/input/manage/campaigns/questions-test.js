import { expect } from 'chai';
import { describe, it, before } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, fillIn, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../../../helpers/test-server';

describe('Integration | Component | input/manage/campaigns/questions', function () {
  setupRenderingTest();

  let icon1;
  let icon2;

  before(function () {
    let server = new TestServer();
    icon1 = server.testData.create.icon('1');
    icon2 = server.testData.create.icon('2');
  });

  describe('view mode', function () {
    it('renders the default values', async function () {
      await render(
        hbs`<Input::Manage::Campaigns::Questions @title="questions" />`
      );

      expect(
        this.element.querySelector('.name-label').textContent.trim()
      ).to.equal('Disabled');
      expect(
        this.element.querySelector('.description-label').textContent.trim()
      ).to.equal('Disabled');
      expect(
        this.element.querySelector('.icons-label').textContent.trim()
      ).to.equal('Disabled');
    });

    it('renders custom values', async function () {
      this.set('value', {
        showNameQuestion: true,
        nameLabel: 'name label',
        showDescriptionQuestion: true,
        descriptionLabel: 'description label',
        iconsLabel: 'icons label',
      });

      this.set('icons', [icon1, icon2]);

      await render(
        hbs`<Input::Manage::Campaigns::Questions @title="settings" @value={{this.value}} @icons={{this.icons}} />`
      );

      expect(
        this.element.querySelector('.name-label').textContent.trim()
      ).to.equal('name label');
      expect(
        this.element.querySelector('.description-label').textContent.trim()
      ).to.equal('description label');
      expect(
        this.element.querySelector('.icons-label').textContent.trim()
      ).to.equal('icons label');
    });
  });

  describe('edit mode', function () {
    it('renders the default values', async function () {
      await render(
        hbs`<Input::Manage::Campaigns::Questions @title="settings" @editablePanel="settings" />`
      );

      expect(
        this.element.querySelector('.show-name-question').checked
      ).to.equal(false);
      expect(this.element.querySelector('.name-label').value).to.equal('');
      expect(this.element.querySelector('.name-label').disabled).to.equal(true);

      expect(
        this.element.querySelector('.show-description-question').checked
      ).to.equal(false);
      expect(this.element.querySelector('.description-label').value).to.equal(
        ''
      );
      expect(
        this.element.querySelector('.description-label').disabled
      ).to.equal(true);

      expect(this.element.querySelector('.icons-label').value).to.equal('');

      expect(this.element.querySelectorAll('.icons-order img').length).to.equal(
        0
      );
    });

    it('renders the custom values', async function () {
      this.set('icons', [icon1, icon2]);
      this.set('value', {
        showNameQuestion: true,
        nameLabel: 'name label',
        showDescriptionQuestion: true,
        descriptionLabel: 'description label',
        iconsLabel: 'icons label',
      });
      await render(
        hbs`<Input::Manage::Campaigns::Questions @icons={{this.icons}} @value={{this.value}} @title="settings" @editablePanel="settings" />`
      );

      expect(
        this.element.querySelector('.show-name-question').checked
      ).to.equal(true);
      expect(this.element.querySelector('.name-label').value).to.equal(
        'name label'
      );
      expect(this.element.querySelector('.name-label').disabled).to.equal(
        false
      );

      expect(
        this.element.querySelector('.show-description-question').checked
      ).to.equal(true);
      expect(this.element.querySelector('.description-label').value).to.equal(
        'description label'
      );
      expect(
        this.element.querySelector('.description-label').disabled
      ).to.equal(false);

      expect(this.element.querySelector('.icons-label').value).to.equal(
        'icons label'
      );
      expect(this.element.querySelectorAll('.icons-order img').length).to.equal(
        2
      );
    });

    it('can change the default values', async function () {
      let value;
      let icons;

      this.set('save', (v, i) => {
        value = v;
        icons = i;
      });

      await render(
        hbs`<Input::Manage::Campaigns::Questions @availableIcons={{this.icons}} @onSave={{this.save}} @title="settings" @editablePanel="settings" />`
      );

      await click('.show-name-question');
      await fillIn('.name-label', 'value-2');

      await click('.show-description-question');
      await fillIn('.description-label', 'value-3');

      await fillIn('.icons-label', 'value-4');

      await click('.btn-submit');

      expect(value).to.deep.equal({
        showNameQuestion: true,
        nameLabel: 'value-2',
        showDescriptionQuestion: true,
        descriptionLabel: 'value-3',
        iconsLabel: 'value-4',
      });
      expect(icons).to.deep.equal([]);
    });

    it('can change the custom values', async function () {
      this.set('icons', [icon1, icon2]);
      let value;
      let icons;
      this.set('value', {
        showNameQuestion: true,
        nameLabel: 'name label',
        showDescriptionQuestion: true,
        descriptionLabel: 'description label',
        iconsLabel: 'icons label',
      });

      this.set('save', (v, i) => {
        value = v;
        icons = i;
      });

      await render(
        hbs`<Input::Manage::Campaigns::Questions @icons={{this.icons}} @value={{this.value}} @onSave={{this.save}} @title="settings" @editablePanel="settings" />`
      );

      await click('.btn-remove-icon');

      await fillIn('.name-label', '');
      await click('.show-name-question');

      await fillIn('.description-label', '');
      await click('.show-description-question');

      await fillIn('.icons-label', '');

      await click('.btn-submit');

      expect(value).to.deep.equal({
        showNameQuestion: false,
        nameLabel: '',
        showDescriptionQuestion: false,
        descriptionLabel: '',
        iconsLabel: '',
      });

      expect(icons).to.deep.equal([icon2]);
    });

    it('resets the values when cancel is pressed', async function () {
      let isCalled;

      this.set('title', 'settings');
      this.set('cancel', () => {
        isCalled = true;
        this.set('title', '');
      });

      await render(
        hbs`<Input::Manage::Campaigns::Questions @onCancel={{this.cancel}} @title={{this.title}} @editablePanel="settings" />`
      );

      await click('.show-name-question');
      await fillIn('.name-label', 'value-2');

      await click('.show-description-question');
      await fillIn('.description-label', 'value-3');

      await fillIn('.icons-label', 'value-4');

      await click('.btn-cancel');

      expect(isCalled).to.equal(true);

      expect(
        this.element.querySelector('.name-label').textContent.trim()
      ).to.equal('Disabled');
      expect(
        this.element.querySelector('.description-label').textContent.trim()
      ).to.equal('Disabled');
      expect(
        this.element.querySelector('.icons-label').textContent.trim()
      ).to.equal('Disabled');
    });
  });
});
