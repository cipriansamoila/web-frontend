import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/manage/visibility', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Input::Manage::Visibility />`);
    expect(this.element.textContent.trim()).to.contain('visibility');
  });

  describe("in view mode", function() {
    it("renders private when the value is not set", async function() {
      await render(hbs`<Input::Manage::Visibility />`);
      expect(this.element.querySelector(".value").textContent.trim()).to.equal('private');
    });

    it("renders public when the value is 1", async function() {
      await render(hbs`<Input::Manage::Visibility @value={{1}} />`);
      expect(this.element.querySelector(".value").textContent.trim()).to.equal('public');
    });

    it("renders pending when the value is -1", async function() {
      await render(hbs`<Input::Manage::Visibility @value={{-1}} />`);
      expect(this.element.querySelector(".value").textContent.trim()).to.equal('pending');
    });

    it("renders private when the value is 0", async function() {
      await render(hbs`<Input::Manage::Visibility @value={{0}} />`);
      expect(this.element.querySelector(".value").textContent.trim()).to.equal('private');
    });
  });

  describe("in edit mode", function() {
    it("renders private when the value is not set", async function() {
      await render(hbs`<Input::Manage::Visibility @editablePanel="visibility" />`);

      expect(this.element.querySelector("select").value).to.equal('0');
    });

    it("renders public when the value is 1", async function() {
      await render(hbs`<Input::Manage::Visibility @value={{1}} @editablePanel="visibility" />`);
      expect(this.element.querySelector("select").value).to.equal('1');
    });

    it("renders pending when the value is -1", async function() {
      await render(hbs`<Input::Manage::Visibility @value={{-1}} @editablePanel="visibility" />`);

      expect(this.element.querySelector("select").value).to.equal('-1');
    });

    it("renders private when the value is 0", async function() {
      await render(hbs`<Input::Manage::Visibility @value={{0}} @editablePanel="visibility" />`);

      expect(this.element.querySelector("select").value).to.equal('0');
    });

    it("triggers onSave when the save button is clicked", async function() {
      let savedValue;
      this.set("save", (value) => {
        savedValue = value;
      });

      await render(hbs`<Input::Manage::Visibility @value={{0}} @onSave={{this.save}} @editablePanel="visibility" />`);

      this.element.querySelector("select").value = "1";
      await triggerEvent('select', 'change');

      await click(".btn-submit");

      expect(savedValue).to.equal(1);
    });
  });
});
