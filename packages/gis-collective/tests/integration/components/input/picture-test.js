import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { tracked } from '@glimmer/tracking';

class MockPicture {
  @tracked _id = "000000000000000000000001"
  @tracked owner = "1234"
  @tracked picture = "http://localhost:9091/pictures/000000000000000000000001/picture"
  @tracked name = ""
  @tracked is360

  get(key) {
    return this[key];
  }
}

describe('Integration | Component | input/picture', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Input::Picture />`);

    expect(this.element.textContent.trim()).to.equal('Click to select an image');
  });

  it('renders the default message when the default image is set', async function() {
    this.set("value", {
      "_id":"000000000000000000000001",
      "owner":"@system",
      "picture":"http://localhost:9091/pictures/000000000000000000000001/picture",
      "name":"default",

      get(key) {
        return this[key];
      }
    });

    await render(hbs`<Input::Picture @value={{this.value}}/>`);

    expect(this.element.textContent.trim()).to.equal('Click to select an image');
  });

  it('renders the 360 button for a custom image', async function() {
    this.set("value", {
      "_id":"000000000000000000000001",
      "owner":"1234",
      "picture":"http://localhost:9091/pictures/000000000000000000000001/picture",
      "name":"",

      get(key) {
        return this[key];
      }
    });

    await render(hbs`<Input::Picture @value={{this.value}}/>`);
    expect(this.element.querySelector(".btn-enable-360")).to.exist;
  });

  it('does not render the 360 button for a default image', async function() {
    this.set("value", {
      "_id":"000000000000000000000001",
      "owner":"@system",
      "picture":"http://localhost:9091/pictures/000000000000000000000001/picture",
      "name":"default",

      get(key) {
        return this[key];
      }
    });

    await render(hbs`<Input::Picture @value={{this.value}}/>`);
    expect(this.element.querySelector(".btn-enable-360")).not.to.exist;
  });

  it('enables the 360 button on click', async function() {
    this.set("value", new MockPicture());

    let newValue = {};
    this.set("change", (index, value) => {
      newValue = value;
    })

    await render(hbs`<Input::Picture @value={{this.value}} @onChange={{this.change}}/>`);
    await click(".btn-enable-360");

    expect(this.element.querySelector(".btn-enable-360.btn-success")).to.exist;
    expect(newValue.is360).to.equal(true);
  });

  it('disables the 360 button on click', async function() {
    let value = new MockPicture();
    value.is360 = true;
    this.set("value", value);

    let newValue = {};
    this.set("change", (index, value) => {
      newValue = value;
    })

    await render(hbs`<Input::Picture @value={{this.value}} @onChange={{this.change}}/>`);
    await click(".btn-enable-360");

    expect(this.element.querySelector(".btn-enable-360.btn-secondary")).to.exist;
    expect(newValue.is360).to.equal(false);
  });
});
