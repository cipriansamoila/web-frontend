import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import {
  render,
  fillIn,
  click,
  triggerKeyEvent,
  waitUntil,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/search', function () {
  setupRenderingTest();

  it('triggers on search action when the button is pressed', async function () {
    let term;

    this.set('search', (t) => {
      term = t;
    });

    await render(hbs`<Input::Search @onSearch={{this.search}}/>`);
    expect(this.element.querySelector('.results')).not.to.exist;

    await fillIn('.input-search', 'some search');
    await click('.btn-search');

    expect(term).to.equal('some search');
  });

  it('triggers on search action when enter is pressed', async function () {
    let term;

    this.set('search', (t) => {
      term = t;
    });

    await render(hbs`<Input::Search @onSearch={{this.search}}/>`);
    await fillIn('.input-search', 'some search');
    await triggerKeyEvent('.input-search', 'keyup', 'Enter');

    expect(term).to.equal('some search');
  });

  it('shows a loading icon while the promise is resolved', async function () {
    let resolve;

    this.set('search', () => {
      return new Promise((r) => {
        resolve = r;
      });
    });

    await render(hbs`<Input::Search @onSearch={{this.search}}/>`);
    await fillIn('.input-search', 'some search');
    await triggerKeyEvent('.input-search', 'keyup', 'Enter');

    expect(this.element.querySelector('.spinner-border')).to.exist;
    await resolve();
    await waitUntil(() => !this.element.querySelector('.spinner-border'));
  });

  it('shows the content when there are results', async function () {
    await render(
      hbs`<Input::Search @hasResults={{true}}>results</Input::Search>`
    );
    await fillIn('.input-search', 'some search');
    await triggerKeyEvent('.input-search', 'keyup', 'Enter');

    expect(this.element.querySelector('.results')).to.exist;
    expect(this.element.querySelector('.results').textContent).to.contain(
      'results'
    );
  });
});
