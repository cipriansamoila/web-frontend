import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent, waitFor, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';
import { A } from '@ember/array';
import { move } from 'ember-drag-sort/utils/trigger';

describe('Integration | Component | input/image-list', function () {
  setupRenderingTest();
  let server;

  beforeEach(function () {
    server = new TestServer();

    this.set('createImage', () => {
      return {
        set(_, val) {
          this.picture = val;
        },
      };
    });
  });

  it('renders the add button when there is no list', async function () {
    await render(hbs`<Input::ImageList />`);

    expect(this.element.querySelector('.btn-image-add')).to.exist;
  });

  it('triggers on change action when a file is selected', async function () {
    this.set('value', []);

    let value;
    this.set('change', (newValue) => {
      value = newValue;
    });

    await render(
      hbs`<Input::ImageList @onChange={{this.change}} @value={{this.value}} @createImage={{this.createImage}}/>`
    );

    await waitFor('.image-list-input');

    const blob = server.testData.create.pngBlob();
    await triggerEvent(".image-list-input[type='file']", 'change', {
      files: [blob],
    });

    expect(value).to.have.length(1);
    expect(value[0].picture).to.startWith(
      'data:image/png;base64,wolQTkcNChoKAAAADUlIRFIAAABkAAAAZAgGAAAAcMOiwpVUAAAACXBIWXMAAAsSAAALE'
    );
  });

  it('can change the order of two items', async function () {
    this.set(
      'value',
      A([
        { picture: 'data:image/png;base64,picture1' },
        { picture: 'data:image/png;base64,picture2' },
      ])
    );

    let value;
    this.set('change', (newValue) => {
      value = newValue;
    });

    await render(
      hbs`<Input::ImageList @onChange={{this.change}} @value={{this.value}} @createImage={{this.createImage}}/>`
    );

    const list = document.querySelector('.dragSortList');
    await move(list, 0, list, 1, false, 'img');

    expect(value).to.have.length(2);
    expect(value[0].picture).to.startWith('data:image/png;base64,picture2');
    expect(value[1].picture).to.startWith('data:image/png;base64,picture1');
  });

  it('can delete an image', async function () {
    this.set(
      'value',
      A([
        { picture: 'data:image/png;base64,picture1' },
        { picture: 'data:image/png;base64,picture2' },
      ])
    );

    let value;
    this.set('change', (newValue) => {
      value = newValue;
    });

    await render(
      hbs`<Input::ImageList @onChange={{this.change}} @value={{this.value}} @createImage={{this.createImage}}/>`
    );

    await click('.btn-delete');

    expect(value).to.have.length(1);
    expect(value[0].picture).to.startWith('data:image/png;base64,picture2');
  });

  it('can rotate an image', async function () {
    this.set(
      'value',
      A([
        {
          picture: 'data:image/png;base64,picture1',
          rotate() {
            this.isRotated = true;
          },
        },
      ])
    );

    let value;
    this.set('change', (newValue) => {
      value = newValue;
    });

    await render(
      hbs`<Input::ImageList @onChange={{this.change}} @value={{this.value}} @createImage={{this.createImage}}/>`
    );

    await click('.btn-rotate');

    expect(value[0].isRotated).to.equal(true);
  });

  it('can set the image as 360', async function () {
    const value = A([
      {
        picture: 'data:image/png;base64,picture1',
        save() {
          this.isSaved = true;
        },
      },
    ]);
    this.set('value', value);

    await render(
      hbs`<Input::ImageList @onChange={{this.change}} @value={{this.value}} @createImage={{this.createImage}}/>`
    );

    await click('.btn-enable-360');

    expect(value[0].is360).to.equal(true);
    expect(value[0].isSaved).to.equal(true);
  });
});
