import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, triggerEvent, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/select-list', function () {
  setupRenderingTest();

  it('renders the empty message when it is set', async function () {
    await render(hbs`
      <Input::SelectList @isEnabled={{true}} @isMainValue={{true}} @title="some title" @emptyMessage="the list is empty"></Input::SelectList>
    `);

    expect(this.element.querySelector('.value').textContent.trim()).to.equal(
      'the list is empty'
    );
  });

  it('renders the name of values passed as argument and the value is enabled', async function () {
    this.set('values', [{ name: 'name1' }, { name: 'name2' }]);
    await render(hbs`
      <Input::SelectList @isEnabled={{true}} @values={{this.values}} @isMainValue={{true}} @title="some title" @editablePanel="title" />
    `);

    expect(this.element.querySelector('ol')).to.exist;

    const elements = this.element.querySelectorAll('ol li');
    expect(elements.length).to.equal(2);
    expect(elements[0].textContent.trim()).to.equal('name1');
    expect(elements[1].textContent.trim()).to.equal('name2');
  });

  it('renders the name of values passed as argument and the value is disabled', async function () {
    this.set('values', [{ name: 'name1' }, { name: 'name2' }]);
    await render(hbs`
      <Input::SelectList @isEnabled={{false}} @emptyMessage="default message" @values={{this.values}} @isMainValue={{true}} @title="some title" @editablePanel="title" />
    `);

    expect(this.element.querySelector('.value').textContent.trim()).to.equal(
      'default message'
    );
  });

  it('renders a list of links when the route is provided', async function () {
    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);
    await render(hbs`
      <Input::SelectList @route="test.route" @list={{this.values}} @isEnabled={{true}} @values={{this.values}} @isMainValue={{true}} @title="title" />
    `);

    const links = this.element.querySelectorAll('a');

    expect(links).to.have.length(2);
    expect(links[0].textContent.trim()).to.equal('name1');
    expect(links[1].textContent.trim()).to.equal('name2');
  });

  describe('in edit mode when it is disabled', function () {
    it('renders a deactivated switch', async function () {
      this.set('values', [
        { id: 1, name: 'name1' },
        { id: 2, name: 'name2' },
      ]);
      await render(hbs`
        <Input::SelectList @list={{this.values}} @isEnabled={{false}} @values={{this.values}} @isMainValue={{true}} @title="title" @editablePanel="title" />
      `);

      expect(
        this.element.querySelector('input[type=checkbox]').checked
      ).to.equal(false);
      expect(this.element.querySelector('ol')).not.to.exist;
    });

    it('renders the list when the switch is activated', async function () {
      this.set('values', [
        { id: 1, name: 'name1' },
        { id: 2, name: 'name2' },
      ]);
      await render(hbs`
        <Input::SelectList @list={{this.values}} @isEnabled={{false}} @values={{this.values}} @isMainValue={{true}} @title="title" @editablePanel="title" />
      `);

      await click(this.element.querySelector('input[type=checkbox]'));
      expect(
        this.element.querySelector('input[type=checkbox]').checked
      ).to.equal(true);
      expect(this.element.querySelector('ol')).to.exist;
    });

    it('triggers the save with an empty list and an disabled state', async function () {
      let title, values, isEnabled;

      this.set('values', [
        { id: 1, name: 'name1' },
        { id: 2, name: 'name2' },
      ]);
      this.set('save', function (a, b, c) {
        title = a;
        values = b;
        isEnabled = c;
      });

      await render(hbs`
        <Input::SelectList @list={{this.values}} @isEnabled={{false}} @values={{this.values}} @isMainValue={{true}} @title="title" @editablePanel="title" @onSave={{this.save}}/>
      `);

      await click('.btn-submit');

      expect(title).to.equal('title');
      expect(values).to.deep.equal([]);
      expect(isEnabled).to.equal(false);
    });

    it('triggers the save with the list and an enabled state when the switch is toggled', async function () {
      let title, values, isEnabled;

      this.set('values', [
        { id: 1, name: 'name1' },
        { id: 2, name: 'name2' },
      ]);
      this.set('save', function (a, b, c) {
        title = a;
        values = b;
        isEnabled = c;
      });

      await render(hbs`
        <Input::SelectList @list={{this.values}} @isEnabled={{false}} @values={{this.values}} @isMainValue={{true}} @title="title" @editablePanel="title" @onSave={{this.save}}/>
      `);

      await click(this.element.querySelector('input[type=checkbox]'));

      await click('.btn-submit');

      expect(title).to.equal('title');
      expect(values).to.deep.equal(this.values);
      expect(isEnabled).to.equal(true);
    });
  });

  describe('when edit mode is enabled', function () {
    it('renders a list of select inputs set to the values', async function () {
      this.set('values', [
        { id: 1, name: 'name1' },
        { id: 2, name: 'name2' },
      ]);
      await render(hbs`
        <Input::SelectList @isEnabled={{true}} @list={{this.values}} @values={{this.values}} @isMainValue={{true}} @title="title" @editablePanel="title" />
      `);

      expect(this.element.querySelector('ol')).to.exist;

      const elements = this.element.querySelectorAll('ol li select');
      expect(elements.length).to.equal(2);
      expect(elements[0].value.trim()).to.equal('1');
      expect(elements[1].value.trim()).to.equal('2');
    });

    it('removes all options and saves them', async function () {
      let title;
      let values;

      this.set('values', [
        { id: 1, name: 'name1' },
        { id: 2, name: 'name2' },
      ]);

      this.set('save', function (a, b) {
        title = a;
        values = b;
      });

      await render(hbs`
        <Input::SelectList @isEnabled={{true}} @list={{this.values}} @values={{this.values}} @isMainValue={{true}} @title="title" @onSave={{this.save}} @editablePanel="title" />
      `);

      expect(this.element.querySelector('ol')).to.exist;

      await click('.btn-delete');
      await click('.btn-delete');

      const elements = this.element.querySelectorAll('ol li select');
      expect(elements.length).to.equal(0);

      await click('.btn-submit');

      expect(title).to.equal('title');
      expect(values).to.deep.equal([]);
    });

    it('can change a value', async function () {
      let title;
      let values;

      this.set('values', [{ id: '1', name: 'name1' }]);
      this.set('list', [
        { id: '1', name: 'name1' },
        { id: '2', name: 'name2' },
      ]);

      this.set('save', function (a, b) {
        title = a;
        values = b;
      });

      await render(hbs`
        <Input::SelectList @isEnabled={{true}} @list={{this.list}} @values={{this.values}} @isMainValue={{true}} @title="title" @onSave={{this.save}} @editablePanel="title" />
      `);

      expect(this.element.querySelector('ol')).to.exist;
      const element = this.element.querySelector('ol li select');
      element.value = '2';
      await triggerEvent('ol li select', 'change');

      await click('.btn-submit');

      expect(title).to.equal('title');
      expect(values).to.deep.equal([{ id: '2', name: 'name2' }]);
    });

    it('can add a value', async function () {
      let title;
      let values;

      this.set('values', []);
      this.set('list', [
        { id: '1', name: 'name1' },
        { id: '2', name: 'name2' },
      ]);

      this.set('save', function (a, b) {
        title = a;
        values = b;
      });

      await render(hbs`
        <Input::SelectList @isEnabled={{true}} @list={{this.list}} @values={{this.values}} @isMainValue={{true}} @title="title" @onSave={{this.save}} @editablePanel="title" />
      `);

      await click('.btn-add-item');
      expect(this.element.querySelector('ol')).to.exist;
      const elements = this.element.querySelectorAll('ol li select');
      expect(elements.length).to.equal(1);

      await click('.btn-submit');

      expect(title).to.equal('title');
      expect(values).to.deep.equal([{ id: '2', name: 'name2' }]);
    });

    it('ignores an invalid value', async function () {
      let title;
      let values;

      this.set('values', []);
      this.set('list', [
        { id: '1', name: 'name1' },
        { id: '2', name: 'name2' },
      ]);

      this.set('save', function (a, b) {
        title = a;
        values = b;
      });

      await render(hbs`
        <Input::SelectList @isEnabled={{true}} @list={{this.list}} @values={{this.values}} @isMainValue={{true}} @title="title" @onSave={{this.save}} @editablePanel="title" />
      `);

      await click('.btn-add-item');
      await waitFor('ol li select');

      this.element.querySelector('ol li select').value = '';
      await triggerEvent('ol li select', 'change');

      await click('.btn-submit');

      expect(title).to.equal('title');
      expect(values).to.deep.equal([]);
    });
  });

  describe('in edit mode when isEnabled is not set', function () {
    it('renders a list of select inputs set to the values', async function () {
      this.set('values', [
        { id: 1, name: 'name1' },
        { id: 2, name: 'name2' },
      ]);
      await render(hbs`
        <Input::SelectList @list={{this.values}} @values={{this.values}} @isMainValue={{true}} @title="title" @editablePanel="title" />
      `);

      expect(this.element.querySelector('ol')).to.exist;

      const elements = this.element.querySelectorAll('ol li select');
      expect(elements.length).to.equal(2);
      expect(elements[0].value.trim()).to.equal('1');
      expect(elements[1].value.trim()).to.equal('2');
    });

    it('removes all options and saves them', async function () {
      let title;
      let values;

      this.set('values', [
        { id: 1, name: 'name1' },
        { id: 2, name: 'name2' },
      ]);

      this.set('save', function (a, b) {
        title = a;
        values = b;
      });

      await render(hbs`
        <Input::SelectList @list={{this.values}} @values={{this.values}} @isMainValue={{true}} @title="title" @onSave={{this.save}} @editablePanel="title" />
      `);

      expect(this.element.querySelector('ol')).to.exist;

      await click('.btn-delete');
      await click('.btn-delete');

      const elements = this.element.querySelectorAll('ol li select');
      expect(elements.length).to.equal(0);

      await click('.btn-submit');

      expect(title).to.equal('title');
      expect(values).to.deep.equal([]);
    });

    it('can change a value', async function () {
      let title;
      let values;

      this.set('values', [{ id: '1', name: 'name1' }]);
      this.set('list', [
        { id: '1', name: 'name1' },
        { id: '2', name: 'name2' },
      ]);

      this.set('save', function (a, b) {
        title = a;
        values = b;
      });

      await render(hbs`
        <Input::SelectList @list={{this.list}} @values={{this.values}} @isMainValue={{true}} @title="title" @onSave={{this.save}} @editablePanel="title" />
      `);

      expect(this.element.querySelector('ol')).to.exist;
      const element = this.element.querySelector('ol li select');
      element.value = '2';
      await triggerEvent('ol li select', 'change');

      await click('.btn-submit');

      expect(title).to.equal('title');
      expect(values).to.deep.equal([{ id: '2', name: 'name2' }]);
    });

    it('can add a value', async function () {
      let title;
      let values;

      this.set('values', []);
      this.set('list', [
        { id: '1', name: 'name1' },
        { id: '2', name: 'name2' },
      ]);

      this.set('save', function (a, b) {
        title = a;
        values = b;
      });

      await render(hbs`
        <Input::SelectList @list={{this.list}} @values={{this.values}} @isMainValue={{true}} @title="title" @onSave={{this.save}} @editablePanel="title" />
      `);

      await click('.btn-add-item');

      expect(this.element.querySelector('ol')).to.exist;
      const elements = this.element.querySelectorAll('ol li select');
      expect(elements.length).to.equal(1);

      await click('.btn-submit');

      expect(title).to.equal('title');
      expect(values).to.deep.equal([{ id: '2', name: 'name2' }]);
    });

    it('ignores an invalid value', async function () {
      let title;
      let values;

      this.set('values', []);
      this.set('list', [
        { id: '1', name: 'name1' },
        { id: '2', name: 'name2' },
      ]);

      this.set('save', function (a, b) {
        title = a;
        values = b;
      });

      await render(hbs`
        <Input::SelectList @list={{this.list}} @values={{this.values}} @isMainValue={{true}} @title="title" @onSave={{this.save}} @editablePanel="title" />
      `);

      await click('.btn-add-item');
      await waitFor('ol li select');

      this.element.querySelector('ol li select').value = '';
      await triggerEvent('ol li select', 'change');

      await click('.btn-submit');

      expect(title).to.equal('title');
      expect(values).to.deep.equal([]);
    });
  });
});
