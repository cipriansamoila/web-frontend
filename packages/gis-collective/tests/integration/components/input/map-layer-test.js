import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, waitFor, typeIn, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/map-layer', function() {
  setupRenderingTest();

  describe('with no parameter', function() {
    it('renders the layer types', async function() {
      await render(hbs`<Input::MapLayer />`);

      const layerTypes = [];
      this.element.querySelectorAll("option").forEach((el) => {
        layerTypes.push(el.textContent.trim());
      });

      expect(layerTypes).to.have.lengthOf(9);
      expect(layerTypes).to.include.members(["Open Street Map", "GISCollective Map", "Stamen", "WMS", "VectorTile"]);
    });

    it('renders with the is-invalid class', async function() {
      await render(hbs`<Input::MapLayer />`);

      const classes = Array.from(this.element.querySelector(".select-layer-type").classList.values());
      expect(classes).to.include("is-invalid");
    });

    it('renders disabled move up and down buttons', async function() {
      await render(hbs`<Input::MapLayer @index=0 @len=1 />`);

      expect(this.element.querySelector(".btn-move-up").disabled).to.eql(true);
      expect(this.element.querySelector(".btn-move-down").disabled).to.eql(true);
    });

    it('should trigger type change action when layer type is changed', async function() {
      var value = "";

      this.set('onChangeType', function(val) {
        value = val;
      });

      await render(hbs`<Input::MapLayer @onChangeType={{this.onChangeType}} />`);

      this.element.querySelector(".select-layer-type").value = "Open Street Map";
      await triggerEvent('.select-layer-type', 'change');

      expect(value).to.eql("Open Street Map");
    });
  });

  describe('with index parameters', function() {
    it('renders the incremented index', async function() {
      await render(hbs`<Input::MapLayer @index=3 />`);
      expect(this.element.querySelector(".btn-index").textContent.trim()).to.eql("4.");
    });

    it('renders the disabled move down button when is the last item', async function() {
      await render(hbs`<Input::MapLayer @index=3 @len=3 />`);

      expect(this.element.querySelector(".btn-move-up").disabled).to.eql(false);
      expect(this.element.querySelector(".btn-move-down").disabled).to.eql(true);
    });

    it('renders the enabled move buttons when is not the last item', async function() {
      await render(hbs`<Input::MapLayer @index=1 @len=3 />`);

      expect(this.element.querySelector(".btn-move-down").disabled).to.eql(false);
      expect(this.element.querySelector(".btn-move-up").disabled).to.eql(false);
    });

    it('can trigger the delete action', async function() {
      let index = 0;
      this.set('onDelete', function(val) {
        index = val;
      });

      await render(hbs`<Input::MapLayer @index=1 @len=3 @onDelete={{this.onDelete}} />`);
      this.element.querySelector(".btn-delete").click();

      expect(index).to.eql(1);
    });

    it('can trigger the move up action', async function() {
      let index = 0;
      this.set('onMoveUp', function(val) {
        index = val;
      });

      await render(hbs`<Input::MapLayer @index=1 @len=3 @onMoveUp={{this.onMoveUp}} />`);
      this.element.querySelector(".btn-move-up").click();

      expect(index).to.eql(1);
    });

    it('can trigger the move down action', async function() {
      let index = 0;
      this.set('onMoveDown', function(val) {
        index = val;
      });

      await render(hbs`<Input::MapLayer @index=1 @len=3 @onMoveDown={{this.onMoveDown}} />`);
      this.element.querySelector(".btn-move-down").click();

      expect(index).to.eql(1);
    });
  });

  describe('with an OSM layer', function() {
    it('sets the layer type in the combo', async function() {
      this.set("layer", {
        type: "Open Street Map",
        options: {}
      });

      await render(hbs`<Input::MapLayer @index=3 @value={{this.layer}} />`);

      expect(this.element.querySelector(".select-layer-type").value).to.eql("Open Street Map");
      expect(this.element.querySelectorAll(".layer-options").length).to.eql(0);
    });

    it('is shown on all zoom levels when the visibility option is missing', async function() {
      this.set("layer", {
        type: "Open Street Map",
        options: {}
      });

      await render(hbs`<Input::MapLayer @index=3 @value={{this.layer}} />`);

      expect(this.element.querySelector(".select-visibility").value).to.equal("true");
    });

    it('should be able to change the visibility type to a range', async function() {
      this.set("layer", {
        type: "Open Street Map",
        options: {}
      });

      let options;

      this.set('onChangeOption', (a, b) => {
        options = b;
        this.set("layer.options", b);
      });

      await render(hbs`<Input::MapLayer @index=3 @value={{this.layer}} @onChangeOption={{this.onChangeOption}} />`);

      this.element.querySelector(".select-visibility").value = "false";
      await triggerEvent('.select-visibility', 'change');

      expect(options).to.deep.equal({
        visibility: [0, 22]
      });
    });

    it('should be able to change the min visibility range', async function() {
      this.set("layer", {
        type: "Open Street Map",
        options: {visibility: [0, 22]}
      });

      let options;

      this.set('onChangeOption', (a, b) => {
        options = b;
        this.set("layer.options", b);
      });

      await render(hbs`<Input::MapLayer @index=3 @value={{this.layer}} @onChangeOption={{this.onChangeOption}} />`);

      this.element.querySelector(".select-min-visibility").value = "10";
      await triggerEvent('.select-min-visibility', 'change');

      expect(options).to.deep.equal({
        visibility: [10, 22]
      });
    });

    it('should be able to change the max visibility range', async function() {
      this.set("layer", {
        type: "Open Street Map",
        options: {visibility: [0, 22]}
      });

      let options;

      this.set('onChangeOption', (a, b) => {
        options = b;
        this.set("layer.options", b);
      });

      await render(hbs`<Input::MapLayer @index=3 @value={{this.layer}} @onChangeOption={{this.onChangeOption}} />`);

      this.element.querySelector(".select-max-visibility").value = "10";
      await triggerEvent('.select-max-visibility', 'change');

      expect(options).to.deep.equal({
        visibility: [0, 10]
      });
    });
  });

  describe('with a Stamen layer', function() {
    it('sets the layer type in the combo and set the properties', async function() {
      this.set("layer1",{
        type: "Stamen",
        options: {}
      });

      await render(hbs`<Input::MapLayer @index=3 @value={{this.layer1}} />`);

      expect(this.element.querySelector(".select-layer-type").value).to.eql("Stamen");
      expect(this.element.querySelectorAll(".layer-options").length).to.eql(1);
    });

    it('triggers the layer value', async function() {
      this.set("layer2", {
        type: "Stamen",
        options: {}
      });

      let index = 0;
      let options;
      this.set('onChangeOption', function(a, b) {
        index = a;
        options = b;
      });

      await render(hbs`<Input::MapLayer @index=3 @value={{this.layer2}} @onChangeOption={{this.onChangeOption}} />`);

      this.element.querySelector(".layer-options .btn-options .btn").click();

      expect(index).to.eql(3);
      expect(options).to.eql({
        layer: "toner"
      });
    });

    it('shows the current value', async function() {
      this.set("layer3", {
        type: "Stamen",
        options: {
          "layer": "toner"
        }
      });

      await render(hbs`<Input::MapLayer @index=3 @value={{this.layer3}} />`);

      const btn = this.element.querySelector(".layer-options .btn-options .btn.btn-success");

      expect(btn).to.not.be.null;
    });
  });

  describe('with a WMS layer', function() {
    it('triggers the layer value', async function() {
      this.set("layer4", {
        type: "WMS",
        options: {}
      });

      let index;
      let options;
      this.set('onChangeOption2', function(a, b) {
        index = a;
        options = b;
      });

      await render(hbs`<Input::MapLayer @index=3 @value={{this.layer4}} @onChangeOption={{this.onChangeOption2}} />`);
      await waitFor(".layer-options input");

      await typeIn(".layer-options input", "http://text.com");
      this.element.querySelector(".layer-options input").blur();
      this.element.querySelector(".layer-options input").blur();

      expect(index).to.eql(3);
      expect(options).to.eql({
        url: "http://text.com"
      });
    });
  });

  describe('with a GIS Collective map layer', function() {
    it('triggers the layer value', async function() {
      this.set("layer", {
        type: "GISCollective Map",
        options: {}
      });

      let options;
      this.set('onChangeOption', function(a, b) {
        options = b;
      });

      this.set('maps', [{
          id: 1,
          name: "test1"
        },{
          id: 2,
          name: "test2"
        }]);

      await render(hbs`<Input::MapLayer @index=3 @value={{this.layer}} @onChangeOption={{this.onChangeOption}} @maps={{this.maps}} />`);


      this.element.querySelector(".select-map-id").value = "2";
      await triggerEvent('.select-map-id', 'change');

      expect(options).to.deep.equal({
        GISCollectiveMap: "2"
      });
    });
  });
});
