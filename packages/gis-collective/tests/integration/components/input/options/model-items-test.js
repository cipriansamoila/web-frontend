import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { click, render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../../helpers/test-server';

describe('Integration | Component | input/options/model-items', function () {
  setupRenderingTest();
  let server;

  before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultTeam('1');
    server.testData.storage.addDefaultTeam('2');

    server.testData.storage.addDefaultCampaign('1');
    server.testData.storage.addDefaultCampaign('2');
    server.testData.storage.addDefaultCampaign('3');
  });

  after(function () {
    server.shutdown();
  });

  it('renders the all option when there is no query', async function () {
    this.set('value', { data: { query: {} } });

    await render(hbs`<Input::Options::ModelItems @value={{this.value}} />`);

    expect(this.element.querySelector('.form-select')).not.to.exist;
  });

  it('allows selecting a team for the campaign model', async function () {
    this.set('value', { data: { query: { team: '' } } });

    let value;
    this.set('onChange', (v) => {
      value = v;
      this.set('value', v);
    });

    await render(
      hbs`<Input::Options::ModelItems @model="campaign" @value={{this.value}} @onChange={{this.onChange}} />`
    );

    this.element.querySelector('.form-select').value = '1';
    await triggerEvent('.form-select', 'change');

    expect(value).to.deep.equal({ query: { team: '1' }, model: 'campaign' });
  });

  it('allows selecting a team for the map model', async function () {
    this.set('value', { data: { query: { team: '' } } });

    let value;
    this.set('onChange', (v) => {
      value = v;
    });

    await render(
      hbs`<Input::Options::ModelItems @model="map" @value={{this.value}} @onChange={{this.onChange}} />`
    );

    this.element.querySelector('.form-select').value = '1';
    await triggerEvent('.form-select', 'change');

    expect(value).to.deep.equal({ query: { team: '1' }, model: 'map' });
  });

  it('allows changing to the `all` value', async function () {
    this.set('value', { data: { query: { team: '1' } } });

    let value;
    this.set('onChange', (v) => {
      value = v;
      this.set('value', v);
    });

    await render(
      hbs`<Input::Options::ModelItems @model="campaign" @value={{this.value}} @onChange={{this.onChange}} />`
    );

    expect(this.element.querySelector('.btn-check-team').checked).to.equal(
      true
    );

    await click('.btn-check-all');

    expect(this.element.querySelector('.form-select')).not.to.exist;

    expect(value).to.deep.equal({ query: {}, model: 'campaign' });
  });

  it('allows changing to the `ids` value', async function () {
    this.set('value', { data: { query: { team: '1' } } });

    let value;
    this.set('onChange', (v) => {
      value = v;
      this.set('value', v);
    });

    await render(
      hbs`<Input::Options::ModelItems @model="campaign" @value={{this.value}} @onChange={{this.onChange}} />`
    );

    await click('.btn-check-ids');
    await click('.btn-add-item');

    expect(value).to.deep.equal({ query: { ids: ['3'] }, model: 'campaign' });
  });
});
