import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/options/container-options', function () {
  setupRenderingTest();

  it('can change the width', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    await render(
      hbs`<Input::Options::ContainerOptions @onChangeOptions={{this.change}}/>`
    );

    expect(this.element.querySelector('.width-select').value).to.equal(
      'default'
    );

    this.element.querySelector('.width-select').value = 'fluid';
    await triggerEvent('.width-select', 'change');

    expect(value).to.deep.equal(['container-fluid']);
  });

  it('can reset the width', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('container', { options: ['container-fluid'] });

    await render(
      hbs`<Input::Options::ContainerOptions @container={{this.container}} @onChangeOptions={{this.change}}/>`
    );

    expect(this.element.querySelector('.width-select').value).to.equal('fluid');

    this.element.querySelector('.width-select').value = 'default';
    await triggerEvent('.width-select', 'change');

    expect(value).to.deep.equal([]);
  });
});
