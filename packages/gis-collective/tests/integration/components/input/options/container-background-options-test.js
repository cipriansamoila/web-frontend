import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/options/container-background-options', function () {
  setupRenderingTest();

  it('renders the current value', async function () {
    this.set('value', {
      backgroundColor: 'primary',
    });
    await render(
      hbs`<Input::Options::ContainerBackgroundOptions @value={{this.value}} />`
    );

    expect(
      this.element.querySelector('.selected-color').textContent.trim()
    ).to.equal('primary');
  });
});
