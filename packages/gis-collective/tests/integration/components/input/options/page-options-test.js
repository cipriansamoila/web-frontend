import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/options/page-options', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<Input::Options::PageOptions />`);
    expect(this.element.textContent.trim()).to.contain('Name');
  });
});
