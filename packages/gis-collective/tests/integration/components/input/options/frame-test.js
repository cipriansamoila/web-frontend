import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/options/frame', function () {
  setupRenderingTest();

  it('renders the content', async function () {
    await render(hbs`
      <Input::Options::Frame>
        template block text
      </Input::Options::Frame>
    `);

    expect(this.element.textContent.trim()).to.equal('template block text');
  });

  it('renders the title', async function () {
    await render(hbs`
      <Input::Options::Frame @title="title">
        template block text
      </Input::Options::Frame>
    `);

    expect(this.element.querySelector('.header').textContent.trim()).to.equal(
      'title'
    );
  });

  it('can toggle the close state', async function () {
    await render(hbs`
      <Input::Options::Frame @title="title">
        template block text
      </Input::Options::Frame>
    `);

    expect(this.element.querySelector('.input-options-frame.closed')).not.to
      .exist;

    await click('.btn-close-frame');
    expect(this.element.querySelector('.input-options-frame.closed')).to.exist;

    await click('.btn-close-frame');
    expect(this.element.querySelector('.input-options-frame.closed')).not.to
      .exist;
  });
});
