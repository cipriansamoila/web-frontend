import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/options/col-options', function () {
  setupRenderingTest();

  it('can set the mobile width', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    await render(
      hbs`<Input::Options::ColOptions @onChangeOptions={{this.change}}/>`
    );

    expect(this.element.querySelector('.mobile-width-select').value).to.equal(
      'default'
    );

    this.element.querySelector('.mobile-width-select').value = '6';
    await triggerEvent('.mobile-width-select', 'change');

    expect(value).to.deep.equal(['col-6']);
  });

  it('can change the mobile width', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('col', { options: ['col-4'] });

    await render(
      hbs`<Input::Options::ColOptions @col={{this.col}} @onChangeOptions={{this.change}}/>`
    );

    expect(this.element.querySelector('.mobile-width-select').value).to.equal(
      '4'
    );

    this.element.querySelector('.mobile-width-select').value = '6';
    await triggerEvent('.mobile-width-select', 'change');

    expect(value).to.deep.equal(['col-6']);
  });

  it('can change the mobile width to the default', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('col', { options: ['col-4'] });

    await render(
      hbs`<Input::Options::ColOptions @col={{this.col}} @onChangeOptions={{this.change}}/>`
    );

    expect(this.element.querySelector('.mobile-width-select').value).to.equal(
      '4'
    );

    this.element.querySelector('.mobile-width-select').value = 'default';
    await triggerEvent('.mobile-width-select', 'change');

    expect(value).to.deep.equal([]);
  });

  it('can change the tablet width', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('col', { options: ['col-md-4'] });

    await render(
      hbs`<Input::Options::ColOptions @col={{this.col}} @onChangeOptions={{this.change}}/>`
    );

    expect(this.element.querySelector('.tablet-width-select').value).to.equal(
      '4'
    );

    this.element.querySelector('.tablet-width-select').value = '6';
    await triggerEvent('.tablet-width-select', 'change');

    expect(value).to.deep.equal(['col-md-6']);
  });

  it('can change the tablet width to the default', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('col', { options: ['col-md-4'] });

    await render(
      hbs`<Input::Options::ColOptions @col={{this.col}} @onChangeOptions={{this.change}}/>`
    );

    expect(this.element.querySelector('.tablet-width-select').value).to.equal(
      '4'
    );

    this.element.querySelector('.tablet-width-select').value = 'default';
    await triggerEvent('.tablet-width-select', 'change');

    expect(value).to.deep.equal([]);
  });

  it('can change the desktop width', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('col', { options: ['col-lg-4'] });

    await render(
      hbs`<Input::Options::ColOptions @col={{this.col}} @onChangeOptions={{this.change}}/>`
    );

    expect(this.element.querySelector('.desktop-width-select').value).to.equal(
      '4'
    );

    this.element.querySelector('.desktop-width-select').value = '6';
    await triggerEvent('.desktop-width-select', 'change');

    expect(value).to.deep.equal(['col-lg-6']);
  });

  it('can change the desktop width to the default', async function () {
    let value;

    this.set('change', (v) => {
      value = v;
    });

    this.set('col', { options: ['col-lg-4'] });

    await render(
      hbs`<Input::Options::ColOptions @col={{this.col}} @onChangeOptions={{this.change}}/>`
    );

    expect(this.element.querySelector('.desktop-width-select').value).to.equal(
      '4'
    );

    this.element.querySelector('.desktop-width-select').value = 'default';
    await triggerEvent('.desktop-width-select', 'change');

    expect(value).to.deep.equal([]);
  });
});
