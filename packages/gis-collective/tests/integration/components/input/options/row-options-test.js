import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/options/row-options', function () {
  setupRenderingTest();

  describe('mobile', function () {
    describe('when there are no options', function () {
      let value;

      beforeEach(async function () {
        value = undefined;
        this.set('change', (v) => {
          value = v;
        });

        await render(
          hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`
        );
      });

      it('can set the height', async function () {
        expect(this.element.querySelector('.mobile-min-height').value).to.equal(
          'default'
        );

        this.element.querySelector('.mobile-min-height').value = '30';
        await triggerEvent('.mobile-min-height', 'change');

        expect(value).to.deep.equal(['min-vh-30']);
      });

      it('can set a vertical gutter', async function () {
        expect(
          this.element.querySelector('.mobile-vertical-gutters').value
        ).to.equal('default');

        this.element.querySelector('.mobile-vertical-gutters').value = '3';
        await triggerEvent('.mobile-vertical-gutters', 'change');

        expect(value).to.deep.equal(['gy-3']);
      });

      it('can set a horizontal gutter', async function () {
        expect(
          this.element.querySelector('.mobile-horizontal-gutters').value
        ).to.equal('default');

        this.element.querySelector('.mobile-horizontal-gutters').value = '3';
        await triggerEvent('.mobile-horizontal-gutters', 'change');

        expect(value).to.deep.equal(['gx-3']);
      });

      it('can set the justify content', async function () {
        await render(
          hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`
        );

        expect(
          this.element.querySelector('.mobile-justify-content').value
        ).to.equal('default');

        this.element.querySelector('.mobile-justify-content').value = 'center';
        await triggerEvent('.mobile-justify-content', 'change');

        expect(value).to.deep.equal(['justify-content-center']);
      });

      it('can set the item alignment', async function () {
        await render(
          hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`
        );

        expect(
          this.element.querySelector('.mobile-align-items').value
        ).to.equal('default');

        this.element.querySelector('.mobile-align-items').value = 'center';
        await triggerEvent('.mobile-align-items', 'change');

        expect(value).to.deep.equal(['align-items-center']);
      });

      it('can set all options', async function () {
        await render(
          hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`
        );

        expect(
          this.element.querySelector('.mobile-align-items').value
        ).to.equal('default');

        this.element.querySelector('.mobile-vertical-gutters').value = '3';
        await triggerEvent('.mobile-vertical-gutters', 'change');
        this.element.querySelector('.mobile-horizontal-gutters').value = '2';
        await triggerEvent('.mobile-horizontal-gutters', 'change');
        this.element.querySelector('.mobile-justify-content').value = 'end';
        await triggerEvent('.mobile-justify-content', 'change');
        this.element.querySelector('.mobile-align-items').value = 'center';
        await triggerEvent('.mobile-align-items', 'change');

        expect(value).to.deep.equal([
          'gy-3',
          'gx-2',
          'justify-content-end',
          'align-items-center',
        ]);
      });
    });

    describe('when there are options', function () {
      let value;

      beforeEach(async function () {
        value = undefined;
        this.set('change', (v) => {
          value = v;
        });

        this.set('row', {
          options: [
            'gy-3',
            'gx-3',
            'justify-content-center',
            'align-items-center',
          ],
        });
        await render(
          hbs`<Input::Options::RowOptions @row={{this.row}} @onChangeOptions={{this.change}}/>`
        );
      });

      it('can reset the vertical gutter', async function () {
        expect(
          this.element.querySelector('.mobile-vertical-gutters').value
        ).to.equal('3');

        this.element.querySelector('.mobile-vertical-gutters').value =
          'default';
        await triggerEvent('.mobile-vertical-gutters', 'change');

        expect(value).to.deep.equal([
          'gx-3',
          'justify-content-center',
          'align-items-center',
        ]);
      });

      it('can reset the horizontal gutter', async function () {
        expect(
          this.element.querySelector('.mobile-horizontal-gutters').value
        ).to.equal('3');

        this.element.querySelector('.mobile-horizontal-gutters').value =
          'default';
        await triggerEvent('.mobile-horizontal-gutters', 'change');

        expect(value).to.deep.equal([
          'gy-3',
          'justify-content-center',
          'align-items-center',
        ]);
      });

      it('can reset the justify content', async function () {
        expect(
          this.element.querySelector('.mobile-justify-content').value
        ).to.equal('center');

        this.element.querySelector('.mobile-justify-content').value = 'default';
        await triggerEvent('.mobile-justify-content', 'change');

        expect(value).to.deep.equal(['gy-3', 'gx-3', 'align-items-center']);
      });

      it('can reset the align items', async function () {
        expect(
          this.element.querySelector('.mobile-align-items').value
        ).to.equal('center');

        this.element.querySelector('.mobile-align-items').value = 'default';
        await triggerEvent('.mobile-align-items', 'change');

        expect(value).to.deep.equal(['gy-3', 'gx-3', 'justify-content-center']);
      });
    });
  });

  describe('tablet', function () {
    describe('when there are no options', function () {
      let value;

      beforeEach(async function () {
        value = undefined;
        this.set('change', (v) => {
          value = v;
        });

        await render(
          hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`
        );
      });

      it('can set a vertical gutter', async function () {
        expect(
          this.element.querySelector('.tablet-vertical-gutters').value
        ).to.equal('default');

        this.element.querySelector('.tablet-vertical-gutters').value = '3';
        await triggerEvent('.tablet-vertical-gutters', 'change');

        expect(value).to.deep.equal(['gy-md-3']);
      });

      it('can set a horizontal gutter', async function () {
        expect(
          this.element.querySelector('.tablet-horizontal-gutters').value
        ).to.equal('default');

        this.element.querySelector('.tablet-horizontal-gutters').value = '3';
        await triggerEvent('.tablet-horizontal-gutters', 'change');

        expect(value).to.deep.equal(['gx-md-3']);
      });

      it('can set the justify content', async function () {
        await render(
          hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`
        );

        expect(
          this.element.querySelector('.tablet-justify-content').value
        ).to.equal('default');

        this.element.querySelector('.tablet-justify-content').value = 'center';
        await triggerEvent('.tablet-justify-content', 'change');

        expect(value).to.deep.equal(['justify-content-md-center']);
      });

      it('can set the item alignment', async function () {
        await render(
          hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`
        );

        expect(
          this.element.querySelector('.tablet-align-items').value
        ).to.equal('default');

        this.element.querySelector('.tablet-align-items').value = 'center';
        await triggerEvent('.tablet-align-items', 'change');

        expect(value).to.deep.equal(['align-items-md-center']);
      });

      it('can set all options', async function () {
        await render(
          hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`
        );

        this.element.querySelector('.tablet-vertical-gutters').value = '3';
        await triggerEvent('.tablet-vertical-gutters', 'change');
        this.element.querySelector('.tablet-horizontal-gutters').value = '2';
        await triggerEvent('.tablet-horizontal-gutters', 'change');
        this.element.querySelector('.tablet-justify-content').value = 'end';
        await triggerEvent('.tablet-justify-content', 'change');
        this.element.querySelector('.tablet-align-items').value = 'center';
        await triggerEvent('.tablet-align-items', 'change');

        expect(value).to.deep.equal([
          'gy-md-3',
          'gx-md-2',
          'justify-content-md-end',
          'align-items-md-center',
        ]);
      });
    });

    describe('when there are options', function () {
      let value;

      beforeEach(async function () {
        value = undefined;
        this.set('change', (v) => {
          value = v;
        });

        this.set('row', {
          options: [
            'gy-md-3',
            'gx-md-3',
            'justify-content-md-center',
            'align-items-md-center',
          ],
        });
        await render(
          hbs`<Input::Options::RowOptions @row={{this.row}} @onChangeOptions={{this.change}}/>`
        );
      });

      it('can reset the vertical gutter', async function () {
        expect(
          this.element.querySelector('.tablet-vertical-gutters').value
        ).to.equal('3');

        this.element.querySelector('.tablet-vertical-gutters').value =
          'default';
        await triggerEvent('.tablet-vertical-gutters', 'change');

        expect(value).to.deep.equal([
          'gx-md-3',
          'justify-content-md-center',
          'align-items-md-center',
        ]);
      });

      it('can reset the horizontal gutter', async function () {
        expect(
          this.element.querySelector('.tablet-horizontal-gutters').value
        ).to.equal('3');

        this.element.querySelector('.tablet-horizontal-gutters').value =
          'default';
        await triggerEvent('.tablet-horizontal-gutters', 'change');

        expect(value).to.deep.equal([
          'gy-md-3',
          'justify-content-md-center',
          'align-items-md-center',
        ]);
      });

      it('can reset the justify content', async function () {
        expect(
          this.element.querySelector('.tablet-justify-content').value
        ).to.equal('center');

        this.element.querySelector('.tablet-justify-content').value = 'default';
        await triggerEvent('.tablet-justify-content', 'change');

        expect(value).to.deep.equal([
          'gy-md-3',
          'gx-md-3',
          'align-items-md-center',
        ]);
      });

      it('can reset the align items', async function () {
        expect(
          this.element.querySelector('.tablet-align-items').value
        ).to.equal('center');

        this.element.querySelector('.tablet-align-items').value = 'default';
        await triggerEvent('.tablet-align-items', 'change');

        expect(value).to.deep.equal([
          'gy-md-3',
          'gx-md-3',
          'justify-content-md-center',
        ]);
      });
    });
  });

  describe('desktop', function () {
    describe('when there are no options', function () {
      let value;

      beforeEach(async function () {
        value = undefined;
        this.set('change', (v) => {
          value = v;
        });

        await render(
          hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`
        );
      });

      it('can set a vertical gutter', async function () {
        expect(
          this.element.querySelector('.desktop-vertical-gutters').value
        ).to.equal('default');

        this.element.querySelector('.desktop-vertical-gutters').value = '3';
        await triggerEvent('.desktop-vertical-gutters', 'change');

        expect(value).to.deep.equal(['gy-lg-3']);
      });

      it('can set a horizontal gutter', async function () {
        expect(
          this.element.querySelector('.desktop-horizontal-gutters').value
        ).to.equal('default');

        this.element.querySelector('.desktop-horizontal-gutters').value = '3';
        await triggerEvent('.desktop-horizontal-gutters', 'change');

        expect(value).to.deep.equal(['gx-lg-3']);
      });

      it('can set the justify content', async function () {
        await render(
          hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`
        );

        expect(
          this.element.querySelector('.desktop-justify-content').value
        ).to.equal('default');

        this.element.querySelector('.desktop-justify-content').value = 'center';
        await triggerEvent('.desktop-justify-content', 'change');

        expect(value).to.deep.equal(['justify-content-lg-center']);
      });

      it('can set the item alignment', async function () {
        await render(
          hbs`<Input::Options::RowOptions @onChangeOptions={{this.change}}/>`
        );

        expect(
          this.element.querySelector('.desktop-align-items').value
        ).to.equal('default');

        this.element.querySelector('.desktop-align-items').value = 'center';
        await triggerEvent('.desktop-align-items', 'change');

        expect(value).to.deep.equal(['align-items-lg-center']);
      });
    });

    describe('when there are options', function () {
      let value;

      beforeEach(async function () {
        value = undefined;
        this.set('change', (v) => {
          value = v;
        });

        this.set('row', {
          options: [
            'gy-lg-3',
            'gx-lg-3',
            'justify-content-lg-center',
            'align-items-lg-center',
          ],
        });
        await render(
          hbs`<Input::Options::RowOptions @row={{this.row}} @onChangeOptions={{this.change}}/>`
        );
      });

      it('can reset the vertical gutter', async function () {
        expect(
          this.element.querySelector('.desktop-vertical-gutters').value
        ).to.equal('3');

        this.element.querySelector('.desktop-vertical-gutters').value =
          'default';
        await triggerEvent('.desktop-vertical-gutters', 'change');

        expect(value).to.deep.equal([
          'gx-lg-3',
          'justify-content-lg-center',
          'align-items-lg-center',
        ]);
      });

      it('can reset the horizontal gutter', async function () {
        expect(
          this.element.querySelector('.desktop-horizontal-gutters').value
        ).to.equal('3');

        this.element.querySelector('.desktop-horizontal-gutters').value =
          'default';
        await triggerEvent('.desktop-horizontal-gutters', 'change');

        expect(value).to.deep.equal([
          'gy-lg-3',
          'justify-content-lg-center',
          'align-items-lg-center',
        ]);
      });

      it('can reset the justify content', async function () {
        expect(
          this.element.querySelector('.desktop-justify-content').value
        ).to.equal('center');

        this.element.querySelector('.desktop-justify-content').value =
          'default';
        await triggerEvent('.desktop-justify-content', 'change');

        expect(value).to.deep.equal([
          'gy-lg-3',
          'gx-lg-3',
          'align-items-lg-center',
        ]);
      });

      it('can reset the align items', async function () {
        expect(
          this.element.querySelector('.desktop-align-items').value
        ).to.equal('center');

        this.element.querySelector('.desktop-align-items').value = 'default';
        await triggerEvent('.desktop-align-items', 'change');

        expect(value).to.deep.equal([
          'gy-lg-3',
          'gx-lg-3',
          'justify-content-lg-center',
        ]);
      });
    });
  });
});
