import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/options/margins', function () {
  setupRenderingTest();

  let value;

  beforeEach(async function () {
    value = undefined;
    this.set('change', (v) => {
      value = v;
    });

    this.set('options', ['mt-1', 'mb-2', 'ms-3', 'me-4']);

    await render(
      hbs`<Input::Options::Margins @options={{this.options}} @type="mobile" @onChangeOptions={{this.change}}/>`
    );
  });

  it('can set the margin top', async function () {
    expect(this.element.querySelector('.mobile-top-margin').value).to.equal(
      '1'
    );

    this.element.querySelector('.mobile-top-margin').value = '5';
    await triggerEvent('.mobile-top-margin', 'change');

    expect(value).to.deep.equal(['mb-2', 'ms-3', 'me-4', 'mt-5']);
  });

  it('can set the margin bottom', async function () {
    expect(this.element.querySelector('.mobile-bottom-margin').value).to.equal(
      '2'
    );

    this.element.querySelector('.mobile-bottom-margin').value = '5';
    await triggerEvent('.mobile-bottom-margin', 'change');

    expect(value).to.deep.equal(['mt-1', 'ms-3', 'me-4', 'mb-5']);
  });

  it('can set the margin start', async function () {
    expect(this.element.querySelector('.mobile-start-margin').value).to.equal(
      '3'
    );

    this.element.querySelector('.mobile-start-margin').value = '5';
    await triggerEvent('.mobile-start-margin', 'change');

    expect(value).to.deep.equal(['mt-1', 'mb-2', 'me-4', 'ms-5']);
  });

  it('can set the margin end', async function () {
    expect(this.element.querySelector('.mobile-end-margin').value).to.equal(
      '4'
    );

    this.element.querySelector('.mobile-end-margin').value = '5';
    await triggerEvent('.mobile-end-margin', 'change');

    expect(value).to.deep.equal(['mt-1', 'mb-2', 'ms-3', 'me-5']);
  });
});
