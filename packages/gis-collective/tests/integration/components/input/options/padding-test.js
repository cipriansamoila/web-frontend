import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/options/paddings', function () {
  setupRenderingTest();
  let value;

  beforeEach(async function () {
    value = undefined;
    this.set('change', (v) => {
      value = v;
    });

    this.set('options', ['pt-1', 'pb-2', 'ps-3', 'pe-4']);

    await render(
      hbs`<Input::Options::Paddings @options={{this.options}} @type="mobile" @onChangeOptions={{this.change}}/>`
    );
  });

  it('can set the padding top', async function () {
    expect(this.element.querySelector('.mobile-top-padding').value).to.equal(
      '1'
    );

    this.element.querySelector('.mobile-top-padding').value = '5';
    await triggerEvent('.mobile-top-padding', 'change');

    expect(value).to.deep.equal(['pb-2', 'ps-3', 'pe-4', 'pt-5']);
  });

  it('can set the padding bottom', async function () {
    expect(this.element.querySelector('.mobile-bottom-padding').value).to.equal(
      '2'
    );

    this.element.querySelector('.mobile-bottom-padding').value = '5';
    await triggerEvent('.mobile-bottom-padding', 'change');

    expect(value).to.deep.equal(['pt-1', 'ps-3', 'pe-4', 'pb-5']);
  });

  it('can set the padding start', async function () {
    expect(this.element.querySelector('.mobile-start-padding').value).to.equal(
      '3'
    );

    this.element.querySelector('.mobile-start-padding').value = '5';
    await triggerEvent('.mobile-start-padding', 'change');

    expect(value).to.deep.equal(['pt-1', 'pb-2', 'pe-4', 'ps-5']);
  });

  it('can set the padding end', async function () {
    expect(this.element.querySelector('.mobile-end-padding').value).to.equal(
      '4'
    );

    this.element.querySelector('.mobile-end-padding').value = '5';
    await triggerEvent('.mobile-end-padding', 'change');

    expect(value).to.deep.equal(['pt-1', 'pb-2', 'ps-3', 'pe-5']);
  });
});
