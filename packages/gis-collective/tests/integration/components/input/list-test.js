import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, waitFor, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/list', function () {
  setupRenderingTest();

  it('renders a deactivated switch', async function () {
    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);
    await render(hbs`
      <Input::List @list={{this.values}} @isEnabled={{false}} @values={{this.values}} />
    `);

    expect(this.element.querySelector('input[type=checkbox]').checked).to.equal(
      false
    );
    expect(this.element.querySelector('ol')).not.to.exist;
  });

  it('renders the list when the switch is activated', async function () {
    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);
    await render(hbs`
      <Input::List @list={{this.values}} @isEnabled={{false}} @values={{this.values}} />
    `);

    await click(this.element.querySelector('input[type=checkbox]'));
    expect(this.element.querySelector('input[type=checkbox]').checked).to.equal(
      true
    );
    expect(this.element.querySelector('ol')).to.exist;
  });

  it('triggers the save with an empty list and in disabled state', async function () {
    let values, isEnabled;

    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);
    this.set('onChange', function (b, c) {
      isEnabled = b;
      values = c;
    });

    await render(hbs`
      <Input::List @list={{this.values}} @isEnabled={{true}} @values={{this.values}} @onChange={{this.onChange}}/>
    `);
    await click(this.element.querySelector('input[type=checkbox]'));

    expect(values).to.deep.equal([]);
    expect(isEnabled).to.equal(false);
  });

  it('triggers the onChange with the list and an enabled state when the switch is toggled', async function () {
    let values, isEnabled;

    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);

    this.set('onChange', function (b, c) {
      isEnabled = b;
      values = c;
    });

    await render(hbs`
      <Input::List @list={{this.values}} @isEnabled={{false}} @values={{this.values}} @onChange={{this.onChange}}/>
    `);

    await click(this.element.querySelector('input[type=checkbox]'));

    expect(values).to.deep.equal(this.values);
    expect(isEnabled).to.equal(true);
  });

  it('renders a list of select inputs set to the values', async function () {
    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);
    await render(hbs`
      <Input::List @isEnabled={{true}} @list={{this.values}} @values={{this.values}} />
    `);

    expect(this.element.querySelector('ol')).to.exist;

    const elements = this.element.querySelectorAll('ol li select');
    expect(elements.length).to.equal(2);
    expect(elements[0].value.trim()).to.equal('1');
    expect(elements[1].value.trim()).to.equal('2');
  });

  it('can remove all options and saves them', async function () {
    let values;

    this.set('values', [
      { id: 1, name: 'name1' },
      { id: 2, name: 'name2' },
    ]);

    this.set('save', function (a, b) {
      values = b;
    });

    await render(hbs`
      <Input::List @isEnabled={{true}} @list={{this.values}} @values={{this.values}} @onChange={{this.save}} />
    `);

    expect(this.element.querySelector('ol')).to.exist;

    await click('.btn-delete');
    await click('.btn-delete');

    const elements = this.element.querySelectorAll('ol li select');
    expect(elements.length).to.equal(0);

    expect(values).to.deep.equal([]);
  });

  it('can change a value', async function () {
    let values;

    this.set('values', [{ id: '1', name: 'name1' }]);
    this.set('list', [
      { id: '1', name: 'name1' },
      { id: '2', name: 'name2' },
    ]);

    this.set('save', function (a, b) {
      values = b;
    });

    await render(hbs`
      <Input::List @isEnabled={{true}} @list={{this.list}} @values={{this.values}} @onChange={{this.save}} />
    `);

    expect(this.element.querySelector('ol')).to.exist;
    const element = this.element.querySelector('ol li select');
    element.value = '2';
    await triggerEvent('ol li select', 'change');

    expect(values).to.deep.equal([{ id: '2', name: 'name2' }]);
  });

  it('can add a value', async function () {
    let values;

    this.set('values', []);
    this.set('list', [
      { id: '1', name: 'name1' },
      { id: '2', name: 'name2' },
    ]);

    this.set('save', function (a, b) {
      values = b;
    });

    await render(hbs`
      <Input::List @isEnabled={{true}} @list={{this.list}} @values={{this.values}} @isMainValue={{true}} @title="title" @onChange={{this.save}} @editablePanel="title" />
    `);

    await click('.btn-add-item');
    expect(this.element.querySelector('ol')).to.exist;
    const elements = this.element.querySelectorAll('ol li select');
    expect(elements.length).to.equal(1);

    expect(values).to.deep.equal([{ id: '2', name: 'name2' }]);
  });

  it('ignores an invalid value', async function () {
    let values;

    this.set('values', []);
    this.set('list', [
      { id: '1', name: 'name1' },
      { id: '2', name: 'name2' },
    ]);

    this.set('save', function (a, b) {
      values = b;
    });

    await render(hbs`
      <Input::List @isEnabled={{true}} @list={{this.list}} @values={{this.values}} @isMainValue={{true}} @title="title" @onChange={{this.save}} @editablePanel="title" />
    `);

    await click('.btn-add-item');
    await waitFor('ol li select');

    this.element.querySelector('ol li select').value = '';
    await triggerEvent('ol li select', 'change');

    expect(values).to.deep.equal([]);
  });
});
