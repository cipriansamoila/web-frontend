import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent, waitFor, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';
import { move } from 'ember-drag-sort/utils/trigger';

describe('Integration | Component | input/sound-list', function() {
  setupRenderingTest();
  let server;

  beforeEach(function() {
    server = new TestServer();

    this.set("createSound", () => {
      return {}
    });
  });

  it('renders an add button when the list is empty', async function() {
    await render(hbs`<Input::SoundList />`);
    expect(this.element.querySelector(".btn-sound-add")).to.exist;
  });

  it('triggers on change event when a sound is selected', async function() {
    this.set("value", []);

    let value;
    this.set("change", (newValue) => {
      value = newValue;
    });

    await render(hbs`<Input::SoundList @onChange={{this.change}} @value={{this.value}} @create={{this.createSound}}/>`);
    await waitFor(".sound-list-input");

    const blob = server.testData.create.mp3Blob();
    await triggerEvent(
      ".sound-list-input",
      "change",
      { files: [blob] }
    );

    expect(value).to.have.length(1);
    expect(value[0].sound).to.startWith("data:audio/mpeg;base64,DcOBwrHDlcK0woHCicOJwoXCucKRw6TCgcKJwrHDlcKVw4wBUSVQw4gAAAA8AAANBcKxwrDCgcOlwr3DlcOIwoHCscK9w5nClAFRQRTDhAAAADQAAA05wqXCncKhw5HCscK9w43ClcOJw4wBUU1NFAAAADwAAA0xwoXDmcKYw5TDoMK4w4jDpMK4w4TDgMOAAQVBJQwABQXDkAAADcKlwrXChcKdwpTCvcKpw4HClQ==");
  });

  it('can change the order of two items', async function() {
    this.set("value", [{sound: "data:audio/mpeg;base64,sound1"}, {sound: "data:audio/mpeg;base64,sound2"}]);

    let value;
    this.set("change", (newValue) => {
      value = newValue;
    });

    await render(hbs`<Input::SoundList @onChange={{this.change}} @value={{this.value}} @create={{this.createSound}}/>`);

    const list = document.querySelector('.dragSortList')
    await move(list, 0, list, 1, false, ".sound-container");

    expect(value).to.have.length(2);
    expect(value[0].sound).to.startWith("data:audio/mpeg;base64,sound2");
    expect(value[1].sound).to.startWith("data:audio/mpeg;base64,sound1");
  });

  it('can delete a sound from the list', async function() {
    this.set("value", [{sound: "data:audio/mpeg;base64,sound1"}, {sound: "data:audio/mpeg;base64,sound2"}]);

    let value;
    this.set("change", (newValue) => {
      value = newValue;
    });

    await render(hbs`<Input::SoundList @onChange={{this.change}} @value={{this.value}} @create={{this.createSound}}/>`);

    const deleteButtons = this.element.querySelectorAll(".btn-delete-sound")

    await click(deleteButtons[0]);

    expect(value).to.have.length(1);
    expect(value[0].sound).to.startWith("data:audio/mpeg;base64,sound2");
  });
});
