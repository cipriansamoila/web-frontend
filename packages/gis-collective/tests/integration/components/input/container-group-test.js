import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/container-group', function () {
  setupRenderingTest();

  describe('as a small group', function () {
    it('renders as small group by default', async function () {
      await render(hbs`
        <Input::ContainerGroup @title="some title">
          template block text
        </Input::ContainerGroup>
      `);

      expect(this.element.querySelector('.group-help')).not.to.exist;
      expect(this.element.querySelector('h2')).not.to.exist;
      expect(this.element.querySelector('h4').textContent.trim()).to.startWith(
        'some title'
      );

      expect(
        this.element.querySelector('.btn-edit').textContent.trim()
      ).to.equal('edit');
      expect(this.element.querySelector('.btn-edit')).to.have.class('btn-link');

      expect(this.element.querySelector('.value').textContent.trim()).to.equal(
        'template block text'
      );
      expect(this.element.querySelector('.row-submit')).not.to.exist;
      expect(this.element.querySelector('.btn-edit')).not.to.have.attribute(
        'disabled',
        ''
      );
    });

    it('can disable the edit mode', async function () {
      await render(hbs`
        <Input::ContainerGroup @isMainValue={{true}} @disabled={{true}} @title="some title"/>
      `);

      expect(this.element.querySelector('.btn-edit')).to.have.attribute(
        'disabled',
        ''
      );
    });

    it('renders as small group with help', async function () {
      await render(hbs`
        <Input::ContainerGroup @help="some message" @title="some title">
          template block text
        </Input::ContainerGroup>
      `);

      expect(this.element.querySelector('h4 .group-help')).to.exist;
      expect(this.element.querySelector('h4 .group-help')).to.have.attribute(
        'title',
        'some message'
      );
      expect(this.element.querySelector('h4 .group-help .fa-question-circle'))
        .to.exist;
    });

    it('triggers onEdit event on click on edit label', async function () {
      let label;

      this.set('onEdit', function (val) {
        label = val;
      });

      await render(hbs`
        <Input::ContainerGroup @onEdit={{this.onEdit}} @title="some title">
          template block text
        </Input::ContainerGroup>
      `);

      await click('.btn-edit');
      expect(label).to.equal('some title');
    });

    it('renders the edit mode when the editable panel = title', async function () {
      await render(hbs`
        <Input::ContainerGroup @editablePanel="some title" @title="some title" as |isEdit|>
          {{isEdit}}
        </Input::ContainerGroup>
      `);

      expect(this.element.querySelector('.btn-edit')).not.to.exist;
      expect(
        this.element.querySelector('.value').textContent.trim()
      ).to.startWith('true');
      expect(this.element.querySelector('.row-submit')).to.exist;
    });

    it('calls save action with the title as param', async function () {
      let title;

      this.set('onSave', function (val) {
        title = val;
      });

      await render(hbs`
        <Input::ContainerGroup @onSave={{this.onSave}} @editablePanel="some title" @title="some title" as |isEdit|>
          {{isEdit}}
        </Input::ContainerGroup>
      `);

      click(this.element.querySelector('.value .btn-submit'));
      await waitUntil(() => title);

      expect(title).to.equal('some title');
    });

    it('calls cancel action with the title as param', async function () {
      let title;

      this.set('onCancel', function (val) {
        title = val;
      });

      await render(hbs`
        <Input::ContainerGroup @onCancel={{this.onCancel}} @editablePanel="some title" @title="some title" as |isEdit|>
          {{isEdit}}
        </Input::ContainerGroup>
      `);

      click(this.element.querySelector('.value .btn-cancel'));
      await waitUntil(() => title);

      expect(title).to.equal('some title');
    });

    it('renders the view mode when the editable panel != title', async function () {
      await render(hbs`
        <Input::ContainerGroup @onEdit={{this.onEdit}} @editablePanel="some title" @title="other title" as |isEdit|>
          {{isEdit}}
        </Input::ContainerGroup>
      `);

      expect(this.element.querySelector('.value').textContent.trim()).to.equal(
        'false'
      );
    });

    it('does not render the save spinner when the savingPanel != title', async function () {
      await render(hbs`
        <Input::ContainerGroup @onEdit={{this.onEdit}} @savingPanel="some title" @title="some title" as |isEdit|>
          {{isEdit}}
        </Input::ContainerGroup>
      `);

      expect(this.element.querySelector('.fa-spinner')).to.exist;
    });

    it('does not render the save spinner when the savingPanel != title', async function () {
      await render(hbs`
        <Input::ContainerGroup @onEdit={{this.onEdit}} @savingPanel="some title" @title="other title" as |isEdit|>
          {{isEdit}}
        </Input::ContainerGroup>
      `);

      expect(this.element.querySelector('.fa-spinner')).not.to.exist;
    });
  });

  describe('as a main group', function () {
    it('renders main group by default', async function () {
      await render(hbs`
        <Input::ContainerGroup @isMainValue={{true}} @title="some title">
          template block text
        </Input::ContainerGroup>
      `);

      expect(this.element.querySelector('.group-help')).not.to.exist;
      expect(this.element.querySelector('h4')).not.to.exist;
      expect(this.element.querySelector('h2').textContent.trim()).to.startWith(
        'some title'
      );

      expect(this.element.querySelector('.btn-edit')).to.exist;
      expect(this.element.querySelector('.btn-edit .fa-pencil-alt')).to.exist;

      expect(this.element.querySelector('.value').textContent.trim()).to.equal(
        'template block text'
      );
      expect(this.element.querySelector('.btn-edit')).not.to.have.attribute(
        'disabled',
        ''
      );
    });

    it('can disable the edit mode', async function () {
      await render(hbs`
        <Input::ContainerGroup @isMainValue={{true}} @disabled={{true}} @title="some title"/>
      `);

      expect(this.element.querySelector('.btn-edit')).to.have.attribute(
        'disabled',
        ''
      );
    });

    it('does not render the edit button when canEdit is false', async function () {
      await render(hbs`
        <Input::ContainerGroup @onEdit={{this.onEdit}} @canEdit={{false}} @title="other title"/>
      `);

      expect(this.element.querySelector('.btn-edit')).not.to.exist;
    });

    it('renders main group with help', async function () {
      await render(hbs`
        <Input::ContainerGroup @help="some message" @title="some title" @isMainValue={{true}}>
          template block text
        </Input::ContainerGroup>
      `);

      expect(this.element.querySelector('h2 .help-tooltip')).to.exist;
      expect(
        this.element
          .querySelector('h2 .help-tooltip .content')
          .textContent.trim()
      ).to.equal('some message');
      expect(this.element.querySelector('h2 .help-tooltip .fa-question-circle'))
        .to.exist;
    });

    it('triggers onEdit event on click on edit label', async function () {
      let label;

      this.set('onEdit', function (val) {
        label = val;
      });

      await render(hbs`
        <Input::ContainerGroup @onEdit={{this.onEdit}} @title="some title" @isMainValue={{true}}>
          template block text
        </Input::ContainerGroup>
      `);

      await click('.btn-edit');
      expect(label).to.equal('some title');
    });

    it('does not render the save spinner when the savingPanel != title', async function () {
      await render(hbs`
        <Input::ContainerGroup @onEdit={{this.onEdit}} @savingPanel="some title" @title="some title" @isMainValue={{true}}>
          {{isEdit}}
        </Input::ContainerGroup>
      `);

      expect(this.element.querySelector('.fa-spinner')).to.exist;
    });

    it('does not render the save spinner when the savingPanel != title', async function () {
      await render(hbs`
        <Input::ContainerGroup @onEdit={{this.onEdit}} @savingPanel="some title" @title="other title" @isMainValue={{true}}>
          {{isEdit}}
        </Input::ContainerGroup>
      `);

      expect(this.element.querySelector('.fa-spinner')).not.to.exist;
    });
  });
});
