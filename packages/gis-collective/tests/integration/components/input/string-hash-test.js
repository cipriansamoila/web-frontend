import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { click, fillIn, render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/string-hash', function () {
  setupRenderingTest();

  it('can add a new item', async function () {
    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::StringHash @onChange={{this.change}}/>`);

    await click('.btn-add-item');

    expect(value).to.deep.equal({
      '': '',
    });
  });

  it('renders an item', async function () {
    this.set('value', {
      key: 'value',
    });

    await render(
      hbs`<Input::StringHash @value={{this.value}} @onChange={{this.change}}/>`
    );

    expect(this.element.querySelectorAll('.input-key')).to.have.length(1);
    expect(this.element.querySelector('.input-key').value).to.equal('key');

    expect(this.element.querySelectorAll('.input-value')).to.have.length(1);
    expect(this.element.querySelector('.input-value').value).to.equal('value');
  });

  it('can change a value', async function () {
    let value;
    this.set('value', {});

    this.set('change', (v) => {
      value = v;
      this.set('value', v);
    });

    await render(
      hbs`<Input::StringHash @value={{this.value}} @onChange={{this.change}}/>`
    );

    await click('.btn-add-item');

    await fillIn('.input-key', 'abc');
    await fillIn('.input-value', 'def');

    expect(value).to.deep.equal({
      abc: 'def',
    });
  });
});
