import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/bs-color', function () {
  setupRenderingTest();

  let value;

  beforeEach(function () {
    value = undefined;
    this.set('change', function (v) {
      value = v;
    });
  });

  describe('when there is no value', function () {
    it('renders a list of the predefined colors', async function () {
      await render(hbs`<Input::BsColor />`);

      const colors = this.element.querySelectorAll(
        '.predefined-colors .btn-color'
      );

      expect(colors.length).to.equal(10);
      expect(colors[0]).to.have.class('btn-color-primary');
      expect(colors[1]).to.have.class('btn-color-secondary');
      expect(colors[2]).to.have.class('btn-color-success');
      expect(colors[3]).to.have.class('btn-color-danger');
      expect(colors[4]).to.have.class('btn-color-warning');
      expect(colors[5]).to.have.class('btn-color-info');
      expect(colors[6]).to.have.class('btn-color-light');
      expect(colors[7]).to.have.class('btn-color-dark');
    });

    it('renders a list of colors', async function () {
      await render(hbs`<Input::BsColor />`);

      const colors = this.element.querySelectorAll('.colors .btn-color');

      expect(colors.length).to.equal(11);
      expect(colors[0]).to.have.class('btn-color-blue');
      expect(colors[1]).to.have.class('btn-color-indigo');
      expect(colors[2]).to.have.class('btn-color-purple');
      expect(colors[3]).to.have.class('btn-color-pink');
      expect(colors[4]).to.have.class('btn-color-red');
      expect(colors[5]).to.have.class('btn-color-orange');
      expect(colors[6]).to.have.class('btn-color-yellow');
      expect(colors[7]).to.have.class('btn-color-green');
      expect(colors[8]).to.have.class('btn-color-teal');
      expect(colors[9]).to.have.class('btn-color-cyan');
      expect(colors[10]).to.have.class('btn-color-gray');
    });

    it('renders transparent when no value is selected', async function () {
      await render(hbs`<Input::BsColor />`);

      expect(
        this.element.querySelector('.selected-color').textContent.trim()
      ).to.equal('transparent');
    });

    it('triggers onChange when a predefined color button is pressed', async function () {
      await render(hbs`<Input::BsColor @onChange={{this.change}} />`);

      await click('.btn-color-secondary');

      expect(value).to.equal('secondary');
    });

    it('triggers onChange when a color button is pressed', async function () {
      await render(hbs`<Input::BsColor @onChange={{this.change}} />`);

      await click('.btn-color-red');

      expect(value).to.equal('red');
    });
  });

  describe('when the primary color is selected', function () {
    it('renders the primary text', async function () {
      await render(hbs`<Input::BsColor @value="primary" />`);

      expect(
        this.element.querySelector('.selected-color').textContent.trim()
      ).to.equal('primary');
    });

    it('renders the checked icon on the primary button', async function () {
      await render(hbs`<Input::BsColor @value="primary" />`);

      expect(this.element.querySelectorAll('.fa-check').length).to.equal(1);
      expect(this.element.querySelector('.btn-color-primary .fa-check')).to
        .exist;
    });

    it('triggers onChange with an empty string when the primary color is selected', async function () {
      await render(
        hbs`<Input::BsColor @onChange={{this.change}} @value="primary" />`
      );

      await click('.btn-color-primary');

      expect(value).to.equal('');
    });
  });

  describe('when the blue color is selected', function () {
    beforeEach(async function () {
      await render(
        hbs`<Input::BsColor @onChange={{this.change}} @value="blue" />`
      );
    });

    it('renders the blue text', async function () {
      expect(
        this.element.querySelector('.selected-color').textContent.trim()
      ).to.equal('blue');
    });

    it('renders the checked icon on the primary button', async function () {
      expect(this.element.querySelectorAll('.fa-check').length).to.equal(1);
      expect(this.element.querySelector('.btn-color-blue .fa-check')).to.exist;
    });

    it('triggers onChange with an empty string when the primary color is selected', async function () {
      await click('.btn-color-blue');

      expect(value).to.equal('');
    });

    it('can select a new shade of blue', async function () {
      await click('.btn-color-blue-100');

      expect(value).to.equal('blue-100');
    });
  });

  describe('when a shade of blue blue color is selected', function () {
    beforeEach(async function () {
      await render(
        hbs`<Input::BsColor @onChange={{this.change}} @value="blue-300" />`
      );
    });

    it('renders the blue text', async function () {
      expect(
        this.element.querySelector('.selected-color').textContent.trim()
      ).to.equal('blue-300');
    });

    it('renders the checked icon on the shade button', async function () {
      expect(this.element.querySelectorAll('.fa-check').length).to.equal(1);
      expect(this.element.querySelector('.btn-color-blue-300 .fa-check')).to
        .exist;
    });

    it('triggers onChange with an empty string when the same shade is selected', async function () {
      await click('.btn-color-blue-300');

      expect(value).to.equal('');
    });

    it('can select a new shade of blue', async function () {
      await click('.btn-color-blue-100');

      expect(value).to.equal('blue-100');
    });
  });
});
