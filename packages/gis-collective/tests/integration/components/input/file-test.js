import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/file', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<Input::File />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('should render an error message', async function () {
    await render(hbs`<Input::File @error="some error message"/>`);

    expect(this.element.querySelector(".invalid-feedback").textContent.trim()).to.equal("some error message");
  });

  it('triggers the onMimeSelect when a new file is selected', async function () {
    let mime;

    this.set("mimeSelect", function (value) {
      mime = value;
    });

    await render(hbs`<Input::File @onMimeSelect={{this.mimeSelect}}/>`);

    let blob = new Blob(['foo', 'bar'], { type: 'text/plain' });
    blob.name = 'foobar.txt';

    await triggerEvent(".form-control", "change", { files: [blob] });
    await waitUntil(() => mime);

    expect(mime).to.equal("text/plain");
  });

  it('triggers the onMimeSelect with a null when a no file is selected', async function () {
    let mime;

    this.set("mimeSelect", function (value) {
      mime = value;
    });

    await render(hbs`<Input::File @onMimeSelect={{this.mimeSelect}}/>`);

    let blob = new Blob(["foo", "bar"], { type: 'text/plain' });
    blob.name = "foobar";

    await triggerEvent(".form-control", "change", { files: [blob] });
    await triggerEvent(".form-control", "change", { files: [ ] });

    expect(mime).to.equal(null);
  });

  it('triggers the onNameChange when a new file is selected', async function () {
    let name;

    this.set("nameSelect", function (value) {
      name = value;
    });

    await render(hbs`<Input::File @onNameChange={{this.nameSelect}}/>`);

    let blob = new Blob(["foo", "bar"], { type: 'text/plain' });
    blob.name = "foobar.txt";

    await triggerEvent(".form-control", "change", { files: [blob] });
    await waitUntil(() => name);

    expect(name).to.equal("foobar.txt");
  });

  it('triggers the onNameChange with a null when a no file is selected', async function () {
    let name;

    this.set("nameSelect", function (value) {
      name = value;
    });

    await render(hbs`<Input::File @onNameChange={{this.nameSelect}}/>`);

    let blob = new Blob(["foo", "bar"], { type: 'text/plain' });
    blob.name = "foobar";

    await triggerEvent(".form-control", "change", { files: [blob] });
    await triggerEvent(".form-control", "change", { files: [ ] });

    expect(name).to.equal(null);
  });

  it('triggers the onTextContentChange when a new file is selected', async function () {
    let content;
    let name;

    this.set("textContentSelect", function (n, value) {
      content = value;
      name = n;
    });

    await render(hbs`<Input::File @onTextContentChange={{this.textContentSelect}}/>`);

    let blob = new Blob(["foo", "bar"], { type: 'text/plain' });
    blob.name = "foobar.txt";

    await triggerEvent(".form-control", "change", { files: [blob] });
    await waitUntil(() => content);

    expect(content).to.equal("foobar");
    expect(name).to.equal("foobar.txt");
  });

  /*
  it('triggers the onTextContentChange when a null when no file is selected', async function () {
    let content;
    let name;

    this.set("textContentSelect", function (n, value) {
      content = value;
      name = n;
    });

    await render(hbs`<Input::File @onTextContentChange={{this.textContentSelect}}/>`);

    let blob = new Blob(["foo", "bar"], { type: 'text/plain' });
    blob.name = "foobar.txt";

    await triggerEvent(".custom-file-input", "change", { files: [blob] });
    await triggerEvent(".custom-file-input", "change", { files: [] });

    expect(content).to.equal(null);
    expect(name).to.equal(null);
  });*/

  it('triggers the onBaseUrlContentChange when a new file is selected', async function () {
    let content;
    let name;

    this.set("contentSelect", function (n, value) {
      content = value;
      name = n;
    });

    await render(hbs`<Input::File @onBaseUrlContentChange={{this.contentSelect}}/>`);

    let blob = new Blob(["foo", "bar"], { type: 'text/plain' });
    blob.name = "foobar.txt";

    await triggerEvent(".form-control", "change", { files: [blob] });
    await waitUntil(() => content);

    expect(content).to.equal("data:text/plain;base64,Zm9vYmFy");
    expect(name).to.equal("foobar.txt");
  });

  it('triggers the onBaseUrlContentChange when a null when no file is selected', async function () {
    let content;
    let name;

    this.set("contentSelect", function (n, value) {
      name = n;
      content = value;
    });

    await render(hbs`<Input::File @onBaseUrlContentChange={{this.contentSelect}}/>`);

    let blob = new Blob(["foo", "bar"], { type: 'text/plain' });
    blob.name = "foobar.txt";

    await triggerEvent(".form-control", "change", { files: [blob] });
    await triggerEvent(".form-control", "change", { files: [] });
    await triggerEvent(".form-control", "change", { files: [] });

    await waitUntil(() => !content);

    expect(content).to.equal(null);
    expect(name).to.equal(null);
  });

  it('shows an info message', async function () {
    await render(hbs`<Input::File @info="some message"/>`);
    await waitUntil(() => this.element.querySelector(".form-text.text-muted"));

    expect(this.element.querySelector(".form-text.text-muted").textContent.trim()).to.equal("some message");
  });
});
