import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, fillIn, click, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/feature-field-mapping', function () {
  setupRenderingTest();

  it('renders the empty value by default', async function () {
    await render(hbs`<Input::FeatureFieldMapping />`);

    expect(this.element.querySelector('select')).to.exist;
    expect(this.element.querySelector('.form-floating')).not.to.exist;
    expect(this.element.querySelector('select').value).to.equal('');
  });

  it('switches to the input box', async function () {
    await render(hbs`<Input::FeatureFieldMapping @key="key" />`);
    expect(this.element.querySelector('select')).to.exist;

    await click('.form-check-input');

    expect(this.element.querySelector('select')).not.to.exist;
    expect(this.element.querySelector('.input-group-name').value).to.equal(
      'other'
    );
    expect(this.element.querySelector('.input-property-name').value).to.equal(
      'key'
    );
  });

  it('triggers the custom name when changed to the input box', async function () {
    let value;

    this.set('change', function (v) {
      value = v;
    });

    await render(
      hbs`<Input::FeatureFieldMapping @key="key" @onChange={{this.change}}/>`
    );
    expect(this.element.querySelector('select')).to.exist;

    await click('.form-check-input');
    expect(value).to.equal('attributes.other.key');
  });

  it('switches to the custom attribute and back', async function () {
    await render(hbs`<Input::FeatureFieldMapping @key="key" />`);
    expect(this.element.querySelector('select')).to.exist;

    await click('.form-check-input');
    await click('.form-check-input');

    expect(this.element.querySelector('select')).to.exist;
    expect(this.element.querySelector('.form-floating')).not.to.exist;
  });

  it('keeps the original value if the value type is switched', async function () {
    this.set('value', 'attributes.custom.test');

    await render(
      hbs`<Input::FeatureFieldMapping @key="key" @value={{this.value}} />`
    );

    await click('.form-check-input');
    await click('.form-check-input');

    expect(this.element.querySelector('.input-group-name').value).to.equal(
      'custom'
    );
    expect(this.element.querySelector('.input-property-name').value).to.equal(
      'test'
    );
  });

  it('triggers an empty string when it switches to a select', async function () {
    let value;

    this.set('change', function (v) {
      value = v;
    });

    await render(
      hbs`<Input::FeatureFieldMapping @key="key" @onChange={{this.change}}/>`
    );

    await click('.form-check-input');
    await click('.form-check-input');
    expect(value).to.equal('');
  });

  it('renders the input text when the value it is not listed', async function () {
    await render(hbs`<Input::FeatureFieldMapping @value="attributes.name"/>`);

    expect(this.element.querySelector('select')).not.to.exist;
    expect(this.element.querySelector('.input-group-name').value).to.equal(
      'other'
    );
    expect(this.element.querySelector('.input-property-name').value).to.equal(
      'name'
    );
  });

  it('renders the nested attribute name in the text editor', async function () {
    await render(
      hbs`<Input::FeatureFieldMapping @value="attributes.group1.name"/>`
    );

    expect(this.element.querySelector('select')).not.to.exist;
    expect(this.element.querySelector('.input-group-name').value).to.equal(
      'group1'
    );
    expect(this.element.querySelector('.input-property-name').value).to.equal(
      'name'
    );
  });

  it('triggers the selected value on change custom attribute', async function () {
    let value;

    this.set('change', function (v) {
      value = v;
    });

    await render(
      hbs`<Input::FeatureFieldMapping @key="key" @onChange={{this.change}}/>`
    );
    await click('.form-check-input');

    await fillIn('.input-group-name', 'group');
    await fillIn('.input-property-name', 'name');
    await click('.input-group-name');

    expect(value).to.equal('attributes.group.name');
  });

  it('triggers the selected value on change', async function () {
    let value;

    this.set('change', function (v) {
      value = v;
    });

    await render(
      hbs`<Input::FeatureFieldMapping @key="key" @onChange={{this.change}}/>`
    );

    this.element.querySelector('select').value = 'maps';
    await triggerEvent('select', 'change');

    expect(value).to.equal('maps');
  });

  it('shows a validation error when the group name is empty', async function () {
    let value;

    this.set('change', function (v) {
      value = v;
    });

    await render(
      hbs`<Input::FeatureFieldMapping @key="key" @onChange={{this.change}}/>`
    );
    await click('.form-check-input');

    await fillIn('.input-group-name', '');
    await click('.input-property-name');

    expect(this.element.querySelector('.input-group-name')).to.have.class(
      'is-invalid'
    );
    expect(value).to.equal('');
  });

  it('shows a validation error when the property name is empty', async function () {
    let value;

    this.set('change', function (v) {
      value = v;
    });

    await render(
      hbs`<Input::FeatureFieldMapping @key="key" @onChange={{this.change}}/>`
    );
    await click('.form-check-input');

    await fillIn('.input-property-name', '');
    await click('.input-group-name');

    expect(this.element.querySelector('.input-property-name')).to.have.class(
      'is-invalid'
    );
    expect(value).to.equal('');
  });
});
