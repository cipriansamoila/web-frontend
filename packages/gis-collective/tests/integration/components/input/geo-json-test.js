import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, typeIn } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/geo-json', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Input::GeoJson />`);
    expect(this.element.querySelector(".input-geo-json").editor).not.be.null;
  });

  it('sets a json value at the first render', async function() {
    this.set("value", { key: "value" });
    await render(hbs`<Input::GeoJson @value={{this.value}}/>`);

    expect(this.element.querySelector(".input-geo-json").editor.getText()).to.equal(`{\n  "key": "value"\n}`);
  });

  it('updates a json value on update', async function() {
    this.set("value", { key: "value" });
    await render(hbs`<Input::GeoJson @value={{this.value}}/>`);

    this.set("value", { key: "other value" });
    expect(this.element.querySelector(".input-geo-json").editor.getText()).to.equal(`{\n  "key": "other value"\n}`);
  });

  it('triggers onChange event when the value is changed', async function() {
    let value;

    this.set("change", function(v) {
      value = v;
    });

    await render(hbs`<Input::GeoJson @onChange={{this.change}}/>`);
    const editor = this.element.querySelector(".input-geo-json").editor;

    editor.setMode("text");

    this.element.querySelector(".jsoneditor-text").value = "";
    await typeIn(".jsoneditor-text", `{ "key": "value" }`);

    expect(value).to.deep.equal({ key: "value" });
  });
});
