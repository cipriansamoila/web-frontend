import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, fillIn } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/detailed-location', function() {
  setupRenderingTest();

  it('renders all location fields', async function() {
    await render(hbs`<Input::DetailedLocation />`);

    const labels = [...this.element.querySelectorAll("label")].map(a => a.textContent.trim());

    expect(labels).to.deep.equal([
      "Country",
      "Province/State",
      "City",
      "Postal code"
    ]);
  });

  it('renders all values fields', async function() {
    this.set("value", {
      country: "1",
      province: "2",
      city: "3",
      postalCode: "4"
    });

    await render(hbs`<Input::DetailedLocation @value={{this.value}}/>`);

    expect(this.element.querySelector(".input-country").value).to.equal("1");
    expect(this.element.querySelector(".input-province").value).to.equal("2");
    expect(this.element.querySelector(".input-city").value).to.equal("3");
    expect(this.element.querySelector(".input-postalCode").value).to.equal("4");


  });

  it('can update the values', async function() {
    this.set("value", {
      country: "1",
      province: "2",
      city: "3",
      postalCode: "4"
    });

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Input::DetailedLocation @value={{this.value}} @onChange={{this.change}}/>`);

    await fillIn(".input-country", "11");
    await fillIn(".input-province", "22");
    await fillIn(".input-city", "33");
    await fillIn(".input-postalCode", "44");

    expect(value).to.deep.equal({
      country: "11",
      province: "22",
      city: "33",
      postalCode: "44"
    });
  });

  it('can delete the values', async function() {
    this.set("value", {
      country: "1",
      province: "2",
      city: "3",
      postalCode: "4"
    });

    let value;
    this.set("change", (v) => {
      value = v;
    });

    await render(hbs`<Input::DetailedLocation @value={{this.value}} @onChange={{this.change}}/>`);

    await fillIn(".input-country", "");
    await fillIn(".input-province", "");
    await fillIn(".input-city", "");
    await fillIn(".input-postalCode", "");

    expect(value).to.deep.equal({
      country: "",
      province: "",
      city: "",
      postalCode: ""
    });
  });
});
