import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';
import { waitUntil, triggerEvent } from '@ember/test-helpers';
import fillIn from '@ember/test-helpers/dom/fill-in';
import click from '@ember/test-helpers/dom/click';

describe('Integration | Component | input/page-link', function () {
  setupRenderingTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    const page1 = server.testData.create.page('section--page--first');
    page1.slug = 'section--page--first';
    server.testData.storage.addPage(page1);

    const page2 = server.testData.create.page('section--page--second');
    page2.slug = 'section--page--second';
    server.testData.storage.addPage(page2);
  });

  it('renders a select with the available pages', async function () {
    await render(hbs`<Input::PageLink />`);

    await waitUntil(() => this.element.querySelectorAll('option').length > 0);

    const options = this.element.querySelectorAll('option');

    expect(options).to.have.length(3);
    expect(options[0].textContent.trim()).to.equal('');
    expect(options[1].textContent.trim()).to.equal(
      'test (/section/page/first)'
    );
    expect(options[2].textContent.trim()).to.equal(
      'test (/section/page/second)'
    );
  });

  it('renders a select with the current value', async function () {
    await render(hbs`<Input::PageLink @value="/section/page/second"/>`);

    await waitUntil(() => this.element.querySelectorAll('option').length > 0);

    expect(this.element.querySelector('select').value).to.equal(
      'section--page--second'
    );
  });

  it('renders an input with an external link', async function () {
    await render(hbs`<Input::PageLink @value="https://giscollective.com"/>`);

    expect(this.element.querySelector('.select.link')).not.to.exist;
    expect(this.element.querySelector('.text-link').value).to.equal(
      'https://giscollective.com'
    );
  });

  it('triggers the on change event when the text value is changed', async function () {
    this.set('value', 'http://a.com');
    let value;

    this.set('change', (v) => {
      value = v;
      this.set('value', v);
    });

    await render(
      hbs`<Input::PageLink @value={{this.value}} @onChange={{this.change}}/>`
    );

    await fillIn('.form-control', 'http://b.com');

    expect(value).to.equal('http://b.com');
  });

  it('triggers the on change event when the value is changed', async function () {
    let value;
    this.set('change', (v) => {
      value = v;
    });
    await render(
      hbs`<Input::PageLink @value="/section/page/second" @onChange={{this.change}}/>`
    );

    await waitUntil(() => this.element.querySelectorAll('option').length > 0);

    this.element.querySelector('select').value = 'section--page--first';
    await triggerEvent('select', 'change');

    expect(value).to.equal('/section/page/first');
  });

  it('can toggle between external and internal link', async function () {
    await render(
      hbs`<Input::PageLink @value="section--page--second" @onChange={{this.change}}/>`
    );

    await click('.chk-external-link');

    expect(this.element.querySelector('.select-link')).not.to.exist;
    expect(this.element.querySelector('.text-link')).to.exist;

    await click('.chk-external-link');

    expect(this.element.querySelector('.select-link')).to.exist;
    expect(this.element.querySelector('.text-link')).not.to.exist;
  });
});
