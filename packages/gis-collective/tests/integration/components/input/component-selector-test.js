import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import click from '@ember/test-helpers/dom/click';

describe('Integration | Component | input/component-selector', function () {
  setupRenderingTest();

  it('lists the default category components', async function () {
    await render(hbs`<Input::ComponentSelector />`);

    const buttons = this.element.querySelectorAll(
      '.group-default .btn-outline-dark'
    );

    expect(buttons).to.have.length(6);
    expect(buttons[0].querySelector('.label').textContent.trim()).to.equal(
      'banner'
    );
    expect(buttons[1].querySelector('.label').textContent.trim()).to.equal(
      'blocks'
    );
    expect(buttons[2].querySelector('.label').textContent.trim()).to.equal(
      'picture'
    );
    expect(buttons[3].querySelector('.label').textContent.trim()).to.equal(
      'pictures'
    );
    expect(buttons[4].querySelector('.label').textContent.trim()).to.equal(
      'sounds'
    );
    expect(buttons[5].querySelector('.label').textContent.trim()).to.equal(
      'menu'
    );
  });

  it('lists the article category components', async function () {
    await render(hbs`<Input::ComponentSelector />`);

    const buttons = this.element.querySelectorAll(
      '.group-article .btn-outline-dark'
    );

    expect(buttons).to.have.length(1);
    expect(buttons[0].querySelector('.label').textContent.trim()).to.equal(
      'article'
    );
  });

  it('lists the campaign category components', async function () {
    await render(hbs`<Input::ComponentSelector />`);

    const buttons = this.element.querySelectorAll(
      '.group-campaign .btn-outline-dark'
    );

    expect(buttons).to.have.length(1);
    expect(buttons[0].querySelector('.label').textContent.trim()).to.equal(
      'campaign card list'
    );
  });

  it('lists the campaign category components', async function () {
    await render(hbs`<Input::ComponentSelector />`);

    const buttons = this.element.querySelectorAll(
      '.group-icon .btn-outline-dark'
    );

    expect(buttons).to.have.length(1);
    expect(buttons[0].querySelector('.label').textContent.trim()).to.equal(
      'icons'
    );
  });

  it('triggers on change with the component name when a button is pressed', async function () {
    let value = '';
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::ComponentSelector @onChange={{this.change}}/>`);

    await click('.btn-banner');

    expect(value).to.equal('banner');
  });

  it('renders the button as selected when the value matches the name', async function () {
    await render(hbs`<Input::ComponentSelector @value="banner"/>`);

    const selectedButtons = this.element.querySelectorAll(
      '.group-default .btn-dark'
    );

    expect(selectedButtons).to.have.length(1);
    expect(
      selectedButtons[0].querySelector('.label').textContent.trim()
    ).to.equal('banner');
  });

  it('allows to unselect a component', async function () {
    let value = '';
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::ComponentSelector @onChange={{this.change}}/>`);

    await click('.btn-none');

    expect(value).to.equal(null);
  });
});
