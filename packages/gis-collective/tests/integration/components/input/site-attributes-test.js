import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, fillIn } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/site-attributes', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<Input::SiteAttributes />`);
    expect(this.element.textContent.trim()).to.equal('Group');
  });

  describe('when there are no attributes', function () {
    let value;

    beforeEach(function () {
      this.set('attributes', {});

      this.set('save', (v) => {
        value = v;
      });
    });

    it('allows to add a new group', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-new-anonymous-group');
      await click('.btn-save');

      expect(value).to.deep.equal({
        'New Group 1': {},
      });
    });

    it('allows to add a new group with an attribute', async function () {
      await render(hbs`<Input::SiteAttributes
      @attributes={{this.attributes}}
      @onSave={{this.save}} />`);

      await click('.btn-new-anonymous-group');
      await click('.btn-add-new-attribute');
      await click('.btn-save');

      expect(value).to.deep.equal({
        'New Group 1': { 'new attribute': '' },
      });
    });

    it('allows to add a new group with an attribute and value', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-new-anonymous-group');
      await click('.btn-add-new-attribute');

      await fillIn('.input-attribute-name', 'b');
      await fillIn('.input-attribute-value', '3');
      await fillIn('.input-group-name', 'group');

      await click('.btn-save');

      expect(value).to.deep.equal({
        group: { b: '3' },
      });
    });
  });

  describe('when there is an object attribute', function () {
    let value;

    beforeEach(function () {
      this.set('attributes', {
        key: { a: 1 },
      });

      this.set('save', (v) => {
        value = v;
      });
    });

    it('allows to remove an object attribute', async function () {
      await render(hbs`<Input::SiteAttributes
      @attributes={{this.attributes}}
      @onSave={{this.save}} />`);

      await click('.btn-edit');
      await click('.input-icon-attribute .btn-delete');
      await click('.btn-save');

      expect(value).to.deep.equal({
        key: {},
      });
    });

    it('allows to rename an attribute', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-attribute-name', 'b');
      await click('.btn-save');

      expect(value).to.deep.equal({
        key: { b: 1 },
      });
    });

    it('allows to update a value', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-attribute-value', 2);
      await click('.btn-save');

      expect(value).to.deep.equal({
        key: { a: '2' },
      });
    });

    it('allows to update the key', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-group-key', 'newKey');
      await click('.btn-save');

      expect(value).to.deep.equal({
        newKey: { a: 1 },
      });
    });

    it('allows to add a new attribute', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');
      await click('.btn-add-attribute');

      const names = this.element.querySelectorAll('.input-attribute-name');
      const values = this.element.querySelectorAll('.input-attribute-value');

      await fillIn(names[1], 'b');
      await fillIn(values[1], '3');

      await click('.btn-save');

      expect(value).to.deep.equal({ key: { a: 1, b: '3' } });
    });

    it('allows to delete the group', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');
      await click('.btn-delete-group');
      await click('.btn-save');

      expect(value).to.deep.equal({});
    });
  });

  describe('when there is an object with dots in attributes keys', function () {
    let value;

    beforeEach(function () {
      this.set('attributes', {
        'key nr.': { 'a.b': 1 },
      });

      this.set('save', (v) => {
        value = v;
      });
    });

    it('allows to rename an attribute', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-attribute-name', 'b');
      await click('.btn-save');

      expect(value).to.deep.equal({
        'key nr.': { b: 1 },
      });
    });

    it('allows to update a value', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-attribute-value', 2);
      await click('.btn-save');

      expect(value).to.deep.equal({
        'key nr.': { 'a.b': '2' },
      });
    });

    it('allows to update the key', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-group-key', 'newKey');
      await click('.btn-save');

      expect(value).to.deep.equal({
        newKey: { 'a.b': 1 },
      });
    });
  });

  describe('when there is an item list', function () {
    let value;

    beforeEach(function () {
      value = null;
      this.set('attributes', {
        key: [{ a: 1 }, { a: 2 }],
      });

      this.set('save', (v) => {
        value = v;
      });
    });

    it('allows to remove an item in list', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');
      await click('.input-icon-attribute .btn-delete');
      await click('.btn-save');

      expect(value).to.deep.equal({
        key: [{}, { a: 2 }],
      });
    });

    it('allows to rename an item in list', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-attribute-name', 'b');
      await click('.btn-save');

      expect(value).to.deep.equal({
        key: [{ b: 1 }, { a: 2 }],
      });
    });

    it('allows to update a value', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-attribute-value', 2);
      await click('.btn-save');

      expect(value).to.deep.equal({
        key: [{ a: '2' }, { a: 2 }],
      });
    });

    it('allows to update the key', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');
      await fillIn('.input-group-key', 'newKey');
      await click('.btn-save');

      expect(value).to.deep.equal({
        newKey: [{ a: 1 }, { a: 2 }],
      });
    });

    it('allows to add a new attribute', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');
      await click('.btn-add-attribute');

      const names = this.element.querySelectorAll('.input-attribute-name');
      const values = this.element.querySelectorAll('.input-attribute-value');

      await fillIn(names[1], 'b');
      await fillIn(values[1], '3');

      await click('.btn-save');

      expect(value).to.deep.equal({ key: [{ a: 1, b: '3' }, { a: 2 }] });
    });

    it('allows to delete a group', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-edit');
      await click('.btn-delete-group');
      await click('.btn-save');

      expect(value).to.deep.equal({
        key: [{ a: 2 }],
      });
    });

    it('allows to add a new item', async function () {
      await render(hbs`<Input::SiteAttributes
        @attributes={{this.attributes}}
        @onSave={{this.save}} />`);

      await click('.btn-add-group');
      await click('.btn-add-attribute');

      await fillIn('.input-attribute-name', 'b');
      await fillIn('.input-attribute-value', '3');

      await click('.btn-save');

      expect(value).to.deep.equal({
        key: [{ a: 1 }, { a: 2 }, { b: '3' }],
      });
    });
  });

  describe('when there is a required attribute', function () {
    beforeEach(function () {
      let icon = {};
      icon.name = 'icon3';
      icon.localName = 'local icon 3';
      icon.attributes = [
        {
          name: 'attribute3',
          help: 'some help message',
          displayName: 'local attribute3',
          type: 'short text',
          isRequired: true
        },
      ];

      this.set('icons', [icon]);

      this.set('attributes', {
        icon3: { attribute3: 'value' },
      });

      this.set('save', () => { });
    });

    it('displays an asterisk next to the attribute name', async function () {
      await render(hbs`<Input::SiteAttributes
      @icons={{this.icons}}
      @attributes={{this.attributes}}
      @onSave={{this.save}} />`);

      expect(this.element.querySelector(".is-required-asterisk")).to.exist;

      await click('.btn-edit');
      expect(this.element.querySelector(".is-required-asterisk")).to.exist;
    });

    it('restores the value on save', async function () {
      await render(hbs`<Input::SiteAttributes
      @icons={{this.icons}}
      @attributes={{this.attributes}}
      @onSave={{this.save}} />`);

      await click('.btn-edit');

      await fillIn('.input-attribute-value', '');
      await click('.btn-save');

      expect(this.element.querySelector(".input-icon-attribute").textContent).to.contain("value");
    })
  });
});
