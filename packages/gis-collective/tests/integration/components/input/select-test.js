import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/select', function() {
  setupRenderingTest();

  it('renders an empty option by default', async function() {
    await render(hbs`<Input::Select />`);

    expect(this.element.textContent.trim()).to.equal('');

    expect(this.element.querySelector("option")).to.exist;

    expect(this.element.querySelector("option").textContent.trim()).to.equal("");
    expect(this.element.querySelector("option")).to.have.attribute("value", null)
  });

  it('does not render the empty value when there is a selected value', async function() {
    this.set("values", [{id: 1, name: "name1"}, {id: 2, name: "name2"}]);
    this.set("value", 1);

    await render(hbs`<Input::Select @list={{this.values}} @value={{this.value}}/>`);

    expect(this.element.querySelectorAll("option").length).to.equal(2);
    expect(this.element.querySelectorAll("option")[0].textContent.trim()).to.equal("name1");
  });

  it('renders a list of objects', async function() {
    this.set("values", [{id: 1, name: "name1"}, {id: 2, name: "name2"}]);
    await render(hbs`<Input::Select @list={{this.values}}/>`);

    expect(this.element.querySelectorAll("option").length).to.equal(3);
    expect(this.element.querySelectorAll("option")[1].textContent.trim()).to.equal("name1");
    expect(this.element.querySelectorAll("option")[2].textContent.trim()).to.equal("name2");

    expect(this.element.querySelectorAll("option")[1]).to.have.attribute("value", "1");
    expect(this.element.querySelectorAll("option")[2]).to.have.attribute("value", "2");
  });

  it('renders a list of strings', async function() {
    this.set("values", ["name1", "name2"]);

    await render(hbs`<Input::Select @list={{this.values}}/>`);

    expect(this.element.querySelectorAll("option").length).to.equal(3);
    expect(this.element.querySelectorAll("option")[1].textContent.trim()).to.equal("name1");
    expect(this.element.querySelectorAll("option")[2].textContent.trim()).to.equal("name2");

    expect(this.element.querySelectorAll("option")[1]).to.have.attribute("value", "name1");
    expect(this.element.querySelectorAll("option")[2]).to.have.attribute("value", "name2");
  });

  it('renders a selected object', async function() {
    this.set("values", [{id: 1, name: "name1"},{id: 2, name: "name2"}]);
    this.set("value", 1);

    await render(hbs`<Input::Select @list={{this.values}} @value={{this.value}}/>`);

    expect(this.element.querySelector("select").value).to.equal("1");
  });

  it('renders a selected string', async function() {
    this.set("values", [ "name1", "name2" ]);
    this.set("value", "name1");

    await render(hbs`<Input::Select @list={{this.values}} @value={{this.value}}/>`);

    expect(this.element.querySelector("select").value).to.equal("name1");
  });

  it('triggers the change action', async function() {
    this.set("values", [ "name1", "name2" ]);

    let result;
    this.set("change", (value) => {
      result = value;
    });

    await render(hbs`<Input::Select @list={{this.values}} @onChange={{this.change}}/>`);

    this.element.querySelector("select").value = "name1";
    await triggerEvent('select', 'change');

    expect(result).to.equal("name1");
  });
});
