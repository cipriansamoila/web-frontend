import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { find, click, render, settled } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/button', function () {
  setupRenderingTest();

  it('renders async success;', async function () {
    let deferred = { resolve: undefined };
    this.action = () => new Promise((resolve) => (deferred.resolve = resolve));

    await render(hbs`
      <Input::Button @action={{this.action}} data-test-foo>
        some text
      </Input::Button>
    `);

    await click('[data-test-foo]');

    expect(
      find('[data-test-foo]').firstElementChild.classList.toString()
    ).to.include('fa-spin');

    // Using resolve this way doesn't integrate with Ember's test iteration the
    // way that RSVP Promises in general do, so we need to use `await settled()`
    // for the next "tick" in the run loop to finish and thereby to flush out
    // the render.
    deferred.resolve();
    await settled();

    expect(find('[data-test-foo]').innerText.trim()).to.equal('some text');
  });

  it('renders async failure;', async function () {
    let deferred = { reject: undefined };
    this.action = () => new Promise((_, reject) => (deferred.reject = reject));

    await render(hbs`<Input::Button @action={{this.action}} data-test-foo />`);

    await click('[data-test-foo]');

    expect(this.element.querySelector('.svg-inline--fa')).to.have.class(
      'fa-spin'
    );
    // Using resolve this way doesn't integrate with Ember's test iteration the
    // way that RSVP Promises in general do, so we need to use `await settled()`
    // for the next "tick" in the run loop to finish and thereby to flush out
    // the render.
    deferred.reject();
    await settled();

    expect(this.element.querySelector('.svg-inline--fa')).to.have.class(
      'fa-exclamation-triangle'
    );
  });
});
