import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/icon selector', function() {
  setupRenderingTest();

  it('renders an alert and no size icon group', async function() {
    await render(hbs`<Input::Icon />`);

    expect(this.element.querySelector(".size")).not.to.exist;
    expect(this.element.querySelector(".btn-select-icon")).to.exist;
    expect(this.element.querySelector(".alert-select-icon")).to.exist;
  });

  it('renders a disabled add button when the image is not set and disabled=true', async function() {
    await render(hbs`<Input::Icon @disabled={{true}}/>`);

    expect(this.element.querySelector(".size")).not.to.exist;
    expect(this.element.querySelector(".btn-select-icon")).to.exist;
    expect(this.element.querySelector(".btn-select-icon")).to.have.attribute("disabled", "");
    expect(this.element.querySelector(".alert-select-icon")).to.exist;
  });

  it('renders does not render an alert when isNew=true', async function() {
    await render(hbs`<Input::Icon @isNew={{true}} />`);

    expect(this.element.querySelector(".size")).not.to.exist;
    expect(this.element.querySelector(".btn-select-icon")).to.exist;
    expect(this.element.querySelector(".alert-select-icon")).not.to.exist;
  });

  it('renders the select file when a value is set', async function() {
    this.set("value", {value: "some value", useParent: false });
    await render(hbs`<Input::Icon @value={{this.value}} />`);

    const images = this.element.querySelectorAll('img');

    expect(images).to.have.length(3);
    expect(images[0].src).to.contain("some%20value");
    expect(images[1].src).to.contain("some%20value");
    expect(images[2].src).to.contain("some%20value");

    expect(this.element.querySelector(".btn-select-icon")).not.to.exist;
    expect(this.element.querySelector(".btn-use-parent-icon")).not.to.exist;
    expect(this.element.querySelector(".btn-replace-icon")).to.exist;
    expect(this.element.querySelector(".btn-replace-icon")).to.have.attribute("disabled", null);
  });

  it('renders a disabled the edit button when a value is set and is disabled', async function() {
    this.set("value", {value: "some value", useParent: false });
    await render(hbs`<Input::Icon @value={{this.value}} @disabled={{true}}/>`);

    expect(this.element.querySelector(".btn-select-icon")).not.to.exist;
    expect(this.element.querySelector(".btn-replace-icon")).to.exist;
    expect(this.element.querySelector(".btn-replace-icon")).to.have.attribute("disabled", "")
  });

  it('renders the use parent icon when the parentValue is set', async function() {
    this.set("value", {value: "some value", useParent: false });
    this.set("parentValue", {value: "some parent value", useParent: false });

    let newValue;
    this.set("onChange", (value) => {
      newValue = value;
    });

    await render(hbs`<Input::Icon @value={{this.value}} @onChange={{this.onChange}} @parentValue={{this.parentValue}}/>`);

    await click(".btn-use-parent-icon");

    expect(this.element.querySelector(".btn-replace-icon")).to.exist;
    expect(this.element.querySelector(".btn-use-parent-icon")).to.exist;
    expect(newValue.useParent).to.be.true;
  });

  it('sets the use parent to false when a new image is selected', async function() {
    this.set("value", {value: "", useParent: true });

    let newValue;
    this.set("onChange", (value) => {
      newValue = value;
    });

    await render(hbs`<Input::Icon @value={{this.value}} @onChange={{this.onChange}} @parentValue={{this.parentValue}}/>`);

    let blob = new Blob(["foo", "bar"], { type: 'image/svg' });
    blob.name = "foobar";

    await triggerEvent("input[type='file']", "change", { files: [blob] });

    expect(newValue.useParent).to.be.false;
  });

  it('renders the add button when use parent is false and the icon value equals the parent value', async function() {
    this.set("value", {value: "some parent value", useParent: false });
    this.set("parentValue", {value: "some parent value", useParent: false });

    await render(hbs`<Input::Icon @value={{this.value}} @onUseParent={{this.onUseParent}} @parentValue={{this.parentValue}}/>`);

    expect(this.element.querySelector(".btn-use-parent-icon")).to.exist;
    expect(this.element.querySelector(".btn-use-parent-icon")).to.have.attribute("disabled", null);
    expect(this.element.querySelector(".size")).not.to.exist;
    expect(this.element.querySelector(".btn-select-icon")).to.exist;
    expect(this.element.querySelector(".btn-replace-icon")).not.to.exist;
  });
});
