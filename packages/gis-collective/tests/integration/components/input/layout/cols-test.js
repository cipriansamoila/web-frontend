import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/layout-cols', function () {
  setupRenderingTest();

  it('renders nothing when there are no columns', async function () {
    await render(hbs`<Input::Layout::Cols />`);
    expect(this.element.querySelector('.col')).not.to.exist;
  });

  it('renders a column when is set', async function () {
    this.set('cols', [{ type: '', options: [] }]);

    await render(hbs`<Input::Layout::Cols @cols={{this.cols}}/>`);
    expect(this.element.querySelectorAll('.col')).to.have.length(1);

    expect(this.element.querySelector('.col')).to.have.attribute(
      'class',
      'col layout-col-editable'
    );

    expect(
      this.element.querySelector('.bs-col-width-desktop').textContent.trim()
    ).to.equal('-');
    expect(
      this.element.querySelector('.bs-col-width-tablet').textContent.trim()
    ).to.equal('-');
    expect(
      this.element.querySelector('.bs-col-width-mobile').textContent.trim()
    ).to.equal('-');
  });

  it('renders the column widths when they are set', async function () {
    this.set('cols', [
      { type: '', options: ['col-1', 'col-md-2', 'col-lg-3'] },
    ]);

    await render(hbs`<Input::Layout::Cols @cols={{this.cols}}/>`);

    expect(this.element.querySelector('.col')).to.have.attribute(
      'class',
      'col layout-col-editable col-1 col-md-2 col-lg-3'
    );

    expect(
      this.element.querySelector('.bs-col-width-desktop').textContent.trim()
    ).to.equal('3');
    expect(
      this.element.querySelector('.bs-col-width-tablet').textContent.trim()
    ).to.equal('2');
    expect(
      this.element.querySelector('.bs-col-width-mobile').textContent.trim()
    ).to.equal('1');
  });

  it('renders two columns when they are set', async function () {
    this.set('cols', [
      { type: '', options: [] },
      { type: '', options: [] },
    ]);

    await render(hbs`<Input::Layout::Cols @cols={{this.cols}}/>`);
    expect(this.element.querySelectorAll('.col')).to.have.length(2);
  });

  it('adds a col at the end when the last plus is pressed', async function () {
    this.set('cols', [
      { type: '1', options: [] },
      { type: '2', options: [] },
    ]);

    let value;
    this.set('onLayoutChange', (v) => {
      value = JSON.parse(JSON.stringify(v));
    });

    await render(
      hbs`<Input::Layout::Cols @cols={{this.cols}} @onLayoutChange={{this.onLayoutChange}}/>`
    );

    expect(this.element.querySelectorAll('.btn-add-col-2')).to.have.length(1);

    await click('.btn-add-col-2');

    expect(value).to.deep.equal([
      { type: '1', options: [] },
      { type: '2', options: [] },
      { type: '', options: [] },
    ]);
  });

  it('deletes a col at the end when the last delete is pressed', async function () {
    this.set('cols', [
      { type: '1', options: [] },
      { type: '2', options: [] },
    ]);

    let value;
    this.set('onLayoutChange', (v) => {
      value = JSON.parse(JSON.stringify(v));
    });

    await render(
      hbs`<Input::Layout::Cols @cols={{this.cols}} @onLayoutChange={{this.onLayoutChange}}/>`
    );

    expect(this.element.querySelectorAll('.btn-delete-col-2')).to.have.length(
      1
    );

    await click('.btn-delete-col-2');

    expect(value).to.deep.equal([{ type: '1', options: [] }]);
  });

  it('selects a col on click', async function () {
    this.set('cols', [
      { type: '1', options: [] },
      { type: '2', options: [] },
    ]);

    let value;
    this.set('onSelect', (v) => {
      value = JSON.parse(JSON.stringify(v));
    });

    await render(
      hbs`<Input::Layout::Cols @cols={{this.cols}} @onSelect={{this.onSelect}}/>`
    );

    const cols = this.element.querySelectorAll('.btn-col-options');

    await click(cols[1]);

    expect(value).to.deep.equal(1);
  });
});
