import { expect } from 'chai';
import { describe, it, beforeEach, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../../helpers/test-server';

describe('Integration | Component | input/layout/rows', function () {
  setupRenderingTest();
  let server;

  before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle('1');
    server.testData.storage.addDefaultPicture('1');
  });

  after(function () {
    server.shutdown();
  });

  beforeEach(function () {
    this.set('rows', [
      {
        options: [],
        cols: [
          {
            type: 'type',
            data: {},
            options: [],
          },
          {
            type: 'type',
            data: {},
            options: [],
          },
        ],
      },
    ]);
  });

  it('allows adding a row when there is nothing', async function () {
    let value;
    this.set('onLayoutChange', (v) => {
      value = JSON.parse(JSON.stringify(v));
    });

    this.set('rows', []);

    await render(hbs`<Input::Layout::Rows @rows={{this.rows}} @onLayoutChange={{this.onLayoutChange}} as | col |>
          {{col.data.id}}
        </Input::Layout::Rows>`);

    await click('.btn-add-first-row');

    expect(value).to.deep.equal([
      { cols: [{ type: '', options: [] }], options: [] },
    ]);
  });

  it('allows adding a row after the first row', async function () {
    let value;
    this.set('onLayoutChange', (v) => {
      value = JSON.parse(JSON.stringify(v));
    });

    await render(hbs`<Input::Layout::Rows @rows={{this.rows}} @onLayoutChange={{this.onLayoutChange}} as | col |>
          {{col.data.id}}
        </Input::Layout::Rows>`);

    await click('.btn-add-row-1');

    expect(value).to.deep.equal([
      {
        options: [],
        cols: [
          { type: 'type', data: {}, options: [] },
          { type: 'type', data: {}, options: [] },
        ],
      },
      { cols: [{ type: '', options: [] }], options: [] },
    ]);
  });

  it('allows adding a row before the first row', async function () {
    let value;
    this.set('onLayoutChange', (v) => {
      value = JSON.parse(JSON.stringify(v));
    });

    await render(hbs`<Input::Layout::Rows @rows={{this.rows}} @onLayoutChange={{this.onLayoutChange}} as | col |>
          {{col.data.id}}
        </Input::Layout::Rows>`);

    await click('.row');
    await click('.btn-add-row-0');

    expect(value).to.deep.equal([
      { cols: [{ type: '', options: [] }], options: [] },
      {
        options: [],
        cols: [
          { type: 'type', data: {}, options: [] },
          { type: 'type', data: {}, options: [] },
        ],
      },
    ]);
  });

  it('allows adding a col on the selected row', async function () {
    let value;
    this.set('onLayoutChange', (v) => {
      value = JSON.parse(JSON.stringify(v));
    });

    await render(hbs`<Input::Layout::Rows @rows={{this.rows}} @onLayoutChange={{this.onLayoutChange}} as | col |>
          {{col.data.id}}
        </Input::Layout::Rows>`);

    await click('.row');
    await click('.btn-add-col');

    expect(value).to.deep.equal([
      {
        options: [],
        cols: [
          { type: 'type', data: {}, options: [] },
          { type: 'type', data: {}, options: [] },
          { type: '', options: [] },
        ],
      },
    ]);
  });

  it('deletes the row when the last col is deleted', async function () {
    let value;
    this.set('onLayoutChange', (v) => {
      this.set('layout', v);
      value = v;
    });

    await render(hbs`<Input::Layout::Rows @rows={{this.rows}} @onLayoutChange={{this.onLayoutChange}} as | col |>
          {{col.data.id}}
        </Input::Layout::Rows>`);

    await click('.row');
    await click('.btn-delete-col');
    await click('.btn-delete-col');

    expect(value).to.have.length(0);
  });

  it('shows the first and second add row button when the first row is selected', async function () {
    this.set('rows', [
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
    ]);

    await render(hbs`<Input::Layout::Rows @rows={{this.rows}} @onLayoutChange={{this.onLayoutChange}} as | col |>
          {{col.data.id}}
        </Input::Layout::Rows>`);

    const rows = this.element.querySelectorAll('.row');

    expect(this.element.querySelector('.row-layout-settings.show')).not.to
      .exist;
    await click(rows[0]);

    expect(
      this.element.querySelectorAll('.row-layout-settings.show')
    ).to.have.length(2);
    expect(
      this.element.querySelectorAll('.row-layout-settings.show .btn-add-row-0')
    ).to.have.length(1);
    expect(
      this.element.querySelectorAll('.row-layout-settings.show .btn-add-row-1')
    ).to.have.length(1);
  });

  it('shows the last two add row buttons when the last row is selected', async function () {
    this.set('rows', [
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
    ]);

    await render(hbs`<Input::Layout::Rows @rows={{this.rows}} @onLayoutChange={{this.onLayoutChange}} as | col |>
          {{col.data.id}}
        </Input::Layout::Rows>`);

    const rows = this.element.querySelectorAll('.row');

    expect(this.element.querySelector('.row-layout-settings.show')).not.to
      .exist;
    await click(rows[2]);

    expect(
      this.element.querySelectorAll('.row-layout-settings.show')
    ).to.have.length(2);
    expect(
      this.element.querySelectorAll('.row-layout-settings.show .btn-add-row-2')
    ).to.have.length(1);
    expect(
      this.element.querySelectorAll('.row-layout-settings.show .btn-add-row-3')
    ).to.have.length(1);
  });

  it('shows the first row as selected when is clicked', async function () {
    this.set('rows', [
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
    ]);

    await render(hbs`<Input::Layout::Rows @rows={{this.rows}} @onLayoutChange={{this.onLayoutChange}} as | col |>
      {{col.data.id}}
    </Input::Layout::Rows>`);

    const rows = this.element.querySelectorAll('.row');

    expect(this.element.querySelectorAll('.row-selected')).to.have.length(0);

    await click(rows[0]);

    expect(this.element.querySelectorAll('.row-selected')).to.have.length(1);
  });

  it('can select a col with a click', async function () {
    this.set('rows', [
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
    ]);

    await render(hbs`<Input::Layout::Rows @rows={{this.rows}} @onLayoutChange={{this.onLayoutChange}} as | col |>
      {{col.data.id}}
    </Input::Layout::Rows>`);

    const cols = this.element.querySelectorAll('.btn-col-options');

    expect(this.element.querySelectorAll('.col-selected')).to.have.length(0);

    await click(cols[1]);

    expect(this.element.querySelectorAll('.col-selected')).to.have.length(1);
  });

  it('triggers the onRowEdit even when the button is clicked', async function () {
    this.set('rows', [
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
      { cols: [{ type: '', options: [] }], options: [] },
    ]);

    let value;
    this.set('onRowEdit', function (v) {
      value = v;
    });

    await render(hbs`<Input::Layout::Rows @rows={{this.rows}} @onRowEdit={{this.onRowEdit}} as | col |>
      {{col.data.id}}
    </Input::Layout::Rows>`);

    const btns = this.element.querySelectorAll('.btn-edit-row');
    await click(btns[1]);

    expect(value).to.equal(1);
  });
});
