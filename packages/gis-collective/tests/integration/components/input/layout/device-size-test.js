import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import click from '@ember/test-helpers/dom/click';

describe('Integration | Component | input/layout/device-size', function () {
  setupRenderingTest();

  it('renders the unselected buttons when no value is set', async function () {
    await render(hbs`<Input::Layout::DeviceSize />`);

    const buttons = this.element.querySelectorAll('.btn-outline-secondary');
    expect(buttons).to.have.length(3);
  });

  it('renders the desktop button selected when the value is desktop', async function () {
    await render(hbs`<Input::Layout::DeviceSize @value="desktop"/>`);

    expect(this.element.querySelector('.btn-desktop')).to.have.class(
      'btn-secondary'
    );

    const buttons = this.element.querySelectorAll('.btn-outline-secondary');
    expect(buttons).to.have.length(2);
  });

  it('renders the tablet button selected when the value is tablet', async function () {
    await render(hbs`<Input::Layout::DeviceSize @value="tablet"/>`);

    expect(this.element.querySelector('.btn-tablet')).to.have.class(
      'btn-secondary'
    );

    const buttons = this.element.querySelectorAll('.btn-outline-secondary');
    expect(buttons).to.have.length(2);
  });

  it('renders the mobile button selected when the value is mobile', async function () {
    await render(hbs`<Input::Layout::DeviceSize @value="mobile"/>`);

    expect(this.element.querySelector('.btn-mobile')).to.have.class(
      'btn-secondary'
    );

    const buttons = this.element.querySelectorAll('.btn-outline-secondary');
    expect(buttons).to.have.length(2);
  });

  it('triggers onChange when the desktop button is clicked', async function () {
    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::Layout::DeviceSize @onChange={{this.change}}/>`);
    await click('.btn-desktop');

    expect(value).to.equal('desktop');
  });

  it('triggers onChange when the tablet button is clicked', async function () {
    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::Layout::DeviceSize @onChange={{this.change}}/>`);
    await click('.btn-tablet');

    expect(value).to.equal('tablet');
  });

  it('triggers onChange when the mobile button is clicked', async function () {
    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(hbs`<Input::Layout::DeviceSize @onChange={{this.change}}/>`);
    await click('.btn-mobile');

    expect(value).to.equal('mobile');
  });

  it('triggers onChange with null when the selected value is clicked', async function () {
    let value;
    this.set('change', (v) => {
      value = v;
    });

    await render(
      hbs`<Input::Layout::DeviceSize @onChange={{this.change}} @value="mobile" />`
    );
    await click('.btn-mobile');

    expect(value).to.equal(null);
  });
});
