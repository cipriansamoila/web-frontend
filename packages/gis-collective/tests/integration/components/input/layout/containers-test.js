import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/layout/containers', function () {
  setupRenderingTest();

  beforeEach(function () {
    this.set('containers', [
      {
        rows: [{ cols: [{ type: 'type', options: [] }], options: [] }],
        options: [],
      },
    ]);
  });

  it('renders a container with a row and col', async function () {
    await render(hbs`
    <Input::Layout::Containers
      @containers={{this.containers}}
      @onLayoutChange={{this.layoutChange}}
      @onColSelect={{this.onColSelect}}
      @onRowEdit={{this.onRowEdit}}
    />`);

    expect(this.element.querySelector('.container')).to.exist;
    expect(this.element.querySelector('.container .row')).to.exist;
    expect(this.element.querySelector('.container .row .col')).to.exist;
  });

  it('can add a new col', async function () {
    let value;
    this.set('layoutChange', (v) => {
      value = JSON.parse(JSON.stringify(v));
    });

    await render(hbs`
    <Input::Layout::Containers
      @containers={{this.containers}}
      @onLayoutChange={{this.layoutChange}}
      @onColSelect={{this.onColSelect}}
      @onRowEdit={{this.onRowEdit}}
    />`);

    await click('.row');
    await click('.btn-add-col');

    expect(value).to.deep.equal([
      {
        rows: [
          {
            cols: [
              { type: 'type', options: [] },
              { type: '', options: [] },
            ],
            options: [],
          },
        ],
        options: [],
      },
    ]);
  });

  it('can select a col', async function () {
    let containerIndex;
    let rowIndex;
    let colIndex;
    this.set('onColSelect', (a, b, c) => {
      containerIndex = a;
      rowIndex = b;
      colIndex = c;
    });

    await render(hbs`
    <Input::Layout::Containers
      @containers={{this.containers}}
      @onLayoutChange={{this.layoutChange}}
      @onColSelect={{this.onColSelect}}
      @onRowEdit={{this.onRowEdit}}
    />`);

    await click('.row');
    await click('.btn-col-options');

    expect(containerIndex).to.equal(0);
    expect(rowIndex).to.equal(0);
    expect(colIndex).to.equal(0);
  });

  it('can select a row', async function () {
    let containerIndex;
    let rowIndex;
    let colIndex;
    this.set('onRowEdit', (a, b, c) => {
      containerIndex = a;
      rowIndex = b;
      colIndex = c;
    });

    await render(hbs`
    <Input::Layout::Containers
      @containers={{this.containers}}
      @onLayoutChange={{this.layoutChange}}
      @onColSelect={{this.onColSelect}}
      @onRowEdit={{this.onRowEdit}}
    />`);

    await click('.btn-edit-row');

    expect(containerIndex).to.equal(0);
    expect(rowIndex).to.equal(0);
    expect(colIndex).to.be.undefined;
  });

  it('can add a new container at the end', async function () {
    let value;
    this.set('layoutChange', (v) => {
      value = JSON.parse(JSON.stringify(v));
    });

    await render(hbs`
    <Input::Layout::Containers
      @containers={{this.containers}}
      @onLayoutChange={{this.layoutChange}}
      @onColSelect={{this.onColSelect}}
      @onRowEdit={{this.onRowEdit}}
    />`);

    await click('.btn-add-container-1');

    expect(value).to.deep.equal([
      {
        rows: [{ cols: [{ type: 'type', options: [] }], options: [] }],
        options: [],
      },
      {
        rows: [{ cols: [{ type: '', options: [] }], options: [] }],
        options: [],
      },
    ]);
  });

  it('can add a new container on top', async function () {
    let value;
    this.set('layoutChange', (v) => {
      value = JSON.parse(JSON.stringify(v));
    });

    await render(hbs`
    <Input::Layout::Containers
      @containers={{this.containers}}
      @onLayoutChange={{this.layoutChange}}
      @onColSelect={{this.onColSelect}}
      @onRowEdit={{this.onRowEdit}}
    />`);

    await click('.btn-add-container-0');

    expect(value).to.deep.equal([
      {
        rows: [{ cols: [{ type: '', options: [] }], options: [] }],
        options: [],
      },
      {
        rows: [{ cols: [{ type: 'type', options: [] }], options: [] }],
        options: [],
      },
    ]);
  });

  it('can remove a container', async function () {
    let value;
    this.set('layoutChange', (v) => {
      value = JSON.parse(JSON.stringify(v));
    });

    await render(hbs`
    <Input::Layout::Containers
      @containers={{this.containers}}
      @onLayoutChange={{this.layoutChange}}
      @onColSelect={{this.onColSelect}}
      @onRowEdit={{this.onRowEdit}}
    />`);

    await click('.btn-remove-container-0');

    expect(value).to.deep.equal([]);
  });

  it('triggers an event when the config button is pressed', async function () {
    let value;
    this.set('onContainerEdit', (v) => {
      value = JSON.parse(JSON.stringify(v));
    });

    await render(hbs`
    <Input::Layout::Containers
      @containers={{this.containers}}
      @onLayoutChange={{this.layoutChange}}
      @onColSelect={{this.onColSelect}}
      @onRowEdit={{this.onRowEdit}}
      @onContainerEdit={{this.onContainerEdit}}
    />`);

    await click('.btn-config-container-0');

    expect(value).to.deep.equal(0);
  });
});
