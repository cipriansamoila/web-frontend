import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, triggerEvent, fillIn } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/user-name', function() {
  setupRenderingTest();

  it('renders the default input states', async function() {
    await render(hbs`<Input::UserName />`);

    expect(this.element.querySelector(".name-salutation label").textContent.trim()).to.equal('Salutation');
    expect(this.element.querySelector(".name-title label").textContent.trim()).to.equal('Title');
    expect(this.element.querySelector(".name-first label").textContent.trim()).to.equal('First name');
    expect(this.element.querySelector(".name-last label").textContent.trim()).to.equal('Last name');
  });

  it('renders the provided values', async function() {
    this.set("value", {
      salutation: "ms",
      title: "dr",
      firstName: "first",
      lastName: "last"
    })

    await render(hbs`<Input::UserName @value={{this.value}}/>`);

    expect(this.element.querySelector(".name-salutation select").value).to.equal('ms');
    expect(this.element.querySelector(".name-title input").value).to.equal('dr');
    expect(this.element.querySelector(".name-first input").value).to.equal('first');
    expect(this.element.querySelector(".name-last input").value).to.equal('last');
  });

  it('triggers on change event when the salutation is changed', async function() {
    let value;

    this.set("change", (v) => {
      value = v
    });

    await render(hbs`<Input::UserName @onChange={{this.change}}/>`);

    this.element.querySelector(".name-salutation select").value = "ms";
    await triggerEvent(".name-salutation select", "change");

    expect(value).to.deep.equal({
      salutation: "ms",
      title: "",
      firstName: "",
      lastName: ""
    });
  });

  it('triggers on change event when the title is changed', async function() {
    let value;

    this.set("change", (v) => {
      value = v
    });

    await render(hbs`<Input::UserName @onChange={{this.change}}/>`);

    await fillIn(".name-title input", "title");

    expect(value).to.deep.equal({
      salutation: "",
      title: "title",
      firstName: "",
      lastName: ""
    });
  });

  it('triggers on change event when the firstName is changed', async function() {
    let value;

    this.set("change", (v) => {
      value = v
    });

    await render(hbs`<Input::UserName @onChange={{this.change}}/>`);

    await fillIn(".name-first input", "value");

    expect(value).to.deep.equal({
      salutation: "",
      title: "",
      firstName: "value",
      lastName: ""
    });
  });

  it('triggers on change event when the lastName is changed', async function() {
    let value;

    this.set("change", (v) => {
      value = v
    });

    await render(hbs`<Input::UserName @onChange={{this.change}}/>`);

    await fillIn(".name-last input", "value");

    expect(value).to.deep.equal({
      salutation: "",
      title: "",
      firstName: "",
      lastName: "value"
    });
  });
});
