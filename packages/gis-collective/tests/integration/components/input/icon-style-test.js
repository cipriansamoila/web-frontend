import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, fillIn, click, triggerEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { tracked } from '@glimmer/tracking';

class MockValue {
  @tracked borderColor = ""
  @tracked backgroundColor = ""
  @tracked borderWidth = 0
  @tracked lineDash = []
  @tracked size = 0
  @tracked shape = ""
}

describe('Integration | Component | input/icon-style', function() {
  setupRenderingTest();

  it('renders all fields when they are set', async function() {
    this.set("value", new MockValue());

    await render(hbs`<Input::IconStyle @value={{this.value}}/>`);

    expect(this.element.querySelectorAll("input,select").length).to.equal(6);

    expect(this.element.querySelector(".icon-style-border-color")).to.exist;
    expect(this.element.querySelector(".icon-style-background-color")).to.exist;
    expect(this.element.querySelector(".icon-style-border-width")).to.exist;
    expect(this.element.querySelector(".icon-style-line-dash")).to.exist;
    expect(this.element.querySelector(".icon-style-size")).to.exist;
    expect(this.element.querySelector(".icon-style-shape")).to.exist;
  });

  it('renders no fields when they are not set', async function() {
    await render(hbs`<Input::IconStyle/>`);

    expect(this.element.querySelectorAll("input,select").length).to.equal(0);
  });

  it('renders all values', async function() {
    this.set("value", {
      borderColor: "blue",
      backgroundColor: "transparent",
      borderWidth: 1,
      lineDash: [1, 2, 3],
      size: 10,
      shape: "square",
    });

    await render(hbs`<Input::IconStyle @value={{this.value}}/>`);

    expect(this.element.querySelector(".icon-style-border-color").value).to.equal("blue");
    expect(this.element.querySelector(".icon-style-background-color").value).to.equal("transparent");
    expect(this.element.querySelector(".icon-style-border-width").value).to.equal("1");
    expect(this.element.querySelector(".icon-style-size").value).to.equal("10");
    expect(this.element.querySelector(".icon-style-shape").value).to.equal("square");
  });

  it('renders the css style colors', async function() {
    const value = new MockValue();
    value.borderColor = "blue";
    value.backgroundColor = "red";
    value.borderWidth = 1;
    value.lineDash = [1, 2, 3];

    this.set("value", value);

    await render(hbs`<Input::IconStyle @value={{this.value}}/>`);

    expect(this.element.querySelector(".icon-style-border-color-color")).to.have.attribute("style", "background-color: blue");
    expect(this.element.querySelector(".icon-style-background-color-color")).to.have.attribute("style", "background-color: red");
  });

  it('updates the border color value', async function() {
    const value = new MockValue();
    value.borderColor = "blue";
    value.backgroundColor = "transparent";
    value.borderWidth = 1;
    value.lineDash = [1, 2, 3];

    this.set("value", value);

    this.set("change", (value) => {
      this.set("value", value)
    });

    await render(hbs`<Input::IconStyle @value={{this.value}} @onChange={{this.change}}/>`);

    await fillIn(".icon-style-border-color", "new color");
    await click(".icon-style-border-width");

    this.timeout(100000000);

    expect(this.element.querySelector(".icon-style-border-color").value).to.equal("new color");
    expect(this.value.borderColor).to.equal("new color");
  });

  it('updates the background color value', async function() {
    const value = new MockValue();
    value.borderColor = "blue";
    value.backgroundColor = "transparent";
    value.borderWidth = 1;
    value.lineDash = [1, 2, 3];

    this.set("value", value);

    this.set("change", (value) => {
      this.set("value", value)
    });

    await render(hbs`<Input::IconStyle @value={{this.value}} @onChange={{this.change}}/>`);

    await fillIn(".icon-style-background-color", "new color");
    await click(".icon-style-border-width");

    expect(this.element.querySelector(".icon-style-background-color").value).to.equal("new color");
    expect(this.value.backgroundColor).to.equal("new color");
  });

  it('updates the border width value', async function() {

    const value = new MockValue();
    value.borderColor = "blue";
    value.backgroundColor = "transparent";
    value.borderWidth = 1;
    value.lineDash = [1, 2, 3];

    this.set("value", value);

    this.set("change", (value) => {
      this.set("value", value)
    });

    await render(hbs`<Input::IconStyle @value={{this.value}} @onChange={{this.change}}/>`);

    await fillIn(".icon-style-border-width", "2");
    await click(".icon-style-border-color");

    expect(this.element.querySelector(".icon-style-border-width").value).to.equal("2");
    expect(this.value.borderWidth).to.equal(2);
  });

  it('updates the line dash value', async function() {
    const value = new MockValue();
    value.borderColor = "blue";
    value.backgroundColor = "transparent";
    value.borderWidth = 1;
    value.lineDash = [1, 2, 3];

    this.set("value", value);

    this.set("change", (value) => {
      this.set("value", value)
    });

    await render(hbs`<Input::IconStyle @value={{this.value}} @onChange={{this.change}}/>`);

    await fillIn(".icon-style-line-dash", "10,20,30");
    await click(".icon-style-border-color");

    expect(this.element.querySelector(".icon-style-line-dash").value).to.equal("10, 20, 30");
    expect(this.value.lineDash).to.deep.equal([10, 20, 30]);
  });

  it('updates the size value', async function() {
    const value = new MockValue();
    value.borderColor = "blue";
    value.backgroundColor = "transparent";
    value.size = 1;

    this.set("value", value);

    this.set("change", (value) => {
      this.set("value", value)
    });

    await render(hbs`<Input::IconStyle @value={{this.value}} @onChange={{this.change}}/>`);

    await fillIn(".icon-style-size", "50");
    await click(".icon-style-border-color");

    expect(this.element.querySelector(".icon-style-size").value).to.equal("50");
    expect(this.value.size).to.equal(50);
  });

  it('updates the shape value', async function() {
    const value = new MockValue();
    value.borderColor = "blue";
    value.shape = "circle";

    this.set("value", value);

    this.set("change", (value) => {
      this.set("value", value)
    });

    await render(hbs`<Input::IconStyle @value={{this.value}} @onChange={{this.change}}/>`);

    this.element.querySelector(".icon-style-shape").value = "square";
    await triggerEvent('.icon-style-shape', 'change');

    expect(this.value.shape).to.equal("square");
  });
});
