import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, fillIn, click, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import PageElements from '../../../helpers/page-elements';

describe('Integration | Component | input/icon-style-all', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Input::IconStyleAll />`);

    expect(PageElements.getTextFor("h3")).to.deep.equal(['Points', 'Lines', 'Polygons']);
    expect(PageElements.getTextFor("h4")).to.deep.equal(['Marker', 'Label', 'Marker', 'Line', 'Label', 'Marker', 'Polygon', 'Label']);
    expect(this.element.querySelectorAll("input").length).to.equal(55);
  });

  it('renders all values', async function() {
    this.set("value", {
      "polygonBorderMarker": { "backgroundColor": "#ffffff", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#000001" },
      "lineMarker": { "backgroundColor": "#ffffff", "lineDash": [], "borderWidth": 1, "borderColor": "#000002" },
      "polygonMarker": { "backgroundColor": "#ffffff", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#000003" },
      "polygon": { "backgroundColor": "#ffffff", "lineDash": [], "borderWidth": 1, "borderColor": "#000004" },
      "site": { "backgroundColor": "#ffffff", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#000005" },
      "line": { "backgroundColor": "#ffffff", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#000006" },
      "polygonBorder": { "backgroundColor": "#ffffff", "lineDash": [], "borderWidth": 1, "borderColor": "#000007" }
    });

    await render(hbs`<Input::IconStyleAll @value={{this.value}}/>`);

    const colorList = this.element.querySelectorAll(".icon-style-border-color");

    const values = [...colorList].map(a => a.value);

    expect(values).to.eql([ "#000005", "#fff", "#000002", "#000006", "#fff", "#000003", "#000004", "#fff" ]);
  });

  it('triggers change when point bg color is changed', async function() {
    this.set("value", {
      "polygonBorderMarker": { "backgroundColor": "#ffffff", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#000001" },
      "lineMarker": { "backgroundColor": "#ffffff", "lineDash": [], "borderWidth": 1, "borderColor": "#000002" },
      "polygonMarker": { "backgroundColor": "#ffffff", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#000003" },
      "polygon": { "backgroundColor": "#ffffff", "lineDash": [], "borderWidth": 1, "borderColor": "#000004" },
      "site": { "backgroundColor": "#ffffff", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#000005" },
      "line": { "backgroundColor": "#ffffff", "lineDash": [], "borderWidth": 1, "borderColor": "#000006" },
      "polygonBorder": { "backgroundColor": "#ffffff", "lineDash": [], "borderWidth": 1, "borderColor": "#000007" }
    });

    let receivedValue;
    this.set("change", (value) => {
      receivedValue = value.toJSON();
      this.set("receivedValue", receivedValue);
    });

    await render(hbs`<Input::IconStyleAll @value={{this.value}} @onChange={{this.change}}/>`);

    await fillIn(".col-site .icon-style-background-color", "#000001");
    await click(".icon-style-border-width");

    await click(".col-site .icon-style-background-color");
    await click(".icon-style-border-width");

    await waitUntil(() => receivedValue);

    expect(receivedValue.site).to.deep.equal({ isVisible: true, "backgroundColor": "#000001", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#000005" });
  });

  it('triggers change when line bg color is changed', async function() {
    this.set("value", {
      "polygonBorderMarker": { "backgroundColor": "#ffffff", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#000001" },
      "lineMarker": { "backgroundColor": "#ffffff", "lineDash": [], "borderWidth": 1, "borderColor": "#000002" },
      "polygonMarker": { "backgroundColor": "#ffffff", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#000003" },
      "polygon": { "backgroundColor": "#ffffff", "lineDash": [], "borderWidth": 1, "borderColor": "#000004" },
      "site": { "backgroundColor": "#ffffff", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "#000005" },
      "line": { "backgroundColor": "#ffffff", "lineDash": [], "borderWidth": 1, "borderColor": "#000006" },
      "polygonBorder": { "backgroundColor": "#ffffff", "lineDash": [], "borderWidth": 1, "borderColor": "#000007" }
    });

    this.set("change", (value) => {
      this.set("value", value);
    });

    await render(hbs`<Input::IconStyleAll @value={{this.value}} @onChange={{this.change}}/>`);

    await fillIn(".col-line .icon-style-background-color", "#000001");
    await click(".icon-style-border-width");

    await click(".col-line .icon-style-background-color");
    await click(".icon-style-border-width");

    expect(this.value.line).to.deep.contain({
      _style: null,
      backgroundColor: "#000001",
      lineDash: [],
      borderWidth: 1,
      borderColor: "#000006"
    });
  });
});
