import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/icons/list', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Input::Icons::List />`);
    expect(this.element.textContent.trim()).to.contain('primary');
    expect(this.element.textContent.trim()).not.to.contain('secondary');
  });
});
