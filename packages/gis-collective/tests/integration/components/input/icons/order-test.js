import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, tap, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { move } from 'ember-drag-sort/utils/trigger';

describe('Integration | Component | input/icons/order', function() {
  setupRenderingTest();

  it('can change the order of two items', async function() {
    this.set("value", [
      {image: { value: "data:image/png;base64,picture1", useParent: false }},
      {image: { value: "data:image/png;base64,picture2", useParent: false }}]);

    let value;
    this.set("change", (newValue) => {
      value = newValue;
    });

    await render(hbs`<Input::Icons::Order @onChange={{this.change}} @value={{this.value}} />`);

    const list = document.querySelector('.dragSortList')
    await move(list, 0, list, 1, false, "img");

    expect(value).to.have.length(2);
    expect(value[0].image.value).to.startWith("data:image/png;base64,picture2");
    expect(value[1].image.value).to.startWith("data:image/png;base64,picture1");
  });

  it('sets the primary icon on touch', async function() {
    this.set("value", [
      { image: { value: "data:image/png;base64,picture1", useParent: false }},
      { image: { value: "data:image/png;base64,picture2", useParent: false }}
    ]);

    let icons;
    this.set("change", (newValue) => {
      icons = newValue;
    });

    await render(hbs`<Input::Icons::Order @onChange={{this.change}} @value={{this.value}} />`);

    const images = document.querySelectorAll('.icon-container img');

    await tap(images[1]);

    expect(icons).to.have.length(2);
    expect(icons[0].image.value).to.startWith("data:image/png;base64,picture2");
    expect(icons[1].image.value).to.startWith("data:image/png;base64,picture1");
  });

  it('triggers the onSelect action when the on select icon button is clicked', async function() {
    let selected;

    this.set("select", () => {
      selected = true;
    });

    await render(hbs`<Input::Icons::Order @onSelect={{this.select}} />`);
    await click(".icon-select");

    expect(selected).to.be.true;
  });

  it('can delete items', async function() {
    this.set("value", [{image: "data:image/png;base64,picture1", useParent: false }, {image: "data:image/png;base64,picture2", useParent: false }]);

    let value;
    this.set("change", (newValue) => {
      value = newValue;
    });

    await render(hbs`<Input::Icons::Order @onChange={{this.change}} @value={{this.value}} />`);
    const buttons = this.element.querySelectorAll(".btn-remove-icon");

    await click(buttons[0]);

    expect(value).to.have.length(1);
    expect(value[0].image).to.startWith("data:image/png;base64,picture2");
  });
});
