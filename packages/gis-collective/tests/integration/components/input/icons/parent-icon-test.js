import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/icons/parent-icon', function() {
  setupRenderingTest();

  it('renders nothing by default', async function() {
    await render(hbs`<Input::Icons::ParentIcon />`);

    expect(this.element.textContent.trim()).to.equal('');
  });
});
