import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | input/icons/list-toggle', function() {
  setupRenderingTest();
  let allIcons = [];

  beforeEach(() => {
    allIcons = [];


    for(let i=0; i < 5; i++) {
      allIcons.push({
        id: `${i}`,
        image: "image",
        localName: `Monument istoric ${i}`,
        subcategory: "Monumente",
        name: "historical monument",
        category: "Cultură"
      });
    }
  });

  it('renders nothing when no icon is present', async function() {
    await render(hbs`<Input::Icons::ListToggle />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders 5 icons when they are set', async function() {
    this.set("list", allIcons);

    await render(hbs`<Input::Icons::ListToggle @list={{this.list}}/>`);

    const names = [...this.element.querySelectorAll(".icon-name")].map(a => a.textContent.trim());

    expect(this.element.querySelectorAll(".icon-element")).to.have.length(5);
    expect(names).to.deep.equal(["Monument istoric 0", "Monument istoric 1", "Monument istoric 2", "Monument istoric 3", "Monument istoric 4"]);
    expect(this.element.querySelector(".is-selected")).not.to.exist;
  });

  it('renders the selected icons', async function() {
    this.set("list", allIcons);
    this.set("value", allIcons);

    await render(hbs`<Input::Icons::ListToggle @list={{this.list}} @value={{this.value}} />`);
    expect(this.element.querySelectorAll(".is-selected")).to.have.length(5);
  });

  it('selects an unselected icon', async function() {
    this.set("list", allIcons);
    this.set("value", allIcons);

    let value;
    this.set("change", (v) => {
      value = v;
    })

    await render(hbs`<Input::Icons::ListToggle @list={{this.list}} @onChange={{this.change}} />`);

    const buttons = this.element.querySelectorAll(".icon-element");

    await click(buttons[0]);

    expect(value).to.deep.equal([ allIcons[0] ]);
  });

  it('deselects a selected icon', async function() {
    this.set("list", allIcons);
    this.set("value", allIcons);

    let value;
    this.set("change", (v) => {
      value = v;
    })

    await render(hbs`<Input::Icons::ListToggle @list={{this.list}} @value={{this.value}} @onChange={{this.change}} />`);

    const buttons = this.element.querySelectorAll(".icon-element");

    await click(buttons[0]);

    expect(value).to.deep.equal([ allIcons[1], allIcons[2], allIcons[3], allIcons[4] ]);
  });
});
