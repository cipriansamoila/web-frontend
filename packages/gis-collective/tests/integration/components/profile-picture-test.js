import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | profile-picture', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<ProfilePicture />`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector(".is-square")).not.to.exist;
  });

  it('renders a square picture', async function() {
    await render(hbs`<ProfilePicture @isSquare={{true}}/>`);
    expect(this.element.querySelector(".is-square")).to.exist;
  });
});
