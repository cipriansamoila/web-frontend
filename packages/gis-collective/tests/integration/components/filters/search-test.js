import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, fillIn, click, triggerKeyEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | filters/search', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<Filters::Search />`);

    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector('.input-group-search .input-search')).to
      .exist;
    expect(this.element.querySelector('.input-group-search .btn-search')).to
      .exist;
    expect(
      this.element.querySelector('.input-group-search .btn-search .fa-search')
    ).to.exist;
  });

  it('renders the value', async function () {
    await render(hbs`<Filters::Search @value="test value"/>`);

    expect(this.element.textContent.trim()).to.equal('');
    expect(
      this.element.querySelector('.input-group-search .input-search').value
    ).to.equal('test value');
  });

  it('triggers the onSearch action by pressing on search button', async function () {
    let term;

    this.set('onSearch', function (v) {
      term = v;
    });

    await render(
      hbs`<Filters::Search @value="test value" @onSearch={{this.onSearch}} />`
    );
    await fillIn('.input-search', 'search string');
    await click('.btn-search');

    expect(term).to.equal('search string');
  });

  it('triggers the onSearch action by pressing enter key', async function () {
    let term;

    this.set('onSearch', function (v) {
      term = v;
    });

    await render(
      hbs`<Filters::Search @value="test value" @onSearch={{this.onSearch}} />`
    );
    await fillIn('.input-search', 'search string');

    await triggerKeyEvent('.input-search', 'keyup', 'Enter');

    expect(term).to.equal('search string');
  });

  it('does not trigger the onSearch action if the value is not changed', async function () {
    let searched = false;

    this.set('onSearch', function () {
      searched = true;
    });

    await render(
      hbs`<Filters::Search @value="test value" @onSearch={{this.onSearch}} />`
    );
    await click('.btn-search');

    expect(searched).not.to.equal(true);
  });

  it('triggers the onType action by typing in the search input', async function () {
    let term;

    this.set('onType', function (v) {
      term = v;
    });

    await render(
      hbs`<Filters::Search @value="test value" @onType={{this.onType}} />`
    );
    await fillIn('.input-search', 'test');

    expect(term).to.equal('test');
  });

  it('shows the spinner when isSearching is true', async function () {
    await render(hbs`<Filters::Search @isSearching={{true}} />`);

    expect(
      this.element.querySelector('.input-group-search .btn-search .fa-search')
    ).not.to.exist;
    expect(
      this.element.querySelector('.input-group-search .btn-search .fa-spinner')
    ).to.exist;
  });

  it('shows the suggestions popup on focus', async function () {
    await render(
      hbs`<Filters::Search @hasSuggestions={{true}}>test</Filters::Search>`
    );

    expect(this.element.querySelector('.search-suggestions')).not.to.exist;
    await click('.input-search');

    expect(this.element.querySelector('.search-suggestions')).to.exist;
    expect(this.element.textContent.trim()).to.equal('test');
  });

  it('does not show the suggestions popup on focus when it is disabled', async function () {
    await render(
      hbs`<Filters::Search @hasSuggestions={{false}}>test</Filters::Search>`
    );

    expect(this.element.querySelector('.search-suggestions')).not.to.exist;
    await click('.input-search');

    expect(this.element.querySelector('.search-suggestions')).not.to.exist;
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('should be able to reset the value', async function () {
    await render(hbs`<Filters::Search @value="test"/>`);

    expect(this.element.querySelector('.input-search').value).to.equal('test');
    await fillIn('.input-search', '');

    expect(this.element.querySelector('.input-search').value).to.equal('');
  });
});
