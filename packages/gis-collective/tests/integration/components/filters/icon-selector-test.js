import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | filters/icon-selector', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Filters::IconSelector />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders an icon', async function() {
    this.set("icons", [{
      _id: "5d66e7089fe9780100ea96d5",
      image: {value: "image", useParent: false},
      localName: "Monument istoric",
      subcategory: "Monumente",
      name: "historical monument",
      category: "Cultură"
    }]);

    await render(hbs`<Filters::IconSelector @icons={{this.icons}}/>`);

    expect(this.element.querySelectorAll(".icon-element").length).to.equal(1);
    expect(this.element.querySelector(".icon-element.selected")).not.to.exist;
    expect(this.element.querySelector(".icon-selector .icon-name").textContent.trim()).to.equal('Monument istoric');
    expect(this.element.querySelector(".icon-selector .icon-image")).to.have.attribute('src', 'image/md');
  });

  it('renders an icon as selected when is in the value list', async function() {
    this.set("icons", [{
      _id: "5d66e7089fe9780100ea96d5",
      image: {value: "image", useParent: false},
      localName: "Monument istoric",
      subcategory: "Monumente",
      name: "historical monument",
      category: "Cultură"
    }]);

    this.set("value", [{
      _id: "5d66e7089fe9780100ea96d5",
      image: {value: "image", useParent: false},
      localName: "Monument istoric",
      subcategory: "Monumente",
      name: "historical monument",
      category: "Cultură"
    }]);

    await render(hbs`<Filters::IconSelector @icons={{this.icons}} @value={{this.value}}/>`);

    expect(this.element.querySelectorAll(".icon-element").length).to.equal(1);
    expect(this.element.querySelectorAll(".icon-element.selected").length).to.equal(1);
    expect(this.element.querySelector(".icon-selector .icon-name").textContent.trim()).to.equal('Monument istoric');
    expect(this.element.querySelector(".icon-selector .icon-image")).to.have.attribute('src', 'image/md');

    this.set("value", []);
    expect(this.element.querySelectorAll(".icon-element").length).to.equal(1);
    expect(this.element.querySelectorAll(".icon-element.selected").length).to.equal(0);
  });

  it('renders an icon as hidden when it is removed from the list', async function() {
    this.set("icons", [{
      id: "5d66e7089fe9780100ea96d5",
      image: {value: "image", useParent: false},
      localName: "Monument istoric",
      subcategory: "Monumente",
      name: "historical monument",
      category: "Cultură"
    }]);

    await render(hbs`<Filters::IconSelector @icons={{this.icons}}/>`);
    expect(this.element.querySelector(".icon-element")).not.to.have.class("d-none");

    this.set("icons", []);

    expect(this.element.querySelectorAll(".icon-element").length).to.equal(1);
    expect(this.element.querySelector(".icon-element")).to.have.class("d-none");
  });

  it('triggers onDeselect when a selected icon is clicked', async function() {
    let value;
    let icon = {
      _id: "5d66e7089fe9780100ea96d5",
      image: {value: "image", useParent: false},
      localName: "Monument istoric",
      subcategory: "Monumente",
      name: "historical monument",
      category: "Cultură"
    };

    this.set("icons", [ icon ]);
    this.set("value", [ icon ]);

    this.set("onDeselect", function(selectedIcon) {
      value = selectedIcon;
    });

    await render(hbs`<Filters::IconSelector @onDeselect={{this.onDeselect}} @icons={{this.icons}} @value={{this.value}}/>`);
    await click(".btn-icon");

    expect(value).to.deep.eq(icon);
  });

  it('triggers onSelect when an icon is clicked', async function() {
    let value;
    let icon = {
      _id: "5d66e7089fe9780100ea96d5",
      image: {value: "image", useParent: false},
      localName: "Monument istoric",
      subcategory: "Monumente",
      name: "historical monument",
      category: "Cultură"
    };

    this.set("icons", [ icon ]);

    this.set("onSelect", function(selectedIcon) {
      value = selectedIcon;
    });

    await render(hbs`<Filters::IconSelector @onSelect={{this.onSelect}} @icons={{this.icons}}/>`);
    await click(".btn-icon");

    expect(value).to.deep.eq(icon);
  });
});
