import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | filters/visibility', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Filters::Visibility />`);
    expect(this.element.textContent.trim()).to.equal('visibility');
  });

  it('renders the popup with options when the button is pressed', async function() {
    await render(hbs`<Filters::Visibility />`);
    await click(".btn-pill-visibility");

    expect(this.element.querySelector(".btn-show-published.btn-outline-secondary")).to.exist;
    expect(this.element.querySelector(".btn-show-unpublished.btn-outline-secondary")).to.exist;

    expect(this.element.querySelector(".btn-show-published").textContent.trim()).to.equal("only published");
    expect(this.element.querySelector(".btn-show-unpublished").textContent.trim()).to.equal("only unpublished");
  });

  it('renders the popup with the published button selected when the value is true', async function() {
    await render(hbs`<Filters::Visibility @value="true"/>`);
    expect(this.element.textContent.trim()).to.equal('only published');

    await click(".btn-pill-visibility");

    expect(this.element.querySelector(".btn-show-published.btn-secondary")).to.exist;
    expect(this.element.querySelector(".btn-show-unpublished.btn-outline-secondary")).to.exist;
  });

  it('renders the popup with the unpublished button selected when the value is false', async function() {
    await render(hbs`<Filters::Visibility @value="false"/>`);
    expect(this.element.textContent.trim()).to.equal('only unpublished');

    await click(".btn-pill-visibility");

    expect(this.element.querySelector(".btn-show-published.btn-outline-secondary")).to.exist;
    expect(this.element.querySelector(".btn-show-unpublished.btn-secondary")).to.exist;
  });

  it('triggers the onChange action with true when the published button is pressed', async function() {
    let newValue;
    this.set("onChange", function(value) {
      newValue = value;
    });

    await render(hbs`<Filters::Visibility @onChange={{this.onChange}}/>`);
    await click(".btn-pill-visibility");
    await click(".btn-show-published");

    expect(newValue).to.equal("true");
  });

  it('triggers the onChange action with false when the unpublished button is pressed', async function() {
    let newValue;
    this.set("onChange", function(value) {
      newValue = value;
    });

    await render(hbs`<Filters::Visibility @onChange={{this.onChange}}/>`);
    await click(".btn-pill-visibility");
    await click(".btn-show-unpublished");

    expect(newValue).to.equal("false");
  });

  it('triggers the onChange action with null when the reset button is pressed', async function() {
    let newValue;
    this.set("onChange", function(value) {
      newValue = value;
    });

    await render(hbs`<Filters::Visibility @onChange={{this.onChange}} @value="true"/>`);
    await click(".btn-reset");

    expect(newValue).to.equal(null);
  });
});
