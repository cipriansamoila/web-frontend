import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, fillIn, triggerKeyEvent } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | filters/map-search-bis', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<Filters::MapSearchBis />`);
    expect(this.element.querySelector('.btn-back')).not.to.exist;
    expect(this.element.querySelector('.filter-badge')).not.to.exist;
    expect(this.element.querySelector('.input-group-icon')).not.to.exist;
  });

  it('shows a spinner while is waiting for search results', async function () {
    await render(
      hbs`<Filters::MapSearchBis @onSearch={{this.search}} @term={{this.term}} @onChange={{this.change}} @isSearching={{true}}/>`
    );

    expect(this.element.querySelector('.fa-spinner')).to.exist;
  });

  it('does nothing after enter key is pressed and the input is empty', async function () {
    let resolve;

    this.set('search', () => {
      return new Promise((r) => {
        resolve = r;
      });
    });

    await render(hbs`<Filters::MapSearchBis @onSearch={{this.search}} />`);
    await fillIn('.input-search', '');

    await triggerKeyEvent(
      this.element.querySelector('.input-search'),
      'keyup',
      'Enter'
    );

    expect(resolve).to.be.undefined;
    expect(this.element.querySelector('.fa-spinner')).not.to.exist;
  });

  it('hides the btn show results when it is searching', async function () {
    await render(
      hbs`<Filters::MapSearchBis @term="test" @isSearching={{true}}/>`
    );

    expect(this.element.querySelector('.btn-show-search')).not.to.exist;
  });

  it('triggers the onChange event while the term is typed in the search field', async function () {
    let value = '';

    this.set('onChange', (v) => {
      value = v;
    });

    await render(hbs`<Filters::MapSearchBis @onChange={{this.onChange}}/>`);

    await fillIn('.input-search', 'test');

    expect(value).to.equal('test');
  });

  it('triggers the onActivate event when the input is activated', async function () {
    let value = '';

    this.set('onActivate', () => {
      value = true;
    });

    await render(hbs`<Filters::MapSearchBis @onActivate={{this.onActivate}}/>`);

    await click('.input-search');

    expect(value).to.equal(true);
  });

  it('triggers the onDeactivate event when the input is blurred', async function () {
    let value = '';

    this.set('onDeactivate', () => {
      value = true;
    });

    await render(
      hbs`<Filters::MapSearchBis @onDeactivate={{this.onDeactivate}}/>`
    );

    await click('.input-search');
    await click(this.element.querySelector('.map-search-bis').parentElement);

    expect(value).to.equal(true);
  });
});
