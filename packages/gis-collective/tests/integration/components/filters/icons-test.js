import { expect } from 'chai';
import { describe, it, afterEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import Modal from '../../../helpers/modal';

describe('Integration | Component | filters/icons', function() {
  setupRenderingTest();

  afterEach(() => {
    Modal.clear();
  });

  it('renders', async function() {
    await render(hbs`<Filters::Icons />`);
    expect(this.element.textContent.trim()).to.contain('icon');
  });

  it('shows the modal on click', async function() {
    this.set("adapter", {
      async group() {
        return  {
          "categories": {
            "key": {
              "icons": [{id:"1"},{id:"2"}]
            }
          }
        };
      }
    });

    await render(hbs`<div id="modal-target"></div><Filters::Icons @adapter={{this.adapter}}/>`);

    await click(".btn-select");

    await waitFor(".modal.fade.show");
    await Modal.waitToDisplay();

    await click(".btn-secondary");
    await Modal.waitToHide();

    expect(this.element.querySelectorAll(".icon-element").length).to.equal(2);
  });

  it('loads all icons when icon sets are not ', async function() {
    let iconSets;

    this.set("adapter", {
      async group(params) {
        iconSets = params;
        return {};
      }
    });

    await render(hbs`<div id="modal-target"></div><Filters::Icons @adapter={{this.adapter}}/>`);

    await click(".btn-select");

    await waitFor(".modal.fade.show");
    await Modal.waitToDisplay();

    await click(".btn-secondary");
    await Modal.waitToHide();

    expect(iconSets).to.deep.equal({});
  });

  it('loads all icons when icon sets are not ', async function() {
    let iconSets;

    this.set("adapter", {
      async group(params) {
        iconSets = params;
        return {};
      }
    });

    this.set("iconSets", ["1","2"])

    await render(hbs`<div id="modal-target"></div><Filters::Icons @iconSets={{this.iconSets}} @adapter={{this.adapter}}/>`);

    await click(".btn-select");

    await waitFor(".modal.fade.show");
    await Modal.waitToDisplay();

    await click(".btn-secondary");
    await Modal.waitToHide();

    expect(iconSets).to.deep.equal({
      "iconSet": "1,2"
    });
  });

  it('selects an icon on click', async function() {
    this.set("adapter", {
      async group() {
        return  {
          "categories": {
            "key": {
              "icons": [{id:"1"},{id:"2"}]
            }
          }
        };
      }
    });

    let icon;
    this.set("change", (value) => {
      icon = value;
    });

    await render(hbs`<div id="modal-target"></div><Filters::Icons @adapter={{this.adapter}} @onChange={{this.change}}/>`);

    await click(".btn-select");

    await Modal.waitToDisplay();

    await click(".btn-collapse");
    await click(".btn-icon");

    await Modal.waitToHide();

    expect(icon).to.deep.equal({ id: "1" });
  });

  it('shows the icon name when it is set', async function() {
    this.set("icon", {
      localName: "local name"
    });

    await render(hbs`<Filters::Icons @icon={{this.icon}} />`);

    expect(this.element.querySelector(".btn-pill-icon").textContent.trim()).to.equal("with the local name icon");
  });

  it("deselects an icon on click", async function() {
    this.set("icon", {
      localName: "local name"
    });

    let newValue = {};
    this.set("change", (value) => {
      newValue = value;
    });

    await render(hbs`<Filters::Icons @icon={{this.icon}} @onChange={{this.change}} />`);
    await click(".btn-reset-pill-icon");

    expect(newValue).to.equal(null);
  });
});
