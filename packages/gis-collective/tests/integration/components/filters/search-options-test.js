import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import {
  render,
  click,
  triggerKeyEvent,
  waitUntil,
  waitFor,
  typeIn,
  fillIn,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | filters/search-options', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<Filters::SearchOptions />`);
    expect(this.element.textContent.trim()).to.equal('not set');
  });

  it('can set a title', async function () {
    await render(hbs`<Filters::SearchOptions @title="title" />`);

    expect(this.element.textContent.trim()).to.equal('title');
    expect(
      this.element.querySelector('.btn.btn-pill-title').textContent.trim()
    ).to.equal('title');
  });

  it('can set a value', async function () {
    await render(
      hbs`<Filters::SearchOptions @title="title" @value="value" @niceValue="value" />`
    );

    expect(this.element.textContent.trim()).to.equal('value');
    expect(
      this.element.querySelector('.btn.btn-pill-title').textContent.trim()
    ).to.equal('value');
    expect(this.element.querySelector('.btn.btn-reset-pill-title')).to.exist;
  });

  it('triggers the on hide action when the reset button is pressed', async function () {
    let closed;

    this.set('onHide', function () {
      closed = true;
    });

    await render(
      hbs`<Filters::SearchOptions @title="title" @value="value" @niceValue="value" @onHide={{this.onHide}}/>`
    );
    await click('.btn-pill');

    await waitFor('.btn-reset');
    await click('.btn-reset');

    expect(closed).to.eq(true);
  });

  it('clicking the reset button should trigger the on reset action', async function () {
    let reset;

    this.set('onReset', function () {
      reset = true;
    });

    await render(
      hbs`<Filters::SearchOptions @title="title" @value="value" @niceValue="value" @onReset={{this.onReset}}/>`
    );
    await click('.btn-reset');

    expect(reset).to.eq(true);
  });

  it('shows the popover when the pill is clicked', async function () {
    await render(hbs`<Filters::SearchOptions @title="title" @value="value"/>`);
    await click('.btn-pill');

    expect(this.element.querySelector('.popover')).to.exist;
    expect(this.element.querySelector('.popover input.input-filter')).to.exist;
  });

  it('triggers the on select action when the popup is shown', async function () {
    let selected;

    this.set('onSearch', function (value) {
      selected = value;
    });

    await render(
      hbs`<Filters::SearchOptions @title="title" @value="value" @onSearch={{this.onSearch}}/>`
    );
    await click('.btn-pill');

    expect(selected).to.eq('value');
  });

  it('shows a spinner when the search returns a promise', async function () {
    this.set('onSearch', function () {
      return { then() {} };
    });

    await render(
      hbs`<Filters::SearchOptions @title="title" @value="value" @onSearch={{this.onSearch}}/>`
    );
    await click('.btn-pill');
    await fillIn(this.element.querySelector('input'), 'other value');

    expect(this.element.querySelector('.spinner')).to.exist;
  });

  it('hides the spinner when the search promise is resolved', async function () {
    this.set('onSearch', function () {
      return {
        then(a) {
          a();
        },
      };
    });

    await render(
      hbs`<Filters::SearchOptions @title="title" @value="value" @onSearch={{this.onSearch}}/>`
    );
    await click('.btn-pill');
    await fillIn(this.element.querySelector('input'), 'other value');

    expect(this.element.querySelector('.spinner')).not.to.exist;
  });

  it('hides the spinner when the search promise is rejected', async function () {
    this.set('onSearch', function () {
      return {
        catch(a) {
          a();
        },
      };
    });

    await render(
      hbs`<Filters::SearchOptions @title="title" @value="value" @onSearch={{this.onSearch}}/>`
    );
    await click('.btn-pill');
    await fillIn(this.element.querySelector('input'), 'other value');

    expect(this.element.querySelector('.spinner')).not.to.exist;
  });

  it('renders the body inside the popover', async function () {
    await render(
      hbs`<Filters::SearchOptions @title="title" @value="value">custom body</Filters::SearchOptions>`
    );
    await click('.btn-pill');

    expect(this.element.querySelector('.popover')).to.exist;
    expect(this.element.querySelector('.popover input.input-filter')).to.exist;
    expect(this.element.querySelector('.popover').textContent.trim()).to.equal(
      'custom body'
    );
  });

  it('resets the input value when the pill is pressed twice', async function () {
    await render(hbs`<Filters::SearchOptions @title="title" @value="value"/>`);
    await click('.btn-pill');

    await fillIn('input', 'other value');

    await click('.btn-pill');
    await click('.btn-pill');

    expect(this.element.querySelector('input').value).to.equal('value');
    expect(this.element.querySelector('.popover')).to.exist;
  });

  it('resets the internal state when the filter is closed', async function () {
    await render(hbs`<Filters::SearchOptions @title="title" @value="value"/>`);
    await click('.btn-pill');
    await click('.btn-pill');

    expect(this.element.querySelector('.popover')).not.to.exist;
  });

  it('hides the popup on Escape key', async function () {
    let selected;

    this.set('onHide', function () {
      selected = true;
    });

    await render(hbs`<Filters::SearchOptions
      @title="title"
      @onHide={{this.onHide}} />`);

    await click(this.element.querySelector('.btn-pill'));
    await triggerKeyEvent(
      this.element.querySelector('input'),
      'keyup',
      'Escape'
    );

    expect(selected).to.exist;
    expect(this.element.querySelector('.popover')).not.to.exist;
  });

  it('triggers on search when a value is typed in the input', async function () {
    let selected;

    this.set('onSearch', function (value) {
      selected = value;
    });

    await render(hbs`<Filters::SearchOptions
      @title="title"
      @onSearch={{this.onSearch}} />`);

    await click(this.element.querySelector('.btn-pill'));
    await typeIn(this.element.querySelector('input'), 'hello');

    expect(selected).to.eq('hello');
  });

  it('triggers on select on enter key', async function () {
    let selected = '';
    let searched = '';

    this.set('onSearch', function (value) {
      searched = value;
    });

    this.set('onSelect', function (value) {
      selected = value;
    });

    await render(hbs`<Filters::SearchOptions
      @cls="btn-outline-secondary"
      @onSearch={{this.onSearch}}
      @onSelect={{this.onSelect}}
      @clsValue="btn-secondary" />`);

    await click(this.element.querySelector('.btn-select'));
    await fillIn(this.element.querySelector('input'), 'value 1');
    await triggerKeyEvent(
      this.element.querySelector('input'),
      'keyup',
      'Enter'
    );

    await waitUntil(() => searched != '');

    expect(selected).to.equal('value 1');
    expect(searched).to.equal('value 1');
    expect(this.element.querySelector('.popover')).not.to.exist;
  });
});
