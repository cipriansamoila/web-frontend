import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import hbs from 'htmlbars-inline-precompile';

describe("Integration | Component | filters/pill", function() {
  setupRenderingTest();

  it("renders an empty value", async function() {
    await render(hbs`<Filters::Pill />`);

    expect(this.element.textContent.trim()).to.equal('no title');
  });

  it("hides the reset button when a value is not set", async function() {
    await render(hbs`<Filters::Pill/>`);

    expect(this.element.querySelector(".btn-reset")).to.be.null;
  });

  it("renders the title when it is present", async function() {
    this.set("title", "my title");
    await render(hbs`<Filters::Pill @title={{this.title}}/>`);

    expect(this.element.textContent.trim()).to.equal('my title');
  });

  it("renders a button when the value is not set", async function() {
    await render(hbs`<Filters::Pill @title="test"/>`);

    expect(this.element.querySelector(".btn")).to.not.be.null;
  });

  it("renders a string value", async function() {
    this.set("value", "test");
    await render(hbs`<Filters::Pill @value={{this.value}}/>`);

    expect(this.element.textContent.trim()).to.equal('test');
  });

  it("renders a reset button when a value exists", async function() {
    this.set("value2", "test");
    await render(hbs`<Filters::Pill @value={{this.value2}}/>`);

    expect(this.element.querySelector(".btn-reset")).to.not.be.null;
  });

  it("triggers onReset action when the reset button is pressed", async function() {
    let called = false;

    this.set("value3", "test");
    this.set("onReset", function() {
      called = true;
    });
    await render(hbs`<Filters::Pill @onReset={{this.onReset}} @value={{this.value3}}/>`);

    await click('.btn-reset');

    expect(called).to.be.true;
  });

  it("triggers onSelect action when the value button is pressed", async function() {
    let called = false;

    this.set("value4", "test");
    this.set("onSelect", function() {
      called = true;
    });
    await render(hbs`<Filters::Pill @onSelect={{this.onSelect}} @value={{this.value4}}/>`);

    await click('.btn-select');

    expect(called).to.be.true;
  });

  it("triggers onSelect action when the title button is pressed", async function() {
    let called = false;

    this.set("title3", "test");
    this.set("onSelect2", function() {
      called = true;
    });

    await render(hbs`<Filters::Pill @onSelect={{this.onSelect2}} @title={{this.title3}}/>`);
    await click('.btn-select');

    expect(called).to.be.true;
  });
});
