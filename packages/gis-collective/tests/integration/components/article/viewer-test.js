import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | article/viewer', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<Article::Viewer />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a header 1', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title1',
            level: 1,
          },
        },
      ],
    });

    await render(hbs`<Article::Viewer @value={{this.value}}/>`);
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'title1'
    );
  });

  it('renders a header 2', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title2',
            level: 2,
          },
        },
      ],
    });

    await render(hbs`<Article::Viewer @value={{this.value}}/>`);
    expect(this.element.querySelector('h2').textContent.trim()).to.equal(
      'title2'
    );
  });

  it('renders a header 3', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title3',
            level: 3,
          },
        },
      ],
    });

    await render(hbs`<Article::Viewer @value={{this.value}}/>`);
    expect(this.element.querySelector('h3').textContent.trim()).to.equal(
      'title3'
    );
  });

  it('renders a header 4', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title4',
            level: 4,
          },
        },
      ],
    });

    await render(hbs`<Article::Viewer @value={{this.value}}/>`);
    expect(this.element.querySelector('h4').textContent.trim()).to.equal(
      'title4'
    );
  });

  it('renders a header 5', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title5',
            level: 5,
          },
        },
      ],
    });

    await render(hbs`<Article::Viewer @value={{this.value}}/>`);
    expect(this.element.querySelector('h5').textContent.trim()).to.equal(
      'title5'
    );
  });

  it('renders a header 6', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title6',
            level: 6,
          },
        },
      ],
    });

    await render(hbs`<Article::Viewer @value={{this.value}}/>`);
    expect(this.element.querySelector('h6').textContent.trim()).to.equal(
      'title6'
    );
  });

  it('renders a paragraph', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: 'Hey. Meet the new <b>Editor</b>.',
          },
        },
      ],
    });
    await render(hbs`<Article::Viewer @value={{this.value}}/>`);
    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'Hey. Meet the new Editor.'
    );
  });

  it('renders an unordered list', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'list',
          data: {
            style: 'unordered',
            items: [
              'It <b>is</b> a block-styled editor',
              'It returns clean data output in JSON',
              'Designed to be extendable and pluggable with a simple API',
            ],
          },
        },
      ],
    });

    await render(hbs`<Article::Viewer @value={{this.value}}/>`);
    const items = this.element.querySelectorAll('ul li');

    expect(items[0].textContent).to.equal(`It is a block-styled editor`);
    expect(items[1].textContent).to.equal(
      `It returns clean data output in JSON`
    );
    expect(items[2].textContent).to.equal(
      `Designed to be extendable and pluggable with a simple API`
    );
  });

  it('renders an ordered list', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'list',
          data: {
            style: 'ordered',
            items: [
              'It <b>is</b> a block-styled editor',
              'It returns clean data output in JSON',
              'Designed to be extendable and pluggable with a simple API',
            ],
          },
        },
      ],
    });

    await render(hbs`<Article::Viewer @value={{this.value}}/>`);
    const items = this.element.querySelectorAll('ol li');

    expect(items[0].textContent).to.equal(`It is a block-styled editor`);
    expect(items[1].textContent).to.equal(
      `It returns clean data output in JSON`
    );
    expect(items[2].textContent).to.equal(
      `Designed to be extendable and pluggable with a simple API`
    );
  });

  it('renders a delimiter', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'delimiter',
          data: {},
        },
      ],
    });

    await render(hbs`<Article::Viewer @value={{this.value}}/>`);
    const delimiter = this.element.querySelector('.ce-delimiter.cdx-block');

    expect(delimiter).to.exist;
  });

  it('renders the links with target when it is set', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: "Hey. <a href='test'>Meet</a> the new <b>Editor</b>.",
          },
        },
      ],
    });
    await render(hbs`<Article::Viewer @value={{this.value}} @target="_self"/>`);
    expect(this.element.querySelector('a')).to.have.attribute(
      'target',
      '_self'
    );
  });

  it('renders the hashtag links inside paragraphs', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: 'Hey. #Meet the new <b>Editor</b>.',
          },
        },
      ],
    });
    await render(hbs`<Article::Viewer @value={{this.value}} @target="_self"/>`);
    expect(this.element.querySelector('a').textContent).to.equal('#Meet');
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/test/Meet'
    );
    expect(this.element.querySelector('a')).to.have.attribute(
      'target',
      '_self'
    );
  });

  it('renders the hashtag links at the beginning of paragraphs', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: '#Meet',
          },
        },
      ],
    });
    await render(hbs`<Article::Viewer @value={{this.value}} @target="_self"/>`);
    expect(this.element.querySelector('a').textContent).to.equal('#Meet');
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/test/Meet'
    );
    expect(this.element.querySelector('a')).to.have.attribute(
      'target',
      '_self'
    );
  });

  it('properly renders an anchor link', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: '<a href="https://en.m.wikipedia.org/wiki/Atmospheric_diving_suit#/media/File%3ANewtsuit_atmospheric_diving_suit.jpg">https://en.m.wikipedia.org/wiki/Atmospheric_diving_suit#/media/File%3ANewtsuit_atmospheric_diving_suit.jpg</a>',
          },
        },
      ],
    });
    await render(hbs`<Article::Viewer @value={{this.value}} @target="_self"/>`);
    expect(this.element.querySelector('a').textContent).to.equal(
      'https://en.m.wikipedia.org/wiki/Atmospheric_diving_suit#/media/File%3ANewtsuit_atmospheric_diving_suit.jpg'
    );
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      'https://en.m.wikipedia.org/wiki/Atmospheric_diving_suit#/media/File%3ANewtsuit_atmospheric_diving_suit.jpg'
    );
  });

  it('renders a quote', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'quote',
          data: {
            text: 'something',
            caption: 'by me',
            alignment: 'left',
          },
        },
      ],
    });

    await render(hbs`<Article::Viewer @value={{this.value}}/>`);
    const text = this.element.querySelector('.blockquote p');
    const caption = this.element.querySelector(
      '.blockquote .blockquote-footer'
    );

    expect(text.textContent).to.equal('something');
    expect(caption.textContent).to.equal('by me');
  });

  it('renders a table', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'table',
          data: {
            content: [
              ['a', 'b', 'c'],
              ['d', 'e', 'f'],
            ],
          },
        },
      ],
    });

    await render(hbs`<Article::Viewer @value={{this.value}}/>`);
    const header = this.element.querySelectorAll('table thead tr th');
    const row = this.element.querySelectorAll('table tbody td');

    expect(header[0].textContent).to.equal('a');
    expect(header[1].textContent).to.equal('b');
    expect(header[2].textContent).to.equal('c');

    expect(row[0].textContent).to.equal('d');
    expect(row[1].textContent).to.equal('e');
    expect(row[2].textContent).to.equal('f');
  });

  it('renders an embedded youtube link', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'embed',
          data: {
            service: 'youtube',
            source: 'https://www.youtube.com/watch?v=4Ilg5kLwv2k',
            embed: 'https://www.youtube.com/embed/4Ilg5kLwv2k',
            width: 580,
            height: 320,
            caption: 'caption',
          },
        },
      ],
    });

    await render(hbs`<Article::Viewer @value={{this.value}}/>`);

    expect(this.element.querySelector('iframe')).to.have.attribute(
      'src',
      'https://www.youtube.com/embed/4Ilg5kLwv2k'
    );
    expect(this.element.querySelector('iframe')).to.have.attribute(
      'title',
      'caption'
    );
  });

  it('yields unknown blocks', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'unknown',
          data: {
            key: 'value',
          },
        },
      ],
    });

    await render(
      hbs`<Article::Viewer @value={{this.value}} as | block | >
        <div class="type">{{block.type}}</div>
        <div class="key">{{block.data.key}}</div>
      </Article::Viewer>`
    );

    expect(this.element.querySelector('.type').textContent.trim()).to.equal(
      'unknown'
    );
    expect(this.element.querySelector('.key').textContent.trim()).to.equal(
      'value'
    );
  });
});
