import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | article/first-paragraph', function() {
  setupRenderingTest();


  it('renders an empty string when there is no value set', async function() {
    await render(hbs`<Article::FirstParagraph />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders an empty string when there are no blocks', async function() {
    this.set("contentBlocks", {});

    await render(hbs`<Article::FirstParagraph @contentBlocks={{this.contentBlocks}}/>`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders nothing when there is no block paragraph', async function() {
    this.set("contentBlocks", {
      blocks: []
    });

    await render(hbs`<Article::FirstParagraph @contentBlocks={{this.contentBlocks}}/>`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the first paragraph when is set', async function() {
    this.set("contentBlocks", {
      blocks: [{
        type: "paragraph",
        data: {
          text: "some text"
        }
      }]
    });

    await render(hbs`<Article::FirstParagraph @contentBlocks={{this.contentBlocks}}/>`);
    expect(this.element.textContent.trim()).to.equal('some text');
  });

  it('renders without html for long texts', async function() {
    this.set("contentBlocks", {
      blocks: [{
        type: "paragraph",
        data: {
          text: "<b>some<b> text"
        }
      }]
    });

    await render(hbs`<Article::FirstParagraph @contentBlocks={{this.contentBlocks}} @max={{6}}/>`);
    expect(this.element.textContent.trim()).to.startWith('some t...');
  });

  it('renders the bold text', async function() {
    this.set("contentBlocks", {
      blocks: [{
        type: "paragraph",
        data: {
          text: "<b>some</b> <strong>text</strong>"
        }
      }]
    });

    await render(hbs`<Article::FirstParagraph @contentBlocks={{this.contentBlocks}}/>`);
    expect(this.element.querySelector("b").textContent.trim()).to.equal('some');
    expect(this.element.querySelector("strong").textContent.trim()).to.equal('text');
  });

  it('renders the italic text', async function() {
    this.set("contentBlocks", {
      blocks: [{
        type: "paragraph",
        data: {
          text: "<i>some</i> <em>text</em>"
        }
      }]
    });

    await render(hbs`<Article::FirstParagraph @contentBlocks={{this.contentBlocks}}/>`);
    expect(this.element.querySelector("i").textContent.trim()).to.equal('some');
    expect(this.element.querySelector("em").textContent.trim()).to.equal('text');
  });

  it('renders the underlined text', async function() {
    this.set("contentBlocks", {
      blocks: [{
        type: "paragraph",
        data: {
          text: "<u>some</u> <mark>text</mark>"
        }
      }]
    });

    await render(hbs`<Article::FirstParagraph @contentBlocks={{this.contentBlocks}}/>`);
    expect(this.element.querySelector("u").textContent.trim()).to.equal('some');
    expect(this.element.querySelector("mark").textContent.trim()).to.equal('text');
  });

  it('renders links', async function() {
    this.set("contentBlocks", {
      blocks: [{
        type: "paragraph",
        data: {
          text: `<a href="link" name="name" target="target">some</a>`
        }
      }]
    });

    await render(hbs`<Article::FirstParagraph @contentBlocks={{this.contentBlocks}}/>`);
    expect(this.element.querySelector("a").textContent.trim()).to.equal('some');
    expect(this.element.querySelector("a")).to.have.attribute("href", "link");
    expect(this.element.querySelector("a")).to.have.attribute("name", "name");
    expect(this.element.querySelector("a")).to.have.attribute("target", "target");
  });

  it('does not render images', async function() {
    this.set("contentBlocks", {
      blocks: [{
        type: "paragraph",
        data: {
          text: `<img src="link" alt="name">`
        }
      }]
    });

    await render(hbs`<Article::FirstParagraph @contentBlocks={{this.contentBlocks}}/>`);
    expect(this.element.querySelector("img")).not.to.exist;
  });

  it('does not render scripts', async function() {
    this.set("contentBlocks", {
      blocks: [{
        type: "paragraph",
        data: {
          text: `<script>hello</script>`
        }
      }]
    });

    await render(hbs`<Article::FirstParagraph @contentBlocks={{this.contentBlocks}}/>`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector("script")).not.to.exist;
  });
});
