import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, waitFor, fillIn, waitUntil, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | article/manage', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Article::Manage />`);
    expect(this.element.querySelector(".alert-danger")).to.exist;
    expect(this.element.querySelector(".alert-danger").textContent.trim()).to.contain("You need to provide a level 1 Heading for the article title");
  });

  it('renders a heading block value', async function() {
    this.set("value", {
      blocks: [{
        "type": "header",
        "data": {
            "text": "title1",
            "level": 1
        }
      }]
    });

    await render(hbs`<Article::Manage @value={{this.value}}/>`);
    await waitFor(".editor-is-ready", { timeout: 2000});

    expect(this.element.querySelector(".btn-publish")).to.have.attribute("disabled", "");
    expect(this.element.querySelector(".btn-reset")).to.have.attribute("disabled", "");

    expect(this.element.querySelector(".alert-danger")).not.to.exist;
    expect(this.element.querySelector("h1")).to.exist;
    expect(this.element.querySelector("h1").textContent.trim()).to.equal("title1");
  });

  it('does not trigger a publish on change', async function() {
    this.set("value", {
      blocks: [{
        "type": "header",
        "data": {
            "text": "title1",
            "level": 1
        }
      }]
    });

    let title;
    let value;
    this.set("publish", (t,v) => {
      title = t;
      value = v;
    });

    await render(hbs`<Article::Manage @value={{this.value}} @onPublish={{this.publish}}/>`);
    await waitFor(".editor-is-ready", { timeout: 2000});

    await fillIn("h1", "newValue");

    waitFor(".editor-changed");

    await waitUntil(() => !this.element.querySelector(".btn-publish").hasAttribute("disabled"));

    expect(title).not.to.exist;
    expect(value).not.to.exist;
    expect(this.element.querySelector(".btn-publish")).to.have.attribute("disabled", null);
    expect(this.element.querySelector(".btn-reset")).to.have.attribute("disabled", null);
  });

  it('resets the value on click on reset', async function() {
    this.set("value", {
      blocks: [{
        "type": "header",
        "data": {
            "text": "title1",
            "level": 1
        }
      }]
    });

    let title;
    let value;
    this.set("publish", (t,v) => {
      title = t;
      value = v;
    });

    await render(hbs`<Article::Manage @value={{this.value}} @onPublish={{this.publish}}/>`);
    await waitFor(".editor-is-ready", { timeout: 2000});

    await fillIn("h1", "newValue");

    waitFor(".editor-changed");

    await waitUntil(() => !this.element.querySelector(".btn-publish").hasAttribute("disabled"));

    await click(".btn-reset");

    await waitFor(".editor-is-ready", { timeout: 2000});
    expect(this.element.querySelector("h1").textContent).to.equal("title1");

    expect(title).not.to.exist;
    expect(value).not.to.exist;
    expect(this.element.querySelector(".btn-publish")).to.have.attribute("disabled", "");
    expect(this.element.querySelector(".btn-reset")).to.have.attribute("disabled", "");
  });

  it('publishes the value on click on publish', async function() {
    this.set("value", {
      blocks: [{
        "type": "header",
        "data": {
            "text": "title1",
            "level": 1
        }
      }]
    });

    let title;
    let value;
    this.set("publish", (t,v) => {
      title = t;
      value = v;
    });

    await render(hbs`<Article::Manage @value={{this.value}} @onPublish={{this.publish}}/>`);
    await waitFor(".editor-is-ready", { timeout: 2000});

    await fillIn("h1", "newValue");

    waitFor(".editor-changed");

    await waitUntil(() => !this.element.querySelector(".btn-publish").hasAttribute("disabled"));

    await click(".btn-publish");

    expect(title).to.equal("newValue");
    expect(value.blocks).to.deep.equal([{
      "type": "header",
      "data": {
          "text": "newValue",
          "level": 1
      }
    }])
    expect(this.element.querySelector(".btn-publish")).to.have.attribute("disabled", null);
    expect(this.element.querySelector(".btn-reset")).to.have.attribute("disabled", null);
  });

});
