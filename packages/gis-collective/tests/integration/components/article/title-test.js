import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | article/title', function() {
  setupRenderingTest();

  it('renders empty string when the value is not set', async function() {
    await render(hbs`<Article::Title />`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders empty string when there is no title block', async function() {
    this.set("value", {
      blocks: []
    })

    await render(hbs`<Article::Title @value={{this.value}}/>`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders empty string when there a paragraph and no header', async function() {
    this.set("value",{
      "blocks": [
        {
          "type": "paragraph",
          "data": {
            "text": "feature 90899"
          }
        }
      ]
    });

    await render(hbs`<Article::Title @value={{this.value}}/>`);

    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the title when there is a title block', async function() {
    this.set("value", {
      blocks: [{
        "type" : "header",
        "data" : {
            "text" : "Editor.js",
            "level" : 1
        }
      }]
    })

    await render(hbs`<Article::Title @value={{this.value}}/>`);

    expect(this.element.textContent.trim()).to.equal('Editor.js');
  });
});
