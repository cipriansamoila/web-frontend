import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, waitFor, fillIn, waitUntil, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { expect } from 'chai';

describe('Integration | Component | article/editor', function () {
  setupRenderingTest();

  it('renders an error for a heading 1 when there is no title', async function () {
    await render(hbs`<Article::Editor />`);

    expect(this.element.querySelector('.alert-danger')).to.exist;
    expect(
      this.element.querySelector('.alert-danger').textContent.trim()
    ).to.contain(
      'You need to provide a level 1 Heading for the article title.'
    );
    expect(this.element.querySelector('.alert-link')).to.exist;
    expect(
      this.element.querySelector('.alert-danger').textContent.trim()
    ).to.contain('Click here to add a title');
  });

  it('adds a heading one when clicking on the alert link', async function () {
    await render(hbs`<Article::Editor />`);

    await waitFor('.editor-is-ready', { timeout: 3000 });

    await click('.add-title-link');
    await waitUntil(() => !this.element.querySelector('.alert-danger'));

    expect(this.element.querySelector('h1')).to.exist;
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'New title'
    );
  });

  it('renders a help text when there is no paragraph', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title1',
            level: 1,
          },
        },
      ],
    });

    await render(hbs`<Article::Editor @value={{this.value}}/>`);
    await waitFor('.editor-is-ready', { timeout: 2000 });

    expect(this.element.querySelector('.text-muted')).to.exist;
    expect(
      this.element.querySelector('.text-muted').textContent.trim()
    ).to.contain('The article has no paragraph.');
    expect(this.element.querySelector('.add-paragraph-link')).to.exist;
    expect(
      this.element.querySelector('.add-paragraph-link').textContent.trim()
    ).to.contain('Click here if you would like to add one');
  });

  it('adds a placeholder paragraph when clicking on the link to add a paragraph', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title1',
            level: 1,
          },
        },
      ],
    });

    await render(hbs`<Article::Editor @value={{this.value}}/>`);
    await waitFor('.editor-is-ready', { timeout: 2000 });

    await click('.add-paragraph-link');
    await waitUntil(() => !this.element.querySelector('.text-muted'));

    expect(this.element.querySelector('.ce-paragraph')).to.exist;
    expect(
      this.element.querySelector('.ce-paragraph').textContent.trim()
    ).to.contain('New paragraph');
  });

  it('does not render a heading error when allowNoHeading is true', async function () {
    await render(hbs`<Article::Editor />`);
    await render(hbs`<Article::Editor @allowNoHeading={{true}} />`);
    expect(this.element.querySelector('.alert-danger')).not.to.exist;
  });

  it('renders a heading block value', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title1',
            level: 1,
          },
        },
      ],
    });

    await render(hbs`<Article::Editor @value={{this.value}}/>`);
    await waitFor('.editor-is-ready', { timeout: 2000 });

    expect(this.element.querySelector('.alert-danger')).not.to.exist;
    expect(this.element.querySelector('h1')).to.exist;
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'title1'
    );
  });

  it('does not escape & chars', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title1',
            level: 1,
          },
        },
      ],
    });

    let title;
    this.set('change', (newTitle) => {
      title = newTitle;
    });
    await render(
      hbs`<Article::Editor @value={{this.value}} @onChange={{this.change}} />`
    );
    await waitFor('.editor-is-ready', { timeout: 2000 });

    await fillIn('.ce-header', 'a & b');
    await waitUntil(() => title, { timeout: 2000 });

    expect(title).to.deep.equal('a & b');
  });

  it('shows an enabled save button when markdown is provided', async function () {
    this.timeout(10000);
    let value;
    let title;

    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'title1',
            level: 1,
          },
        },
      ],
    });

    this.set('change', (newTitle, newValue) => {
      title = newTitle;
      value = newValue.blocks;
    });

    await render(
      hbs`<Article::Editor @value={{this.value}} @onChange={{this.change}}/>`
    );
    await waitFor('.editor-is-ready', { timeout: 5000 });

    await fillIn('h1', 'newValue');

    await waitUntil(() => title == 'newValue');

    expect(value).to.deep.equal([
      { type: 'header', data: { level: 1, text: 'newValue' } },
    ]);
  });
});
