import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/license', function() {
  setupRenderingTest();

  it('renders the name and url when they are set', async function() {
    this.set("value", {
      name: "some license",
      url: "some url"
    });

    await render(hbs`<Browse::License @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.contain("some license");
    expect(this.element.querySelector("a")).to.have.attribute("href", "some url");
  });

  it('renders the name when url is not set', async function() {
    this.set("value", {
      name: "some license"
    });

    await render(hbs`<Browse::License @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.contain("some license");
    expect(this.element.querySelector("a")).not.to.exist;
  });

  it('renders the url and "unknown" when the name is not set', async function() {
    this.set("value", {
      url: "some url"
    });

    await render(hbs`<Browse::License @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.contain("License");
    expect(this.element.querySelector("a")).to.have.attribute("href", "some url");
  });
});
