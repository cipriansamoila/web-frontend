import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/map/download', function() {
  setupRenderingTest();

  it('renders the csv and geojson for a map that can not be edited', async function() {
    this.set("map", {
      id: "id-123"
    })

    await render(hbs`<Browse::Map::Download @map={{this.map}} />`);

    let labels = [];
    let links = [];
    this.element.querySelectorAll(".dropdown-item").forEach(element => {
      labels.push(element.textContent.trim());

      if(element.attributes.getNamedItem("href")) {
        links.push(element.attributes.getNamedItem("href").value.trim());
      }
    });

    expect(labels).to.deep.equal([ "CSV", "GeoJson" ]);
    expect(links).to.deep.equal([ "/mock-server/features?&map=id-123&format=csv", "/mock-server/features?&map=id-123&format=geojson" ]);
  });


  it('renders the csv, geojson and geopackage for a map that can be edited', async function() {
    this.set("map", {
      id: "id-123",
      canEdit: true
    })

    await render(hbs`<Browse::Map::Download @map={{this.map}} />`);

    let labels = [];
    let links = [];
    this.element.querySelectorAll(".dropdown-item").forEach(element => {
      labels.push(element.textContent.trim());

      if(element.attributes.getNamedItem("href")) {
        links.push(element.attributes.getNamedItem("href").value.trim());
      }
    });

    expect(labels).to.deep.equal([ "CSV", "GeoJson", "GeoPackage" ]);
    expect(links).to.deep.equal([ "/mock-server/features?&map=id-123&format=csv", "/mock-server/features?&map=id-123&format=geojson", "/mock-server/maps/id-123/geopackage" ]);
  });
});
