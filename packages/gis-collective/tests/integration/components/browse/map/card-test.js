import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/map/card', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Browse::Map::Card />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the map name, description and a small cover', async function() {
    this.set("map", {
      name: "test name",
      contentBlocks: {
        blocks: [
          { type: 'header', data: { text: "name", level: 1 } },
          { type: 'paragraph', data: { text: 'test description' } }]
      },
      hasCover: true,
      squareCover: {
        picture: "map square cover",
        get(key) {
          return this[key];
        }
      }
    });

    await render(hbs`<Browse::Map::Card @map={{this.map}}/>`);

    expect(this.element.querySelector(".cover-image")).to.have.attribute("style", "background-image: url(map square cover/sm)");
    expect(this.element.querySelector(".map-name").textContent.trim()).to.equal("test name");
    expect(this.element.querySelector(".map-description").textContent.trim()).to.equal("test description");
  });

  it('renders the eye-slash icon when it is not published', async function() {
    this.set("map", {
      visibility: { isPublic: false }
    });

    await render(hbs`<Browse::Map::Card @map={{this.map}}/>`);

    expect(this.element.querySelector(".map-name + .icon-not-published .fa-eye-slash")).to.exist;
  });

  it('does not render the eye-slash icon when it is published', async function() {
    this.set("map", {
      visibility: { isPublic: true }
    });

    await render(hbs`<Browse::Map::Card @map={{this.map}}/>`);

    expect(this.element.querySelector(".map-name + .icon-not-published .fa-eye-slash")).not.to.exist;
  });

  it('does not render the options menu when canEdit is false', async function() {
    this.set("map", {
      canEdit: false
    });

    await render(hbs`<Browse::Map::Card @map={{this.map}}/>`);

    expect(this.element.querySelector(".btn-extend")).not.to.exist;
  });

  it('renders the options menu when canEdit is true', async function() {
    this.set("map", {
      canEdit: true
    });

    await render(hbs`<Browse::Map::Card @map={{this.map}}/>`);

    expect(this.element.querySelector(".btn-extend")).to.exist;
  });

  it('opens the map options when the btn extend is pressed', async function() {
    this.set("map", {
      id: 1,
      canEdit: true
    });

    await render(hbs`<Browse::Map::Card @map={{this.map}}/>`);
    await click(".btn-extend");

    expect(this.element.querySelector(".map-options.is-open")).to.exist;
  });
});
