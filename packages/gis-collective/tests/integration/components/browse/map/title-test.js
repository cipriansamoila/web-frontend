import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/map/title', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Browse::Map::Title />`);

    expect(this.element.querySelector(".row.browse-map-title")).to.exist;
    expect(this.element.querySelector(".btn-map-features")).not.to.exist;
    expect(this.element.querySelector(".btn-edit-map")).not.to.exist;
    expect(this.element.querySelector(".btn-import-map")).not.to.exist;
    expect(this.element.querySelector(".btn-map-download")).not.to.exist;
  });

  it('renders the map title', async function() {
    this.set("map", {
      name: "title"
    });

    await render(hbs`<Browse::Map::Title @map={{this.map}}/>`);
    expect(this.element.querySelector("h1").textContent).to.contain('title');
    expect(this.element.querySelector(".tag-line")).not.to.exist;
    expect(this.element.querySelector(".map-cover-image")).not.to.exist;
    expect(this.element.querySelector(".map-square-cover-image")).not.to.exist;
  });

  it('renders the map tagline', async function() {
    this.set("map", {
      name: "title",
      tagLine: "tagline"
    });

    await render(hbs`<Browse::Map::Title @map={{this.map}}/>`);
    expect(this.element.querySelector(".tag-line")).to.exist;
    expect(this.element.querySelector(".tag-line").textContent.trim()).to.equal("tagline");
    expect(this.element.querySelector(".map-cover-image")).not.to.exist;
    expect(this.element.querySelector(".map-square-cover-image")).not.to.exist;
  });

  it('renders the map cover twice if the cover is set', async function() {
    const map = this.owner.lookup('service:store').createRecord('map', {
      name: "title",
      cover: this.owner.lookup('service:store').createRecord('picture', {name: "test", picture: "picture1" })
    });

    this.set("map", map);

    await render(hbs`<Browse::Map::Title @map={{this.map}}/>`);

    expect(this.element.querySelector(".map-cover-image")).to.exist;
    expect(this.element.querySelector(".map-cover-image")).to.have.attribute("style", "background-image: url(picture1/lg);");

    expect(this.element.querySelector(".map-square-cover-image")).to.exist;
    expect(this.element.querySelector(".map-square-cover-image")).to.have.attribute("style", "background-image: url(picture1/lg);");
  });

  it('renders the map cover twice if it is the only the square cover is set', async function() {
    const map = this.owner.lookup('service:store').createRecord('map', {
      name: "title",
      squareCover: this.owner.lookup('service:store').createRecord('picture', {name: "test", picture: "picture2" })
    });

    this.set("map", map);

    await render(hbs`<Browse::Map::Title @map={{this.map}}/>`);

    expect(this.element.querySelector(".map-cover-image")).to.exist;
    expect(this.element.querySelector(".map-cover-image")).to.have.attribute("style", "background-image: url(picture2/lg);");

    expect(this.element.querySelector(".map-square-cover-image")).to.exist;
    expect(this.element.querySelector(".map-square-cover-image")).to.have.attribute("style", "background-image: url(picture2/lg);");
  });

  it('renders the map square cover twice if the cover is the default one', async function() {
    const map = this.owner.lookup('service:store').createRecord('map', {
      name: "title",
      cover: this.owner.lookup('service:store').createRecord('picture', {name: "default", picture: "picture1" }),
      squareCover: this.owner.lookup('service:store').createRecord('picture', {name: "test", picture: "picture2" })
    });

    this.set("map", map);

    await render(hbs`<Browse::Map::Title @map={{this.map}}/>`);

    expect(this.element.querySelector(".map-cover-image")).to.exist;
    expect(this.element.querySelector(".map-cover-image")).to.have.attribute("style", "background-image: url(picture2/lg);");

    expect(this.element.querySelector(".map-square-cover-image")).to.exist;
    expect(this.element.querySelector(".map-square-cover-image")).to.have.attribute("style", "background-image: url(picture2/lg);");
  });

  it('renders the map cover twice if the square cover is the default one', async function() {
    const map = this.owner.lookup('service:store').createRecord('map', {
      name: "title",
      cover: this.owner.lookup('service:store').createRecord('picture', {name: "test", picture: "picture1" }),
      squareCover: this.owner.lookup('service:store').createRecord('picture', {name: "default", picture: "picture2" })
    });

    this.set("map", map);

    await render(hbs`<Browse::Map::Title @map={{this.map}}/>`);

    expect(this.element.querySelector(".map-cover-image")).to.exist;
    expect(this.element.querySelector(".map-cover-image")).to.have.attribute("style", "background-image: url(picture1/lg);");

    expect(this.element.querySelector(".map-square-cover-image")).to.exist;
    expect(this.element.querySelector(".map-square-cover-image")).to.have.attribute("style", "background-image: url(picture1/lg);");
  });

  it('renders the map cover twice if the square cover has no picture', async function() {
    const map = this.owner.lookup('service:store').createRecord('map', {
      name: "title",
      cover: this.owner.lookup('service:store').createRecord('picture', {name: "test", picture: "picture1" }),
      squareCover: this.owner.lookup('service:store').createRecord('picture', {name: "" })
    });

    this.set("map", map);

    await render(hbs`<Browse::Map::Title @map={{this.map}}/>`);

    expect(this.element.querySelector(".map-cover-image")).to.exist;
    expect(this.element.querySelector(".map-cover-image")).to.have.attribute("style", "background-image: url(picture1/lg);");

    expect(this.element.querySelector(".map-square-cover-image")).to.exist;
    expect(this.element.querySelector(".map-square-cover-image")).to.have.attribute("style", "background-image: url(picture1/lg);");
  });

  it('renders the map square cover twice if the cover has no picture', async function() {
    const map = this.owner.lookup('service:store').createRecord('map', {
      name: "title",
      cover: this.owner.lookup('service:store').createRecord('picture', {name: "" }),
      squareCover: this.owner.lookup('service:store').createRecord('picture', {name: "", picture: "picture2" })
    });

    this.set("map", map);

    await render(hbs`<Browse::Map::Title @map={{this.map}}/>`);

    expect(this.element.querySelector(".map-cover-image")).to.exist;
    expect(this.element.querySelector(".map-cover-image")).to.have.attribute("style", "background-image: url(picture2/lg);");

    expect(this.element.querySelector(".map-square-cover-image")).to.exist;
    expect(this.element.querySelector(".map-square-cover-image")).to.have.attribute("style", "background-image: url(picture2/lg);");
  });

  it('does not render the map options when the map is not editable', async function() {
    const map = this.owner.lookup('service:store').createRecord('map', {
      name: "title"
    });

    this.set("map", map);

    await render(hbs`<Browse::Map::Title @map={{this.map}}/>`);
    expect(this.element.querySelector(".btn-extend")).to.not.exist;
  });

  it('renders the map options when the map is editable', async function() {
    const map = this.owner.lookup('service:store').createRecord('map', {
      name: "title",
      canEdit: true
    });

    this.set("map", map);

    await render(hbs`<Browse::Map::Title @map={{this.map}}/>`);

    expect(this.element.querySelector(".btn-map-features")).not.to.exist;
    expect(this.element.querySelector(".btn-edit-map")).to.exist;
    expect(this.element.querySelector(".btn-import-map")).to.exist;
    expect(this.element.querySelector(".btn-map-download")).to.exist;
  });
});
