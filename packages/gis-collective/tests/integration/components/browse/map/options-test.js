import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/map/options', function () {
  setupRenderingTest();

  it('renders', async function () {
    this.set('map', {
      id: 'id',
      canEdit: true,
    });
    await render(hbs`<Browse::Map::Options @map={{this.map}}/>`);

    let labels = [];
    this.element.querySelectorAll('.dropdown-item').forEach((element) => {
      labels.push(element.textContent.trim());
    });

    expect(labels).to.deep.equal([
      'View features',
      'Edit the map',
      'Map files',
      'Map sheet',
    ]);
  });

  it('renders the small version', async function () {
    this.set('map', {
      id: 'id',
      canEdit: true,
    });
    await render(
      hbs`<Browse::Map::Options @isSmall={{true}} @map={{this.map}}/>`
    );

    let labels = [];

    expect(this.element.querySelector('.dropdown-item')).not.to.exist;

    this.element.querySelectorAll('.btn').forEach((element) => {
      labels.push(element.textContent.trim());
    });

    expect(labels).to.deep.equal(['', '', '', '']);
  });
});
