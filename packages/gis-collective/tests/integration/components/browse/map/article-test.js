import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/map/article', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<Browse::Map::Article />`);

    expect(this.element.querySelector('.row.browse-map-title')).to.exist;
    expect(this.element.querySelector('.description')).to.exist;
    expect(this.element.querySelector('.btn-edit-map')).not.to.exist;
  });

  it('renders the title, tagline, license and description', async function () {
    this.set('map', {
      name: 'test name',
      contentBlocks: {
        blocks: [
          { type: 'header', data: { text: 'test name', level: 1 } },
          { type: 'paragraph', data: { text: 'test description' } },
        ],
      },
      license: {
        name: 'some license',
        url: 'some url',
      },
      tagLine: 'some tag line',
    });

    await render(hbs`<Browse::Map::Article @map={{this.map}} />`);

    expect(this.element.querySelector('.dropdown-item-edit')).not.to.exist;
    expect(this.element.querySelector('h1').textContent.trim()).to.contain(
      'test name'
    );
    expect(this.element.querySelector('.tag-line').textContent.trim()).to.equal(
      'some tag line'
    );
    expect(
      this.element.querySelector('.description p').textContent.trim()
    ).to.equal('test description');
    expect(this.element.querySelector('.btn-map-download')).not.to.exist;
    expect(this.element.querySelector('.license')).to.exist;
  });

  it('renders the title and description when the tagline is missing', async function () {
    this.set('map', {
      name: 'test name',
      contentBlocks: {
        blocks: [
          { type: 'header', data: { text: 'test name', level: 1 } },
          { type: 'paragraph', data: { text: 'test description' } },
        ],
      },
    });

    await render(hbs`<Browse::Map::Article @map={{this.map}} />`);
    expect(this.element.querySelector('.dropdown-item-edit')).not.to.exist;
    expect(this.element.querySelector('.tag-line')).not.to.exist;
    expect(this.element.querySelector('h1').textContent).to.contain(
      'test name'
    );
    expect(
      this.element.querySelector('.description p').textContent.trim()
    ).to.equal('test description');
  });

  it('renders the edit button for editable maps', async function () {
    this.set('map', {
      name: 'test name',
      description: 'test description',
      tagLine: 'some tag line',
      canEdit: true,
    });

    await render(hbs`<Browse::Map::Article @map={{this.map}} />`);
    expect(this.element.querySelector('.btn-edit-map')).to.exist;
    expect(this.element.querySelector('.btn-map-download')).to.exist;
  });
});
