import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/attributes-table', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Browse::AttributesTable />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a table with one row', async function() {
    this.set("value", [
      [{
        displayName: "key1",
        value: "value 1"
      },{
        displayName: "key2",
        value: "value 2"
      },{
        displayName: "key3",
        value: "value 3"
      }]
    ]);
    await render(hbs`<Browse::AttributesTable @value={{this.value}}/>`);

    const headers = this.element.querySelectorAll('thead th');

    expect(headers[0].textContent.trim()).to.equal('#');
    expect(headers[1].textContent.trim()).to.equal('key1');
    expect(headers[2].textContent.trim()).to.equal('key2');
    expect(headers[3].textContent.trim()).to.equal('key3');

    const values = this.element.querySelectorAll('tbody th, tbody td');

    expect(values[0].textContent.trim()).to.equal('1');
    expect(values[1].textContent.trim()).to.equal('value 1');
    expect(values[2].textContent.trim()).to.equal('value 2');
    expect(values[3].textContent.trim()).to.equal('value 3');
  });

  it('renders a table with two rows', async function() {
    this.set("value", [
      [{
        displayName: "key1",
        value: "value 1"
      },{
        displayName: "key2",
        value: "value 2"
      },{
        displayName: "key3",
        value: "value 3"
      }],
      [{
        displayName: "key1",
        value: "value 4"
      },{
        displayName: "key3",
        value: "value 5"
      },{
        displayName: "key4",
        value: "value 6"
      }]
    ]);
    await render(hbs`<Browse::AttributesTable @value={{this.value}}/>`);

    const headers = this.element.querySelectorAll('thead th');

    expect(headers.length).to.equal(5);
    expect(headers[0].textContent.trim()).to.equal('#');
    expect(headers[1].textContent.trim()).to.equal('key1');
    expect(headers[2].textContent.trim()).to.equal('key2');
    expect(headers[3].textContent.trim()).to.equal('key3');
    expect(headers[4].textContent.trim()).to.equal('key4');

    const values = this.element.querySelectorAll('tbody th, tbody td');

    expect(values[0].textContent.trim()).to.equal('1');
    expect(values[1].textContent.trim()).to.equal('value 1');
    expect(values[2].textContent.trim()).to.equal('value 2');
    expect(values[3].textContent.trim()).to.equal('value 3');
    expect(values[4].textContent.trim()).to.equal('---');

    expect(values[5].textContent.trim()).to.equal('2');
    expect(values[6].textContent.trim()).to.equal('value 4');
    expect(values[7].textContent.trim()).to.equal('---');
    expect(values[8].textContent.trim()).to.equal('value 5');
    expect(values[9].textContent.trim()).to.equal('value 6');
  });
});
