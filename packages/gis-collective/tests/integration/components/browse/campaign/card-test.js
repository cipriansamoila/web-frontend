import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/campaign/card', function () {
  setupRenderingTest();

  beforeEach(function () {
    this.owner.lookup('service:notifications').ask = async () => {
      return 'yes';
    };
  });

  it('renders', async function () {
    await render(hbs`<Browse::Campaign::Card />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the campaign title', async function () {
    this.set('campaign', {
      id: 'some-id',
      name: 'some name',
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    expect(this.element.querySelector('.name').textContent.trim()).to.equal(
      'some name'
    );
  });

  it('renders the campaign picture', async function () {
    this.set('campaign', {
      id: 'some-id',
      cover: {
        picture: 'some-picture',
      },
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    expect(this.element.querySelector('.cover')).to.have.attribute(
      'style',
      "background-image: url('some-picture/md')"
    );
  });

  it('does not render the campaign options', async function () {
    this.set('campaign', {
      id: 'some-id',
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    expect(this.element.querySelector('.campaign-options')).not.to.exist;
  });

  it('renders the campaign options when the campaign is editable', async function () {
    this.set('campaign', {
      id: 'some-id',
      canEdit: true,
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    expect(this.element.querySelector('.campaign-options')).to.exist;
  });

  it('allows to delete an editable campaign', async function () {
    this.set('campaign', {
      id: 'some-id',
      canEdit: true,
    });

    let deletedCampaign;
    this.set('onDelete', (val) => {
      deletedCampaign = val;
    });

    await render(
      hbs`<Browse::Campaign::Card @campaign={{this.campaign}} @onDelete={{this.onDelete}} />`
    );

    await click('.campaign-options');
    await click('.btn-delete');

    expect(deletedCampaign).deep.equal({
      id: 'some-id',
      canEdit: true,
    });
  });

  it('allows to edit an editable campaign', async function () {
    this.set('campaign', {
      id: 'some-id',
      canEdit: true,
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    await click('.campaign-options');
    expect(this.element.querySelector('.dropdown-item-edit')).to.exist;
  });

  it('shows an unpublished icon when the campaign is not public', async function () {
    this.set('campaign', {
      id: 'some-id',
      visibility: {
        isPublic: false,
      },
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    expect(this.element.querySelector('.icon-not-published')).to.exist;
  });

  it('does not show an unpublished icon when the campaign is public', async function () {
    this.set('campaign', {
      id: 'some-id',
      visibility: {
        isPublic: true,
      },
    });

    await render(hbs`<Browse::Campaign::Card @campaign={{this.campaign}} />`);

    expect(this.element.querySelector('.icon-not-published')).not.to.exist;
  });
});
