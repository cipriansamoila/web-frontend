import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/campaign/options', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Browse::Campaign::Options />`);

    expect(this.element.textContent.trim()).to.equal('Edit');
  });

  it('renders the delete button', async function() {
    this.set("campaign", {
      id: "some-id",
      cover: {
        picture: "some-picture"
      }
    });

    this.set("onDelete", () => true)

    await render(hbs`<Browse::Campaign::Options @campaign={{this.campaign}} @onDelete={{this.onDelete}}/>`);

    expect(this.element.textContent.trim()).to.contain('Delete');
  });
});
