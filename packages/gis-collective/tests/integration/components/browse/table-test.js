import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/table', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Browse::Table />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a table when there is an array of objects', async function() {
    this.set("value", [
      { key1: "value 1", key2: "value 2", key3: "value 3" }
    ])

    await render(hbs`<Browse::Table @value={{this.value}} />`);
    const headers = this.element.querySelectorAll('thead th');

    expect(headers[0].textContent.trim()).to.equal('#');
    expect(headers[1].textContent.trim()).to.equal('key1');
    expect(headers[2].textContent.trim()).to.equal('key2');
    expect(headers[3].textContent.trim()).to.equal('key3');

    const values = this.element.querySelectorAll('tbody th, tbody td');

    expect(values[0].textContent.trim()).to.equal('1');
    expect(values[1].textContent.trim()).to.equal('value 1');
    expect(values[2].textContent.trim()).to.equal('value 2');
    expect(values[3].textContent.trim()).to.equal('value 3');
  });

  it('renders a dash for missing values', async function() {
    this.set("value", [
      { key1: "value 1", key2: "value 2", key3: "value 3" },
      { key1: "value 4" }
    ])

    await render(hbs`<Browse::Table @value={{this.value}} />`);

    const headers = this.element.querySelectorAll('thead th');

    expect(headers[0].textContent.trim()).to.equal('#');
    expect(headers[1].textContent.trim()).to.equal('key1');
    expect(headers[2].textContent.trim()).to.equal('key2');
    expect(headers[3].textContent.trim()).to.equal('key3');

    const values = this.element.querySelectorAll('tbody th, tbody td');

    expect(values[0].textContent.trim()).to.equal('1');
    expect(values[1].textContent.trim()).to.equal('value 1');
    expect(values[2].textContent.trim()).to.equal('value 2');
    expect(values[3].textContent.trim()).to.equal('value 3');
    expect(values[4].textContent.trim()).to.equal('2');
    expect(values[5].textContent.trim()).to.equal('value 4');
    expect(values[6].textContent.trim()).to.equal('-');
    expect(values[7].textContent.trim()).to.equal('-');
  });
});
