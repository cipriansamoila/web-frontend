import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/attributes', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Browse::Attributes />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders yes for true boolean values', async function() {
    this.set("value", [{
      displayName: "name",
      value: true,
      type: "boolean",
      isPresent: true
    }])
    await render(hbs`<Browse::Attributes @value={{value}} />`);

    expect(this.element.querySelector(".value").textContent.trim()).to.equal('yes');
  });

  it('renders no for false boolean values', async function() {
    this.set("value", [{
      displayName: "name",
      value: false,
      type: "boolean",
      isPresent: true
    }])
    await render(hbs`<Browse::Attributes @value={{value}} />`);
    expect(this.element.querySelector(".value").textContent.trim()).to.equal('no');
  });

  it('renders --- for unset boolean values', async function() {
    this.set("value",[{
      displayName: "name",
      type: "boolean",
      isPresent: true
    }])
    await render(hbs`<Browse::Attributes @value={{value}} />`);
    expect(this.element.querySelector(".value").textContent.trim()).to.equal('---');
  });

  it('renders --- for null boolean values', async function() {
    this.set("value",[{
      displayName: "name",
      type: "boolean",
      value: null,
      isPresent: true
    }])
    await render(hbs`<Browse::Attributes @value={{value}} />`);
    expect(this.element.querySelector(".value").textContent.trim()).to.equal('---');
  });

  it('renders --- for unset value', async function() {
    this.set("value", [{
      displayName: "name",
      isPresent: true
    }])
    await render(hbs`<Browse::Attributes @value={{value}} />`);
    expect(this.element.querySelector(".value").textContent.trim()).to.equal('---');
  });

  it('renders a string value', async function() {
    this.set("value",[{
      displayName: "name",
      value: "value",
      isPresent: true
    }])
    await render(hbs`<Browse::Attributes @value={{value}} />`);
    expect(this.element.querySelector(".value").textContent.trim()).to.equal('value');
  });

  it('renders a link value', async function() {
    this.set("value",[{
      displayName: "link",
      value: "https://giscollective.com",
      isPresent: true
    }]);

    await render(hbs`<Browse::Attributes @value={{value}} />`);
    expect(this.element.querySelector("a").textContent.trim()).to.equal("https://giscollective.com");
    expect(this.element.querySelector("a")).to.have.attribute("href", "https://giscollective.com");
  });
});
