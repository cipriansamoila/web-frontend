import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/group-iterator', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Browse::GroupIterator />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('iterates over an object with a list', async function() {
    this.set("items", {
      "categories": {
        "key": {
          "icons": [{}]
        }
      }
    });

    await render(hbs`
      <Browse::GroupIterator @items={{this.items}} as | key list |>
        {{key}}:{{list.length}}
      </Browse::GroupIterator>
    `);

    expect(this.element.querySelector(".collapse").textContent.trim()).to.equal('key:1');
  });

  it('iterates over a nested object with a list', async function() {
    this.set("items", {
      "categories": {
        "level1": {
          "categories": {
            "level2": {
              "categories": {
                "level3": {
                  "icons": [{}, {}]
                }
              }
            }
          }
        }
      }
    });

    await render(hbs`
      <Browse::GroupIterator @items={{this.items}} as | key list |>
        {{key}}:{{list.length}}
      </Browse::GroupIterator>
    `);

    expect(this.element.querySelector(".card-level1-level2-level3").textContent.trim()).to.contain('level1.level2.level3:2');
  });

  it('iterates over a list of subcategories', async function() {
    this.set("items", {
      "categories": {
        "level1": {
          "categories": {
            "level2.1": {
              "icons": [{}, {}]
            },
            "level2.2": {
              "icons": [{}, {}]
            }
          }
        }
      }
    });

    await render(hbs`
      <Browse::GroupIterator @items={{this.items}} as | key list |>
        {{key}}:{{list.length}}
      </Browse::GroupIterator>
    `);

    expect(this.element.querySelector(".card-level1-level2-1").textContent.trim()).to.contain('level1.level2.1:2');
    expect(this.element.querySelector(".card-level1-level2-2").textContent.trim()).to.contain('level1.level2.2:2');
  });

  it('it hides a group when it is removed from the items value', async function() {
    this.set("items", {
      "categories": {
        "level1": {
          "categories": {
            "level2": {
              "categories": {
                "level3": {
                  "icons": [{}, {}]
                }
              }
            }
          }
        }
      }
    });

    await render(hbs`
      <Browse::GroupIterator @items={{this.items}} as | key list |>
        {{key}}:{{list.length}}
      </Browse::GroupIterator>
    `);

    this.set("items", {
      "categories": {
        "level1": {
          "categories": {
            "level2": {
            }
          }
        }
      }
    });

    expect(this.element.querySelector(".card-level1-level2-level3")).to.have.class("d-none");
    expect(this.element.querySelector(".card-level1-level2-level3").textContent.trim()).to.contain('level1.level2.level3:2');
  });
});
