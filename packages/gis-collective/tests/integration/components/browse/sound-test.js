import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/sound', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Browse::Sound />`);
    expect(this.element.textContent.trim()).to.equal('');
  });
});
