import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/team/card', function() {
  setupRenderingTest();

  it('renders all fields', async function() {
    this.set("team", {
      logo: { picture: "test picture", get() { return "test picture" } },
      name: "team name",
      about: "about the team",
      owners: [ "" ],
      leaders: [ "" ],
      members: [ "" ],
      guests: [ "" ]
    });

    await render(hbs`<Browse::Team::Card @team={{this.team}} />`);

    expect(this.element.querySelector(".cover-image")).to.have.attribute("style", "background-image: url(test picture/sm)");
    expect(this.element.querySelector(".team-name").textContent.trim()).to.equal('team name');
    expect(this.element.querySelector(".team-member-count").textContent.trim()).to.equal('3 members');
    expect(this.element.querySelector(".team-guest-count").textContent.trim()).to.equal('one guest');
  });

  it('does not render the guest count when there are none', async function() {
    this.set("team", {
      logo: { picture: "test picture", get() { return "test picture" } },
      name: "team name",
      about: "about the team",
      owners: [ "" ],
      leaders: [ "" ],
      members: [ "" ],
      guests: [ ]
    });

    await render(hbs`<Browse::Team::Card @team={{this.team}} />`);

    expect(this.element.querySelector(".team-guest-count")).not.to.exist;
  });

  it('does not renders one member when there is an owner', async function() {
    this.set("team", {
      logo: { picture: "test picture", get() { return "test picture" } },
      name: "team name",
      about: "about the team",
      owners: [ "" ],
      leaders: [],
      members: [],
      guests: []
    });

    await render(hbs`<Browse::Team::Card @team={{this.team}} />`);

    expect(this.element.querySelector(".team-member-count").textContent.trim()).to.equal('one member');
  });

  it('triggers a click with the team id when the cover is clicked', async function() {
    this.set("team", {
      logo: { picture: "test picture", get() { return "test picture" } },
      name: "team name",
      id: "test id",
      about: "about the team",
      owners: [ "" ],
      leaders: [],
      members: [],
      guests: []
    });

    let id = "";

    this.set("click", (value) => {
      id = value;
    })

    await render(hbs`<Browse::Team::Card @team={{this.team}} @onClick={{this.click}}/>`);

    await click(".cover-image");

    expect(id).to.equal('test id');
  });
});
