import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/article', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Browse::Article />`);
    expect(this.element.textContent.trim()).to.equal('');

    await render(hbs`
      <Browse::Article>
        template block text
      </Browse::Article>
    `);

    expect(this.element.textContent.trim()).to.equal('template block text');
  });

  it('renders the title when it is set', async function() {
    await render(hbs`<Browse::Article @title="the title" />`);
    expect(this.element.querySelector("h1").textContent.trim()).to.equal('the title');
  });

  it('renders the content when it is set', async function() {
    await render(hbs`<Browse::Article @content="**the content**" />`);
    expect(this.element.querySelector("strong").textContent.trim()).to.equal('the content');
  });
});
