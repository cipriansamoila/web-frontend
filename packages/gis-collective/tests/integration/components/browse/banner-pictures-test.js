import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/banner-pictures', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<Browse::BannerPictures />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a list with 2 pictures', async function () {
    this.set('pictures', [
      { picture: 'picture1', styleLg: 'picture1lg' },
      { picture: 'picture2', styleLg: 'picture2lg' },
    ]);

    await render(hbs`<Browse::BannerPictures @pictures={{this.pictures}} />`);

    let slides = this.element.querySelectorAll('.swiper-slide .bg-picture');

    expect(slides.length).to.equal(2);
    expect(slides[0]).to.have.attribute(
      'style',
      'background-image: url(picture1/lg);'
    );
    expect(slides[1]).to.have.attribute(
      'style',
      'background-image: url(picture2/lg);'
    );
  });

  it('allows to make the photos full screen', async function () {
    this.set('pictures', [{ picture: 'picture1' }, { picture: 'picture2' }]);

    await render(hbs`<Browse::BannerPictures @pictures={{this.pictures}} />`);

    await click('.btn-fullscreen');

    expect(this.element.querySelector('.fullscreen-container.is-fullscreen')).to
      .exist;
    expect(this.element.querySelector('.btn-toggle-360')).to.not.exist;
  });

  it('should render 360 photos', async function () {
    this.set('pictures', [
      { picture: 'picture1', is360: true },
      { picture: 'picture2' },
    ]);

    await render(hbs`<Browse::BannerPictures @pictures={{this.pictures}} />`);

    await click('.btn-toggle-360');
    expect(this.element.querySelector('.fullscreen-container.is-fullscreen')).to
      .exist;
    expect(this.element.querySelector('.picture-360-view')).to.exist;
  });
});
