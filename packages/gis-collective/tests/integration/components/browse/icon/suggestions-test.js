import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/icon/suggestions', function () {
  setupRenderingTest();

  describe('when there is no icon list', function () {
    it('renders a button', async function () {
      await render(hbs`<Browse::Icon::Suggestions />`);
      expect(
        this.element.querySelector('.btn-show-all').textContent.trim()
      ).to.equal('show all icons');

      expect(this.element.querySelector('.btn-deselect-icon')).not.to.exist;
    });

    it('triggers an event on show all click', async function () {
      let showAll;

      this.set('showAll', () => {
        showAll = true;
      });

      await render(
        hbs`<Browse::Icon::Suggestions @onShowAll={{this.showAll}}/>`
      );

      await click('.btn-show-all');

      expect(showAll).to.equal(true);
    });
  });

  describe('when there is an icon list', function () {
    beforeEach(function () {
      this.set('icons', [
        {
          _id: '1',
          image: { value: 'image1', useParent: false },
          localName: 'Monument istoric 1',
          subcategory: 'Monumente',
          name: 'historical monument',
          category: 'Cultură',
        },
        {
          _id: '2',
          image: { value: 'image2', useParent: false },
          localName: 'Monument istoric 2',
          subcategory: 'Monumente',
          name: 'historical monument',
          category: 'Cultură',
        },
      ]);
    });

    it('renders a button for each icon', async function () {
      await render(hbs`<Browse::Icon::Suggestions @icons={{this.icons}} />`);
      const icons = this.element.querySelectorAll('.btn-icon');

      expect(icons).to.have.length(3);
      expect(icons[0].textContent.trim()).to.equal('Monument istoric 1');
      expect(icons[1].textContent.trim()).to.equal('Monument istoric 2');
      expect(icons[2].textContent.trim()).to.equal('show all icons');
    });

    it('renders an image for each icon', async function () {
      await render(hbs`<Browse::Icon::Suggestions @icons={{this.icons}} />`);
      const icons = this.element.querySelectorAll('.btn-icon img');

      expect(icons).to.have.length(2);
      expect(icons[0]).to.have.attribute('src', 'image1');
      expect(icons[1]).to.have.attribute('src', 'image2');
    });

    it('raises the onSelectIcon when a button is pressed', async function () {
      let icon;

      this.set('onSelectIcon', (i) => {
        icon = i;
      });

      await render(
        hbs`<Browse::Icon::Suggestions @icons={{this.icons}} @onSelectIcon={{this.onSelectIcon}} />`
      );
      await click('.btn-icon');

      expect(icon).to.deep.equal({
        _id: '1',
        image: { value: 'image1', useParent: false },
        localName: 'Monument istoric 1',
        subcategory: 'Monumente',
        name: 'historical monument',
        category: 'Cultură',
      });
    });

    it('raises the onDeselectIcon when a button is pressed', async function () {
      let pressed;

      this.set('onDeselectIcon', () => {
        pressed = true;
      });

      await render(
        hbs`<Browse::Icon::Suggestions @hasSelection={{true}} @icons={{this.icons}} @onDeselectIcon={{this.onDeselectIcon}} />`
      );
      await click('.btn-deselect-icon');

      expect(pressed).to.equal(true);
    });
  });
});
