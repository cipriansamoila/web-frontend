import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/fullscreen-swiper-button', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Browse::FullscreenSwiperButton />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the button title', async function() {
    await render(hbs`<Browse::FullscreenSwiperButton @text="text" />`);

    expect(this.element.querySelector(".svg-inline--")).not.to.exist;
    expect(this.element.querySelector(".btn.btn-link").textContent.trim()).to.equal('text');
  });

  it('renders the button icon', async function() {
    await render(hbs`<Browse::FullscreenSwiperButton @icon="adjust" />`);

    expect(this.element.querySelector(".btn.btn-link .fa-adjust")).to.exist;
    expect(this.element.querySelector(".btn.btn-link").textContent.trim()).to.equal('');
  });

  it('renders the button image', async function() {
    this.set("picture", {
      picture: "some_image"
    })
    await render(hbs`<Browse::FullscreenSwiperButton @picture={{this.picture}} />`);

    expect(this.element.querySelector(".btn.btn-link .svg-inline--fa")).not.to.exist;
    expect(this.element.querySelector(".btn.btn-link img")).to.exist;
    expect(this.element.querySelector(".btn.btn-link img")).to.have.attribute("src", "some_image");
    expect(this.element.querySelector(".btn.btn-link").textContent.trim()).to.equal('');
  });

  it('enters and exits the fullscreen mode and shows all pictures on click', async function() {
    await render(hbs`<Browse::FullscreenSwiperButton @icon="adjust" />`);

    await click(".btn-show-gallery");

    expect(this.element.querySelector(".is-fullscreen")).to.exist;
    expect(this.element.querySelector(".btn-hide-gallery")).to.exist;
    expect(this.element.querySelector(".swiper-container")).to.exist;
    expect(this.element.querySelector(".btn-show-gallery")).not.to.exist;

    await click(".btn-hide-gallery");
    expect(this.element.querySelector(".is-fullscreen")).not.to.exist;
    expect(this.element.querySelector(".btn-hide-gallery")).not.to.exist;
    expect(this.element.querySelector(".swiper-container")).not.to.exist;
    expect(this.element.querySelector(".btn-show-gallery")).to.exist;
  });
});
