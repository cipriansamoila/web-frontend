import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { transform } from 'ol/proj';

let testPolygon = {
  type: 'Polygon',
  coordinates: [
    [
      [-10.0, -20.0],
      [10.0, -10.0],
      [10.0, 10.0],
      [-10.0, -10.0],
    ],
  ],
};

let testMultiPolygon = {
  type: 'MultiPolygon',
  coordinates: [
    [
      [
        [-10.0, -20.0],
        [10.0, -10.0],
        [10.0, 10.0],
        [-10.0, -10.0],
      ],
    ],
  ],
};

let testJsonLine = {
  type: 'LineString',
  coordinates: [
    [102.0, 50.0],
    [103.0, 51.0],
    [104.0, 50.0],
    [105.0, 51.0],
  ],
};

let testJsonMultiLine = {
  type: 'MultiLineString',
  coordinates: [
    [
      [100.0, 0.0],
      [101.0, 1.0],
    ],
    [
      [102.0, 2.0],
      [103.0, 3.0],
    ],
  ],
};

let testJsonPoint = {
  type: 'Point',
  coordinates: [102.0, 10.0],
};

describe('Integration | Component | browse/geometry', function () {
  setupRenderingTest();

  it('renders a map', async function () {
    await render(hbs`<Browse::Geometry />`);
    await waitUntil(() => this.element.querySelector('.map').olMap);
  });

  it('renders a point', async function () {
    this.set('value', testJsonPoint);
    await render(hbs`<Browse::Geometry @value={{this.value}}/>`);

    await waitUntil(() => this.element.querySelector('.map').olMap);

    const map = this.element.querySelector('.map').olMap;

    const view = map.getView();
    const features = map.getLayers().item(0).getSource().getFeatures();

    const coordinates = features[0]
      .getGeometry()
      .transform('EPSG:3857', 'EPSG:4326')
      .getCoordinates();

    expect(coordinates[0]).to.be.closeTo(101, 1);
    expect(coordinates[1]).to.be.closeTo(10, 1);

    const center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');

    expect(center[0]).to.be.closeTo(102, 1);
    expect(center[1]).to.be.closeTo(10, 1);
    expect(view.getZoom()).to.be.closeTo(16, 1);
  });

  it('renders a line', async function () {
    this.set('value', testJsonLine);
    await render(hbs`<Browse::Geometry @value={{this.value}}/>`);

    await waitUntil(() => this.element.querySelector('.map').olMap);

    const map = this.element.querySelector('.map').olMap;

    const view = map.getView();
    const features = map.getLayers().item(0).getSource().getFeatures();

    const coordinates = features[0]
      .getGeometry()
      .transform('EPSG:3857', 'EPSG:4326')
      .getCoordinates();

    expect(coordinates.length).to.equal(4);
    expect(coordinates[0][0]).to.be.closeTo(101, 1);
    expect(coordinates[0][1]).to.be.closeTo(50, 1);

    const center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');

    expect(center[0]).to.be.closeTo(103, 1);
    expect(center[1]).to.be.closeTo(50, 1);
    expect(view.getZoom()).to.be.closeTo(8, 1);
  });

  it('renders a multi line', async function () {
    this.set('value', testJsonMultiLine);
    await render(hbs`<Browse::Geometry @value={{this.value}}/>`);

    await waitUntil(() => this.element.querySelector('.map').olMap);

    const map = this.element.querySelector('.map').olMap;

    const view = map.getView();
    const features = map.getLayers().item(0).getSource().getFeatures();

    const coordinates = features[0]
      .getGeometry()
      .transform('EPSG:3857', 'EPSG:4326')
      .getCoordinates();

    expect(coordinates[0].length).to.equal(2);
    expect(coordinates[0][0][0]).to.be.closeTo(101, 1);
    expect(coordinates[0][0][1]).to.be.closeTo(0, 1);

    const center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');

    expect(center[0]).to.be.closeTo(101, 1);
    expect(center[1]).to.be.closeTo(1, 1);
    expect(view.getZoom()).to.be.closeTo(8, 1);
  });

  it('renders a polygon', async function () {
    this.set('value', testPolygon);
    await render(hbs`<Browse::Geometry @value={{this.value}}/>`);

    await waitUntil(() => this.element.querySelector('.map').olMap);

    const map = this.element.querySelector('.map').olMap;

    const view = map.getView();
    const features = map.getLayers().item(0).getSource().getFeatures();

    const coordinates = features[0]
      .getGeometry()
      .transform('EPSG:3857', 'EPSG:4326')
      .getCoordinates();

    expect(coordinates[0].length).to.equal(4);
    expect(coordinates[0][0][0]).to.be.closeTo(-10, 1);
    expect(coordinates[0][0][1]).to.be.closeTo(-20, 1);

    const center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');

    expect(center[0]).to.be.closeTo(0, 1);
    expect(center[1]).to.be.closeTo(-5, 1);
    expect(view.getZoom()).to.be.closeTo(4, 1);
  });

  it('renders a multi polygon', async function () {
    this.set('value', testMultiPolygon);
    await render(hbs`<Browse::Geometry @value={{this.value}}/>`);

    await waitUntil(() => this.element.querySelector('.map').olMap);

    const map = this.element.querySelector('.map').olMap;

    const view = map.getView();
    const features = map.getLayers().item(0).getSource().getFeatures();

    const coordinates = features[0]
      .getGeometry()
      .transform('EPSG:3857', 'EPSG:4326')
      .getCoordinates();

    expect(coordinates[0][0].length).to.equal(4);
    expect(coordinates[0][0][0][0]).to.be.closeTo(-10, 1);
    expect(coordinates[0][0][0][1]).to.be.closeTo(-20, 1);

    const center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');

    expect(center[0]).to.be.closeTo(0, 1);
    expect(center[1]).to.be.closeTo(-5, 1);
    expect(view.getZoom()).to.be.closeTo(4, 1);
  });
});
