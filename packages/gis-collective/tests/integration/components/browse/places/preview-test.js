import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/places/preview', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Browse::Places::Preview />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the name when the icon is not set', async function() {
    this.set('place', {
      name: "place name",
      icons: []
    });

    await render(hbs`<Browse::Places::Preview @place={{this.place}}/>`);
    expect(this.element.textContent.trim()).to.equal("place name");
    expect(this.element.querySelector(".icon")).not.to.exist;
  });

  it('renders the name and icon when they are set', async function() {
    this.set('place', {
      name: "place name",
      icons: [{
        name: "test icon",
        localName: "test icon",
        image: { value: "image", useParent: false }
      }]
    });

    await render(hbs`<Browse::Places::Preview @place={{this.place}}/>`);
    expect(this.element.textContent.trim()).to.equal("place name");
    expect(this.element.querySelector("img")).to.have.attribute("src", "image");
  });

  it('triggers the onClick event on click', async function() {
    let place;

    this.set('place', {
      name: "place name",
    });
    this.set("click", (p) => {
      place = p;
    });
    await render(hbs`<Browse::Places::Preview @place={{this.place}} @onClick={{this.click}}/>`);

    await click(".place-preview");

    expect(place).to.deep.equal({ name: 'place name' });
  });
});
