import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/profile/contributions', function () {
  setupRenderingTest();


  it('renders only the past year', async function () {
    this.set("contributions", []);

    this.set("relativeDate", new Date("2020-10-10T00:00:00Z"));
    await render(hbs`<Browse::Profile::Contributions @relativeDate={{this.relativeDate}} @contributions={{this.contributions}}/>`);

    expect(this.element.querySelector(".day--10-0")).not.to.exist;
    expect(this.element.querySelector(".day--10-1")).not.to.exist;
    expect(this.element.querySelector(".day--10-2")).not.to.exist;
    expect(this.element.querySelector(".day--10-3")).not.to.exist;
    expect(this.element.querySelector(".day--10-4")).not.to.exist;
    expect(this.element.querySelector(".day--10-5")).to.exist;

    expect(this.element.querySelector(".day-41-4")).to.exist;
    expect(this.element.querySelector(".day-41-5")).not.to.exist;
    expect(this.element.querySelector(".day-41-6")).not.to.exist;
  });

  it('renders a contribution from today', async function() {
    this.set("contributions", [
      { year: "2020", day: 1, week: 40, count: 1 }]);

    this.set("relativeDate", new Date("2019-10-01T19:42:27Z"));
    await render(hbs`<Browse::Profile::Contributions @relativeDate={{this.relativeDate}} @contributions={{this.contributions}}/>`);

    expect(this.element.querySelector(".level-cell-1")).to.have.attribute("x", "936");
    expect(this.element.querySelector(".level-cell-1")).to.have.attribute("y", "36");
  });

  it('renders a level 1 contribution', async function () {
    this.set("contributions", [
      { year: "2020", day: 1, week: 10, count: 1 }]);

    this.set("relativeDate", new Date("2020-10-10T00:00:00Z"));
    await render(hbs`<Browse::Profile::Contributions @relativeDate={{this.relativeDate}} @contributions={{this.contributions}}/>`);

    expect(this.element.querySelector(".level-cell-1")).to.exist;
    expect(this.element.querySelector(".level-cell-1.level-10-1")).to.exist;
  });

  it('renders a level 2 contribution', async function () {
    this.set("contributions", [
      { year: "2020", day: 1, week: 10, count: 11 }]);

    this.set("relativeDate", new Date("2020-10-10T00:00:00Z"));
    await render(hbs`<Browse::Profile::Contributions @relativeDate={{this.relativeDate}} @contributions={{this.contributions}}/>`);

    expect(this.element.querySelector(".level-cell-2")).to.exist;
    expect(this.element.querySelector(".level-cell-2.level-10-1")).to.exist;
  });

  it('renders a level 3 contribution', async function () {
    this.set("contributions", [
      { year: "2020", day: 1, week: 10, count: 21 }]);

    this.set("relativeDate", new Date("2020-10-10T00:00:00Z"));
    await render(hbs`<Browse::Profile::Contributions @relativeDate={{this.relativeDate}} @contributions={{this.contributions}}/>`);

    expect(this.element.querySelector(".level-cell-3")).to.exist;
    expect(this.element.querySelector(".level-cell-3.level-10-1")).to.exist;
  });

  it('renders a level 4 contribution', async function () {
    this.set("contributions", [
      { year: "2020", day: 1, week: 10, count: 31 }]);

    this.set("relativeDate", new Date("2020-10-10T00:00:00Z"));
    await render(hbs`<Browse::Profile::Contributions @relativeDate={{this.relativeDate}} @contributions={{this.contributions}}/>`);

    expect(this.element.querySelector(".level-cell-4")).to.exist;
    expect(this.element.querySelector(".level-cell-4.level-10-1")).to.exist;
  });

  it('renders a level 5 contribution', async function () {
    this.set("contributions", [
      { year: "2020", day: 1, week: 10, count: 41 }]);

    this.set("relativeDate", new Date("2020-10-10T00:00:00Z"));
    await render(hbs`<Browse::Profile::Contributions @relativeDate={{this.relativeDate}} @contributions={{this.contributions}}/>`);

    expect(this.element.querySelector(".level-cell-5")).to.exist;
    expect(this.element.querySelector(".level-cell-5.level-10-1")).to.exist;
  });

  it('renders a level 5 contribution when there are more than 50 contributions', async function () {
    this.set("contributions", [
      { year: "2020", day: 1, week: 10, count: 51 }]);

    this.set("relativeDate", new Date("2020-10-10T00:00:00Z"));
    await render(hbs`<Browse::Profile::Contributions @relativeDate={{this.relativeDate}} @contributions={{this.contributions}}/>`);

    expect(this.element.querySelector(".level-cell-5")).to.exist;
    expect(this.element.querySelector(".level-cell-5.level-10-1")).to.exist;
  });
});
