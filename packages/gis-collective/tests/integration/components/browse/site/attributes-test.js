import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/site/attributes', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<Browse::Site::Attributes />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the icons when attributes are set', async function () {
    this.set('value', [
      {
        displayName: 'icon1',
        attributes: [
          {
            displayName: 'key',
            value: 'value',
          },
        ],
      },
    ]);

    await render(hbs`<Browse::Site::Attributes @value={{this.value}}/>`);

    expect(this.element.textContent.trim()).to.contain('icon1');
    expect(this.element.textContent.trim()).to.contain('key');
    expect(this.element.textContent.trim()).to.contain('value');
    expect(this.element.querySelector('.index')).not.to.exist;
  });

  it('does not render the icons when values are not present', async function () {
    this.set('value', [
      {
        displayName: 'icon1',
        attributes: [
          {
            displayName: 'key',
            isPresent: false,
          },
        ],
      },
    ]);

    await render(hbs`<Browse::Site::Attributes @value={{this.value}}/>`);

    expect(this.element.textContent.trim()).to.contain('icon1');
    expect(this.element.textContent.trim()).not.to.contain('key');
  });

  it('renders all when there is an list of values for an attribute', async function () {
    this.set('value', [
      {
        displayName: 'icon1',
        isTable: true,
        attributes: [
          [{ displayName: 'key', value: 'value1' }],
          [{ displayName: 'key', value: 'value2' }],
        ],
      },
    ]);

    await render(hbs`<Browse::Site::Attributes @value={{this.value}}/>`);

    expect(this.element.querySelectorAll('.card-header').length).to.equal(2);
    expect(this.element.querySelectorAll('.browse-attributes').length).to.equal(
      2
    );
    expect(this.element.querySelectorAll('.index').length).to.equal(2);
  });

  it('renders nothing when the attributes are empty', async function () {
    this.set('value', [
      {
        displayName: 'icon1',
        isEmpty: true,
        attributes: [{ displayName: 'key' }],
      },
    ]);

    await render(hbs`<Browse::Site::Attributes @value={{this.value}}/>`);

    expect(this.element.textContent.trim()).to.equal('');
  });
});
