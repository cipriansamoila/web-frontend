import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/site/options', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Browse::Site::Options />`);

    expect(this.element.querySelector(".dropdown-item-issues").textContent.trim()).to.equal('Issues');
    expect(this.element.querySelector(".dropdown-item-edit").textContent.trim()).to.equal('Edit');
  });
});
