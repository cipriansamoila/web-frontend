import { expect } from 'chai';
import { describe, it, before, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestData from '../../../../helpers/test-data';
import Service from '@ember/service';

class MockPosition extends Service {
  hasPosition = true
  center = [0, 0]
}

describe('Integration | Component | browse/site/card', function () {
  setupRenderingTest();
  let defaultIcon;
  let defaultPicture;
  let defaultSound;

  before(function () {
    defaultIcon = new TestData().create.icon();
    defaultPicture = new TestData().create.picture();
    defaultSound = new TestData().create.sound();
  });

  describe("when the position is available", function() {

    beforeEach(function() {
      this.owner.register('service:position', MockPosition);
    });

    it('should render the distance label', async function () {
      this.set("site", {
        name: "name",
        description: "description",
        isPublished: true,
        position: {
          type: "Point",
          coordinates: [10, 10]
        },
        icons: [defaultIcon]
      });

      await render(hbs`<Browse::Site::Card @feature={{this.site}} />`);

      expect(this.element.querySelector(".text-distance")).to.exist;
      expect(this.element.querySelector(".text-distance").textContent.trim()).to.equal("1568.52km");
    });
  });

  describe("when the position is not available", function () {
    beforeEach(function() {
      const positionService = this.owner.lookup('service:position');
      positionService.geolocation = null;
    });

    it('should render a published site with name, description and one icon', async function () {
      this.set("site", {
        name: "name",
        description: "description",
        contentBlocks: {
          blocks: [
            { type: 'header', data: { text: "name", level: 1 } },
            { type: 'paragraph', data: { text: 'description' } }]
        },
        isPublished: true,
        icons: [defaultIcon]
      });
      await render(hbs`<Browse::Site::Card @feature={{this.site}} />`);

      expect(this.element.querySelector(".card-title").textContent.trim()).to.equal('name');
      expect(this.element.querySelector(".site-description").textContent.trim()).to.equal('description');
      expect(this.element.querySelector(".cover")).not.have.attribute("style");
      expect(this.element.querySelectorAll(".browse-icon-list img").length).to.equal(1);
      expect(this.element.querySelector(".icon-not-published")).not.to.exist;
      expect(this.element.querySelector(".icon-has-issues")).not.to.exist;
      expect(this.element.querySelector(".btn-extend")).not.to.exist;
      expect(this.element.querySelector(".btn-issues")).not.to.exist;
      expect(this.element.querySelector(".btn-delete")).not.to.exist;
      expect(this.element.querySelector(".text-distance")).not.to.exist;
      expect(this.element.querySelector(".dropdown-item-edit")).not.to.exist;
    });

    it('should show the cover of a site if it has pictures', async function () {
      this.set("site", {
        pictures: [defaultPicture]
      });
      await render(hbs`<Browse::Site::Card @feature={{this.site}} />`);

      expect(this.element.querySelector(".cover-image")).have.attribute("style", `background-image: url('${defaultPicture.picture}/md')`);
    });

    it('should show the sound if the site has a sound', async function () {
      this.set("site", {
        sounds: [defaultSound],
        firstSound: defaultSound
      });
      await render(hbs`<Browse::Site::Card @feature={{this.site}} />`);

      expect(this.element.querySelector(".sound-container-minimal")).to.exist;
    });

    it('should show an icon for unpublished sites', async function () {
      this.set("site", {
        visibility: 0
      });
      await render(hbs`<Browse::Site::Card @feature={{this.site}} />`);
      expect(this.element.querySelector(".icon-not-published")).to.exist;
    });

    it('should show an icon for pending sites', async function () {
      this.set("site", {
        visibility: -1
      });
      await render(hbs`<Browse::Site::Card @feature={{this.site}} />`);

      expect(this.element.querySelector(".icon-pending")).to.exist;
    });

    it('should show an icon for sites with issues', async function () {
      this.set("site", {
        issueCount: 3
      });
      await render(hbs`<Browse::Site::Card @feature={{this.site}} />`);

      expect(this.element.querySelector(".icon-has-issues")).to.exist;
    });

    it('should render an editable site with name, description and one icon', async function () {
      this.set("site", {
        name: "name",
        description: "description",
        contentBlocks: {
          blocks: [
            { type: 'header', data: { text: "name", level: 1 } },
            { type: 'paragraph', data: { text: 'description' } }]
        },
        visibility: 1,
        icons: [defaultIcon],
        canEdit: true
      });
      await render(hbs`<Browse::Site::Card @feature={{this.site}} />`);

      expect(this.element.querySelector(".card-title").textContent.trim()).to.equal('name');
      expect(this.element.querySelector(".site-description").textContent.trim()).to.equal('description');
      expect(this.element.querySelector(".cover")).not.have.attribute("style");
      expect(this.element.querySelectorAll(".browse-icon-list img").length).to.equal(1);
      expect(this.element.querySelector(".btn-extend")).to.exist;
      expect(this.element.querySelector(".dropdown-item-edit")).to.exist;

      expect(this.element.querySelector(".icon-not-published")).not.to.exist;
      expect(this.element.querySelector(".icon-issues")).not.to.exist;
      expect(this.element.querySelector(".btn-issues")).not.to.exist;
      expect(this.element.querySelector(".btn-delete")).not.to.exist;
    });

    it('should show the extended options by pressing the extend button', async function () {
      this.set("site", {
        name: "name",
        description: "description",
        isPublished: true,
        icons: [defaultIcon],
        canEdit: true
      });
      await render(hbs`<Browse::Site::Card @feature={{this.site}} />`);

      await click(".btn-extend");

      expect(this.element.querySelectorAll(".browse-icon-list img").length).to.equal(1);

      expect(this.element.querySelector(".dropdown-item-issues")).to.exist;
      expect(this.element.querySelector(".dropdown-item-edit")).to.exist;
      expect(this.element.querySelector(".btn-delete")).not.to.exist;
    });

    it('should hide the extended options by pressing twice the extend button', async function () {
      this.set("site", {
        name: "name",
        description: "description",
        isPublished: true,
        icons: [defaultIcon],
        canEdit: true
      });
      await render(hbs`<Browse::Site::Card @feature={{this.site}} />`);

      await click(".btn-extend");
      await click(".btn-extend");

      expect(this.element.querySelectorAll(".browse-icon-list img").length).to.equal(1);
      expect(this.element.querySelector("a.btn-issues")).not.to.exist;
      expect(this.element.querySelector(".btn-delete")).not.to.exist;
    });

    it('should trigger the delete action on click on the delete button', async function () {
      this.set("site", {
        name: "name",
        description: "description",
        isPublished: true,
        icons: [defaultIcon],
        canEdit: true
      });

      let deletedItem;
      this.set("delete", function (item) {
        deletedItem = item;
      });

      await render(hbs`<Browse::Site::Card @feature={{this.site}} @onDelete={{this.delete}}/>`);

      await click(".btn-extend");
      await click(".btn-delete");

      expect(deletedItem).to.equal(this.site);
    });
  });
});
