import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/site/cover', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Browse::Site::Cover />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the feature blurhash when there is no picture', async function() {
    this.set("feature", {
      hashBg: "hashBg"
    });

    await render(hbs`<Browse::Site::Cover @feature={{this.feature}} />`);
    expect(this.element.querySelector(".cover-hash")).to.have.attribute("style", "hashBg");
  });

  it('renders the first picture blurhash when there is a picture', async function() {
    this.set("feature", {
      hashBg: "featureBg",
      pictures: [{
        picture: "some-picture",
        hashBg: "picture-hash"
      }]
    });

    await render(hbs`<Browse::Site::Cover @feature={{this.feature}} />`);
    expect(this.element.querySelector(".cover-hash")).to.have.attribute("style", "picture-hash");
    expect(this.element.querySelector(".cover-image")).to.have.attribute("style", "background-image: url('some-picture/md')");
  });
});
