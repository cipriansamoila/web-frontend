import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | browse/site/preview-import', function () {
  setupRenderingTest();
  let queried = [];

  beforeEach(function () {
    queried = [];
    this.set('queryItem', (model, id) => {
      queried.push(`${model}: ${id}`);

      return {
        id,
        name: 'something',
        fullName: 'something else',
        picture: id,
      };
    });
  });

  it('renders nothing where the value is not set', async function () {
    await render(hbs`<Browse::Site::PreviewImport />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders all the base feature fields when they are set', async function () {
    this.set('value', {
      errors: [],
      result: {
        visibility: 1,
        maps: ['5ca89e42ef1f7e010007f5cb'],
        contributors: ['5b870669796da25424540deb'],
        name: 'Name',
        description: 'the community center run by the city hall.',
        position: { type: 'Point', coordinates: [24.498192, 47.133197] },
      },
    });

    await render(hbs`<Browse::Site::PreviewImport @value={{this.value}}/>`);

    await waitUntil(() => this.element.querySelector('.map'));
    await waitUntil(() => this.element.querySelector('.map').olMap);

    const map = this.element.querySelector('.map').olMap;
    const features = map.getLayers().item(0).getSource().getFeatures();
    const coordinates = features[0]
      .getGeometry()
      .transform('EPSG:3857', 'EPSG:4326')
      .getCoordinates();

    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'Name'
    );
    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'the community center run by the city hall.'
    );
    expect(
      this.element.querySelector('.visibility').textContent.trim()
    ).to.contain('visibility');
    expect(
      this.element.querySelector('.visibility').textContent.trim()
    ).to.contain('public');
    expect(JSON.stringify(coordinates)).to.equal(
      `[24.498192,47.13319699999997]`
    );
  });

  it('renders the update badge when the result has an id', async function () {
    this.set('value', {
      errors: [],
      result: {
        _id: 'some-id',
      },
    });

    await render(
      hbs`<Browse::Site::PreviewImport @queryItem={{this.queryItem}} @value={{this.value}}/>`
    );

    expect(queried).to.deep.equal(['feature: some-id']);
    expect(this.element.querySelector('.badge-update-feature')).to.exist;
    expect(this.element.querySelector('a').textContent.trim()).to.equal(
      'something'
    );
  });

  it('renders an error when it exists', async function () {
    this.set('value', {
      errors: [
        {
          error: 'attributes.some group must contain only objects',
          key: 'a',
          destination: 'b',
        },
      ],
      result: {},
    });

    await render(hbs`<Browse::Site::PreviewImport @value={{this.value}}/>`);

    expect(this.element.querySelector('.error-0').textContent.trim()).to.equal(
      'Cannot convert a to b because attributes.some group must contain only objects'
    );
  });

  it('renders a validation error when it exists', async function () {
    this.set('value', {
      errors: [],
      validationError: {
        code: 'empty-name',
        message: 'The feature must have a name.',
      },
      result: {},
    });

    await render(hbs`<Browse::Site::PreviewImport @value={{this.value}}/>`);

    expect(
      this.element.querySelector('.error-validation').textContent.trim()
    ).to.equal('The record has no name.');
  });

  it('queries and renders the maps when they are set', async function () {
    this.set('value', {
      errors: [],
      result: {
        maps: ['1', '2'],
      },
    });

    await render(
      hbs`<Browse::Site::PreviewImport @queryItem={{this.queryItem}} @value={{this.value}}/>`
    );

    expect(queried).to.deep.equal(['map: 1', 'map: 2']);

    expect(this.element.querySelector('.maps').textContent.trim()).to.contain(
      'maps'
    );

    const links = this.element.querySelectorAll('.maps a');
    expect(links).to.have.length(2);
    expect(links[0].textContent.trim()).to.contain('something');
  });

  it('queries and renders the contributors when they are set', async function () {
    this.set('value', {
      errors: [],
      result: {
        contributors: ['1', '2'],
      },
    });

    await render(
      hbs`<Browse::Site::PreviewImport @queryItem={{this.queryItem}} @value={{this.value}}/>`
    );

    expect(queried).to.deep.equal(['user-profile: 1', 'user-profile: 2']);
    expect(
      this.element.querySelector('.contributors').textContent.trim()
    ).to.contain('contributors');

    const links = this.element.querySelectorAll('.contributors a');
    expect(links).to.have.length(2);
    expect(links[0].textContent.trim()).to.contain('something else');
  });

  it('queries and renders the icons when they are set', async function () {
    this.set('value', {
      errors: [],
      result: {
        icons: ['1', '2'],
      },
    });

    await render(
      hbs`<Browse::Site::PreviewImport @queryItem={{this.queryItem}} @value={{this.value}}/>`
    );

    expect(queried).to.deep.equal(['icon: 1', 'icon: 2']);
    const links = this.element.querySelectorAll('svg');
    expect(links).to.have.length(2);
  });

  it('queries and renders the pictures when they are set', async function () {
    this.set('value', {
      errors: [],
      result: {
        pictures: ['1', '2'],
      },
    });

    await render(
      hbs`<Browse::Site::PreviewImport @queryItem={{this.queryItem}} @value={{this.value}}/>`
    );

    expect(queried).to.deep.equal(['picture: 1', 'picture: 2']);
    const images = this.element.querySelectorAll('img');
    expect(images).to.have.length(2);
    expect(images[0]).to.have.attribute('src', '1/sm');
  });

  it('renders the external pictures when they are set', async function () {
    this.set('value', {
      errors: [],
      result: {
        pictures: ['HTTP://some-pic-1', 'HTTPS://some-pic-2'],
      },
    });

    await render(
      hbs`<Browse::Site::PreviewImport @queryItem={{this.queryItem}} @value={{this.value}}/>`
    );

    expect(queried).to.deep.equal([]);
    const images = this.element.querySelectorAll('img');
    expect(images).to.have.length(2);
    expect(images[0]).to.have.attribute('src', 'HTTP://some-pic-1');
  });

  it('queries and renders the sounds when they are set', async function () {
    this.set('value', {
      errors: [],
      result: {
        sounds: ['1', '2'],
      },
    });

    await render(
      hbs`<Browse::Site::PreviewImport @queryItem={{this.queryItem}} @value={{this.value}}/>`
    );

    expect(queried).to.deep.equal(['sound: 1', 'sound: 2']);

    const sounds = this.element.querySelectorAll('.sound-player');
    expect(sounds).to.have.length(2);
  });

  it('renders the external sounds when they are set', async function () {
    this.set('value', {
      errors: [],
      result: {
        sounds: ['HTTP://some-sound-1', 'HTTPS://some-sound-2'],
      },
    });

    await render(
      hbs`<Browse::Site::PreviewImport @queryItem={{this.queryItem}} @value={{this.value}}/>`
    );

    expect(queried).to.deep.equal([]);

    const sounds = this.element.querySelectorAll('audio');
    expect(sounds).to.have.length(2);
  });

  it('renders the model info fields', async function () {
    this.set('value', {
      errors: [],
      result: {
        info: {
          createdOn: '2020-01-01T00:00:00Z',
          author: '1',
          originalAuthor: '2',
        },
      },
    });

    await render(
      hbs`<Browse::Site::PreviewImport @queryItem={{this.queryItem}} @value={{this.value}}/>`
    );

    expect(queried).to.deep.equal(['user-profile: 1', 'user-profile: 2']);
    expect(this.element.querySelector('.author').textContent.trim()).to.contain(
      'author'
    );
    expect(
      this.element.querySelector('.original-author').textContent.trim()
    ).to.contain('original author');
    expect(
      this.element.querySelector('.created-on').textContent.trim()
    ).to.contain('created on');
    expect(
      this.element.querySelector('.created-on').textContent.trim()
    ).to.contain('2020-01-01T00:00:00Z');

    const links = this.element.querySelectorAll(
      '.author a, .original-author a'
    );
    expect(links).to.have.length(2);
    expect(links[0].textContent.trim()).to.contain('something else');
  });

  it('renders the attribute group objects', async function () {
    this.set('value', {
      errors: [],
      result: {
        attributes: {
          group1: {
            key: 'value',
          },
          group2: {
            key2: 'value2',
          },
        },
      },
    });

    await render(
      hbs`<Browse::Site::PreviewImport @queryItem={{this.queryItem}} @value={{this.value}}/>`
    );

    expect(this.element.querySelector('.browse-attributes')).to.exist;
    const headers = [...this.element.querySelectorAll('.card-header')].map(
      (a) => a.textContent.trim()
    );

    expect(headers).to.deep.equal(['group1', 'group2']);
  });
});
