import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | sheet/title', function () {
  setupRenderingTest();

  it('renders empty string when the title is not set', async function () {
    await render(hbs`<Sheet::Title />`);
    expect(
      this.element.querySelector('.sheet-title').textContent.trim()
    ).to.equal('');
  });

  it('renders the title when is set', async function () {
    await render(hbs`<Sheet::Title @title="some title"/>`);
    expect(
      this.element.querySelector('.sheet-title').textContent.trim()
    ).to.equal('some title');
  });
});
