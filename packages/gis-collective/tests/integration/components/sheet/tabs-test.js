import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import click from '@ember/test-helpers/dom/click';

describe('Integration | Component | sheet/tabs', function () {
  setupRenderingTest();

  it('renders a selected general tab when no icons are set', async function () {
    await render(hbs`<Sheet::Tabs />`);
    expect(
      this.element.querySelector('.nav-link.active').textContent.trim()
    ).to.equal('General');
  });

  it('does not render the next and prev buttons', async function () {
    await render(hbs`<Sheet::Tabs />`);
    expect(this.element.querySelector('.btn-next')).not.to.exist;
    expect(this.element.querySelector('.btn-prev')).not.to.exist;
  });

  it('renders an icon and can be selected', async function () {
    let icon = {
      id: '1',
      localName: 'local name',
      image: {
        value: 'imageValue',
      },
    };

    this.set('icons', [icon]);

    let value;
    this.set('select', (v) => {
      value = v;
    });

    await render(
      hbs`<Sheet::Tabs @icons={{this.icons}} @onSelect={{this.select}} />`
    );

    const tabs = this.element.querySelectorAll('.nav-link');

    await click(tabs[1]);

    expect(value).to.equal(icon);
    expect(
      this.element.querySelector('.nav-link.active').textContent.trim()
    ).to.equal('General');
    expect(tabs).to.have.length(2);
    expect(tabs[0].textContent.trim()).to.equal('General');
    expect(tabs[1].textContent.trim()).to.equal('local name');
  });

  it('can select the general info', async function () {
    let value;
    this.set('select', (v) => {
      value = v;
    });

    await render(
      hbs`<Sheet::Tabs @icons={{this.icons}} @onSelect={{this.select}} />`
    );

    const tabs = this.element.querySelectorAll('.nav-link');

    await click(tabs[0]);

    expect(value).to.equal('general');
  });

  it('renders a selected icon when is a value', async function () {
    let icon = {
      id: '1',
      localName: 'local name',
      image: {
        value: 'imageValue',
      },
    };

    this.set('icon', icon);
    this.set('icons', [icon]);

    await render(
      hbs`<Sheet::Tabs @icons={{this.icons}} @value={{this.icon}} />`
    );

    expect(
      this.element.querySelector('.nav-link.active').textContent.trim()
    ).to.equal('local name');
  });

  it('next and prev icons when there are many icons', async function () {
    let icons = [];

    for (let i = 0; i < 100; i++) {
      icons.push({
        id: i,
        localName: 'icon ' + i,
        image: {
          value: 'imageValue',
        },
      });
    }

    this.set('icons', icons);

    await render(hbs`<Sheet::Tabs @icons={{this.icons}} />`);

    await click('.btn-next');
    expect(this.element.querySelector('.nav').scrollLeft).not.to.equal(0);

    await click('.btn-prev');
    expect(this.element.querySelector('.nav').scrollLeft).to.equal(0);
  });
});
