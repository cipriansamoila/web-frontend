import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import {
  render,
  click,
  fillIn,
  doubleClick,
  waitFor,
  waitUntil,
} from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import { SheetColData } from 'ogm/lib/SheetColData';

describe('Integration | Component | sheet/frame', function () {
  setupRenderingTest();

  it('renders a column name', async function () {
    this.set('cols', [
      {
        name: 'name',
        style: 'width: 200px',
      },
    ]);

    await render(
      hbs`<div style="height: 483px"><Sheet::Frame @cols={{this.cols}}/></div>`
    );

    const header = this.element.querySelectorAll('th');

    expect(header[0].textContent.trim()).to.equal('name');
  });

  it('selects a cell on click', async function () {
    this.set('cols', [
      {
        name: 'name',
        valuePath: 'name',
        style: 'width: 200px',
      },
      {
        name: 'desc',
        valuePath: 'desc',
        style: 'width: 200px',
      },
    ]);

    this.set('records', [
      {
        id: 1,
        name: 'name1',
        desc: 'desc1',
      },
      {
        id: 2,
        name: 'name2',
        desc: 'desc2',
      },
    ]);

    await render(
      hbs`<div style="height: 483px"><Sheet::Frame @records={{this.records}} @cols={{this.cols}}/></div>`
    );

    const td = this.element.querySelectorAll('td.is-first-column');

    await click(td[0]);
    expect(td[0]).to.have.class('is-selected');
    expect(td[0].querySelector('.td-container')).to.have.class('is-selected');
    expect(td[1]).not.to.have.class('is-selected');

    await click(td[1]);
    expect(td[0]).not.to.have.class('is-selected');
    expect(td[1]).to.have.class('is-selected');
  });

  it('raises the on scroll event on first render', async function () {
    this.set('cols', [
      {
        name: 'name',
        valuePath: 'name',
        style: 'width: 200px',
      },
      {
        name: 'desc',
        valuePath: 'desc',
        style: 'width: 200px',
      },
    ]);

    this.set('records', [
      {
        id: 1,
        name: 'name1',
        desc: 'desc1',
      },
      {
        id: 2,
        name: 'name2',
        desc: 'desc2',
      },
    ]);

    let begin;
    let end;

    this.set('onScroll', (b, e) => {
      begin = b;
      end = e;
    });

    await render(
      hbs`<div style="height: 483px"><Sheet::Frame @onScroll={{this.onScroll}} @records={{this.records}} @cols={{this.cols}}/></div>`
    );

    await waitUntil(() => !isNaN(begin));

    expect(begin).to.equal(0);
    expect(end).to.equal(1);
  });

  it('allows editing a text value', async function () {
    this.set('cols', [
      new SheetColData({
        name: 'name',
        valuePath: 'name',
        editorType: 'inline',
        style: 'width: 200px',
      }),
      new SheetColData({
        name: 'desc',
        valuePath: 'desc',
        style: 'width: 200px',
      }),
    ]);

    this.set('records', [
      {
        id: 1,
        name: 'name1',
        desc: 'desc1',
      },
      {
        id: 2,
        name: 'name2',
        desc: 'desc2',
      },
    ]);

    let row = {};
    let key = '';
    let value = '';
    let begin;
    this.set('onScroll', (b) => {
      begin = b;
    });

    this.set('onEdit', (r, k, v) => {
      row = r;
      key = k;
      value = v;
    });

    await render(
      hbs`<div style="height: 483px"><Sheet::Frame @onEdit={{this.onEdit}} @onScroll={{this.onScroll}} @records={{this.records}} @cols={{this.cols}}/></div>`
    );

    await waitUntil(() => !isNaN(begin));

    const cells = this.element.querySelectorAll('.editor-inline');

    await click(cells[0]);

    expect(cells[0]).to.have.attribute('contenteditable', 'true');
    expect(cells[1]).to.have.attribute('contenteditable', null);

    await fillIn(cells[0], 'a & b');

    expect(key).to.equal('');
    expect(value).to.equal('');

    await click(cells[1]);
    expect(key).to.deep.equal({
      name: 'name',
      valuePath: 'name',
      width: 100,
      type: undefined,
      editorType: 'inline',
    });
    expect(value).to.equal('a & b');
    expect(row).to.equal(this.records[0]);
  });

  it('allows editing an article', async function () {
    this.set('cols', [
      new SheetColData({
        name: 'name',
        valuePath: 'name',
        style: 'width: 200px',
      }),
      new SheetColData({
        name: 'desc',
        valuePath: 'desc',
        editorType: 'blocks',
        type: 'blocks-preview',
        style: 'width: 200px',
      }),
    ]);

    this.set('records', [
      {
        id: 1,
        name: 'name1',
        desc: { blocks: [] },
      },
      {
        id: 2,
        name: 'name2',
        desc: { blocks: [] },
      },
    ]);

    let row = {};
    let key = '';
    let value = '';
    let begin;
    this.set('onScroll', (b) => {
      begin = b;
    });

    this.set('onEdit', (r, k, v) => {
      row = r;
      key = k;
      value = v.blocks;
    });

    await render(
      hbs`<div style="height: 483px"><Sheet::Frame @onEdit={{this.onEdit}} @onScroll={{this.onScroll}} @records={{this.records}} @cols={{this.cols}}/></div>`
    );

    await waitUntil(() => !isNaN(begin));

    const cells = this.element.querySelectorAll('.editor-custom');

    await click(cells[0]);
    expect(cells[0]).to.have.attribute('contenteditable', null);
    expect(cells[1]).to.have.attribute('contenteditable', null);

    await doubleClick(cells[0]);
    await waitFor('.editor-is-ready', { timeout: 5000 });

    await click('.add-paragraph-link');
    await click('.btn-set');

    expect(key).to.deep.equal({
      name: 'desc',
      valuePath: 'desc',
      width: 100,
      type: 'blocks-preview',
      editorType: 'blocks',
    });
    expect(value).to.deep.equal([
      { type: 'paragraph', data: { text: 'New paragraph' } },
    ]);
    expect(row).to.equal(this.records[0]);
  });

  it('does not allow editing cols with no editor type', async function () {
    this.set('cols', [
      new SheetColData({
        name: 'name',
        valuePath: 'name',
        style: 'width: 200px',
      }),
      new SheetColData({
        name: 'desc',
        valuePath: 'desc',
        style: 'width: 200px',
      }),
    ]);

    this.set('records', [
      {
        id: 1,
        name: 'name1',
        desc: 'desc1',
      },
      {
        id: 2,
        name: 'name2',
        desc: 'desc2',
      },
    ]);

    let called = false;
    let begin;
    this.set('onScroll', (b) => {
      begin = b;
    });

    this.set('onEdit', () => {
      called = true;
    });

    await render(
      hbs`<div style="height: 483px"><Sheet::Frame @onEdit={{this.onEdit}} @onScroll={{this.onScroll}} @records={{this.records}} @cols={{this.cols}}/></div>`
    );

    await waitUntil(() => !isNaN(begin));

    const cells = this.element.querySelectorAll('.td-container');

    await click(cells[0]);

    expect(cells[0]).to.have.attribute('contenteditable', null);
    expect(cells[1]).to.have.attribute('contenteditable', null);

    expect(called).to.equal(false);

    await click(cells[1]);
    expect(called).to.equal(false);
  });
});
