import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | modal-container', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<ModalContainer @title="Modal title" />`);
    expect(this.element.textContent.trim()).to.contain('Modal title');
  });
});
