import { expect } from 'chai';
import { describe, it, before, after, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../helpers/test-server';

describe('Integration | Component | presentation-slider', function () {
  setupRenderingTest();
  let server;

  before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle('1');
    server.testData.storage.addDefaultPicture('2');
  });

  after(function () {
    server.shutdown();
  });

  it('renders nothing when there is no argument', async function () {
    await render(hbs`<PresentationSlider />`);
    expect(this.element.textContent.trim()).to.equal('');
    const slides = this.element.querySelectorAll('.swiper-slide');

    expect(this.element.textContent.trim()).to.equal('');
    expect(slides).to.have.length(0);
  });

  it('renders nothing when there is an empty col list', async function () {
    this.set('cols', []);
    await render(hbs`<PresentationSlider @cols={{this.cols}}/>`);
    const slides = this.element.querySelectorAll('.swiper-slide');

    expect(this.element.textContent.trim()).to.equal('');
    expect(slides).to.have.length(0);
  });

  it('renders two columns when they are set', async function () {
    this.set('cols', [
      {
        col: 0,
        row: 0,
        type: 'article',
        data: {
          id: '1',
        },
      },
      {
        col: 1,
        row: 0,
        type: 'picture',
        data: {
          id: '2',
        },
      },
    ]);

    await render(hbs`<PresentationSlider @cols={{this.cols}}/>`);
    const articleSlides = this.element.querySelectorAll('.article-page');
    const contentSlides = this.element.querySelectorAll('.content-page');

    expect(articleSlides).to.have.length(1);
    expect(contentSlides).to.have.length(1);

    expect(this.element.querySelector('h1').textContent.trim()).to.contain(
      'title'
    );
    expect(
      this.element.querySelector('.article p').textContent.trim()
    ).to.equal('some content');
    expect(this.element.querySelector('img')).to.have.attribute(
      'src',
      'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/lg'
    );
  });

  describe('when there are two pages', function () {
    beforeEach(async function () {
      this.set('cols', [
        {
          col: 0,
          row: 0,
          type: 'article',
          data: {
            id: '1',
          },
        },
        {
          col: 1,
          row: 0,
          type: 'picture',
          data: {
            id: '2',
          },
        },
        {
          col: 0,
          row: 1,
          type: 'article',
          data: {
            id: '1',
          },
        },
        {
          col: 1,
          row: 1,
          type: 'picture',
          data: {
            id: '2',
          },
        },
      ]);
    });

    describe('when there is an intro and an end', function () {
      beforeEach(async function () {
        await render(hbs`<PresentationSlider @cols={{this.cols}}/>`);
      });

      it('shows the first slide when the intro is done', async function () {
        render(
          hbs`<PresentationSlider @cols={{this.cols}} @hasIntro={{true}} @hasEnd={{true}} @introTimeout={{1000}}/>`
        );

        await waitUntil(() =>
          this.element?.querySelector('.slides-welcome-intro.current')
        );
        const currentPage = this.element.querySelectorAll('.current');
        expect(currentPage).to.have.length(1);
        expect(currentPage[0]).to.have.class('slides-welcome-intro');

        await waitUntil(() =>
          this.element?.querySelector('.article-page-0.current')
        );
      });

      it('hides the controls on the intro page', async function () {
        render(
          hbs`<PresentationSlider @cols={{this.cols}} @hasIntro={{true}} @hasEnd={{true}} @introTimeout={{1000}}/>`
        );

        await waitUntil(() =>
          this.element?.querySelector('.slides-welcome-intro.current')
        );

        const currentPage = this.element.querySelectorAll('.current');
        expect(currentPage).to.have.length(1);
        expect(currentPage[0]).to.have.class('slides-welcome-intro');
        expect(this.element.querySelector('.controls')).to.have.class('d-none');

        await waitUntil(() =>
          this.element?.querySelector('.article-page-0.current')
        );
        expect(this.element.querySelector('.controls')).not.to.have.class(
          'd-none'
        );
      });

      it('shows the end slide after the last one', async function () {
        await render(
          hbs`<PresentationSlider @cols={{this.cols}} @hasEnd={{true}}/>`
        );

        await click('.btn-next');
        await click('.btn-next');

        const currentPage = this.element.querySelectorAll('.current');
        expect(currentPage).to.have.length(1);
        expect(currentPage[0]).to.have.class('slides-welcome-end');
        expect(this.element.querySelector('.controls')).to.have.class('d-none');
      });
    });

    describe('when there is no intro or end', function () {
      beforeEach(async function () {
        await render(hbs`<PresentationSlider @cols={{this.cols}}/>`);
      });

      it('renders all columns', async function () {
        const articleSlides = this.element.querySelectorAll('.article-page');
        const contentSlides = this.element.querySelectorAll('.content-page');
        const titles = this.element.querySelectorAll('h1');
        const images = this.element.querySelectorAll('img');

        expect(articleSlides).to.have.length(2);
        expect(contentSlides).to.have.length(2);
        expect(titles).to.have.length(4);
        expect(images).to.have.length(4);

        expect(titles[0].textContent.trim()).to.contain('title');
        expect(titles[1].textContent.trim()).to.contain('title');
        expect(images[0]).to.have.attribute(
          'src',
          'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/lg'
        );
        expect(images[1]).to.have.attribute(
          'src',
          'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/lg'
        );
      });

      it('shows the first page', async function () {
        const pages = this.element.querySelectorAll('.current');

        expect(pages).to.have.length(2);
        expect(pages[0]).to.have.class('article-page-0');
        expect(pages[1]).to.have.class('content-page-0');
      });

      it('shows the second page when the next button is pressed', async function () {
        await click('.btn-next');
        const pages = this.element.querySelectorAll('.current');

        expect(pages).to.have.length(2);
        expect(pages[0]).to.have.class('article-page-1');
        expect(pages[1]).to.have.class('content-page-1');
        expect(
          this.element.querySelector('.presentation-label').textContent
        ).to.contain('2 of 2');
      });

      it('shows the second page when the next button is pressed many times', async function () {
        await click('.btn-next');
        await click('.btn-next');
        const pages = this.element.querySelectorAll('.current');

        expect(pages).to.have.length(2);
        expect(pages[0]).to.have.class('article-page-1');
        expect(pages[1]).to.have.class('content-page-1');
        expect(
          this.element.querySelector('.presentation-label').textContent
        ).to.contain('2 of 2');
      });

      it('shows the first page when the prev button is pressed', async function () {
        await click('.btn-next');
        await click('.btn-prev');
        const pages = this.element.querySelectorAll('.current');

        expect(pages).to.have.length(2);
        expect(pages[0]).to.have.class('article-page-0');
        expect(pages[1]).to.have.class('content-page-0');
        expect(
          this.element.querySelector('.presentation-label').textContent
        ).to.contain('1 of 2');
      });

      it('shows the first page when the prev button is pressed many times', async function () {
        await click('.btn-next');
        await click('.btn-prev');
        await click('.btn-prev');
        const pages = this.element.querySelectorAll('.current');

        expect(pages).to.have.length(2);
        expect(pages[0]).to.have.class('article-page-0');
        expect(pages[1]).to.have.class('content-page-0');
        expect(
          this.element.querySelector('.presentation-label').textContent
        ).to.contain('1 of 2');
      });
    });
  });
});
