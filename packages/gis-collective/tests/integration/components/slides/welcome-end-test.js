import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | slides/welcome-end', function () {
  setupRenderingTest();

  it('triggers onPrevPage when the back button is pressed', async function () {
    let called;

    this.set('prevPage', () => {
      called = true;
    });

    await render(hbs`<Slides::WelcomeEnd @onPrevPage={{this.prevPage}}/>`);
    await click('.btn-back');

    expect(called).to.equal(true);
  });
});
