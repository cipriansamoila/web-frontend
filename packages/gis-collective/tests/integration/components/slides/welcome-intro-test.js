import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | slides/welcome-intro', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<Slides::WelcomeIntro />`);
    expect(this.element.textContent.trim().toLocaleLowerCase()).to.contain(
      'welcome'
    );
  });
});
