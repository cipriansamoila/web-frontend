import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';

describe('Integration | Component | page-col/feature-card', function () {
  setupRenderingTest();
  let server;
  let feature;

  before(function () {
    server = new TestServer();
    feature = server.testData.storage.addDefaultFeature('1');
  });

  after(function () {
    server.shutdown();
  });

  it('renders a feature when the value is set', async function () {
    this.set('value', feature);

    await render(hbs`<PageCol::FeatureCard @value={{this.value}} />`);

    expect(this.element.querySelector('.site-card')).to.exist;
    expect(
      this.element.querySelector('.card-title').textContent.trim()
    ).to.equal('Nomadisch Grün - Local Urban Food');
    expect(this.element.querySelector('.card-title')).to.have.attribute(
      'data-route',
      'browse.sites.site'
    );
  });

  it('can use a custom route', async function () {
    this.set('value', feature);
    this.set('options', {
      route: 'browse.sites.other-site',
    });

    await render(
      hbs`<PageCol::FeatureCard @value={{this.value}} @options={{this.options}} />`
    );

    expect(this.element.querySelector('.card-title')).to.have.attribute(
      'data-route',
      'browse.sites.other-site'
    );
  });
});
