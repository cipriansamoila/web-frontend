import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | page-col/feature-visibility', function () {
  setupRenderingTest();

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<PageCol::FeatureVisibility />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders "public" when the value is 1', async function () {
    await render(hbs`<PageCol::FeatureVisibility @value={{1}}/>`);
    expect(this.element.textContent.trim()).to.equal('public');
  });

  it('renders "private" when the value is 0', async function () {
    await render(hbs`<PageCol::FeatureVisibility @value={{0}}/>`);
    expect(this.element.textContent.trim()).to.equal('private');
  });

  it('renders "public" when the value is 1', async function () {
    await render(hbs`<PageCol::FeatureVisibility @value={{-1}}/>`);
    expect(this.element.textContent.trim()).to.equal('pending');
  });
});
