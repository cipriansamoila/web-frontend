import { expect } from 'chai';
import { describe, it, before } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';
import waitUntil from '@ember/test-helpers/wait-until';

describe('Integration | Component | page-col/pictures', function () {
  setupRenderingTest();

  let server;
  let picture1;
  let picture2;

  before(function () {
    server = new TestServer();

    picture1 = server.testData.create.picture('1');
    picture2 = server.testData.create.picture('2');

    server.testData.storage.addPicture(picture1);
    server.testData.storage.addPicture(picture2);

    server.post('/mock-server/pictures', (request) => {
      const pictureRequest = JSON.parse(request.requestBody);
      pictureRequest.picture['_id'] = '5cc8dc1038e882010061545a';

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(pictureRequest),
      ];
    });
  });

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<PageCol::Pictures />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  describe('when the value is a page col', function () {
    it('renders a list of pictures', async function () {
      this.set('value', { data: { records: [picture1, picture2] } });
      await render(hbs`<PageCol::Pictures @value={{this.value}} />`);

      expect(this.element.querySelector('img')).to.have.attribute(
        'src',
        'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/lg'
      );
    });

    it('can apply the paddings', async function () {
      this.set('value', {
        data: {
          records: [picture1, picture2],
          options: ['pt-1', 'ps-md-2', 'pe-lg-3'],
        },
      });

      await render(hbs`<PageCol::Pictures @value={{this.value}} />`);
      expect(this.element.querySelector('img')).to.have.attribute(
        'class',
        'pt-1 ps-md-2 pe-lg-3'
      );
      expect(
        this.element.querySelector('.page-col-picture-list')
      ).to.have.attribute('class', 'page-col-picture-list');
    });

    it('can apply the text align', async function () {
      this.set('value', {
        data: {
          records: [picture1, picture2],
          containerOptions: ['text-center'],
        },
      });

      await render(hbs`<PageCol::Pictures @value={{this.value}} />`);
      expect(
        this.element.querySelector('.page-col-picture-list')
      ).to.have.attribute('class', 'page-col-picture-list text-center');
    });

    it('can apply the height', async function () {
      this.set('value', {
        data: {
          records: [picture1, picture2],
          heightSm: 100,
          heightMd: 200,
          heightLg: 300,
        },
      });

      await render(hbs`<PageCol::Pictures @value={{this.value}} />`);

      await waitUntil(() => this.element.querySelector('img').offsetHeight);

      expect(this.element.querySelector('img').offsetHeight).to.equal(300);
    });
  });
});
