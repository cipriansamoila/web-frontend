import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';

describe('Integration | Component | page-col/icons', function () {
  setupRenderingTest();

  let server;
  let icon1;
  let icon2;

  before(function () {
    server = new TestServer();

    icon1 = server.testData.create.icon('1');
    icon2 = server.testData.create.icon('2');

    server.testData.storage.addIcon(icon1);
    server.testData.storage.addIcon(icon2);

    server.post('/mock-server/icons', (request) => {
      const iconRequest = JSON.parse(request.requestBody);
      iconRequest.icon['_id'] = '5cc8dc1038e882010061545a';

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(iconRequest),
      ];
    });
  });

  after(function () {
    server.shutdown();
  });

  describe('when the value is a list of icons', function () {
    it('renders a list of icons', async function () {
      this.set('value', [icon1, icon2]);
      await render(hbs`<PageCol::Icons @value={{this.value}} />`);

      expect(this.element.querySelectorAll('img')).to.have.length(2);
      expect(this.element.querySelector('img')).to.have.attribute(
        'src',
        'https://new.opengreenmap.org/api-v1/icons/5ca7bfbfecd8490100cab97d/image'
      );
    });
  });

  describe('when the value is a list of wrapped ids', function () {
    it('renders a small list of icons without links', async function () {
      this.set('value', { data: { ids: [icon1._id, icon2._id] } });
      await render(hbs`<PageCol::Icons @value={{this.value}} />`);

      expect(this.element.querySelector('.browse-icon-list.size-lg')).not.to
        .exist;
      expect(this.element.querySelectorAll('a')).to.have.length(0);
      expect(this.element.querySelectorAll('img')).to.have.length(2);
      expect(this.element.querySelector('img')).to.have.attribute(
        'src',
        'https://new.opengreenmap.org/api-v1/icons/5ca7bfbfecd8490100cab97d/image'
      );
    });

    it('renders a small list of icons with gutter and row col size', async function () {
      this.set('value', {
        data: { ids: [icon1._id, icon2._id], rowColSize: '4', gutter: '5' },
      });
      await render(hbs`<PageCol::Icons @value={{this.value}} />`);

      expect(this.element.querySelector('.row.g-5.row-cols-4')).to.exist;
    });

    it('renders a small list of icons with links', async function () {
      this.set('value', {
        data: {
          ids: [icon1._id, icon2._id],
          viewMode: 'small',
          links: 'enabled',
        },
      });
      await render(hbs`<PageCol::Icons @value={{this.value}} />`);

      expect(this.element.querySelector('.browse-icon-list.size-lg')).not.to
        .exist;
      expect(this.element.querySelectorAll('a')).to.have.length(2);
      expect(this.element.querySelectorAll('img')).to.have.length(2);
      expect(this.element.querySelector('img')).to.have.attribute(
        'src',
        'https://new.opengreenmap.org/api-v1/icons/5ca7bfbfecd8490100cab97d/image'
      );
    });

    it('renders a medium list of icons with links', async function () {
      this.set('value', {
        data: {
          ids: [icon1._id, icon2._id],
          viewMode: 'medium',
          links: 'enabled',
        },
      });
      await render(hbs`<PageCol::Icons @value={{this.value}} />`);

      expect(this.element.querySelector('.browse-icon-list.size-lg')).to.exist;
      expect(this.element.querySelectorAll('a')).to.have.length(2);
      expect(this.element.querySelectorAll('img')).to.have.length(2);
      expect(this.element.querySelector('img')).to.have.attribute(
        'src',
        'https://new.opengreenmap.org/api-v1/icons/5ca7bfbfecd8490100cab97d/image'
      );
    });

    it('renders a card list of icons with links', async function () {
      this.set('value', {
        data: {
          ids: [icon1._id, icon2._id],
          viewMode: 'cards',
          links: 'enabled',
        },
      });
      await render(hbs`<PageCol::Icons @value={{this.value}} />`);

      expect(this.element.querySelector('.browse-icon-list.size-lg')).not.to
        .exist;
      expect(this.element.querySelectorAll('a')).to.have.length(2);
      expect(
        this.element.querySelector('.card-body a.icon-name').textContent.trim()
      ).to.equal('Healthy Dining');
      expect(this.element.querySelector('img')).to.have.attribute(
        'src',
        'https://new.opengreenmap.org/api-v1/icons/5ca7bfbfecd8490100cab97d/image/md'
      );
    });
  });
});
