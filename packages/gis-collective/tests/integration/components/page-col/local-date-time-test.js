import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | page-col/local-date-time', function () {
  setupRenderingTest();

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<PageCol::LocalDateTime />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a date object value as a local date', async function () {
    this.set('value', Date.parse('2019-01-01T00:00:00.000Z'));

    await render(hbs`<PageCol::LocalDateTime @value={{this.value}} />`);

    expect(this.element.querySelector('.date').textContent.trim()).to.equal(
      '1/1/2019'
    );
    expect(this.element.querySelector('.time').textContent.trim()).to.contain(
      `00:00 AM`
    );
  });

  it('renders a date string value as a local date', async function () {
    this.set('value', '2019-01-01T00:00:00.000Z');

    await render(hbs`<PageCol::LocalDateTime @value={{this.value}} />`);

    expect(this.element.querySelector('.date').textContent.trim()).to.equal(
      '1/1/2019'
    );
    expect(this.element.querySelector('.time').textContent.trim()).to.contain(
      `00:00 AM`
    );
  });
});
