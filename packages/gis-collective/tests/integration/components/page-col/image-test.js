import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | page-col/image', function () {
  setupRenderingTest();

  it('renders nothing when there is no value', async function () {
    await render(hbs`<PageCol::Image />`);
    expect(this.element.querySelector('img')).not.to.exist;

    expect(this.element.querySelector('.page-col-image')).not.to.have.class(
      'image-tool--stretched'
    );
    expect(this.element.querySelector('.page-col-image')).not.to.have.class(
      'image-tool--withBorder'
    );
    expect(this.element.querySelector('.page-col-image')).not.to.have.class(
      'image-tool--withBackground'
    );
  });

  it('renders an image when the file is set', async function () {
    this.set('value', {
      type: 'image',
      data: {
        withBorder: false,
        file: {
          url: 'some url',
        },
        stretched: true,
        withBackground: false,
        caption: '',
      },
    });

    await render(hbs`<PageCol::Image @value={{this.value}} />`);

    expect(this.element.querySelector('img')).to.exist;
    expect(this.element.querySelector('img')).to.exist;
    expect(this.element.querySelector('.caption')).not.to.exist;
    expect(this.element.querySelector('img')).to.have.attribute(
      'src',
      'some url'
    );
  });

  it('renders a caption when is set', async function () {
    this.set('value', {
      type: 'image',
      data: {
        withBorder: false,
        file: {
          url: 'some url',
        },
        stretched: true,
        withBackground: false,
        caption: 'some caption',
      },
    });

    await render(hbs`<PageCol::Image @value={{this.value}} />`);

    expect(this.element.querySelector('.page-col-image-caption')).to.exist;
    expect(
      this.element.querySelector('.page-col-image-caption').textContent.trim()
    ).to.equal('some caption');
  });

  it('sets all classes when all attributes are set to true', async function () {
    this.set('value', {
      type: 'image',
      data: {
        withBorder: true,
        file: {
          url: 'some url',
        },
        stretched: true,
        withBackground: true,
        caption: '',
      },
    });

    await render(hbs`<PageCol::Image @value={{this.value}} />`);

    expect(this.element.querySelector('.page-col-image')).to.have.class(
      'image-tool--stretched'
    );
    expect(this.element.querySelector('.page-col-image')).to.have.class(
      'image-tool--filled'
    );
    expect(this.element.querySelector('.page-col-image')).to.have.class(
      'image-tool--withBorder'
    );
    expect(this.element.querySelector('.page-col-image')).to.have.class(
      'image-tool--withBackground'
    );
  });
});
