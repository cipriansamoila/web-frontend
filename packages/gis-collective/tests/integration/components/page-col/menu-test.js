import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';

describe('Integration | Component | page-col/menu', function () {
  setupRenderingTest();
  let service;
  let server;
  let space;

  this.beforeEach(async function () {
    server = new TestServer();
    service = this.owner.lookup('service:space');
    space = server.testData.storage.addDefaultSpace();
    await service.setup();
  });

  it('renders the space buttons', async function () {
    await render(hbs`<PageCol::Menu />`);

    const links = this.element.querySelectorAll('a');

    expect(links).to.have.length(3);
    expect(links[0].textContent.trim()).to.equal('About');
    expect(links[0]).to.have.attribute('href', '/61292c4c7bdf9301008fd7be');
    expect(links[1].textContent.trim()).to.equal('Browse');
    expect(links[1]).to.have.attribute('href', '/61292c4c7bdf9301008fd7bf');
    expect(links[2].textContent.trim()).to.equal('Propose a site');
    expect(links[2]).to.have.attribute('href', '/61292c4c7bdf9301008fd7bg');
  });

  describe('when the space has an external link', function () {
    this.beforeEach(async function () {
      space.menu.items[0].link = {
        url: 'https://giscollective.com',
      };
      await service.setup();
    });

    it('renders the external link', async function () {
      await render(hbs`<PageCol::Menu />`);

      const links = this.element.querySelectorAll('a');

      expect(links).to.have.length(3);
      expect(links[0].textContent.trim()).to.equal('About');
      expect(links[0]).to.have.attribute('href', 'https://giscollective.com');
    });
  });
});
