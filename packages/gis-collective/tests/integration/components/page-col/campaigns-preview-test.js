import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';

describe('Integration | Component | page-col/campaigns-preview', function () {
  setupRenderingTest();
  let server;

  before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultTeam('1');
  });

  after(function () {
    server.shutdown();
  });

  it('renders all teams when there is no value', async function () {
    await render(hbs`<PageCol::CampaignsPreview />`);
    expect(this.element.textContent.trim()).to.equal(
      'campaigns for: all teams'
    );
  });

  it('renders the team name when is set', async function () {
    this.set('value', { data: { query: { team: '1' } } });
    await render(hbs`<PageCol::CampaignsPreview @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.equal(
      'campaigns for: Open Green Map'
    );
  });
});
