import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | page-col/icon-list', function () {
  setupRenderingTest();

  it('renders nothing when no value is set', async function () {
    await render(hbs`<PageCol::IconList />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a list of icons', async function () {
    this.set('value', [
      {
        image: {
          useParent: false,
          value:
            'http://localhost:9091/icons/5ca7bfd2ecd8490100cab9b5/image/value',
        },
      },
      {
        image: {
          useParent: false,
          value:
            'http://localhost:9091/icons/5ca7bfd2ecd8490100cab9b5/image/value',
        },
      },
    ]);

    await render(hbs`<PageCol::IconList @value={{this.value}} />`);
    expect(this.element.querySelectorAll('.icon')).to.have.length(2);
    expect(this.element.querySelector('.icon')).to.have.attribute(
      'src',
      'http://localhost:9091/icons/5ca7bfd2ecd8490100cab9b5/image/value'
    );
  });
});
