import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | page-col/map-list-text', function () {
  setupRenderingTest();

  it('renders nothing when there is no value', async function () {
    await render(hbs`<PageCol::MapNameList />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a list of maps with links when the value is set', async function () {
    this.set('value', [
      {
        id: '1',
        name: 'map 1',
      },
      {
        id: '2',
        name: 'map 2',
      },
    ]);

    await render(hbs`<PageCol::MapNameList @value={{this.value}}/>`);
    const links = this.element.querySelectorAll('a');

    expect(links[0].textContent.trim()).to.equal('map 1');
    expect(links[1].textContent.trim()).to.equal('map 2');
  });
});
