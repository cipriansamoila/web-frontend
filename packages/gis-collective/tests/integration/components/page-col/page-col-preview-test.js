import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';
import waitFor from '@ember/test-helpers/dom/wait-for';

describe('Integration | Component | page-col/preview', function () {
  setupRenderingTest();
  let server;

  before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle('1');
    server.testData.storage.addDefaultArticle('2');
    server.testData.storage.addDefaultPicture('1');
  });

  after(function () {
    server.shutdown();
  });

  it('renders nothing when a col is not set', async function () {
    await render(hbs`<PageCol::Preview />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders an article column', async function () {
    this.set('value', {
      type: 'article',
      data: {
        id: '1',
      },
    });

    await render(hbs`<PageCol::Preview @value={{this.value}}/>`);
    expect(
      this.element.querySelector('.article-preview').textContent
    ).to.contain('some content');
  });

  it('renders a picture column', async function () {
    this.set('value', {
      type: 'picture',
      data: {
        id: '1',
        model: 'picture',
      },
    });

    await render(hbs`<PageCol::Preview @value={{this.value}}/>`);
    await waitFor('.picture-preview img');

    expect(
      this.element.querySelector('.picture-preview img')
    ).to.have.attribute(
      'src',
      'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/sm'
    );
  });
});
