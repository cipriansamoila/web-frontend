import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | page-col/view', function () {
  setupRenderingTest();

  it('renders nothing when there is no argument', async function () {
    await render(hbs`<PageCol::View />`);
    expect(this.element.textContent.trim()).to.equal('');
  });
});
