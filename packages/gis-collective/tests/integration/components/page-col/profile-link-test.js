import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';

describe('Integration | Component | page-col/profile-link', function () {
  setupRenderingTest();
  let server;
  let profile;

  before(function () {
    server = new TestServer();
    profile = server.testData.storage.addDefaultProfile('1');
  });

  after(function () {
    server.shutdown();
  });

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<PageCol::ProfileLink />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the profile name and a link when the profile exists', async function () {
    this.set('value', profile._id);

    await render(hbs`<PageCol::ProfileLink @value={{this.value}} />`);
    expect(this.element.textContent.trim()).to.equal('mr Bogdan Szabo');
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/browse/profiles/1'
    );
  });

  it('renders the profile id when it does not exist', async function () {
    this.set('value', 'other');

    await render(hbs`<PageCol::ProfileLink @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.equal('other');
    expect(this.element.querySelector('a')).not.to.exist;
  });

  it('renders the profile when it contains an @', async function () {
    this.set('value', '@other');

    await render(hbs`<PageCol::ProfileLink @value={{this.value}} />`);

    expect(this.element.textContent.trim()).to.equal('@other');
    expect(this.element.querySelector('a')).not.to.exist;
  });

  it('renders nothing when the value is an empty object', async function () {
    this.set('value', {});

    await render(hbs`<PageCol::ProfileLink @value={{this.value}} />`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector('a')).not.to.exist;
  });

  it('renders the profile when the value has a fullname and id', async function () {
    this.set('value', { id: 'id', fullName: 'fullName' });

    await render(hbs`<PageCol::ProfileLink @value={{this.value}} />`);
    expect(this.element.textContent.trim()).to.equal('fullName');
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/browse/profiles/id'
    );
  });
});
