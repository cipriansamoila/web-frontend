import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | page-col/sound-list', function () {
  setupRenderingTest();

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<PageCol::SoundList />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders two sounds', async function () {
    this.set('value', [
      {
        sound: 'sound1',
        name: 'name1',
      },
      {
        sound: 'sound2',
        name: 'name2',
      },
    ]);

    await render(hbs`<PageCol::SoundList @value={{this.value}} />`);

    expect(
      this.element.querySelectorAll('.sound-container-minimal')
    ).to.have.length(2);
    expect(this.element.querySelectorAll('audio')).to.have.length(2);
    expect(this.element.querySelectorAll('source')[0]).to.have.attribute(
      'src',
      'sound1'
    );
    expect(this.element.querySelectorAll('source')[1]).to.have.attribute(
      'src',
      'sound2'
    );
  });
});
