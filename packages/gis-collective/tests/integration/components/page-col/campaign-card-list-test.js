import { expect } from 'chai';
import { describe, it, before, after, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';
import click from '@ember/test-helpers/dom/click';

describe('Integration | Component | page-col/campaign-card-list', function () {
  setupRenderingTest();
  let server;
  let campaign;

  before(function () {
    server = new TestServer();
    campaign = server.testData.storage.addDefaultCampaign('1');
  });

  after(function () {
    server.shutdown();
  });

  beforeEach(function () {
    this.owner.lookup('service:notifications').ask = async () => {
      return 'yes';
    };
  });

  it('renders the preloaded campaigns when they are set', async function () {
    server.history = [];
    this.set('value', {
      data: { query: { team: '1' } },
      modelKey: 'campaigns_team_1',
    });

    this.set('model', {
      campaigns_team_1: { columns: [[campaign]] },
    });

    await render(
      hbs`<PageCol::CampaignCardList @model={{this.model}} @value={{this.value}} />`
    );

    expect(this.element.querySelectorAll('.campaign-card')).to.have.length(1);
    expect(server.history).to.deep.equal([]);
  });

  it('allows deleting a record', async function () {
    this.set('value', {
      data: { query: { team: '1' } },
      modelKey: 'campaigns_team_1',
    });

    this.set('model', {
      campaigns_team_1: { columns: [[campaign]] },
    });

    server.server.delete(`/mock-server/campaigns/${campaign._id}`, () => {
      return [204, { 'Content-Type': 'application/json' }, JSON.stringify({})];
    });

    let destroyRecordCalled = false;
    campaign.canEdit = true;
    campaign.destroyRecord = () => {
      destroyRecordCalled = true;
    };

    await render(
      hbs`<PageCol::CampaignCardList @model={{this.model}} @value={{this.value}} />`
    );

    await waitFor('.campaign-card .btn-delete');
    await click('.campaign-card .btn-delete');

    expect(destroyRecordCalled).to.equal(true);
  });
});
