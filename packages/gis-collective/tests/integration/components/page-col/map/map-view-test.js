import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { click, render, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../../helpers/test-server';

describe('Integration | Component | page-col/map/map-view', function () {
  setupRenderingTest();

  let server;
  let map;
  let baseMap1;
  let baseMap2;

  before(function () {
    server = new TestServer();
    map = server.testData.storage.addDefaultMap('1');
    baseMap1 = server.testData.storage.addDefaultBaseMap();
    baseMap2 = server.testData.storage.addDefaultBaseMap('2');

    baseMap1.cover = {
      _id: '1',
      picture: '1',
      isLoaded: true,
    };
    baseMap1.attributions = [
      {
        name: '© OpenStreetMap contributors',
        url: 'https://www.openstreetmap.org/copyright',
      },
    ];
    baseMap2.cover = {
      _id: '2',
      picture: '2',
      isLoaded: true,
    };

    baseMap1['layers'] = [
      {
        type: 'Open Street Maps',
        options: {},
      },
    ];

    baseMap2['layers'] = [
      {
        type: 'Open Street Maps',
        options: {},
      },
    ];

    map.id = map._id;
    map.baseMaps.list = [baseMap1, baseMap2];
    map.license = {
      name: 'CC BY-NC 4.0',
      url: 'https://creativecommons.org/licenses/by-nc/4.0/',
    };
  });

  after(function () {
    server.shutdown();
  });

  it('renders the map layers', async function () {
    this.set('value', {
      data: { query: { map: '1' } },
      modelKey: 'map_1',
    });

    this.set('model', {
      map_1: map,
    });

    await render(
      hbs`<PageCol::Map::MapView @model={{this.model}} @value={{this.value}} />`
    );

    const olMap = this.element.querySelector('.map').olMap;
    const layers = olMap.getLayers().getArray();
    expect(layers).to.have.length(2);

    expect(
      this.element.querySelector('.map-layer-base').textContent.trim()
    ).to.equal(baseMap1._id);
    expect(
      this.element.querySelector('.vector-tiles').textContent.trim()
    ).to.equal('/api-v1/tiles/{z}/{x}/{y}?format=vt&map=1');
  });

  it('allows to switch the base map', async function () {
    this.set('value', {
      data: { query: { map: '1' } },
      modelKey: 'map_1',
    });

    this.set('model', {
      map_1: map,
    });

    await render(
      hbs`<PageCol::Map::MapView @model={{this.model}} @value={{this.value}} />`
    );

    await waitFor('.base-map-select .item');
    await click('.base-map-select .item');

    expect(
      this.element.querySelector('.map-layer-base').textContent.trim()
    ).to.equal(baseMap2._id);
  });

  it('shows the map license', async function () {
    this.set('value', {
      data: { query: { map: '1' } },
      modelKey: 'map_1',
    });

    this.set('model', {
      map_1: map,
    });

    await render(
      hbs`<PageCol::Map::MapView @model={{this.model}} @value={{this.value}} />`
    );

    const links = this.element.querySelectorAll('.attributions a');
    expect(links).to.have.length(2);
    expect(links[0]).to.have.attribute(
      'href',
      'https://creativecommons.org/licenses/by-nc/4.0/'
    );
    expect(links[0].textContent.trim()).to.equal('CC BY-NC 4.0');
    expect(links[1]).to.have.attribute(
      'href',
      'https://www.openstreetmap.org/copyright'
    );
    expect(links[1].textContent.trim()).to.equal(
      '© OpenStreetMap contributors'
    );
  });
});
