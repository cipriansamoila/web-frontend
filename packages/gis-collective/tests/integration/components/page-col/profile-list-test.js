import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';

describe('Integration | Component | page-col/profile-list', function () {
  setupRenderingTest();
  let server;
  let profile1;
  let profile2;

  before(function () {
    server = new TestServer();
    profile1 = server.testData.storage.addDefaultProfile('1');
    profile2 = server.testData.storage.addDefaultProfile('2');
  });

  after(function () {
    server.shutdown();
  });

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<PageCol::ProfileList />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the profile name and a link when the profile exists', async function () {
    this.set('value', [profile1._id, profile2._id]);

    await render(hbs`<PageCol::ProfileList @value={{this.value}} />`);
    expect(this.element.textContent.trim()).to.contain('mr Bogdan Szabo');
    expect(this.element.querySelector('a')).to.have.attribute(
      'href',
      '/browse/profiles/1'
    );
  });
});
