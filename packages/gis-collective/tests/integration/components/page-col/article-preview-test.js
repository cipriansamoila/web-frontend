import { expect } from 'chai';
import { describe, it, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';

describe('Integration | Component | page-col/article-preview', function () {
  setupRenderingTest();
  let server;

  before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle('1');

    const editableArticle = server.testData.create.article('2');
    editableArticle.canEdit = true;
    server.testData.storage.addArticle(editableArticle);
  });

  after(function () {
    server.shutdown();
  });

  it('renders nothing when there is no value', async function () {
    await render(hbs`<PageCol::ArticlePreview />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the title and the first paragraph', async function () {
    this.set('value', {
      type: 'article',
      data: {
        id: '1',
      },
    });

    await render(hbs`<PageCol::ArticlePreview @value={{this.value}} />`);

    expect(this.element.querySelector('a')).not.to.exist;
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'some title'
    );
    expect(
      this.element.querySelector('.article-preview').textContent.trim()
    ).to.contain('some content');
  });

  it('renders the edit button when the article is editable', async function () {
    this.set('value', {
      type: 'article',
      data: {
        id: '2',
      },
    });

    await render(hbs`<PageCol::ArticlePreview @value={{this.value}} />`);
    expect(this.element.querySelector('a')).to.exist;
  });
});
