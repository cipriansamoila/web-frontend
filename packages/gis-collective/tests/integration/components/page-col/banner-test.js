import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';

describe('Integration | Component | page-col/banner', function () {
  setupRenderingTest();
  let server;
  let picture;

  beforeEach(function () {
    server = new TestServer();
    picture = server.testData.storage.addDefaultPicture('1');
  });

  it('renders nothing when the value is not set', async function () {
    await render(hbs`<PageCol::Banner />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the texts when they are set and the pictures are not', async function () {
    this.set('value', {
      data: {
        heading: '1',
        subHeading: '2',
        description: {
          blocks: [{ type: 'paragraph', data: { text: 'test article' } }],
        },
        buttonLabel: '3',
        buttonLink: 'http://giscollective.com',
      },
    });

    await render(hbs`<PageCol::Banner @value={{this.value}}/>`);

    expect(this.element.querySelector('h1').textContent.trim()).to.equal('1');
    expect(this.element.querySelector('h1')).to.have.attribute('style', null);
    expect(this.element.querySelector('h2').textContent.trim()).to.equal('2');
    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'test article'
    );
    expect(this.element.querySelector('.btn').textContent.trim()).to.equal('3');
    expect(this.element.querySelector('.btn')).to.have.attribute(
      'href',
      'http://giscollective.com'
    );
  });

  it('renders the heading picture when is set', async function () {
    this.set('value', {
      data: {
        heading: '1',
        headingPicture: picture._id,
        subHeading: '2',
        description: {
          blocks: [{ type: 'paragraph', data: { text: 'test article' } }],
        },
        buttonLabel: '3',
        buttonLink: 'http://giscollective.com',
      },
    });

    await render(hbs`<PageCol::Banner @value={{this.value}}/>`);
    await waitFor('.has-heading-picture');

    expect(this.element.querySelector('h1').textContent.trim()).to.equal('1');
    expect(this.element.querySelector('h1')).to.have.attribute(
      'style',
      'background-image: url(https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture)'
    );
  });

  it('renders the side picture when is set', async function () {
    this.set('value', {
      data: {
        heading: '1',
        sidePicture: picture._id,
        sidePictureMobile: 'after',
        subHeading: '2',
        description: {
          blocks: [{ type: 'paragraph', data: { text: 'test article' } }],
        },
        buttonLabel: '3',
        buttonLink: 'http://giscollective.com',
      },
    });

    await render(hbs`<PageCol::Banner @value={{this.value}}/>`);
    await waitFor('.has-side-picture');

    expect(this.element.querySelector('.side-picture')).to.have.attribute(
      'style',
      'background-image: url(https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture)'
    );
  });

  it('renders an internal link when the path starts with /', async function () {
    this.set('value', {
      data: {
        title: '1',
        description: '2',
        buttonLabel: '3',
        buttonLink: '/event/page',
      },
    });

    await render(hbs`<PageCol::Banner @value={{this.value}}/>`);
    expect(this.element.querySelector('.btn')).to.have.attribute(
      'href',
      '/event/page'
    );
  });
});
