import { expect } from 'chai';
import { describe, it, before, after, beforeEach } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../../helpers/test-server';

describe('Integration | Component | page-col/map-card-list', function () {
  setupRenderingTest();
  let server;
  let map;

  before(function () {
    server = new TestServer();
    map = server.testData.storage.addDefaultMap('1');
  });

  after(function () {
    server.shutdown();
  });

  beforeEach(function () {
    this.owner.lookup('service:notifications').ask = async () => {
      return 'yes';
    };
  });

  it('renders the preloaded campaigns when they are set', async function () {
    server.history = [];
    this.set('value', {
      data: { query: { team: '1' } },
      modelKey: 'maps_team_1',
    });

    this.set('model', {
      maps_team_1: { columns: [[map]] },
    });

    await render(
      hbs`<PageCol::MapCardList @model={{this.model}} @value={{this.value}} />`
    );

    expect(this.element.querySelectorAll('.map-card')).to.have.length(1);
    expect(server.history).to.deep.equal([]);
  });
});
