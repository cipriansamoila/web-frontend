import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | page-col/blocks', function () {
  setupRenderingTest();

  it('renders', async function () {
    await render(hbs`<PageCol::Blocks />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the blocks', async function () {
    this.set('value', {
      blocks: [
        {
          type: 'header',
          data: {
            text: 'Head',
            level: 1,
          },
        },
        {
          type: 'paragraph',
          data: {
            text: 'Hey. Meet the new Editor.',
          },
        },
      ],
    });

    await render(hbs`<PageCol::Blocks @value={{this.value}} />`);
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'Head'
    );
    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'Hey. Meet the new Editor.'
    );
  });

  it('renders the blocks wrapped in a col data', async function () {
    this.set('value', {
      data: {
        blocks: [
          {
            type: 'header',
            data: {
              text: 'Head',
              level: 1,
            },
          },
          {
            type: 'paragraph',
            data: {
              text: 'Hey. Meet the new Editor.',
            },
          },
        ],
      },
    });

    await render(hbs`<PageCol::Blocks @value={{this.value}} />`);
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'Head'
    );
    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'Hey. Meet the new Editor.'
    );
    expect(this.element.querySelector('.page-col-blocks')).to.have.attribute(
      'class',
      'page-col-blocks'
    );
  });

  it('renders and applies the options', async function () {
    this.set('value', {
      data: {
        alignment: 'center',
        lineHeight: 'sm',
        font: 'monospace',
        blocks: [
          {
            type: 'header',
            data: {
              text: 'Head',
              level: 1,
            },
          },
          {
            type: 'paragraph',
            data: {
              text: 'Hey. Meet the new Editor.',
            },
          },
        ],
      },
    });

    await render(hbs`<PageCol::Blocks @value={{this.value}} />`);
    expect(this.element.querySelector('.page-col-blocks')).to.have.attribute(
      'class',
      'page-col-blocks text-center lh-sm font-monospace'
    );
  });

  it('renders MD string as blocks', async function () {
    this.set('value', '# Head\n\nHey. Meet the new Editor.');

    await render(hbs`<PageCol::Blocks @value={{this.value}} />`);
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'Head'
    );
    expect(this.element.querySelector('p').textContent.trim()).to.equal(
      'Hey. Meet the new Editor.'
    );
  });

  it('renders a plain string when preview is true', async function () {
    this.set('value', '# Head\n\nHey. Meet the new Editor.');

    await render(
      hbs`<PageCol::Blocks @value={{this.value}} @preview={{true}}/>`
    );
    expect(this.element.querySelector('h1')).not.exist;
    expect(this.element.querySelector('p')).not.exist;
    expect(this.element.textContent.trim()).to.equal(
      'Head\n\nHey. Meet the new Editor.'
    );
  });
});
