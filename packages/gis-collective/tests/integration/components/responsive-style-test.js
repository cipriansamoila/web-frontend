import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | responsive-style', function () {
  setupRenderingTest();

  it('renders', async function () {
    this.set('value', {
      sm: {
        'font-size': '1px',
      },
      md: {
        'font-size': '2px',
      },
      lg: {
        'font-size': '3px',
      },
    });

    await render(hbs`<ResponsiveStyle @selector="#id" @style={{this.value}}/>`);
    expect(this.element.textContent.trim()).to.contain('#id {font-size: 1px;}');
    expect(this.element.textContent.trim()).to.contain('#id {font-size: 2px;}');
    expect(this.element.textContent.trim()).to.contain('#id {font-size: 3px;}');
  });
});
