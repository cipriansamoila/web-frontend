import { expect } from 'chai';
import { describe, it, beforeEach, before, after } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, click, waitUntil } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';
import TestServer from '../../helpers/test-server';

describe('Integration | Component | layout-rows', function () {
  setupRenderingTest();
  let server;

  before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultArticle('1');
    server.testData.storage.addDefaultPicture('1');
    server.testData.storage.addDefaultArticle('2');
    server.testData.storage.addDefaultPicture('2');
  });

  after(function () {
    server.shutdown();
  });

  it('renders nothing when page and cols are not set', async function () {
    await render(hbs`<LayoutRows/>`);
    expect(this.element.textContent.trim()).to.equal('');
    expect(this.element.querySelector('.row')).not.to.exist;
    expect(this.element.querySelector('.col')).not.to.exist;
  });

  describe('when there is a layout with 2 cols', function () {
    beforeEach(function () {
      this.set('cols', [
        {
          col: 0,
          row: 0,
          type: 'article',
          data: {
            id: '1',
          },
        },
        {
          col: 1,
          row: 0,
          type: 'picture',
          data: {
            id: '2',
          },
        },
      ]);

      this.set('layout', [
        {
          options: [],
          cols: [
            {
              type: 'type',
              data: {},
              options: [],
            },
            {
              type: 'type',
              data: {},
              options: [],
            },
          ],
        },
      ]);
    });

    it('renders the layout frame', async function () {
      await render(hbs`<LayoutRows @layout={{this.layout}} @cols={{this.cols}} as | col |>
        <PageCol::View @value={{col.value}} />
      </LayoutRows>`);

      expect(this.element.querySelector('.row')).to.exist;
      expect(this.element.querySelectorAll('.row .col')).to.have.length(2);
    });

    it('renders nothing wen the rows are removed', async function () {
      await render(hbs`<LayoutRows @layout={{this.layout}} @cols={{this.cols}} as | col |>
        <PageCol::View @value={{col.value}} />
      </LayoutRows>`);

      this.set('layout', []);
      expect(this.element.querySelector('.row')).not.to.exist;
    });

    it('renders nothing wen the cols are removed', async function () {
      await render(hbs`<LayoutRows @layout={{this.layout}} @cols={{this.cols}} as | col |>
        <PageCol::View @value={{col.value}} />
      </LayoutRows>`);

      this.set('layout', [
        {
          options: [],
          cols: [],
        },
      ]);
      expect(this.element.querySelector('.col')).not.to.exist;
    });

    it('renders another row when is added', async function () {
      await render(hbs`<LayoutRows @layout={{this.layout}} @cols={{this.cols}} as | col |>
        <PageCol::View @value={{col.value}} />
      </LayoutRows>`);

      this.layout.addObject({ cols: [] });

      await waitUntil(() => this.element.querySelectorAll('.row').length > 1);
      expect(this.element.querySelectorAll('.row')).to.have.length(2);
      expect(this.element.querySelectorAll('.col')).to.have.length(2);
    });

    it('renders the column content', async function () {
      await render(hbs`<LayoutRows @layout={{this.layout}} @cols={{this.cols}} as | col |>
        {{col.data.id}}
      </LayoutRows>`);

      const cols = this.element.querySelectorAll('.row .col');

      expect(cols[0].textContent).to.contain('1');
      expect(cols[1].textContent).to.contain('2');
    });

    it('allows selecting cols when selectable is true', async function () {
      let value;
      this.set('selectCol', (v) => {
        value = v;
      });

      await render(hbs`<LayoutRows @layout={{this.layout}} @cols={{this.cols}} @selectable={{true}} @onSelect={{this.selectCol}} as | col |>
        {{col.data.id}}
      </LayoutRows>`);

      await click('.col');

      expect(value).to.deep.equal({
        col: 0,
        row: 0,
        type: 'article',
        data: { id: '1' },
      });
    });
  });
});
