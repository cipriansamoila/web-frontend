import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render, waitFor } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | icon', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<Icon />`);

    expect(this.element.querySelector(".fa-dot-circle")).not.to.exist;
    expect(this.element.querySelector("img")).not.to.exist;
    expect(this.element.querySelector(".loading-icon")).not.to.exist;
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders the icon when it is set', async function() {
    this.set("icon", {
      name: "test icon",
      localName: "test icon",
      image: { value: "image", useParent: false }
    });

    await render(hbs`<Icon @src={{this.icon}} />`);

    expect(this.element.querySelector(".fa-dot-circle")).not.to.exist;
    expect(this.element.querySelector(".loading-icon")).not.to.exist;

    expect(this.element.querySelector("img")).to.exist;
    expect(this.element.querySelector("img")).to.have.attribute("src", "image");
    expect(this.element.querySelector("img")).to.have.attribute("alt", "test icon");
  });

  it('renders a question circle fa icon when the image is not set', async function() {
    this.set("icon", {
      name: "test icon"
    });

    await render(hbs`<Icon @src={{this.icon}} />`);

    expect(this.element.querySelector("img")).not.to.exist;
    expect(this.element.querySelector(".loading-icon")).not.to.exist;
    expect(this.element.querySelector(".fa-question-circle")).to.exist;
  });

  it('renders an animated circle if it is a promise', async function() {
    this.set("icon", {
      then() {}
    });

    await render(hbs`<Icon @src={{this.icon}} />`);

    expect(this.element.querySelector("img")).not.to.exist;
    expect(this.element.querySelector(".fa-question-circle")).not.to.exist;
    expect(this.element.querySelector(".loading-icon")).to.exist;
  });

  it('replaces the loading container with the icon image when the promise is resolved', async function() {
    this.set("icon", new Promise((resolve) => {
      setTimeout(() => resolve({
        name: "name",
        image: { value: "image", useParent: false } }), 200);
    }));

    await render(hbs`<Icon @src={{this.icon}} />`);
    await waitFor(".loading-icon");
    await waitFor("img");

    expect(this.element.querySelector(".fa-question-circle")).not.to.exist;
    expect(this.element.querySelector(".loading-icon")).not.to.exist;
  });

  it('replaces the loading container with the missing icon when the promise is rejected', async function() {
    let iconReject;

    this.set("icon", {
      then(resolve, reject) { iconReject = reject; }
    });

    await render(hbs`<Icon @src={{this.icon}} />`);
    await waitFor(".loading-icon");

    iconReject();
    await waitFor(".fa-question-circle");

    expect(this.element.querySelector("img")).not.to.exist;
    expect(this.element.querySelector(".loading-icon")).not.to.exist;
  });

  it('does not resolve the promise when the waiting attribute is true', async function() {
    this.set("icon", new Promise((resolve) => { resolve({
      image: { value: "", useParent: false } }) }));

    await render(hbs`<Icon @src={{this.icon}} @waiting={{true}}/>`);
    await waitFor(".loading-icon");

    expect(this.element.querySelector("img")).not.to.exist;
    expect(this.element.querySelector(".fa-question-circle")).not.to.exist;
    expect(this.element.querySelector(".loading-icon")).to.exist;
  });

  it('is shown as loading when the image is missing and waiting is true', async function() {
    this.set("icon", {});

    await render(hbs`<Icon @src={{this.icon}} @waiting={{true}}/>`);
    await waitFor(".loading-icon");

    expect(this.element.querySelector("img")).not.to.exist;
    expect(this.element.querySelector(".fa-question-circle")).not.to.exist;
    expect(this.element.querySelector(".loading-icon")).to.exist;
  });
});
