import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupRenderingTest } from 'ember-mocha';
import { render } from '@ember/test-helpers';
import { hbs } from 'ember-cli-htmlbars';

describe('Integration | Component | card-cover', function() {
  setupRenderingTest();

  it('renders', async function() {
    await render(hbs`<CardCover />`);
    expect(this.element.textContent.trim()).to.equal('');
  });

  it('renders a picture with size', async function() {
    this.set("pictures", [
      { get() { return "my-url" } }
    ]);

    await render(hbs`<CardCover @pictures={{this.pictures}} @size="md" />`);
    expect(this.element.querySelector(".cover-image")).to.have.attribute('style', 'background-image: url(my-url/md)');
  });

  it('renders a picture without size', async function() {
    this.set("pictures", [
      { get() { return "my-url" } }
    ]);

    await render(hbs`<CardCover @pictures={{this.pictures}} />`);
    expect(this.element.querySelector(".cover-image")).to.have.attribute('style', 'background-image: url(my-url)');
  });

  it('shows the fallback hash when is set and no picture exists', async function() {
    await render(hbs`<CardCover @pictures={{this.pictures}} @fallbackHash="hash" />`);
    expect(this.element.querySelector(".cover-hash")).to.have.attribute('style', 'hash');
  });

  it('shows the first picture hash when is set', async function() {
    this.set("pictures", [{
        get(name) { return name === "hashBg" ? "picture-hash" : "my-url" }
      }
    ]);

    await render(hbs`<CardCover @pictures={{this.pictures}} @pictures={{this.pictures}} @fallbackHash="hash" />`);
    expect(this.element.querySelector(".cover-hash")).to.have.attribute('style', 'picture-hash');
  });
});
