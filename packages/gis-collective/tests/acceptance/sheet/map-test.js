import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL } from '@ember/test-helpers';
import TestServer from '../../helpers/test-server';
import waitUntil from '@ember/test-helpers/wait-until';
import {
  fillIn,
  click,
  doubleClick,
  waitFor,
  typeIn,
  triggerEvent,
} from '@ember/test-helpers';
import { selectChoose, selectSearch } from 'ember-power-select/test-support';
import PageElements from '../../helpers/page-elements';
import { authenticateSession } from 'ember-simple-auth/test-support';

describe('Acceptance | sheet/map', function () {
  setupApplicationTest();
  let server;
  let map;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultPicture();

    map = server.testData.storage.addDefaultMap();
    authenticateSession();
  });

  afterEach(function () {
    server.shutdown();
  });

  describe('when there are 2 features without attributes', function () {
    beforeEach(async function () {
      let feature2 = server.testData.storage.addDefaultFeature('1');
      feature2.pictures = [];

      server.testData.storage.addDefaultProfile('5b8a59caef739394031a3f67');

      const feature = server.testData.create.feature('2');
      feature.description = {
        blocks: [
          {
            type: 'paragraph',
            data: {
              text: 'Head',
              level: 1,
            },
          },
        ],
      };
      feature.pictures = [];

      server.testData.storage.addFeature(feature);

      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            publicSites: 2,
            totalFeatures: 2,
            totalContributors: 2,
          }),
        ];
      });

      await visit(`/sheet/map/${map._id}`);

      await waitUntil(
        () => this.element.querySelector('.value-id').textContent.trim() == '1'
      );
    });

    it('only shows the general tab', async function () {
      const items = this.element.querySelectorAll('.sheet-tabs .nav-item');

      expect(items).to.have.length(1);
      expect(items[0].textContent.trim()).to.equal('General');
    });

    it('renders the generic sheet components', async function () {
      expect(server.history).to.contain(
        'GET /mock-server/features?limit=10&map=5ca89e37ef1f7e010007f54c&skip=0&sortBy=name&sortOrder=asc'
      );
      expect(currentURL()).to.equal(`/sheet/map/${map._id}`);
      expect(
        this.element.querySelector('.sheet-title').textContent.trim()
      ).to.equal(map.name);
      expect(this.element.querySelector('.sheet-frame')).to.exist;
    });

    it('can change the sort', async function () {
      const cols = this.element.querySelectorAll('th');

      await click(cols[3].querySelector('.table-head'));

      await waitUntil(
        () =>
          server.history.filter(
            (a) => a.indexOf('GET /mock-server/features') == 0
          ).length == 2
      );

      expect(server.history).to.contain(
        'GET /mock-server/features?limit=10&map=5ca89e37ef1f7e010007f54c&skip=0&sortBy=position&sortOrder=desc'
      );
    });

    it('renders the two feature names', async function () {
      const elements = this.element.querySelectorAll('.value-id');

      await waitUntil(() => elements[0].textContent.trim() != '');

      const featuresRequests = server.history.filter(
        (a) => a.indexOf('/features') != -1
      );

      expect(elements[0].textContent.trim()).to.eql('1');
      expect(elements[1].textContent.trim()).to.eql('2');
      expect(server.history).to.contain(
        'GET /mock-server/maps/5ca89e37ef1f7e010007f54c/meta'
      );
      expect(featuresRequests[0]).to.contain('limit=10');
      expect(featuresRequests[0]).to.contain('skip=0');
      expect(featuresRequests[0]).to.contain('map=5ca89e37ef1f7e010007f54c');
    });

    it('renders all the feature fields', async function () {
      const cols = this.element.querySelectorAll('th');

      expect(cols[0].textContent.trim()).to.eql('id');
      expect(cols[1].textContent.trim()).to.eql('name');
      expect(cols[2].textContent.trim()).to.eql('description');
      expect(cols[3].textContent.trim()).to.eql('position');
      expect(cols[4].textContent.trim()).to.eql('maps');
      expect(cols[5].textContent.trim()).to.eql('info.createdOn');
      expect(cols[6].textContent.trim()).to.eql('info.lastChangeOn');
      expect(cols[7].textContent.trim()).to.eql('info.changeIndex');
      expect(cols[8].textContent.trim()).to.eql('info.originalAuthor');
      expect(cols[9].textContent.trim()).to.eql('pictures');
      expect(cols[10].textContent.trim()).to.eql('sounds');
      expect(cols[11].textContent.trim()).to.eql('icons');
      expect(cols[12].textContent.trim()).to.eql('visibility');
    });

    it('renders the description', async function () {
      const elements = this.element.querySelectorAll('.value-description');
      const idElements = this.element.querySelectorAll('.value-id');

      await waitUntil(() => idElements[0].textContent.trim() != '');

      expect(elements[0].textContent.trim()).to.eql('some description');
      expect(elements[1].textContent.trim()).to.eql('Head');
    });

    describe('updating values', function () {
      let receivedSite;

      beforeEach(function () {
        receivedSite = undefined;

        server.put('/mock-server/features/:id', (request) => {
          receivedSite = JSON.parse(request.requestBody);

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedSite),
          ];
        });

        server.post('/mock-server/pictures', (request) => {
          const pictureRequest = JSON.parse(request.requestBody);
          pictureRequest.picture['_id'] = '5cc8dc1038e882010061545a';

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(pictureRequest),
          ];
        });

        server.post('/mock-server/sounds', (request) => {
          const soundRequest = JSON.parse(request.requestBody);
          soundRequest.sound['_id'] = '5cc8dc1038e882010061545a';

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(soundRequest),
          ];
        });

        server.testData.storage.addDefaultIcon('2');

        let map1 = server.testData.create.map('1');
        let map2 = server.testData.create.map('2');

        server.testData.storage.addMap(map1);
        server.testData.storage.addMap(map2);

        let user2 = server.testData.storage.addDefaultUser(false, '2');
        user2.username = 'gedaiu2';
        server.testData.storage.addDefaultProfile('2');
      });

      it('can change the feature name', async function () {
        let nameCells = this.element.querySelectorAll('.value-name');

        await click(nameCells[0]);
        await fillIn(nameCells[0].querySelector('.td-container'), 'new name');
        await click(nameCells[1]);

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.name).to.equal('new name');
      });

      it('can change the feature description', async function () {
        let descriptionCells =
          this.element.querySelectorAll('.value-description');
        await doubleClick(descriptionCells[0]);
        await waitFor('.editor-is-ready', { timeout: 3000 });

        await fillIn(
          this.element.querySelector('.ce-paragraph'),
          'new description content'
        );

        await waitUntil(
          () =>
            !this.element
              .querySelector('.btn-set')
              .attributes.getNamedItem('disabled')
        );

        await click('.btn-set');

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.name).to.equal(
          'Nomadisch Grün - Local Urban Food'
        );
        expect(receivedSite.feature.description.blocks).to.deep.equal([
          {
            type: 'header',
            data: { text: 'Nomadisch Grün - Local Urban Food', level: 1 },
          },
          { type: 'paragraph', data: { text: 'new description content' } },
        ]);
      });

      it('can change the geometry', async function () {
        let cells = this.element.querySelectorAll('.value-position');
        await doubleClick(cells[0]);

        await waitUntil(() => this.element.querySelector('.map').olMap);
        await click('.btn-paste');

        const editor = this.element.querySelector('.input-geo-json').editor;
        editor.setMode('text');

        this.element.querySelector('.jsoneditor-text').value = '';
        await typeIn(
          '.jsoneditor-text',
          `{ "type": "Point", "coordinates": [11, 12] }`
        );

        await waitUntil(
          () =>
            !this.element
              .querySelector('.btn-set')
              .attributes.getNamedItem('disabled')
        );

        await click('.btn-set');

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.position).to.deep.equal({
          type: 'Point',
          coordinates: [11, 12],
        });
      });

      it('can change the map', async function () {
        let cells = this.element.querySelectorAll('.value-maps');
        await doubleClick(cells[0]);

        await click('.btn-add-item');

        await waitUntil(
          () =>
            !this.element
              .querySelector('.btn-set')
              .attributes.getNamedItem('disabled')
        );

        await click('.btn-set');

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.maps).to.deep.equal([
          '5ca89e37ef1f7e010007f54c',
          '2',
        ]);
      });

      it('can change the original author', async function () {
        let cells = this.element.querySelectorAll('.value-originalAuthor');
        await doubleClick(cells[0]);

        await selectSearch('.ember-power-select-trigger', 'gedaiu');
        selectChoose('.ember-power-select-trigger', 'gedaiu2');

        await waitUntil(
          () =>
            !this.element
              .querySelector('.btn-set')
              .attributes.getNamedItem('disabled')
        );

        await click('.btn-set');

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.info.originalAuthor).to.equal('2');
      });

      it('can change the pictures', async function () {
        let cells = this.element.querySelectorAll('.value-pictures');
        await doubleClick(cells[0]);

        await waitFor('.image-list-input');

        const blob = server.testData.create.pngBlob();
        await triggerEvent(".image-list-input[type='file']", 'change', {
          files: [blob],
        });

        await waitUntil(
          () =>
            !this.element
              .querySelector('.btn-set')
              .attributes.getNamedItem('disabled')
        );

        await click('.btn-set');

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.pictures).to.deep.equal([
          '5cc8dc1038e882010061545a',
        ]);
      });

      it('can change the sounds', async function () {
        let cells = this.element.querySelectorAll('.value-sounds');
        await doubleClick(cells[0]);

        await waitFor('.sound-list-input');

        const blob = server.testData.create.mp3Blob();
        await triggerEvent('.sound-list-input', 'change', { files: [blob] });

        await waitUntil(
          () =>
            !this.element
              .querySelector('.btn-set')
              .attributes.getNamedItem('disabled')
        );

        await click('.btn-set');

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.sounds).to.deep.equal([
          '5cc8dc1038e882010061545a',
        ]);
      });

      it('can change the icons', async function () {
        let cells = this.element.querySelectorAll('.value-icons');
        await doubleClick(cells[0]);

        await click('.btn-select-icons');
        await click('.btn-icon');

        await click('.btn-set');

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.icons).to.deep.equal(['2']);
      });

      it('can change the visibility', async function () {
        let cells = this.element.querySelectorAll('.value-visibility');
        await doubleClick(cells[0]);

        const select = this.element.querySelector(
          '.manage-editors-feature-visibility select'
        );

        select.value = '-1';
        await triggerEvent(select, 'change');

        await click('.btn-set');

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.visibility).to.deep.equal(-1);
      });
    });
  });

  describe('when there are 2 features with attributes', function () {
    let icon;

    beforeEach(async function () {
      icon = server.testData.storage.addDefaultIcon();
      icon.attributes = [
        {
          name: 'some text',
          localName: 'local attribute',
          type: 'short text',
        },
        {
          name: 'some integer',
          localName: 'local attribute',
          type: 'integer',
        },
        {
          name: 'some decimal',
          localName: 'local attribute',
          type: 'decimal',
        },
        {
          name: 'some bool value',
          localName: 'local attribute',
          type: 'boolean',
        },
        {
          name: 'some options value',
          localName: 'local attribute',
          type: 'options',
          options:
            'nature, birds, animals, human activity, music, sound pollution',
        },
        {
          name: 'some options with other value',
          localName: 'local attribute',
          type: 'options with other',
          options:
            'nature, birds, animals, human activity, music, sound pollution',
        },
      ];

      server.testData.storage.addDefaultFeature('1');
      server.testData.storage.addDefaultProfile('5b8a59caef739394031a3f67');

      const feature = server.testData.create.feature('2');
      server.testData.storage.addFeature(feature);
      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            publicSites: 2,
            totalFeatures: 2,
            totalContributors: 2,
          }),
        ];
      });

      await visit(`/sheet/map/${map._id}`);

      await waitUntil(
        () => this.element.querySelector('.value-id').textContent.trim() == '1'
      );
    });

    it('can change the tab', async function () {
      const tabs = this.element.querySelectorAll('.sheet-tabs .nav-link');
      await click(tabs[1]);

      expect(currentURL()).to.equal(`/sheet/map/${map._id}?icon=${icon._id}`);
      expect(tabs[1]).to.have.class('active');
    });

    describe('updating simple values', function () {
      let receivedSite;

      beforeEach(async function () {
        receivedSite = undefined;

        server.put('/mock-server/features/:id', (request) => {
          receivedSite = JSON.parse(request.requestBody);

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedSite),
          ];
        });

        const tabs = this.element.querySelectorAll('.sheet-tabs .nav-link');
        await click(tabs[1]);
        await PageElements.wait(1000);
      });

      it('can change the text attribute', async function () {
        let attributeCells = this.element.querySelectorAll('.value-some-text');

        await click(attributeCells[0]);

        await fillIn(
          attributeCells[0].querySelector('.td-container'),
          'new value'
        );
        await click(attributeCells[1]);

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.attributes).to.deep.equal({
          'Healthy Dining': { 'some text': 'new value' },
        });
      });

      it('can change the integer attribute', async function () {
        let attributeCells = this.element.querySelectorAll(
          '.value-some-integer'
        );

        await click(attributeCells[0]);

        await fillIn(attributeCells[0].querySelector('.td-container'), '123');
        await click(attributeCells[1]);

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.attributes).to.deep.equal({
          'Healthy Dining': { 'some integer': 123 },
        });
      });

      it('can change the decimal attribute', async function () {
        let attributeCells = this.element.querySelectorAll(
          '.value-some-decimal'
        );

        await click(attributeCells[0]);

        await fillIn(attributeCells[0].querySelector('.td-container'), '123.4');
        await click(attributeCells[1]);

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.attributes).to.deep.equal({
          'Healthy Dining': { 'some decimal': 123.4 },
        });
      });

      it('can change the boolean attribute', async function () {
        let attributeCells = this.element.querySelectorAll(
          '.value-some-bool-value'
        );

        await doubleClick(attributeCells[0]);

        const select = this.element.querySelector('select');

        select.value = 'false';
        await triggerEvent(select, 'change');
        await waitUntil(
          () =>
            !this.element
              .querySelector('.btn-set')
              .attributes.getNamedItem('disabled')
        );

        await click('.btn-set');

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.attributes).to.deep.equal({
          'Healthy Dining': { 'some bool value': false },
        });
      });

      it('can change the options attribute', async function () {
        let attributeCells = this.element.querySelectorAll(
          '.value-some-options-value'
        );

        await doubleClick(attributeCells[0]);

        const select = this.element.querySelector('select');

        select.value = 'music';
        await triggerEvent(select, 'change');
        await waitUntil(
          () =>
            !this.element
              .querySelector('.btn-set')
              .attributes.getNamedItem('disabled')
        );

        await click('.btn-set');

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.attributes).to.deep.equal({
          'Healthy Dining': { 'some options value': 'music' },
        });
      });

      it('can change the options with other attribute', async function () {
        let attributeCells = this.element.querySelectorAll(
          '.value-some-options-with-other-value'
        );

        await doubleClick(attributeCells[0]);

        const select = this.element.querySelector('select');

        select.value = 'music';
        await triggerEvent(select, 'change');
        await waitUntil(
          () =>
            !this.element
              .querySelector('.btn-set')
              .attributes.getNamedItem('disabled')
        );

        await click('.btn-set');

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.attributes).to.deep.equal({
          'Healthy Dining': { 'some options with other value': 'music' },
        });
      });
    });
  });

  describe('when there are 2 features with list attributes', function () {
    let icon;

    beforeEach(async function () {
      icon = server.testData.storage.addDefaultIcon();
      icon.allowMany = true;
      icon.attributes = [
        {
          name: 'some text',
          localName: 'local attribute',
          type: 'short text',
        },
        {
          name: 'some integer',
          localName: 'local attribute',
          type: 'integer',
        },
        {
          name: 'some decimal',
          localName: 'local attribute',
          type: 'decimal',
        },
        {
          name: 'some bool value',
          localName: 'local attribute',
          type: 'boolean',
        },
        {
          name: 'some options value',
          localName: 'local attribute',
          type: 'options',
          options:
            'nature, birds, animals, human activity, music, sound pollution',
        },
        {
          name: 'some options with other value',
          localName: 'local attribute',
          type: 'options with other',
          options:
            'nature, birds, animals, human activity, music, sound pollution',
        },
      ];

      server.testData.storage.addDefaultFeature('1');
      server.testData.storage.addDefaultProfile('5b8a59caef739394031a3f67');

      const feature = server.testData.create.feature('2');
      feature.attributes['Healthy Dining'] = [
        { 'some text': 'text 1' },
        { 'some text': 'text 2' },
      ];

      server.testData.storage.addFeature(feature);
      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            publicSites: 2,
            totalFeatures: 2,
            totalContributors: 2,
          }),
        ];
      });

      await visit(`/sheet/map/${map._id}`);

      await waitUntil(
        () => this.element.querySelector('.value-id').textContent.trim() == '1'
      );
    });

    it('shows the list values', async function () {
      const tabs = this.element.querySelectorAll('.sheet-tabs .nav-link');
      await click(tabs[1]);

      await waitUntil(
        () => this.element.querySelectorAll('.value-some-text li').length
      );

      const li = this.element.querySelectorAll('.value-some-text li');

      expect(li[0].textContent.trim()).to.equal('text 1');
      expect(li[1].textContent.trim()).to.equal('text 2');
    });

    describe('updating list values', function () {
      let receivedSite;

      beforeEach(async function () {
        receivedSite = undefined;

        server.put('/mock-server/features/:id', (request) => {
          receivedSite = JSON.parse(request.requestBody);

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedSite),
          ];
        });

        const tabs = this.element.querySelectorAll('.sheet-tabs .nav-link');
        await click(tabs[1]);
        await PageElements.wait(1000);
      });

      it('can change the text attribute', async function () {
        let attributeCells = this.element.querySelectorAll('.value-some-text');

        await doubleClick(attributeCells[0]);
        await click('.btn-add');

        await fillIn('.input-text-field', 'new value');

        await waitUntil(
          () =>
            !this.element
              .querySelector('.btn-set')
              .attributes.getNamedItem('disabled')
        );

        await click('.btn-set');

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.attributes).to.deep.equal({
          'Healthy Dining': [{ 'some text': 'new value' }],
        });
      });

      it('can delete a list item', async function () {
        let attributeCells = this.element.querySelectorAll('.value-some-text');

        await doubleClick(attributeCells[1]);
        await click('.btn-delete');

        await waitUntil(
          () =>
            !this.element
              .querySelector('.btn-set')
              .attributes.getNamedItem('disabled')
        );

        await click('.btn-set');

        await waitUntil(() => receivedSite);

        expect(receivedSite.feature.attributes).to.deep.equal({
          'Healthy Dining': [{ 'some text': 'text 2' }],
        });
      });
    });
  });
});
