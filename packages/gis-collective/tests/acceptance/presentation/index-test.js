import { describe, it, before, after } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL } from '@ember/test-helpers';
import TestServer from '../../helpers/test-server';

describe('Acceptance | presentation/index', function () {
  setupApplicationTest();
  let server;

  before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultPresentation(`presentation--name`);
    server.testData.storage.addDefaultPicture();
  });

  after(function () {
    server.shutdown();
  });

  it('can visit an existing presentation', async function () {
    await visit('/presentation/name');

    expect(this.element.querySelector('.error')).not.to.exist;
    expect(this.element.querySelector('.presentation-slider')).to.exist;
    expect(currentURL()).to.equal('/presentation/name');
  });
});
