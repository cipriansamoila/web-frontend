import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, click } from '@ember/test-helpers';
import TestServer from '../helpers/test-server';
import { authenticateSession } from 'ember-simple-auth/test-support';

describe('Acceptance | page', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
  });

  afterEach(function () {
    server.shutdown();
  });

  it('shows an error when the page is not found', async function () {
    await visit('/not-found');
    expect(currentURL()).to.equal('/not-found');
    expect(this.element.querySelector('.error')).to.exist;
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'Page Not Found'
    );
  });

  describe('when there is a page that matches the slug', function () {
    let page;

    beforeEach(function () {
      page = server.testData.create.page('section--page');
      server.testData.storage.addPage(page);
      server.testData.storage.addDefaultLayout();
      server.testData.storage.addDefaultArticle();
    });

    it('renders the page', async function () {
      await visit('/section/page');

      expect(this.element.querySelector('.error')).not.to.exist;
      expect(this.element.querySelector('.row')).to.exist;
      expect(this.element.querySelector('.col')).to.exist;
      expect(this.element.querySelector('h1').textContent.trim()).to.equal(
        'title'
      );
    });

    it('renders an empty page when there is no column', async function () {
      page.cols = [];

      await visit('/section/page');

      expect(
        this.element.querySelector('.page-col.empty').textContent.trim()
      ).to.equal('');
    });
  });

  describe('when the page can be edited and is private', function () {
    beforeEach(async function () {
      authenticateSession();
      server.testData.storage.addDefaultUser();

      const page = server.testData.create.page('section--page');
      page.visibility.isPublic = false;
      page.canEdit = true;

      server.testData.storage.addPage(page);
      server.testData.storage.addDefaultLayout();
      server.testData.storage.addDefaultArticle();

      await visit('/section/page');
    });

    it('renders an alert', async function () {
      expect(
        this.element.querySelector('.alert-page-private').textContent.trim()
      ).to.equal('This page is private.');
    });

    it('renders an edit button', async function () {
      expect(this.element.querySelector('.btn-edit-page')).to.exist;
      await click('.btn-edit-page');
      expect(currentURL()).to.equal('/manage/pages/section--page/content');
    });
  });
});
