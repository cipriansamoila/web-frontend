import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  waitUntil,
  fillIn,
  triggerEvent,
  blur,
} from '@ember/test-helpers';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';
import { authenticateSession } from 'ember-simple-auth/test-support';
import PageElements from '../../helpers/page-elements';

describe('Acceptance | campaigns/campaign', function () {
  setupApplicationTest();
  let server;
  let campaign;
  let picture;
  let geolocation;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultTeam('000000000000000000000001');
    server.testData.storage.addDefaultMap('5ca89e37ef1f7e010007f54c');
    picture = server.testData.storage.addDefaultPicture();

    const positionService = this.owner.lookup('service:position');
    geolocation = {
      watchPosition(callback) {
        callback({
          coords: {
            longitude: 1,
            latitude: 2,
            accuracy: 14,
            altitude: 15,
            altitudeAccuracy: 16,
          },
        });

        return 1;
      },
      clearWatch() {},
    };

    positionService.geolocation = geolocation;
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  describe('for a public campaign', function () {
    beforeEach(async function () {
      campaign = server.testData.storage.addDefaultCampaign();
      await visit(`/campaigns/${campaign._id}`);
    });

    it('can visit /campaigns/:id', async function () {
      await visit(`/campaigns/${campaign._id}`);
      expect(currentURL()).to.equal(`/campaigns/${campaign._id}`);
      expect(this.element.querySelector('h1')).not.to.equal(`Page Not Found`);
    });

    it('should display the campaign article', async function () {
      await visit(`/campaigns/${campaign._id}`);
      expect(this.element.querySelector('h1').textContent.trim()).to.equal(
        'Campaign 1'
      );
      expect(
        this.element.querySelector('.article p').textContent.trim()
      ).to.equal('Description text');
    });

    it('should display the campaign cover', async function () {
      await visit(`/campaigns/${campaign._id}`);
      expect(this.element.querySelector('.cover')).to.have.attribute(
        'style',
        `background-image: url('${picture.picture}/md')`
      );
    });

    it('should not display the unpublished icon', async function () {
      await visit(`/campaigns/${campaign._id}`);
      expect(this.element.querySelector('.cover')).to.have.attribute(
        'style',
        `background-image: url('${picture.picture}/md')`
      );
    });

    it('should contain all the browse links with the first site map', async function () {
      await visit(`/campaigns/${campaign._id}`);

      expect(PageElements.breadcrumbs()).to.deep.equal([
        'Campaigns',
        campaign.name,
      ]);
      expect(PageElements.breadcrumbLinks()).to.deep.equal(['/campaigns']);
    });
  });

  describe('for a public campaign that needs registration', function () {
    beforeEach(async function () {
      campaign = server.testData.create.campaign();
      campaign.options = {
        registrationMandatory: true,
      };
      server.testData.storage.addCampaign(campaign);

      await visit(`/campaigns/${campaign._id}`);
    });

    it('can visit /campaigns/:id', async function () {
      await visit(`/campaigns/${campaign._id}`);
      expect(currentURL()).to.equal(`/campaigns/${campaign._id}`);
      expect(this.element.querySelector('h1')).not.to.equal(`Page Not Found`);
    });

    it('should display the campaign article', async function () {
      await visit(`/campaigns/${campaign._id}`);
      expect(this.element.querySelector('h1').textContent.trim()).to.equal(
        'Campaign 1'
      );
      expect(
        this.element.querySelector('.article p').textContent.trim()
      ).to.equal('Description text');
      expect(this.element.querySelector('.cover')).to.have.attribute(
        'style',
        `background-image: url('${picture.picture}/md')`
      );
    });

    it('shows a message that the user needs to be registered', async function () {
      await visit(`/campaigns/${campaign._id}`);

      expect(this.element.querySelector('.alert-warning')).to.exist;
      expect(this.element.querySelector('.alert-link')).to.exist;
      expect(this.element.querySelector('.alert-link')).to.have.attribute(
        'href',
        '/login'
      );
    });
  });

  describe('for a private editable campaign', function () {
    let receivedCampaign;

    beforeEach(async () => {
      campaign = server.testData.create.campaign();
      campaign.canEdit = true;
      campaign.visibility.isPublic = false;

      server.testData.storage.addCampaign(campaign);
      authenticateSession();
      await visit(`/campaigns/${campaign._id}`);

      server.put(`/mock-server/campaigns/${campaign._id}`, (request) => {
        receivedCampaign = JSON.parse(request.requestBody);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedCampaign),
        ];
      });
    });

    it('should display the unpublished icon', async function () {
      expect(this.element.querySelector('.icon-not-published')).to.exist;
    });

    it('should allow editing the campaign', async function () {
      await click('.subtitle-container .dropdown-item-edit');
      expect(currentURL()).to.equal(`/manage/campaigns/${campaign._id}`);
    });

    it('should allow publishing the campaign', async function () {
      await click('.subtitle-container .btn-manage-publish');

      await waitUntil(() => receivedCampaign != null, { timeout: 3000 });
      expect(receivedCampaign.campaign.visibility.isPublic).to.equal(true);
    });
  });

  describe('for a public editable campaign', function () {
    let receivedCampaign;

    beforeEach(async () => {
      campaign = server.testData.create.campaign();
      campaign.canEdit = true;
      campaign.visibility.isPublic = true;

      server.testData.storage.addCampaign(campaign);
      authenticateSession();
      await visit(`/campaigns/${campaign._id}`);

      server.put(`/mock-server/campaigns/${campaign._id}`, (request) => {
        receivedCampaign = JSON.parse(request.requestBody);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedCampaign),
        ];
      });
    });

    it('should allow unpublishing the campaign', async function () {
      await click('.subtitle-container .btn-manage-unpublish');

      await waitUntil(() => receivedCampaign != null, { timeout: 3000 });
      expect(receivedCampaign.campaign.visibility.isPublic).to.equal(false);
    });
  });

  describe('the campaign questions', function () {
    let icon1;
    let icon2;
    let icon3;
    let receivedAnswer;
    let receivedPictures;

    beforeEach(async function () {
      icon1 = server.testData.create.icon('000000000000000000000001');
      icon1.name = 'icon 1';
      icon1.localName = 'local icon 1';
      icon1.attributes = [
        {
          name: 'attribute1',
          help: 'some help message',
          displayName: 'local attribute1',
          type: 'short text',
        },
      ];
      server.testData.storage.addIcon(icon1);

      icon2 = server.testData.create.icon('000000000000000000000002');
      icon2.name = 'icon 2';
      icon2.localName = 'local icon 2';
      icon2.allowMany = true;
      icon2.attributes = [
        {
          name: 'attribute1',
          displayName: 'local attribute1',
          type: 'short text',
        },
      ];
      server.testData.storage.addIcon(icon2);

      icon3 = server.testData.create.icon('000000000000000000000003');
      icon3.name = 'icon 3';
      icon3.localName = 'local icon 3';
      icon3.allowMany = true;
      icon3.attributes = [
        {
          name: 'attribute1',
          displayName: 'local attribute1',
          type: 'short text',
        },
      ];
      server.testData.storage.addIcon(icon3);

      campaign = server.testData.create.campaign();
      campaign.icons = [icon1._id, icon2._id];
      campaign.optionalIcons = [icon3._id];

      server.post(`/mock-server/campaignanswers`, (request) => {
        receivedAnswer = JSON.parse(request.requestBody);
        receivedAnswer.campaignAnswer._id = 'some-answer';

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedAnswer),
        ];
      });

      receivedPictures = [];
      server.post(
        '/mock-server/pictures',
        (request) => {
          const pictureRequest = JSON.parse(request.requestBody);
          pictureRequest.picture['_id'] = receivedPictures.length + 1;
          receivedPictures.push(pictureRequest);

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(pictureRequest),
          ];
        },
        false
      );
    });

    describe('a campaign with custom labels', function () {
      beforeEach(async function () {
        campaign.options = {
          showNameQuestion: true,
          nameLabel: 'a custom name question',

          showDescriptionQuestion: true,
          descriptionLabel: 'a custom description question',

          iconsLabel: 'a custom icon question',
        };

        server.testData.storage.addCampaign(campaign);

        await visit(`/campaigns/${campaign._id}`);
        await PageElements.wait(300);
      });

      it('renders a custom icon header', function () {
        const header = this.element.querySelector(
          '.optional-icons-group .card-header'
        );
        expect(header.textContent.trim()).to.equal('a custom icon question');
      });

      it('renders the about section', function () {
        const header = this.element.querySelector('.group-about .card-header');
        expect(header.textContent.trim()).to.equal('about');
      });

      it('can set and submit the about questions', async function () {
        await fillIn('.group-about input', 'value 1');

        const nameInput = this.element.querySelector(
          '.group-about .attribute-value input'
        );
        await fillIn(nameInput, 'name');
        await blur(nameInput);

        const descriptionInput = this.element.querySelector(
          '.group-about .attribute-value div.CodeMirror'
        );
        descriptionInput.CodeMirror.setValue('description');

        await click('.btn-submit');

        await waitUntil(() => receivedAnswer);

        expect(receivedAnswer.campaignAnswer.attributes['about']).to.deep.equal(
          {
            name: 'name',
            description: 'description',
          }
        );

        expect(currentURL()).to.equal(`/campaigns/${campaign._id}/success`);
      });
    });

    describe('a campaign with no custom labels', () => {
      beforeEach(async function () {
        server.testData.storage.addCampaign(campaign);

        await visit(`/campaigns/${campaign._id}`);
        await PageElements.wait(300);
      });

      it('renders the about section', function () {
        expect(this.element.querySelector('.group-about .card-header')).not.to
          .exist;
      });

      it('should show all the icon questions', async function () {
        let currentUrl = currentURL();
        await fillIn('.card-icon-1 input', 'value 1');
        await fillIn('.card-icon-2 input', 'value 2');

        await click('.btn-submit');

        await waitUntil(() => receivedAnswer && currentURL() != currentUrl);

        expect(receivedAnswer).to.deep.equal({
          campaignAnswer: {
            position: { type: 'Point', coordinates: [0, 0] },
            attributes: {
              'position details': {
                altitude: 0,
                accuracy: 1000,
                altitudeAccuracy: 1000,
                type: 'gps',
              },
              'icon 1': { attribute1: 'value 1' },
              'icon 2': [{ attribute1: 'value 2' }],
            },
            campaign: '5ca78aa160780601008f6aaa',
            icons: ['000000000000000000000001', '000000000000000000000002'],
            _id: 'some-answer',
          },
        });

        expect(currentURL()).to.equal(`/campaigns/${campaign._id}/success`);
      });

      it('should allow adding a second icon instance', async function () {
        let currentUrl = currentURL();

        await click('.btn-add-icon-2');

        const inputs = this.element.querySelectorAll('.card-icon-2 input');
        await fillIn('.card-icon-1 input', 'value 1');
        await fillIn(inputs[0], 'value 2');
        await fillIn(inputs[1], 'value 3');

        await click('.btn-submit');

        await waitUntil(() => receivedAnswer && currentURL() != currentUrl);

        expect(receivedAnswer).to.deep.equal({
          campaignAnswer: {
            position: { type: 'Point', coordinates: [0, 0] },
            attributes: {
              'position details': {
                altitude: 0,
                accuracy: 1000,
                altitudeAccuracy: 1000,
                type: 'gps',
              },
              'icon 1': { attribute1: 'value 1' },
              'icon 2': [{ attribute1: 'value 2' }, { attribute1: 'value 3' }],
            },
            campaign: '5ca78aa160780601008f6aaa',
            icons: ['000000000000000000000001', '000000000000000000000002'],
            _id: 'some-answer',
          },
        });

        expect(currentURL()).to.equal(`/campaigns/${campaign._id}/success`);
      });

      it('should allow selecting an optional icon', async function () {
        let currentUrl = currentURL();

        const header = this.element.querySelector(
          '.optional-icons-group .card-header'
        );
        expect(header.textContent.trim()).to.equal('icons');

        await click('.input-icons-list-toggle .icon-element');
        await click('.btn-submit');

        await waitUntil(() => receivedAnswer && currentURL() != currentUrl);

        expect(receivedAnswer).to.deep.equal({
          campaignAnswer: {
            position: { type: 'Point', coordinates: [0, 0] },
            attributes: {
              'position details': {
                altitude: 0,
                accuracy: 1000,
                altitudeAccuracy: 1000,
                type: 'gps',
              },
              'icon 1': { attribute1: null },
              'icon 2': [{ attribute1: null }],
              'icon 3': [{ attribute1: null }],
            },
            campaign: '5ca78aa160780601008f6aaa',
            icons: [
              '000000000000000000000001',
              '000000000000000000000002',
              '000000000000000000000003',
            ],
            _id: 'some-answer',
          },
        });
      });

      it('should not add an icon and its attributes when selecting an optional icon twice', async function () {
        let currentUrl = currentURL();

        await click('.input-icons-list-toggle .icon-element');
        await click('.input-icons-list-toggle .icon-element');
        await click('.btn-submit');

        await waitUntil(() => receivedAnswer && currentURL() != currentUrl);

        expect(receivedAnswer).to.deep.equal({
          campaignAnswer: {
            position: { type: 'Point', coordinates: [0, 0] },
            attributes: {
              'position details': {
                type: 'gps',
                altitude: 0,
                accuracy: 1000,
                altitudeAccuracy: 1000,
              },
              'icon 1': { attribute1: null },
              'icon 2': [{ attribute1: null }],
            },
            campaign: '5ca78aa160780601008f6aaa',
            icons: ['000000000000000000000001', '000000000000000000000002'],
            _id: 'some-answer',
          },
        });
      });

      it('deleting a second icon instance should delete the attributes', async function () {
        let currentUrl = currentURL();

        await click('.btn-add-icon-2');

        const inputs = this.element.querySelectorAll('.card-icon-2 input');
        const containers = this.element.querySelectorAll(
          '.group-container.group-icon-2'
        );

        await fillIn('.card-icon-1 input', 'value 1');
        await fillIn(inputs[0], 'value 2');
        await fillIn(inputs[1], 'value 3');

        await click(containers[1].querySelector('.close'));

        await click('.btn-submit');

        await waitUntil(() => receivedAnswer && currentURL() != currentUrl);

        expect(receivedAnswer).to.deep.equal({
          campaignAnswer: {
            position: { type: 'Point', coordinates: [0, 0] },
            attributes: {
              'position details': {
                type: 'gps',
                altitude: 0,
                accuracy: 1000,
                altitudeAccuracy: 1000,
              },
              'icon 1': { attribute1: 'value 1' },
              'icon 2': [{ attribute1: 'value 2' }],
            },
            campaign: '5ca78aa160780601008f6aaa',
            icons: ['000000000000000000000001', '000000000000000000000002'],
            _id: 'some-answer',
          },
        });

        expect(currentURL()).to.equal(`/campaigns/${campaign._id}/success`);
      });

      it('should allow adding 2 pictures', async function () {
        let currentUrl = currentURL();

        const blob = server.testData.create.pngBlob();

        await triggerEvent(
          ".card-pictures input.image-list-input[type='file']",
          'change',
          { files: [blob] }
        );

        await waitUntil(
          () => this.element.querySelectorAll('.image-list img').length > 0
        );

        await triggerEvent(
          ".card-pictures input.image-list-input[type='file']",
          'change',
          { files: [blob] }
        );

        await waitUntil(
          () => this.element.querySelectorAll('.image-list img').length > 1
        );

        await click('.btn-submit');

        await waitUntil(() => receivedAnswer && currentURL() != currentUrl);

        expect(receivedAnswer).to.deep.equal({
          campaignAnswer: {
            position: { type: 'Point', coordinates: [0, 0] },
            attributes: {
              'position details': {
                type: 'gps',
                altitude: 0,
                accuracy: 1000,
                altitudeAccuracy: 1000,
              },
              'icon 1': { attribute1: null },
              'icon 2': [{ attribute1: null }],
            },
            campaign: '5ca78aa160780601008f6aaa',
            pictures: ['1', '2'],
            icons: ['000000000000000000000001', '000000000000000000000002'],
            _id: 'some-answer',
          },
        });

        expect(receivedPictures.length).to.equal(2);
        expect(currentURL()).to.equal(`/campaigns/${campaign._id}/success`);
      });

      it('should allow changing the position', async function () {
        let currentUrl = currentURL();

        await waitUntil(() =>
          this.element.querySelector('.group-position-details')
        );

        await click('.group-position-details .btn-expand');
        await click('.group-position-details .btn-change');

        const options = this.element.querySelectorAll(
          '.group-position-details .list-group .list-group-item'
        );

        await click(options[1]);

        const inputs = this.element.querySelectorAll(
          '.card-position-details input'
        );
        await fillIn(inputs[0], '20');
        await fillIn(inputs[1], '30');
        await fillIn(inputs[2], '40');
        await fillIn(inputs[3], '50');
        await fillIn(inputs[4], '60');

        await click('.btn-submit');
        await waitUntil(() => receivedAnswer && currentURL() != currentUrl);

        expect(receivedAnswer).to.deep.equal({
          campaignAnswer: {
            position: { type: 'Point', coordinates: [20, 30] },
            attributes: {
              'position details': {
                type: 'manual',
                altitude: 40,
                accuracy: 50,
                altitudeAccuracy: 60,
              },
              'icon 1': { attribute1: null },
              'icon 2': [{ attribute1: null }],
            },
            campaign: '5ca78aa160780601008f6aaa',
            icons: ['000000000000000000000001', '000000000000000000000002'],
            _id: 'some-answer',
          },
        });

        expect(receivedPictures.length).to.equal(0);
        expect(currentURL()).to.equal(`/campaigns/${campaign._id}/success`);
      });
    });
  });
});
