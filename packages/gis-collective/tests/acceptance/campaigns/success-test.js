import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL } from '@ember/test-helpers';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';

describe('Acceptance | campaigns/success', function () {
  setupApplicationTest();
  let server;
  let campaign;
  let geolocation;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultTeam();

    const positionService = this.owner.lookup('service:position');
    geolocation = {
      watchPosition(callback) {
        callback({
          coords: {
            longitude: 1,
            latitude: 2,
            accuracy: 14,
            altitude: 15,
            altitudeAccuracy: 16,
          },
        });

        return 1;
      },
      clearWatch() {},
    };

    positionService.geolocation = geolocation;
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  describe('for a public campaign', function () {
    beforeEach(async function () {
      campaign = server.testData.storage.addDefaultCampaign();
      await visit(`/campaigns/${campaign._id}/success`);
    });

    it('can visit /campaigns/:id/success', async function () {
      await visit(`/campaigns/${campaign._id}/success`);
      expect(currentURL()).to.equal(`/campaigns/${campaign._id}/success`);
      expect(this.element.querySelector('h1')).not.to.equal(`Page Not Found`);
    });

    it('should display the campaign success messages', async function () {
      await visit(`/campaigns/${campaign._id}/success`);
      expect(this.element.querySelector('h1').textContent.trim()).to.equal(
        'Your answer was saved'
      );
      expect(this.element.querySelector('p').textContent.trim()).to.equal(
        "Your answer was saved and it's waiting a review."
      );
    });

    it('should display links to homepage and campaign', async function () {
      await visit(`/campaigns/${campaign._id}/success`);

      expect(
        this.element.querySelector('.btn-go-to-campaign')
      ).to.have.attribute('href', '/campaigns/5ca78aa160780601008f6aaa');
      expect(
        this.element.querySelector('.btn-go-to-homepage')
      ).to.have.attribute('href', '/browse/maps/_/map-view');
    });
  });
});
