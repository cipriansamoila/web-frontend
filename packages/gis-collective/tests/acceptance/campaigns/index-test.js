import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, click, waitFor } from '@ember/test-helpers';
import TestServer from '../../helpers/test-server';
import { authenticateSession } from 'ember-simple-auth/test-support';
import Modal from '../../helpers/modal';
import waitUntil from '@ember/test-helpers/wait-until';

describe('Acceptance | campaigns/index', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultPicture();

    const layout = server.testData.create.layout();
    layout.containers = [
      {
        rows: [
          {
            options: [],
            cols: [
              {
                type: 'type',
                data: {},
                options: [],
              },
            ],
          },
          {
            options: [],
            cols: [
              {
                type: 'type',
                data: {},
                options: [],
              },
            ],
          },
        ],
      },
    ];

    server.testData.storage.addLayout(layout);

    const page = server.testData.create.page();
    page._id = 'campaigns';
    page.cols = [
      {
        row: 0,
        col: 0,
        type: 'article',
        data: {
          id: 'campaigns',
          model: 'article',
        },
      },
      {
        row: 1,
        col: 0,
        type: 'campaign-card-list',
        data: {
          model: 'campaign',
        },
      },
    ];
    server.testData.storage.addPage(page);

    const article = server.testData.create.article();
    article._id = 'campaigns';
    article.content = '# Campaigns';
    server.testData.storage.addArticle(article);
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('can visit /campaigns', async function () {
    await visit('/campaigns');
    expect(this.element.querySelector('.error')).not.to.exist;
    expect(currentURL()).to.equal('/campaigns');
  });

  describe('when there is a campaign available', function () {
    let campaign;
    let picture;

    beforeEach(function () {
      campaign = server.testData.storage.addDefaultCampaign();
      picture = server.testData.storage.addDefaultPicture();
    });

    it('should display the campaign', async function () {
      await visit('/campaigns');

      expect(
        this.element.querySelector('.container h1').textContent.trim()
      ).to.equal('Campaigns');
      expect(this.element.querySelector('.campaign-card')).to.exist;

      expect(this.element.querySelector('.card-title').textContent).to.contain(
        campaign.name
      );

      await waitUntil(
        () =>
          this.element.querySelector('.cover').attributes.getNamedItem('style')
            ?.value != ''
      );

      const style = this.element
        .querySelector('.cover')
        .attributes.getNamedItem('style')?.value;
      expect(style).to.equal(`background-image: url('${picture.picture}/md')`);
      expect(this.element.querySelector('.campaign-card .btn-extend')).not.to
        .exist;
    });
  });

  describe('when there are two campaigns available', function () {
    beforeEach(function () {
      server.testData.storage.addDefaultCampaign('5ca8baf2ef1f7e01000849ba');
      server.testData.storage.addDefaultCampaign('6ca8baf2ef1f7e01000849bz');

      server.testData.storage.addDefaultPicture();
    });

    it('should display both campaigns', async function () {
      await visit('/campaigns');

      expect(this.element.querySelector('.campaign-card')).to.exist;

      const campaigns = this.element.querySelectorAll('.campaign-card a');

      expect(campaigns[0]).to.have.attribute(
        'href',
        '/campaigns/5ca8baf2ef1f7e01000849ba'
      );
      expect(campaigns[1]).to.have.attribute(
        'href',
        '/campaigns/6ca8baf2ef1f7e01000849bz'
      );
    });
  });

  describe('when the user is authenticated', function () {
    beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultUser(true, '5b8a59caef739394031a3f67');
    });

    describe('when there is an editable campaign available', function () {
      let campaign;

      beforeEach(function () {
        campaign = server.testData.create.campaign();
        campaign.canEdit = true;
        server.testData.storage.addCampaign(campaign);

        server.testData.storage.addDefaultPicture();
        server.server.delete(`/mock-server/campaigns/${campaign._id}`, () => {
          return [
            204,
            { 'Content-Type': 'application/json' },
            JSON.stringify({}),
          ];
        });
      });

      it('should delete the campaign', async function () {
        await visit('/campaigns');

        await click('.campaign-card .btn-extend');

        await waitFor('.campaign-card .btn-delete');
        await click('.campaign-card .btn-delete');

        await Modal.waitToDisplay();
        await Modal.clickDangerButton();

        expect(
          server.history.filter((a) => a.indexOf('DELETE') != -1)[0]
        ).to.contain(`/mock-server/campaigns/${campaign._id}`);
      });
    });
  });
});
