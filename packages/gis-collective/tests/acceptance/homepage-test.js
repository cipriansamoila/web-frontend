import { describe, it, beforeEach, afterEach } from 'mocha';
import { authenticateSession } from 'ember-simple-auth/test-support';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  waitUntil,
  waitFor,
  fillIn,
  click,
  blur,
} from '@ember/test-helpers';
import TestServer from '../helpers/test-server';

describe('Acceptance | homepage', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultIconSet();

    for (let i = 0; i < 10; i++) {
      server.testData.storage.addDefaultIcon(i);
    }
  });

  afterEach(function () {
    server.shutdown();
  });

  it('can visit /', async function () {
    await visit('/');
    expect(currentURL()).to.equal('/browse/maps/_/map-view');
  });

  describe('user tracking', function () {
    let tracking;

    it('does not trigger an event when matomo is not enabled', async function () {
      await visit('/');
      tracking = this.owner.lookup('service:tracking');

      expect(tracking.trackers['matomo']).not.to.exist;
    });

    describe('when matomo preferences are set', function () {
      let events = [];

      beforeEach(async function () {
        events = [];
        server.testData.storage.addPreference(
          'integrations.matomo.url',
          '/matomo-url'
        );
        server.testData.storage.addPreference(
          'integrations.matomo.siteId',
          '22'
        );

        tracking = this.owner.lookup('service:tracking');

        server.get(`/matomo-url/matomo.php`, (request) => {
          events.push(request.queryParams);
          return [200, {}, ''];
        });

        await visit('/');
      });

      it('registers the matomo tracker', function () {
        expect(tracking.trackers['matomo']).to.exist;
      });

      it('raises two events', async function () {
        const history = server.history.filter((a) => a.indexOf('matomo') != -1);

        expect(events.length).to.equal(2);
        expect(history.length).to.equal(2);
        expect(history[0]).to.startWith('GET /matomo-url/matomo.php?');
        expect(history[1]).to.startWith('GET /matomo-url/matomo.php?');
        expect(history[0]).to.contain(
          'map%20view&_cvar=%7B%22mapId%22%3A%22_%22%7D'
        );
        expect(history[1]).to.contain('browse%2Fmaps');
        expect(events[0]).to.deep.contain({
          action_name: 'map view',
          _cvar: '{"mapId":"_"}',
          idsite: '22',
          rec: '1',
          apiv: '1',
        });

        expect(events[1]).to.deep.contain({
          action_name: 'page view',
          idsite: '22',
          rec: '1',
          apiv: '1',
        });
      });

      it('raises 4 events on navigate', async function () {
        await click('.input-search');
        await click('.btn-view-all-icons');

        await waitUntil(() => events.length >= 2, { timeout: 5000 });

        const history = server.history.filter((a) => a.indexOf('matomo') != -1);

        expect(events.length).to.equal(4);
        expect(history.length).to.equal(4);
        expect(history[1]).to.startWith('GET /matomo-url/matomo.php?');
        expect(history[1]).to.contain('browse%2Fmaps%2F_%2Fmap-view');

        expect(events[0].action_name).to.equal('map view');
        expect(events[1]).to.deep.contain({
          action_name: 'page view',
          idsite: '22',
          rec: '1',
          apiv: '1',
        });
      });
    });
  });

  describe('When there are no stored models', function () {
    it('does not render the Browse menu item', async function () {
      await visit('/');
      expect(document.querySelector('.btn-browse')).not.to.exist;
    });

    it('does not render the Campaigns menu item', async function () {
      await visit('/');
      expect(document.querySelector('.btn-campaigns')).not.to.exist;
    });
  });

  describe('When there are some campaigns', function () {
    beforeEach(function () {
      server.testData.storage.addModel({
        _id: '000000000000000000000002',
        itemCount: 9,
        name: 'Campaign',
      });
    });

    it('does not render the Browse menu item', async function () {
      await visit('/');
      expect(document.querySelector('.btn-browse')).not.to.exist;
    });

    it('does render the Campaigns menu item', async function () {
      await visit('/');
      expect(document.querySelector('.btn-campaigns')).to.exist;
    });
  });

  describe('When there are some stored models', function () {
    beforeEach(function () {
      server.testData.storage.addModel({
        _id: '000000000000000000000001',
        itemCount: 9,
        name: 'Feature',
      });

      server.testData.storage.addModel({
        _id: '000000000000000000000002',
        itemCount: 9,
        name: 'Campaign',
      });
    });

    it('does not select any button in the main bar', async function () {
      await visit('/');
      expect(this.element.querySelector('.navbar .nav-item.active')).not.to
        .exist;
    });

    it('queries the the grouped icons', async function () {
      await visit('/browse/maps/_/map-view/filter-icons?viewbox=13,52,13,52');

      await waitUntil(
        () => server.history.filter((a) => a.indexOf('icons') != -1).length > 0
      );
      expect(server.history).to.contain('GET /mock-server/icons?group=true');
    });

    it('sets the world view extent if one is not set', async function () {
      await visit('/');

      await waitUntil(() => this.element.querySelector('.map-inner .map-view'));
      await waitUntil(
        () =>
          this.element
            .querySelector('.map-inner .map-view')
            .attributes.getNamedItem('data-extent-view').value != ''
      );

      const extent = this.element
        .querySelector('.map-inner .map-view')
        .attributes.getNamedItem('data-extent-view')
        .value.split(',')
        .map((a) => parseFloat(a));

      expect(extent.length).to.eq(4);
      expect(extent[0]).to.be.below(-130);
      expect(extent[1]).to.be.below(-25);
      expect(extent[2]).to.be.above(53);
      expect(extent[3]).to.be.above(53);
    });

    it('loads the logo', async function () {
      await visit('/');
      expect(document.querySelector('.logo')).to.exist;
    });

    it('loads the Browse menu item', async function () {
      await visit('/');
      expect(document.querySelector('.btn-browse').textContent.trim()).to.equal(
        'Browse'
      );
    });

    it('loads the Propose a site menu item', async function () {
      await visit('/');
      expect(
        document.querySelector('.btn-add-site').textContent.trim()
      ).to.equal('Propose a site');
    });

    it('hides the About menu item', async function () {
      await visit('/');
      expect(document.querySelector('.btn-about')).not.to.exist;
    });

    it('load the Help menu item', async function () {
      await visit('/');

      expect(document.querySelector('.dropdown-help')).to.exist;
    });

    it('loads the Sign in button', async function () {
      await visit('/');
      expect(document.querySelector('.btn-sign-in')).to.exist;
    });

    it('loads the Search field', async function () {
      await visit('/');
      expect(document.querySelector('.search-container')).to.exist;
      expect(document.querySelector('.search-container .input-search')).to
        .exist;
    });

    describe('When the isMultiProjectMode is false', function () {
      beforeEach(function () {
        server.testData.storage.addPreference(
          'access.isMultiProjectMode',
          'false'
        );
      });

      it('hides the Browse menu item when the user is not authenticated', async function () {
        await visit('/');
        expect(document.querySelector('.btn-browse')).not.to.exist;
      });

      it('hides the Browse menu item when a regular user is authenticated', async function () {
        server.testData.storage.addDefaultUser(false, 'me');
        authenticateSession();

        await visit('/');
        expect(document.querySelector('.btn-browse')).not.to.exist;
      });

      it('shows the Browse menu item when an admin user is authenticated', async function () {
        server.testData.storage.addDefaultUser(true, 'me');
        authenticateSession();

        await visit('/');
        expect(document.querySelector('.btn-browse')).to.exist;
      });
    });

    describe('When the allowManageWithoutTeams is true', function () {
      beforeEach(function () {
        server.testData.storage.addPreference(
          'access.allowManageWithoutTeams',
          'true'
        );
      });

      it('hides the Manage menu item when the user is not authenticated', async function () {
        await visit('/');
        expect(document.querySelector('.dropdown-manage')).not.to.exist;
      });

      it('hides the Manage menu item when a regular user is authenticated', async function () {
        server.testData.storage.addDefaultUser(false, 'me');
        authenticateSession();

        await visit('/');

        expect(document.querySelector('.dropdown-manage')).not.to.exist;
      });

      it('shows the Manage menu item when a regular user is authenticated and it has a team', async function () {
        server.testData.storage.addDefaultUser(false, 'me');
        server.testData.storage.addDefaultTeam();
        authenticateSession();

        await visit('/');

        expect(document.querySelector('.dropdown-manage')).to.exist;
      });

      it('shows the Manage menu item when an admin user is authenticated', async function () {
        server.testData.storage.addDefaultUser(true, 'me');
        authenticateSession();

        await visit('/');
        expect(document.querySelector('.dropdown-manage')).to.exist;
      });
    });

    describe('When the allowManageWithoutTeams is false', function () {
      beforeEach(function () {
        server.testData.storage.addPreference(
          'access.allowManageWithoutTeams',
          'false'
        );
      });

      it('hides the Manage menu item when the user is not authenticated', async function () {
        await visit('/');
        expect(document.querySelector('.dropdown-manage')).not.to.exist;
      });

      it('hides the Manage menu item when a regular user is authenticated', async function () {
        server.testData.storage.addDefaultUser(false, 'me');
        authenticateSession();

        await visit('/');
        expect(document.querySelector('.dropdown-manage')).not.to.exist;
      });

      it('shows the Manage menu item when a regular user is authenticated and there is a team', async function () {
        server.testData.storage.addDefaultTeam();
        server.testData.storage.addDefaultUser(false, 'me');
        authenticateSession();

        await visit('/');
        expect(document.querySelector('.dropdown-manage')).to.exist;
      });

      it('shows the Manage menu item when an admin user is authenticated', async function () {
        server.testData.storage.addDefaultUser(true, 'me');
        authenticateSession();

        await visit('/');
        expect(document.querySelector('.dropdown-manage')).to.exist;
      });
    });
  });

  describe('When the about article is visible', function () {
    beforeEach(function () {
      const article = server.testData.create.article('about');
      article.visibility.isPublic = true;
      server.testData.storage.addArticle(article);
    });

    it('shows the About menu item', async function () {
      await visit('/');
      expect(document.querySelector('.btn-about')).to.exist;
    });
  });

  describe('search', function () {
    it('sends a request with the search term without pressing search', async function () {
      await visit('/');
      await fillIn('.search-container .input-search', 'siteName');

      await waitUntil(
        () =>
          server.history.filter((a) => a.indexOf('features') != -1).length > 0
      );
      expect(
        server.history.filter((a) => a.indexOf('/features') != -1)[0]
      ).to.contain('term=siteName');
    });

    it('redirects to spotlight when the search field is cleared', async function () {
      await visit('/browse/maps/_/map-view/s/farm');
      await fillIn('.search-container .input-search', '');

      expect(currentURL()).to.equal('/browse/maps/_/map-view/spotlight');
    });

    it('stays in the search mode when the search input is focused', async function () {
      await visit('/browse/maps/_/map-view/s/farm');
      await click('.search-container .input-search');

      expect(currentURL()).to.equal('/browse/maps/_/map-view/s/farm');
    });

    it('the back to search button is not visible while the search results are visible', async function () {
      await visit('/');
      await fillIn('.search-container .input-search', 'siteName');

      expect(this.element.querySelector('.btn-show-search')).not.to.exist;
    });

    it('hides the show search results button when the result is cleared', async function () {
      await visit('/');
      await fillIn('.search-container .input-search', 'siteName');
      await click('.search-container .btn-close-search');

      expect(this.element.querySelector('.btn-show-search')).not.to.exist;
      expect(this.element.querySelector('.input-search').value).to.equal('');
    });

    describe('when there is a site', function () {
      let map;
      let site;

      beforeEach(function () {
        map = server.testData.create.map();
        server.testData.storage.addDefaultPicture();
        server.testData.storage.addDefaultTeam();
        server.testData.storage.addDefaultIcon();
        server.testData.storage.addDefaultIcon('5ca7bfc0ecd8490100cab980');
        server.testData.storage.addMap(map);
        site = server.testData.create.feature();
        server.testData.storage.addFeature(site);
      });

      it('returns the searched site if there is a match', async function () {
        await visit('/');
        await fillIn('.search-container .input-search', site.name);

        await waitUntil(
          () =>
            server.history.filter((a) => a.indexOf('features') != -1).length > 0
        );

        expect(document.querySelector('.site-card .card-title')).to.exist;
        expect(
          document.querySelector('.site-card .card-title').textContent.trim()
        ).to.equal(site.name);
      });

      it('displays site info when clicking on the search result', async function () {
        await visit('/');
        await fillIn('.search-container .input-search', site.name);
        await waitUntil(
          () =>
            server.history.filter((a) => a.indexOf('features') != -1).length > 0
        );

        await click('.site-card .card-img-overlay');

        await waitFor('.content h1');

        expect(
          document.querySelector('.content h1').textContent.trim()
        ).to.equal(site.name);
        expect(document.querySelector('.content .btn-open')).to.have.attribute(
          'href',
          `/browse/sites/${site._id}`
        );
        expect(document.querySelector('.btn-close-search')).to.exist;
      });

      it('shows the search results when the article is closed and the search input is focused', async function () {
        await visit('/');
        await fillIn('.search-container .input-search', site.name);
        await waitUntil(
          () =>
            server.history.filter((a) => a.indexOf('features') != -1).length > 0
        );

        await click('.site-card');
        await click('.map-view');

        await waitUntil(() => currentURL() == '/browse/maps/_/map-view');

        await blur('.input-search');
        expect(this.element.querySelector('.input-search').value).to.equal(
          site.name
        );

        await click('.input-search');

        await waitUntil(() => currentURL() != '/browse/maps/_/map-view');

        expect(currentURL()).to.equal(
          '/browse/maps/_/map-view/s/Nomadisch%20Gr%C3%BCn%20-%20Local%20Urban%20Food'
        );
      });

      it('is possible to navigate to the article and then back to the search list', async function () {
        await visit('/');
        await fillIn('.search-container .input-search', site.name);
        await waitUntil(
          () =>
            server.history.filter((a) => a.indexOf('features') != -1).length > 0
        );

        await click('.site-card .card-img-overlay');

        expect(currentURL()).to.startWith(
          `/browse/maps/_/map-view/${site._id}`
        );
        expect(this.element.querySelector('.content.show')).to.exist;
        expect(this.element.querySelector('.cover-feature')).to.exist;

        await click('.btn-go-back');

        expect(currentURL()).to.startWith(
          `/browse/maps/_/map-view/s/Nomadisch%20Gr%C3%BCn%20-%20Local%20Urban%20Food`
        );
      });
    });
  });

  describe('the site article', function () {
    let map;
    let site;
    let picture;
    let icon;

    beforeEach(function () {
      map = server.testData.create.map();
      picture = server.testData.storage.addDefaultPicture();
      icon = server.testData.storage.addDefaultIcon();
      server.testData.storage.addDefaultIconSet();
      server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultIcon('5ca7bfc0ecd8490100cab980');
      server.testData.storage.addMap(map);
      site = server.testData.create.feature();
      server.testData.storage.addFeature(site);
    });

    it('returns the searched site if there is a match', async function () {
      await visit(`/browse/maps/_/map-view/${site._id}`);

      await waitUntil(
        () =>
          this.element
            .querySelector('.cover-feature')
            .attributes.getNamedItem('style')
            .value.indexOf('undefined') == -1
      );

      expect(this.element.querySelector('.cover-feature')).to.exist;
      expect(this.element.querySelector('.btn-close-search')).to.exist;

      expect(this.element.querySelector('.content .btn-open')).to.exist;
      expect(this.element.querySelector('.content .btn-edit')).not.to.exist;

      expect(
        this.element.querySelector('.content .btn-open')
      ).to.have.attribute('href', `/browse/sites/${site._id}`);
      expect(
        this.element.querySelector('.content h1').textContent.trim()
      ).to.equal(site.name);
      expect(
        this.element.querySelector('.content p').textContent.trim()
      ).to.equal(site.description);

      expect(this.element.querySelector('.cover-feature')).to.have.attribute(
        'style',
        `background-image: url('${picture.picture}/lg')`
      );

      expect(
        this.element.querySelectorAll('.browse-icon-list img').length
      ).to.equal(1);
      expect(
        this.element.querySelector('.browse-icon-list img')
      ).to.have.attribute('src', icon.image.value);
      expect(
        this.element.querySelector('.browse-icon-list img')
      ).to.have.attribute('alt', icon.name);

      expect(this.element.querySelector('.navbar')).to.exist;
      expect(this.element.querySelector('.navbar')).to.have.class(
        'bar-invisible'
      );
    });

    it('opens the article page when open button is pressed', async function () {
      await visit(`/browse/maps/_/map-view/${site._id}`);

      await click('.btn-open');

      expect(currentURL()).to.equal(`/browse/sites/${site._id}`);

      expect(this.element.querySelector('.navbar.map-menu')).not.to.exist;
      expect(this.element.querySelector('.navbar')).not.to.have.class(
        'bar-invisible'
      );
    });

    describe('when the site is editable', function () {
      beforeEach(function () {
        authenticateSession();
        site.canEdit = true;
        server.testData.storage.addFeature(site);
        server.testData.storage.addDefaultUser(
          false,
          '5b8a59caef739394031a3f67'
        );
      });

      it('renders the edit button', async function () {
        await visit(`/browse/maps/_/map-view/${site._id}`);

        await waitFor('.content-scroll h1');

        expect(this.element.querySelector('.content .btn-edit')).to.exist;
        expect(document.querySelector('.content .btn-edit')).to.have.attribute(
          'href',
          `/manage/sites/${site._id}`
        );
      });

      it('opens the edit feature page when open button is pressed', async function () {
        await visit(`/browse/maps/_/map-view/${site._id}`);

        await click('.btn-edit');

        expect(currentURL()).to.equal(`/manage/sites/${site._id}`);

        expect(this.element.querySelector('.navbar.map-menu')).not.to.exist;
        expect(this.element.querySelector('.navbar')).not.to.have.class(
          'bar-invisible'
        );
      });
    });
  });
});
