import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  waitUntil,
  fillIn,
  triggerEvent,
} from '@ember/test-helpers';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';
import { authenticateSession } from 'ember-simple-auth/test-support';

describe('Acceptance | preferences/tokens', function () {
  setupApplicationTest();
  let server;
  let user;
  let receivedToken;
  let receivedTokenCreation;

  beforeEach(function () {
    server = new TestServer();

    user = server.testData.create.user();
    user.tokens = [
      {
        scopes: ['api'],
        created: '2021-04-14T20:20:07.696986',
        expire: '2021-04-22T22:00:00Z',
        name: 'ter',
      },
    ];

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addUser('me', user);

    server.testData.storage.addDefaultProfile(user._id);
  });

  afterEach(function () {
    Modal.clear();
    server.shutdown();
  });

  describe('when the user is authenticated', function () {
    beforeEach(function () {
      authenticateSession();

      server.get(`/mock-server/users/${user._id}/listTokens`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({ tokens: user.tokens }),
        ];
      });

      receivedToken = null;
      server.post(`/mock-server/users/${user._id}/revoke`, (request) => {
        receivedToken = JSON.parse(request.requestBody);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedToken),
        ];
      });

      receivedTokenCreation = null;
      server.post(`/mock-server/users/${user._id}/token`, (request) => {
        receivedTokenCreation = JSON.parse(request.requestBody);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({ token: 'test-token' }),
        ];
      });
    });

    it('can visit /preferences/tokens', async function () {
      await visit('/preferences/tokens');

      expect(currentURL()).to.equal('/preferences/tokens');
      expect(this.element.querySelector('.main-container > .error')).not.to
        .exist;
    });

    it('can remove a token', async function () {
      await visit('/preferences/tokens');

      await click('.btn-danger');

      await Modal.waitToDisplay();

      await click('.btn-resolve');

      await waitUntil(() => receivedToken);

      expect(receivedToken).to.deep.equal({ name: 'ter' });
    });

    it('can add a new token', async function () {
      await visit('/preferences/tokens');

      await fillIn('#tokenName', 'some token name');

      this.element.querySelector('.form-select.year').value = 2030;
      await triggerEvent('.form-select.year', 'change');
      this.element.querySelector('.form-select.month').value = 3;
      await triggerEvent('.form-select.month', 'change');
      this.element.querySelector('.form-select.day').value = 20;
      await triggerEvent('.form-select.day', 'change');

      await click('.btn-submit');

      waitUntil(() => receivedTokenCreation);

      expect(receivedTokenCreation.name).to.equal('some token name');

      expect(this.element.querySelector('#tokenName').value).to.equal(
        'test-token'
      );

      await click('.btn-done');

      expect(this.element.querySelector('.btn-done')).not.to.exist;
      expect(this.element.querySelector('.btn-submit')).to.exist;
    });
  });
});
