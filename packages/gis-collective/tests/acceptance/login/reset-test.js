import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, click, fillIn, waitFor } from '@ember/test-helpers';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';
import FastBootMock from '../../helpers/fast-boot-mock';

describe('Acceptance | login/reset', function () {
  setupApplicationTest();
  let server;
  let receivedUserData;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();

    receivedUserData = null;
    server.post('/mock-server/users/forgotpassword', (request) => {
      receivedUserData = JSON.parse(request.requestBody);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify({})];
    });
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  describe('when the page is rendered using fastboot', function () {
    beforeEach(function () {
      this.owner.register('service:fastboot', FastBootMock);
    });

    it('should show the loading logo', async function () {
      await visit('/login/reset');
      expect(this.element.querySelector('.container.loading')).to.exist;
    });
  });

  describe('when the user is not authenticated', function () {
    it('can visit /login/reset', async function () {
      await visit('/login/reset');
      expect(currentURL()).to.equal('/login/reset');

      expect(
        this.element.querySelector('.main-container h2').textContent.trim()
      ).to.equal('Reset password');
      expect(
        this.element.querySelector('.main-container label').textContent.trim()
      ).to.equal('E-mail');
      expect(
        this.element.querySelector('.main-container .btn').textContent.trim()
      ).to.equal('Reset password');
      expect(
        this.element.querySelector('.main-container p').textContent.trim()
      ).to.startWith('Already have an account?');
      expect(
        this.element.querySelector('.main-container p a').textContent.trim()
      ).to.equal('Sign in');
    });

    it('sends the provided email to the server', async function () {
      await visit('/login/reset');
      expect(currentURL()).to.equal('/login/reset');

      await fillIn('#identification', 'me@gmail.com');
      await click(this.element.querySelector('.main-container .btn'));

      expect(receivedUserData).to.deep.equal({ email: 'me@gmail.com' });

      await Modal.waitToDisplay();

      expect(Modal.isVisible()).to.equal(true);
      expect(Modal.title()).to.equal('Forgot password');
      expect(Modal.message()).to.equal(
        'If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes.'
      );

      await click('.btn-resolve');
      await Modal.waitToHide();
    });
  });

  describe('when the email and token are present in the url', function () {
    beforeEach(function () {
      server.post('/mock-auth-server/resetpassword', (request) => {
        receivedUserData = JSON.parse(request.requestBody);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({}),
        ];
      });
    });

    it('shows the reset password form', async function () {
      await visit(
        '/login/reset?email=john@doe.com&token=098f2741-dc3a-4f62-94d0-77b298add8d7'
      );
      expect(currentURL()).to.equal(
        '/login/reset?email=john@doe.com&token=098f2741-dc3a-4f62-94d0-77b298add8d7'
      );

      expect(
        this.element.querySelector('.main-container h2').textContent.trim()
      ).to.equal('Reset password');
      expect(
        this.element.querySelector('.main-container .btn').textContent.trim()
      ).to.equal('Reset password');
      expect(
        this.element.querySelector('.main-container .btn')
      ).to.have.attribute('disabled', '');
      expect(
        this.element
          .querySelector('label[for=InputNewPassword]')
          .textContent.trim()
      ).to.equal('New password');
      expect(
        this.element
          .querySelector('label[for=InputConfirmPassword]')
          .textContent.trim()
      ).to.equal('Confirm new password');
      expect(this.element.querySelector('.invalid-feedback').textContent.trim())
        .to.exist;
    });

    it('sends the new password to the server when they match', async function () {
      await visit(
        '/login/reset?email=john@doe.com&token=098f2741-dc3a-4f62-94d0-77b298add8d7'
      );
      await fillIn('#InputNewPassword', 'secretpassword');
      await fillIn('#InputConfirmPassword', 'secretpassword');

      expect(
        this.element.querySelector('.valid-feedback').textContent.trim()
      ).to.equal('Everything is in order');
      expect(
        this.element.querySelector('.main-container .btn')
      ).to.have.attribute('disabled', null);

      await click('.btn-submit');
      waitFor(() => receivedUserData);

      expect(receivedUserData).to.deep.equal({
        email: 'john@doe.com',
        token: '098f2741-dc3a-4f62-94d0-77b298add8d7',
        password: 'secretpassword',
      });
      await Modal.waitToDisplay();

      expect(Modal.isVisible()).to.equal(true);
      expect(Modal.title()).to.equal('change password');
      expect(Modal.message()).to.equal('Your password has been changed!');
      expect(currentURL()).to.equal('/login');

      await click('.btn-resolve');
      await Modal.waitToHide();
    });
  });

  describe('when the server returns an error', function () {
    beforeEach(function () {
      server.post('/mock-server/users/forgotpassword', () => {
        return [
          400,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            errors: [
              {
                description: 'Invalid reset password token.',
                status: 400,
                title: 'Validation error',
              },
            ],
          }),
        ];
      });
    });

    it('shows a popup when the server returns an error', async function () {
      this.timeout(1000000);
      await visit(
        '/login/reset?email=john@doe.com&token=098f2741-dc3a-4f62-94d0-77b298add8d7'
      );

      await fillIn('#InputNewPassword', 'secretpassword');
      await fillIn('#InputConfirmPassword', 'secretpassword');

      expect(
        this.element.querySelector('.valid-feedback').textContent.trim()
      ).to.equal('Everything is in order');
      expect(
        this.element.querySelector('.main-container .btn')
      ).to.have.attribute('disabled', null);

      await click('.btn-submit');
      await Modal.waitToDisplay();

      expect(Modal.isVisible()).to.equal(true);
      expect(Modal.title()).to.equal('Validation error');
      expect(Modal.message()).to.equal('Invalid reset password token.');

      expect(currentURL()).to.equal(
        '/login/reset?email=john@doe.com&token=098f2741-dc3a-4f62-94d0-77b298add8d7'
      );

      await click('.btn-resolve');
      await Modal.waitToHide();
    });
  });
});
