import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, click, fillIn } from '@ember/test-helpers';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';

describe('Acceptance | login', function () {
  setupApplicationTest();
  let server;
  let user;

  beforeEach(function () {
    server = new TestServer();
    let translation1 = server.testData.create.translation('1');
    translation1.name = 'Romana';
    translation1.locale = 'ro-ro';
    translation1.file = null;

    let translation2 = server.testData.create.translation('2');
    translation2.name = 'Francois';
    translation2.locale = 'fr-fr';
    translation2.file = null;

    user = server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultArticle();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addTranslation(translation1);
    server.testData.storage.addTranslation(translation2);
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  describe('when the user should be redirected to the welcome presentation', function () {
    beforeEach(function () {
      let profile = server.testData.create.userProfile(user._id);
      profile.showWelcomePresentation = true;
      server.testData.storage.addUserProfile(profile);
      server.testData.storage.addDefaultPresentation('presentation--welcome');

      server.post('/mock-server/auth/token', () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            access_token: '559370ce-eaf6-4cd3-aba1-a384745672d9',
            token_type: 'Bearer',
            expires_in: 3600,
            refresh_token: '2bd7e0b6-d177-46f1-8575-678606dbf0fc',
          }),
        ];
      });
    });

    it('should redirect to the presentation when is enabled', async function () {
      server.testData.storage.addPreference(
        'appearance.showWelcomePresentation',
        true
      );
      await visit('/login');

      await fillIn('#identification', 'Useremail@mail.com');
      await fillIn('#password', 'password');

      await click('button[type=submit]');

      expect(currentURL()).to.equal('/presentation/welcome?locale=en-us');
    });

    it('should redirect to the world map when is disabled', async function () {
      server.testData.storage.addPreference(
        'appearance.showWelcomePresentation',
        false
      );
      await visit('/login');

      await fillIn('#identification', 'Useremail@mail.com');
      await fillIn('#password', 'password');

      await click('button[type=submit]');

      expect(currentURL()).to.equal('/browse/maps/_/map-view');
    });
  });

  describe('when the login article is not set', function () {
    it('can visit /login', async function () {
      await visit('/login');

      expect(this.element.querySelector('#identification')).to.exist;
      expect(this.element.querySelector('#password')).to.exist;
      expect(this.element.querySelector('button[type=submit]')).to.exist;

      expect(currentURL()).to.equal('/login');
    });

    it('should not show the menu on /login for private services', async function () {
      server.testData.storage.addPreference('register.mandatory', true);
      server.testData.storage.addPreference('register.enabled', false);

      await visit('/login');
      expect(this.element.querySelector('.navbar')).not.to.exist;
    });

    it('should use the default locale when it has an invalid value', async function () {
      await visit('/login?locale=ro-rooo');
      expect(currentURL()).to.equal('/login');
    });

    it('should persist the language after login', async function () {
      await visit('/login?locale=ro-ro');
      expect(currentURL()).to.equal('/login?locale=ro-ro');

      await fillIn('#identification', 'Useremail@mail.com');
      await fillIn('#password', 'password');

      let received;

      server.post('/mock-server/auth/token', (request) => {
        received = request.requestBody;
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            access_token: '559370ce-eaf6-4cd3-aba1-a384745672d9',
            token_type: 'Bearer',
            expires_in: 3600,
            refresh_token: '2bd7e0b6-d177-46f1-8575-678606dbf0fc',
          }),
        ];
      });

      await click('button[type=submit]');

      expect(received).to.equal(
        'grant_type=password&username=useremail%40mail.com&password=password'
      );
      expect(currentURL()).to.equal('/browse/maps/_/map-view?locale=ro-ro');
    });

    it('should redirect after login if a redirect query param is used', async function () {
      await visit('/login?redirect=/browse/sites');
      expect(currentURL()).to.equal('/login?redirect=/browse/sites');

      await fillIn('#identification', 'useremail@mail.com');
      await fillIn('#password', 'password');
      server.post('/mock-server/auth/token', () => [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          access_token: '559370ce-eaf6-4cd3-aba1-a384745672d9',
          token_type: 'Bearer',
          expires_in: 3600,
          refresh_token: '2bd7e0b6-d177-46f1-8575-678606dbf0fc',
        }),
      ]);
      await click('button[type=submit]');

      expect(currentURL()).to.equal('/browse/sites?locale=en-us');
    });

    it('should redirect to the redirect query param if the user is already logged in', async function () {
      await visit('/login');
      await fillIn('#identification', 'useremail@mail.com');
      await fillIn('#password', 'password');
      server.post('/mock-server/auth/token', () => [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          access_token: '559370ce-eaf6-4cd3-aba1-a384745672d9',
          token_type: 'Bearer',
          expires_in: 3600,
          refresh_token: '2bd7e0b6-d177-46f1-8575-678606dbf0fc',
        }),
      ]);
      await click('button[type=submit]');

      await visit('/login?redirect=/browse/sites');
      expect(currentURL()).to.equal('/browse/sites');
    });

    it('should display an error for invalid credentials', async function () {
      server.post('/mock-server/auth/token', () => {
        return [
          401,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            error: 'Invalid password or username',
          }),
        ];
      });

      await visit('/login');

      await fillIn('#identification', 'invalid-email');
      await fillIn('#password', 'invalid-password');
      await click('button[type=submit]');

      expect(
        this.element.querySelector('.alert-danger').textContent.trim()
      ).to.equal('Invalid password or username');
    });

    it('should not display the article', async function () {
      await visit('/login');
      expect(this.element.querySelector('.login-article')).not.to.exist;
      expect(this.element.querySelector('h1')).not.to.exist;
    });
  });

  describe('when the login article is set', function () {
    let article;

    beforeEach(function () {
      article = server.testData.create.article('login');
      article.title = 'login title';
      article.content = '# login title\n\nlogin description';

      server.testData.storage.addArticle(article);
    });

    it('should display the article', async function () {
      await visit('/login');

      expect(this.element.querySelector('.login-article h1')).to.exist;
      expect(this.element.querySelector('.login-article div p')).to.exist;

      expect(
        this.element.querySelector('.login-article h1').textContent.trim()
      ).to.equal('login title');
      expect(
        this.element.querySelector('.login-article p').textContent.trim()
      ).to.equal('login description');
    });
  });

  describe('when the login article has an empty content', function () {
    let article;

    beforeEach(function () {
      article = server.testData.create.article('login');
      article.title = '';
      article.content = '';

      server.testData.storage.addArticle(article);
    });

    it('should not display the article', async function () {
      await visit('/login');

      expect(this.element.querySelector('.login-article')).not.to.exist;
      expect(this.element.querySelector('h1')).not.to.exist;
    });
  });
});
