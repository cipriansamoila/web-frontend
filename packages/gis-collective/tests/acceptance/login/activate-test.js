import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL } from '@ember/test-helpers';
import TestServer from '../../helpers/test-server';
import FastBootMock from '../../helpers/fast-boot-mock';

describe('Acceptance | login/activate', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
  });

  afterEach(function () {
    server.shutdown();
  });

  it('can visit /login/activate', async function () {
    await visit('/login/activate');
    expect(currentURL()).to.equal('/login/activate');

    expect(this.element.querySelector('h2').textContent.trim()).to.equal(
      'Account activation'
    );
    expect(this.element.querySelector('.btn-secondary[type=submit]')).to.exist;
    expect(this.element.querySelector('#identification')).to.exist;
  });

  describe('when the page is rendered using fastboot', function () {
    let hasActivateRequest;

    beforeEach(function () {
      hasActivateRequest = false;
      server.testData.storage.addDefaultPreferences();
      server.post('/mock-server/users/activate', () => {
        hasActivateRequest = true;
        return [400, { 'Content-Type': 'application/json' }];
      });

      this.owner.register('service:fastboot', FastBootMock);
    });

    it('should show the loading logo', async function () {
      await visit('/login/activate?email=test@yahoo.com&token=13526e27');

      expect(hasActivateRequest).to.equal(false);
      expect(this.element.querySelector('.container.loading')).to.exist;
    });
  });

  describe('when the activation request fails', function () {
    beforeEach(function () {
      server.post('/mock-server/users/activate', () => {
        return [
          400,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            errors: [
              {
                description: 'Invalid request.',
                status: 400,
                title: 'Validation error',
              },
            ],
          }),
        ];
      });
    });

    it('should show the error message', async function () {
      await visit('/login/activate?email=test@yahoo.com&token=13526e27');
      expect(currentURL()).to.equal(
        '/login/activate?email=test@yahoo.com&token=13526e27'
      );

      expect(this.element.querySelector('h2').textContent.trim()).to.equal(
        'Account activation'
      );
      expect(this.element.querySelector('.alert').textContent.trim()).to.equal(
        'The activation token has expired. Please try again.'
      );
      expect(this.element.querySelector('.btn-secondary[type=submit]')).to
        .exist;
      expect(this.element.querySelector('#identification')).to.exist;
    });
  });

  describe('when the activation is successful', function () {
    beforeEach(function () {
      server.post('/mock-server/users/activate', () => {
        return [204, { 'Content-Type': 'application/json' }];
      });
    });

    it('should confirm activation and not show an error message', async function () {
      await visit('/login/activate?email=test@yahoo.com&token=13526e27');
      expect(currentURL()).to.equal(
        '/login/activate?email=test@yahoo.com&token=13526e27'
      );

      expect(this.element.querySelector('h2').textContent.trim()).to.equal(
        'Account activation'
      );
      expect(this.element.querySelector('.alert')).to.not.exist;
      expect(this.element.querySelector('p').textContent.trim()).to.contain(
        'Your account was activated.'
      );
    });
  });
});
