import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  fillIn,
  triggerEvent,
} from '@ember/test-helpers';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';
import FastBootMock from '../../helpers/fast-boot-mock';

describe('Acceptance | login/register', function () {
  setupApplicationTest();
  let server;
  let preferences;
  let receivedUserData;

  beforeEach(function () {
    server = new TestServer();
    preferences = {};

    preferences['register.mandatory'] = {
      _id: '5cc3677fb93d1f0100c58f2e',
      value: false,
      name: 'register.mandatory',
    };
    preferences['register.enabled'] = {
      _id: '5cc3677fb93d1f0100c58f2e',
      value: true,
      name: 'register.enabled',
    };
    preferences['appearance.name'] = {
      _id: '5cc3677fb93d1f0100c58f2e',
      value: 'Open Green Map',
      name: 'appearance.name',
    };

    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPreferences();

    receivedUserData = null;
    server.post('/mock-server/users/register', (request) => {
      receivedUserData = JSON.parse(request.requestBody);

      return [200, { 'Content-Type': 'application/json' }, JSON.stringify({})];
    });
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  describe('when the page is rendered using fastboot', function () {
    beforeEach(function () {
      server.testData.storage.addPreference('captcha.enabled', 'false');
      this.owner.register('service:fastboot', FastBootMock);
    });

    it('should show the loading logo', async function () {
      await visit('/login/register');

      expect(this.element.querySelector('.container.loading')).to.exist;
    });
  });

  describe('when the user is not authenticated and recaptcha is disabled', function () {
    beforeEach(function () {
      server.testData.storage.addPreference('captcha.enabled', 'false');
    });

    describe('when is embedded', function () {
      it('should not render the login link', async function () {
        await visit('/login/register?embed=true');

        expect(this.element.querySelector('.sign-in-link-section')).not.to
          .exist;
      });

      it('redirects to /login/register-success when the form is successfully submitted', async function () {
        await visit('/login/register?embed=true');

        this.element.querySelector('.name-salutation select').value = 'ms';
        await triggerEvent('.name-salutation select', 'change');

        await fillIn('.input-title', 'Dr.');
        await fillIn('.input-first-name', 'John');
        await fillIn('.input-last-name', 'Doe');
        await fillIn('.input-username', 'God');
        await fillIn('.input-email', 'God@heaven.sky');
        await fillIn('.input-password', '1234567890');
        await fillIn('.input-password-confirm', '1234567890');
        await click('#termsAndPrivacyPolicy');

        await click('.btn-submit');

        expect(currentURL()).to.equal('/login/register-success?embed=true');
      });
    });

    it('should render the login link', async function () {
      await visit('/login/register');

      expect(this.element.querySelector('.sign-in-link-section')).to.exist;
    });

    it('the submit button should be disabled by default', async function () {
      await visit('/login/register');

      expect(this.element.querySelector('.btn-submit')).to.have.attribute(
        'disabled',
        ''
      );
      expect(currentURL()).to.equal('/login/register');
    });

    it('the submit button should be disabled when the name is filled', async function () {
      await visit('/login/register');

      await fillIn('.input-first-name', 'John');
      await fillIn('.input-last-name', 'Doe');

      expect(this.element.querySelector('.btn-submit')).to.have.attribute(
        'disabled',
        ''
      );
      expect(currentURL()).to.equal('/login/register');
    });

    it('the submit button should be disabled when the username is filled', async function () {
      await visit('/login/register');

      await fillIn('.input-username', 'John');

      expect(this.element.querySelector('.btn-submit')).to.have.attribute(
        'disabled',
        ''
      );
      expect(currentURL()).to.equal('/login/register');
    });

    it('the submit button should be disabled when the email is filled', async function () {
      await visit('/login/register');

      await fillIn('.input-email', 'john@asd.asd');

      expect(this.element.querySelector('.btn-submit')).to.have.attribute(
        'disabled',
        ''
      );
      expect(currentURL()).to.equal('/login/register');
    });

    it('the submit button should be disabled when the password is filled', async function () {
      await visit('/login/register');

      await fillIn('.input-password', '1234567890');
      await fillIn('.input-password-confirm', '1234567890');

      expect(this.element.querySelector('.btn-submit')).to.have.attribute(
        'disabled',
        ''
      );
      expect(currentURL()).to.equal('/login/register');
    });

    it('the submit button should be disabled when the terms and privacy policy checkbox is checked', async function () {
      await visit('/login/register');
      await click('#termsAndPrivacyPolicy');

      expect(this.element.querySelector('.btn-submit')).to.have.attribute(
        'disabled',
        ''
      );
      expect(currentURL()).to.equal('/login/register');
    });

    it('the submit button should be disabled when all mandatory fields filled in but the terms and privacy policy checkbox is not checked', async function () {
      await visit('/login/register');
      await fillIn('.input-first-name', 'John');
      await fillIn('.input-last-name', 'Doe');
      await fillIn('.input-username', 'God');
      await fillIn('.input-email', 'god@heaven.sky');
      await fillIn('.input-password', '1234567890');
      await fillIn('.input-password-confirm', '1234567890');

      expect(this.element.querySelector('.btn-submit')).to.have.attribute(
        'disabled',
        ''
      );
      expect(currentURL()).to.equal('/login/register');
    });

    it('the submit button should be disabled when the passwords do not match', async function () {
      await visit('/login/register');

      await fillIn('.input-first-name', 'John');
      await fillIn('.input-last-name', 'Doe');
      await fillIn('.input-username', 'God');
      await fillIn('.input-email', 'god@heaven.sky');
      await fillIn('.input-password', '1234567890');
      await fillIn('.input-password-confirm', 'other');

      expect(this.element.querySelector('.btn-submit')).to.have.attribute(
        'disabled',
        ''
      );
      expect(currentURL()).to.equal('/login/register');
    });

    it('the submit button should be disabled when the passwords have 9 chars', async function () {
      await visit('/login/register');

      await fillIn('.input-first-name', 'John');
      await fillIn('.input-last-name', 'Doe');
      await fillIn('.input-username', 'God');
      await fillIn('.input-email', 'god@heaven.sky');
      await fillIn('.input-password', '123456789');
      await fillIn('.input-password-confirm', '123456789');

      expect(this.element.querySelector('.btn-submit')).to.have.attribute(
        'disabled',
        ''
      );
      expect(currentURL()).to.equal('/login/register');
    });

    it('the submit button should be disabled when the email is invalid', async function () {
      await visit('/login/register');

      await fillIn('.input-first-name', 'John');
      await fillIn('.input-last-name', 'Doe');
      await fillIn('.input-username', 'God');
      await fillIn('.input-email', 'invalid');
      await fillIn('.input-password', '1234567890');
      await fillIn('.input-password-confirm', '1234567890');

      expect(this.element.querySelector('.btn-submit')).to.have.attribute(
        'disabled',
        ''
      );
      expect(currentURL()).to.equal('/login/register');
    });

    it('can create a new account', async function () {
      await visit('/login/register');

      this.element.querySelector('.name-salutation select').value = 'ms';
      await triggerEvent('.name-salutation select', 'change');

      await fillIn('.input-title', 'Dr.');
      await fillIn('.input-first-name', 'John');
      await fillIn('.input-last-name', 'Doe');
      await fillIn('.input-username', 'God');
      await fillIn('.input-email', 'God@heaven.sky');
      await fillIn('.input-password', '1234567890');
      await fillIn('.input-password-confirm', '1234567890');
      await click('#termsAndPrivacyPolicy');

      expect(this.element.querySelector('.btn-submit')).not.to.have.attribute(
        'disabled'
      );
      await click('.btn-submit');

      expect(receivedUserData).to.deep.equal({
        title: 'Dr.',
        salutation: 'ms',
        firstName: 'John',
        lastName: 'Doe',
        username: 'God',
        email: 'god@heaven.sky',
        password: '1234567890',
      });

      await Modal.waitToDisplay();
      expect(
        this.element.querySelector('.modal-title').textContent.trim()
      ).to.equal('Create account');
      expect(
        this.element.querySelector('.modal-body').textContent.trim()
      ).to.equal(
        'Your account has been created! Check your email for the activation link.'
      );

      expect(currentURL()).to.equal('/login');

      await click('.btn-resolve');
      await Modal.waitToHide();
    });
  });
});
