import { describe, it, beforeEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, triggerEvent, waitUntil, click } from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import typeIn from '@ember/test-helpers/dom/type-in';
import { authenticateSession } from 'ember-simple-auth/test-support';

describe('Acceptance | wizard/import/file', function () {
  setupApplicationTest();
  let server;
  let mapFile;
  let meta;
  let receivedFile;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    meta = {
      time: '2021-02-17T19:20:14',
      ignored: 0,
      ping: '2021-02-17T19:20:15',
      total: 63,
      errors: 0,
      sent: 0,
      duration: 1,
      processed: 63,
      runId: '602d6c20630f6601005307bc.0',
      status: 'success',
    };
    server.testData.storage.addDefaultMap('5e2f42c96e290a010057dd95');

    server.get(`/mock-server/mapfiles/:id/meta`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(meta),
      ];
    });

    server.put(`/mock-server/mapfiles/:id`, (request) => {
      receivedFile = JSON.parse(request.requestBody);
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedFile),
      ];
    });

    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultIconSet();
    authenticateSession();
  });

  describe('when there is a file with no discovered fields', function () {
    beforeEach(function () {
      mapFile = server.testData.storage.addDefaultMapFile();
    });

    it('shows a message that the file is not valid', async function () {
      await visit(`/wizard/import/file/${mapFile._id}/fields`);

      expect(this.element.querySelector('.file-not-analyzed-yet')).to.exist;
    });
  });

  describe('when there is a file with discovered fields', function () {
    beforeEach(function () {
      mapFile = server.testData.create.mapFile();
      mapFile.options = {
        uuid: '',
        destinationMap: '1',
        fields: [
          { key: 'isPublished', destination: '' },
          { key: 'name', destination: 'name', preview: ['name 1', 'name 2'] },
          { key: '_id', destination: '' },
          { key: 'maps ', destination: 'attributes' },
          { key: 'position.lon', destination: 'position.lon' },
          { key: 'description', destination: 'description' },
          { key: 'pictures', destination: 'pictures' },
          { key: 'icons ', destination: 'icons' },
          { key: 'attributes', destination: '' },
          { key: 'contributors ', destination: 'contributors' },
          { key: 'position.lat', destination: 'position.lat' },
        ],
      };
      server.testData.storage.addMapFile(mapFile);
      server.testData.storage.addDefaultMap('1');
    });

    it('renders the preview values', async function () {
      await visit(`/wizard/import/file/${mapFile._id}/fields`);

      const preview = this.element.querySelectorAll('.field-preview');

      expect(preview).to.have.length(2);
      expect(preview[0].textContent.trim()).to.equal('name 1');
      expect(preview[1].textContent.trim()).to.equal('name 2');
    });

    it('can change the fields matching', async function () {
      await visit(`/wizard/import/file/${mapFile._id}/fields`);

      const fieldMappings = this.element.querySelectorAll(
        '.feature-field-mapping'
      );

      const select = fieldMappings[0].querySelector('.form-select');
      select.value = '';
      await triggerEvent(select, 'change');

      await click('.btn-submit');

      await waitUntil(() => receivedFile);

      expect(receivedFile.mapFile.options).to.deep.equal({
        uuid: '',
        destinationMap: '1',
        extraIcons: [],
        fields: [
          { key: 'isPublished', destination: '' },
          { key: 'name', destination: 'name', preview: ['name 1', 'name 2'] },
          { key: '_id', destination: '' },
          { key: 'maps ', destination: 'attributes' },
          { key: 'position.lon', destination: 'position.lon' },
          { key: 'description', destination: 'description' },
          { key: 'pictures', destination: 'pictures' },
          { key: 'icons ', destination: 'icons' },
          { key: 'attributes', destination: '' },
          { key: 'contributors ', destination: 'contributors' },
          { key: 'position.lat', destination: 'position.lat' },
        ],
      });
    });

    it('can change the destination map', async function () {
      await visit(`/wizard/import/file/${mapFile._id}/fields`);

      this.element.querySelector('.import-destination-map').value =
        '5e2f42c96e290a010057dd95';
      await triggerEvent('.import-destination-map', 'change');

      await click('.btn-submit');
      await waitUntil(() => receivedFile);

      expect(receivedFile.mapFile.options.destinationMap).to.deep.equal(
        '5e2f42c96e290a010057dd95'
      );
    });

    it('can remove the destination map', async function () {
      await visit(`/wizard/import/file/${mapFile._id}/fields`);

      this.element.querySelector('.import-destination-map').value = 'undefined';
      await triggerEvent('.import-destination-map', 'change');

      await click('.btn-submit');
      await waitUntil(() => receivedFile);

      expect(receivedFile.mapFile.options.destinationMap).to.be.undefined;
    });

    it('can change the crs option', async function () {
      await visit(`/wizard/import/file/${mapFile._id}/fields`);

      await typeIn('.import-crs', 'some crs');

      await click('.btn-submit');
      await waitUntil(() => receivedFile);

      expect(receivedFile.mapFile.options.crs).to.eql('some crs');
    });

    it('can change the updateBy option', async function () {
      await visit(`/wizard/import/file/${mapFile._id}/fields`);

      this.element.querySelector('.import-update-by').value = '_id';
      await triggerEvent('.import-update-by', 'change');

      await click('.btn-submit');
      await waitUntil(() => receivedFile);

      expect(receivedFile.mapFile.options.updateBy).to.eql('_id');
    });

    it('can add extra icons', async function () {
      await visit(`/wizard/import/file/${mapFile._id}/fields`);

      await click('.icon-select');
      await click('.btn-icon');
      await click('.btn-success');

      await click('.btn-submit');
      await waitUntil(() => receivedFile);

      expect(receivedFile.mapFile.options.extraIcons).to.eql([
        '5ca7bfc0ecd8490100cab980',
      ]);
    });

    it('can remove the update by file', async function () {
      await visit(`/wizard/import/file/${mapFile._id}/fields`);

      this.element.querySelector('.import-update-by').value = '_id';
      await triggerEvent('.import-update-by', 'change');

      this.element.querySelector('.import-update-by').value = '-';
      await triggerEvent('.import-update-by', 'change');

      await click('.btn-submit');
      await waitUntil(() => receivedFile);

      expect(receivedFile.mapFile.options.updateBy).to.be.undefined;
    });
  });
});
