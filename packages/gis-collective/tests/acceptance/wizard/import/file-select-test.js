import { describe, it } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, triggerEvent } from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import { authenticateSession } from 'ember-simple-auth/test-support';
import click from '@ember/test-helpers/dom/click';
import waitUntil from '@ember/test-helpers/wait-until';

describe('Acceptance | wizard/import/file select', function () {
  setupApplicationTest();
  let server;
  let map;

  this.beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);

    map = server.testData.storage.addDefaultMap();

    server.testData.storage.addDefaultMap('5e2f42c96e290a010057dd95');

    authenticateSession();
  });

  it('has a disabled upload button by default', async function () {
    await visit(`/wizard/import/file/${map._id}/select`);

    expect(this.element.querySelector('.btn-upload')).to.have.attribute(
      'disabled',
      ''
    );

    expect(currentURL()).to.equal(`/wizard/import/file/${map._id}/select`);
  });

  it('has an enabled upload button when a file is selected', async function () {
    await visit(`/wizard/import/file/${map._id}/select`);

    const blob = server.testData.create.pngBlob();

    await triggerEvent("[type='file']", 'change', { files: [blob] });

    expect(this.element.querySelector('.btn-upload')).to.have.attribute(
      'disabled',
      null
    );

    expect(currentURL()).to.equal(`/wizard/import/file/${map._id}/select`);
  });

  it('uploads the file and navigates to the next step when the upload file is pressed', async function () {
    let receivedFile;
    let mapFile = server.testData.create.mapFile();
    server.testData.storage.addMapFile(mapFile);

    server.get(`/mock-server/mapfiles/${mapFile._id}/meta`, () => {
      return [200, { 'Content-Type': 'application/json' }, JSON.stringify({})];
    });
    server.post(
      `/mock-server/maps/5ca89e37ef1f7e010007f54c/files`,
      (request) => {
        receivedFile = JSON.parse(request.requestBody);
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({ file: mapFile._id }),
        ];
      }
    );

    await visit(`/wizard/import/file/${map._id}/select`);

    const blob = server.testData.create.pngBlob();

    await waitUntil(() => this.element.querySelector("[type='file']"));
    await triggerEvent("[type='file']", 'change', { files: [blob] });

    await waitUntil(
      () =>
        !this.element
          .querySelector('.btn-upload')
          .attributes.getNamedItem('disabled')
    );

    await click('.btn-upload');
    await waitUntil(() => receivedFile);

    expect(receivedFile.name).to.equal('foobar.png');
    expect(receivedFile.contentType).to.equal('image/png');
    expect(receivedFile.size).to.equal(1048576);

    await waitUntil(() => currentURL().indexOf('select') == -1);

    expect(currentURL()).to.equal(
      `/wizard/import/file/5e3bf85528c8144486996173/fields`
    );
  });
});
