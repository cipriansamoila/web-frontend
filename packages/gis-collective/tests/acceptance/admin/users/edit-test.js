import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  waitFor,
  click,
  fillIn,
  triggerEvent,
  waitUntil,
} from '@ember/test-helpers';
import { authenticateSession } from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';

describe('Acceptance | admin/users/edit', function () {
  setupApplicationTest();
  let server;
  let receivedProfile;
  let receivedUser;
  let profile;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultProfile('me');
    profile = server.testData.storage.addDefaultProfile(
      '5ca90cc982ff92d26a7ef906'
    );
    server.testData.storage.addDefaultPicture();

    receivedProfile = null;
    receivedUser = null;

    server.put(
      '/mock-server/userprofiles/5ca90cc982ff92d26a7ef906',
      (request) => {
        receivedProfile = JSON.parse(request.requestBody);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedProfile),
        ];
      }
    );

    server.put('/mock-server/users/5ca90cc982ff92d26a7ef906', (request) => {
      receivedUser = JSON.parse(request.requestBody);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedUser),
      ];
    });
  });

  afterEach(function () {
    server.shutdown();
  });

  describe('Accessing the route', function () {
    beforeEach(function () {
      server.testData.storage.addUser('5ca90cc982ff92d26a7ef906', {
        _id: '5ca90cc982ff92d26a7ef906',
        email: 'besttman@yandex.ru',
        username: 'besttman',
        isAdmin: false,
        name: 'Besttman',
      });
    });

    it('should redirect to index if the user is not an admin', async function () {
      server.testData.storage.addDefaultUser(false);
      authenticateSession();

      await visit('/admin/users/5ca90cc982ff92d26a7ef906');
      expect(currentURL()).to.equal('/browse/maps/_/map-view');
    });

    it('should redirect to index if the user is not authenticated', async function () {
      await visit('/admin/users/5ca90cc982ff92d26a7ef906');
      expect(currentURL()).to.equal('/browse/maps/_/map-view');
    });

    it('should let administrators visit /admin/users/:id', async function () {
      authenticateSession();

      await visit('/admin/users/5ca90cc982ff92d26a7ef906');
      expect(currentURL()).to.equal('/admin/users/5ca90cc982ff92d26a7ef906');

      expect(this.element.querySelector('.main-container > .error')).not.to
        .exist;
    });
  });

  describe('for an normal user', function () {
    beforeEach(function () {
      server.testData.storage.addUser('5ca90cc982ff92d26a7ef906', {
        _id: '5ca90cc982ff92d26a7ef906',
        email: 'besttman@yandex.ru',
        username: 'besttman',
        isAdmin: false,
        name: 'Besttman',
      });
    });

    describe('when the simple location is preferred', function () {
      it('should fill the values in the public profile', async function () {
        authenticateSession();

        await visit('/admin/users/5ca90cc982ff92d26a7ef906');
        expect(currentURL()).to.equal('/admin/users/5ca90cc982ff92d26a7ef906');

        await waitFor('.profile-group');

        let values = {};

        [...this.element.querySelectorAll('.profile-group input')].forEach(
          (element) => {
            if (
              element.attributes.getNamedItem('id') &&
              element.attributes.getNamedItem('type').value != 'checkbox'
            ) {
              values[element.attributes.getNamedItem('id').value] =
                element.value;
            }
          }
        );

        values = JSON.parse(JSON.stringify(values));

        expect(values).to.deep.contain({
          InputStatusMessage: 'Hello',
          'status-emoji': 'earth_africa',
          InputSkype: 'skype_id',
          InputLinkedin: 'szabobogdan',
          InputTwitter: 'szabobogdan1',
          InputWebsite: 'https://szabobogdan.com',
          InputLocation: 'Berlin',
          InputJobTitle: 'Developer',
          InputOrganization: 'GISCollective',
          InputEmail: 'besttman@yandex.ru',
          InputUserName: 'besttman',
        });
      });

      it('should not disable the user account fields', async function () {
        authenticateSession();

        await visit('/admin/users/5ca90cc982ff92d26a7ef906');
        await waitFor('.profile-group');

        expect(
          this.element.querySelectorAll('input[disabled], select[disabled]')
        ).not.to.exist;
      });

      it('should update the profile values if they are changed by the user', async function () {
        authenticateSession();

        await visit('/admin/users/5ca90cc982ff92d26a7ef906');
        expect(currentURL()).to.equal('/admin/users/5ca90cc982ff92d26a7ef906');

        await waitFor('#InputStatusMessage');

        await fillIn('#InputStatusMessage', 'new status message');

        await click('.btn-profile-update');

        expect(receivedProfile).to.deep.equal({
          userProfile: {
            joinedTime: '2010-01-01T02:30:00.000Z',
            statusEmoji: 'earth_africa',
            statusMessage: 'new status message',
            userName: 'some_username',
            salutation: 'Dr.',
            title: 'mr',
            firstName: 'Bogdan',
            lastName: 'Szabo',
            skype: 'skype_id',
            linkedin: 'szabobogdan',
            twitter: 'szabobogdan1',
            website: 'https://szabobogdan.com',
            location: {
              isDetailed: false,
              simple: 'Berlin',
              detailedLocation: {
                city: '',
                country: '',
                postalCode: '',
                province: '',
              },
            },
            jobTitle: 'Developer',
            organization: 'GISCollective',
            bio: 'This is me',
            showCalendarContributions: true,
            showPrivateContributions: false,
            showWelcomePresentation: false,
            picture: '5cc8dc1038e882010061545a',
          },
        });
      });

      it('should update the joined time value', async function () {
        authenticateSession();

        await visit('/admin/users/5ca90cc982ff92d26a7ef906');
        await click('.account-info-group .form-switch');
        this.element.querySelector('.month').value = 4;
        await triggerEvent('.month', 'change');

        await click('.btn-account-update');
        await waitUntil(() => receivedProfile);

        expect(receivedProfile.userProfile.joinedTime).to.deep.equal(
          '2010-05-01T01:30:00.000Z'
        );
      });

      it('should update the email and username', async function () {
        authenticateSession();

        await visit('/admin/users/5ca90cc982ff92d26a7ef906');

        await fillIn('#InputEmail', 'new@email.com');
        await fillIn('#InputUserName', 'new user');

        await click('.btn-account-update');
        await waitUntil(() => receivedUser);

        expect(receivedUser).to.deep.equal({
          user: {
            lastActivity: null,
            email: 'new@email.com',
            username: 'new user',
            isAdmin: false,
          },
        });
      });

      it('should offer to promote a regular user to admin', async function () {
        this.timeout(20000);
        authenticateSession();

        await visit('/admin/users/5ca90cc982ff92d26a7ef906');
        expect(currentURL()).to.equal('/admin/users/5ca90cc982ff92d26a7ef906');

        expect(
          this.element.querySelector('.role-text').textContent.trim()
        ).to.equal('mr Bogdan Szabo is a regular user.');
        expect(
          this.element.querySelector('.btn-user-rights').textContent.trim()
        ).to.equal('Promote to admin');

        await click('.btn-user-rights');
        await Modal.waitToDisplay();

        expect(
          document.querySelector('.modal-title').textContent.trim()
        ).to.equal('promote user');
        expect(
          document.querySelector('.modal-body').textContent.trim()
        ).to.contain(
          'Are you sure you want to promote the user `besttman@yandex.ru`?'
        );

        await click('.btn-reject');
        await Modal.waitToHide();
      });

      it('should not offer to downgrade the authenticated account', async function () {
        authenticateSession();

        await visit('/admin/users/me');
        expect(currentURL()).to.equal('/admin/users/me');

        expect(
          this.element.querySelector('.role-text').textContent.trim()
        ).to.equal('mr Bogdan Szabo is an administrator.');

        expect(this.element.querySelector('.btn-user-rights')).not.to.exist;
        expect(
          this.element.querySelector('.downgrade-message').textContent.trim()
        ).to.equal(
          "You can't remove your account, because you are an administrator. Ask another administrator to remove this account, or to remove your administrator rights."
        );
      });

      it('should not offer to delete the authenticated account', async function () {
        authenticateSession();

        await visit('/admin/users/me');
        expect(currentURL()).to.equal('/admin/users/me');

        expect(this.element.querySelector('.btn-delete-account')).not.to.exist;
        expect(
          this.element.querySelector('.delete-message').textContent.trim()
        ).to.equal(
          "You can't remove your account, because you are an administrator. Ask another administrator to remove this account, or to remove your administrator rights."
        );
      });

      it('should offer to delete a regular user', async function () {
        this.timeout(20000);
        authenticateSession();

        await visit('/admin/users/5ca90cc982ff92d26a7ef906');
        expect(currentURL()).to.equal('/admin/users/5ca90cc982ff92d26a7ef906');

        await click('.btn-delete-account');
        await Modal.waitToDisplay();

        expect(
          document.querySelector('.modal-title').textContent.trim()
        ).to.equal('delete account');
        expect(
          document.querySelector('.modal-body').textContent.trim()
        ).to.contain('Are you sure you want to delete this account?');

        await click('.btn-reject');
        await Modal.waitToHide();
      });
    });

    describe('when the detailed location is preferred', function () {
      beforeEach(function () {
        server.testData.storage.addPreference(
          'profile.detailedLocation',
          'true'
        );
        profile['location'] = {
          isDetailed: true,
          detailedLocation: {
            country: 'country',
            province: 'province',
            city: 'city',
            postalCode: 'postalCode',
          },
        };
      });

      it('should fill the location fields', async function () {
        authenticateSession();

        await visit('/admin/users/5ca90cc982ff92d26a7ef906');
        expect(currentURL()).to.equal('/admin/users/5ca90cc982ff92d26a7ef906');

        await waitFor('.profile-group');

        expect(this.element.querySelector('.input-country').value).to.equal(
          'country'
        );
        expect(this.element.querySelector('.input-province').value).to.equal(
          'province'
        );
        expect(this.element.querySelector('.input-city').value).to.equal(
          'city'
        );
        expect(this.element.querySelector('.input-postalCode').value).to.equal(
          'postalCode'
        );
      });

      it('should update the location fields', async function () {
        authenticateSession();

        await visit('/admin/users/5ca90cc982ff92d26a7ef906');
        expect(currentURL()).to.equal('/admin/users/5ca90cc982ff92d26a7ef906');

        await waitFor('.profile-group');

        await fillIn('.input-country', 'new country');
        await fillIn('.input-province', 'new province');
        await fillIn('.input-city', 'new city');
        await fillIn('.input-postalCode', 'new postalCode');

        await click('.btn-profile-update');

        expect(receivedProfile.userProfile.location).to.deep.equal({
          detailedLocation: {
            city: 'new city',
            country: 'new country',
            postalCode: 'new postalCode',
            province: 'new province',
          },
          isDetailed: true,
          simple: 'new country, new province, new city, new postalCode',
        });
      });
    });
  });

  describe('for an admin user', function () {
    beforeEach(function () {
      server.testData.storage.addUser('5ca90cc982ff92d26a7ef906', {
        _id: '5ca90cc982ff92d26a7ef906',
        email: 'besttman@yandex.ru',
        username: 'besttman',
        isAdmin: true,
        name: 'Besttman',
      });
    });

    it('should offer to downgrade an admin', async function () {
      this.timeout(20000);
      authenticateSession();

      await visit('/admin/users/5ca90cc982ff92d26a7ef906');
      expect(currentURL()).to.equal('/admin/users/5ca90cc982ff92d26a7ef906');

      expect(
        this.element.querySelector('.role-text').textContent.trim()
      ).to.equal('mr Bogdan Szabo is an administrator.');
      expect(
        this.element.querySelector('.btn-user-rights').textContent.trim()
      ).to.equal('Downgrade to regular user');

      await click('.btn-user-rights');
      await Modal.waitToDisplay();

      expect(
        document.querySelector('.modal-title').textContent.trim()
      ).to.equal('downgrade user');
      expect(
        document.querySelector('.modal-body').textContent.trim()
      ).to.contain('Are you sure you want to downgrade `besttman@yandex.ru`?');

      await click('.btn-reject');
      await Modal.waitToHide();
    });
  });
});
