import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  fillIn,
  triggerEvent,
  click,
} from '@ember/test-helpers';
import { authenticateSession } from 'ember-simple-auth/test-support';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';

describe('Acceptance | admin/smtp', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('Should redirect to index if the user is not an admin', async function () {
    server.testData.storage.addDefaultUser(false);
    authenticateSession();

    await visit('/admin/smtp');
    expect(currentURL()).to.equal('/browse/maps/_/map-view');
  });

  it('Should redirect to index if the user is not authenticated', async function () {
    await visit('/admin/smtp');
    expect(currentURL()).to.equal('/browse/maps/_/map-view');
  });

  it('Should let administrators visit /admin/smtp', async function () {
    authenticateSession();

    server.testData.storage.addPreference('secret.smtp.authType', '');
    server.testData.storage.addPreference('secret.smtp.connectionType', '');
    server.testData.storage.addPreference('secret.smtp.tlsValidationMode', '');
    server.testData.storage.addPreference('secret.smtp.tlsVersion', '');
    server.testData.storage.addPreference('secret.smtp.host', '');
    server.testData.storage.addPreference('secret.smtp.port', '');
    server.testData.storage.addPreference('secret.smtp.password', '');
    server.testData.storage.addPreference('secret.smtp.username', '');
    server.testData.storage.addPreference('secret.smtp.from', '');

    await visit('/admin/smtp');
    expect(currentURL()).to.equal('/admin/smtp');

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;
  });

  it('The save button should be disabled by default', async function () {
    authenticateSession();

    server.testData.storage.addPreference('secret.smtp.authType', '');
    server.testData.storage.addPreference('secret.smtp.connectionType', '');
    server.testData.storage.addPreference('secret.smtp.tlsValidationMode', '');
    server.testData.storage.addPreference('secret.smtp.tlsVersion', '');
    server.testData.storage.addPreference('secret.smtp.host', '');
    server.testData.storage.addPreference('secret.smtp.port', '');
    server.testData.storage.addPreference('secret.smtp.password', '');
    server.testData.storage.addPreference('secret.smtp.username', '');
    server.testData.storage.addPreference('secret.smtp.from', '');

    await visit('/admin/smtp');
    expect(currentURL()).to.equal('/admin/smtp');

    expect(this.element.querySelector('.btn.btn-primary')).to.have.attribute(
      'disabled',
      ''
    );
  });

  it('The inputs should have the server values', async function () {
    authenticateSession();

    server.testData.storage.addPreference('secret.smtp.authType', 'login');
    server.testData.storage.addPreference('secret.smtp.connectionType', 'tls');
    server.testData.storage.addPreference(
      'secret.smtp.tlsValidationMode',
      'checkTrust'
    );
    server.testData.storage.addPreference('secret.smtp.tlsVersion', 'ssl3');
    server.testData.storage.addPreference('secret.smtp.host', 'test.host');
    server.testData.storage.addPreference('secret.smtp.port', '213');
    server.testData.storage.addPreference('secret.smtp.password', 'secret');
    server.testData.storage.addPreference('secret.smtp.username', 'me');
    server.testData.storage.addPreference('secret.smtp.from', 'test@gmail.com');

    await visit('/admin/smtp');
    expect(currentURL()).to.equal('/admin/smtp');

    expect(this.element.querySelector('.input-smtp-auth-type').value).to.equal(
      'login'
    );
    expect(
      this.element.querySelector('.input-smtp-connection-type').value
    ).to.equal('tls');
    expect(
      this.element.querySelector('.input-smtp-tls-validation-mode').value
    ).to.equal('checkTrust');
    expect(
      this.element.querySelector('.input-smtp-tls-version').value
    ).to.equal('ssl3');
    expect(this.element.querySelector('.input-smtp-host').value).to.equal(
      'test.host'
    );
    expect(this.element.querySelector('.input-smtp-port').value).to.equal(
      '213'
    );
    expect(this.element.querySelector('.input-smtp-password').value).to.equal(
      'secret'
    );
    expect(this.element.querySelector('.input-smtp-username').value).to.equal(
      'me'
    );
    expect(this.element.querySelector('.input-smtp-from').value).to.equal(
      'test@gmail.com'
    );
  });

  it('triggers a test email when the test button is pressed', async function () {
    let called;
    server.post(
      '/mock-server/users/5b870669796da25424540deb/testNotification',
      () => {
        called = true;
        return [200, { 'Content-Type': 'application/json' }];
      }
    );

    authenticateSession();

    server.testData.storage.addPreference('secret.smtp.authType', 'login');
    server.testData.storage.addPreference('secret.smtp.connectionType', 'tls');
    server.testData.storage.addPreference(
      'secret.smtp.tlsValidationMode',
      'checkTrust'
    );
    server.testData.storage.addPreference('secret.smtp.tlsVersion', 'ssl3');
    server.testData.storage.addPreference('secret.smtp.host', 'test.host');
    server.testData.storage.addPreference('secret.smtp.port', '213');
    server.testData.storage.addPreference('secret.smtp.password', 'secret');
    server.testData.storage.addPreference('secret.smtp.username', 'me');
    server.testData.storage.addPreference('secret.smtp.from', 'test@gmail.com');

    await visit('/admin/smtp');

    await click('.btn-test-message');

    expect(called).to.be.true;
  });

  describe('failing put request', function () {
    beforeEach(function () {
      server.put('/mock-server/preferences/:id', () => {
        return [
          400,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            errors: [
              {
                title: 'Some error',
                description: 'some message',
                status: 400,
              },
            ],
          }),
        ];
      });

      server.testData.storage.addPreference('secret.smtp.authType', '');
      server.testData.storage.addPreference('secret.smtp.connectionType', '');
      server.testData.storage.addPreference(
        'secret.smtp.tlsValidationMode',
        ''
      );
      server.testData.storage.addPreference('secret.smtp.tlsVersion', '');
      server.testData.storage.addPreference('secret.smtp.host', '');
      server.testData.storage.addPreference('secret.smtp.port', '');
      server.testData.storage.addPreference('secret.smtp.password', '');
      server.testData.storage.addPreference('secret.smtp.username', '');
      server.testData.storage.addPreference(
        'secret.smtp.from',
        'test@gmail.com'
      );
    });

    it('Should show a modal with the error', async function () {
      this.timeout(20000);
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      await fillIn('.input-smtp-host', 'new value');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));
      await Modal.waitToDisplay(4000);

      expect(
        document.querySelector('.modal-title').textContent.trim()
      ).to.equal('Some error');
      expect(document.querySelector('.modal-body').textContent.trim()).to.equal(
        'some message'
      );

      await click('.btn-resolve');
      await Modal.waitToHide(4000);
    });
  });

  describe('successful put request', function () {
    let receivedPreference;

    beforeEach(function () {
      server.put('/mock-server/preferences/:id', (request) => {
        receivedPreference = JSON.parse(request.requestBody);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedPreference),
        ];
      });

      server.testData.storage.addPreference('secret.smtp.authType', '');
      server.testData.storage.addPreference('secret.smtp.connectionType', '');
      server.testData.storage.addPreference(
        'secret.smtp.tlsValidationMode',
        ''
      );
      server.testData.storage.addPreference('secret.smtp.tlsVersion', '');
      server.testData.storage.addPreference('secret.smtp.host', '');
      server.testData.storage.addPreference('secret.smtp.port', '');
      server.testData.storage.addPreference('secret.smtp.password', '');
      server.testData.storage.addPreference('secret.smtp.username', '');
      server.testData.storage.addPreference('secret.smtp.from', '');
    });

    it('Changing the host should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      await fillIn('.input-smtp-host', 'new value');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.smtp.host', value: 'new value' },
      });
    });

    it('Changing the port should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      await fillIn('.input-smtp-port', '42');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.smtp.port', value: '42' },
      });
    });

    it('Changing the password should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      await fillIn('.input-smtp-password', 'new value');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.smtp.password', value: 'new value' },
      });
    });

    it('Changing the username should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      await fillIn('.input-smtp-username', 'new value');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.smtp.username', value: 'new value' },
      });
    });

    it('Changing the from field should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      await fillIn('.input-smtp-from', 'new_value@gmail.com');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.smtp.from', value: 'new_value@gmail.com' },
      });
    });

    it('Changing the auth-type value should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      this.element.querySelector('.input-smtp-auth-type').value = 'plain';
      await triggerEvent('.input-smtp-auth-type', 'change');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.smtp.authType', value: 'plain' },
      });
    });

    it('Changing the connection-type value should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      this.element.querySelector('.input-smtp-connection-type').value = 'plain';
      await triggerEvent('.input-smtp-connection-type', 'change');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.smtp.connectionType', value: 'plain' },
      });
    });

    it('Changing the tls-validation-mode value should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      this.element.querySelector('.input-smtp-tls-validation-mode').value =
        'checkCert';
      await triggerEvent('.input-smtp-tls-validation-mode', 'change');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: {
          name: 'secret.smtp.tlsValidationMode',
          value: 'checkCert',
        },
      });
    });

    it('Changing the smtp-tls-version value should enable the save', async function () {
      authenticateSession();

      await visit('/admin/smtp');
      expect(currentURL()).to.equal('/admin/smtp');

      this.element.querySelector('.input-smtp-tls-version').value = 'any';
      await triggerEvent('.input-smtp-tls-version', 'change');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.smtp.tlsVersion', value: 'any' },
      });
    });
  });
});
