import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, click, waitUntil, currentURL } from '@ember/test-helpers';
import { authenticateSession } from 'ember-simple-auth/test-support';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';

describe('Acceptance | admin/access', function () {
  setupApplicationTest();
  let server;
  let receivedPreference;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();

    server.put('/mock-server/preferences/:id', (request) => {
      receivedPreference = JSON.parse(request.requestBody);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedPreference),
      ];
    });
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should be redirect to index when the user is not authenticated', async function () {
    await visit('/admin/access');
    expect(currentURL()).to.equal('/browse/maps/_/map-view');
  });

  describe('an authenticated user', function () {
    beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultUser(false);
    });

    it('should be redirect to index', async function () {
      await visit('/admin/access');
      expect(currentURL()).to.equal('/browse/maps/_/map-view');
    });
  });

  describe('an authenticated admin', function () {
    beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultUser(true);
    });

    it('is able to disable the `Propose a site` form', async function () {
      authenticateSession();

      await visit('/admin/access');

      await click('.allow-proposing-sites');
      await click('.btn-submit');

      await waitUntil(() => receivedPreference);

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'access.allowProposingSites', value: 'false' },
      });
    });

    it('is able to disable the Manage section', async function () {
      authenticateSession();

      await visit('/admin/access');

      await click('.allow-manage-without-teams');
      await click('.btn-submit');

      await waitUntil(() => receivedPreference);

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'access.allowManageWithoutTeams', value: 'false' },
      });
    });

    it('is able to disable the multi project mode', async function () {
      authenticateSession();

      await visit('/admin/access');

      await click('.is-multi-project-mode');
      await click('.btn-submit');

      await waitUntil(() => receivedPreference);

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'access.isMultiProjectMode', value: 'false' },
      });
    });

    it('renders a disable submit button by default', async function () {
      authenticateSession();

      await visit('/admin/access');
      expect(this.element.querySelector('.btn-submit')).to.have.attribute(
        'disabled',
        ''
      );
    });
  });
});
