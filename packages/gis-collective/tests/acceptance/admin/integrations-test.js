import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, fillIn, click } from '@ember/test-helpers';
import { authenticateSession } from 'ember-simple-auth/test-support';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';

describe('Acceptance | admin/integrations', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to index if the user is not an admin', async function () {
    server.testData.storage.addDefaultUser(false);
    authenticateSession();

    await visit('/admin/integrations');
    expect(currentURL()).to.equal('/browse/maps/_/map-view');
  });

  it('should redirect to index if the user is not authenticated', async function () {
    await visit('/admin/integrations');
    expect(currentURL()).to.equal('/browse/maps/_/map-view');
  });

  it('should show a disabled submit button', async function () {
    authenticateSession();

    server.testData.storage.addPreference('integrations.nominatim', '');
    server.testData.storage.addPreference(
      'integrations.matomo.url',
      'matomo-url'
    );
    server.testData.storage.addPreference(
      'integrations.matomo.siteId',
      'matomo-site-id'
    );

    await visit('/admin/integrations');

    expect(currentURL()).to.equal('/admin/integrations');

    expect(this.element.querySelector('.btn.btn-primary')).to.have.attribute(
      'disabled',
      ''
    );
  });

  it('should have the server values', async function () {
    authenticateSession();

    server.testData.storage.addPreference('integrations.nominatim', 'some-key');
    server.testData.storage.addPreference(
      'integrations.matomo.url',
      'matomo-url'
    );
    server.testData.storage.addPreference(
      'integrations.matomo.siteId',
      'matomo-site-id'
    );

    await visit('/admin/integrations');
    expect(currentURL()).to.equal('/admin/integrations');

    expect(this.element.querySelector('.input-nominatim').value).to.equal(
      'some-key'
    );
    expect(this.element.querySelector('.input-matomo-url').value).to.equal(
      'matomo-url'
    );
    expect(this.element.querySelector('.input-matomo-site-id').value).to.equal(
      'matomo-site-id'
    );
  });

  describe('successful put request', function () {
    let receivedPreferences = [];

    beforeEach(function () {
      receivedPreferences = [];

      server.put('/mock-server/preferences/:id', (request) => {
        receivedPreferences.push(JSON.parse(request.requestBody));

        return [
          200,
          { 'Content-Type': 'application/json' },
          request.requestBody,
        ];
      });

      server.testData.storage.addPreference(
        'integrations.nominatim',
        'some-key'
      );
      server.testData.storage.addPreference(
        'integrations.matomo.url',
        'matomo-url'
      );
      server.testData.storage.addPreference(
        'integrations.matomo.siteId',
        'matomo-site-id'
      );
    });

    it('updates the values on submit', async function () {
      authenticateSession();

      await visit('/admin/integrations');

      await fillIn('.input-nominatim', 'new value 1');
      await fillIn('.input-matomo-url', 'new value 2');
      await fillIn('.input-matomo-site-id', 'new value 3');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreferences).to.deep.contain({
        preference: { name: 'integrations.nominatim', value: 'new value 1' },
      });

      expect(receivedPreferences).to.deep.contain({
        preference: { name: 'integrations.matomo.url', value: 'new value 2' },
      });

      expect(receivedPreferences).to.deep.contain({
        preference: {
          name: 'integrations.matomo.siteId',
          value: 'new value 3',
        },
      });
    });
  });
});
