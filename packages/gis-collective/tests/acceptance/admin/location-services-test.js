import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, triggerEvent, click } from '@ember/test-helpers';
import { authenticateSession } from 'ember-simple-auth/test-support';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';

describe('Acceptance | admin/location-services', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('Should redirect to index if the user is not an admin', async function () {
    server.testData.storage.addDefaultUser(false);
    authenticateSession();

    await visit('/admin/location-services');
    expect(currentURL()).to.equal('/browse/maps/_/map-view');
  });

  it('Should redirect to index if the user is not authenticated', async function () {
    await visit('/admin/location-services');
    expect(currentURL()).to.equal('/browse/maps/_/map-view');
  });

  it('Should let administrators visit /admin/location-services', async function () {
    authenticateSession();
    server.testData.storage.addPreference(
      'locationServices.maskingPrecision',
      ''
    );

    await visit('/admin/location-services');

    expect(currentURL()).to.equal('/admin/location-services');

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;
  });

  it('should have a disabled submit button', async function () {
    authenticateSession();
    server.testData.storage.addPreference(
      'locationServices.maskingPrecision',
      ''
    );

    await visit('/admin/location-services');

    expect(this.element.querySelector('.btn.btn-primary')).to.have.attribute(
      'disabled',
      ''
    );
  });

  it('should have the server values', async function () {
    authenticateSession();

    server.testData.storage.addPreference(
      'locationServices.maskingPrecision',
      '0'
    );

    await visit('/admin/location-services');
    expect(currentURL()).to.equal('/admin/location-services');

    expect(
      this.element.querySelector('.input-masking-precision').value
    ).to.equal('0');
  });

  describe('failing put request', function () {
    beforeEach(function () {
      server.put('/mock-server/preferences/:id', () => {
        return [
          400,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            errors: [
              {
                title: 'Some error',
                description: 'some message',
                status: 400,
              },
            ],
          }),
        ];
      });

      server.testData.storage.addPreference(
        'locationServices.maskingPrecision',
        ''
      );
    });

    it('should show a modal with the error', async function () {
      this.timeout(20000);
      authenticateSession();

      await visit('/admin/location-services');
      expect(currentURL()).to.equal('/admin/location-services');

      this.element.querySelector('select').value = '1';
      await triggerEvent('select', 'change');

      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));
      await Modal.waitToDisplay(4000);

      expect(
        document.querySelector('.modal-title').textContent.trim()
      ).to.equal('Some error');
      expect(document.querySelector('.modal-body').textContent.trim()).to.equal(
        'some message'
      );

      await click('.btn-resolve');
      await Modal.waitToHide(4000);
    });
  });

  describe('successful put request', function () {
    let receivedPreference;

    beforeEach(function () {
      server.put('/mock-server/preferences/:id', (request) => {
        receivedPreference = JSON.parse(request.requestBody);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedPreference),
        ];
      });

      server.testData.storage.addPreference(
        'locationServices.maskingPrecision',
        ''
      );
    });

    it('Changing the masking precision should enable the save', async function () {
      authenticateSession();

      await visit('/admin/location-services');
      expect(currentURL()).to.equal('/admin/location-services');

      this.element.querySelector('select').value = '2';
      await triggerEvent('select', 'change');

      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'locationServices.maskingPrecision', value: '2' },
      });
    });
  });
});
