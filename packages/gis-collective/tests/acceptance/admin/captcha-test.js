import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  fillIn,
  triggerEvent,
  click,
} from '@ember/test-helpers';
import { authenticateSession } from 'ember-simple-auth/test-support';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';

describe('Acceptance | admin/captcha', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);

    server.testData.storage.addPreference(
      'secret.recaptcha.siteKey',
      'site key value'
    );
    server.testData.storage.addPreference(
      'secret.recaptcha.secretKey',
      'secret key value'
    );
    server.testData.storage.addPreference(
      'secret.mtcaptcha.siteKey',
      'site key value'
    );
    server.testData.storage.addPreference(
      'secret.mtcaptcha.privateKey',
      'private key value'
    );
  });

  afterEach(async function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to index if the user is not an admin', async function () {
    server.testData.storage.addDefaultUser(false);
    authenticateSession();

    await visit('/admin/captcha');
    expect(currentURL()).to.equal('/browse/maps/_/map-view');
  });

  it('should redirect to index if the user is not authenticated', async function () {
    await visit('/admin/captcha');
    expect(currentURL()).to.equal('/browse/maps/_/map-view');
  });

  it('should let administrators visit /admin/captcha', async function () {
    authenticateSession();

    server.testData.storage.addPreference('captcha.enabled', 'recaptcha');

    await visit('/admin/captcha');
    expect(currentURL()).to.equal('/admin/captcha');

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;
  });

  it('the save button should be disabled by default', async function () {
    authenticateSession();

    server.testData.storage.addPreference('captcha.enabled', 'reCAPTCHA');

    await visit('/admin/captcha');
    expect(currentURL()).to.equal('/admin/captcha');

    expect(this.element.querySelector('.btn.btn-primary')).to.have.class(
      'disabled'
    );
    expect(this.element.querySelector('.btn.btn-primary')).to.have.attribute(
      'disabled',
      ''
    );
  });

  it('the inputs should have the recaptcha server values', async function () {
    authenticateSession();

    server.testData.storage.addPreference('captcha.enabled', 'reCAPTCHA');

    await visit('/admin/captcha');
    expect(currentURL()).to.equal('/admin/captcha');

    expect(this.element.querySelector('.input-captcha-enabled').value).to.equal(
      'reCAPTCHA'
    );
    expect(
      this.element.querySelector('.input-recaptcha-site-key').value
    ).to.equal('site key value');
    expect(
      this.element.querySelector('.input-recaptcha-secret-key').value
    ).to.equal('secret key value');

    expect(this.element.querySelector('.input-mtcaptcha-site-key')).not.to
      .exist;
    expect(this.element.querySelector('.input-mtcaptcha-private-key')).not.to
      .exist;
  });

  it('the inputs should have the mtcaptcha server values', async function () {
    authenticateSession();

    server.testData.storage.addPreference('captcha.enabled', 'mtCAPTCHA');

    await visit('/admin/captcha');
    expect(currentURL()).to.equal('/admin/captcha');

    expect(this.element.querySelector('.input-captcha-enabled').value).to.equal(
      'mtCAPTCHA'
    );
    expect(this.element.querySelector('.input-recaptcha-site-key')).not.to
      .exist;
    expect(this.element.querySelector('.input-recaptcha-secret-key')).not.to
      .exist;

    expect(
      this.element.querySelector('.input-mtcaptcha-site-key').value
    ).to.equal('site key value');
    expect(
      this.element.querySelector('.input-mtcaptcha-private-key').value
    ).to.equal('private key value');
  });

  describe('failing put request', function () {
    beforeEach(function () {
      server.put('/mock-server/preferences/:id', () => {
        return [
          400,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            errors: [
              {
                title: 'Some error',
                description: 'some message',
                status: 400,
              },
            ],
          }),
        ];
      });
    });

    it('Should show a modal with the error', async function () {
      authenticateSession();
      this.timeout(100000000);
      server.testData.storage.addPreference('captcha.enabled', 'reCAPTCHA');

      await visit('/admin/captcha');
      expect(currentURL()).to.equal('/admin/captcha');

      await fillIn('.input-recaptcha-site-key', 'new value');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      await Modal.waitToDisplay();

      expect(
        document.querySelector('.modal-title').textContent.trim()
      ).to.equal('Some error');
      expect(document.querySelector('.modal-body').textContent.trim()).to.equal(
        'some message'
      );

      await click('.modal-footer .btn-resolve');
      await Modal.waitToHide(4000);
    });
  });

  describe('successful put request', function () {
    let receivedPreference;

    beforeEach(function () {
      server.put('/mock-server/preferences/:id', (request) => {
        receivedPreference = JSON.parse(request.requestBody);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedPreference),
        ];
      });
    });

    it('Changing the site key should enable the save', async function () {
      authenticateSession();

      server.testData.storage.addPreference('captcha.enabled', 'reCAPTCHA');

      await visit('/admin/captcha');
      expect(currentURL()).to.equal('/admin/captcha');

      await fillIn('.input-recaptcha-site-key', 'new value');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.recaptcha.siteKey', value: 'new value' },
      });
    });

    it('Changing the secret key should enable the save', async function () {
      authenticateSession();

      server.testData.storage.addPreference('captcha.enabled', 'reCAPTCHA');

      await visit('/admin/captcha');
      expect(currentURL()).to.equal('/admin/captcha');

      await fillIn('.input-recaptcha-secret-key', 'new value');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'secret.recaptcha.secretKey', value: 'new value' },
      });
    });

    it('Changing the enabled value should enable the save', async function () {
      authenticateSession();

      server.testData.storage.addPreference('captcha.enabled', 'reCAPTCHA');

      await visit('/admin/captcha');
      expect(currentURL()).to.equal('/admin/captcha');

      this.element.querySelector('.input-captcha-enabled').value = 'false';
      await triggerEvent('.input-captcha-enabled', 'change');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(receivedPreference).to.deep.equal({
        preference: { name: 'captcha.enabled', value: 'false' },
      });
    });
  });
});
