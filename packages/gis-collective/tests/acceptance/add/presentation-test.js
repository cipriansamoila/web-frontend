import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  fillIn,
  triggerEvent,
} from '@ember/test-helpers';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';
import PageElements from '../../helpers/page-elements';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';

describe('Acceptance | add/presentation', function () {
  setupApplicationTest();
  let server;
  let receivedPresentation;
  let team;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();

    receivedPresentation = null;
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token', async function () {
    invalidateSession();

    await visit(`/add/presentation`);
    expect(currentURL()).to.equal('/login?redirect=%2Fadd%2Fpresentation');
  });

  describe('when an user is authenticated and no team is available', function () {
    beforeEach(function () {
      server.testData.storage.addDefaultUser();
      server.testData.storage.addDefaultLayout();
    });

    it('can visit the presentation with an authenticated user', async function () {
      authenticateSession();

      await visit(`/add/presentation`);
      expect(currentURL()).to.equal('/add/presentation');

      expect(this.element.querySelector('h1').textContent.trim()).to.equal(
        'New presentation'
      );
      expect(this.element.querySelector('.breadcrumb')).not.to.exist;

      expect(
        this.element.querySelector('.row-name h3').textContent.trim()
      ).to.contain('Name');
      expect(this.element.querySelector('.row-name input.form-control')).to
        .exist;

      expect(
        this.element.querySelector('.row-slug h3').textContent.trim()
      ).to.contain('Slug');
      expect(this.element.querySelector('.row-slug input.form-control')).to
        .exist;

      expect(
        this.element.querySelector('.row-layout h3').textContent.trim()
      ).to.contain('Layout');
      expect(this.element.querySelector('.row-layout select.form-control')).to
        .exist;

      expect(
        this.element.querySelector('.row-team h3').textContent.trim()
      ).to.contain('Team');
      expect(this.element.querySelector('.row-team select')).to.not.exist;

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to
        .exist;
      expect(
        this.element.querySelector('.btn.btn-primary.btn-submit')
      ).to.have.attribute('disabled', '');

      expect(this.element.querySelector('.row-team .btn-add-team')).to.exist;
      expect(this.element.querySelector('.alert-danger .btn-add-team')).to
        .exist;
    });

    it('the button in the alert should redirect the user to the add a team form', async function () {
      authenticateSession();
      await visit(`/add/presentation`);
      await click('.alert-danger .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=add.presentation');
    });

    it('the button in the Team section should also redirect to the add a team form', async function () {
      authenticateSession();
      await visit(`/add/presentation`);
      await click('.row-team .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=add.presentation');
    });
  });

  describe('when an user is authenticated and a team is available', function () {
    let layout;

    beforeEach(function () {
      team = server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultUser();
      layout = server.testData.storage.addDefaultLayout();
    });

    it('can visit the presentation with an authenticated user', async function () {
      authenticateSession();

      await visit(`/add/presentation`);
      expect(currentURL()).to.equal('/add/presentation');

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to
        .exist;
      expect(
        this.element.querySelector('.btn.btn-primary.btn-submit')
      ).to.have.attribute('disabled', '');
    });

    describe('when the server request is successful', function () {
      let presentation;

      beforeEach(function () {
        team = server.testData.storage.addDefaultTeam();
        presentation = server.testData.storage.addDefaultPresentation();
        server.testData.storage.addDefaultTeam('000000000000000000000001');

        server.post('/mock-server/presentations', (request) => {
          receivedPresentation = JSON.parse(request.requestBody);
          receivedPresentation.presentation._id = presentation._id;

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedPresentation),
          ];
        });
      });

      it('redirects to the edit presentation after the request response is received', async function () {
        authenticateSession();

        await visit(`/add/presentation`);

        await fillIn('.input-name', 'new presentation name');
        await fillIn('.input-slug', 'new-slug');

        this.element.querySelector('.row-layout select').value = layout._id;
        await triggerEvent('.row-layout select', 'change');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(receivedPresentation.presentation.name).to.equal(
          'new presentation name'
        );
        expect(receivedPresentation.presentation.slug).to.equal('new-slug');
        expect(receivedPresentation.presentation.layout).to.equal(
          '000000000000000000000001'
        );
        expect(receivedPresentation.presentation.visibility).to.deep.equal({
          isPublic: false,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        });

        expect(currentURL()).to.equal(
          `/manage/presentations/${presentation._id}`
        );
      });
    });

    describe('when the server request fails', function () {
      beforeEach(function () {
        server.post('/mock-server/presentations', () => {
          return [
            400,
            { 'Content-Type': 'application/json' },
            JSON.stringify({
              errors: [
                {
                  description: 'The `presentation.slug` field is required.',
                  status: 400,
                  title: 'Validation error',
                },
              ],
            }),
          ];
        });
      });

      it('shows a modal with the error message', async function () {
        this.timeout(10000);
        authenticateSession();

        await visit(`/add/presentation`);

        await fillIn('.input-name', 'new presentation name');
        await fillIn('.input-slug', 'new-slug');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(currentURL()).to.equal(`/add/presentation`);

        await Modal.waitToDisplay();

        expect(Modal.title()).to.equal('Validation error');
        expect(Modal.message()).to.equal(
          'The `presentation.slug` field is required.'
        );
      });
    });
  });

  describe('when an admin user is authenticated', function () {
    beforeEach(() => {
      team = server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultUser(true);
      server.testData.storage.addDefaultLayout();
    });

    it('can visit the presentation with an authenticated user', async function () {
      authenticateSession();

      await visit(`/add/presentation`);
      expect(currentURL()).to.equal('/add/presentation');
    });

    describe('when the server request is successfully', function () {
      let presentation;

      beforeEach(function () {
        presentation = server.testData.storage.addDefaultPage();
        server.post('/mock-server/presentations', (request) => {
          receivedPresentation = JSON.parse(request.requestBody);
          receivedPresentation.presentation._id = presentation._id;

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedPresentation),
          ];
        });
      });

      it('queries the teams without the all query param', async function () {
        authenticateSession();

        await visit(`/add/presentation`);

        await fillIn('.input-name', 'new presentation name');
        await fillIn('.input-slug', 'new-slug');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        expect(server.history).to.contain('GET /mock-server/teams?edit=true');

        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(receivedPresentation.presentation.name).to.deep.equal(
          'new presentation name'
        );
        expect(currentURL()).to.equal(
          `/manage/presentations/${presentation._id}`
        );
      });

      it('queries the teams with the all query param after the admin button is pressed', async function () {
        authenticateSession();

        await visit(`/add/presentation`);
        expect(server.history).to.contain('GET /mock-server/teams?edit=true');

        await click('.btn-show-all-teams');
        expect(server.history).to.contain(
          'GET /mock-server/teams?all=true&edit=true'
        );
      });
    });
  });
});
