import { describe, it, before, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  waitUntil,
  waitFor,
  fillIn,
  blur,
  triggerEvent,
} from '@ember/test-helpers';
import { transform } from 'ol/proj';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';
import PageElements from '../../helpers/page-elements';
import resetStorages from 'ember-local-storage/test-support/reset-storage';
import { authenticateSession } from 'ember-simple-auth/test-support';

describe('Acceptance | add/site', function () {
  setupApplicationTest();
  let server;
  let receivedSite;
  let receivedPictures;

  before(function () {
    if (window.localStorage) {
      window.localStorage.clear();
    }

    if (window.sessionStorage) {
      window.sessionStorage.clear();
    }

    resetStorages();
  });

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultIconSet();

    server.testData.storage.addDefaultBaseMap('000000111111222222333333');
    server.testData.storage.addDefaultBaseMap('000000111111222222444444');

    server.testData.storage.addDefaultMap('000000000000000000000001');
    server.testData.storage.addDefaultMap('000000000000000000000002');

    receivedSite = null;
    receivedPictures = [];
    server.post(
      '/mock-server/pictures',
      (request) => {
        const pictureRequest = JSON.parse(request.requestBody);
        pictureRequest.picture['_id'] = receivedPictures.length + 1;
        receivedPictures.push(pictureRequest);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(pictureRequest),
        ];
      },
      false
    );

    const positionService = this.owner.lookup('service:position');

    positionService.geolocation = {
      watchPosition(callback) {
        callback({
          coords: {
            longitude: 1,
            latitude: 2,
            accuracy: 14,
            altitude: 15,
            altitudeAccuracy: 16,
          },
        });

        return 1;
      },
      clearWatch() {},
    };
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();

    if (window.localStorage) {
      window.localStorage.clear();
    }
    if (window.sessionStorage) {
      window.sessionStorage.clear();
    }

    resetStorages();
  });

  describe('an authenticated user', function () {
    beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultUser();
      server.testData.storage.addDefaultTeam('5ca78e2160780601008f69e6');

      server.post('/mock-server/features', (request) => {
        receivedSite = JSON.parse(request.requestBody);

        receivedSite['feature']['_id'] = '000000111111222222333333';
        receivedSite['feature']['canEdit'] = true;

        server.testData.storage.addFeature(receivedSite['feature']);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedSite),
        ];
      });
    });

    it('has an option to be redirected to the browse site page after submit', async function () {
      this.timeout(10000);
      await visit('/add/site');

      /// set some values in the about group
      await click(this.element.querySelector('.card-about .btn-add'));

      const mapButton = this.element.querySelector(
        '.card-about ul button.list-group-item-action'
      );
      await click(mapButton);

      const nameInput = this.element.querySelector(
        '.card-about .attribute-value input'
      );
      await fillIn(nameInput, 'name');
      await blur(nameInput);

      const descriptionInput = this.element.querySelector(
        '.card-about .attribute-value div.CodeMirror'
      );
      descriptionInput.CodeMirror.setValue('description');

      await click('.btn-submit');
      const buttons = this.element.querySelectorAll('.btn-success');

      expect(buttons[0].textContent.trim()).to.equal(
        'Back to Harta Verde București'
      );
      expect(buttons[1].textContent.trim()).to.equal('View site');

      await click(buttons[1]);

      expect(currentURL()).to.equal(
        `/browse/sites/${receivedSite.feature._id}`
      );
    });

    it('has an option to be redirected to the map page after submit', async function () {
      this.timeout(10000);
      await visit('/add/site');

      /// set some values in the about group
      await click(this.element.querySelector('.card-about .btn-add'));

      const mapButton = this.element.querySelector(
        '.card-about ul button.list-group-item-action'
      );
      await click(mapButton);

      const nameInput = this.element.querySelector(
        '.card-about .attribute-value input'
      );
      await fillIn(nameInput, 'name');
      await blur(nameInput);

      const descriptionInput = this.element.querySelector(
        '.card-about .attribute-value div.CodeMirror'
      );
      descriptionInput.CodeMirror.setValue('description');

      await click('.btn-submit');

      const buttons = this.element.querySelectorAll('.btn-success');

      expect(buttons[0].textContent.trim()).to.equal(
        'Back to Harta Verde București'
      );
      expect(buttons[1].textContent.trim()).to.equal('View site');

      await click(buttons[0]);

      expect(currentURL()).to.equal(
        `/browse/maps/000000000000000000000001/map-view`
      );
    });
  });

  describe('an anonymous user', function () {
    beforeEach(function () {
      server.post('/mock-server/features', (request) => {
        receivedSite = JSON.parse(request.requestBody);

        receivedSite['feature']['_id'] = '000000111111222222333333';

        server.testData.storage.addFeature(receivedSite['feature']);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedSite),
        ];
      });
    });

    it('can visit /add/site', async function () {
      await visit('/add/site');

      expect(currentURL()).to.equal('/add/site');
      expect(this.element.querySelector('.main-container > .error')).not.to
        .exist;
    });

    it('has position, about, pictures and icons sections and the submit button is disabled', async function () {
      await visit('/add/site');
      expect(currentURL()).to.equal('/add/site');

      expect(
        this.element
          .querySelectorAll('.attribute-group > .card-header')[0]
          .textContent.trim()
      ).to.equal('location');
      expect(
        this.element
          .querySelectorAll('.attribute-group > .card-header')[1]
          .textContent.trim()
      ).to.equal('How do you want to select the location?');
      expect(
        this.element
          .querySelectorAll('.attribute-group > .card-header')[2]
          .textContent.trim()
      ).to.equal('about');
      expect(
        this.element
          .querySelectorAll('.attribute-group > .card-header')[3]
          .textContent.trim()
      ).to.equal('pictures');
      expect(
        this.element
          .querySelectorAll('.attribute-group > .card-header')[4]
          .textContent.trim()
      ).to.equal('icons');
      expect(this.element.querySelectorAll('.btn-submit').length).to.equal(1);
      expect(
        this.element.querySelectorAll('.btn-submit[disabled]').length
      ).to.equal(1);
    });

    it('should query the server for maps, icons, icons sets and base maps', async function () {
      await visit('/add/site');

      const mapRequests = server.history.filter(
        (a) => a.indexOf('/maps') != -1
      );
      expect(mapRequests.length).to.equal(1);

      expect(mapRequests[0]).to.startWith('GET /mock-server/maps?containing=');
      expect(server.history).to.contain(
        'GET /mock-server/iconsets?default=true'
      );
      expect(server.history).to.contain(
        'GET /mock-server/icons?iconSet=5ca7b702ecd8490100cab96f&locale=en-us&group=true'
      );
      expect(server.history).to.contain(
        'GET /mock-server/basemaps?default=true'
      );
    });

    it('should set the default site position when lon,lat are set and ignoreGps is true', async function () {
      const lon = 16.99104134574346;
      const lat = 47.02458525573982;

      await visit(`/add/site?ignoreGps=true&lon=${lon}&lat=${lat}`);

      await click(
        this.element.querySelector('.card-position-details .btn-expand')
      );

      expect(
        parseFloat(
          this.element.querySelectorAll(
            '.card-position-details .attribute-value input'
          )[0].value
        )
      ).to.equal(lon);
      expect(
        parseFloat(
          this.element.querySelectorAll(
            '.card-position-details .attribute-value input'
          )[1].value
        )
      ).to.equal(lat);

      await waitFor('.ol-map-plain');

      const mapCenter = transform(
        this.element.querySelector('.ol-map-plain').olMap.getView().getCenter(),
        'EPSG:3857',
        'EPSG:4326'
      );

      expect(mapCenter[0]).to.equal(lon);
      expect(mapCenter[1]).to.equal(lat);
    });

    it('should be able to open the custom map editor', async function () {
      await visit('/add/site');

      expect(this.element.querySelector('.map-container')).to.exist;
      expect(this.element.querySelector('.attributes-position .btn-edit')).to
        .exist;

      await waitUntil(
        () =>
          this.element.querySelectorAll(
            '.attributes-position .btn-edit[disabled]'
          ).length == 0
      );
      await click(this.element.querySelector('.attributes-position .btn-edit'));

      /// the nav bar should be hidden
      expect(this.element.querySelector('.navbar.bar-invisible')).to.exist;

      /// the map should be fullscreen
      expect(this.element.querySelector('.map-container')).to.have.class(
        'fullscreen'
      );

      await waitUntil(
        () =>
          this.element.querySelectorAll('.base-map-select .item').length == 1
      );

      /// there is another base map available
      expect(
        this.element.querySelectorAll('.base-map-select .item').length
      ).to.equal(1);

      /// there is a submit button
      expect(this.element.querySelector('.map-container .btn-submit')).to.exist;

      /// there is a close button
      expect(this.element.querySelector('.map-container .btn-dismiss')).to
        .exist;
    });

    it('should be able open the custom map editor and dismiss it for 10 times', async function () {
      this.timeout(10000);
      await visit('/add/site');
      await waitUntil(
        () =>
          this.element.querySelectorAll(
            '.attributes-position .btn-edit[disabled]'
          ).length == 0
      );

      for (let i = 0; i < 10; i++) {
        await click(
          this.element.querySelector('.attributes-position .btn-edit')
        );
        expect(this.element.querySelector('.map-container')).to.have.class(
          'fullscreen'
        );

        await click(this.element.querySelector('.map-container .btn-dismiss'));
        expect(this.element.querySelector('.map-container')).to.exist;
        expect(this.element.querySelector('.attributes-position .btn-edit')).to
          .exist;
      }
    });

    it('should be able open the custom map editor and submit it for 10 times', async function () {
      this.timeout(10000);
      await visit('/add/site');
      await waitUntil(
        () =>
          this.element.querySelectorAll(
            '.attributes-position .btn-edit[disabled]'
          ).length == 0
      );

      for (let i = 0; i < 10; i++) {
        await click(
          this.element.querySelector('.attributes-position .btn-edit')
        );
        expect(this.element.querySelector('.map-container')).to.have.class(
          'fullscreen'
        );

        await click(this.element.querySelector('.map-container .btn-dismiss'));
        expect(this.element.querySelector('.map-container')).to.exist;
        expect(this.element.querySelector('.attributes-position .btn-edit')).to
          .exist;
      }
    });

    it('should not have editable position fields when gps is active', async function () {
      await visit('/add/site');

      await click(
        this.element.querySelector('.group-position-details .btn-expand')
      );

      expect(
        this.element.querySelector(
          '.attributes-position-details .list-group input'
        )
      ).not.to.exist;
    });

    it('should not have editable position fields when the query param ignoreGps=false', async function () {
      await visit('/add/site?ignoreGps=false');

      await click(
        this.element.querySelector('.group-position-details .btn-expand')
      );
      expect(
        this.element.querySelector('.group-position-details .list-group input')
      ).not.to.exist;
    });

    it('should have editable position fields when the query param ignoreGps=true', async function () {
      await visit('/add/site?ignoreGps=true');

      await click(
        this.element.querySelector('.group-position-details .btn-expand')
      );

      expect(
        this.element.querySelectorAll('.group-position-details input').length
      ).to.equal(5);
    });

    it('should have editable position fields after manually editing the position using the map', async function () {
      this.timeout(10000);

      await visit('/add/site?ignoreGps=false');

      await click(
        this.element.querySelector('.group-position-details .btn-expand')
      );

      expect(
        this.element.querySelector('.value-options .name-list').textContent
      ).to.contain('use my current location');

      await click(this.element.querySelector('.attributes-position .btn-edit'));
      await click(this.element.querySelector('.map-container .btn-submit'));

      expect(
        this.element.querySelector('.value-options .name-list').textContent
      ).to.contain('choose manually');
    });

    it('should submit a new site by filling the position and about fields', async function () {
      await visit('/add/site?ignoreGps=true');

      await click(
        this.element.querySelector('.card-position-details .btn-expand')
      );

      await click(this.element.querySelector('.group-position-details'));

      /// set some values in the position group
      let positionInputs = this.element.querySelectorAll(
        '.group-position-details .attribute-value input'
      );
      await fillIn(positionInputs[0], '1');
      await blur(positionInputs[0]);

      positionInputs = this.element.querySelectorAll(
        '.group-position-details .attribute-value input'
      );
      await fillIn(positionInputs[1], '2');
      await blur(positionInputs[1]);

      positionInputs = this.element.querySelectorAll(
        '.group-position-details .attribute-value input'
      );
      await fillIn(positionInputs[2], '3');
      await blur(positionInputs[2]);

      positionInputs = this.element.querySelectorAll(
        '.group-position-details .attribute-value input'
      );
      await fillIn(positionInputs[3], '4');
      await blur(positionInputs[3]);

      positionInputs = this.element.querySelectorAll(
        '.group-position-details .attribute-value input'
      );
      await fillIn(positionInputs[4], '5');
      await blur(positionInputs[4]);

      /// set some values in the about group
      await click(this.element.querySelector('.card-about .btn-add'));

      const mapButton = this.element.querySelector(
        '.card-about ul button.list-group-item-action'
      );
      await click(mapButton);

      const nameInput = this.element.querySelector(
        '.card-about .attribute-value input'
      );
      await fillIn(nameInput, 'name');
      await blur(nameInput);

      const descriptionInput = this.element.querySelector(
        '.card-about .attribute-value div.CodeMirror'
      );
      descriptionInput.CodeMirror.setValue('description');

      await click('.btn-submit');

      const buttons = this.element.querySelectorAll('.btn-success');

      expect(buttons[0].textContent.trim()).to.equal(
        'Back to Harta Verde București'
      );
      expect(buttons[1].textContent.trim()).to.equal('Add another site');

      expect(receivedSite).to.deep.equal({
        feature: {
          _id: '000000111111222222333333',
          name: 'name',
          description: 'description',
          position: {
            type: 'Point',
            coordinates: [1, 2],
          },
          visibility: 0,
          attributes: {
            'position details': {
              altitude: 3,
              accuracy: 4,
              type: 'manual',
              altitudeAccuracy: 5,
            },
          },
          info: {},
          maps: ['000000000000000000000001'],
          pictures: [],
          icons: [],
        },
      });

      expect(currentURL()).to.equal(
        '/add/site-success?map=000000000000000000000001'
      );
    });

    it('should clear the form after a site is submitted', async function () {
      this.timeout(10000);
      await visit('/add/site?ignoreGps=true');

      await click(
        this.element.querySelector('.group-position-details .btn-expand')
      );

      /// set some values in the position group
      let positionInputs = this.element.querySelectorAll(
        '.group-position-details .attribute-value input'
      );
      await fillIn(positionInputs[0], '1');
      await blur(positionInputs[0]);

      positionInputs = this.element.querySelectorAll(
        '.group-position-details .attribute-value input'
      );
      await fillIn(positionInputs[1], '2');
      await blur(positionInputs[1]);

      positionInputs = this.element.querySelectorAll(
        '.group-position-details .attribute-value input'
      );
      await fillIn(positionInputs[2], '3');
      await blur(positionInputs[2]);

      positionInputs = this.element.querySelectorAll(
        '.group-position-details .attribute-value input'
      );
      await fillIn(positionInputs[3], '4');
      await blur(positionInputs[3]);

      positionInputs = this.element.querySelectorAll(
        '.group-position-details .attribute-value input'
      );
      await fillIn(positionInputs[4], '5');
      await blur(positionInputs[4]);

      /// set some values in the about group
      await click(this.element.querySelector('.card-about .btn-add'));

      const mapButton = this.element.querySelector(
        '.card-about ul button.list-group-item-action'
      );
      await click(mapButton);

      const nameInput = this.element.querySelector(
        '.card-about .attribute-value input'
      );
      await fillIn(nameInput, 'name');
      await blur(nameInput);

      const descriptionInput = this.element.querySelector(
        '.card-about .attribute-value div.CodeMirror'
      );
      descriptionInput.CodeMirror.setValue('description');

      await click('.btn-submit');

      const buttons = this.element.querySelectorAll('.btn-success');

      await click(buttons[1]);

      expect(
        this.element.querySelector('.card-about .attribute-value input').value
      ).to.equal('');

      expect(currentURL()).to.equal('/add/site');
    });

    it('has an option to add a new site after submit', async function () {
      this.timeout(10000);
      await visit('/add/site');

      /// set some values in the about group
      await click(this.element.querySelector('.card-about .btn-add'));

      const mapButton = this.element.querySelector(
        '.card-about ul button.list-group-item-action'
      );
      await click(mapButton);

      const nameInput = this.element.querySelector(
        '.card-about .attribute-value input'
      );
      await fillIn(nameInput, 'name');
      await blur(nameInput);

      const descriptionInput = this.element.querySelector(
        '.card-about .attribute-value div.CodeMirror'
      );
      descriptionInput.CodeMirror.setValue('description');

      await click('.btn-submit');

      const buttons = this.element.querySelectorAll('.btn-success');

      expect(buttons[0].textContent.trim()).to.equal(
        'Back to Harta Verde București'
      );
      expect(buttons[1].textContent.trim()).to.equal('Add another site');

      await click(buttons[1]);

      expect(currentURL()).to.equal('/add/site');
    });

    it('has an option to go to the homepage when the map is not selected', async function () {
      this.timeout(10000);
      await visit('/add/site');

      /// set some values in the about group
      await click(this.element.querySelector('.card-about .btn-add'));

      const nameInput = this.element.querySelector(
        '.card-about .attribute-value input'
      );
      await fillIn(nameInput, 'name');
      await blur(nameInput);

      const descriptionInput = this.element.querySelector(
        '.card-about .attribute-value div.CodeMirror'
      );
      descriptionInput.CodeMirror.setValue('description');

      await click('.btn-submit');

      const buttons = this.element.querySelectorAll('.btn-success');

      expect(buttons[0].textContent.trim()).to.equal('Go to homepage');
      expect(buttons[1].textContent.trim()).to.equal('Add another site');

      await click(buttons[0]);

      expect(currentURL()).to.equal('/browse/maps/_/map-view');
    });

    it('should submit a new site with 2 pictures', async function () {
      this.timeout(20000);
      await visit('/add/site?ignoreGps=false');

      const blob = server.testData.create.pngBlob();

      await triggerEvent(
        ".card-pictures input.image-list-input[type='file']",
        'change',
        { files: [blob] }
      );

      await waitUntil(
        () => this.element.querySelectorAll('.image-list img').length > 0
      );

      await triggerEvent(
        ".card-pictures input.image-list-input[type='file']",
        'change',
        { files: [blob] }
      );

      await waitUntil(
        () => this.element.querySelectorAll('.image-list img').length > 1
      );

      await click('.btn-submit');

      // eslint-disable-next-line no-console
      console.log('waiting pictures');
      await waitUntil(() => receivedPictures.length == 2, { timeout: 8000 });

      // eslint-disable-next-line no-console
      console.log('waiting site');
      await waitUntil(() => receivedSite, { timeout: 8000 });

      // eslint-disable-next-line no-console
      console.log('all good');
      expect(receivedSite.feature.pictures).to.deep.equal(['1', '2']);
    });

    describe('when the user position is not available', function () {
      beforeEach(function () {
        const positionService = this.owner.lookup('service:position');
        positionService.geolocation = null;
      });

      it('should hide the `capture using GPS` field', async function () {
        await visit('/add/site');
        await click(
          this.element.querySelector('.group-position-details .btn-expand')
        );

        expect(
          this.element.querySelector('.attributes-position').textContent.trim()
        ).not.contains('capture using GPS');
      });

      it('should hide the the selected point', async function () {
        await visit('/add/site');

        await click(
          this.element.querySelector('.group-position-details .btn-expand')
        );

        expect(this.element.querySelector('.map-inner .ol-point')).not.to.exist;
      });
    });

    describe('when the user position is available', function () {
      let positionService;

      beforeEach(function () {
        positionService = this.owner.lookup('service:position');

        positionService.geolocation = {
          watchPosition(callback) {
            callback({
              coords: {
                latitude: 12,
                longitude: 13,
                accuracy: 14,
                altitude: 15,
                altitudeAccuracy: 16,
              },
            });

            return 1;
          },
          clearWatch() {},
        };
      });

      it('should show the `capture using GPS` field', async function () {
        await visit('/add/site');

        await click(
          this.element.querySelector('.group-position-details .btn-expand')
        );
        expect(
          this.element.querySelector('.attributes-position').textContent.trim()
        ).contains('Pick a location on the map');
      });

      it('should not show the the selected point', async function () {
        await visit('/add/site');

        await click(
          this.element.querySelector('.group-position-details .btn-expand')
        );

        expect(this.element.querySelector('.map-inner .ol-point')).not.to.exist;
      });

      it('should query the maps for the new point when the position changes', async function () {
        await visit('/add/site');

        expect(server.history).to.contain(
          'GET /mock-server/maps?containing=13%2C12'
        );
        expect(server.history).not.to.contain(
          'GET /mock-server/maps?containing=2%2C1'
        );

        let i = 0;
        await waitUntil(
          () =>
            server.history.filter((a) => {
              positionService.latitude = 1 + i;
              positionService.longitude = 2 + i;
              return a.indexOf('GET /mock-server/maps') >= 0;
            }).length > 1,
          { timeout: 5000 }
        );

        expect(server.history).not.to.contain(
          'GET /mock-server/maps?containing=2%2C12'
        );
        expect(server.history).to.contain(
          'GET /mock-server/maps?containing=2%2C1'
        );
      });
    });

    describe('using the local storage backup', function () {
      it('should inform the user that the site was restored', async function () {
        window.localStorage.setItem(
          'storage:pending-site',
          `{"lastKnownValue":
        { "name":"test name",
          "description":"test description",
          "position":{"type":"Point","coordinates":[15,55]},
          "issueCount":null,
          "visibility": 0,
          "canEdit":false,
          "info":{},
          "maps":[],
          "icons":[],
          "pictures":[],
          "attributes": {
            "position details":{"accuracy":99.48,"capturedUsingGPS":false,"altitude":999,"altitudeAccuracy":888}
          }}}`
        );

        await visit('/add/site');

        expect(this.element.querySelector('.alert.alert-info')).to.exist;
        expect(this.element.querySelector('.alert.alert-info .btn-reset')).to
          .exist;
        expect(
          this.element.querySelector('.alert .alert-heading').textContent.trim()
        ).to.equal('Your site was restored!');
        expect(
          this.element.querySelector('.alert p').textContent.trim()
        ).to.equal(
          'This site was restored from a previous session. You can finish editing it, or you can reset the form.'
        );
      });

      it('should reset the stored site', async function () {
        window.localStorage.setItem(
          'storage:pending-site',
          `{"lastKnownValue":
        { "name":"test name",
          "description":"test description",
          "position":{"type":"Point","coordinates":[15,55]},
          "issueCount":null,
          "visibility":0,
          "canEdit":false,
          "info":{},
          "maps":[],
          "icons":[],
          "pictures":[],
          "attributes": {
            "position details":{"accuracy":99.48,"capturedUsingGPS":false,"altitude":999,"altitudeAccuracy":888}
          }}}`
        );

        await visit('/add/site');

        expect(this.element.querySelector('.card-about input').value).to.equal(
          'test name'
        );

        await click(this.element.querySelector('.btn-reset'));

        const textInput = this.element.querySelector('div.CodeMirror');

        expect(this.element.querySelector('.alert.alert-info')).not.to.exist;
        expect(this.element.querySelector('.card-about input').value).to.equal(
          ''
        );
        expect(textInput.CodeMirror.getValue()).to.equal('');
      });

      it('should not inform the user that the site was restored if the storage is empty', async function () {
        await visit('/add/site');
        expect(this.element.querySelector('.alert.alert-info')).not.to.exist;
      });

      it('should restore the stored name, description and attributes', async function () {
        window.localStorage.setItem(
          'storage:pending-site',
          `{"lastKnownValue":
        { "name":"test name",
          "description":"test description",
          "position":{"type":"Point","coordinates":[15,55]},
          "issueCount":null,
          "visibility":0,
          "canEdit":false,
          "info":{},
          "maps":[],
          "icons":[],
          "pictures":[],
          "attributes": {
            "position details":{"accuracy":99.48,"capturedUsingGPS":false,"altitude":999,"altitudeAccuracy":888}
          }}}`
        );

        await visit('/add/site');

        await click('.btn-submit');

        expect(receivedSite.feature.attributes).to.deep.equal({
          'position details': {
            accuracy: 99.48,
            capturedUsingGPS: false,
            altitude: 999,
            altitudeAccuracy: 888,
          },
        });

        expect(receivedSite.feature.name).to.equal('test name');
        expect(receivedSite.feature.description).to.equal('test description');

        expect(receivedSite.feature.position).to.deep.equal({
          type: 'Point',
          coordinates: [15, 55],
        });
      });

      it('should clear the local storage on submit', async function () {
        window.localStorage.setItem(
          'storage:pending-site',
          `{"lastKnownValue":
        { "name":"test name",
          "description":"test description",
          "position":{"type":"Point","coordinates":[15,55]},
          "issueCount":null,
          "visibility":0,
          "canEdit":false,
          "info":{},
          "maps":[],
          "icons":[],
          "pictures":[],
          "attributes": {
            "position details":{"accuracy":99.48,"capturedUsingGPS":false,"altitude":999,"altitudeAccuracy":888}
          }}}`
        );

        const encodedPng = server.testData.create.base64Png();
        window.localStorage.setItem(
          'storage:pending-pictures',
          `{"lastKnownValue":{
          "new": ["${encodedPng}"],
          "stored": []
        }}`
        );

        await visit('/add/site');

        await click('.btn-submit');

        const storedSite = JSON.parse(
          window.localStorage.getItem('storage:pending-site')
        );
        expect(storedSite).to.deep.equal({ lastKnownValue: null });

        const storedPictures = JSON.parse(
          window.localStorage.getItem('storage:pending-pictures')
        );
        expect(storedPictures).to.deep.equal({
          lastKnownValue: { new: [], stored: [] },
        });
      });

      it('should restore the stored maps', async function () {
        window.localStorage.setItem(
          'storage:pending-site',
          `{"lastKnownValue":
        { "name":"test name",
          "description":"test description",
          "position":{"type":"Point","coordinates":[15,55]},
          "issueCount":null,
          "visibility":0,
          "canEdit":false,
          "info":{},
          "maps":["000000000000000000000001"],
          "icons":[],
          "pictures":[],
          "attributes": {
            "position details":{"accuracy":99.48,"capturedUsingGPS":false,"altitude":999,"altitudeAccuracy":888}
          }}}`
        );

        await visit('/add/site');

        expect(
          this.element.querySelector('.card-about').textContent
        ).to.contain('Harta Verde București');

        await click('.btn-submit');

        expect(receivedSite.feature.attributes).to.deep.equal({
          'position details': {
            accuracy: 99.48,
            capturedUsingGPS: false,
            altitude: 999,
            altitudeAccuracy: 888,
          },
        });

        expect(receivedSite.feature.maps).to.deep.equal([
          '000000000000000000000001',
        ]);
        expect(receivedSite.feature.icons).to.deep.equal([]);
      });

      it('should not restore the missing maps', async function () {
        window.localStorage.setItem(
          'storage:pending-site',
          `{"lastKnownValue":
        { "name":"test name",
          "description":"test description",
          "position":{"type":"Point","coordinates":[15,55]},
          "issueCount":null,
          "visibility":0,
          "canEdit":false,
          "info":{},
          "maps":["000000000000000000000033"],
          "icons":[],
          "pictures":[],
          "attributes": {
            "position details":{"accuracy":99.48,"capturedUsingGPS":false,"altitude":999,"altitudeAccuracy":888}
          }}}`
        );

        await visit('/add/site');

        await click('.btn-submit');

        expect(receivedSite.feature.attributes).to.deep.equal({
          'position details': {
            accuracy: 99.48,
            capturedUsingGPS: false,
            altitude: 999,
            altitudeAccuracy: 888,
          },
        });

        expect(receivedSite.feature.maps).to.deep.equal([]);
        expect(receivedSite.feature.icons).to.deep.equal([]);
      });

      it('should restore the stored icons', async function () {
        window.localStorage.setItem(
          'storage:pending-site',
          `{"lastKnownValue":
        { "name":"test name",
          "description":"test description",
          "position":{"type":"Point","coordinates":[15,55]},
          "issueCount":null,
          "visibility":0,
          "canEdit":false,
          "info":{},
          "maps":[],
          "icons":["5ca7bfc0ecd8490100cab980"],
          "pictures":[],
          "attributes": {
            "position details":{"accuracy":99.48,"capturedUsingGPS":false,"altitude":999,"altitudeAccuracy":888}
          }}}`
        );

        await visit('/add/site');

        expect(
          this.element.querySelectorAll('.icons-order .icon-container img')
            .length
        ).to.equal(1);
        expect(
          this.element.querySelector('.icons-order').textContent
        ).to.contain('Healthy Dining');

        await click('.btn-submit');

        expect(receivedSite.feature.attributes).to.deep.equal({
          'position details': {
            accuracy: 99.48,
            capturedUsingGPS: false,
            altitude: 999,
            altitudeAccuracy: 888,
          },
        });

        expect(receivedSite.feature.maps).to.deep.equal([]);
        expect(receivedSite.feature.icons).to.deep.equal([
          '5ca7bfc0ecd8490100cab980',
        ]);
      });

      it('should not restore the missing icons', async function () {
        window.localStorage.setItem(
          'storage:pending-site',
          `{"lastKnownValue":
        { "name":"test name",
          "description":"test description",
          "position":{"type":"Point","coordinates":[15,55]},
          "issueCount":null,
          "visibility":0,
          "canEdit":false,
          "info":{},
          "maps":[],
          "icons":["000000000000000000000033"],
          "pictures":[],
          "attributes": {
            "position details":{"accuracy":99.48,"capturedUsingGPS":false,"altitude":999,"altitudeAccuracy":888}
          }}}`
        );

        await visit('/add/site');

        await click('.btn-submit');

        expect(receivedSite.feature.attributes).to.deep.equal({
          'position details': {
            accuracy: 99.48,
            capturedUsingGPS: false,
            altitude: 999,
            altitudeAccuracy: 888,
          },
        });

        expect(receivedSite.feature.maps).to.deep.equal([]);
        expect(receivedSite.feature.icons).to.deep.equal([]);
      });

      it('should restore the new pictures', async function () {
        const encodedPng = server.testData.create.base64Png();

        window.localStorage.setItem(
          'storage:pending-site',
          `{"lastKnownValue":
      { "name":"",
        "description":"",
        "position":{"type":"Point","coordinates":[15,55]},
        "issueCount":null,
        "visibility":0,
        "canEdit":false,
        "info":{},
        "maps":[],
        "icons":[],
        "pictures":[],
        "attributes": {
          "position details":{"accuracy":99.48,"capturedUsingGPS":false,"altitude":999,"altitudeAccuracy":888}
        }}}`
        );

        window.localStorage.setItem(
          'storage:pending-pictures',
          `{"lastKnownValue":{
        "new": ["${encodedPng}"],
        "stored": []
      }}`
        );

        await visit('/add/site');

        expect(
          this.element.querySelectorAll('.card-pictures .image-list img').length
        ).to.equal(1);
        expect(
          this.element
            .querySelector('.card-pictures .image-list img')
            .attributes.getNamedItem('src').textContent
        ).to.equal(`${encodedPng}`);

        await click('.btn-submit');

        expect(receivedSite.feature.pictures).to.deep.equal(['1']);
        expect(receivedPictures.length).to.equal(1);
      });

      it('should restore the stored pictures', async function () {
        window.localStorage.setItem(
          'storage:pending-site',
          `{"lastKnownValue":
          { "name":"test name",
            "description":"",
            "position":{"type":"Point","coordinates":[15,55]},
            "issueCount":null,
            "visibility":0,
            "canEdit":false,
            "info":{},
            "maps":[],
            "icons":[],
            "pictures":[],
            "attributes": {
              "position details":{"accuracy":99.48,"capturedUsingGPS":false,"altitude":999,"altitudeAccuracy":888}
          }}}`
        );

        window.localStorage.setItem(
          'storage:pending-pictures',
          `{"lastKnownValue":{
          "new": [],
          "stored": ["5cc8dc1038e882010061545a"]
        }}`
        );

        await visit('/add/site');

        expect(
          this.element.querySelectorAll('.card-pictures .image-list img').length
        ).to.equal(1);
        expect(
          this.element
            .querySelector('.card-pictures .image-list img')
            .attributes.getNamedItem('src').textContent
        ).to.equal(
          `https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/sm`
        );

        await click('.btn-submit');
        expect(receivedPictures.length).to.equal(0);
      });

      it('should ignore the missing stored pictures', async function () {
        window.localStorage.setItem(
          'storage:pending-site',
          `{"lastKnownValue":
      { "name":"test name",
        "description":"",
        "position":{"type":"Point","coordinates":[15,55]},
        "issueCount":null,
        "visibility":0,
        "canEdit":false,
        "info":{},
        "maps":[],
        "icons":[],
        "pictures":[],
        "attributes": {
          "position details":{"accuracy":99.48,"capturedUsingGPS":false,"altitude":999,"altitudeAccuracy":888}
        }}}`
        );
        window.localStorage.setItem(
          'storage:pending-pictures',
          `{"lastKnownValue":{
        "new": [],
        "stored": ["000000001"]
      }}`
        );

        await visit('/add/site');
        await click('.btn-submit');

        expect(
          this.element.querySelectorAll('.card-pictures .image-list > .image')
            .length
        ).to.equal(0);
        expect(receivedPictures.length).to.equal(0);
      });

      it('should show an alert on invalid pending site storage', async function () {
        this.timeout(9000);
        window.localStorage.setItem('storage:pending-site', `a""`);
        window.localStorage.setItem('storage:pending-pictures', `""`);

        await visit('/add/site');
        expect(this.element.querySelector('.alert.alert-danger')).to.exist;
        expect(
          this.element.querySelector('.alert').textContent.trim()
        ).to.contain(
          'Your previous session was not restored because it contains corrupted data.'
        );
      });
    });

    describe('the site attributes', function () {
      let icon1;
      let icon2;

      beforeEach(function () {
        icon1 = server.testData.create.icon('000000000000000000000001');
        icon1.name = 'icon 1';
        icon1.localName = 'local icon 1';
        icon1.attributes = [
          {
            name: 'attribute1',
            help: 'some help message',
            displayName: 'local attribute1',
            type: 'short text',
          },
        ];

        server.testData.storage.addIcon(icon1);

        icon2 = server.testData.create.icon('000000000000000000000002');
        icon2.name = 'icon 2';
        icon2.localName = 'local icon 2';
        icon2.allowMany = true;
        icon2.attributes = [
          {
            name: 'attribute1',
            displayName: 'local attribute1',
            type: 'short text',
          },
        ];

        server.testData.storage.addIcon(icon2);
      });

      it('should not be visible if the attributes are set but no icon is selected', async function () {
        window.localStorage.setItem(
          'storage:pending-site',
          `{"lastKnownValue":
      { "name":"test name",
        "description":"test description",
        "position":{"type":"Point","coordinates":[15,55]},
        "issueCount":null,
        "visibility":0,
        "canEdit":false,
        "info":{},
        "maps":[],
        "icons":[],
        "pictures":[],
        "attributes": {
          "position details":{"accuracy":99.48,"capturedUsingGPS":false,"altitude":999,"altitudeAccuracy":888},
          "icon 1": {}
        }}}`
        );

        await visit('/add/site');

        await waitUntil(() => this.element.querySelector('.attributes-icons'));
        expect(this.element.querySelector('.attributes-icon-1')).not.to.exist;
        expect(this.element.querySelector('.attributes-local-icon-1')).not.to
          .exist;
        expect(this.element.querySelector('.attributes-position-details')).not
          .to.exist;
        expect(this.element.querySelector('.btn-add-attributes')).not.to.exist;
      });

      describe('for an icon without allow many flag', function () {
        beforeEach(async function () {
          this.timeout(20000);
          await visit('/add/site');
          await PageElements.selectIconsInModal(['000000000000000000000001']);
        });

        it('should be visible as add button after selecting an icon with attributes', async function () {
          expect(
            this.element.querySelector('.btn-add-attributes').textContent.trim()
          ).to.equal('local icon 1');
          await click('.btn-submit');

          expect(receivedSite).to.deep.equal({
            feature: {
              _id: '000000111111222222333333',
              name: '',
              description: '',
              position: { type: 'Point', coordinates: [1, 2] },
              visibility: 0,
              attributes: {
                'position details': {
                  altitude: 15,
                  accuracy: 14,
                  altitudeAccuracy: 16,
                  type: 'gps',
                },
              },
              info: {},
              maps: [],
              pictures: [],
              icons: ['000000000000000000000001'],
            },
          });
        });

        it('should be able to add attributes', async function () {
          await click('.btn-add-attributes');
          expect(this.element.querySelector('.btn-add-attributes')).not.to
            .exist;

          /// check the card titles
          const headers = this.element.querySelectorAll(
            '.group-icon-1 .card-header'
          );

          expect(headers.length).to.equal(1);

          expect(headers[0].textContent.trim()).not.to.startWith('1.');
          expect(headers[0].textContent.trim()).to.endWith('local icon 1');
          expect(headers[0].querySelector('img')).to.have.attribute(
            'src',
            icon2['image'].value
          );

          expect(
            this.element
              .querySelector('.group-icon-1 .input-group-text')
              .textContent.trim()
          ).to.equal('local attribute1');

          await click('.btn-submit');

          expect(receivedSite).to.deep.equal({
            feature: {
              _id: '000000111111222222333333',
              name: '',
              description: '',
              position: { type: 'Point', coordinates: [1, 2] },
              visibility: 0,
              attributes: {
                'position details': {
                  altitude: 15,
                  accuracy: 14,
                  altitudeAccuracy: 16,
                  type: 'gps',
                },
                'icon 1': { attribute1: null },
              },
              info: {},
              maps: [],
              pictures: [],
              icons: ['000000000000000000000001'],
            },
          });

          await Modal.waitToHide();
        });

        it('should be able to add attributes and then remove them', async function () {
          await click('.btn-add-attributes');
          expect(this.element.querySelector('.btn-add-attributes')).not.to
            .exist;

          await click('.group-icon-1 .close');

          expect(this.element.querySelector('.btn-add-attributes')).to.exist;

          /// check the card titles
          const headers = this.element.querySelectorAll(
            '.group-icon-1 .card-header'
          );
          expect(headers.length).to.equal(0);

          await click('.btn-submit');

          expect(receivedSite).to.deep.equal({
            feature: {
              _id: '000000111111222222333333',
              name: '',
              description: '',
              position: { type: 'Point', coordinates: [1, 2] },
              visibility: 0,
              attributes: {
                'position details': {
                  altitude: 15,
                  accuracy: 14,
                  altitudeAccuracy: 16,
                  type: 'gps',
                },
              },
              info: {},
              maps: [],
              pictures: [],
              icons: ['000000000000000000000001'],
            },
          });
        });

        it('should be able to add attributes and set a value', async function () {
          this.timeout(5000);

          await click('.btn-add-attributes');

          /// set the values
          let inputs = this.element.querySelectorAll(
            '.group-icon-1 .attribute-value input'
          );
          await fillIn(inputs[0], 'value 1');

          await click('.btn-submit');

          expect(receivedSite).to.deep.equal({
            feature: {
              _id: '000000111111222222333333',
              name: '',
              description: '',
              position: { type: 'Point', coordinates: [1, 2] },
              visibility: 0,
              attributes: {
                'position details': {
                  altitude: 15,
                  accuracy: 14,
                  altitudeAccuracy: 16,
                  type: 'gps',
                },
                'icon 1': { attribute1: 'value 1' },
              },
              info: {},
              maps: [],
              pictures: [],
              icons: ['000000000000000000000001'],
            },
          });
        });
      });

      describe('for an icon with allow many flag', function () {
        beforeEach(async function () {
          await visit('/add/site');
          await PageElements.selectIconsInModal(['000000000000000000000002']);
        });

        it('should be able to add 3 sets of attributes', async function () {
          await click('.btn-add-attributes');
          await click('.btn-add-attributes');
          await click('.btn-add-attributes');

          /// check the card titles
          const headers = this.element.querySelectorAll(
            '.group-icon-2 .card-header'
          );

          expect(headers.length).to.equal(3);

          expect(headers[0].textContent.trim()).to.startWith('1.');
          expect(headers[1].textContent.trim()).to.startWith('2.');
          expect(headers[2].textContent.trim()).to.startWith('3.');

          expect(headers[0].textContent.trim()).to.endWith('local icon 2');
          expect(headers[1].textContent.trim()).to.endWith('local icon 2');
          expect(headers[2].textContent.trim()).to.endWith('local icon 2');

          expect(headers[0].querySelector('img')).to.have.attribute(
            'src',
            icon2['image'].value
          );

          //it should have an add button
          expect(
            this.element.querySelector('.btn-add-attributes').textContent.trim()
          ).to.equal('local icon 2');

          await click('.btn-submit');

          expect(receivedSite).to.deep.equal({
            feature: {
              _id: '000000111111222222333333',
              name: '',
              description: '',
              position: { type: 'Point', coordinates: [1, 2] },
              visibility: 0,
              attributes: {
                'position details': {
                  accuracy: 14,
                  altitude: 15,
                  altitudeAccuracy: 16,
                  type: 'gps',
                },
                'icon 2': [
                  { attribute1: null },
                  { attribute1: null },
                  { attribute1: null },
                ],
              },
              info: {},
              maps: [],
              pictures: [],
              icons: ['000000000000000000000002'],
            },
          });
        });

        it('should be able to add 3 sets of attributes and then remove all of them', async function () {
          await click('.btn-add-attributes');
          await click('.btn-add-attributes');
          await click('.btn-add-attributes');

          await click('.group-icon-2 .close');
          await click('.group-icon-2 .close');
          await click('.group-icon-2 .close');

          /// check the card titles
          const headers = this.element.querySelectorAll(
            '.group-icon-2 .card-header'
          );

          expect(headers.length).to.equal(0);

          //it should have an add button
          expect(
            this.element.querySelector('.btn-add-attributes').textContent.trim()
          ).to.equal('local icon 2');

          await click('.btn-submit');

          expect(receivedSite).to.deep.equal({
            feature: {
              _id: '000000111111222222333333',
              name: '',
              description: '',
              position: { type: 'Point', coordinates: [1, 2] },
              visibility: 0,
              attributes: {
                'position details': {
                  accuracy: 14,
                  altitude: 15,
                  altitudeAccuracy: 16,
                  type: 'gps',
                },
              },
              info: {},
              maps: [],
              pictures: [],
              icons: ['000000000000000000000002'],
            },
          });
        });

        it('should be able to add 3 sets of attributes and then remove 2 of them', async function () {
          await click('.btn-add-attributes');
          await click('.btn-add-attributes');
          await click('.btn-add-attributes');

          await click('.group-icon-2 .close');
          await click('.group-icon-2 .close');

          /// check the card titles
          const headers = this.element.querySelectorAll(
            '.group-icon-2 .card-header'
          );

          expect(headers.length).to.equal(1);

          //it should have an add button
          expect(
            this.element.querySelector('.btn-add-attributes').textContent.trim()
          ).to.equal('local icon 2');

          await click('.btn-submit');

          expect(receivedSite).to.deep.equal({
            feature: {
              _id: '000000111111222222333333',
              name: '',
              description: '',
              position: { type: 'Point', coordinates: [1, 2] },
              visibility: 0,
              attributes: {
                'position details': {
                  accuracy: 14,
                  altitude: 15,
                  altitudeAccuracy: 16,
                  type: 'gps',
                },
                'icon 2': [{ attribute1: null }],
              },
              info: {},
              maps: [],
              pictures: [],
              icons: ['000000000000000000000002'],
            },
          });
        });

        it('should be able to add 3 sets of attributes and set a value to the attribute', async function () {
          this.timeout(5000);

          await click('.btn-add-attributes');
          await click('.btn-add-attributes');
          await click('.btn-add-attributes');

          /// set the values
          let inputs = this.element.querySelectorAll(
            '.group-icon-2 .attribute-value input'
          );
          await fillIn(inputs[0], 'value 1');

          inputs = this.element.querySelectorAll(
            '.group-icon-2 .attribute-value input'
          );
          await fillIn(inputs[1], 'value 2');

          inputs = this.element.querySelectorAll(
            '.group-icon-2 .attribute-value input'
          );
          await fillIn(inputs[2], 'value 3');

          //it should have an add button
          expect(
            this.element.querySelector('.btn-add-attributes').textContent.trim()
          ).to.equal('local icon 2');

          await click('.btn-submit');

          expect(receivedSite).to.deep.equal({
            feature: {
              _id: '000000111111222222333333',
              name: '',
              description: '',
              position: { type: 'Point', coordinates: [1, 2] },
              visibility: 0,
              attributes: {
                'position details': {
                  accuracy: 14,
                  altitude: 15,
                  altitudeAccuracy: 16,
                  type: 'gps',
                },
                'icon 2': [
                  { attribute1: 'value 1' },
                  { attribute1: 'value 2' },
                  { attribute1: 'value 3' },
                ],
              },
              info: {},
              maps: [],
              pictures: [],
              icons: ['000000000000000000000002'],
            },
          });
        });
      });

      describe('for an icon with a required attribute', function () {
        let icon3;
        beforeEach(async function () {
          icon3 = server.testData.create.icon('000000000000000000000003');
          icon3.name = 'icon 3';
          icon3.localName = 'local icon 3';
          icon3.attributes = [
            {
              name: 'attribute3',
              help: 'some help message',
              displayName: 'local attribute3',
              type: 'short text',
              isRequired: true,
            },
          ];

          server.testData.storage.addIcon(icon3);

          this.timeout(20000);
          await visit('/add/site');
          await PageElements.selectIconsInModal(['000000000000000000000003']);
        });

        it('should not be able to submit when mandatory attributes are not set', async function () {
          await click('.btn-add-attributes');

          expect(this.element.querySelector('.btn-submit')).to.have.attribute(
            'disabled',
            ''
          );
        });
      });

      describe('setting attributes', function () {
        afterEach(function () {
          server.shutdown();
          Modal.clear();

          if (window.localStorage) {
            window.localStorage.clear();
          }
          if (window.sessionStorage) {
            window.sessionStorage.clear();
          }

          resetStorages();
        });

        it('should properly show help messages', async function () {
          this.timeout(5000);
          await visit('/add/site');
          await PageElements.selectIconsInModal([
            '000000000000000000000001',
            '000000000000000000000002',
          ]);

          await click('.btn-add-icon-1');
          await click('.btn-add-icon-2');

          expect(this.element.querySelector('.group-icon-1 .input-group-help'))
            .to.exist;
          expect(
            this.element.querySelector('.group-icon-1 .input-group-help')
              .textContent
          ).to.contain('some help message');
          expect(this.element.querySelector('.group-icon-2 .input-group-help'))
            .not.to.exist;
        });

        it('should be able to set a short text value', async function () {
          const icon = server.testData.create.icon('000000000000000000000003');
          icon.name = 'icon3';
          icon.localName = 'local icon3';
          icon.attributes = [
            {
              name: 'attribute',
              localName: 'local attribute',
              type: 'short text',
            },
          ];

          server.testData.storage.addIcon(icon);

          await visit('/add/site');
          await PageElements.selectIconsInModal(['000000000000000000000003']);

          await click('.btn-add-attributes');

          /// set the values
          let inputs = this.element.querySelectorAll(
            '.group-icon3 .attribute-value input'
          );
          await fillIn(inputs[0], 'text value');

          await click('.btn-submit');

          expect(receivedSite.feature.attributes.icon3).to.deep.equal({
            attribute: 'text value',
          });
        });

        it('should be able to set a long text value', async function () {
          const icon = server.testData.create.icon('000000000000000000000003');
          icon.name = 'icon3';
          icon.localName = 'local icon3';
          icon.attributes = [
            {
              name: 'attribute',
              localName: 'local attribute',
              type: 'long text',
            },
          ];

          server.testData.storage.addIcon(icon);

          await visit('/add/site');
          await PageElements.selectIconsInModal(['000000000000000000000003']);

          await click('.btn-add-attributes');

          /// set the values
          const textInput = this.element.querySelector(
            '.group-icon3 .attribute-value div.CodeMirror'
          );
          textInput.CodeMirror.setValue('description');

          await click('.btn-submit');

          expect(receivedSite.feature.attributes.icon3).to.deep.equal({
            attribute: 'description',
          });
        });

        it('should be able to set an integer value', async function () {
          const icon = server.testData.create.icon('000000000000000000000003');
          icon.name = 'icon3';
          icon.localName = 'local icon3';
          icon.attributes = [
            {
              name: 'attribute1',
              localName: 'local attribute 1',
              type: 'integer',
            },
            {
              name: 'attribute2',
              localName: 'local attribute 2',
              type: 'integer',
            },
          ];

          server.testData.storage.addIcon(icon);

          await visit('/add/site');
          await PageElements.selectIconsInModal(['000000000000000000000003']);

          await click('.btn-add-attributes');

          /// set the values
          let inputs = this.element.querySelectorAll(
            '.group-icon3 .attribute-value input'
          );
          await fillIn(inputs[0], '100');
          await fillIn(inputs[1], '-12');

          await click('.btn-submit');
          expect(receivedSite.feature.attributes.icon3).to.deep.equal({
            attribute1: 100,
            attribute2: -12,
          });
        });

        it('should be able to set a boolean value', async function () {
          this.timeout(5000);
          const icon = server.testData.create.icon('000000000000000000000003');
          icon.name = 'icon3';
          icon.localName = 'local icon3';
          icon.attributes = [
            {
              name: 'attribute1',
              localName: 'local attribute 1',
              type: 'boolean',
            },
            {
              name: 'attribute2',
              localName: 'local attribute 2',
              type: 'boolean',
            },
            {
              name: 'attribute3',
              localName: 'local attribute 3',
              type: 'boolean',
            },
          ];

          server.testData.storage.addIcon(icon);

          await visit('/add/site');
          await PageElements.selectIconsInModal(['000000000000000000000003']);

          await click('.btn-add-attributes');

          /// set the values
          let inputs = this.element.querySelectorAll(
            '.group-icon3 .value-boolean'
          );
          await click(inputs[0].querySelector('.btn-true'));
          expect(inputs[0].querySelector('.btn-true.btn-secondary')).to.exist;
          expect(inputs[0].querySelector('.btn-false.btn-secondary')).not.to
            .exist;

          await click(inputs[1].querySelector('.btn-false'));
          expect(inputs[1].querySelector('.btn-false.btn-secondary')).to.exist;
          expect(inputs[1].querySelector('.btn-true.btn-secondary')).not.to
            .exist;

          await click('.btn-submit');

          await waitUntil(() => receivedSite);
          expect(receivedSite.feature.attributes.icon3).to.deep.equal({
            attribute1: true,
            attribute2: false,
            attribute3: null,
          });
        });

        it('should be able to set a decimal value', async function () {
          this.timeout(5000);
          const icon = server.testData.create.icon('000000000000000000000003');
          icon.name = 'icon3';
          icon.localName = 'local icon3';
          icon.attributes = [
            {
              name: 'attribute1',
              localName: 'local attribute 1',
              type: 'decimal',
            },
            {
              name: 'attribute2',
              localName: 'local attribute 2',
              type: 'decimal',
            },
            {
              name: 'attribute3',
              localName: 'local attribute 3',
              type: 'decimal',
            },
            {
              name: 'attribute4',
              localName: 'local attribute 4',
              type: 'decimal',
            },
            {
              name: 'attribute5',
              localName: 'local attribute 5',
              type: 'decimal',
            },
          ];

          server.testData.storage.addIcon(icon);

          await visit('/add/site');
          await PageElements.selectIconsInModal(['000000000000000000000003']);

          await click('.btn-add-attributes');

          /// set the values
          let inputs = this.element.querySelectorAll(
            '.group-icon3 .attribute-value input'
          );
          await fillIn(inputs[0], '100');
          await fillIn(inputs[1], '100.25');
          await fillIn(inputs[2], '100,25');
          await fillIn(inputs[3], ' 100 ');
          await fillIn(inputs[4], 'test');

          await click('.btn-submit');
          expect(receivedSite.feature.attributes.icon3).to.deep.equal({
            attribute1: 100,
            attribute2: 100.25,
            attribute3: 100,
            attribute4: 100,
            attribute5: null,
          });
        });

        it.skip('should be able to set options value', async function () {
          this.timeout(10000);
          const icon = server.testData.create.icon('000000000000000000000003');
          icon.name = 'icon3';
          icon.localName = 'local icon3';
          icon.attributes = [
            {
              name: 'attribute1',
              localName: 'local attribute 1',
              type: 'options',
              options: 'option1,option2,option3',
            },
            {
              name: 'attribute2',
              localName: 'local attribute 2',
              type: 'options',
              options: 'option1,option2,option3',
            },
          ];

          server.testData.storage.addIcon(icon);

          await visit('/add/site');
          await PageElements.selectIconsInModal(['000000000000000000000003']);

          await click('.btn-add-attributes');

          /// set the values
          await waitUntil(
            () => this.element.querySelector('.group-icon3 .btn-add'),
            { timeout: 2000 }
          );
          await click('.group-icon3 .btn-add');

          await click('.group-icon3 .list-group-item.not-active');
          await click(this.element.querySelector('.btn-submit'));

          expect(receivedSite.feature.attributes.icon3).to.deep.equal({
            attribute1: 'option1',
            attribute2: null,
          });
        });
      });
    });
  });
});
