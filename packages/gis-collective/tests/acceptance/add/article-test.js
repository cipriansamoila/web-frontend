import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  fillIn,
  waitFor,
  triggerEvent,
} from '@ember/test-helpers';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';
import PageElements from '../../helpers/page-elements';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';

describe('Acceptance | add/article', function () {
  setupApplicationTest();
  let server;
  let receivedArticle;
  let team;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();

    receivedArticle = null;
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token', async function () {
    invalidateSession();

    await visit(`/add/article`);
    expect(currentURL()).to.equal('/login?redirect=%2Fadd%2Farticle');
  });

  describe('when an user is authenticated and no team is available', function () {
    beforeEach(function () {
      server.testData.storage.addDefaultUser();
      server.testData.storage.addDefaultLayout();
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/add/article`);
      expect(currentURL()).to.equal('/add/article');

      expect(this.element.querySelector('h1').textContent.trim()).to.equal(
        'New article'
      );
      expect(this.element.querySelector('.breadcrumb')).not.to.exist;

      expect(
        this.element.querySelector('.row-slug h3').textContent.trim()
      ).to.contain('Slug');
      expect(this.element.querySelector('.row-slug input.form-control')).to
        .exist;

      expect(
        this.element.querySelector('.row-team h3').textContent.trim()
      ).to.contain('Team');
      expect(this.element.querySelector('.row-team select')).to.not.exist;

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to
        .exist;
      expect(
        this.element.querySelector('.btn.btn-primary.btn-submit')
      ).to.have.attribute('disabled', '');

      expect(this.element.querySelector('.row-team .btn-add-team')).to.exist;
      expect(this.element.querySelector('.alert-danger .btn-add-team')).to
        .exist;
    });

    it('the button in the alert should redirect the user to the add a team form', async function () {
      authenticateSession();
      await visit(`/add/article`);
      await click('.alert-danger .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=add.article');
    });

    it('the button in the Team section should also redirect to the add a team form', async function () {
      authenticateSession();
      await visit(`/add/article`);
      await click('.row-team .btn-add-team');

      expect(currentURL()).to.equal('/manage/teams/add?next=add.article');
    });
  });

  describe('when an user is authenticated and a team is available', function () {
    beforeEach(function () {
      team = server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultUser();
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/add/article`);
      expect(currentURL()).to.equal('/add/article');

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to
        .exist;
      expect(
        this.element.querySelector('.btn.btn-primary.btn-submit')
      ).to.have.attribute('disabled', '');
    });

    describe('when the server request is successful', function () {
      let page;

      beforeEach(function () {
        team = server.testData.storage.addDefaultTeam();
        page = server.testData.storage.addDefaultPage();
        server.testData.storage.addDefaultTeam('000000000000000000000001');

        server.post('/mock-server/articles', (request) => {
          receivedArticle = JSON.parse(request.requestBody);
          receivedArticle.article._id = page._id;

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedArticle),
          ];
        });
      });

      it('redirects to the edit page after the request response is received', async function () {
        authenticateSession();

        await visit(`/add/article`);
        await waitFor('.editor-is-ready', { timeout: 3000 });

        await fillIn('.ce-paragraph', 'test article');

        await fillIn('.input-slug', 'new-slug');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(receivedArticle.article.content.blocks).to.deep.equal([
          { type: 'paragraph', data: { text: 'test article' } },
        ]);
        expect(receivedArticle.article.slug).to.equal('new-slug');
        expect(receivedArticle.article.visibility).to.deep.equal({
          isPublic: false,
          isDefault: false,
          team: '5ca78e2160780601008f69e6',
        });

        expect(currentURL()).to.equal(`/manage/articles/${page._id}`);
      });
    });

    describe('when the server request fails', function () {
      beforeEach(function () {
        server.post('/mock-server/articles', () => {
          return [
            400,
            { 'Content-Type': 'application/json' },
            JSON.stringify({
              errors: [
                {
                  description: 'The `page.slug` field is required.',
                  status: 400,
                  title: 'Validation error',
                },
              ],
            }),
          ];
        });
      });

      it('shows a modal with the error message', async function () {
        this.timeout(10000);
        authenticateSession();

        await visit(`/add/article`);

        await waitFor('.editor-is-ready', { timeout: 3000 });

        await fillIn('.ce-paragraph', 'test article');
        await fillIn('.input-slug', 'new-slug');

        await fillIn('.input-slug', 'new-slug');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(currentURL()).to.equal(`/add/article`);

        await Modal.waitToDisplay();

        expect(Modal.title()).to.equal('Validation error');
        expect(Modal.message()).to.equal('The `page.slug` field is required.');
      });
    });
  });

  describe('when an admin user is authenticated', function () {
    beforeEach(() => {
      team = server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultUser(true);
      server.testData.storage.addDefaultLayout();
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/add/article`);
      expect(currentURL()).to.equal('/add/article');
    });

    describe('when the server request is successfully', function () {
      let page;

      beforeEach(function () {
        page = server.testData.storage.addDefaultPage();
        server.post('/mock-server/articles', (request) => {
          receivedArticle = JSON.parse(request.requestBody);
          receivedArticle.article._id = page._id;

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedArticle),
          ];
        });
      });

      it('queries the teams without the all query param', async function () {
        authenticateSession();

        await visit(`/add/article`);

        await waitFor('.editor-is-ready', { timeout: 3000 });

        await fillIn('.ce-paragraph', 'test article');
        await fillIn('.input-slug', 'new-slug');

        await fillIn('.input-slug', 'new-slug');

        this.element.querySelector('.row-team select').value = team._id;
        await triggerEvent('.row-team select', 'change');

        await PageElements.wait(1000);
        expect(server.history).to.contain('GET /mock-server/teams?edit=true');

        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));
        expect(currentURL()).to.equal(`/manage/articles/${page._id}`);
      });

      it('queries the teams with the all query param after the admin button is pressed', async function () {
        authenticateSession();

        await visit(`/add/article`);
        expect(server.history).to.contain('GET /mock-server/teams?edit=true');

        await click('.btn-show-all-teams');
        expect(server.history).to.contain(
          'GET /mock-server/teams?all=true&edit=true'
        );
      });
    });
  });
});
