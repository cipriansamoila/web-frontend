import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  fillIn,
  triggerEvent,
} from '@ember/test-helpers';
import TestServer from '../../helpers/test-server';
import Modal from '../../helpers/modal';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';

describe('Acceptance | add/feature', function () {
  setupApplicationTest();
  let server;
  let receivedFeature;
  let map;
  let feature;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();

    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultIconSet();

    server.testData.storage.addDefaultBaseMap('000000111111222222333333');
    server.testData.storage.addDefaultBaseMap('000000111111222222444444');

    map = server.testData.storage.addDefaultMap();
    feature = server.testData.storage.addDefaultFeature();

    server.testData.storage.addDefaultMap('000000000000000000000002');
    server.testData.storage.addDefaultUser(false, '5b8a59caef739394031a3f67');

    receivedFeature = null;
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token', async function () {
    invalidateSession();

    await visit(`/add/feature`);
    expect(currentURL()).to.equal('/login?redirect=%2Fadd%2Ffeature');
  });

  describe('when an user is authenticated', function () {
    beforeEach(() => {
      server.testData.storage.addDefaultUser();
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/add/feature`);
      expect(currentURL()).to.equal('/add/feature');

      expect(this.element.querySelector('h1').textContent.trim()).to.equal(
        'New feature'
      );
      expect(this.element.querySelector('.breadcrumb')).not.to.exist;

      expect(
        this.element.querySelector('.row-name h5').textContent.trim()
      ).to.contain('name');
      expect(this.element.querySelector('.row-name input.form-control')).to
        .exist;

      expect(
        this.element.querySelector('.row-map h5').textContent.trim()
      ).to.contain('map');
      expect(this.element.querySelector('.row-map select')).to.exist;
      expect(
        this.element.querySelectorAll('.row-map select option').length
      ).to.equal(3);

      expect(
        this.element.querySelector('.row-geometry h5').textContent.trim()
      ).to.contain('geometry');
      expect(this.element.querySelector('.row-geometry .map-container')).to
        .exist;

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to
        .exist;
      expect(
        this.element.querySelector('.btn.btn-primary.btn-submit')
      ).to.have.attribute('disabled', '');
    });

    describe('when the server request is successfully', function () {
      beforeEach(function () {
        server.post('/mock-server/features', (request) => {
          receivedFeature = JSON.parse(request.requestBody);
          receivedFeature.feature._id = feature._id;

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedFeature),
          ];
        });
      });

      it('redirects to the edit page after the request response is received', async function () {
        this.timeout(10000);
        authenticateSession();

        await visit(`/add/feature`);

        await fillIn('.row-name input.form-control', 'new feature name');

        this.element.querySelector('.row-map select').value = map._id;
        await triggerEvent('.row-map select', 'change');

        await click('.btn-paste');

        const editor = this.element.querySelector('.input-geo-json').editor;
        editor.setMode('text');

        this.element.querySelector('.jsoneditor-text').value = '';
        await fillIn(
          '.jsoneditor-text',
          `{ "type": "Point", "coordinates": [1, 2] }`
        );

        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(receivedFeature).to.deep.equal({
          feature: {
            _id: '5ca78e2160780601008f69e6',
            name: 'new feature name',
            description: '',
            position: {
              type: 'Point',
              coordinates: [1, 2],
            },
            visibility: 0,
            info: {},
            maps: ['5ca89e37ef1f7e010007f54c'],
          },
        });

        expect(currentURL()).to.equal(`/manage/sites/${feature._id}`);
      });

      it('can change the map', async function () {
        authenticateSession();

        await visit(`/add/feature`);

        this.element.querySelector('.row-map select').value =
          '000000000000000000000002';
        await triggerEvent('.row-map select', 'change');

        expect(this.element.querySelector('.row-map select').value).to.equal(
          '000000000000000000000002'
        );
      });
    });

    describe('when the server request fails', function () {
      beforeEach(function () {
        server.post('/mock-server/features', () => {
          return [
            400,
            { 'Content-Type': 'application/json' },
            JSON.stringify({
              errors: [
                {
                  description:
                    'The `Feature.description` field is required: {"info":{"createdOn":"2020-06-08T10:46:16Z","changeIndex":0,"lastChangeOn":"2020-06-08T10:46:16Z","originalAuthor":"5b870669796da25424540deb","author":"5b870669796da25424540deb"},"maps":["5ca89e2fef1f7e010007f50a"],"contributors":["5b870669796da25424540deb"],"name":"dsgf","visibility":0,"position":{"type":"Point","coordinates":[5.189189189189191,29.98833891386687]}}',
                  status: 400,
                  title: 'Validation error',
                },
              ],
            }),
          ];
        });
      });

      it('shows a modal with the error message', async function () {
        this.timeout(10000);
        authenticateSession();

        await visit(`/add/feature`);

        await fillIn('.row-name input.form-control', 'new feature name');

        this.element.querySelector('.row-map select').value = map._id;
        await triggerEvent('.row-map select', 'change');

        await click('.btn-paste');

        const editor = this.element.querySelector('.input-geo-json').editor;
        editor.setMode('text');

        this.element.querySelector('.jsoneditor-text').value = '';
        await fillIn(
          '.jsoneditor-text',
          `{ "type": "Point", "coordinates": [1, 2] }`
        );

        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(currentURL()).to.equal(`/add/feature`);

        await Modal.waitToDisplay();

        expect(Modal.title()).to.equal('Validation error');
        expect(Modal.message()).to.equal(
          'The `Feature.description` field is required: ' +
            '{"info":{"createdOn":"2020-06-08T10:46:16Z","changeIndex":0,"lastChangeOn":"2020-06-08T10:46:16Z",' +
            '"originalAuthor":"5b870669796da25424540deb","author":"5b870669796da25424540deb"},"maps":' +
            '["5ca89e2fef1f7e010007f50a"],"contributors":["5b870669796da25424540deb"],"name":"dsgf",' +
            '"visibility":0,"position":{"type":"Point","coordinates":[5.189189189189191,29.98833891386687]}}'
        );
      });
    });
  });

  describe('when an admin user is authenticated', function () {
    beforeEach(() => {
      server.testData.storage.addDefaultUser(true);
    });

    it('can visit the page with an authenticated user', async function () {
      authenticateSession();

      await visit(`/add/feature`);
      expect(currentURL()).to.equal('/add/feature');
    });

    describe('when the server request is successfully', function () {
      beforeEach(function () {
        server.post('/mock-server/features', (request) => {
          receivedFeature = JSON.parse(request.requestBody);
          receivedFeature.feature._id = feature._id;

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedFeature),
          ];
        });
      });

      it('queries the maps without the all query param', async function () {
        authenticateSession();

        await visit(`/add/feature`);

        await fillIn('.row-name input.form-control', 'new feature name');

        this.element.querySelector('.row-map select').value = map._id;
        await triggerEvent('.row-map select', 'change');

        await click('.btn-paste');

        const editor = this.element.querySelector('.input-geo-json').editor;
        editor.setMode('text');

        this.element.querySelector('.jsoneditor-text').value = '';
        await fillIn(
          '.jsoneditor-text',
          `{ "type": "Point", "coordinates": [1, 2] }`
        );

        expect(server.history).to.contain('GET /mock-server/maps?canAdd=true');

        await click(this.element.querySelector('.btn.btn-primary.btn-submit'));

        expect(receivedFeature).to.deep.equal({
          feature: {
            _id: '5ca78e2160780601008f69e6',
            name: 'new feature name',
            description: '',
            position: { type: 'Point', coordinates: [1, 2] },
            visibility: 0,
            info: {},
            maps: ['5ca89e37ef1f7e010007f54c'],
          },
        });

        expect(currentURL()).to.equal(`/manage/sites/${feature._id}`);
      });

      it('queries the maps with the all query param after the admin button is pressed', async function () {
        authenticateSession();

        await visit(`/add/feature`);
        expect(server.history).to.contain('GET /mock-server/maps?canAdd=true');

        await click('.btn-show-all-teams');
        expect(server.history).to.contain(
          'GET /mock-server/maps?all=true&canAdd=true'
        );
      });
    });
  });
});
