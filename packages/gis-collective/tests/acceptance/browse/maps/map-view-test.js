import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  waitUntil,
  waitFor,
} from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';

describe('Acceptance | browse/maps/map-view', function () {
  setupApplicationTest();
  let server;
  let receivedMetric;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);

    server.server.get(`/mock-server/maps/:id/meta`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          publicSites: 220,
          totalContributors: 33,
        }),
      ];
    });

    receivedMetric = null;
    server.post(`/mock-server/metrics`, (request) => {
      receivedMetric = JSON.parse(request.requestBody);
      receivedMetric.metric._id = '1';

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedMetric),
      ];
    });
  });

  afterEach(function () {
    server.shutdown();
  });

  describe('when an icon is selected', function () {
    let icon;

    beforeEach(function () {
      icon = server.testData.storage.addDefaultIcon();
    });

    it('renders the selected icon as a pill', async function () {
      await visit(`/browse/maps/_/map-view?icons=${icon._id}`);

      expect(
        this.element.querySelector('.browse-icon-pill img')
      ).to.have.attribute(
        'src',
        'https://new.opengreenmap.org/api-v1/icons/5ca7bfbfecd8490100cab97d/image'
      );
    });

    it('deselects the icon when the pill is removed', async function () {
      await visit(`/browse/maps/_/map-view?icons=${icon._id}`);

      await click('.browse-icon-pill .btn-remove');

      expect(this.element.querySelector('.browse-icon-pill')).not.to.exist;
      expect(currentURL()).to.equal(`/browse/maps/_/map-view`);
    });
  });

  describe('when a map is available', function () {
    let map;
    let team;
    let site;

    beforeEach(function () {
      map = server.testData.create.map('5ca89e37ef1f7e010007f54c');
      site = server.testData.create.feature();
      team = server.testData.create.team();

      server.testData.storage.addMap(map);
      server.testData.storage.addFeature(site);
      server.testData.storage.addTeam(team);
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultBaseMap();
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultIconSet();
      server.testData.storage.addDefaultIcon();

      for (let i = 0; i < 10; i++) {
        server.testData.storage.addDefaultIcon(i);
      }
    });

    describe('and it is embedded', function () {
      it('pushes a metric when the map is loaded', async function () {
        await visit(`/browse/maps/${map._id}/map-view?embed=true`);
        await waitUntil(() => receivedMetric);

        expect(receivedMetric.metric).to.deep.contain({
          _id: '1',
          name: '5ca89e37ef1f7e010007f54c',
          type: 'mapView',
          value: 1,
        });
      });

      it('does not render the main bar when the `embed` query param is `true`', async function () {
        await visit(`/browse/maps/${map._id}/map-view?embed=true`);
        await waitFor('.show-map');
        await waitFor('.show-team');

        expect(this.element.querySelector('.navbar')).not.to.exist;
      });

      it('renders map info links with target=_blank and the href without embedded query param', async function () {
        await visit(`/browse/maps/${map._id}/map-view?embed=true`);
        await waitFor('.show-map');
        await waitFor('.show-team');

        expect(
          this.element.querySelector('.btn-map-features')
        ).to.have.attribute('target', '_blank');
        expect(this.element.querySelector('.show-map')).to.have.attribute(
          'target',
          '_blank'
        );
        expect(this.element.querySelector('.show-team')).to.have.attribute(
          'target',
          '_blank'
        );
        expect(this.element.querySelector('.show-map')).to.have.attribute(
          'href',
          `/browse/sites?map=${map._id}`
        );
        expect(this.element.querySelector('.show-team')).to.have.attribute(
          'href',
          `/browse/teams/${team._id}`
        );
      });

      it('renders the full screen button', async function () {
        await visit(`/browse/maps/${map._id}/map-view?embed=true`);
        await waitFor('.btn-group-zoom');

        expect(this.element.querySelector('.btn-fullscreen')).to.exist;
      });
    });

    describe('and it is allowed to be downloaded and it has a license', function () {
      beforeEach(function () {
        map = server.testData.create.map('5ca89e37ef1f7e010007f54d');
        map.showPublicDownloadLinks = true;
        map.license = {
          name: 'name',
          url: 'url',
        };
        server.testData.storage.addMap(map);
      });

      it('renders the download button', async function () {
        await visit(`/browse/maps/${map._id}/map-view`);
        await waitFor('.btn-group-zoom');

        expect(this.element.querySelector('.disable .btn-map-download')).to
          .exist;
      });

      it('renders the license', async function () {
        await visit(`/browse/maps/${map._id}/map-view`);
        await waitFor('.btn-group-zoom');

        expect(this.element.querySelector('.disable .license')).to.exist;
        expect(
          this.element.querySelector('.disable .license').textContent
        ).to.contain('name');
      });
    });

    describe('and a masked site is available', function () {
      beforeEach(function () {
        site.isMasked = true;
      });

      it('displays the masked info message when opening the side-bar', async function () {
        this.timeout(10000);
        await visit(`/browse/maps/${map._id}/map-view/${site._id}`);

        expect(
          this.element.querySelector('.masked-info').textContent
        ).to.contain(
          'Location masking is applied, so the location displayed publicly will be imprecise.'
        );
      });
    });

    it('does not render the full screen button', async function () {
      await visit(`/browse/maps/${map._id}/map-view`);
      await waitFor('.btn-group-zoom');

      expect(this.element.querySelector('.disable .btn-fullscreen')).to.exist;
    });

    it('does not render the license', async function () {
      await visit(`/browse/maps/${map._id}/map-view`);
      await waitFor('.btn-group-zoom');

      expect(this.element.querySelector('.disable .license')).not.to.exist;
    });

    it('does not render the download button', async function () {
      await visit(`/browse/maps/${map._id}/map-view`);
      await waitFor('.btn-group-zoom');

      expect(this.element.querySelector('.disable .btn-map-download')).not.to
        .exist;
    });

    it('sets the map extent if one is not set', async function () {
      await visit(`/browse/maps/${map._id}/map-view`);

      await waitFor('.map-inner .map-view');
      await waitUntil(
        () =>
          this.element
            .querySelector('.map-inner .map-view')
            .attributes.getNamedItem('data-extent-view').value != ''
      );

      const extent = this.element
        .querySelector('.map-inner .map-view')
        .attributes.getNamedItem('data-extent-view')
        .value.split(',')
        .map((a) => parseFloat(a));

      expect(extent.length).to.eq(4);
      expect(extent[0]).to.be.below(-120);
      expect(extent[0]).to.be.above(-125);

      expect(extent[1]).to.be.above(46);
      expect(extent[1]).to.be.below(48);

      expect(extent[2]).to.be.below(-120);
      expect(extent[2]).to.be.above(-125);

      expect(extent[3]).to.be.above(47);
      expect(extent[3]).to.be.below(49);
    });

    it('queries icons when the icon filter is opened', async function () {
      await visit(`/browse/maps/${map._id}/map-view`);
      await waitFor('.show-map');

      await click('.input-search');

      server.history = [];
      await click('.btn-view-all-icons');
      await waitUntil(() => server.history.length > 0);

      await waitUntil(
        () => server.history.filter((a) => a.indexOf('icons') != -1).length > 0
      );

      const iconsRequest = server.history.filter(
        (a) => a.indexOf('icons') != -1
      )[0];

      expect(iconsRequest).to.contain(`group=true`);
      expect(iconsRequest).to.contain(`map=${map._id}`);
    });

    it('queries the map icons for the icons associated with that map', async function () {
      await visit(
        `/browse/maps/${map._id}/map-view?viewbox=13.412649102184016,52.49050249999996,13.447435697815981,52.50050250000001`
      );
      await waitFor('.show-map');

      await click('.input-search');

      server.history = [];
      await click('.btn-view-all-icons');
      await waitUntil(() => server.history.length > 0);

      await waitUntil(
        () => server.history.filter((a) => a.indexOf('icons') != -1).length > 0
      );
      const iconsRequest = server.history.filter(
        (a) => a.indexOf('icons') != -1
      )[0];

      expect(iconsRequest).to.contain(`map=${map._id}`);
      expect(this.element.querySelector('.navbar')).to.exist;
      expect(this.element.querySelector('.show-map')).to.have.attribute(
        'target',
        '_self'
      );
      expect(this.element.querySelector('.show-team')).to.have.attribute(
        'target',
        '_self'
      );
    });

    it('navigates to the map page from the map view', async function () {
      await visit(`/browse/maps/${map._id}/map-view`);

      await waitFor('.show-map');

      await click(this.element.querySelector('.show-map'));

      expect(currentURL()).to.equal(`/browse/sites?map=${map._id}`);
    });

    it('resets the query params when the route is changed', async function () {
      this.timeout(10000);
      await visit(`/browse/maps/${map._id}/map-view?viewbox=24%2C46%2C25%2C46`);
      const originalUrl = currentURL();

      await waitFor('.show-map');
      await click('.show-map');

      await waitFor('.show-map');
      await click('.show-map');

      expect(currentURL()).to.startWith(`/browse/maps/${map._id}/map-view`);
      expect(currentURL()).not.to.contain(originalUrl);
    });

    it('clears the content when going back to map view after using Show more for a site', async function () {
      await visit(`/browse/maps/${map._id}/map-view/${site._id}`);

      await click('.btn-open');

      await waitFor(`a[href*="map=${map._id}"]`);
      await click(`a[href*="map=${map._id}"]`);

      await waitFor('.show-map');
      await click('.show-map');

      expect(this.element.querySelector('.has-article')).not.to.exist;
      expect(this.element.querySelector('.has-content')).not.to.exist;
    });

    it('hides the map info when the filter is open', async function () {
      this.timeout(10000);
      await visit(`/browse/maps/${map._id}/map-view?viewbox=24%2C46%2C25%2C46`);

      expect(this.element.querySelector('.map-attributions.is-visible')).to
        .exist;

      await click('.input-search');

      server.history = [];
      await click('.btn-view-all-icons');
      await waitUntil(() => server.history.length > 0);

      expect(this.element.querySelector('.map-attributions.is-visible')).not.to
        .exist;
    });

    it('does not display the site masked info when opening the site side-bar', async function () {
      this.timeout(10000);
      await visit(`/browse/maps/${map._id}/map-view/${site._id}`);

      expect(this.element.querySelector('.masked-info')).to.not.exist;
    });
  });
});
