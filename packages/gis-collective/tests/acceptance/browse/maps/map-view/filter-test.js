import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, waitUntil } from '@ember/test-helpers';
import TestServer from '../../../../helpers/test-server';

describe('Acceptance | browse/maps/map-view/filter', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);

    server.server.get(`/mock-server/maps/:id/meta`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          publicSites: 220,
          totalContributors: 33,
        }),
      ];
    });
  });

  afterEach(function () {
    server.shutdown();
  });

  describe('on the map view', function () {
    let map;

    beforeEach(function () {
      map = server.testData.storage.addDefaultMap();
    });

    it('loads only icons from that map from the default viewbox on first load', async function () {
      await visit(`/browse/maps/${map._id}/map-view/filter-icons`);

      await waitUntil(
        () => server.history.filter((a) => a.indexOf('/icons') != -1).length > 0
      );
      expect(server.history).to.contain(
        `GET /mock-server/icons?map=${map._id}&group=true`
      );
    });
  });
});
