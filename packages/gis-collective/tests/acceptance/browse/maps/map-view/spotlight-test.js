import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, click } from '@ember/test-helpers';
import TestServer from '../../../../helpers/test-server';

describe('Acceptance | browse/maps/map-view/spotlight', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultIconSet('1');
    server.testData.storage.addDefaultIconSet('2');

    server.server.get(`/mock-server/maps/:id/meta`, () => [
      200,
      { 'Content-Type': 'application/json' },
      JSON.stringify({
        publicSites: 220,
        totalContributors: 33,
      }),
    ]);
  });

  afterEach(function () {
    server.shutdown();
  });

  describe('on the world view', function () {
    describe('when the server returns no features, icons or maps', function () {
      it('shows a list of icon sets', async function () {
        await visit('/browse/maps/_/map-view/spotlight');

        expect(this.element.querySelector('h5.icon-set-title')).not.to.exist;
        expect(this.element.querySelector('.no-data')).to.exist;
      });
    });

    describe('when the server returns icons', function () {
      beforeEach(function () {
        server.testData.storage.addDefaultIcon('1');
        server.testData.storage.addDefaultIcon('2');
      });

      it('shows a list of icon sets', async function () {
        await visit('/browse/maps/_/map-view/spotlight');

        expect(
          this.element.querySelector('h5.icon-set-title').textContent.trim()
        ).to.equal('Green Map® Icons Version 3');
        expect(this.element.querySelector('.no-data')).not.to.exist;

        expect(this.element.querySelector('.icon-name')).to.exist;
      });

      it('selects an icon when clicked', async function () {
        await visit('/browse/maps/_/map-view/spotlight');

        await click('.btn-icon');

        expect(currentURL()).to.contain('?icons=1');
      });
    });
  });
});
