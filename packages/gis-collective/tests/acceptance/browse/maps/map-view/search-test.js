import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, click } from '@ember/test-helpers';
import TestServer from '../../../../helpers/test-server';

describe('Acceptance | browse/maps/map-view/search', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);

    server.server.get(`/mock-server/maps/:id/meta`, () => [
      200,
      { 'Content-Type': 'application/json' },
      JSON.stringify({
        publicSites: 220,
        totalContributors: 33,
      }),
    ]);
  });

  afterEach(function () {
    server.shutdown();
  });

  describe('on the world view', function () {
    describe('when the server returns no features', function () {
      it('shows a message to the user', async function () {
        await visit('/browse/maps/_/map-view/s/test');

        expect(
          this.element.querySelector('.no-search-results').textContent.trim()
        ).to.equal('Your search - test - did not match any features.');
        expect(this.element.querySelector('.no-search-results-suggestions')).not
          .to.exist;
        expect(currentURL()).to.equal('/browse/maps/_/map-view/s/test');
      });
    });

    describe('when there are map, feature and places results', function () {
      let map;

      beforeEach(function () {
        map = server.testData.storage.addDefaultMap();

        for (let i = 0; i < 5; i++) {
          server.testData.storage.addDefaultFeature(i);
          server.testData.storage.addDefaultGeocoding(i);
        }

        server.testData.storage.addDefaultPicture();
        server.testData.storage.addDefaultIcon();
      });

      it('shows only the first 5 features and a button to show more', async function () {
        await visit('/browse/maps/_/map-view/s/test');

        expect(this.element.querySelector('.no-search-results')).not.to.exist;
        expect(this.element.querySelector('.no-search-results-suggestions')).not
          .to.exist;

        expect(this.element.querySelectorAll('.site-card').length).to.be.above(
          4
        );

        await click('.browse-all-sites');

        expect(currentURL()).to.equal(
          '/browse/maps/_/map-view/s/test?allFeatures=1'
        );
      });

      it('shows the places', async function () {
        await visit('/browse/maps/_/map-view/s/test');

        expect(this.element.querySelectorAll('.place-preview')).to.have.length(
          5
        );
        expect(
          this.element.querySelector('.place-preview').textContent
        ).to.containIgnoreCase(
          'Berlin, Coos County, New Hampshire, 03570, United States'
        );
      });

      it('shows the places licenses', async function () {
        await visit('/browse/maps/_/map-view/s/test');

        expect(this.element.querySelectorAll('.place-license')).to.have.length(
          1
        );
        expect(
          this.element.querySelector('.place-license').textContent
        ).to.containIgnoreCase('Data © OpenStreetMap contributors, ODbL 1.0');
        expect(
          this.element.querySelector('.place-license a')
        ).to.have.attribute('href', 'https://osm.org/copyright');
      });

      it('navigates to the map on click', async function () {
        await visit('/browse/maps/_/map-view/s/test');

        await click('.map-name');

        expect(currentURL()).to.equal(`/browse/maps/${map._id}/map-view`);
      });
    });

    describe('when there are only feature results', function () {
      beforeEach(function () {
        for (let i = 0; i < 5; i++) {
          server.testData.storage.addDefaultFeature(i);
        }

        server.testData.storage.addDefaultPicture();
        server.testData.storage.addDefaultIcon();
      });

      it('does not render the places list', async function () {
        await visit('/browse/maps/_/map-view/s/test');
        expect(
          this.element.querySelector('.search-results').textContent
        ).not.to.containIgnoreCase(`places`);
      });

      it('does not render the map list', async function () {
        await visit('/browse/maps/_/map-view/s/test');
        expect(
          this.element.querySelector('.search-results').textContent
        ).not.to.containIgnoreCase(`maps`);
      });
    });
  });

  describe('on the map view', function () {
    let map;

    beforeEach(function () {
      map = server.testData.storage.addDefaultMap();
    });

    describe('when the server returns no features', function () {
      it('shows a message with suggestions to the user', async function () {
        await visit(`/browse/maps/${map._id}/map-view/s/test`);

        expect(
          this.element.querySelector('.no-search-results').textContent.trim()
        ).to.equal('Your search - test - did not match any features.');
        expect(this.element.querySelector('.no-search-results-suggestions')).to
          .exist;
      });
    });

    describe('when there are results', function () {
      beforeEach(function () {
        for (let i = 0; i < 5; i++) {
          server.testData.storage.addDefaultFeature(i);
        }

        server.testData.storage.addDefaultPicture();
        server.testData.storage.addDefaultIcon();
      });

      it('shows all features and hides the show more button', async function () {
        await visit(`/browse/maps/${map._id}/map-view/s/test`);

        expect(this.element.querySelector('.no-search-results')).not.to.exist;
        expect(this.element.querySelector('.no-search-results-suggestions')).not
          .to.exist;

        expect(this.element.querySelectorAll('.site-card').length).to.be.above(
          4
        );
        expect(this.element.querySelector('.browse-all-sites')).not.to.exist;
      });
    });
  });
});
