import { describe, it, before, after } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit } from '@ember/test-helpers';
import TestServer from '../../../../helpers/test-server';

describe('Acceptance | browse/maps/map-view/feature', function () {
  setupApplicationTest();
  let server;
  let feature;

  before(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultIcon();
    feature = server.testData.storage.addDefaultFeature();

    server.server.get(`/mock-server/maps/:id/meta`, () => [
      200,
      { 'Content-Type': 'application/json' },
      JSON.stringify({
        publicSites: 220,
        totalContributors: 33,
      }),
    ]);
  });

  after(function () {
    server.shutdown();
  });

  it('shows the panel as a feature', async function () {
    await visit(`/browse/maps/_/map-view/${feature._id}`);

    expect(this.element.querySelector('.map-search-bis')).to.have.class(
      'has-feature-content'
    );
  });
});
