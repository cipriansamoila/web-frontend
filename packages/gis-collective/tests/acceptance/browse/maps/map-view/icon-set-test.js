import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, click, typeIn, fillIn } from '@ember/test-helpers';
import TestServer from '../../../../helpers/test-server';

describe('Acceptance | browse/maps/map-view/icon-set', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultIconSet('1');
    server.testData.storage.addDefaultIconSet('2');

    server.server.get(`/mock-server/maps/:id/meta`, () => [
      200,
      { 'Content-Type': 'application/json' },
      JSON.stringify({
        publicSites: 220,
        totalContributors: 33,
      }),
    ]);
  });

  afterEach(function () {
    server.shutdown();
  });

  describe('when the server returns icons', function () {
    beforeEach(function () {
      let icon1 = server.testData.storage.addDefaultIcon('1');
      server.testData.storage.addDefaultIcon('2');

      icon1.name = 'some icon';
      icon1.localName = 'some icon';
    });

    it('shows a list of icon sets', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1');

      expect(this.element.querySelector('.icon-set-wrapper')).to.exist;
      expect(this.element.querySelector('.icon-name')).to.exist;
      expect(this.element.querySelector('.btn-spotlight-back')).to.exist;
      expect(
        this.element.querySelector('h5.icon-set-title').textContent.trim()
      ).to.equal('Green Map® Icons Version 3');
    });

    it('does not change the path when the search input is focused', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1');
      await click('.input-search');

      expect(this.element.querySelector('.icon-set-wrapper')).to.exist;
    });

    it('selects an icon when clicked', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1');

      await click('.btn-icon');

      expect(currentURL()).to.contain('?icons=1');
    });

    it('navigates to the spotlight when the btn back button is pressed', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1');
      await click('.btn-spotlight-back');

      expect(currentURL()).to.equal('/browse/maps/_/map-view/spotlight');
    });

    it('filters icons by name', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1');

      await fillIn('.input-search', 'some i');

      expect(this.element.querySelectorAll('.icon-name').length).to.equal(1);
      expect(
        this.element.querySelector('.icon-name').textContent.trim()
      ).to.equal('some icon');

      expect(currentURL()).to.equal(
        '/browse/maps/_/map-view/icon-set/1?search=some%20i'
      );
    });

    it('hides the menu and fills in the search term when is loaded', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1?search=some%20i');

      expect(this.element.querySelector('.bar-invisible')).to.exist;

      expect(this.element.querySelector('.input-search').value).to.equal(
        'some i'
      );
      expect(this.element.querySelectorAll('.icon-name').length).to.equal(1);
      expect(
        this.element.querySelector('.icon-name').textContent.trim()
      ).to.equal('some icon');
    });

    it('removes the term when going back', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1?search=some%20i');

      await typeIn('.input-search', 'some i');
      await click('.btn-spotlight-back');

      expect(this.element.querySelector('.input-search').value).to.equal('');
      expect(currentURL()).to.equal('/browse/maps/_/map-view/spotlight');
    });

    it('removes the term when an icon is selected', async function () {
      await visit('/browse/maps/_/map-view/icon-set/1?search=some%20i');

      await fillIn('.input-search', 'some i');
      await click('.btn-icon');

      expect(this.element.querySelector('.input-search').value).to.equal('');
      expect(currentURL()).to.equal('/browse/maps/_/map-view?icons=1');
    });
  });
});
