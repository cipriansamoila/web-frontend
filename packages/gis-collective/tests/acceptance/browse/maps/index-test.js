import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, click, fillIn, waitFor } from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import { authenticateSession } from 'ember-simple-auth/test-support';
import Modal from '../../../helpers/modal';

describe('Acceptance | browse/maps/index', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultIconSet();
  });

  afterEach(function () {
    Modal.clear();
    server.shutdown();
  });

  it('can visit /browse/maps', async function () {
    await visit('/browse/maps');
    expect(currentURL()).to.equal('/browse/maps');
  });

  it('should search maps by name', async function () {
    await visit('/browse/maps');

    await fillIn('.main-container .input-search', 'search something');

    server.history = [];
    await click('.btn-search');
    expect(currentURL()).to.equal(`/browse/maps?search=search%20something`);

    expect(
      server.history.filter((a) => a.indexOf('/maps') != -1)[0]
    ).to.contain('term=search+something');
  });

  it('should switch to the list mode on list click', async function () {
    await visit('/browse/maps');

    await click('.btn-list');
    expect(currentURL()).to.equal(`/browse/maps?viewMode=list`);
  });

  it('should switch to the card mode on card click', async function () {
    await visit('/browse/maps?viewMode=list');

    await click('.btn-card-deck');
    expect(currentURL()).to.equal(`/browse/maps`);
  });

  it('should not show the add map button', async function () {
    await visit('/browse/maps?viewMode=list');

    expect(this.element.querySelector('.btn-add-map')).not.to.exist;
  });

  describe('area filtering', function () {
    let area;

    beforeEach(function () {
      area = server.testData.create.area('000000216078060100000000');
      server.testData.storage.addArea(area);
    });

    it('displays area filtering', async function () {
      await visit('/browse/maps');

      expect(this.element.querySelector('.btn-pill-area')).to.exist;
    });

    it('filters by area', async function () {
      await visit('/browse/maps?area=Berlin');

      expect(server.history).to.contain(
        'GET /mock-server/maps?area=Berlin&limit=25&page=1&per_page=25&skip=0'
      );
    });

    it('removes filters when area filtering is removed', async function () {
      await visit('/browse/maps?area=Berlin');

      await click('.btn-reset');

      expect(server.history).to.contain(
        'GET /mock-server/maps?limit=25&page=1&per_page=25&skip=0'
      );
    });
  });

  describe('team filtering', function () {
    beforeEach(async function () {
      await visit('/browse/maps');
    });

    it('should be invisible', async function () {
      expect(this.element.querySelector('.btn-pill-team')).not.to.exist;
    });

    it('should not query the editable teams', async function () {
      expect(server.history).not.to.contain(`GET /mock-server/teams?edit=true`);
    });
  });

  describe('visibility filtering', function () {
    beforeEach(async function () {
      await visit('/browse/maps');
    });

    it('should be invisible', async function () {
      expect(this.element.querySelector('.btn-pill-visibility')).not.to.exist;
    });

    it('should not query the unpublished teams', async function () {
      expect(server.history).not.to.contain(
        `GET /mock-server/teams?published=false`
      );
    });
  });

  describe('when a public map is available', function () {
    let mapId;
    let map;

    beforeEach(function () {
      mapId = '5ca89e37ef1f7e010007f54c';
      map = server.testData.create.map(mapId);

      server.testData.storage.addMap(map);
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultBaseMap();
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultTeam();

      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            publicSites: 220,
            totalContributors: 33,
          }),
        ];
      });
    });

    it('is listed as public', async function () {
      await visit('/browse/maps/');

      expect(this.element.querySelector('.icon-not-published')).to.not.exist;
    });
  });

  describe('when a private map is available', function () {
    let mapId;
    let map;

    beforeEach(function () {
      mapId = '5ca89e37ef1f7e010007f54c';
      map = server.testData.create.map(mapId);
      map.visibility.isPublic = false;

      server.testData.storage.addMap(map);
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultBaseMap();
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultTeam();

      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            publicSites: 220,
            totalContributors: 33,
          }),
        ];
      });
    });

    it('is listed as Not published', async function () {
      await visit('/browse/maps/');

      expect(this.element.querySelector('.icon-not-published')).to.exist;
    });
  });

  describe('when the user is authenticated', function () {
    let team;

    beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultIcon();
      server.testData.storage.addDefaultUser(true, '5b8a59caef739394031a3f67');
      team = server.testData.storage.addDefaultTeam();
    });

    describe('team filtering', function () {
      beforeEach(async function () {
        await visit('/browse/maps');
      });

      it('should be visible', async function () {
        expect(this.element.querySelector('.btn-pill-team')).to.exist;
      });

      it('should be able to search and select a team', async function () {
        await click('.btn-pill-team');
        await fillIn('.popup-filter input', 'some text');

        await click('.popup-filter .list-group-item-action');

        expect(server.history).to.contain(
          `GET /mock-server/maps?limit=25&page=1&per_page=25&skip=0&team=${team._id}`
        );
        expect(currentURL()).to.equal(`/browse/maps?team=${team._id}`);
      });

      it('should be able to clear the selected team', async function () {
        authenticateSession();

        await visit(`/browse/maps?team=${team._id}`);

        await waitFor('.btn-reset-pill-team');
        await click('.btn-reset-pill-team');

        expect(
          this.element.querySelector('.btn-pill-team').textContent.trim()
        ).to.equal('team');
        expect(currentURL()).to.equal(`/browse/maps`);
      });

      it('should show the editable teams when the team filter is opened', async function () {
        await click('.btn-pill-team');
        expect(server.history).to.contain(`GET /mock-server/teams?edit=true`);

        expect(
          this.element
            .querySelector('.popup-filter .list-group-item-action')
            .textContent.trim()
        ).to.equal(team.name);
      });
    });

    describe('visibility filtering', function () {
      beforeEach(async function () {
        await visit('/browse/maps');
      });

      it('should be visible', async function () {
        expect(this.element.querySelector('.btn-pill-visibility')).to.exist;
      });

      it('should query the unpublished maps', async function () {
        await visit(`/browse/maps?published=false`);
        expect(server.history).to.contain(
          `GET /mock-server/maps?limit=25&page=1&per_page=25&published=false&skip=0`
        );
      });

      it('should be able to select a visibility value', async function () {
        expect(server.history).to.contain(
          `GET /mock-server/maps?limit=25&page=1&per_page=25&skip=0`
        );

        await click('.btn-pill-visibility');
        await click('.btn-show-published');

        expect(currentURL()).to.equal(`/browse/maps?published=true`);
        expect(
          this.element.querySelector('.btn-pill-visibility').textContent.trim()
        ).to.equal('only published');
      });

      it('should be able to clear the selected visibility', async function () {
        this.timeout(15000);
        authenticateSession();

        await visit(`/browse/maps?published=true`);

        await waitFor('.btn-reset-pill-visibility');
        await click('.btn-reset-pill-visibility');

        expect(currentURL()).to.equal(`/browse/maps`);
        expect(
          this.element.querySelector('.btn-pill-visibility').textContent.trim()
        ).to.equal('visibility');
      });
    });

    describe('when there are only published items', function () {
      beforeEach(function () {
        authenticateSession();

        for (let i = 0; i < 25; i++) {
          const map = server.testData.create.map(i + 1);
          map.canEdit = true;
          server.testData.storage.addMap(map);

          server.delete_(`/mock-server/maps/${map._id}`, () => [201, {}, '{}']);
        }
      });

      describe('using the card view', function () {
        beforeEach(async function () {
          await visit('/browse/maps');
        });

        it('should be able to delete a map', async function () {
          this.timeout(15000);

          await click('.map-card .btn-extend');

          await waitFor('.map-card .btn-delete');
          await click('.map-card .btn-delete');

          await Modal.waitToDisplay();
          await Modal.clickDangerButton();

          //expect(server.history.filter(a => a.indexOf("DELETE") != -1)[0]).to.contain(`/mock-server/features/1`);
        });
      });

      describe('using the table view', function () {
        beforeEach(async function () {
          server.server.get(`/mock-server/maps/1/meta`, () => {
            return [
              200,
              { 'Content-Type': 'application/json' },
              JSON.stringify({
                publicSites: 220,
                totalContributors: 33,
              }),
            ];
          });
          await visit(`/browse/maps?viewMode=list`);
        });

        it('should be able to delete a map', async function () {
          this.timeout(15000);

          await click('td .dropdown-toggle');
          await click('.dropdown-menu .btn-delete');

          await Modal.waitToDisplay();
          await Modal.clickDangerButton();

          //expect(server.history.filter(a => a.indexOf("DELETE") != -1)[0]).to.contain(`/mock-server/maps/1`);
        });

        it('should not show the publish option for a selected map', async function () {
          this.timeout(15000);
          await click('.chk-select');

          expect(this.element.querySelector('.navbar .btn-publish')).not.to
            .exist;
          expect(this.element.querySelector('.navbar .btn-unpublish')).to.exist;
        });

        it('should be able to unpublish a map', async function () {
          this.timeout(15000);
          await click('.chk-select');

          await click(this.element.querySelector('.navbar .btn-unpublish'));
        });

        it("should not display the 'not published' icon", async function () {
          this.timeout(15000);
          expect(this.element.querySelector('.icon-not-published')).not.to
            .exist;
        });

        it('should be able to edit the map', async function () {
          await visit(`/browse/maps?viewMode=list`);

          await click('.btn-open-options');
          await click('.dropdown-item-edit');

          expect(currentURL()).to.equal('/manage/maps/1');
        });

        it('should be able to go to the file import page', async function () {
          await visit(`/browse/maps?viewMode=list`);

          await click('.btn-open-options');
          await click('.dropdown-item-file-import');

          expect(currentURL()).to.equal('/manage/maps/1/import');
        });

        it('should be able to open the sites list', async function () {
          await visit(`/browse/maps?viewMode=list`);

          await click('.btn-open-options');

          await click('.dropdown-item-view-features');
          expect(currentURL()).to.equal('/browse/sites?map=1');
        });
      });
    });
  });
});
