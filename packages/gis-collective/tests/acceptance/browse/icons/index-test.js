import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, click, fillIn } from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';
import PageElements from '../../../helpers/page-elements';
import { authenticateSession } from 'ember-simple-auth/test-support';
import waitUntil from '@ember/test-helpers/wait-until';

describe('Acceptance | browse/icons/index', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
  });

  afterEach(function () {
    Modal.clear();
    server.shutdown();
  });

  it('can visit /browse/icons', async function () {
    await visit('/browse/icons');
    expect(currentURL()).to.equal('/browse/icons');
  });

  describe('when there is an icon set', function () {
    let iconSet;

    beforeEach(function () {
      iconSet = server.testData.storage.addDefaultIconSet();
      server.testData.storage.addDefaultPicture();
    });

    it('should search for icon sets', async function () {
      await visit('/browse/icons');

      await fillIn('.main-container .input-search', 'search something');

      server.history = [];
      await click('.btn-search');
      expect(currentURL()).to.equal(`/browse/icons?search=search%20something`);

      expect(
        server.history.filter((a) => a.indexOf('/iconsets') != -1)[0]
      ).to.contain('term=search+something');
    });

    it('should render the item as card', async function () {
      await visit('/browse/icons');
      await waitUntil(() =>
        this.element
          .querySelector('.icon-set-card .cover-image')
          .attributes.getNamedItem('style')
      );

      expect(
        this.element.querySelectorAll('.icon-set-card').length
      ).to.be.greaterThan(0);
      expect(
        this.element
          .querySelector('.icon-set-card .icon-set-name')
          .textContent.trim()
      ).to.equal('Green Map® Icons Version 3');
      expect(
        this.element
          .querySelector('.icon-set-card .icon-set-description')
          .textContent.trim()
      ).to.equal('description');
      expect(
        this.element.querySelector('.icon-set-card .icon-set-name')
      ).to.have.attribute('href', `/browse/icons/${iconSet._id}`);

      expect(
        this.element.querySelector('.icon-set-card .cover-image')
      ).to.have.attribute(
        'style',
        `background-image: url(https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture/sm)`
      );
    });

    it('should render the item as table', async function () {
      await visit('/browse/icons?viewMode=list');

      expect(this.element.querySelectorAll('.icon-set-card').length).to.equal(
        0
      );
      expect(
        this.element.querySelectorAll('tbody tr').length
      ).to.be.greaterThan(0);

      expect(
        this.element.querySelector('tbody tr .name').textContent.trim()
      ).to.equal('Green Map® Icons Version 3');
      expect(
        this.element.querySelector('tbody .description').textContent.trim()
      ).to.equal('description');
    });

    it('should render the breadcrumbs', async function () {
      await visit('/browse/icons');

      expect(PageElements.breadcrumbs()).to.deep.equal(['Browse', 'Icons']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal(['/browse']);
    });
  });

  describe('when the user is authenticated and there is a private icon set', function () {
    let iconSet;

    beforeEach(function () {
      authenticateSession();

      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultIcon();
      server.testData.storage.addDefaultUser(true);

      iconSet = server.testData.create.iconSet();
      iconSet.visibility.isPublic = false;
      iconSet.canEdit = false;

      server.testData.storage.addIconSet(iconSet);
    });

    it('should render the not published icon on the card view', async function () {
      await visit('/browse/icons');

      expect(this.element.querySelector('.icon-set-card .icon-not-published'))
        .to.exist;
    });
  });

  describe('when the user is authenticated and there is a public icon set', function () {
    let iconSet;

    beforeEach(function () {
      authenticateSession();

      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultIcon();
      server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultUser(true);

      iconSet = server.testData.create.iconSet();
      iconSet['canEdit'] = true;

      server.testData.storage.addIconSet(iconSet);

      server.delete_(`/mock-server/iconsets/${iconSet._id}`, () => [
        201,
        {},
        '{}',
      ]);
    });

    afterEach(function () {
      Modal.clear();
    });

    it('should allow edit from the card view', async function () {
      await visit('/browse/icons');

      await click('.dropdown .btn-extend');
      await click('.dropdown .dropdown-item-edit');

      expect(currentURL()).to.equal(
        '/manage/icons/edit/5ca7b702ecd8490100cab96f'
      );
    });

    it('should allow edit from the table view', async function () {
      await visit('/browse/icons?viewMode=list');

      await click('.dropdown .btn-extend');
      await click('.dropdown .dropdown-item-edit');

      expect(currentURL()).to.equal(
        '/manage/icons/edit/5ca7b702ecd8490100cab96f'
      );
    });

    it('should allow delete from the card view', async function () {
      await visit('/browse/icons');

      await click('.dropdown .btn-extend');
      await click('.dropdown .btn-delete');

      await Modal.waitToDisplay();
      await Modal.clickDangerButton();

      expect(server.history).to.contain(
        `DELETE /mock-server/iconsets/${iconSet._id}`
      );
    });

    /*
    it("should allow delete from the table view", async function() {
      await visit('/browse/icons?viewMode=list');

      await click(".dropdown .btn-extend");
      await click(".dropdown .btn-delete");

      await Modal.waitToDisplay();
      await Modal.clickDangerButton();

      expect(server.history).to.contain(`DELETE /mock-server/iconsets/${iconSet._id}`);
    });
    */
  });
});
