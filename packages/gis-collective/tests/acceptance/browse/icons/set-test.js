import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, click, fillIn } from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';
import PageElements from '../../../helpers/page-elements';
import { authenticateSession } from 'ember-simple-auth/test-support';

describe('Acceptance | browse/icons/set', function() {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
  });

  afterEach(function () {
    Modal.clear();
    server.shutdown();
  });

  it('can visit /browse/icons/set', async function() {
    const iconSet = server.testData.storage.addDefaultIconSet();
    await visit(`/browse/icons/${iconSet._id}`);

    expect(currentURL()).to.equal(`/browse/icons/${iconSet._id}`);
    expect(this.element.querySelector(".error")).not.to.exist;
  });

  describe("when there is an icon set with an icon", function() {
    let iconSet;
    let icon;

    beforeEach(function() {
      iconSet = server.testData.create.iconSet();
      iconSet.visibility.isPublic = false;
      iconSet.canEdit = false;

      server.testData.storage.addIconSet(iconSet);
      icon = server.testData.storage.addDefaultIcon();
      server.testData.storage.addDefaultPicture();
    });

    it("should render the icon as card", async function() {
      await visit(`/browse/icons/${iconSet._id}`);

      expect(this.element.querySelectorAll(".icon-card").length).to.be.greaterThan(0);
      expect(this.element.querySelector(".icon-card .icon-name").textContent.trim()).to.equal("Healthy Dining");
      expect(this.element.querySelector(".icon-card .icon-description").textContent.trim()).to.startWith(
        "The emphasis is on wholesome, healthful, fresh foods, made with local and/or organic ingredients.");

      expect(this.element.querySelector(".icon-card .icon-name")).to.have.attribute("href", `/browse/sites?icon=${icon._id}`);
      expect(this.element.querySelector(".icon-card img.icon")).to.have.attribute("src", `${icon.image.value}/md`);
    });

    it("should render the icon as table", async function() {
      await visit(`/browse/icons/${iconSet._id}?viewMode=list`);

      expect(this.element.querySelectorAll(".icon-card").length).to.equal(0);
      expect(this.element.querySelectorAll("tbody tr").length).to.be.greaterThan(0);

      expect(this.element.querySelector("tbody tr .name").textContent.trim()).to.equal("Healthy Dining");
      expect(this.element.querySelector("tbody .description").textContent.trim()).to.startWith(
        "The emphasis is on wholesome, healthful, fresh foods, made with local and/or organic ingredients.");
    });

    it("should render the breadcrumbs", async function() {
      await visit(`/browse/icons/${iconSet._id}`);

      expect(PageElements.breadcrumbs()).to.deep.equal(['Browse', 'Icons', 'Green Map® Icons Version 3']);
      expect(PageElements.breadcrumbLinks()).to.deep.equal(['/browse', '/browse/icons']);
    });

    it("should search for icons", async function() {
      await visit(`/browse/icons/${iconSet._id}`);

      await fillIn(".main-container .input-search", "search something");

      server.history = [];
      await click(".btn-search");
      expect(currentURL()).to.equal(`/browse/icons/${iconSet._id}?search=search%20something`);

      expect(server.history.filter(a => a.indexOf("/icons") != -1)[0]).to.contain("term=search+something");
    });

    it("should show the icon set article", async function() {
      await visit(`/browse/icons/${iconSet._id}`);

      expect(this.element.querySelector(".icon-set-article .btn-edit")).not.to.exist;
      expect(this.element.querySelector(".icon-set-article h1").textContent.trim()).to.equal(iconSet.name);
      expect(this.element.querySelector(".icon-set-article p").textContent.trim()).to.equal(iconSet.description);
    });
  });

  describe("when the user is authenticated and the data is editable", function() {
    let iconSet;
    let icon;

    beforeEach(function() {
      authenticateSession();

      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultTeam();
      server.testData.storage.addDefaultUser(true);

      iconSet = server.testData.create.iconSet();
      iconSet.visibility.isPublic = false;
      iconSet.canEdit = true;

      server.testData.storage.addIconSet(iconSet);

      icon = server.testData.create.icon();
      icon.canEdit = true;

      server.testData.storage.addIcon(icon);

      server.delete_(`/mock-server/icons/${icon._id}`, () => [ 201, {}, "{}" ]);
    });

    it("should allow edit the icon set", async function() {
      await visit(`/browse/icons/${iconSet._id}`);

      await click(".icon-set-article .btn-edit");

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}`);
    });

    it("should allow edit from the card view", async function() {
      await visit(`/browse/icons/${iconSet._id}`);

      await click(".dropdown .btn-extend");
      await click(".dropdown .dropdown-item-edit");

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
    });

    it("should allow delete from the card view", async function() {
      await visit(`/browse/icons/${iconSet._id}`);

      await click(".dropdown .btn-extend");
      await click(".dropdown .btn-delete");

      await Modal.waitToDisplay();
      await Modal.clickDangerButton();

      expect(server.history).to.contain(`DELETE /mock-server/icons/${icon._id}`);
    });

    it("should allow edit from the table view", async function() {
      await visit(`/browse/icons/${iconSet._id}?viewMode=list`);

      await click(".dropdown .btn-extend");
      await click(".dropdown .dropdown-item-edit");

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
    });
  });
});
