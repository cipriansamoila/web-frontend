import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  fillIn,
  waitFor,
  waitUntil,
} from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import { authenticateSession } from 'ember-simple-auth/test-support';

import Modal from '../../../helpers/modal';
import PageElements from '../../../helpers/page-elements';

describe('Acceptance | browse/sites/index', function () {
  setupApplicationTest();
  let server;
  let map;
  let icon;

  beforeEach(function () {
    server = new TestServer();
    map = server.testData.storage.addDefaultMap();
    server.testData.storage.addDefaultPreferences();
    icon = server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultUser(true);

    server.server.get(`/mock-server/maps/:id/meta`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          publicFeatures: 220,
          totalFeatures: 220,
          totalContributors: 33,
        }),
      ];
    });
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('can visit /browse/sites', async function () {
    await visit('/browse/sites');
    expect(currentURL()).to.equal('/browse/sites');
  });

  it('should be able to render the page without errors', async function () {
    authenticateSession();

    await visit(`/browse/sites`);

    expect(currentURL()).to.equal(`/browse/sites`);

    expect(this.element.querySelector('.error')).to.not.exist;
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'sites'
    );
  });

  it('should search sites by name', async function () {
    await visit('/browse/sites');

    await fillIn('.main-container .input-search', 'search something');

    server.history = [];
    await click('.btn-search');
    expect(currentURL()).to.equal(`/browse/sites?search=search%20something`);

    expect(
      server.history.filter((a) => a.indexOf('/features') != -1)[0]
    ).to.contain('term=search+something');
  });

  it('should not display the list options and checkboxes in the table view', async function () {
    authenticateSession();

    await visit(`/browse/sites?viewMode=list`);
    expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);

    expect(this.element.querySelector('td .dropdown-toggle')).not.to.exist;
    expect(this.element.querySelector('tbody .chk-select')).not.to.exist;
  });

  it('should not display the author filter', async function () {
    this.timeout(15000);

    await visit(`/browse/sites?viewMode=list`);
    expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);

    expect(this.element.querySelector('.btn-pill-author')).not.to.exist;
  });

  it('should not display the visibility filter', async function () {
    await visit(`/browse/sites?viewMode=list`);
    expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);

    expect(this.element.querySelector('.btn-pill-visibility')).not.to.exist;
  });

  describe('the area filter', function () {
    let area;

    beforeEach(function () {
      area = server.testData.create.area('000000216078060100000000');
      server.testData.storage.addArea(area);
    });

    it('should be visible', async function () {
      await visit('/browse/sites');

      expect(this.element.querySelector('.btn-pill-area')).to.exist;
    });

    it('filters by area', async function () {
      await visit('/browse/sites?area=Berlin');

      expect(server.history).to.contain(
        'GET /mock-server/features?area=Berlin&limit=25&page=1&per_page=25&skip=0'
      );
      expect(
        this.element.querySelector('.btn-pill-area').textContent.trim()
      ).to.equal('in Berlin');
    });

    it('removes filters when area filtering is removed', async function () {
      await visit('/browse/sites?area=Berlin');
      await click('.btn-reset');

      expect(server.history).to.contain(
        'GET /mock-server/features?limit=25&page=1&per_page=25&skip=0'
      );
    });
  });

  describe('the icon filter', function () {
    it('should be visible', async function () {
      this.timeout(15000);
      authenticateSession();

      await visit(`/browse/sites?viewMode=list`);
      expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);

      expect(this.element.querySelector('.btn-pill-icon')).to.exist;
    });

    it('should be able to clear the selected icon', async function () {
      this.timeout(15000);
      authenticateSession();

      await visit(`/browse/sites?viewMode=list&icon=5ca7bfc0ecd8490100cab980`);

      await waitFor('.btn-reset-pill-icon');
      await click('.btn-reset-pill-icon');

      expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);
      expect(
        this.element.querySelector('.btn-pill-icon').textContent.trim()
      ).to.equal('icon');
    });

    it('should be able to search an icon name', async function () {
      this.timeout(15000);
      authenticateSession();

      await visit(`/browse/sites?viewMode=list`);

      await click('.btn-pill-icon');
      await Modal.waitToDisplay();

      await fillIn('.icon-selector-filter', 'health');
      await click('.btn-icon');

      await click('.modal-footer .btn');
      await Modal.waitToHide();

      expect(currentURL()).to.equal(
        `/browse/sites?icon=5ca7bfc0ecd8490100cab980&viewMode=list`
      );
      expect(
        this.element.querySelector('.btn-pill-icon').textContent.trim()
      ).to.equal('with the Healthy Dining icon');
    });
  });

  describe('the map filter', function () {
    it('should be visible', async function () {
      this.timeout(15000);

      await visit(`/browse/sites?viewMode=list`);
      expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);

      expect(this.element.querySelector('.btn-pill-map')).to.exist;
    });

    it('should render the map article when is used', async function () {
      this.timeout(15000);

      await visit(`/browse/sites?viewMode=list&map=5ca89e37ef1f7e010007f54c`);
      expect(PageElements.breadcrumbs()).to.deep.equal([
        'Browse',
        'Sites',
        'Harta Verde București',
      ]);

      expect(this.element.querySelector('.team-owner')).to.exist;

      expect(this.element.querySelector('.map-article')).to.exist;
      expect(this.element.querySelector('.map-article .description')).to.exist;
      expect(this.element.querySelector('.map-article .tag-line')).to.exist;
      expect(this.element.querySelector('.map-article .btn-map-download')).not
        .to.exist;

      expect(
        this.element.querySelector('.map-article h1').textContent.trim()
      ).to.contain('Harta Verde București');
      expect(
        this.element.querySelector('.map-article .description').textContent
      ).to.containIgnoreSpaces(map.description);
      expect(
        this.element.querySelector('.map-article .tag-line').textContent
      ).to.containIgnoreSpaces(map.tagLine);
    });

    it('should render the map meta when it exists', async function () {
      this.timeout(15000);

      await visit(`/browse/sites?viewMode=list&map=5ca89e37ef1f7e010007f54c`);
      expect(PageElements.breadcrumbs()).to.deep.equal([
        'Browse',
        'Sites',
        'Harta Verde București',
      ]);

      expect(
        this.element.querySelector('.meta-container').textContent
      ).to.contain('220 published sites');
      expect(
        this.element.querySelector('.meta-container').textContent
      ).to.contain('220 sites');
      expect(
        this.element.querySelector('.meta-container').textContent
      ).to.contain('33 contributors');
    });

    it('should be able to clear the selected map', async function () {
      this.timeout(15000);

      await visit(`/browse/sites?viewMode=list&map=5ca89e37ef1f7e010007f54c`);

      await waitFor('.btn-reset-pill-map');
      await click('.btn-reset-pill-map');

      expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);
      expect(
        this.element.querySelector('.btn-pill-map').textContent.trim()
      ).to.equal('map');
    });

    it('should be able to search a map name', async function () {
      this.timeout(15000);
      authenticateSession();

      await visit(`/browse/sites?viewMode=list`);

      await click('.btn-pill-map');
      await fillIn('.popup-filter input', 'some text');

      await click('.popup-filter .list-group-item-action');
      await waitUntil(() => currentURL().indexOf('map=') != -1);

      expect(currentURL()).to.equal(
        `/browse/sites?map=5ca89e37ef1f7e010007f54c&viewMode=list`
      );
      expect(
        this.element.querySelector('.btn-pill-map').textContent.trim()
      ).to.equal('from the Harta Verde București map');
    });

    it('can access the map view from the article', async function () {
      await visit(`/browse/sites?viewMode=list&map=${map._id}`);

      await click('.show-map');
      expect(currentURL()).to.startWith(`/browse/maps/${map._id}/map-view`);
    });

    describe('when the map has sites', function () {
      beforeEach(function () {
        server.testData.storage.addDefaultFeature();
      });

      it('returns subsequent features pages when scrolling down the associated features', async function () {
        this.timeout(10000);

        await visit(`/browse/sites?map=${map._id}`);

        await waitFor('.infinity-loader');
        await waitUntil(
          () => this.element.querySelectorAll('.site-card').length > 0
        );

        const before = server.history.length;

        await waitUntil(
          () => {
            this.element.querySelector('.infinity-loader').scrollIntoView();
            return server.history.length > before;
          },
          { timeout: 2000 }
        );

        expect(server.history).to.contain(
          `GET /mock-server/features?limit=25&map=${map._id}&page=2&per_page=25&skip=25`
        );
      });
    });

    describe('selecting a map that is created by a private team', function () {
      beforeEach(function () {
        map.visibility.team = null;
        server.testData.storage.addMap(map);
      });

      it('should not display the team details', async function () {
        await visit(`/browse/sites?viewMode=list&map=${map._id}`);
        expect(this.element.querySelector('.team-owner')).not.to.exist;
      });
    });

    describe('when the isMultiProjectMode is false', function () {
      beforeEach(function () {
        server.testData.storage.addPreference(
          'access.isMultiProjectMode',
          'false'
        );
      });

      it('hides the breadcrumbs and the map filter', async function () {
        await visit(`/browse/sites?viewMode=list&map=${map._id}`);

        expect(document.querySelector('.breadcrumb')).not.to.exist;
        expect(document.querySelector('.btn-pill-map')).not.to.exist;
      });

      it('hides the breadcrumbs and the map filter', async function () {
        server.testData.storage.addDefaultUser(false, 'me');
        authenticateSession();

        await visit(`/browse/sites?viewMode=list&map=${map._id}`);
        expect(document.querySelector('.breadcrumb')).not.to.exist;
        expect(document.querySelector('.btn-pill-map')).not.to.exist;
      });

      it('shows the breadcrumbs and the map filter', async function () {
        server.testData.storage.addDefaultUser(true, 'me');
        authenticateSession();

        await visit(`/browse/sites?viewMode=list&map=${map._id}`);
        expect(document.querySelector('.breadcrumb')).to.exist;
        expect(document.querySelector('.btn-pill-map')).to.exist;
      });
    });
  });

  describe('When a public site is available', function () {
    let site;
    let picture;
    let icon;

    beforeEach(function () {
      site = server.testData.create.feature('5ca8baf2ef1f7e01000849ba');
      site.description = {
        time: 1609930497038,
        blocks: [
          { type: 'header', data: { level: 1, text: 'name' } },
          { type: 'paragraph', data: { text: 'desc' } },
        ],
        version: '2.19.0',
      };
      picture = server.testData.create.picture();
      icon = server.testData.create.icon();

      server.testData.storage.addFeature(site);
      server.testData.storage.addPicture(picture);
      server.testData.storage.addIcon(icon);
    });

    it("is shown without 'not published' icon in the grid view", async function () {
      await visit('/browse/sites/');

      expect(this.element.querySelector('.icon-not-published')).to.not.exist;
    });

    it("is shown without 'not published' icon in the list view", async function () {
      authenticateSession();

      await visit(`/browse/sites?viewMode=list`);

      expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);
      expect(this.element.querySelector('.icon-not-published')).not.to.exist;
    });

    it('displays the description in the list view', async function () {
      await visit(`/browse/sites?viewMode=list`);

      let tableTexts = PageElements.getTextFor('tr td');
      expect(tableTexts[0]).to.equal('desc');
    });

    it('returns subsequent features pages when scrolling down the associated features', async function () {
      for (let i = 0; i < 20; i++) {
        const feature = server.testData.create.feature(i + 1);
        feature.canEdit = true;
        server.testData.storage.addFeature(feature);
      }

      this.timeout(10000);

      await visit(`/browse/sites`);
      await waitFor('.infinity-loader');
      await waitUntil(
        () => this.element.querySelectorAll('.site-card').length > 0
      );

      const before = server.history.length;

      await waitUntil(
        () => {
          this.element.querySelector('.infinity-loader').scrollIntoView();
          return server.history.length > before;
        },
        { timeout: 2000 }
      );

      expect(server.history).to.contain(
        `GET /mock-server/features?limit=25&page=2&per_page=25&skip=25`
      );
    });
  });

  describe('When a private site is available', function () {
    let site;
    let picture;
    let icon;

    beforeEach(function () {
      site = server.testData.create.feature('5ca8baf2ef1f7e01000849ba');
      picture = server.testData.create.picture();
      icon = server.testData.create.icon();
      site.visibility = 0;

      server.testData.storage.addFeature(site);
      server.testData.storage.addPicture(picture);
      server.testData.storage.addIcon(icon);
    });

    it('is shown as `not published` in the grid view', async function () {
      await visit('/browse/sites');

      expect(this.element.querySelector('.icon-not-published')).to.exist;
    });

    it("is shown as 'not published' icon in the list view", async function () {
      authenticateSession();

      await visit(`/browse/sites?viewMode=list`);

      expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);
      expect(this.element.querySelector('.icon-not-published')).to.exist;
    });
  });

  describe('When the user is authenticated', function () {
    beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultIcon();
      server.testData.storage.addDefaultUser();
    });

    describe('the author filter', function () {
      it('should be visible', async function () {
        authenticateSession();

        await visit(`/browse/sites?viewMode=list`);
        expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);

        expect(this.element.querySelector('.btn-pill-author')).to.exist;
      });

      it('should be able to search by the authenticated user', async function () {
        this.timeout(15000);
        authenticateSession();

        await visit(`/browse/sites?viewMode=list`);

        await click('.btn-pill-author');
        await click('.btn-current-user');

        expect(currentURL()).to.equal(
          `/browse/sites?author=5b870669796da25424540deb&viewMode=list`
        );
        expect(
          this.element.querySelector('.btn-pill-author').textContent.trim()
        ).to.equal('created by you');

        expect(
          server.history.filter(
            (a) =>
              a.indexOf(
                'GET /mock-server/features?author=5b870669796da25424540deb'
              ) != -1
          ).length
        ).to.eq(1);
      });

      it('should be able to clear the selected author', async function () {
        this.timeout(15000);
        authenticateSession();

        await visit(
          `/browse/sites?viewMode=list&author=5b870669796da25424540deb`
        );

        await waitFor('.btn-reset-pill-author');
        await click('.btn-reset-pill-author');

        expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);
        expect(
          this.element.querySelector('.btn-pill-author').textContent.trim()
        ).to.equal('author');
      });

      it('should be able to search an author name', async function () {
        this.timeout(15000);
        authenticateSession();

        await visit(`/browse/sites?viewMode=list`);

        await click('.btn-pill-author');
        await fillIn('.popup-filter input', 'some text');

        await click('.popup-filter .list-group-item-action');

        expect(currentURL()).to.equal(
          `/browse/sites?author=5b870669796da25424540deb&viewMode=list`
        );
        expect(
          this.element.querySelector('.btn-pill-author').textContent.trim()
        ).to.equal('created by you');
      });
    });

    describe('the visibility filter', function () {
      it('should be visible', async function () {
        this.timeout(15000);
        authenticateSession();

        await visit(`/browse/sites?viewMode=list`);
        expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);

        expect(this.element.querySelector('.btn-pill-visibility')).to.exist;
      });

      it('should be able to clear the selected visibility', async function () {
        this.timeout(15000);
        authenticateSession();

        await visit(`/browse/sites?viewMode=list&visibility=1`);

        await waitFor('.btn-reset-pill-visibility');
        await click('.btn-reset-pill-visibility');

        expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);
        expect(
          this.element.querySelector('.btn-pill-visibility').textContent.trim()
        ).to.equal('visibility');
      });

      it('should be able to select a visibility value', async function () {
        this.timeout(15000);
        authenticateSession();

        await visit(`/browse/sites?viewMode=list`);

        await click('.btn-pill-visibility');
        await click('.btn-show-published');

        expect(currentURL()).to.equal(
          `/browse/sites?viewMode=list&visibility=1`
        );
        expect(
          this.element.querySelector('.btn-pill-visibility').textContent.trim()
        ).to.equal('only published');
      });
    });

    describe('When there are only published items', function () {
      let receivedSite;

      beforeEach(function () {
        for (let i = 0; i < 25; i++) {
          const feature = server.testData.create.feature(i + 1);
          feature.canEdit = true;
          server.testData.storage.addFeature(feature);

          server.delete_(`/mock-server/features/${feature._id}`, () => [201]);
          server.put(`/mock-server/features/${feature._id}`, (request) => {
            receivedSite = JSON.parse(request.requestBody);

            return [
              200,
              { 'Content-Type': 'application/json' },
              JSON.stringify(receivedSite),
            ];
          });
        }
      });

      it('should be able to delete a site using the card view', async function () {
        this.timeout(15000);
        authenticateSession();

        await visit('/browse/sites');

        await click('.site-card .btn-extend');

        await waitFor('.site-card .btn-delete');
        await click('.site-card .btn-delete');

        await Modal.waitToDisplay();
        await Modal.clickDangerButton();
        await Modal.waitToHide();

        expect(
          server.history.filter((a) => a.indexOf('DELETE') != -1)[0]
        ).to.contain(`/mock-server/features/1`);
      });

      it('should be able to delete a site using the table view', async function () {
        this.timeout(15000);
        authenticateSession();

        await visit(`/browse/sites?viewMode=list`);
        expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);

        await click('td .dropdown-toggle');
        await click('.dropdown-menu .btn-delete');

        await Modal.waitToDisplay();
        await Modal.clickDangerButton();
        await Modal.waitToHide();

        expect(
          server.history.filter((a) => a.indexOf('DELETE') != -1)[0]
        ).to.contain(`/mock-server/features/1`);
      });

      it('should not show the publish option for a selected site', async function () {
        authenticateSession();

        await visit(`/browse/sites?viewMode=list`);
        await click('tbody .chk-select');

        expect(this.element.querySelector('.navbar .btn-publish')).not.to.exist;
        expect(this.element.querySelector('.navbar .btn-unpublish')).to.exist;
      });

      it('should be able to unpublish a site', async function () {
        authenticateSession();

        await visit(`/browse/sites?viewMode=list`);
        await click('tbody .chk-select');

        await click(this.element.querySelector('.navbar .btn-unpublish'));

        expect(this.element.querySelector('.navbar .btn-unpublish')).not.to
          .exist;
        expect(this.element.querySelector('.navbar .btn-publish')).to.exist;

        expect(server.history).to.contain(`PUT /mock-server/features/1`);
        expect(receivedSite['feature']['visibility']).to.equal(0);
      });

      it('should be able to set a site as pending', async function () {
        authenticateSession();

        await visit(`/browse/sites?viewMode=list`);
        await click('tbody .chk-select');

        await click(this.element.querySelector('.navbar .btn-set-pending'));

        expect(this.element.querySelector('.navbar .btn-unpublish')).to.exist;
        expect(this.element.querySelector('.navbar .btn-publish')).to.exist;
        expect(this.element.querySelector('.navbar .btn-set-as-pending')).not.to
          .exist;
        expect(this.element.querySelector('.icon-pending')).to.exist;

        expect(server.history).to.contain(`PUT /mock-server/features/1`);
        expect(receivedSite['feature']['visibility']).to.equal(-1);
      });

      it("should not display the 'not published' icon in the list view", async function () {
        this.timeout(15000);

        await visit(`/browse/sites?viewMode=list`);
        expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);

        expect(this.element.querySelector('.icon-not-published')).not.to.exist;
      });

      describe('When navigating on the page via Show all', function () {
        beforeEach(function () {
          server.testData.storage.addDefaultUser(
            false,
            '5b8a59caef739394031a3f67'
          );
          server.testData.storage.addModel({
            _id: '000000000000000000000001',
            itemCount: 9,
            name: 'Feature',
          });
        });

        it('should reset previous query parameters', async function () {
          this.timeout(15000);
          await visit(
            '/browse/sites?icon=' +
              icon._id +
              '&map=' +
              map._id +
              '&author=' +
              map.info.author +
              '&published=true'
          );
          await visit('/browse');

          await click('.btn-show-all-features');

          expect(currentURL()).to.not.contain('icon=');
          expect(currentURL()).to.not.contain('map=');
          expect(currentURL()).to.not.contain('author=');
          expect(currentURL()).to.not.contain('published=');
        });
      });
    });

    describe('when there are only unpublished items', function () {
      let receivedSite;
      beforeEach(function () {
        for (let i = 0; i < 25; i++) {
          const site = server.testData.create.feature(i + 1);
          site.canEdit = true;
          site.visibility = 0;
          server.testData.storage.addFeature(site);

          server.delete_(`/mock-server/features/${site._id}`, () => [201]);
          server.put(`/mock-server/features/${site._id}`, (request) => {
            receivedSite = JSON.parse(request.requestBody);

            return [
              200,
              { 'Content-Type': 'application/json' },
              JSON.stringify(receivedSite),
            ];
          });
        }
      });

      it('should not show the unpublish option for a selected site', async function () {
        await visit(`/browse/sites?viewMode=list`);

        await click('tbody .chk-select');

        expect(this.element.querySelector('.navbar .btn-publish')).to.exist;
        expect(this.element.querySelector('.navbar .btn-unpublish')).not.to
          .exist;
      });

      it("should display the 'not published' icon in the grid view", async function () {
        this.timeout(15000);

        await visit(`/browse/sites`);
        expect(currentURL()).to.equal(`/browse/sites`);

        expect(this.element.querySelector('.icon-not-published')).to.exist;
      });

      it('should be able to publish a site', async function () {
        authenticateSession();

        await visit(`/browse/sites?viewMode=list`);
        await click('tbody .chk-select');

        await click(this.element.querySelector('.navbar .btn-publish'));

        expect(this.element.querySelector('.navbar .btn-unpublish')).to.exist;
        expect(this.element.querySelector('.navbar .btn-publish')).not.to.exist;

        expect(server.history).to.contain(`PUT /mock-server/features/1`);
        expect(receivedSite['feature']['visibility']).to.equal(1);
      });

      it("should display the 'not published' icon in the list view", async function () {
        this.timeout(15000);

        await visit(`/browse/sites?viewMode=list`);
        expect(currentURL()).to.equal(`/browse/sites?viewMode=list`);

        expect(this.element.querySelector('.icon-not-published')).to.exist;
      });

      describe('the select all features', () => {
        beforeEach(() => {
          server.post(`/mock-server/features/publish`, () => {
            return [204];
          });
          server.post(`/mock-server/features/unpublish`, () => {
            return [204];
          });
          server.post(`/mock-server/features/pending`, () => {
            return [204];
          });
          server.delete(`/mock-server/features`, () => {
            return [204];
          });

          server.testData.storage.addDefaultUser(
            false,
            '5df626b89e13420100359ee8'
          );
          server.testData.storage.addDefaultIcon('5f09e5e08de5e10100890b8f');
        });

        it('should display a disabled select all checkbox when there is no map filter', async function () {
          await visit(`/browse/sites?viewMode=list`);
          expect('.custom-checkbox.select-all input').to.have.attribute(
            'disabled',
            ''
          );
        });

        it('should allow setting all map features as pending', async function () {
          await visit(
            `/browse/sites?viewMode=list&map=5ca89e37ef1f7e010007f54c`
          );

          await click('.select-all .chk-select');
          await click(this.element.querySelector('.navbar .btn-set-pending'));

          expect(server.history).to.contain(
            'POST /mock-server/features/pending?map=5ca89e37ef1f7e010007f54c'
          );
        });

        it('should allow publishing all map features', async function () {
          await visit(
            `/browse/sites?viewMode=list&map=5ca89e37ef1f7e010007f54c`
          );
          expect('.custom-checkbox.select-all input').to.have.attribute(
            'disabled',
            null
          );

          await click('.select-all .chk-select');
          await click(this.element.querySelector('.navbar .btn-publish'));

          expect(server.history).to.contain(
            'POST /mock-server/features/publish?map=5ca89e37ef1f7e010007f54c'
          );
        });

        it('should allow unpublishing all map features', async function () {
          await visit(
            `/browse/sites?viewMode=list&map=5ca89e37ef1f7e010007f54c`
          );
          expect('.custom-checkbox.select-all input').to.have.attribute(
            'disabled',
            null
          );

          await click('.select-all .chk-select');
          await click(this.element.querySelector('.navbar .btn-unpublish'));

          expect(server.history).to.contain(
            'POST /mock-server/features/unpublish?map=5ca89e37ef1f7e010007f54c'
          );
        });

        it('should allow delete all map features', async function () {
          await visit(
            `/browse/sites?viewMode=list&map=5ca89e37ef1f7e010007f54c`
          );
          expect('.custom-checkbox.select-all input').to.have.attribute(
            'disabled',
            null
          );

          await click('.select-all .chk-select');
          await click(this.element.querySelector('.navbar .btn-delete'));

          expect(server.history).to.contain(
            'DELETE /mock-server/features?map=5ca89e37ef1f7e010007f54c'
          );
        });

        it('should apply the selected filters', async function () {
          await visit(
            `/browse/sites` +
              `?author=5df626b89e13420100359ee8` +
              `&icon=5f09e5e08de5e10100890b8f` +
              `&tag=test` +
              `&map=5ca89e37ef1f7e010007f54c` +
              `&visibility=1` +
              `&area=testarea` +
              `&search=searchterm` +
              `&viewMode=list`
          );
          expect('.custom-checkbox.select-all input').to.have.attribute(
            'disabled',
            null
          );

          await click('.select-all .chk-select');
          await click(this.element.querySelector('.navbar .btn-delete'));

          expect(server.history).to.contain(
            'DELETE /mock-server/features?map=5ca89e37ef1f7e010007f54c' +
              '&search=searchterm' +
              '&tag=test' +
              '&author=5df626b89e13420100359ee8' +
              '&icon=5f09e5e08de5e10100890b8f' +
              '&area=testarea' +
              '&visibility=1'
          );
        });
      });
    });
  });
});
