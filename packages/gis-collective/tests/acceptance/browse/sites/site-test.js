import {
  describe,
  it,
  beforeEach,
  afterEach
} from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, click, typeIn, triggerEvent, waitUntil } from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';
import {
  authenticateSession
} from 'ember-simple-auth/test-support';

describe('Acceptance | browse/sites/site', function () {
  setupApplicationTest();
  let server;
  let receivedIssue;
  let receivedPicture;

  beforeEach(function () {
    server = new TestServer();

    server.testData.storage.addDefaultUser(false, "5b8a59caef739394031a3f67");
    server.testData.storage.addDefaultProfile("5b8a59caef739394031a3f67");
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultMap();
    server.testData.storage.addDefaultIconSet();

    receivedIssue = null;
    server.post("/mock-server/issues", (request) => {
      receivedIssue = JSON.parse(request.requestBody);
      receivedIssue.issue._id = "1";

      return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedIssue)];
    });

    receivedPicture = null;
    server.post("/mock-server/pictures", (request) => {
      receivedPicture = JSON.parse(request.requestBody);
      receivedPicture.picture._id = "1";

      return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedPicture)];
    });
  });

  afterEach(function () {
    Modal.clear();
    server.shutdown();
  });

  describe('When a site with unknown creator(user does not exist) is available', function () {
    let site;
    let picture;
    let icon;
    let siteId;

    beforeEach(function () {
      siteId = "5ca8baf2ef1f7e01000849ba";
      site = server.testData.create.feature(siteId);
      picture = server.testData.create.picture();
      icon = server.testData.create.icon();

      server.testData.storage.addFeature(site);
      server.testData.storage.addPicture(picture);
      server.testData.storage.addIcon(icon);
    });

    it('it can view that site page', async function () {
      await visit(`/browse/sites/${siteId}`);
      expect(currentURL()).to.equal(`/browse/sites/${siteId}`);
    });

    describe('when the isMultiProjectMode is false', function() {
      beforeEach(function() {
        server.testData.storage.addPreference('access.isMultiProjectMode', "false");
      });

      it('hides the breadcrumbs for maps', async function() {
        await visit(`/browse/sites/${siteId}`);

        expect(document.querySelectorAll('.breadcrumb-item')).to.have.length(2);
      });

      it('hides the breadcrumbs for maps', async function() {
        server.testData.storage.addDefaultUser(false, "me");
        authenticateSession();

        await visit(`/browse/sites/${siteId}`);

        expect(document.querySelectorAll('.breadcrumb-item')).to.have.length(2);
      });

      it('shows the breadcrumbs for maps', async function() {
        server.testData.storage.addDefaultUser(true, "me");
        authenticateSession();

        await visit(`/browse/sites/${siteId}`);

        expect(document.querySelectorAll('.breadcrumb-item')).to.have.length(4);
      });
    });

    it('should hide the action dropdown', async function () {
      await visit(`/browse/sites/${siteId}`);

      expect(this.element.querySelector(".btn-manage-actions")).not.to.exist;
    });

    it('displays the profile of the author', async function () {
      await visit(`/browse/sites/${siteId}`);

      expect(this.element.querySelector('.profile-card')).to.exist;
      expect(this.element.querySelector('.profile-card a').textContent.trim()).to.equal("mr Bogdan Szabo");
    });

    it('does not display the masked info message', async function() {
      await visit(`/browse/sites/${siteId}`);

      expect(this.element.querySelector('.masked-info')).to.not.exist;
    });
  });

  describe("when the user is authenticated an has access to edit the site", function () {
    let site;
    let receivedSite;

    beforeEach(async function () {
      authenticateSession();
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultIcon();

      site = server.testData.create.feature();
      site.canEdit = true;
      receivedSite = null;

      server.put(`/mock-server/features/${site._id}`, (request) => {
        receivedSite = JSON.parse(request.requestBody);

        return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedSite)];
      });
    });

    it("should show the actions menu with `make public` for private sites", async function () {
      site.visibility = 0;

      server.testData.storage.addFeature(site);
      await visit(`/browse/sites/${site._id}`);

      expect(this.element.querySelector(".btn-manage-publish")).to.exist;
      expect(this.element.querySelector(".btn-manage-unpublish")).not.to.exist;
      expect(this.element.querySelector(".btn-manage-actions")).to.exist;
    });

    it("should the private icon", async function () {
      site.visibility = 0;

      server.testData.storage.addFeature(site);
      await visit(`/browse/sites/${site._id}`);

      expect(this.element.querySelector(".icon-not-published")).to.exist;
    });

    it("should show the actions menu with `make private` for public sites", async function () {
      site.visibility = 1;

      server.testData.storage.addFeature(site);
      await visit(`/browse/sites/${site._id}`);

      expect(this.element.querySelector(".btn-manage-publish")).not.to.exist;
      expect(this.element.querySelector(".btn-manage-unpublish")).to.exist;
      expect(this.element.querySelector(".btn-manage-actions")).to.exist;
    });

    it("should unpublish public sites", async function () {
      site.visibility = 1;

      server.testData.storage.addFeature(site);
      await visit(`/browse/sites/${site._id}`);

      await click(".btn-manage-unpublish");

      await waitUntil(() => receivedSite != null, { timeout: 3000 });

      expect(receivedSite.feature.visibility).to.equal(0);
    });

    it("should publish private sites", async function () {
      site.visibility = 0;

      server.testData.storage.addFeature(site);
      await visit(`/browse/sites/${site._id}`);

      await click(".btn-manage-publish");

      await waitUntil(() => receivedSite != null, { timeout: 3000 });
      expect(receivedSite.feature.visibility).to.equal(1);
    });
  });


  describe("when the user is authenticated an has access to edit the site", function () {
    let site;

    beforeEach(async function () {
      authenticateSession();
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultIcon();

      site = server.testData.create.feature();
      site.canEdit = true;
      site.visibility = -1;
    });

    it("should the pending icon", async function () {
      server.testData.storage.addFeature(site);
      await visit(`/browse/sites/${site._id}`);

      expect(this.element.querySelector(".icon-pending")).to.exist;
    });
  });

  describe('when a site created by an existing user is available', function () {
    let site;
    let picture;
    let icon;
    let profile;

    beforeEach(function () {
      profile = server.testData.storage.addDefaultProfile();
      site = server.testData.create.feature("5ca8baf2ef1f7e01000849ba");
      site.info.originalAuthor = profile._id;
      picture = server.testData.create.picture();
      icon = server.testData.create.icon();

      server.testData.storage.addFeature(site);
      server.testData.storage.addPicture(picture);
      server.testData.storage.addIcon(icon);
    });

    it('displays the username in the "Added by" info', async function () {
      await visit(`/browse/sites/${site._id}`);

      expect(this.element.querySelector('.profile-name').textContent).to.contain("Bogdan Szabo");
    });

    it('displays the show in fullscreen option', async function () {
      await visit(`/browse/sites/${site._id}`);

      expect(this.element.querySelector('.btn-show-fullscreen')).to.exist;
      expect(this.element.querySelector('.btn-show-fullscreen').textContent).to.contain("show in fullscreen");
    });
  });

  describe('when a masked site is available', function () {
    let site;
    let picture;
    let icon;
    let profile;

    beforeEach(function () {
      profile = server.testData.storage.addDefaultProfile();
      site = server.testData.create.feature("5ca8baf2ef1f7e01000849ba");
      site.info.originalAuthor = profile._id;
      picture = server.testData.create.picture();
      icon = server.testData.create.icon();

      server.testData.storage.addFeature(site);
      server.testData.storage.addPicture(picture);
      server.testData.storage.addIcon(icon);
      site.isMasked = true;
    });

    it('displays the masked message in the "Where" section', async function () {
      await visit(`/browse/sites/${site._id}`);

      expect(this.element.querySelector('.masked-info').textContent).to.contain("Location masking is applied, so the location displayed publicly will be imprecise.");
    });
  })

  describe('When a site without any pictures or sounds is available', function () {
    let site;
    let icon;
    let siteId;
    let receivedSite;

    beforeEach(function () {
      siteId = "5ca8baf2ef1f7e01000849ba";
      site = server.testData.create.feature(siteId);
      site.pictures = [];
      icon = server.testData.create.icon();

      server.testData.storage.addFeature(site);
      server.testData.storage.addIcon(icon);

      receivedSite = null;
      server.put(`/mock-server/features/${siteId}`, (request) => {
        receivedSite = JSON.parse(request.requestBody);

        return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedSite)];
      });
    });

    it('displays the alert to add pictures', async function () {
      await visit(`/browse/sites/${siteId}`);

      expect(this.element.querySelector('.alert-dismissible')).to.exist;
      expect(this.element.querySelector('.alert-dismissible').textContent).to.contain("This site has no pictures.");
      expect(this.element.querySelector('.alert-link').textContent).to.contain("Be the first who adds one.");
    });

    it('does not display the option to show in fullscreen', async function () {
      await visit(`/browse/sites/${siteId}`);

      expect(this.element.querySelector('.btn-show-fullscreen')).to.not.exist;
    });

    it('does not display any sound', async function () {
      await visit(`/browse/sites/${siteId}`);

      expect(this.element.querySelector('.sound-container')).to.not.exist;
    });

    it('allows an unauthenticated user to suggest a photo', async function () {
      await visit(`/browse/sites/${siteId}`);
      await click('.alert-link');

      await Modal.waitToDisplay();

      expect(this.element.querySelector('.upload-site-picture')).to.exist;
      expect(this.element.querySelector('.upload-site-picture').clientWidth).to.equal(
        this.element.querySelector(".input-picture").clientWidth);

      const blob = server.testData.create.pngBlob();

      await triggerEvent(
        ".upload-site-picture input[type='file']",
        "change",
        { files: [blob] }
      );

      await typeIn(".picture-title", "test");
      await click(".modal.show .btn-primary");

      await Modal.waitToHide();
      await click(".modal.show .btn-resolve");
      await Modal.waitToHide();

      expect(receivedIssue.issue.title).to.equal("test");
      expect(receivedIssue.issue.feature).to.equal(site._id);
      expect(receivedIssue.issue.author).to.equal("@anonymous");
      expect(receivedIssue.issue.type).to.equal("newImage");
      expect(receivedIssue.issue.file).to.exist;
      expect(receivedIssue.issue.file).to.be.string;
    });

    it('allows an authenticated user to suggest a photo', async function () {
      authenticateSession();

      await visit(`/browse/sites/${siteId}`);
      await click('.alert-link');

      await Modal.waitToDisplay();

      expect(this.element.querySelector('.upload-site-picture')).to.exist;
      expect(this.element.querySelector('.upload-site-picture').clientWidth).to.equal(
        this.element.querySelector(".input-picture").clientWidth);

      const blob = server.testData.create.pngBlob();

      await triggerEvent(
        ".upload-site-picture input[type='file']",
        "change",
        { files: [blob] }
      );

      await typeIn(".picture-title", "test");
      await click(".modal.show .btn-primary");
      await Modal.waitToHide();

      await click(".modal.show .btn-resolve");
      await Modal.waitToHide();

      expect(receivedIssue.issue.title).to.equal("test");
      expect(receivedIssue.issue.feature).to.equal(site._id);
      expect(receivedIssue.issue.author).to.equal("5b870669796da25424540deb");
      expect(receivedIssue.issue.type).to.equal("newImage");
      expect(receivedIssue.issue.file).to.exist;
      expect(receivedIssue.issue.file).to.be.string;
    });

    it('redirects the user to the edit page when the site is editable', async function () {
      this.timeout(5000);

      site = server.testData.create.feature(siteId);
      site.canEdit = true;
      site.pictures = [];
      server.testData.storage.addFeature(site);

      authenticateSession();

      await visit(`/browse/sites/${siteId}`);
      await click('.alert-link');

      expect(currentURL()).to.equal('/manage/sites/' + siteId);
    });

    it('does not show the Rotate picture button before a photo was selected', async function () {
      await visit(`/browse/sites/${siteId}`);
      await click('.alert-link');
      await Modal.waitToDisplay();

      expect(this.element.querySelector('.btn-rotate')).to.not.exist;

      await click(".modal-footer .btn-secondary");
      await Modal.waitToHide();
    });
  });

  describe('when site with 2 sounds is available', function () {
    let site;
    let siteId;

    beforeEach(function () {
      siteId = "5ca8baf2ef1f7e01000849ba";
      site = server.testData.create.feature(siteId);
      site.maps = [];
      site.icons = [];
      site.sounds = ["a", "b"];
      site.canEdit = false;

      server.testData.storage.addFeature(site);
      server.testData.storage.addDefaultIcon();
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultSound("a");
      server.testData.storage.addDefaultSound("b");
    });

    it('displays two sound ', async function () {
      await visit(`/browse/sites/${siteId}`);

      expect(this.element.querySelectorAll('.sound-container')).to.have.length(2);
    });
  });

  describe('when a read only site without any maps is available', function () {
    let site;
    let siteId;

    beforeEach(function () {
      siteId = "5ca8baf2ef1f7e01000849ba";
      site = server.testData.create.feature(siteId);
      site.maps = [];
      site.icons = [];
      site.canEdit = false;

      server.testData.storage.addFeature(site);
      server.testData.storage.addDefaultIcon();
      server.testData.storage.addDefaultPicture();
    });

    it('displays no error', async function () {
      await visit(`/browse/sites/${siteId}`);

      expect(this.element.querySelector('.alert-danger.alert-no-map')).not.to.exist;
    });
  });

  describe('when an editable site without any maps is available', function () {
    let site;
    let siteId;

    beforeEach(function () {
      authenticateSession();

      siteId = "5ca8baf2ef1f7e01000849ba";
      site = server.testData.create.feature(siteId);
      site.maps = [];
      site.icons = [];
      site.canEdit = true;

      server.testData.storage.addFeature(site);
      server.testData.storage.addDefaultIcon();
      server.testData.storage.addDefaultPicture();
    });

    it('displays an error message', async function () {
      await visit(`/browse/sites/${siteId}`);

      expect(this.element.querySelector('.alert-danger.alert-no-map')).to.exist;
      expect(this.element.querySelector('.alert-danger.alert-no-map').textContent).to.contain("This site is not linked to a map.");
      expect(this.element.querySelector('a.alert-link').textContent).to.contain("Edit to add one.");

      await click("a.alert-link");
      expect(currentURL()).to.equal('/manage/sites/' + siteId);
    });
  });
});
