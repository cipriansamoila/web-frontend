import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL } from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';
import { authenticateSession } from 'ember-simple-auth/test-support';

describe('Acceptance | browse/teams/team', function () {
  setupApplicationTest();
  let server;
  let team;
  let user;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultMap();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultIcon();
    team = server.testData.create.team();
    user = server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultProfile(user._id);

    team.owners = [user._id];

    server.testData.storage.addDefaultPicture();
    server.testData.storage.addTeam(team);
    server.testData.storage.addDefaultUser(true);
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('can visit /browse/teams/team', async function () {
    await visit(`/browse/teams/${team._id}`);
    expect(currentURL()).to.equal(`/browse/teams/${team._id}`);

    expect(this.element.querySelector('h1').textContent.trim()).to.eql(
      'Open Green Map'
    );
    expect(this.element.querySelectorAll('h2')[0].textContent.trim()).to.eql(
      'members'
    );
    expect(this.element.querySelectorAll('h2')[1].textContent.trim()).to.eql(
      'team maps'
    );
    expect(this.element.querySelector('.profile-card')).to.exist;
    expect(this.element.querySelector('.map-card')).to.exist;
    expect(this.element.querySelector('.breadcrumb')).to.exist;
    expect(this.element.querySelector('.campaign-card')).not.to.exist;

    expect(server.history).to.contain(
      'GET /mock-server/userprofiles/5b870669796da25424540deb'
    );
    expect(server.history).to.contain(
      'GET /mock-server/users/5b870669796da25424540deb'
    );
    expect(server.history).to.contain(
      'GET /mock-server/maps?team=5ca78e2160780601008f69e6'
    );
  });

  describe('when the team has a campaign', function () {
    beforeEach(function () {
      server.testData.storage.addDefaultCampaign();
    });

    it('shows the campaign in the list', async function () {
      await visit(`/browse/teams/${team._id}`);

      expect(this.element.querySelector('.campaign-card')).to.exist;
      expect(
        this.element.querySelector('.campaign-card .name').textContent.trim()
      ).to.equal('Campaign 1');
    });
  });

  describe('when the isMultiProjectMode is false', function () {
    beforeEach(function () {
      server.testData.storage.addPreference(
        'access.isMultiProjectMode',
        'false'
      );
    });

    it('hides the breadcrumbs', async function () {
      await visit(`/browse/teams/${team._id}`);

      expect(document.querySelector('.breadcrumb')).not.to.exist;
    });

    it('hides the breadcrumbs', async function () {
      server.testData.storage.addDefaultUser(false, 'me');
      authenticateSession();

      await visit(`/browse/teams/${team._id}`);
      expect(document.querySelector('.breadcrumb')).not.to.exist;
    });

    it('shows the breadcrumbs', async function () {
      server.testData.storage.addDefaultUser(true, 'me');
      authenticateSession();

      await visit(`/browse/teams/${team._id}`);
      expect(document.querySelector('.breadcrumb')).to.exist;
    });
  });
});
