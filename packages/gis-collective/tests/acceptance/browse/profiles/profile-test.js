import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, click } from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';
import waitUntil from '@ember/test-helpers/wait-until';

describe('Acceptance | browse/profiles/profile', function () {
  setupApplicationTest();
  let server;
  let profile;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultPicture();

    const date = new Date();
    const year = date.getUTCFullYear();
    const day = date.getUTCDay();
    const week = 0;

    server.get(`/mock-server/userprofiles/:id/contributions`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify([{ year, count: 1, day, week }]),
      ];
    });
  });

  afterEach(function () {
    Modal.clear();
    server.shutdown();
  });

  describe('when there authenticated user is an administrator', function () {
    beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultUser(false, '5b8a59caef739394031a3000');
      server.testData.storage.addDefaultUser(true);
      server.testData.storage.addDefaultUser(true, 'me');
      profile = server.testData.storage.addDefaultProfile(
        '5b8a59caef739394031a3000'
      );
    });

    it('can go to the admin edit profile page', async function () {
      await visit(`/browse/profiles/${profile._id}`);

      expect(this.element.querySelector('.btn-edit-profile')).to.have.attribute(
        'href',
        `/admin/users/${profile._id}`
      );
    });

    it('queries the user contributions', async function () {
      await visit(`/browse/profiles/${profile._id}`);

      expect(server.history).to.contain(
        `GET /mock-server/userprofiles/${profile._id}/contributions`
      );
      expect(this.element.querySelector('.level-cell-1')).to.exist;
    });
  });

  describe('when there is an editable profile with all fields', function () {
    beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultUser();
      profile = server.testData.storage.addDefaultProfile();
      server.testData.storage.addDefaultProfile('5b870669796da25424540deb');
    });

    it('can render all user fields', async function () {
      await visit(`/browse/profiles/${profile._id}`);
      await waitUntil(
        () =>
          this.element
            .querySelector('.profile-picture')
            .attributes.getNamedItem('style')?.value
      );
      await waitUntil(
        () =>
          this.element
            .querySelector('.profile-picture')
            .attributes.getNamedItem('style')
            .value.indexOf('user.svg') == -1
      );

      expect(currentURL()).to.equal(`/browse/profiles/${profile._id}`);

      expect(
        this.element.querySelector('.joined-time').textContent.trim()
      ).to.equal('Member since 1/1/2010');
      expect(
        this.element.querySelector('.username').textContent.trim()
      ).to.equal('@some_username');
      expect(
        this.element.querySelector('.fullname').textContent.trim()
      ).to.equal('mr Bogdan Szabo');
      expect(
        this.element.querySelector('.location').textContent.trim()
      ).to.equal('Berlin');
      expect(this.element.querySelector('.job').textContent.trim()).to.equal(
        'Developer at GISCollective'
      );

      expect(this.element.querySelector('.linkedin')).to.have.attribute(
        'href',
        'https://www.linkedin.com/in/szabobogdan'
      );
      expect(this.element.querySelector('.skype')).to.have.attribute(
        'href',
        'skype:skype_id'
      );
      expect(this.element.querySelector('.twitter')).to.have.attribute(
        'href',
        'https://twitter.com/@szabobogdan1'
      );
      expect(this.element.querySelector('.website')).to.have.attribute(
        'href',
        'https://szabobogdan.com'
      );

      expect(this.element.querySelector('.profile-picture')).to.have.attribute(
        'style',
        `background-image: url('https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture')`
      );
    });

    it('can go to the edit profile page', async function () {
      await visit(`/browse/profiles/${profile._id}`);

      await click('.btn-edit-profile');

      expect(currentURL()).to.equal(`/preferences/profile`);
    });
  });

  describe('when there is an editable profile with no fields', function () {
    beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultUser();
      profile = server.testData.create.userProfile();
      profile.organization = '';
      profile.location = '';
      profile.bio = '';
      profile.statusEmoji = '';
      profile.linkedin = '';
      profile.twitter = '';
      profile.skype = '';
      profile.canEdit = true;
      profile.website = '';
      profile.joinedTime = '';
      profile.jobTitle = '';
      profile.showPrivateContributions = false;
      profile.statusMessage = '';
      profile.firstName = '';
      profile.title = '';
      profile.lastName = '';
      profile.userName = '';

      server.testData.storage.addUserProfile(profile);
    });

    it('does not render the missing fields', async function () {
      await visit(`/browse/profiles/${profile._id}`);
      expect(currentURL()).to.equal(`/browse/profiles/${profile._id}`);
      await waitUntil(
        () =>
          this.element
            .querySelector('.profile-picture')
            .attributes.getNamedItem('style')?.value
      );
      await waitUntil(
        () =>
          this.element
            .querySelector('.profile-picture')
            .attributes.getNamedItem('style')
            .value.indexOf('user.svg') == -1
      );

      expect(
        this.element.querySelector('.contributions-calendar-hidden-message')
      ).not.to.exist;

      expect(this.element.querySelector('.joined-time')).not.to.exist;
      expect(this.element.querySelector('.username')).not.to.exist;
      expect(
        this.element.querySelector('.fullname').textContent.trim()
      ).to.equal('---');
      expect(this.element.querySelector('.location')).not.to.exist;
      expect(this.element.querySelector('.job')).not.to.exist;

      expect(this.element.querySelector('.linkedin')).not.to.exist;
      expect(this.element.querySelector('.skype')).not.to.exist;
      expect(this.element.querySelector('.twitter')).not.to.exist;
      expect(this.element.querySelector('.website')).not.to.exist;

      expect(this.element.querySelector('.profile-picture')).to.have.attribute(
        'style',
        `background-image: url('https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture')`
      );
    });
  });

  describe('when there is an readable profile without picture', function () {
    beforeEach(function () {
      invalidateSession();
      profile = server.testData.create.userProfile();
      profile.picture = null;
      profile.canEdit = false;

      server.testData.storage.addUserProfile(profile);
    });

    it('can render all user fields', async function () {
      await visit(`/browse/profiles/${profile._id}`);
      expect(currentURL()).to.equal(`/browse/profiles/${profile._id}`);

      expect(
        this.element.querySelector('.joined-time').textContent.trim()
      ).to.equal('Member since 1/1/2010');
      expect(
        this.element.querySelector('.username').textContent.trim()
      ).to.equal('@some_username');
      expect(
        this.element.querySelector('.fullname').textContent.trim()
      ).to.equal('mr Bogdan Szabo');
      expect(
        this.element.querySelector('.location').textContent.trim()
      ).to.equal('Berlin');
      expect(this.element.querySelector('.job').textContent.trim()).to.equal(
        'Developer at GISCollective'
      );

      expect(this.element.querySelector('.linkedin')).to.have.attribute(
        'href',
        'https://www.linkedin.com/in/szabobogdan'
      );
      expect(this.element.querySelector('.skype')).to.have.attribute(
        'href',
        'skype:skype_id'
      );
      expect(this.element.querySelector('.twitter')).to.have.attribute(
        'href',
        'https://twitter.com/@szabobogdan1'
      );
      expect(this.element.querySelector('.website')).to.have.attribute(
        'href',
        'https://szabobogdan.com'
      );

      expect(this.element.querySelector('.profile-picture')).to.have.attribute(
        'style',
        `background-image: url('/img/user.svg')`
      );
    });

    it('can go to the edit profile page', async function () {
      await visit(`/browse/profiles/${profile._id}`);

      expect(this.element.querySelector('.btn-edit-profile')).not.to.exist;
    });
  });

  describe('when there is an readable profile without job title', function () {
    beforeEach(function () {
      invalidateSession();
      profile = server.testData.create.userProfile();
      profile.jobTitle = '';

      server.testData.storage.addUserProfile(profile);
    });

    it('can renders only the organization', async function () {
      await visit(`/browse/profiles/${profile._id}`);
      expect(currentURL()).to.equal(`/browse/profiles/${profile._id}`);

      expect(this.element.querySelector('.job').textContent.trim()).to.equal(
        'GISCollective'
      );
    });
  });

  describe('when an user is not in any team', function () {
    beforeEach(function () {
      profile = server.testData.create.userProfile('000000001');
      server.testData.storage.addUserProfile(profile);

      server.get(`/mock-server/teams`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({ teams: [] }),
        ];
      });
    });

    it('should show a message', async function () {
      await visit(`/browse/profiles/${profile._id}`);

      expect(
        this.element.querySelector('.profile-team-message').textContent.trim()
      ).to.equal('mr Bogdan Szabo does not currently belong to a team.');
    });
  });

  describe('when the authenticated user is not in any team', function () {
    beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultUser(false, '5b8a59caef739394031a3000');
      profile = server.testData.create.userProfile('5b8a59caef739394031a3000');
      server.testData.storage.addUserProfile(profile);

      server.get(`/mock-server/teams`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({ teams: [] }),
        ];
      });
    });

    it('should show a message', async function () {
      await visit(`/browse/profiles/${profile._id}`);

      expect(
        this.element.querySelector('.profile-team-message').textContent.trim()
      ).to.equal('You are not part of any team.');
    });
  });

  describe('when the user is part of a team', function () {
    beforeEach(function () {
      profile = server.testData.storage.addDefaultProfile();
      server.testData.storage.addDefaultTeam();
    });

    it('should show a message', async function () {
      await visit(`/browse/profiles/${profile._id}`);

      expect(this.element.querySelector('.team-card')).to.exist;
      expect(this.element.querySelector('.profile-team-message')).not.to.exist;
    });

    it('transitions to the team when the cover is clicked', async function () {
      await visit(`/browse/profiles/${profile._id}`);

      await click('.team-card .cover-image');

      expect(currentURL()).to.equal(`/browse/teams/5ca78e2160780601008f69e6`);
    });
  });

  describe('when the calendar is hidden', function () {
    beforeEach(function () {
      server.testData.storage.addDefaultUser(false, '5b8a59caef739394031a3000');
      profile = server.testData.create.userProfile('5b8a59caef739394031a3000');
      profile.showCalendarContributions = false;

      server.testData.storage.addUserProfile(profile);

      server.get(`/mock-server/teams`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({ teams: [] }),
        ];
      });
    });

    it('should not show the calendar to an unauthenticated user', async function () {
      await visit(`/browse/profiles/${profile._id}`);
      expect(this.element.querySelector('.contributions-calendar')).not.to
        .exist;
    });

    it('should show the calendar to an admin user', async function () {
      server.testData.storage.addDefaultUser(true, '5b8a59caef739394031a3001');
      authenticateSession();

      await visit(`/browse/profiles/${profile._id}`);
      expect(this.element.querySelector('.contributions-calendar')).to.exist;
    });

    it('should show the calendar to the user owning it', async function () {
      authenticateSession();

      await visit(`/browse/profiles/${profile._id}`);
      expect(this.element.querySelector('.contributions-calendar')).to.exist;
    });

    it('shows a message when the user calendar is hidden and editable', async function () {
      authenticateSession();

      await visit(`/browse/profiles/${profile._id}`);
      expect(
        this.element.querySelector('.contributions-calendar-hidden-message')
      ).to.exist;
    });
  });
});
