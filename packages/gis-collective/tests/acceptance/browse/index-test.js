import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  fillIn,
  waitFor,
  waitUntil,
} from '@ember/test-helpers';
import TestServer from '../../helpers/test-server';
import { authenticateSession } from 'ember-simple-auth/test-support';
import Modal from '../../helpers/modal';

describe('Acceptance | browse/index', function () {
  setupApplicationTest();
  let server;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultPicture();
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('can visit /browse', async function () {
    await visit('/browse');
    expect(currentURL()).to.equal('/browse');
  });

  it('should search maps and sites by name', async function () {
    await visit('/browse');

    await fillIn('.main-container .input-search', 'search something');

    server.history = [];
    await click('.btn-search');

    expect(currentURL()).to.equal(`/browse?search=search%20something`);

    expect(
      server.history.filter((a) => a.indexOf('/features') != -1)[0]
    ).to.contain('term=search+something');
    expect(
      server.history.filter((a) => a.indexOf('/maps') != -1)[0]
    ).to.contain('term=search+something');
    expect(
      server.history.filter((a) => a.indexOf('/models') != -1)[0]
    ).to.contain('term=search+something');
  });

  describe('When there is a site available', function () {
    let site;
    let picture;
    let icon;

    beforeEach(function () {
      site = server.testData.create.feature('5ca8baf2ef1f7e01000849ba');
      picture = server.testData.create.picture();
      icon = server.testData.create.icon();

      server.testData.storage.addFeature(site);
      server.testData.storage.addPicture(picture);
      server.testData.storage.addIcon(icon);
    });

    it('should display the site', async function () {
      await visit('/browse');
      expect(currentURL()).to.equal('/browse');

      expect(this.element.querySelector('.site-card')).to.exist;
      expect(
        this.element.querySelector('.container h1').textContent.trim()
      ).to.equal('sites');

      expect(this.element.querySelector('.card-title').textContent).to.contain(
        site.name
      );
      expect(this.element.querySelector('.cover-image')).to.have.style(
        `background-image`,
        `url("${picture.picture}/md")`
      );
      expect(this.element.querySelector('.icon')).to.have.attribute(
        'src',
        icon.image.value
      );
    });
  });

  describe('When there is a map available', function () {
    let map;
    let picture;

    beforeEach(function () {
      map = server.testData.create.map('5ca89e27ef1f7e010007f4af');
      picture = server.testData.create.picture();

      server.testData.storage.addMap(map);
      server.testData.storage.addPicture(picture);
      server.testData.storage.addDefaultIconSet();
    });

    it('should display the map', async function () {
      await visit('/browse');

      await waitUntil(() =>
        this.element
          .querySelector('.map-card .cover-image')
          .attributes.getNamedItem('style')
      );

      expect(currentURL()).to.equal('/browse');

      expect(this.element.querySelector('.map-card')).to.exist;
      expect(
        this.element.querySelector('.container h1').textContent.trim()
      ).to.equal('maps');

      expect(
        this.element.querySelector('.map-card .cover-image')
      ).to.have.style(`background-image`, `url("${picture.picture}/sm")`);

      expect(
        this.element.querySelector('.map-name').textContent.trim()
      ).to.equal(map.name);
      expect(this.element.querySelector('.map-name')).to.have.attribute(
        'href',
        '/browse/maps/5ca89e27ef1f7e010007f4af/map-view'
      );
      expect(
        this.element.querySelector('.map-description').textContent.trim()
      ).to.equal(map.description);
    });

    it('should navigate to the map page on click on the title', async function () {
      this.timeout(5000);

      server.server.get(`/mock-server/maps/:id/meta`, () => [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          publicSites: 220,
          totalContributors: 33,
        }),
      ]);

      await visit('/browse');
      expect(currentURL()).to.equal('/browse');

      await click(this.element.querySelector('.map-card .map-name'));

      expect(currentURL()).to.equal(
        '/browse/maps/5ca89e27ef1f7e010007f4af/map-view'
      );
    });

    it('should navigate to the map page on click on the cover', async function () {
      server.server.get(`/mock-server/maps/:id/meta`, () => [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          publicSites: 220,
          totalContributors: 33,
        }),
      ]);

      await visit('/browse');
      expect(currentURL()).to.equal('/browse');

      await click(this.element.querySelector('.map-card .cover-image'));

      expect(currentURL()).to.equal(
        '/browse/maps/5ca89e27ef1f7e010007f4af/map-view'
      );
    });
  });

  describe('When there are two sites available', function () {
    beforeEach(function () {
      server.testData.storage.addDefaultFeature('5ca8baf2ef1f7e01000849ba');
      server.testData.storage.addDefaultFeature('6ca8baf2ef1f7e01000849bz');

      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultIcon();
    });
    it('should display both sites', async function () {
      await visit('/browse');

      expect(this.element.querySelector('.site-card')).to.exist;
      expect(this.element.querySelectorAll('.site-card').length).to.equal(2);
    });
  });

  describe('When there are two maps available', function () {
    beforeEach(function () {
      server.testData.storage.addDefaultMap('5ca89e27ef1f7e010007f4af');
      server.testData.storage.addDefaultMap('6ca89e27ef1f7e010007f4ag');

      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultIconSet();
    });

    it('should display both maps', async function () {
      await visit('/browse');

      expect(this.element.querySelector('.map-card')).to.exist;
      expect(this.element.querySelectorAll('.map-card').length).to.equal(2);
    });
  });

  describe('When there are two icon sets available', function () {
    beforeEach(function () {
      server.testData.storage.addDefaultIconSet('5ca89e27ef1f7e010007f4af');
      server.testData.storage.addDefaultIconSet('6ca89e27ef1f7e010007f4ag');

      server.testData.storage.addDefaultPicture();
    });

    it('should display both icon sets', async function () {
      await visit('/browse');

      expect(this.element.querySelector('.icon-set-card')).to.exist;
      expect(this.element.querySelectorAll('.icon-set-card').length).to.equal(
        2
      );
    });
  });

  describe('When GET models returns more than 4 maps', function () {
    beforeEach(function () {
      server.testData.storage.addDefaultMap('5ca89e27ef1f7e010007f4af');
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultIconSet();

      server.testData.storage.addModel({
        _id: '000000000000000000000001',
        itemCount: 5,
        name: 'Map',
      });
    });

    it('should display the Show more link', async function () {
      await visit('/browse');

      expect(this.element.querySelector('.show-all')).to.exist;
    });
  });

  describe('When GET models returns more than 8 features', function () {
    beforeEach(function () {
      server.testData.storage.addDefaultFeature('5ca8baf2ef1f7e01000849ba');
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultIcon();

      server.testData.storage.addModel({
        _id: '000000000000000000000002',
        itemCount: 9,
        name: 'Feature',
      });
    });

    it('should display the Show more link', async function () {
      await visit('/browse');

      expect(this.element.querySelector('.show-all')).to.exist;
    });
  });

  describe('When GET models returns more than 4 icon sets', function () {
    beforeEach(function () {
      server.testData.storage.addDefaultIconSet();
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultIcon();

      server.testData.storage.addModel({
        _id: '000000000000000000000002',
        itemCount: 5,
        name: 'IconSet',
      });
    });

    it('should display the Show more link', async function () {
      await visit('/browse');

      expect(this.element.querySelector('.btn-show-all-icon-sets')).to.exist;
      await click(`.btn-show-all-icon-sets`);

      expect(currentURL()).to.equal('/browse/icons');
    });
  });

  describe('when the user is authenticated', function () {
    beforeEach(function () {
      authenticateSession();
      server.testData.storage.addDefaultPicture();
      server.testData.storage.addDefaultIcon();
      server.testData.storage.addDefaultUser(true, '5b8a59caef739394031a3f67');
      server.testData.storage.addDefaultTeam();
    });

    describe('when there is a public feature, map and icon set', function () {
      let site;
      let map;
      let picture;
      let icon;
      let iconSet;

      beforeEach(function () {
        site = server.testData.create.feature('5ca8baf2ef1f7e01000849ba');
        map = server.testData.create.map('5ca8baf2ef1f7e01000849ba');
        picture = server.testData.create.picture();
        icon = server.testData.create.icon();

        server.testData.storage.addFeature(site);
        server.testData.storage.addMap(map);
        server.testData.storage.addPicture(picture);
        server.testData.storage.addIcon(icon);
        iconSet = server.testData.storage.addDefaultIconSet();
      });

      it('should display the map, iconset and feature', async function () {
        authenticateSession();
        await visit('/browse');
        expect(currentURL()).to.equal('/browse');

        const mapCard = this.element.querySelector('.map-card-deck');
        const siteCard = this.element.querySelector('.site-card-deck');
        const iconCard = this.element.querySelector('.icon-card-deck');

        expect(mapCard).to.exist;
        expect(
          this.element.querySelectorAll('.container h1')[0].textContent.trim()
        ).to.equal('maps');
        expect(mapCard.querySelector('.map-name').textContent.trim()).to.equal(
          map.name
        );
        expect(mapCard.querySelector('.cover-image')).to.have.style(
          'background-image',
          `url("${picture.picture}/sm")`
        );
        expect(mapCard.querySelector('.icon-not-published')).not.to.exist;

        expect(iconCard).to.exist;
        expect(
          this.element.querySelectorAll('.container h1')[1].textContent.trim()
        ).to.equal('icons');
        expect(iconCard.querySelector('.icon-set-name').textContent).to.contain(
          iconSet.name
        );
        expect(iconCard.querySelector('.cover-image')).to.have.style(
          'background-image',
          `url("${picture.picture}/sm")`
        );

        expect(siteCard).to.exist;
        expect(
          this.element.querySelectorAll('.container h1')[2].textContent.trim()
        ).to.equal('sites');
        expect(siteCard.querySelector('.card-title').textContent).to.contain(
          site.name
        );
        expect(siteCard.querySelector('.cover-image')).to.have.style(
          `background-image`,
          `url("${picture.picture}/md")`
        );
        expect(siteCard.querySelector('.icon')).to.have.attribute(
          'src',
          icon.image.value
        );
      });
    });

    describe('when there is a map available', function () {
      let map;

      beforeEach(function () {
        map = server.testData.create.map();
        map.canEdit = true;
        server.testData.storage.addMap(map);

        server.testData.storage.addDefaultPicture();
        server.testData.storage.addDefaultIconSet();

        server.server.delete(`/mock-server/maps/${map._id}`, () => {
          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify({}),
          ];
        });
      });

      it('should delete the map', async function () {
        await visit('/browse');

        await click('.map-card .btn-extend');

        await waitFor('.map-card .btn-delete');
        await click('.map-card .btn-delete');

        await Modal.waitToDisplay();
        await Modal.clickDangerButton();

        expect(
          server.history.filter((a) => a.indexOf('DELETE') != -1)[0]
        ).to.contain(`/mock-server/maps/${map._id}`);
      });
    });

    describe('when there is a feature available', function () {
      let feature;

      beforeEach(function () {
        feature = server.testData.create.feature();
        feature.canEdit = true;
        server.testData.storage.addFeature(feature);

        server.testData.storage.addDefaultPicture();

        server.server.delete(`/mock-server/features/${feature._id}`, () => {
          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify({}),
          ];
        });
      });

      it('should delete the feature', async function () {
        await visit('/browse');

        await click('.site-card .btn-extend');

        await waitFor('.site-card .btn-delete');
        await click('.site-card .btn-delete');

        await Modal.waitToDisplay();
        await Modal.clickDangerButton();

        expect(
          server.history.filter((a) => a.indexOf('DELETE') != -1)[0]
        ).to.contain(`/mock-server/features/${feature._id}`);
      });
    });
  });
});
