import {
  describe,
  it,
  beforeEach,
  afterEach
} from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  fillIn,
  triggerEvent,
  waitUntil,
  waitFor
} from '@ember/test-helpers';
import {
  authenticateSession,
  invalidateSession
} from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';
import PageElements from '../../../helpers/page-elements';

describe('Acceptance | manage/maps/add', function () {
  setupApplicationTest();
  let server;
  let receivedMap;
  let iconSet;
  let map;

  beforeEach(function () {
    server = new TestServer();
    map = server.testData.create.map();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    iconSet = server.testData.storage.addDefaultIconSet(iconSet);

    server.post('/mock-server/maps', (request) => {
      receivedMap = JSON.parse(request.requestBody);
      receivedMap.map._id = map._id;

      server.testData.storage.addMap(receivedMap.map);

      return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedMap)];
    });

    receivedMap = null;
  });

  afterEach(function () {
    server.shutdown();
    const elm = document.querySelector(".modal-backdrop");

    if (elm) {
      elm.parentNode.removeChild(elm);
    }

    document.querySelector("body").classList.remove("modal-open");
  });

  describe("when there is a team available", function () {
    let team;

    beforeEach(function () {
      team = server.testData.create.team();
      server.testData.storage.addTeam(team);
    });

    it('should redirect to login without an auth token it', async function () {
      invalidateSession();

      await visit('/manage/maps/add');
      expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fmaps%2Fadd');
    });

    it('can visit /manage/maps/add with an authenticated user', async function () {
      authenticateSession();
      await visit('/manage/maps/add');

      expect(currentURL()).to.equal('/manage/maps/add');

      expect(this.element.querySelector("h1").textContent.trim()).to.equal("New map");
      expect(this.element.querySelector('.breadcrumb')).not.to.exist;
      expect(PageElements.rowTitles()).to.deep.equal(['Name', 'Description', 'Team']);
      expect(this.element.querySelector(".alert-danger .btn-add-team")).not.to.exist;
      expect(this.element.querySelector(".alert-danger")).not.to.exist;

    });

    it('can add a map', async function () {
      this.timeout(20000);
      authenticateSession();
      await visit('/manage/maps/add');
      await waitFor(".editor-is-ready", {timeout: 3000});

      expect(currentURL()).to.equal('/manage/maps/add');

      await fillIn(".row-name input", "my new map");
      await fillIn(".ce-paragraph", "test description");

      this.element.querySelector(".row-team select").value = team._id;
      await triggerEvent(".row-team select", "change");

      await PageElements.wait(1000);
      await click(".btn-submit");

      expect(currentURL()).to.equal(`/manage/maps/${map._id}`);

      expect(receivedMap.map._id).to.equal("5ca89e37ef1f7e010007f54c");
      expect(receivedMap.map.name).to.equal("my new map");
      expect(receivedMap.map.description.blocks).to.deep.equal([{
          "type": "header",
          "data": {
            "text": "my new map",
            "level": 1
          }
        },{
          "type": "paragraph",
          "data": {
            "text": "test description"
          }
        }
      ]);
      expect(receivedMap.map.visibility).to.deep.equal({
        isPublic: false,
        isDefault: false,
        team: '5ca78e2160780601008f69e6'
      });
    });

    it('should load only teams that the user can edit', async function () {
      authenticateSession();
      await visit('/manage/maps/add');
      await waitUntil(() => server.history.filter(a => a.indexOf("teams") != -1).length > 0);
      expect(server.history).to.contain("GET /mock-server/teams?all=false&edit=true");
    });

  });

  describe("when there are two teams available", function() {
    let team1;
    let team2;

    beforeEach(function () {
      team1 = server.testData.storage.addDefaultTeam("team-1");
      team2 = server.testData.storage.addDefaultTeam("team-2");
    });

    it('can add a map with the first team', async function () {
      this.timeout(20000);
      authenticateSession();
      await visit('/manage/maps/add');
      await waitFor(".editor-is-ready", {timeout: 3000});

      expect(currentURL()).to.equal('/manage/maps/add');

      await fillIn(".row-name input", "my new map");
      await fillIn(".ce-paragraph", "test description");

      this.element.querySelector(".row-team select").value = team1._id;
      await triggerEvent(".row-team select", "change");

      await PageElements.wait(1000);
      await click(".btn-submit");

      expect(currentURL()).to.equal(`/manage/maps/${map._id}`);
      expect(receivedMap.map.visibility).to.deep.equal({
        isPublic: false,
        isDefault: false,
        team: team1._id
      });
    });

    it('can add a map with the second team', async function () {
      this.timeout(20000);
      authenticateSession();
      await visit('/manage/maps/add');
      await waitFor(".editor-is-ready", {timeout: 3000});

      expect(currentURL()).to.equal('/manage/maps/add');

      await fillIn(".row-name input", "my new map");
      await fillIn(".ce-paragraph", "test description");

      this.element.querySelector(".row-team select").value = team2._id;
      await triggerEvent(".row-team select", "change");

      await PageElements.wait(1000);
      await click(".btn-submit");

      expect(currentURL()).to.equal(`/manage/maps/${map._id}`);
      expect(receivedMap.map.visibility).to.deep.equal({
        isPublic: false,
        isDefault: false,
        team: team2._id
      });
    });
  });

  describe('when no team is available', function () {

    it('should show a button to guide the user to create a team first', async function () {
      authenticateSession();
      await visit('/manage/maps/add');

      expect(this.element.querySelector(".row-team .btn-add-team")).to.exist;
      expect(this.element.querySelector(".alert-danger .btn-add-team")).to.exist;
    });

    it('the button in the alert should redirect the user to the add a team form', async function () {
      authenticateSession();
      await visit('/manage/maps/add');
      await click(".alert-danger .btn-add-team");

      expect(currentURL()).to.equal('/manage/teams/add?next=manage.maps.add');
    });

    it('the button in the Team section should also redirect to the add a team form', async function() {
      authenticateSession();
      await visit('/manage/maps/add');
      await click(".row-team .btn-add-team");

      expect(currentURL()).to.equal('/manage/teams/add?next=manage.maps.add');
    });
  });
});
