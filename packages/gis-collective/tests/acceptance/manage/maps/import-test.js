import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, waitUntil, click } from '@ember/test-helpers';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';
import PageElements from '../../../helpers/page-elements';

describe('Acceptance | manage/maps/import', function () {
  setupApplicationTest();
  let server;
  let map;
  let mapFile;
  let receivedFile;
  let isCancel;
  let meta;

  beforeEach(function () {
    server = new TestServer();
    map = server.testData.create.map();
    mapFile = server.testData.create.mapFile();
    meta = {
      time: '2021-02-17T19:20:14',
      ignored: 0,
      ping: '2021-02-17T19:20:15',
      total: 63,
      errors: 0,
      sent: 0,
      duration: 1,
      processed: 63,
      runId: '602d6c20630f6601005307bc.0',
      status: 'success',
    };

    server.get(`/mock-server/mapfiles/:id/meta`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(meta),
      ];
    });

    mapFile.options = {
      fields: {
        name: '',
        longitude: 'position.lon',
        latitude: 'position.lat',
        size: 'attributes.size',
        color: 'attributes.color',
      },
    };

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addMapFile(mapFile);
    server.testData.storage.addMap(map);
    server.testData.storage.addDefaultMap('5e2f42c96e290a010057dd95');

    receivedFile = null;
    server.put(`/mock-server/mapfiles/${mapFile._id}`, (request) => {
      receivedFile = JSON.parse(request.requestBody);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedFile),
      ];
    });

    isCancel = false;
    server.post(`/mock-server/mapfiles/${mapFile._id}/cancelImport`, () => {
      isCancel = true;

      return [200, { 'Content-Type': 'application/json' }, '{}'];
    });
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/maps/${map._id}/import`);
    expect(currentURL()).to.contain('/login');
  });

  it('show the map file', async function () {
    authenticateSession();
    server.get(`/mock-server/mapfiles/${mapFile._id}/meta`, () => [
      200,
      'text/json',
      '{}',
    ]);

    await visit(`/manage/maps/${map._id}/import`);

    expect(currentURL()).to.equal(`/manage/maps/${map._id}/import`);
    expect(this.element.querySelector('.error')).to.not.exist;

    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'Map files'
    );
    expect(PageElements.breadcrumbs()).to.deep.equal([
      'Browse',
      'Maps',
      map.name,
      'Map files',
    ]);

    const table = this.element.querySelector('.col-files table');

    expect(table.querySelectorAll('tbody tr').length).to.equal(1);

    expect(table.querySelector('.name').textContent.trim()).to.equal(
      'United Nations District Map.csv'
    );
    expect(table.querySelector('.size').textContent.trim()).to.equal('39 kB');
    expect(table.querySelector('.items-total').textContent.trim()).to.equal(
      '-'
    );

    expect(table.querySelector('.item-download').textContent.trim()).to.equal(
      'Download'
    );
    expect(table.querySelector('.item-download')).to.have.attribute(
      'href',
      mapFile.file
    );
  });

  describe('for a scheduled job', function () {
    let table;

    beforeEach(async function () {
      authenticateSession();
      server.get(`/mock-server/mapfiles/${mapFile._id}/meta`, () => [
        200,
        { 'content-type': 'application/json' },
        JSON.stringify({
          status: 'scheduled',
        }),
      ]);

      await visit(`/manage/maps/${map._id}/import`);
      table = this.element.querySelector('.col-files table');
      await waitUntil(() => table.querySelector('.progress-bar'));
    });

    it('should show an animated progress', async function () {
      const progress = table.querySelector('.progress-bar');

      expect(progress).not.to.have.class('.progress-bar-striped');
      expect(progress).to.have.class('bg-info');
      expect(progress).to.have.attribute('aria-valuenow', '1');
      expect(progress).to.have.attribute('aria-valuemax', '1');
    });

    it('should allow stopping the job', async function () {
      expect(table.querySelector('.btn-cancel-import')).to.exist;

      await click('.btn-cancel-import');

      expect(isCancel).to.equal(true);
    });
  });

  describe('for a preparing job', function () {
    let table;

    beforeEach(async function () {
      authenticateSession();
      server.get(`/mock-server/mapfiles/${mapFile._id}/meta`, () => [
        200,
        { 'content-type': 'application/json' },
        JSON.stringify({
          status: 'preparing',
        }),
      ]);

      await visit(`/manage/maps/${map._id}/import`);
      table = this.element.querySelector('.col-files table');
      await waitUntil(() => table.querySelector('.progress-bar'));
    });

    it('should show an animated', async function () {
      const progress = table.querySelector('.progress-bar');

      expect(progress).not.to.have.class('.progress-bar-striped');
      expect(progress).to.have.attribute('aria-valuenow', '1');
      expect(progress).to.have.attribute('aria-valuemax', '1');
    });

    it('should allow stopping the job', async function () {
      expect(table.querySelector('.btn-cancel-import')).to.exist;

      await click('.btn-cancel-import');

      expect(isCancel).to.equal(true);
    });
  });

  describe('for a running job', function () {
    let table;

    beforeEach(async function () {
      authenticateSession();
      server.get(`/mock-server/mapfiles/${mapFile._id}/meta`, () => [
        200,
        { 'content-type': 'application/json' },
        JSON.stringify({
          runId: '5f0d77e653660f49f92276ff.0',
          time: '2020-07-14T11:17:24',
          ping: '2020-07-14T11:21:43',
          ignored: 0,
          total: 153297,
          errors: 0,
          sent: 0,
          duration: 259,
          processed: 2600,
          status: 'running',
        }),
      ]);

      await visit(`/manage/maps/${map._id}/import`);
      table = this.element.querySelector('.col-files table');
      await waitUntil(() => table.querySelector('.progress-bar'));
    });

    it('should show an animated', async function () {
      const progress = table.querySelector(
        '.progress-bar.progress-bar-striped.bg-success'
      );

      expect(progress).to.have.attribute('aria-valuenow', '2600');
      expect(progress).to.have.attribute('aria-valuemax', '153297');
    });

    it('should allow stopping the job', async function () {
      expect(table.querySelector('.btn-cancel-import')).to.exist;

      await click('.btn-cancel-import');

      expect(isCancel).to.equal(true);
    });
  });

  describe('for a success job', function () {
    let table;

    beforeEach(async function () {
      authenticateSession();
      server.get(`/mock-server/mapfiles/${mapFile._id}/meta`, () => [
        200,
        { 'content-type': 'application/json' },
        JSON.stringify({
          runId: '5f0d77e653660f49f92276ff.0',
          time: '2020-07-14T11:17:24',
          ping: '2020-07-14T11:21:43',
          ignored: 0,
          total: 153297,
          errors: 0,
          sent: 0,
          duration: 259,
          processed: 153297,
          status: 'success',
        }),
      ]);

      await visit(`/manage/maps/${map._id}/import`);
      table = this.element.querySelector('.col-files table');
      await waitUntil(() => table.querySelector('.progress-bar'));
    });

    it('should show a simple progress', async function () {
      const progress = table.querySelector('.progress-bar');

      expect(progress).not.to.have.class('.progress-bar-striped');
      expect(progress).to.have.class('bg-success');
      expect(progress).to.have.attribute('aria-valuenow', '153297');
      expect(progress).to.have.attribute('aria-valuemax', '153297');
    });
  });

  describe('for an error job', function () {
    let table;

    beforeEach(async function () {
      authenticateSession();
      server.get(`/mock-server/mapfiles/${mapFile._id}/meta`, () => [
        200,
        { 'content-type': 'application/json' },
        JSON.stringify({
          runId: '5f0d77e653660f49f92276ff.0',
          time: '2020-07-14T11:17:24',
          ping: '2020-07-14T11:21:43',
          ignored: 0,
          total: 153297,
          errors: 1000,
          sent: 0,
          duration: 259,
          processed: 0,
          status: 'error',
        }),
      ]);

      await visit(`/manage/maps/${map._id}/import`);
      table = this.element.querySelector('.col-files table');
      await waitUntil(() => table.querySelector('.progress-bar'));
    });

    it('should show a simple progress', async function () {
      const progress = table.querySelector('.progress-bar');

      expect(progress).not.to.have.class('.progress-bar-striped');
      expect(progress).to.have.class('bg-danger');
      expect(progress).to.have.attribute('aria-valuenow', '1000');
      expect(progress).to.have.attribute('aria-valuemax', '153297');
    });
  });

  describe('for a cancelled job', function () {
    let table;

    beforeEach(async function () {
      authenticateSession();
      server.get(`/mock-server/mapfiles/${mapFile._id}/meta`, () => [
        200,
        { 'content-type': 'application/json' },
        JSON.stringify({
          runId: '5f0d77e653660f49f92276ff.0',
          time: '2020-07-14T11:17:24',
          ping: '2020-07-14T11:21:43',
          ignored: 0,
          total: 153297,
          errors: 0,
          sent: 0,
          duration: 259,
          processed: 153297,
          status: 'cancelled',
        }),
      ]);

      await visit(`/manage/maps/${map._id}/import`);
      table = this.element.querySelector('.col-files table');
      await waitUntil(() => table.querySelector('.progress-bar'));
    });

    it('should show a simple progress', async function () {
      const progress = table.querySelector('.progress-bar');

      expect(progress).not.to.have.class('.progress-bar-striped');
      expect(progress).to.have.class('bg-success');
      expect(progress).to.have.attribute('aria-valuenow', '153297');
      expect(progress).to.have.attribute('aria-valuemax', '153297');
    });
  });

  describe('for a timeout job', function () {
    let table;

    beforeEach(async function () {
      authenticateSession();
      server.get(`/mock-server/mapfiles/${mapFile._id}/meta`, () => [
        200,
        { 'content-type': 'application/json' },
        JSON.stringify({
          runId: '5f0d77e653660f49f92276ff.0',
          time: '2020-07-14T11:17:24',
          ping: '2020-07-14T11:21:43',
          ignored: 0,
          total: 153297,
          errors: 0,
          sent: 0,
          duration: 259,
          processed: 153297,
          status: 'timeout',
        }),
      ]);

      await visit(`/manage/maps/${map._id}/import`);
      table = this.element.querySelector('.col-files table');
      await waitUntil(() => table.querySelector('.progress-bar'));
    });

    it('should show a simple progress', async function () {
      const progress = table.querySelector('.progress-bar');

      expect(progress).not.to.have.class('.progress-bar-striped');
      expect(progress).to.have.class('bg-success');
      expect(progress).to.have.attribute('aria-valuenow', '153297');
      expect(progress).to.have.attribute('aria-valuemax', '153297');
    });
  });

  it('should show the import log by pressing the view log button', async function () {
    authenticateSession();
    server.get(`/mock-server/mapfiles/${mapFile._id}/meta`, () => [
      200,
      { 'content-type': 'application/json' },
      JSON.stringify({
        status: 'warning',
        isRunning: true,
      }),
    ]);

    server.get(`/mock-server/mapfiles/${mapFile._id}/log`, () => [
      200,
      { 'content-type': 'application/json' },
      JSON.stringify({
        id: mapFile._id,
        log: 'this is the log',
      }),
    ]);

    await visit(`/manage/maps/${map._id}/import`);

    await click('.btn-open-options');
    await click('.item-view-log');

    expect(this.element.querySelector('pre.log').textContent).to.contain(
      `this is the log`
    );
  });

  it('should show the field match page by pressing the file name', async function () {
    authenticateSession();
    server.get(`/mock-server/mapfiles/${mapFile._id}/meta`, () => [
      200,
      { 'content-type': 'application/json' },
      JSON.stringify({
        status: 'warning',
        isRunning: true,
      }),
    ]);

    server.get(`/mock-server/mapfiles/${mapFile._id}/log`, () => [
      200,
      { 'content-type': 'application/json' },
      JSON.stringify({
        id: mapFile._id,
        log: 'this is the log',
      }),
    ]);

    await visit(`/manage/maps/${map._id}/import`);
    await click('td.name button');

    expect(currentURL()).to.contain(
      `/wizard/import/file/5e3bf85528c8144486996173`
    );
  });
});
