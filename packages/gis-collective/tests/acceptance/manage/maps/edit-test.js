import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  waitUntil,
  triggerEvent,
  waitFor,
  fillIn,
  triggerKeyEvent,
} from '@ember/test-helpers';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';
import PageElements from '../../../helpers/page-elements';

describe('Acceptance | manage/maps/edit', function () {
  setupApplicationTest();
  let server;
  let map;
  let mapFile;
  let receivedMap;
  let defaultPicture;

  beforeEach(function () {
    server = new TestServer();
    defaultPicture = server.testData.create.picture();
    defaultPicture._id = '1';

    map = server.testData.create.map();
    mapFile = server.testData.create.mapFile();
    const user = server.testData.create.user('5b8a59caef739394031a3f67');

    server.testData.storage.addDefaultTeam('000000000000000000000001');

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultPreferences();

    server.testData.storage.addDefaultBaseMap();
    server.testData.storage.addDefaultBaseMap('000000000000000000000002');

    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();

    server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultIconSet('000000000000000000000002');

    server.testData.storage.addMapFile(mapFile);
    server.testData.storage.addMap(map);

    server.post(`/mock-server/pictures`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: defaultPicture,
        }),
      ];
    });

    server.put(`/mock-server/maps/${map._id}`, (request) => {
      receivedMap = JSON.parse(request.requestBody);
      receivedMap.map._id = map._id;

      server.testData.storage.addMap(receivedMap.map);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedMap),
      ];
    });

    receivedMap = null;
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should contain all the breadcrumbs links', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal([
      'Browse',
      'Maps',
      map.name,
      'Edit',
    ]);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/browse',
      '/browse/maps',
      `/browse/sites?map=${map._id}`,
    ]);
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/maps/${map._id}`);
    expect(currentURL()).to.equal(
      '/login?redirect=%2Fmanage%2Fmaps%2F5ca89e37ef1f7e010007f54c'
    );
  });

  it('renders the form for the default map', async function () {
    this.timeout(15000);
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    await waitFor('.editor-is-ready', { timeout: 3000 });

    expect(this.element.querySelector('.alert-map-with-unpublished-basemaps'))
      .not.to.exist;
    expect(this.element.querySelector('.ce-header').textContent).to.equal(
      map.name
    );
    expect(PageElements.containerGroupTitles()).to.deep.equal([
      'team',
      'original author',
      'Tagline',
      'start date',
      'end date',
      'icon sets',
      'base maps',
      'mapping area',
      'mask features',
      'license',
      'embedded map',
      'vector tiles url',
    ]);

    expect(PageElements.containerGroupContents()).to.deep.equal([
      'Open Green Map',
      'is public',
      'add features as pending',
      'show download links',
      'Feature clustering',
      '@unknown\n\n\n    \n      created on 1/1/1',
      'Gândește global, carografiază local!',
      'Tue Jan 01 2008',
      'not set',
      'The default icon sets will be used.',
      'The default base maps will be used.',
      '',
      'Features belonging to this map will not be masked.',
      'unknown license',
    ]);
  });

  it('renders a link to the team in the visibility section', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    expect(this.element.querySelector('.container-group-team a')).to.exist;
    expect(
      this.element.querySelector('.container-group-team a')
    ).to.have.attribute('href', '/browse/teams/5ca78e2160780601008f69e6');
  });

  it('can change the name', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);
    await waitFor('.editor-is-ready', { timeout: 3000 });

    await click('.ce-header');
    await fillIn('.ce-header', 'new name');
    await triggerKeyEvent('.ce-header', 'keyup', 'Enter');

    await waitUntil(
      () => !this.element.querySelector('.btn-publish').hasAttribute('disabled')
    );
    await click('.btn-publish');

    await waitUntil(() => receivedMap);
    expect(receivedMap.map.name).to.equal('new name');
  });

  it('can change the description', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);
    await waitFor('.editor-is-ready', { timeout: 3000 });

    await fillIn('.ce-paragraph', 'new description');
    await waitUntil(
      () =>
        !this.element.querySelector('.btn-publish').hasAttribute('disabled'),
      { timeout: 3000 }
    );
    await click('.btn-publish');

    await waitUntil(() => receivedMap);
    expect(receivedMap.map.description.blocks).to.deep.equal([
      { type: 'header', data: { text: map.name, level: 1 } },
      { type: 'paragraph', data: { text: 'new description' } },
    ]);
  });

  it('can change the map cover', async function () {
    this.timeout(40000);
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);
    const blob = server.testData.create.pngBlob();

    await triggerEvent(".row-cover .cover input[type='file']", 'change', {
      files: [blob],
    });

    await waitUntil(() => receivedMap);
    expect(receivedMap.map.squareCover).to.equal(null);
    expect(receivedMap.map.cover).to.equal('1');
  });

  it('can change the map square cover', async function () {
    this.timeout(40000);
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);
    const blob = server.testData.create.pngBlob();

    await triggerEvent(
      ".row-cover .square-cover input[type='file']",
      'change',
      { files: [blob] }
    );

    await waitUntil(() => receivedMap);
    expect(receivedMap.map.squareCover).to.equal('1');
    expect(receivedMap.map.cover).to.equal('5cc8dc1038e882010061545a');
  });

  it('can add an icon set', async function () {
    this.timeout(4000);
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);

    await click('.container-group-icon-sets .btn-edit');
    await click('.container-group-icon-sets .form-switch input');

    await click('.container-group-icon-sets .btn-add-item');
    await click('.container-group-icon-sets .btn-submit');

    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(receivedMap.map.iconSets).to.deep.equal({
      useCustomList: true,
      list: ['5ca7b702ecd8490100cab96f', '000000000000000000000002'],
    });
  });

  it('can update the map license', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);

    await click('.container-group-license .btn-edit');

    await fillIn('.container-group-license .input-license-url', 'new url');
    await fillIn('.container-group-license .input-license-name', 'new name');

    await click('.container-group-license .btn-submit');

    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(receivedMap.map.license).to.deep.equal({
      name: 'new name',
      url: 'new url',
    });
  });

  it('can add an base map', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);

    await click('.container-group-base-maps .btn-edit');
    await click('.container-group-base-maps .form-switch input');

    await click('.container-group-base-maps .btn-add-item');
    await click('.container-group-base-maps .btn-submit');

    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(receivedMap.map.baseMaps).to.deep.equal({
      useCustomList: true,
      list: ['000000000000000000000002'],
    });
  });

  it('should render the "is public" label', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    expect(this.element.querySelector('.side-bar').textContent).to.contain(
      'is public'
    );
    expect(
      this.element.querySelector('.container-group-is-public label').textContent
    ).to.contain('is public');
  });

  it('should be able to unpublish the map', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(
      this.element.querySelector(
        '.side-bar .container-group-is-public .btn-switch'
      )
    );

    await waitUntil(() => receivedMap != null, { timeout: 3000 });

    expect(receivedMap.map.visibility.isPublic).to.equal(false);
  });

  it('should be able to set the addFeaturesAsPending flag', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(
      this.element.querySelector(
        '.side-bar .container-group-add-features-as-pending .btn-switch'
      )
    );

    await waitUntil(() => receivedMap != null, { timeout: 3000 });

    expect(receivedMap.map.addFeaturesAsPending).to.equal(true);
  });

  it('should be able to enable the download links', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(
      this.element.querySelector(
        '.side-bar .container-group-show-download-links .btn-switch'
      )
    );

    await waitUntil(() => receivedMap != null, { timeout: 3000 });

    expect(receivedMap.map.showPublicDownloadLinks).to.equal(true);
  });

  it('should be able to change the team', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-team .btn-edit');

    this.element.querySelector('.value.editing select').value =
      '000000000000000000000001';
    await triggerEvent('.value.editing select', 'change');

    await waitUntil(() => receivedMap != null, { timeout: 3000 });

    expect(receivedMap.map.visibility.team).to.equal(
      '000000000000000000000001'
    );
  });

  it('should render the iframe embed code', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);
    const code = this.element
      .querySelector('.row-embedded-map textarea')
      .textContent.replace('\n', '')
      .split(' ')
      .map((a) => a.trim())
      .filter((a) => a)
      .join(' ');

    expect(code).to.startWith(`<iframe src="https://localhost:`);
    expect(code).to.endWith(
      `/browse/maps/${map._id}/map-view?embed=true" allowFullScreen="true" allow="fullscreen; geolocation;" title="${map.name}" frameborder="0" width="100%" height="500"></iframe>`
    );
  });

  it('should render the vector tiles url', async function () {
    authenticateSession();

    await visit(`/manage/maps/${map._id}`);

    expect(
      this.element.querySelector('.row-vector-tiles a').textContent.trim()
    ).to.startWith(`https://localhost:`);

    expect(
      this.element.querySelector('.row-vector-tiles a').textContent.trim()
    ).to.endWith(
      `/mock-server/tile/about.json?map=5ca89e37ef1f7e010007f54c&authorization=[your api key]`
    );

    expect(
      this.element.querySelector('.row-vector-tiles a').getAttribute('href')
    ).to.startWith(`https://localhost:`);

    expect(
      this.element.querySelector('.row-vector-tiles a').getAttribute('href')
    ).to.endWith(`/mock-server/tile/about.json?map=5ca89e37ef1f7e010007f54c`);
  });

  it('should not show the show all teams button', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-team .btn-edit');

    expect(
      this.element.querySelector('.container-group-team .btn-show-all-teams')
    ).not.to.exist;
  });

  it('should not show the hideOnMainMap toggle', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    expect(
      this.element.querySelector(
        '.side-bar .container-group-hide-on-main-map .btn-switch'
      )
    ).not.to.exist;
    expect(
      this.element
        .querySelector('.side-bar .value-hide-on-main-map')
        .textContent.trim()
    ).to.equal('no');
  });

  it('should show yes in the hideOnMainMap section when the map is hidden', async function () {
    map = server.testData.create.map('3');
    map.hideOnMainMap = true;
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/maps/3`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    expect(
      this.element.querySelector(
        '.side-bar .container-group-hide-on-main-map .btn-switch'
      )
    ).not.to.exist;
    expect(
      this.element
        .querySelector('.side-bar .container-group-hide-on-main-map .value')
        .textContent.trim()
    ).to.equal('yes');
  });

  it('should show yes in the index map section when the map is an index', async function () {
    map = server.testData.create.map('3');
    map.isIndex = true;
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    expect(
      this.element.querySelector(
        '.side-bar .container-group-is-index .btn-switch'
      )
    ).not.to.exist;
    expect(
      this.element
        .querySelector('.side-bar .container-group-is-index .value')
        .textContent.trim()
    ).to.equal('yes');
  });

  it('should be able to change the clustering mode', async function () {
    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('#feature-clustering');

    await waitUntil(() => receivedMap);
    expect(receivedMap.map.cluster).to.deep.equal({ mode: 1, map: '' });
  });

  it('should be able to enable a map mask', async function () {
    let maskMap = server.testData.create.map('mask-map');
    server.testData.storage.addMap(maskMap);

    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    await click('.container-group-mask-features .btn-edit');
    await click('.container-group-mask-features .enable-map');

    this.element.querySelector('.container-group-mask-features select').value =
      'mask-map';
    await triggerEvent('select', 'change');
    await click('.container-group-mask-features .btn-submit');
    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(receivedMap.map.mask).to.deep.equal({
      isEnabled: true,
      map: 'mask-map',
    });
  });

  it('should redirect to the team dashboard page after deleting the map', async function () {
    server.server.delete(`/mock-server/maps/${map._id}`, () => {
      return [200, { 'Content-Type': 'application/json' }, JSON.stringify({})];
    });

    authenticateSession();
    await visit(`/manage/maps/${map._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.btn-delete');

    await Modal.waitToDisplay();
    await Modal.clickDangerButton();

    expect(
      server.history.filter((a) => a.indexOf('DELETE') != -1)[0]
    ).to.contain(`/mock-server/maps/${map._id}`);
    expect(currentURL()).to.equal(
      `/manage/dashboards/5ca78e2160780601008f69e6`
    );
  });

  describe('when the authenticated user is an administrator', function () {
    beforeEach(function () {
      server.testData.storage.addDefaultUser(true);
    });

    it('should be able to toggle the hideOnMainMap field', async function () {
      authenticateSession();
      await visit(`/manage/maps/${map._id}`);

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      await click(
        this.element.querySelector(
          '.side-bar .container-group-hide-on-main-map .btn-switch'
        )
      );

      await waitUntil(() => receivedMap != null, { timeout: 3000 });

      expect(receivedMap.map.hideOnMainMap).to.equal(true);
    });

    it('should be able to toggle the is index field', async function () {
      authenticateSession();
      await visit(`/manage/maps/${map._id}`);

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      await click(
        this.element.querySelector(
          '.side-bar .container-group-is-index .btn-switch'
        )
      );

      await waitUntil(() => receivedMap != null, { timeout: 3000 });

      expect(receivedMap.map.isIndex).to.equal(true);
    });

    it('should show all teams on pressing the show all teams button', async function () {
      authenticateSession();
      await visit(`/manage/maps/${map._id}`);

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      expect(server.history).not.to.contain(`GET /mock-server/teams?all=true`);

      await click('.container-group-team .btn-edit');
      await click('.container-group-team .btn-show-all-teams');

      expect(currentURL()).to.equal(`/manage/maps/${map._id}?allTeams=true`);
      expect(server.history).to.contain(`GET /mock-server/teams?all=true`);

      expect(
        this.element.querySelector('.container-group-team .btn-show-all-teams')
      ).to.exist;
    });

    it('should show owned teams on pressing the show all teams button', async function () {
      authenticateSession();
      await visit(`/manage/maps/${map._id}?allTeams=true`);

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      expect(server.history).not.to.contain(`GET /mock-server/teams?edit=true`);

      await click('.container-group-team .btn-edit');
      await click('.container-group-team .btn-show-all-teams');

      expect(currentURL()).to.equal(`/manage/maps/${map._id}`);
      expect(server.history).to.contain(`GET /mock-server/teams?edit=true`);

      expect(
        this.element.querySelector('.container-group-team .btn-show-all-teams')
      ).to.exist;
    });
  });

  describe('when there is a private base map', function () {
    beforeEach(function () {
      const basemap = server.testData.create.baseMap(
        '000000000000000000000002'
      );
      basemap.visibility.isPublic = false;
      server.testData.storage.addBaseMap(basemap);
    });

    it('should show an alert that the basemaps are private', async function () {
      authenticateSession();

      await visit(`/manage/maps/${map._id}`);

      await click('.container-group-base-maps .btn-edit');
      await click('.container-group-base-maps .form-switch input');

      await click('.container-group-base-maps .btn-add-item');
      await click('.container-group-base-maps .btn-submit');

      await waitUntil(() => !this.element.querySelector('.fa-spinner'));
      expect(this.element.querySelector('.alert-map-with-unpublished-basemaps'))
        .to.exist;
    });
  });
});
