import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, click } from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';

describe('Acceptance | manage/dashboards/team', function () {
  setupApplicationTest();
  let server;
  let team;

  beforeEach(function () {
    this.timeout(10000);
    server = new TestServer();
    team = server.testData.create.team();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addTeam(team);
    server.testData.storage.addDefaultIcon();
  });

  afterEach(function () {
    Modal.clear();
    server.shutdown();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/dashboards/${team._id}`);
    expect(currentURL()).to.equal(
      '/login?redirect=%2Fmanage%2Fdashboards%2F5ca78e2160780601008f69e6'
    );
  });

  it('should show the dashboard page', async function () {
    authenticateSession();

    await visit(`/manage/dashboards/${team._id}`);

    expect(currentURL()).to.equal(`/manage/dashboards/${team._id}`);
    expect(this.element.querySelector('.error')).not.to.exist;
    expect(this.element.querySelector('header').textContent.trim()).to.contain(
      team.name
    );
  });

  describe('when the team has no maps', function () {
    it('should display the option to add a map', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelector('.alert-info')).to.exist;
      expect(this.element.querySelector('.btn-add-map')).to.exist;
      expect(this.element.querySelector('.alert-info a')).to.have.attribute(
        'href',
        '/manage/maps/add'
      );
    });

    it('should not display the option to add a map when the team is a publisher', async function () {
      team.isPublisher = true;
      server.testData.storage.addTeam(team);

      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(this.element.querySelector('.alert-info')).not.to.exist;
      expect(this.element.querySelector('.btn-add-map')).not.to.exist;
    });
  });

  describe('when the team has a map', function () {
    let map;
    let picture;

    beforeEach(function () {
      map = server.testData.create.map('5ca89e27ef1f7e010007f4af');
      map.canEdit = true;
      map.visibility.team = team._id;

      picture = server.testData.create.picture();

      server.testData.storage.addMap(map);
      server.testData.storage.addPicture(picture);
    });

    it('should display the map as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(
        this.element.querySelectorAll('.card-dashboard-map').length
      ).to.equal(1);
      expect(
        this.element
          .querySelector('.card-dashboard-map .title')
          .textContent.trim()
      ).to.equal(map.name);
      expect(
        this.element
          .querySelector('.card-dashboard-map .description')
          .textContent.trim()
      ).to.equal(map.description);
    });

    it('should be able to delete the map', async function () {
      authenticateSession();
      let deletedSite = false;
      server.delete(`/mock-server/maps/${map._id}`, () => {
        deletedSite = true;

        return [204, {}, ''];
      });
      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-map .dropdown-toggle');
      await click('.card-dashboard-map .btn-delete');

      await click('.btn-delete');
      await Modal.waitToDisplay();
      await Modal.clickDangerButton();

      expect(deletedSite).to.eql(true);
      expect(this.element.querySelector('.card-dashboard-map')).not.to.exist;
    });
  });

  describe('when the team has a campaign', function () {
    let campaign;

    beforeEach(function () {
      campaign = server.testData.create.campaign();
      campaign.canEdit = true;
      campaign.visibility.isPublic = false;
      campaign.visibility.team = team._id;

      server.testData.storage.addCampaign(campaign);
    });

    it('should display the campaign as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(
        this.element.querySelectorAll('.card-dashboard-campaign').length
      ).to.equal(1);
      expect(
        this.element
          .querySelector('.card-dashboard-campaign .title')
          .textContent.trim()
      ).to.equal(campaign.name);
      expect(
        this.element
          .querySelector('.card-dashboard-campaign .description')
          .textContent.trim()
      ).to.equal(campaign.article);
    });
  });

  describe('when the team has an iconset', function () {
    let iconSet;

    beforeEach(function () {
      iconSet = server.testData.create.iconSet('5ca89e27ef1f7e010007f4af');
      iconSet.canEdit = true;
      iconSet.visibility.team = team._id;

      server.testData.storage.addIconSet(iconSet);
    });

    it('should display the iconSet as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(
        this.element.querySelectorAll('.card-dashboard-icon-set').length
      ).to.equal(1);
      expect(
        this.element
          .querySelector('.card-dashboard-icon-set .title')
          .textContent.trim()
      ).to.equal(iconSet.name);
      expect(
        this.element
          .querySelector('.card-dashboard-icon-set .description')
          .textContent.trim()
      ).to.equal(iconSet.description);
    });

    it('should be able to delete the iconset', async function () {
      authenticateSession();
      let deletedSite = false;
      server.delete(`/mock-server/iconsets/${iconSet._id}`, () => {
        deletedSite = true;

        return [204, {}, ''];
      });
      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-icon-set .dropdown-toggle');
      await click('.card-dashboard-icon-set .btn-delete');

      await click('.btn-delete');
      await Modal.waitToDisplay();
      await Modal.clickDangerButton();

      expect(deletedSite).to.eql(true);
      expect(this.element.querySelector('.card-dashboard-icon-set')).not.to
        .exist;
    });
  });

  describe('when the team has a basemap', function () {
    let basemap;

    beforeEach(function () {
      basemap = server.testData.create.baseMap('5ca89e27ef1f7e010007f4af');
      basemap.canEdit = true;
      basemap.visibility.team = team._id;

      server.testData.storage.addBaseMap(basemap);
    });

    it('should display the basemap as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(
        this.element.querySelectorAll('.card-dashboard-basemap').length
      ).to.equal(1);
      expect(
        this.element
          .querySelector('.card-dashboard-basemap .title')
          .textContent.trim()
      ).to.equal(basemap.name);
    });

    it('should be able to delete the basemap', async function () {
      authenticateSession();
      let deletedSite = false;
      server.delete(`/mock-server/basemaps/${basemap._id}`, () => {
        deletedSite = true;

        return [204, {}, ''];
      });
      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-basemap .dropdown-toggle');
      await click('.card-dashboard-basemap .btn-delete');

      await click('.btn-delete');
      await Modal.waitToDisplay();
      await Modal.clickDangerButton();

      expect(deletedSite).to.eql(true);
      expect(this.element.querySelector('.card-dashboard-basemap')).not.to
        .exist;
    });
  });

  describe('when the team has a page', function () {
    let page;

    beforeEach(function () {
      page = server.testData.create.page('5ca89e27ef1f7e010007f4af');
      page.canEdit = true;
      page.visibility.team = team._id;

      server.testData.storage.addPage(page);
    });

    it('should display the page as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(
        this.element.querySelectorAll('.card-dashboard-page').length
      ).to.equal(1);
      expect(
        this.element
          .querySelector('.card-dashboard-page .title')
          .textContent.trim()
      ).to.equal(page.name);
    });

    it('should be able to delete the page', async function () {
      authenticateSession();
      let deletedPage = false;
      server.delete(`/mock-server/pages/${page._id}`, () => {
        deletedPage = true;

        return [204, {}, ''];
      });
      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-page .dropdown-toggle');
      await click('.card-dashboard-page .btn-delete');

      await click('.btn-delete');
      await Modal.waitToDisplay();
      await Modal.clickDangerButton();

      expect(deletedPage).to.eql(true);
      expect(this.element.querySelector('.card-dashboard-page')).not.to.exist;
    });
  });

  describe('when the team has a presentation', function () {
    let presentation;

    beforeEach(function () {
      presentation = server.testData.create.presentation(
        '5ca89e27ef1f7e010007f4af'
      );
      presentation.canEdit = true;
      presentation.visibility.team = team._id;

      server.testData.storage.addPresentation(presentation);
    });

    it('should display the presentation as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(
        this.element.querySelectorAll('.card-dashboard-presentation').length
      ).to.equal(1);
      expect(
        this.element
          .querySelector('.card-dashboard-presentation .title')
          .textContent.trim()
      ).to.equal(presentation.name);
    });

    it('should be able to delete the presentation', async function () {
      authenticateSession();
      let deletedPresentation = false;
      server.delete(`/mock-server/presentations/${presentation._id}`, () => {
        deletedPresentation = true;

        return [204, {}, ''];
      });
      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-presentation .dropdown-toggle');
      await click('.card-dashboard-presentation .btn-delete');

      await click('.btn-delete');
      await Modal.waitToDisplay();
      await Modal.clickDangerButton();

      expect(deletedPresentation).to.eql(true);
      expect(this.element.querySelector('.card-dashboard-presentation')).not.to
        .exist;
    });
  });

  describe('when the team has an article', function () {
    let article;

    beforeEach(function () {
      article = server.testData.create.article('5ca89e27ef1f7e010007f4af');
      article.canEdit = true;
      article.visibility.team = team._id;

      server.testData.storage.addArticle(article);
    });

    it('should display the article as a card', async function () {
      authenticateSession();

      await visit(`/manage/dashboards/${team._id}`);

      expect(
        this.element.querySelectorAll('.card-dashboard-article').length
      ).to.equal(2);
      expect(
        this.element
          .querySelector('.card-dashboard-article .title')
          .textContent.trim()
      ).to.equal(article.title);
    });

    it('should be able to delete the article', async function () {
      authenticateSession();
      let deletedArticle = false;
      server.delete(`/mock-server/articles/${article._id}`, () => {
        deletedArticle = true;

        return [204, {}, ''];
      });
      await visit(`/manage/dashboards/${team._id}`);

      await click('.card-dashboard-article .dropdown-toggle');
      await click('.card-dashboard-article .btn-delete');

      await click('.btn-delete');
      await Modal.waitToDisplay();
      await Modal.clickDangerButton();

      expect(deletedArticle).to.eql(true);
      expect(
        this.element.querySelectorAll('.card-dashboard-article')
      ).to.have.length(1);
    });
  });
});
