import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  waitUntil,
  triggerEvent,
  waitFor,
  fillIn,
  triggerKeyEvent,
} from '@ember/test-helpers';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';
import PageElements from '../../../helpers/page-elements';
import typeIn from '@ember/test-helpers/dom/type-in';

describe('Acceptance | manage/campaigns/edit', function () {
  setupApplicationTest();
  let server;
  let campaign;
  let receivedCampaign;
  let deletedCampaign;
  let defaultPicture;

  beforeEach(function () {
    server = new TestServer();
    defaultPicture = server.testData.create.picture();
    defaultPicture._id = '1';

    const user = server.testData.create.user('some-user-id');
    user.name = 'some user';
    user.email = 'some@asda.asd';

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultTeam('000000000000000000000001');
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultIconSet();
    campaign = server.testData.storage.addDefaultCampaign();
    server.testData.storage.addDefaultMap('5ca89e37ef1f7e010007f54c');

    receivedCampaign = null;

    server.post(`/mock-server/pictures`, () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({
          picture: defaultPicture,
        }),
      ];
    });

    server.put(`/mock-server/campaigns/${campaign._id}`, (request) => {
      receivedCampaign = JSON.parse(request.requestBody);
      receivedCampaign.campaign._id = campaign._id;

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify({ campaign }),
      ];
    });

    server.delete(`/mock-server/campaigns/${campaign._id}`, () => {
      deletedCampaign = true;

      return [204, {}, ''];
    });
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should contain all the browse links', async function () {
    authenticateSession();
    await visit(`/manage/campaigns/${campaign._id}`);

    expect(PageElements.breadcrumbs()).to.deep.equal([
      'Campaigns',
      campaign.name,
      'Edit',
    ]);
    expect(PageElements.breadcrumbLinks()).to.deep.equal([
      '/campaigns',
      `/campaigns/${campaign._id}`,
    ]);
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/campaigns/${campaign._id}`);
    expect(currentURL()).to.equal(
      '/login?redirect=%2Fmanage%2Fcampaigns%2F5ca78aa160780601008f6aaa'
    );
  });

  it('should be able to change the team', async function () {
    authenticateSession();
    await visit(`/manage/campaigns/${campaign._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-team .btn-edit');

    this.element.querySelector('.value.editing select').value =
      '000000000000000000000001';

    await triggerEvent('.value.editing select', 'change');
    await waitUntil(() => receivedCampaign != null, { timeout: 5000 });

    expect(receivedCampaign.campaign.visibility.team).to.equal(
      '000000000000000000000001'
    );
  });

  it('should be able to change the `isPublic` flag', async function () {
    authenticateSession();
    await visit(`/manage/campaigns/${campaign._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('#toggle-is-public');

    await waitUntil(() => receivedCampaign != null, { timeout: 5000 });

    expect(receivedCampaign.campaign.visibility.isPublic).to.equal(false);
  });

  it('should be able to change the `original author`', async function () {
    this.timeout(20000);
    authenticateSession();
    await visit(`/manage/campaigns/${campaign._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-original-author .btn-edit');
    await click('.ember-power-select-trigger');
    await fillIn('.ember-power-select-search-input', 'some user');
    await click('.ember-power-select-option');

    await waitUntil(() => receivedCampaign, { timeout: 10000 });

    expect(receivedCampaign.campaign.info.originalAuthor).to.equal(
      'some-user-id'
    );
  });

  it('can change the campaign options', async function () {
    authenticateSession();
    await visit(`/manage/campaigns/${campaign._id}`);

    await click('.container-group-questions .btn-edit');
    await click('.container-group-questions .btn-submit');

    await waitUntil(() => receivedCampaign, { timeout: 10000 });

    expect(receivedCampaign.campaign.options).to.deep.equal({
      showNameQuestion: false,
      nameLabel: '',
      showDescriptionQuestion: false,
      descriptionLabel: '',
      iconsLabel: '',
    });
  });

  it('can change the restrict to registered users', async function () {
    authenticateSession();
    await visit(`/manage/campaigns/${campaign._id}`);

    await click('#toggle-mandatory-registration');

    await waitUntil(() => receivedCampaign, { timeout: 10000 });

    expect(receivedCampaign.campaign.options).to.deep.equal({
      registrationMandatory: true,
    });
  });

  it('can change the feature name prefix', async function () {
    authenticateSession();
    await visit(`/manage/campaigns/${campaign._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-feature-name-prefix .btn-edit');
    await typeIn('.container-group-feature-name-prefix input', 'new value');
    await click('.container-group-feature-name-prefix .btn-submit');

    await waitUntil(() => receivedCampaign, { timeout: 10000 });

    expect(receivedCampaign.campaign.options).to.deep.equal({
      featureNamePrefix: 'new value',
    });
  });

  it('should should be able to delete the campaign', async function () {
    this.timeout(20000);
    authenticateSession();
    await visit(`/manage/campaigns/${campaign._id}`);

    await click('.side-bar .btn-open');
    await click('.side-bar .btn-delete');
    await waitFor('.modal.show');

    expect(
      this.element.querySelector('.modal.show .modal-header').textContent
    ).to.contain('delete campaign');
    expect(
      this.element.querySelector('.modal.show .modal-body').textContent
    ).to.contain('Are you sure you want to delete `Campaign 1`?');

    await click('.modal.show .btn-danger');

    expect(deletedCampaign).to.equal(true);
    expect(currentURL()).to.equal(`/campaigns`);
  });

  it('can change the name', async function () {
    authenticateSession();
    await visit(`/manage/campaigns/${campaign._id}`);
    await waitFor('.editor-is-ready', { timeout: 3000 });

    await click('.ce-header');
    await fillIn('.ce-header', 'new name');
    await triggerKeyEvent('.ce-header', 'keyup', 'Enter');

    await waitUntil(
      () => !this.element.querySelector('.btn-publish').hasAttribute('disabled')
    );
    await click('.btn-publish');

    await waitUntil(() => receivedCampaign, { timeout: 3000 });
    expect(receivedCampaign.campaign.name).to.equal('new name');
  });

  it('can change the article', async function () {
    authenticateSession();
    this.timeout(15000);
    await visit(`/manage/campaigns/${campaign._id}`);
    await waitFor('.editor-is-ready', { timeout: 3000 });

    await fillIn('.ce-paragraph', 'new description');
    await waitUntil(
      () =>
        !this.element.querySelector('.btn-publish').hasAttribute('disabled'),
      { timeout: 3000 }
    );
    await click('.btn-publish');

    await waitUntil(() => receivedCampaign, { timeout: 3000 });

    expect(receivedCampaign.campaign.article.blocks).to.deep.equal([
      { type: 'header', data: { text: campaign.name, level: 1 } },
      { type: 'paragraph', data: { text: 'new description' } },
    ]);
  });

  it('can change the cover', async function () {
    this.timeout(40000);
    authenticateSession();

    await visit(`/manage/campaigns/${campaign._id}`);
    const blob = server.testData.create.pngBlob();

    await triggerEvent(".row-cover input[type='file']", 'change', {
      files: [blob],
    });

    await waitUntil(() => receivedCampaign);
    expect(receivedCampaign.campaign.cover).to.equal('1');
  });

  it('can change the start date', async function () {
    this.timeout(40000);
    authenticateSession();

    await visit(`/manage/campaigns/${campaign._id}`);
    await click('.container-group-start-date .btn-edit');
    await click('.container-group-start-date .form-switch input');

    this.element.querySelector('.form-select.year').value = 2020;
    await triggerEvent('.form-select.year', 'change');
    this.element.querySelector('.form-select.month').value = 3;
    await triggerEvent('.form-select.month', 'change');
    this.element.querySelector('.form-select.day').value = 20;
    await triggerEvent('.form-select.day', 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedCampaign);
    expect(receivedCampaign.campaign.startDate).to.startWith('2020-04-19');
  });

  it('can change the end date', async function () {
    this.timeout(40000);
    authenticateSession();

    await visit(`/manage/campaigns/${campaign._id}`);
    await click('.container-group-end-date .btn-edit');
    await click('.container-group-end-date .form-switch input');

    this.element.querySelector('.form-select.year').value = 2020;
    await triggerEvent('.form-select.year', 'change');
    this.element.querySelector('.form-select.month').value = 3;
    await triggerEvent('.form-select.month', 'change');
    this.element.querySelector('.form-select.day').value = 20;
    await triggerEvent('.form-select.day', 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedCampaign);
    expect(receivedCampaign.campaign.endDate).to.startWith('2020-04-19');
  });

  it('should be able to enable a map campaign', async function () {
    let map = server.testData.create.map('campaign-map');
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/campaigns/${campaign._id}`);

    await click('.container-group-map .btn-edit');
    this.element.querySelector(
      '.container-group-map input.enable-map'
    ).checked = true;
    await triggerEvent('.container-group-map input.enable-map', 'change');

    this.element.querySelector('.container-group-map select').value =
      'campaign-map';
    await triggerEvent('select', 'change');
    await click('.container-group-map .btn-submit');
    await waitUntil(() => !this.element.querySelector('.fa-spinner'));

    expect(receivedCampaign.campaign.map).to.deep.equal({
      isEnabled: true,
      map: 'campaign-map',
    });
  });

  it('should allow selecting icons for the campaign', async function () {
    let map = server.testData.create.map('campaign-map');
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/campaigns/${campaign._id}`);

    await click('.row-icons .icon-select');
    await Modal.waitToDisplay();

    await fillIn('.icon-selector-filter', 'healt');
    await click('.btn-icon');
    await click('.btn-success');

    await waitUntil(() => receivedCampaign);
    expect(receivedCampaign.campaign.icons).to.deep.equal([
      '5ca7bfc0ecd8490100cab980',
    ]);
    await Modal.waitToHide();
  });

  it('should allow selecting optional icons for the campaign', async function () {
    let map = server.testData.create.map('campaign-map');
    server.testData.storage.addMap(map);

    authenticateSession();
    await visit(`/manage/campaigns/${campaign._id}`);

    await click('.container-group-questions .btn-edit');

    await click('.container-group-questions .icon-select');
    await Modal.waitToDisplay();

    await fillIn('.icon-selector-filter', 'healt');

    await click('.container-group-questions .btn-icon');
    await click('.container-group-questions .modal-footer .btn-success');

    await waitFor('.container-group-questions img.icon');
    await click('.container-group-questions .btn-submit');

    await waitUntil(() => receivedCampaign);
    expect(receivedCampaign.campaign.optionalIcons).to.deep.equal([
      '5ca7bfc0ecd8490100cab980',
    ]);
    await Modal.waitToHide();
  });

  describe('alerts', function () {
    let otherIcon;

    beforeEach(function () {
      otherIcon = server.testData.storage.addDefaultIcon('2');
    });

    it('shows an alert when there are two icons with the same name in the optional icons field', async function () {
      let map = server.testData.create.map('campaign-map');
      server.testData.storage.addMap(map);

      authenticateSession();
      await visit(`/manage/campaigns/${campaign._id}`);

      await click('.container-group-questions .btn-edit');
      await click('.container-group-questions .icon-select');
      await Modal.waitToDisplay();

      await fillIn('.icon-selector-filter', 'healt');

      const btnIcons = this.element.querySelectorAll(
        '.container-group-questions .btn-icon'
      );

      await click(btnIcons[0]);
      await click(btnIcons[1]);

      await click('.container-group-questions .modal-footer .btn-success');
      await Modal.waitToHide();
      await click('.container-group-questions .btn-submit');

      expect(this.element.querySelector('.alert-duplicate-icon-name')).to.exist;

      await click('.alert-duplicate-icon-name button');
      expect(this.element.querySelector('.alert-duplicate-icon-name')).not.to
        .exist;

      await waitUntil(() => receivedCampaign);
      expect(receivedCampaign.campaign.optionalIcons).to.deep.equal([
        '5ca7bfc0ecd8490100cab980',
      ]);
    });

    it('shows an alert when there are two icons with the same name in the icons field', async function () {
      let map = server.testData.create.map('campaign-map');
      server.testData.storage.addMap(map);

      authenticateSession();
      await visit(`/manage/campaigns/${campaign._id}`);

      await click('.row-icons .icon-select');
      await Modal.waitToDisplay();

      await fillIn('.icon-selector-filter', 'healt');

      const btnIcons = this.element.querySelectorAll('.row-icons .btn-icon');

      await click(btnIcons[0]);
      await click(btnIcons[1]);

      await click('.row-icons .btn-success');

      await Modal.waitToHide();

      expect(this.element.querySelector('.alert-duplicate-icon-name')).to.exist;

      await click('.alert-duplicate-icon-name button');
      expect(this.element.querySelector('.alert-duplicate-icon-name')).not.to
        .exist;

      await waitUntil(() => receivedCampaign);
      expect(receivedCampaign.campaign.icons).to.deep.equal([
        '5ca7bfc0ecd8490100cab980',
      ]);
    });

    it('shows an alert when there are an icon with the same name is in both icons and optional icons', async function () {
      let map = server.testData.create.map('campaign-map');
      server.testData.storage.addMap(map);

      campaign.icons = [otherIcon._id];
      campaign.optionalIcons = [otherIcon._id];

      authenticateSession();
      await visit(`/manage/campaigns/${campaign._id}`);

      expect(this.element.querySelector('.alert-duplicate-icon-name-default'))
        .to.exist;
      expect(this.element.querySelector('.alert-duplicate-icon-name-optional'))
        .to.exist;

      await click('.alert-duplicate-icon-name-optional button');

      await waitUntil(() => receivedCampaign);

      expect(receivedCampaign.campaign.icons).to.deep.equal(['2']);

      expect(receivedCampaign.campaign.optionalIcons).to.deep.equal([]);
    });
  });
});
