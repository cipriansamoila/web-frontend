import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  waitUntil,
  waitFor,
  fillIn,
  triggerEvent,
  triggerKeyEvent,
} from '@ember/test-helpers';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';
import PageElements from '../../../helpers/page-elements';

describe('Acceptance | manage/sites/edit', function () {
  setupApplicationTest();
  let server;
  let map;
  let otherMap;

  beforeEach(function () {
    server = new TestServer();
    otherMap = server.testData.create.map('2');
    otherMap.name = 'the other map';

    map = server.testData.storage.addDefaultMap();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultUser(false, '5b8a59caef739394031a3f67');
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addMap(otherMap);
    server.testData.storage.addDefaultIcon();
    server.testData.storage.addDefaultIconSet();
    server.testData.storage.addDefaultPicture();
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  describe('with a line', function () {
    let line;

    beforeEach(function () {
      line = server.testData.create.feature('5ca78e2160780601008f69e6');
      line.position.type = 'LineString';
      line.position.coordinates = [
        [1, 2],
        [3, 4],
      ];

      server.testData.storage.addFeature(line);

      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({}),
        ];
      });
    });

    it('should render the feature', async function () {
      authenticateSession();

      await visit(`/manage/sites/${line._id}`);

      expect(this.element.querySelector('.main-container > .error')).not.to
        .exist;

      expect(
        this.element.querySelector('.container-group-original-author a')
      ).to.have.attribute('href', '/browse/profiles/5b8a59caef739394031a3f67');
      expect(
        this.element.querySelector('.container-group-original-author a')
      ).to.have.attribute('href', '/browse/profiles/5b8a59caef739394031a3f67');

      expect(server.history).to.contain(
        'GET /mock-server/maps/5ca89e37ef1f7e010007f54c'
      );
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;
      const features = map.getLayers().item(1).getSource().getFeatures();

      const coordinates = features[0]
        .getGeometry()
        .transform('EPSG:3857', 'EPSG:4326')
        .getCoordinates()
        .map((pair) => [parseInt(pair[0]), parseInt(pair[1])]);

      expect(coordinates).to.deep.equal([
        [1, 2],
        [3, 4],
      ]);
    });
  });

  describe('with a multi line', function () {
    let multiline;

    beforeEach(function () {
      multiline = server.testData.create.feature('5ca78e2160780601008f69e6');
      multiline.position.type = 'MultiLineString';
      multiline.position.coordinates = [
        [
          [1, 2],
          [3, 4],
        ],
      ];

      server.testData.storage.addFeature(multiline);

      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({}),
        ];
      });
    });

    it('should render the feature', async function () {
      authenticateSession();

      await visit(`/manage/sites/${multiline._id}`);
      expect(this.element.querySelector('.main-container > .error')).not.to
        .exist;

      expect(server.history).to.contain(
        'GET /mock-server/maps/5ca89e37ef1f7e010007f54c'
      );
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;
      const features = map.getLayers().item(1).getSource().getFeatures();
      const coordinates = features[0]
        .getGeometry()
        .transform('EPSG:3857', 'EPSG:4326')
        .getCoordinates()
        .map((a) => a.map((pair) => [parseInt(pair[0]), parseInt(pair[1])]));

      expect(coordinates).to.deep.equal([
        [
          [1, 2],
          [3, 4],
        ],
      ]);
    });
  });

  describe('with a polygon', function () {
    let polygon;

    beforeEach(function () {
      polygon = server.testData.create.feature('5ca78e2160780601008f69e6');
      polygon.position.type = 'Polygon';
      polygon.position.coordinates = [
        [
          [1, 2],
          [3, 4],
          [4, 5],
          [1, 2],
        ],
      ];

      server.testData.storage.addFeature(polygon);

      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({}),
        ];
      });
    });

    it('should render the map feature', async function () {
      authenticateSession();

      await visit(`/manage/sites/${polygon._id}`);
      expect(this.element.querySelector('.main-container > .error')).not.to
        .exist;

      expect(server.history).to.contain(
        'GET /mock-server/maps/5ca89e37ef1f7e010007f54c'
      );
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;
      const features = map.getLayers().item(1).getSource().getFeatures();
      const coordinates = features[0]
        .getGeometry()
        .transform('EPSG:3857', 'EPSG:4326')
        .getCoordinates()
        .map((a) => a.map((pair) => [parseInt(pair[0]), parseInt(pair[1])]));

      expect(coordinates).to.deep.equal([
        [
          [1, 2],
          [3, 4],
          [4, 5],
          [1, 2],
        ],
      ]);
    });
  });

  describe('with a MultiPolygon', function () {
    let multiPolygon;

    beforeEach(function () {
      multiPolygon = server.testData.create.feature('5ca78e2160780601008f69e6');
      multiPolygon.position.type = 'MultiPolygon';
      multiPolygon.position.coordinates = [
        [
          [
            [1, 2],
            [3, 4],
            [4, 5],
            [1, 2],
          ],
        ],
      ];

      server.testData.storage.addFeature(multiPolygon);

      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({}),
        ];
      });
    });

    it('should render the map feature', async function () {
      authenticateSession();

      await visit(`/manage/sites/${multiPolygon._id}`);
      expect(this.element.querySelector('.main-container > .error')).not.to
        .exist;

      expect(server.history).to.contain(
        'GET /mock-server/maps/5ca89e37ef1f7e010007f54c'
      );
      await waitUntil(() => this.element.querySelector('.map').olMap);

      const map = this.element.querySelector('.map').olMap;
      const features = map.getLayers().item(1).getSource().getFeatures();
      const coordinates = features[0]
        .getGeometry()
        .transform('EPSG:3857', 'EPSG:4326')
        .getCoordinates()
        .map((a) =>
          a.map((b) => b.map((pair) => [parseInt(pair[0]), parseInt(pair[1])]))
        );

      expect(coordinates).to.deep.equal([
        [
          [
            [1, 2],
            [3, 4],
            [4, 5],
            [1, 2],
          ],
        ],
      ]);
    });
  });

  describe('with a published site', function () {
    let site;
    let receivedSite;
    let deletedSite;
    let receivedSound;

    beforeEach(function () {
      site = server.testData.create.feature('5ca78e2160780601008f69e6');
      site.visibility = 1;
      server.testData.storage.addFeature(site);

      server.put('/mock-server/features/' + site._id, (request) => {
        receivedSite = JSON.parse(request.requestBody);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedSite),
        ];
      });

      receivedSound = null;
      server.post('/mock-server/sounds', (request) => {
        receivedSound = JSON.parse(request.requestBody);
        receivedSound.sound._id = 'test';

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedSound),
        ];
      });

      server.delete('/mock-server/features/' + site._id, () => {
        deletedSite = true;

        return [204, {}, ''];
      });

      server.server.get(`/mock-server/maps/:id/meta`, () => {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify({}),
        ];
      });

      deletedSite = false;
      receivedSite = null;
    });

    describe('the breadcrumbs', function () {
      it('should contain all the browse links with the first site map', async function () {
        authenticateSession();
        await visit('/manage/sites/5ca78e2160780601008f69e6');

        expect(PageElements.breadcrumbs()).to.deep.equal([
          'Browse',
          'Maps',
          map.name,
          site.name,
          'Edit',
        ]);
        expect(PageElements.breadcrumbLinks()).to.deep.equal([
          '/browse',
          '/browse/maps',
          `/browse/sites?map=${map._id}`,
          `/browse/sites/${site._id}?map=${map._id}`,
        ]);
      });

      it('should contain all the browse links without the first site map if the site map list is empty', async function () {
        authenticateSession();
        site.maps = [];
        await visit('/manage/sites/5ca78e2160780601008f69e6');

        expect(PageElements.breadcrumbs()).to.deep.equal([
          'Browse',
          'Maps',
          site.name,
          'Edit',
        ]);
        expect(PageElements.breadcrumbLinks()).to.deep.equal([
          '/browse',
          '/browse/maps',
          `/browse/sites/${site._id}`,
        ]);
      });

      it('should contain all the browse links with another map if it is set in the query params', async function () {
        authenticateSession();
        await visit(
          `/manage/sites/5ca78e2160780601008f69e6?parentMap=${otherMap._id}`
        );

        expect(PageElements.breadcrumbs()).to.deep.equal([
          'Browse',
          'Maps',
          otherMap.name,
          site.name,
          'Edit',
        ]);
        expect(PageElements.breadcrumbLinks()).to.deep.equal([
          '/browse',
          '/browse/maps',
          `/browse/sites?map=${otherMap._id}`,
          `/browse/sites/${site._id}?map=${otherMap._id}`,
        ]);
      });
    });

    it('should redirect to login without an auth token it', async function () {
      invalidateSession();

      await visit('/manage/sites/5ca78e2160780601008f69e6');
      expect(currentURL()).to.equal(
        '/login?redirect=%2Fmanage%2Fsites%2F5ca78e2160780601008f69e6'
      );
    });

    it('should render the is public label', async function () {
      authenticateSession();

      await visit('/manage/sites/5ca78e2160780601008f69e6');
      expect(this.element.querySelector('.main-container > .error')).not.to
        .exist;

      expect(this.element.querySelector('.side-bar').textContent).to.contain(
        'visibility'
      );
      expect(
        this.element.querySelector('.container-group-visibility .value')
          .textContent
      ).to.contain('public');
    });

    it('should should be able to update the map list', async function () {
      authenticateSession();

      await visit('/manage/sites/5ca78e2160780601008f69e6');

      const listElements = this.element.querySelectorAll(
        '.container-group-maps ol li'
      );
      expect(listElements.length).to.equal(1);

      await click('.container-group-maps .btn-edit');
      await click('.container-group-maps .btn-add-item');
      await click('.container-group-maps .btn-submit');

      await waitUntil(() => receivedSite);

      expect(receivedSite.feature.maps).to.deep.equal([
        '5ca89e37ef1f7e010007f54c',
        '2',
      ]);
    });

    it('should should be able to delete the site', async function () {
      this.timeout(10000);
      authenticateSession();
      await visit(`/manage/sites/${site._id}?parentMap=${map._id}`);

      await click('.side-bar .btn-open');
      await click('.side-bar .btn-delete');

      await waitFor('.modal.show');

      expect(
        this.element.querySelector('.modal.show .modal-header').textContent
      ).to.contain('delete site');
      expect(
        this.element.querySelector('.modal.show .modal-body').textContent
      ).to.contain(
        'Are you sure you want to delete `Nomadisch Grün - Local Urban Food`?'
      );

      await click('.modal.show .btn-danger');

      expect(deletedSite).to.eq(true);
      expect(currentURL()).to.equal(`/browse/sites?map=${map._id}`);
    });

    it('can change the name', async function () {
      authenticateSession();
      await visit(`/manage/sites/${site._id}`);
      await waitFor('.editor-is-ready', { timeout: 3000 });

      await click('.ce-header');
      await fillIn('.ce-header', 'new name');
      await triggerKeyEvent('.ce-header', 'keyup', 'Enter');

      await waitUntil(
        () =>
          !this.element.querySelector('.btn-publish').hasAttribute('disabled')
      );
      await click('.btn-publish');

      await waitUntil(() => receivedSite);
      expect(receivedSite.feature.name).to.equal('new name');
    });

    it('can change the description', async function () {
      authenticateSession();
      this.timeout(15000);
      await visit(`/manage/sites/${site._id}`);
      await waitFor('.editor-is-ready', { timeout: 3000 });

      await fillIn('.ce-paragraph', 'new description');
      await waitUntil(
        () =>
          !this.element.querySelector('.btn-publish').hasAttribute('disabled'),
        { timeout: 3000 }
      );
      await click('.btn-publish');

      await waitUntil(() => receivedSite);

      expect(receivedSite.feature.description.blocks).to.deep.equal([
        { type: 'header', data: { text: site.name, level: 1 } },
        { type: 'paragraph', data: { text: 'new description' } },
      ]);
    });

    it('should be able to unpublish the site', async function () {
      authenticateSession();

      await visit('/manage/sites/5ca78e2160780601008f69e6');
      expect(this.element.querySelector('.main-container > .error')).not.to
        .exist;

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      await click(
        this.element.querySelector(
          '.side-bar .container-group-visibility .btn-edit'
        )
      );

      const select = this.element.querySelector(
        '.container-group-visibility select'
      );

      select.value = '0';
      await triggerEvent(select, 'change');

      await click(
        this.element.querySelector(
          '.side-bar .container-group-visibility .btn-submit'
        )
      );

      await waitUntil(() => receivedSite != null, { timeout: 3000 });

      expect(receivedSite.feature.visibility).to.equal(0);
    });

    it('should add a new sound', async function () {
      this.timeout(10000);
      authenticateSession();

      await visit('/manage/sites/5ca78e2160780601008f69e6');
      await waitFor('.sound-list-input');

      const blob = server.testData.create.mp3Blob();
      await triggerEvent('.sound-list-input', 'change', { files: [blob] });

      await waitUntil(() => receivedSound != null, { timeout: 3000 });
      await waitUntil(() => receivedSite != null, { timeout: 3000 });

      expect(receivedSite.feature.sounds).to.deep.equal(['test']);

      expect(receivedSound.sound).to.deep.equal({
        name: 'coolsound.mp3',
        sound:
          'data:audio/mpeg;base64,DcOBwrHDlcK0woHCicOJwoXCucKRw6TCgcKJwrHDlcKVw4wBUSVQw4gAAAA8AAANBcKxwrDCgcOlwr3DlcOIwoHCscK9w5nClAFRQRTDhAAAADQAAA05wqXCncKhw5HCscK9w43ClcOJw4wBUU1NFAAAADwAAA0xwoXDmcKYw5TDoMK4w4jDpMK4w4TDgMOAAQVBJQwABQXDkAAADcKlwrXChcKdwpTCvcKpw4HClQ==',
        feature: '5ca78e2160780601008f69e6',
        _id: 'test',
      });
    });

    describe('With a site with a @system picture', function () {
      let picture;
      let site;

      beforeEach(function () {
        const pictureId = '00000000000000000000000001';
        picture = server.testData.create.picture(pictureId);
        picture.owner = '@system';

        site = server.testData.create.feature(pictureId);
        site.pictures = [pictureId];

        server.testData.storage.addPicture(picture);
        server.testData.storage.addFeature(site);
      });

      it('should not have the picture rotate button', async function () {
        authenticateSession();

        await visit('/manage/sites/00000000000000000000000001');
        expect(this.element.querySelector('.btn-rotate')).not.to.exist;
      });
    });

    describe('Editing the attributes', function () {
      let siteCustom;

      beforeEach(function () {
        siteCustom = server.testData.create.feature('5ca78e2160780601008f6000');
        siteCustom.visibility = 1;
        siteCustom['attributes'] = { 'new test group': { key: 'value' } };

        server.testData.storage.addFeature(siteCustom);

        server.put('/mock-server/features/' + siteCustom._id, (request) => {
          receivedSite = JSON.parse(request.requestBody);

          return [
            200,
            { 'Content-Type': 'application/json' },
            JSON.stringify(receivedSite),
          ];
        });

        receivedSite = null;
      });

      it('should be able to add a new custom attribute', async function () {
        this.timeout(5000);
        authenticateSession();
        await visit(`/manage/sites/${site._id}`);

        await click('.btn-new-anonymous-group');
        await fillIn('.input-group-name', 'new test group');

        await click('.btn-add-new-attribute');

        await fillIn('.input-attribute-name', 'key');
        await fillIn('.input-attribute-value', 'value');
        await click('.btn-save');

        await waitUntil(() => receivedSite != null, { timeout: 3000 });

        expect(receivedSite).to.deep.equal({
          feature: {
            name: 'Nomadisch Grün - Local Urban Food',
            description: 'some description',
            position: { type: 'Point', coordinates: [13.433576, 52.495781] },
            visibility: 1,
            attributes: {
              'new test group': { key: 'value' },
            },
            info: {
              createdOn: '2009-10-31T00:30:00.000Z',
              author: '5b8a59caef739394031a3f67',
              lastChangeOn: '2009-10-31T00:30:00.000Z',
              originalAuthor: '5b8a59caef739394031a3f67',
              changeIndex: 3,
            },
            maps: ['5ca89e37ef1f7e010007f54c'],
            pictures: ['5cc8dc1038e882010061545a'],
            icons: ['5ca7bfc0ecd8490100cab980'],
          },
        });
      });

      it('should be able to delete a custom attribute', async function () {
        this.timeout(5000);
        authenticateSession();
        await visit(`/manage/sites/${siteCustom._id}`);

        await click('.input-site-attributes .btn-edit');
        await click('.input-site-attributes .btn-delete');

        await click('.btn-save');
        await waitUntil(() => receivedSite != null, { timeout: 3000 });

        expect(receivedSite.feature.attributes).to.deep.equal({
          'new test group': {},
        });
      });

      it('should be able to delete a custom group', async function () {
        this.timeout(5000);
        authenticateSession();
        await visit(`/manage/sites/${siteCustom._id}`);

        await click('.input-site-attributes .btn-edit');
        await click('.input-site-attributes .btn-delete-group');

        await click('.btn-save');
        await waitUntil(() => receivedSite != null, { timeout: 3000 });

        expect(receivedSite.feature.attributes).to.deep.equal({});
      });
    });
  });
});
