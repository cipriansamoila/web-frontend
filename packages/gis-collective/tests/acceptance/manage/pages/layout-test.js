import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  doubleClick,
  click,
  waitUntil,
  triggerEvent,
  fillIn,
} from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';

describe('Acceptance | manage/pages/layout', function () {
  setupApplicationTest();

  let server;
  let page;
  let receivedPage;
  let receivedLayout;
  let layout;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    const user = server.testData.create.user('5b8a59caef739394031a3f67');
    server.testData.storage.addUser('me', user);

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultProfile(user._id);
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultArticle();
    layout = server.testData.storage.addDefaultLayout(
      '000000000000000000000001'
    );

    server.testData.storage.addDefaultLayout('000000000000000000000002');
    page = server.testData.storage.addDefaultPage();

    receivedPage = null;
    server.put(`/mock-server/pages/${page._id}`, (request) => {
      receivedPage = JSON.parse(request.requestBody);
      receivedPage.page._id = page._id;

      server.testData.storage.addPage(receivedPage.page);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedPage),
      ];
    });

    server.post(`/mock-server/pages`, (request) => {
      receivedPage = JSON.parse(request.requestBody);
      receivedPage.page._id = 'new-id';

      server.testData.storage.addPage(receivedPage.page);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedPage),
      ];
    });

    receivedLayout = null;
    server.put(`/mock-server/layouts/${layout._id}`, (request) => {
      receivedLayout = JSON.parse(request.requestBody);
      receivedLayout.layout._id = layout._id;

      server.testData.storage.addLayout(receivedLayout.layout);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedLayout),
      ];
    });

    server.post(`/mock-server/layouts`, (request) => {
      receivedLayout = JSON.parse(request.requestBody);
      receivedLayout.layout._id = 'new-id';

      server.testData.storage.addLayout(receivedLayout.layout);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedLayout),
      ];
    });
  });

  afterEach(function () {
    server.shutdown();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/pages/${page._id}/layout`);

    expect(currentURL()).to.equal(
      '/login?redirect=%2Fmanage%2Fpages%2F000000000000000000000001%2Flayout'
    );
  });

  it('can navigate to the page content page', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.btn-show-content');

    expect(currentURL()).to.equal(
      '/manage/pages/000000000000000000000001/content'
    );
  });

  it('shows the col editor on col click', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);
    const cols = this.element.querySelectorAll('.btn-col-options');

    await doubleClick(cols[1]);

    expect(this.element.querySelector('.offcanvas.show .input-col-options')).to
      .exist;
  });

  it('can change the selected col width', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);
    const cols = this.element.querySelectorAll('.btn-col-options');

    await click('.row');
    await doubleClick(cols[1]);

    this.element.querySelector('.mobile-width-select').value = '3';
    await triggerEvent('.mobile-width-select', 'change');

    await click('.btn-close');

    expect(this.element.querySelector('.col.col-3')).to.exist;
  });

  it('shows the row editor when the row settings button is pressed', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.btn-edit-row');

    expect(this.element.querySelector('.offcanvas.show .input-row-options')).to
      .exist;
  });

  it('can change the row options', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.btn-edit-row');

    this.element.querySelector('.mobile-vertical-gutters').value = '3';
    await triggerEvent('.mobile-vertical-gutters', 'change');

    expect(this.element.querySelector('.row')).to.have.attribute(
      'class',
      'row gy-3 row-selectable'
    );
  });

  it('shows the container editor when the container settings button is pressed', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.btn-config-container');

    expect(
      this.element.querySelector('.offcanvas.show .input-container-options')
    ).to.exist;
  });

  it('can change the container options', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.btn-config-container');

    expect(this.element.querySelector('.input-layout .container')).to.exist;
    expect(this.element.querySelector('.input-layout .container-fluid')).not.to
      .exist;

    this.element.querySelector('.width-select').value = 'fluid';
    await triggerEvent('.width-select', 'change');

    expect(this.element.querySelector('.input-layout .container')).not.to.exist;
    expect(this.element.querySelector('.input-layout .container-fluid')).to
      .exist;
  });

  it('can save a layout', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.btn-config-container');

    this.element.querySelector('.width-select').value = 'fluid';
    await triggerEvent('.width-select', 'change');
    await click('.btn-close');
    await click('.btn-save');

    await waitUntil(() => receivedLayout);

    expect(receivedPage).not.to.exist;
    expect(receivedLayout.layout).to.deep.equal({
      name: 'test',
      containers: [
        {
          rows: [
            {
              cols: [
                { type: 'type', data: {}, options: [] },
                { type: 'type', data: {}, options: [] },
              ],
              options: [],
            },
          ],
          options: ['container-fluid'],
        },
      ],
      _id: '000000000000000000000001',
    });
  });

  it('can create a copy of the layout', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    await click('.btn-config-container');

    this.element.querySelector('.width-select').value = 'fluid';
    await triggerEvent('.width-select', 'change');
    await click('.btn-close');
    await click('.btn-copy-layout');

    await waitUntil(() => receivedLayout);
    await waitUntil(() => receivedPage);

    expect(currentURL()).to.equal('/manage/pages/new-id/layout');

    expect(
      this.element.querySelector('.layout-name').textContent.trim()
    ).to.equal('test - copy');

    expect(receivedPage.page._id).to.equal('new-id');
    expect(receivedPage.page.layout).to.equal('new-id');
    expect(receivedPage.page.name).to.equal('test - copy');
    expect(receivedPage.page.slug).to.equal('page--test-copy');
    expect(receivedPage.page.visibility).to.deep.equal({
      isPublic: false,
      isDefault: false,
      team: '5ca88c99ecd8490100caba50',
    });
    expect(receivedLayout.layout).to.deep.equal({
      name: 'test - copy',
      containers: [
        {
          rows: [
            {
              cols: [
                { type: 'type', data: {}, options: [] },
                { type: 'type', data: {}, options: [] },
              ],
              options: [],
            },
          ],
          options: ['container-fluid'],
        },
      ],
      _id: 'new-id',
    });
  });

  it('can change the layout settings', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/layout`);

    expect(
      this.element.querySelector('.layout-name').textContent.trim()
    ).to.equal('test');

    await click('.btn-config-layout');

    await fillIn('.type-name', 'new name');

    await click('.btn-close');

    expect(
      this.element.querySelector('.layout-name').textContent.trim()
    ).to.equal('new name');

    await click('.btn-save');

    await waitUntil(() => receivedLayout);

    expect(receivedPage).not.to.exist;
    expect(receivedLayout.layout).to.deep.equal({
      name: 'new name',
      containers: [
        {
          rows: [
            {
              cols: [
                { type: 'type', data: {}, options: [] },
                { type: 'type', data: {}, options: [] },
              ],
              options: [],
            },
          ],
          options: [],
        },
      ],
      _id: '000000000000000000000001',
    });
  });

  describe('when there is a page with two rows', function () {
    beforeEach(async function () {
      page.cols.push({
        container: 0,
        col: 0,
        row: 1,
        type: 'article',
        data: { id: '2' },
      });

      page.cols.push({
        container: 0,
        col: 1,
        row: 1,
        type: 'picture',
        data: {
          id: '2',
        },
      });

      layout.containers[0].rows.push({
        options: [],
        cols: [
          {
            type: 'type',
            data: {},
            options: [],
          },
          {
            type: 'type',
            data: {},
            options: [],
          },
        ],
      });

      authenticateSession();
      await visit(`/manage/pages/${page._id}/layout`);
    });

    it('pushes the rows on the page when a row on the layout is added', async function () {
      await click('.btn-add-row-0');
      await click('.row-selectable');
      await click('.btn-save');

      expect(receivedLayout.layout.containers[0].rows).to.have.length(3);
      expect(receivedPage.page.cols[0].row).to.equal(1);
      expect(receivedPage.page.cols[1].row).to.equal(1);
    });

    it('removes the cols on the page when a col on the layout is deleted', async function () {
      await click('.row-selectable');
      await click('.btn-delete-col');
      await click('.btn-save');

      expect(receivedLayout.layout.containers).to.have.length(1);
      expect(receivedPage.page.cols).to.have.length(3);
      expect(receivedPage.page.cols[0]).to.deep.contain({
        container: 0,
        col: 0,
        row: 0,
        type: 'article',
        data: { id: '5ca78e2160780601008f69e6' },
      });
    });

    it('removes the rows on the page when a row on the layout is deleted', async function () {
      await click('.row-selectable');
      await click('.btn-delete-col');
      await click('.btn-delete-col');
      await click('.btn-save');

      expect(receivedLayout.layout.containers).to.have.length(1);
      expect(receivedPage.page.cols).to.have.length(2);
      expect(receivedPage.page.cols[0]).to.deep.contain({
        container: 0,
        col: 0,
        row: 0,
        type: 'article',
        data: { id: '2' },
      });
      expect(receivedPage.page.cols[1]).to.deep.contain({
        container: 0,
        col: 1,
        row: 0,
        type: 'picture',
        data: { id: '2' },
      });
    });

    it('pushes the containers on the page when a container on the layout is added', async function () {
      await click('.btn-add-container-0');
      await click('.btn-save');

      expect(receivedLayout.layout.containers).to.have.length(2);
      expect(receivedLayout.layout.containers[0].rows).to.have.length(1);
      expect(receivedPage.page.cols[0].container).to.equal(1);
      expect(receivedPage.page.cols[1].container).to.equal(1);
    });

    it('decrements the container index on the page when a container on the layout is deleted', async function () {
      await click('.btn-remove-container-0');
      await click('.btn-save');

      expect(receivedLayout.layout.containers).to.have.length(0);
      expect(receivedPage.page.cols).to.have.length(0);
    });

    it('removes the containers on the page when a container on the layout is deleted', async function () {
      await click('.btn-add-container-0');
      await click('.btn-save');
      await click('.btn-remove-container-0');
      await click('.btn-save');

      expect(receivedLayout.layout.containers).to.have.length(1);
      expect(receivedLayout.layout.containers[0].rows).to.have.length(2);
      expect(receivedPage.page.cols[0].container).to.equal(0);
      expect(receivedPage.page.cols[1].container).to.equal(0);
    });
  });
});
