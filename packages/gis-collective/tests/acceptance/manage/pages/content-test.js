import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  triggerEvent,
  click,
  fillIn,
} from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';
import waitUntil from '@ember/test-helpers/wait-until';
import typeIn from '@ember/test-helpers/dom/type-in';

describe('Acceptance | manage/pages/content', function () {
  setupApplicationTest();

  let server;
  let page;
  let receivedPage;
  let receivedLayout;
  let layout;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    const user = server.testData.create.user('5b8a59caef739394031a3f67');
    server.testData.storage.addUser('me', user);

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultProfile(user._id);
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultArticle();
    layout = server.testData.storage.addDefaultLayout(
      '000000000000000000000001'
    );
    server.testData.storage.addDefaultLayout('000000000000000000000002');
    page = server.testData.storage.addDefaultPage();

    receivedPage = null;
    server.put(`/mock-server/pages/${page._id}`, (request) => {
      receivedPage = JSON.parse(request.requestBody);
      receivedPage.page._id = page._id;

      server.testData.storage.addPage(receivedPage.page);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedPage),
      ];
    });

    server.post(`/mock-server/pages`, (request) => {
      receivedPage = JSON.parse(request.requestBody);
      receivedPage.page._id = 'new-id';

      server.testData.storage.addPage(receivedPage.page);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedPage),
      ];
    });

    server.post(`/mock-server/layouts`, (request) => {
      receivedLayout = JSON.parse(request.requestBody);
      receivedLayout.layout._id = 'new-id';

      server.testData.storage.addLayout(receivedLayout.layout);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedLayout),
      ];
    });
  });

  afterEach(function () {
    server.shutdown();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/pages/${page._id}/content`);

    expect(currentURL()).to.equal(
      '/login?redirect=%2Fmanage%2Fpages%2F000000000000000000000001%2Fcontent'
    );
  });

  it('can navigate to the page layout page', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    expect(
      this.element.querySelector('.page-name').textContent.trim()
    ).to.equal('test');

    await click('.btn-show-layout');

    expect(currentURL()).to.equal(
      '/manage/pages/000000000000000000000001/layout'
    );
  });

  it('shows the published visibility for a public page', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    expect(
      this.element.querySelector('.page-visibility').textContent.trim()
    ).to.equal('public');
  });

  it('shows a link to the layout editor when there are no containers', async function () {
    authenticateSession();

    layout.containers = [];

    await visit(`/manage/pages/${page._id}/content`);

    expect(
      this.element.querySelector('.empty-layout p').textContent.trim()
    ).to.equal(
      'The layout has no containers. You need to edit the layout before editing the content.'
    );

    await click('.empty-layout .btn-show-layout');

    expect(currentURL()).to.equal(
      '/manage/pages/000000000000000000000001/layout'
    );
  });

  it('shows the private visibility for a private page', async function () {
    page.visibility.isPublic = false;
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    expect(
      this.element.querySelector('.page-visibility').textContent.trim()
    ).to.equal('private');
  });

  it('shows the editor on click', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.col');

    expect(this.element.querySelector('.page-col-sheet.offcanvas.show')).to
      .exist;

    expect(this.element.querySelector('.page-col-id').value).to.equal(
      '5ca78e2160780601008f69e6'
    );
  });

  it('shows a message inside the empty cols', async function () {
    authenticateSession();
    page.cols = [];
    await visit(`/manage/pages/${page._id}/content`);

    expect(
      this.element.querySelector('.page-col.empty').textContent.trim()
    ).to.equal('This column is empty. Click to select a component.');
  });

  it('allows changing a column value', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.col');

    this.element.querySelector('.page-col-id').value = 'about';
    await triggerEvent('.page-col-id', 'change');

    await click('.btn-set');
    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.cols).to.deep.equal([
      {
        container: 0,
        col: 0,
        row: 0,
        type: 'article',
        data: { id: 'about', model: 'article' },
      },
      {
        container: 0,
        col: 1,
        row: 0,
        type: 'picture',
        data: { id: '5cc8dc1038e882010061545a' },
      },
    ]);
  });

  it('adds a new column when is not set', async function () {
    authenticateSession();
    page.cols = [];
    await visit(`/manage/pages/${page._id}/content`);

    await click('.col');
    await click('.btn-article');

    this.element.querySelector('.page-col-id').value = 'about';
    await triggerEvent('.page-col-id', 'change');

    await click('.btn-set');
    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.cols).to.deep.equal([
      {
        container: 0,
        col: 0,
        row: 0,
        type: 'article',
        data: { id: 'about', model: 'article' },
      },
    ]);
  });

  it('allows changing the column type', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.col');
    await click('.btn-banner');

    await typeIn('.text-heading', 'title');
    await typeIn('.text-subheading', 'description');

    await click('.btn-set');
    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.cols).to.deep.equal([
      {
        container: 0,
        col: 0,
        row: 0,
        type: 'banner',
        data: {
          heading: 'title',
          subHeading: 'description',
          description: { blocks: [] },
        },
      },
      {
        container: 0,
        col: 1,
        row: 0,
        type: 'picture',
        data: { id: '5cc8dc1038e882010061545a' },
      },
    ]);
  });

  it('allows changing the page name', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.btn-config-page');
    expect(this.element.querySelector('.page-name').value).to.equal('test');

    await fillIn('.page-name', 'new name');

    await click('.btn-close');
    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page).to.deep.equal({
      info: {
        createdOn: '2020-12-19T14:09:09.000Z',
        lastChangeOn: '2020-12-19T14:09:09.000Z',
        author: '5b870669796da25424540deb',
        originalAuthor: '5b870669796da25424540deb',
      },
      name: 'new name',
      slug: 'page--test',
      containers: [],
      cols: [
        {
          container: 0,
          col: 0,
          row: 0,
          type: 'article',
          data: { id: '5ca78e2160780601008f69e6' },
        },
        {
          container: 0,
          col: 1,
          row: 0,
          type: 'picture',
          data: { id: '5cc8dc1038e882010061545a' },
        },
      ],
      visibility: {
        isPublic: true,
        isDefault: false,
        team: '5ca88c99ecd8490100caba50',
      },
      layout: '000000000000000000000001',
      _id: '000000000000000000000001',
    });
  });

  it('allows changing the page slug', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.btn-config-page');
    expect(this.element.querySelector('.page-path').value).to.equal(
      '/page/test'
    );
    await fillIn('.page-path', 'a//b//c/d');

    await click('.btn-close');
    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.slug).to.equal('a--b--c--d');
  });

  it('allows changing the layout', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.btn-config-page');
    expect(this.element.querySelector('.page-layout').value).to.equal(
      '000000000000000000000001'
    );

    this.element.querySelector('.page-layout').value =
      '000000000000000000000002';
    await triggerEvent('.page-layout', 'change');

    await click('.btn-close');
    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.layout).to.equal('000000000000000000000002');
  });

  it('allows changing the visibility', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.btn-config-page');

    expect(
      this.element.querySelector('.chk-visibility input').checked
    ).to.equal(true);

    await click('.chk-visibility');
    await click('.btn-close');
    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.visibility).to.deep.equal({
      isPublic: true,
      isDefault: false,
      team: '5ca88c99ecd8490100caba50',
    });
  });

  it('allows setting the container background', async function () {
    layout.containers.push({
      options: [],
      rows: [
        {
          cols: [{}],
        },
      ],
    });

    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    let config = this.element.querySelectorAll('.btn-config-container');
    await click(config[1]);

    await click('.btn-color-info');
    await click('.btn-close');
    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.containers).to.deep.equal([
      {},
      {
        backgroundColor: 'info',
      },
    ]);
  });

  it('allows changing the container background', async function () {
    page.containers = [{ backgroundColor: 'gray' }];
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.btn-config-container');

    expect(this.element.querySelector('.bg-gray > .container')).to.exist;
    expect(
      this.element
        .querySelector('.input-container-background-options .selected-color')
        .textContent.trim()
    ).equal('gray');

    await click('.btn-color-red');
    await click('.btn-close');
    await click('.btn-save');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.containers).to.deep.equal([
      {
        backgroundColor: 'red',
      },
    ]);
  });

  it('can create a copy of the page', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}/content`);

    await click('.btn-copy-page');

    await waitUntil(() => receivedPage);
    await waitUntil(() => receivedLayout);

    expect(currentURL()).to.equal('/manage/pages/new-id/content');

    expect(
      this.element.querySelector('.page-name').textContent.trim()
    ).to.equal('test - copy');

    expect(receivedPage.page._id).to.equal('new-id');
    expect(receivedPage.page.layout).to.equal('new-id');
    expect(receivedPage.page.name).to.equal('test - copy');
    expect(receivedPage.page.slug).to.equal('page--test-copy');
    expect(receivedPage.page.visibility).to.deep.equal({
      isPublic: false,
      isDefault: false,
      team: '5ca88c99ecd8490100caba50',
    });
    expect(receivedLayout.layout).to.deep.equal({
      name: 'test - copy',
      containers: [
        {
          rows: [
            {
              cols: [
                { type: 'type', data: {}, options: [] },
                { type: 'type', data: {}, options: [] },
              ],
              options: [],
            },
          ],
          options: [],
        },
      ],
      _id: 'new-id',
    });
  });
});
