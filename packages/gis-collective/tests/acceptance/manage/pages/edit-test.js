import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  fillIn,
  triggerEvent,
  waitUntil,
} from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';
import PageElements from '../../../helpers/page-elements';

describe('Acceptance | manage/pages/edit', function () {
  setupApplicationTest();

  let server;
  let page;
  let receivedPage;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    const user = server.testData.create.user('5b8a59caef739394031a3f67');
    server.testData.storage.addUser('me', user);

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultProfile(user._id);
    server.testData.storage.addDefaultLayout('000000000000000000000001');
    server.testData.storage.addDefaultLayout('000000000000000000000002');
    page = server.testData.storage.addDefaultPage();

    receivedPage = null;
    server.put(`/mock-server/pages/${page._id}`, (request) => {
      receivedPage = JSON.parse(request.requestBody);
      receivedPage.page._id = page._id;

      server.testData.storage.addPage(receivedPage.page);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedPage),
      ];
    });
  });

  afterEach(function () {
    server.shutdown();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/pages/${page._id}`);

    expect(currentURL()).to.equal(
      '/login?redirect=%2Fmanage%2Fpages%2F000000000000000000000001'
    );
  });

  it('renders the form for a page', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}`);

    expect(PageElements.containerGroupTitles()).to.deep.equal([
      'team',
      'original author',
      'Name',
      'Slug',
      'Layout',
      'Content',
    ]);
  });

  it('can update the name', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}`);

    await click('.container-group-name .btn-edit');
    await fillIn('.container-group-name input', 'new name');
    await click('.btn-submit');

    await waitUntil(() => receivedPage);

    expect(receivedPage).to.deep.equal({
      page: {
        name: 'new name',
        slug: 'page--test',
        cols: page.cols,
        containers: [],
        visibility: {
          isPublic: true,
          isDefault: false,
          team: '5ca88c99ecd8490100caba50',
        },
        info: {
          createdOn: '2020-12-19T14:09:09.000Z',
          lastChangeOn: '2020-12-19T14:09:09.000Z',
          author: '5b870669796da25424540deb',
          originalAuthor: '5b870669796da25424540deb',
        },
        layout: '000000000000000000000001',
        _id: '000000000000000000000001',
      },
    });
  });

  it('can update the slug', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}`);

    await click('.container-group-slug .btn-edit');
    await fillIn('.container-group-slug input', 'new name');
    await click('.btn-submit');

    await waitUntil(() => receivedPage);

    expect(receivedPage).to.deep.equal({
      page: {
        name: 'test',
        slug: 'new name',
        cols: page.cols,
        containers: [],
        visibility: {
          isPublic: true,
          isDefault: false,
          team: '5ca88c99ecd8490100caba50',
        },
        info: {
          createdOn: '2020-12-19T14:09:09.000Z',
          lastChangeOn: '2020-12-19T14:09:09.000Z',
          author: '5b870669796da25424540deb',
          originalAuthor: '5b870669796da25424540deb',
        },
        layout: '000000000000000000000001',
        _id: '000000000000000000000001',
      },
    });
  });

  it('can update the layout', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}`);

    await click('.container-group-layout .btn-edit');

    this.element.querySelector('select').value = '000000000000000000000002';
    await triggerEvent('select', 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedPage);

    expect(receivedPage).to.deep.equal({
      page: {
        name: 'test',
        slug: 'page--test',
        cols: page.cols,
        containers: [],
        visibility: {
          isPublic: true,
          isDefault: false,
          team: '5ca88c99ecd8490100caba50',
        },
        info: {
          createdOn: '2020-12-19T14:09:09.000Z',
          lastChangeOn: '2020-12-19T14:09:09.000Z',
          author: '5b870669796da25424540deb',
          originalAuthor: '5b870669796da25424540deb',
        },
        layout: '000000000000000000000002',
        _id: '000000000000000000000001',
      },
    });
  });

  it('can update the Content', async function () {
    authenticateSession();
    await visit(`/manage/pages/${page._id}`);

    await click('.container-group-content .btn-edit');
    this.element.querySelector('.page-col-id').value = 'about';
    await triggerEvent('.page-col-id', 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedPage);

    expect(receivedPage.page.cols).to.deep.equal([
      {
        container: 0,
        col: 0,
        row: 0,
        type: 'article',
        data: { id: 'about', model: 'article' },
      },
      {
        container: 0,
        col: 1,
        row: 0,
        type: 'picture',
        data: { id: '5cc8dc1038e882010061545a' },
      },
    ]);
  });
});
