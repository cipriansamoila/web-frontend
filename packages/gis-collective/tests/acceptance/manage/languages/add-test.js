import {
  describe,
  it,
  beforeEach,
  afterEach
} from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  fillIn,
  triggerEvent,
  waitUntil,
  click
} from '@ember/test-helpers';
import {
  authenticateSession,
  invalidateSession
} from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';

describe('Acceptance | manage/languages/add', function() {
  setupApplicationTest();
  let server;
  let receivedTranslation;
  let translation;

  beforeEach(function() {
    server = new TestServer();

    translation = server.testData.create.translation();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);

    server.post('/mock-server/translations', (request) => {

      receivedTranslation = JSON.parse(request.requestBody);
      receivedTranslation.translation._id = translation._id;

      server.testData.storage.addTranslation(receivedTranslation.translation);

      return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedTranslation)];
    });

    receivedTranslation = null;
  });

  afterEach(function() {
    server.shutdown();
    Modal.clear();
  });

  it("should redirect to login without an auth token it", async function() {
    invalidateSession();

    await visit(`/manage/languages/add`);
    expect(currentURL()).to.contain('/login');
  });

  it("show the form when the user is authenticated", async function() {
    authenticateSession();

    await visit(`/manage/languages/add`);

    expect(currentURL()).to.equal(`/manage/languages/add`);
    expect(this.element.querySelector(".error")).to.not.exist;

    expect(this.element.querySelector(".row-name input[type='text']")).to.exist;
    expect(this.element.querySelector(".row-name h5").textContent.trim()).to.equal("name");

    expect(this.element.querySelector(".row-locale input[type='text']")).to.exist;
    expect(this.element.querySelector(".row-locale h5").textContent.trim()).to.equal("locale");

    expect(this.element.querySelector(".row-file input[type='file']")).to.exist;
    expect(this.element.querySelector(".row-file h5").textContent.trim()).to.equal("file");

    expect(this.element.querySelector(".row-submit button")).to.exist;
    expect(this.element.querySelector(".row-submit button").textContent.trim()).to.equal("add");

    expect(this.element.querySelector('.row-submit button')).have.class('disabled');
    expect(this.element.querySelector('.row-submit button')).have.attribute('disabled', "");
  });

  it("show be able to create a new language", async function() {
    authenticateSession();

    await visit(`/manage/languages/add`);

    expect(currentURL()).to.equal(`/manage/languages/add`);
    expect(this.element.querySelector(".error")).to.not.exist;

    await fillIn(".row-name input", "test name");
    await fillIn(".row-locale input", "test locale");

    let blob = new Blob(['foo', 'bar'], {type: 'text/plain'});
    blob.name = 'foobar.txt';

    await triggerEvent(
      ".row-file input[type='file']",
      'change',
      { files: [ blob ]}
    );

    await waitUntil(() => !this.element.querySelector('.row-submit button').classList.contains("disabled"));

    expect(this.element.querySelector('.row-submit button')).not.have.class('disabled');
    expect(this.element.querySelector('.row-submit button')).not.have.attribute('disabled', "");

    await click(this.element.querySelector('.row-submit button'));
    expect(receivedTranslation).to.deep.equal({ "translation": {
      _id: translation._id,
      name: 'test name',
      locale: 'test locale',
      file: 'data:text/plain;base64,Zm9vYmFy'
    }});
  });

  it("should remove the pending model on route change", async function() {
    authenticateSession();

    await visit(`/manage/languages/add`);

    await click(".dropdown-manage");
    await click(".dropdown-item-languages");

    expect(currentURL()).to.equal(`/manage/languages`);
    const table = this.element.querySelector("table tbody tr");

    expect(table).not.to.exist;
  });
});