import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  fillIn,
  triggerEvent,
  waitUntil,
  click,
} from '@ember/test-helpers';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';

describe('Acceptance | manage/languages/edit', function () {
  setupApplicationTest();
  let server;
  let receivedTranslation;
  let translation;

  beforeEach(function () {
    server = new TestServer();

    translation = server.testData.create.translation();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addTranslation(translation);

    server.put('/mock-server/translations/' + translation._id, (request) => {
      receivedTranslation = JSON.parse(request.requestBody);
      receivedTranslation.translation._id = translation._id;

      server.testData.storage.addTranslation(receivedTranslation.translation);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedTranslation),
      ];
    });

    receivedTranslation = null;
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/languages/` + translation._id);
    expect(currentURL()).to.contain('/login');
  });

  it('should navigate to home page if the user is not an admin', async function () {
    server.testData.storage.addDefaultUser(false);
    authenticateSession();

    await visit(`/manage/languages/` + translation._id);
    expect(currentURL()).to.equal('/browse/maps/_/map-view');
  });

  it('show the form when the user is authenticated', async function () {
    authenticateSession();

    await visit(`/manage/languages/` + translation._id);

    expect(currentURL()).to.equal(`/manage/languages/` + translation._id);
    expect(this.element.querySelector('.error')).to.not.exist;

    expect(this.element.querySelector(".row-name input[type='text']")).to.exist;
    expect(
      this.element.querySelector(".row-name input[type='text']").value
    ).to.equal(translation.name);
    expect(
      this.element.querySelector('.row-name h5').textContent.trim()
    ).to.equal('name');

    expect(this.element.querySelector(".row-locale input[type='text']")).to
      .exist;
    expect(
      this.element.querySelector(".row-locale input[type='text']").value
    ).to.equal(translation.locale);
    expect(
      this.element.querySelector('.row-locale h5').textContent.trim()
    ).to.equal('locale');

    expect(this.element.querySelector(".row-file input[type='file']")).to.exist;
    expect(
      this.element.querySelector('.row-file h5').textContent.trim()
    ).to.equal('file');

    expect(this.element.querySelector('.row-submit button')).to.exist;
    expect(
      this.element.querySelector('.row-submit button').textContent.trim()
    ).to.equal('save');

    expect(this.element.querySelector('.row-submit button')).have.class(
      'disabled'
    );
    expect(this.element.querySelector('.row-submit button')).have.attribute(
      'disabled',
      ''
    );
  });

  it('should update the name', async function () {
    authenticateSession();

    await visit(`/manage/languages/` + translation._id);

    expect(currentURL()).to.equal(`/manage/languages/` + translation._id);

    await fillIn(".row-name input[type='text']", 'new name');

    expect(this.element.querySelector('.row-submit button')).not.have.class(
      'disabled'
    );
    expect(this.element.querySelector('.row-submit button')).not.have.attribute(
      'disabled',
      ''
    );

    await click('.row-submit button');
    await waitUntil(() => receivedTranslation);

    expect(receivedTranslation).to.deep.equal({
      translation: {
        name: 'new name',
        locale: 'ro-ro',
        file: 'https://new.opengreenmap.org/api-v1/translations/5ca7bfc0ecd8490100cab980/file',
        _id: '5ca7bfc0ecd8490100cab980',
      },
    });
  });

  it('should update the locale', async function () {
    authenticateSession();

    await visit(`/manage/languages/` + translation._id);

    expect(currentURL()).to.equal(`/manage/languages/` + translation._id);

    await fillIn(".row-locale input[type='text']", 'new locale');

    expect(this.element.querySelector('.row-submit button')).not.have.class(
      'disabled'
    );
    expect(this.element.querySelector('.row-submit button')).not.have.attribute(
      'disabled',
      ''
    );

    await click('.row-submit button');
    await waitUntil(() => receivedTranslation);

    expect(receivedTranslation).to.deep.equal({
      translation: {
        name: 'Romana',
        locale: 'new locale',
        file: 'https://new.opengreenmap.org/api-v1/translations/5ca7bfc0ecd8490100cab980/file',
        _id: '5ca7bfc0ecd8490100cab980',
      },
    });
  });

  it('should update the file', async function () {
    authenticateSession();

    await visit(`/manage/languages/` + translation._id);

    expect(currentURL()).to.equal(`/manage/languages/` + translation._id);

    let blob = new Blob(['foo', 'bar'], { type: 'text/plain' });
    blob.name = 'foobar.txt';

    await triggerEvent(".row-file input[type='file']", 'change', {
      files: [blob],
    });

    await waitUntil(
      () => !this.element.querySelector('.row-submit button.disabled')
    );

    expect(this.element.querySelector('.row-submit button')).not.have.class(
      'disabled'
    );
    expect(this.element.querySelector('.row-submit button')).not.have.attribute(
      'disabled',
      ''
    );

    await click('.row-submit button');
    await waitUntil(() => receivedTranslation);

    expect(receivedTranslation).to.deep.equal({
      translation: {
        name: 'Romana',
        locale: 'ro-ro',
        file: 'data:text/plain;base64,Zm9vYmFy',
        _id: '5ca7bfc0ecd8490100cab980',
      },
    });
  });

  it('can add a custom translation', async function () {
    authenticateSession();

    await visit(`/manage/languages/${translation._id}`);

    expect(currentURL()).to.equal(`/manage/languages/${translation._id}`);

    await click('.btn-add-item');
    await fillIn('.input-key', 'abc');
    await fillIn('.input-value', 'def');

    await click('.row-submit button');
    await waitUntil(() => receivedTranslation);

    expect(receivedTranslation.translation).to.deep.equal({
      name: 'Romana',
      locale: 'ro-ro',
      file: 'https://new.opengreenmap.org/api-v1/translations/5ca7bfc0ecd8490100cab980/file',
      customTranslations: { abc: 'def' },
      _id: '5ca7bfc0ecd8490100cab980',
    });
  });
});
