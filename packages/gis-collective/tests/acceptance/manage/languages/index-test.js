import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import { visit, currentURL, click } from '@ember/test-helpers';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';

describe('Acceptance | manage/languages', function () {
  setupApplicationTest();

  let server;
  let deletedTranslation;
  let translation;

  beforeEach(function () {
    server = new TestServer();

    translation = server.testData.create.translation();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser(true);
    server.testData.storage.addTranslation(translation);

    server.delete_('/mock-server/translations/' + translation._id, () => {
      deletedTranslation = true;
      return [204];
    });

    deletedTranslation = null;
  });

  afterEach(function () {
    server.shutdown();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/languages`);
    expect(currentURL()).to.contain('/login');
  });

  it('should navigate to home page if the user is not an admin', async function () {
    server.testData.storage.addDefaultUser(false);
    authenticateSession();

    await visit(`/manage/languages`);
    expect(currentURL()).to.equal('/browse/maps/_/map-view');

    const table = this.element.querySelector('table');

    expect(table).not.to.exist;
  });

  it('should list the configured languages', async function () {
    authenticateSession();

    await visit(`/manage/languages`);
    expect(currentURL()).to.contain('/manage/languages');

    const table = this.element.querySelector('table');
    expect(table.querySelector('.item-name').textContent.trim()).to.equal(
      'Romana'
    );
    expect(table.querySelector('.item-locale').textContent.trim()).to.equal(
      'ro-ro'
    );
    expect(
      table.querySelector('.dropdown-menu .dropdown-edit').textContent.trim()
    ).to.equal('Edit');
    expect(
      table
        .querySelector('.dropdown-menu .dropdown-download')
        .textContent.trim()
    ).to.equal('Download');
    expect(
      table
        .querySelector('.dropdown-menu .dropdown-download')
        .attributes.getNamedItem('href').value
    ).to.equal(translation.file);
    expect(
      table.querySelector('.dropdown-menu .btn-delete').textContent.trim()
    ).to.equal('Delete');
  });

  it('should navigate to language edit page on click on the name', async function () {
    authenticateSession();

    await visit(`/manage/languages`);
    expect(currentURL()).to.contain('/manage/languages');

    await click(this.element.querySelector('table .item-name a'));

    expect(currentURL()).to.contain('/manage/languages/' + translation._id);
  });

  it('should navigate to language edit page on click on the edit button', async function () {
    authenticateSession();

    await visit(`/manage/languages`);
    expect(currentURL()).to.contain('/manage/languages');

    await click(this.element.querySelector('table .dropdown-toggle'));
    await click(this.element.querySelector('table .dropdown-edit'));

    expect(currentURL()).to.contain('/manage/languages/' + translation._id);
  });

  it('should delete a language', async function () {
    this.timeout(4000);
    authenticateSession();

    await visit(`/manage/languages`);
    expect(currentURL()).to.contain('/manage/languages');

    const table = this.element.querySelector('table');

    await click(table.querySelector('.dropdown-toggle'));
    await click(table.querySelector('.dropdown-menu .btn-delete'));

    await Modal.waitToDisplay();

    await click(this.element.querySelector('.modal-footer .btn-resolve'));

    await Modal.waitToHide();

    expect(deletedTranslation).to.equal(true);
    expect(this.element.querySelector('table tbody tr')).not.to.exist;
  });
});
