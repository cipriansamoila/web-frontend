import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  fillIn,
  triggerEvent,
  waitUntil,
} from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';
import PageElements from '../../../helpers/page-elements';

describe('Acceptance | manage/presentations/edit', function () {
  setupApplicationTest();

  let server;
  let presentation;
  let receivedPresentation;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    const user = server.testData.create.user('5b8a59caef739394031a3f67');
    server.testData.storage.addUser('me', user);

    server.testData.storage.addUser(user._id, user);
    server.testData.storage.addDefaultProfile(user._id);
    server.testData.storage.addDefaultLayout('000000000000000000000001');
    server.testData.storage.addDefaultLayout('000000000000000000000002');
    presentation = server.testData.storage.addDefaultPresentation();

    receivedPresentation = null;
    server.put(`/mock-server/presentations/${presentation._id}`, (request) => {
      receivedPresentation = JSON.parse(request.requestBody);
      receivedPresentation.presentation._id = presentation._id;

      server.testData.storage.addDefaultPresentation(
        receivedPresentation.presentation
      );

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedPresentation),
      ];
    });
  });

  afterEach(function () {
    server.shutdown();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit(`/manage/presentations/${presentation._id}`);

    expect(currentURL()).to.equal(
      '/login?redirect=%2Fmanage%2Fpresentations%2F000000000000000000000001'
    );
  });

  it('renders the form for a presentation', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}`);

    expect(PageElements.containerGroupTitles()).to.deep.equal([
      'team',
      'original author',
      'Name',
      'Slug',
      'Layout',
      'Content',
    ]);
  });

  it('can update the name', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}`);

    await click('.container-group-name .btn-edit');
    await fillIn('.container-group-name input', 'new name');
    await click('.btn-submit');

    await waitUntil(() => receivedPresentation);

    expect(receivedPresentation.presentation.name).to.deep.equal('new name');
  });

  it('can update the slug', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}`);

    await click('.container-group-slug .btn-edit');
    await fillIn('.container-group-slug input', 'new name');
    await click('.btn-submit');

    await waitUntil(() => receivedPresentation);

    expect(receivedPresentation.presentation.slug).to.deep.equal('new name');
  });

  it('can update the layout', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}`);

    await click('.container-group-layout .btn-edit');

    this.element.querySelector('select').value = '000000000000000000000002';
    await triggerEvent('select', 'change');

    await click('.btn-submit');

    await waitUntil(() => receivedPresentation);

    expect(receivedPresentation.presentation.layout).to.deep.equal(
      '000000000000000000000002'
    );
  });

  it('can update the Content', async function () {
    authenticateSession();
    await visit(`/manage/presentations/${presentation._id}`);

    await click('.container-group-content .btn-edit');

    await click('.btn-add-row');
    await click('.btn-submit');

    await waitUntil(() => receivedPresentation);

    expect(receivedPresentation.presentation.cols).to.deep.equal([
      {
        container: 0,
        col: 0,
        row: 0,
        type: 'article',
        data: { id: '5ca78e2160780601008f69e6' },
      },
      {
        container: 0,
        col: 1,
        row: 0,
        type: 'picture',
        data: { id: '5cc8dc1038e882010061545a' },
      },
      {
        container: 0,
        col: 0,
        row: 1,
        type: 'article',
        data: {},
      },
      {
        container: 0,
        col: 1,
        row: 1,
        type: 'picture',
        data: {},
      },
    ]);
  });
});
