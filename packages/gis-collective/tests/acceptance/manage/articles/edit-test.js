import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  waitUntil,
  click,
  fillIn,
  waitFor,
} from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';

describe('Acceptance | manage/articles/edit', function () {
  setupApplicationTest();
  let server;
  let article;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    article = server.testData.storage.addDefaultArticle();
  });

  afterEach(function () {
    server.shutdown();
  });

  it('redirects to the login page when the user is not authenticated', async function () {
    invalidateSession();
    await visit(`/manage/articles/${article._id}`);
    expect(currentURL()).to.equal(
      `/login?redirect=%2Fmanage%2Farticles%2F${article._id}`
    );
  });

  describe('when the user is authenticated', function () {
    let receivedArticle;

    beforeEach(async function () {
      server.testData.storage.addDefaultUser();
      server.testData.storage.addDefaultTeam();
      authenticateSession();

      await visit(`/manage/articles/${article._id}`);

      await waitUntil(() => !this.element.querySelector('.fa-spinner'));
      await waitUntil(() => this.element.querySelector('.editor-is-ready'), {
        timeout: 4000,
      });

      receivedArticle = null;
      server.put(`/mock-server/articles/${article._id}`, (request) => {
        receivedArticle = JSON.parse(request.requestBody);

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedArticle),
        ];
      });
    });

    it('can visit /manage/articles/:id', async function () {
      expect(this.element.querySelector('.btn-publish')).to.have.attribute(
        'disabled',
        ''
      );
      expect(this.element.querySelector('.article-editor')).to.exist;
      expect(
        this.element.querySelector('.ce-header').textContent.trim()
      ).to.equal('title');
      expect(
        this.element.querySelector('.ce-paragraph').textContent.trim()
      ).to.equal('some content');
    });

    it('can update the title and a paragraph', async function () {
      await click('.ce-header');
      await fillIn('.ce-header', ' --- new title');

      await click('.ce-paragraph');
      await fillIn('.ce-paragraph', ' --- new paragraph');

      await click('.ce-header');

      await waitUntil(
        () =>
          !this.element.querySelector('.btn-publish').hasAttribute('disabled'),
        { timeout: 3000 }
      );

      await click('.btn-publish');
      await waitUntil(() => receivedArticle, { timeout: 10000 });

      expect(receivedArticle.article.content.blocks).to.deep.equal([
        { type: 'header', data: { text: ' --- new title', level: 1 } },
        { type: 'paragraph', data: { text: ' --- new paragraph' } },
      ]);
    });

    it('can update the slug', async function () {
      await click('.container-group-slug .btn-edit');
      await fillIn('.container-group-slug input', 'new name');
      await click('.btn-submit');

      await waitUntil(() => receivedArticle);

      expect(receivedArticle.article.slug).to.equal('new name');
    });

    it('resets the value when the cancel button is pressed', async function () {
      await click('.ce-header');
      await fillIn('.ce-header', ' --- new title');

      await click('.ce-paragraph');
      await fillIn('.ce-paragraph', ' --- new paragraph');

      await click('.ce-header');

      await waitUntil(
        () =>
          !this.element.querySelector('.btn-publish').hasAttribute('disabled'),
        { timeout: 3000 }
      );

      await click('.btn-reset');

      await waitFor('.ce-header');

      expect(receivedArticle).to.equal(null);
      expect(
        this.element.querySelector('.ce-header').textContent.trim()
      ).to.equal('title');
      expect(
        this.element.querySelector('.ce-paragraph').textContent.trim()
      ).to.equal('some content');
    });

    it('should be able to unpublish the article', async function () {
      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      await click(
        this.element.querySelector(
          '.side-bar .container-group-is-public .btn-switch'
        )
      );

      await waitUntil(() => receivedArticle != null, { timeout: 3000 });

      expect(receivedArticle.article.visibility.isPublic).to.equal(false);
    });

    it('does not show the isDefault section', async function () {
      expect(this.element.querySelector('.container-group-is-default')).not.to
        .exist;
    });
  });
});
