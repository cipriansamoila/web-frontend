import {
  describe,
  it,
  beforeEach,
  afterEach
} from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  waitUntil,
  click,
  fillIn,
  waitFor
} from '@ember/test-helpers';
import {
  authenticateSession,
  invalidateSession
} from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';


describe('Acceptance | manage/icons/translate', function() {
  setupApplicationTest();
  let server;
  let receivedIcon;
  let iconSet;
  let icon;

  beforeEach(function () {
    server = new TestServer();
    iconSet = server.testData.create.iconSet();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addIconSet(iconSet);
  });

  afterEach(function () {
    server.shutdown();
  });

  describe("with the default icon", function() {
    beforeEach(function () {
      icon = server.testData.create.icon();
      server.testData.storage.addIcon(icon);
      server.testData.storage.addDefaultTranslation();

      server.put('/mock-server/icons/' + icon._id, (request) => {
        receivedIcon = JSON.parse(request.requestBody);
        receivedIcon.icon._id = icon._id;

        server.testData.storage.addIcon(receivedIcon.icon);

        return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedIcon)];
      });

      receivedIcon = null;
    });

    it("should redirect to login without an auth token it", async function() {
      invalidateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);
      expect(currentURL()).to.contain('/login');
    });

    it("should go to the icon edit page on edit button click", async function() {
      authenticateSession();
      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);

      await click(".btn-go-edit");

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
    });

    it("should go to 'icon edit' page on breadcrumb click", async function() {
      authenticateSession();
      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);

      const breadcrumbs = this.element.querySelectorAll(".breadcrumb a");

      await click(breadcrumbs[2]);
      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}`);
    });

    it("should go to the 'your icon sets' page on breadcrumb click", async function() {
      authenticateSession();
      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);

      const breadcrumbs = this.element.querySelectorAll(".breadcrumb a");
      await click(breadcrumbs[0]);

      expect(currentURL()).to.equal(`/manage/dashboards/5ca78e2160780601008f69e6`);
    });

    it("should show be able to change the article", async function() {
      this.timeout(6000);

      authenticateSession();
      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);

      await waitFor(".editor-is-ready", {timeout: 3000});
      await fillIn(".ce-header", "new name");
      await fillIn(".ce-paragraph", "new description");
      await waitUntil(() => !this.element.querySelector(".btn-publish").hasAttribute("disabled"), { timeout: 3000 } );
      await click(".btn-publish");

      await waitUntil(() => receivedIcon);

      expect(receivedIcon.icon.localName).to.equal('new name');
      expect(receivedIcon.icon.description.blocks).to.deep.equal([
        { type: 'header',    data: { text: 'new name', level: 1 } },
        { type: 'paragraph', data: { text: 'new description'    } }
      ]);
      expect(server.history).to.contain("PUT /mock-server/icons/5ca7bfc0ecd8490100cab980?locale=ro-ro");
    });

    it("should show be able to change the category", async function() {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);

      await click(".row-category .btn-edit");
      await fillIn(".row-category input", "new category");
      await click(".btn-submit");

      await waitUntil(() => receivedIcon);

      expect(receivedIcon.icon.category).to.equal('new category');
      expect(receivedIcon.icon.subcategory).to.equal('Green Economy');
      expect(server.history).to.contain("PUT /mock-server/icons/5ca7bfc0ecd8490100cab980?locale=ro-ro");
    });

    it("should show be able to change the subcategory", async function() {
      authenticateSession();

      await visit(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);

      expect(currentURL()).to.equal(`/manage/icons/edit/${iconSet._id}/${icon._id}/translate`);

      await click(".row-subcategory .btn-edit");
      await fillIn(".row-subcategory input", "new subcategory");
      await click(".btn-submit");

      await waitUntil(() => receivedIcon);

      expect(receivedIcon.icon.subcategory).to.equal('new subcategory');
      expect(receivedIcon.icon.category).to.equal('Sustainable Living');
      expect(server.history).to.contain("PUT /mock-server/icons/5ca7bfc0ecd8490100cab980?locale=ro-ro");
    });
  });
});
