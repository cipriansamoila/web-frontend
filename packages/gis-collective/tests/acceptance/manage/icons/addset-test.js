import {
  describe,
  it,
  beforeEach,
  afterEach
} from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  fillIn,
  triggerEvent,
  waitUntil,
  waitFor
} from '@ember/test-helpers';
import {
  authenticateSession,
  invalidateSession
} from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';
import PageElements from '../../../helpers/page-elements';

describe('Acceptance | manage/icons/add icon set', function () {
  setupApplicationTest();
  let server;
  let receivedIconSet;
  let iconSet;

  beforeEach(function () {
    server = new TestServer();
    iconSet = server.testData.create.iconSet();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();

    server.post('/mock-server/iconsets', (request) => {
      server.testData.storage.addDefaultIconSet();

      receivedIconSet = JSON.parse(request.requestBody);
      receivedIconSet.iconSet._id = iconSet._id;

      return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedIconSet)];
    });

    receivedIconSet = null;
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  describe("when there is a team available", function () {
    let team;

    beforeEach(function () {
      team = server.testData.create.team();
      server.testData.storage.addTeam(team);
    });

    it('should redirect to login without an auth token it', async function () {
      invalidateSession();

      await visit('/manage/icons/add');
      expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Ficons%2Fadd');
    });

    it('can visit /manage/icons/add', async function () {
      this.timeout(20000);
      authenticateSession();
      await visit('/manage/icons/add');
      await waitFor(".editor-is-ready", {timeout: 3000});

      expect(currentURL()).to.equal('/manage/icons/add');
      expect(this.element.querySelector('h1').textContent.trim()).to.equal("New icon set");
      expect(this.element.querySelector('.breadcrumb')).not.to.exist;

      expect(this.element.querySelector('.row-name h3').textContent.trim()).to.contain("Name");
      expect(this.element.querySelector('.row-name input.form-control')).to.exist;

      expect(this.element.querySelector('.row-description h3').textContent.trim()).to.contain("Description");
      expect(this.element.querySelector('.row-description .editor-js')).to.exist;
      expect(this.element.querySelector('.ce-paragraph').textContent).to.contain("This is our new icon set designed to be used in our new projects.");

      expect(this.element.querySelector('.row-team h3').textContent.trim()).to.contain("Team");
      expect(this.element.querySelector('.row-team select')).to.exist;

      expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.exist;

      expect(this.element.querySelector(".alert-danger .btn-add-team")).not.to.exist;
      expect(this.element.querySelector(".alert-danger")).not.to.exist;
    });

    it('does not make a "/iconsets/categories" request when navigating from the add page to Browse', async function () {
      authenticateSession();
      await visit('/manage/icons/add');

      await visit('/browse');

      expect(server.history).to.not.contain("GET /mock-server/iconsets/categories");
    });

    it('can create an icon set', async function () {
      this.timeout(20000);
      authenticateSession();
      await visit('/manage/icons/add');
      await waitFor(".editor-is-ready", {timeout: 3000});

      expect(currentURL()).to.equal('/manage/icons/add');
      await fillIn('.row-name input.form-control', "test name");
      await fillIn(".ce-paragraph", "test description");

      this.element.querySelector(".row-team select").value = team._id;
      await triggerEvent(".row-team select", "change");

      await PageElements.wait(1000);

      await click(".btn-submit");

      expect(receivedIconSet.iconSet.description.blocks).to.deep.equal([{
          "type": "header",
          "data": {
            "text": "test name",
            "level": 1
          }
        },{
          "type": "paragraph",
          "data": {
            "text": "test description"
          }
        }
      ]);

      expect(receivedIconSet.iconSet).to.deep.equal({
        "_id": iconSet._id,
        "cover": null,
        "name": "test name",
        "description": {
          blocks: [
            {
              "type": "header",
              "data": {
                "text": "test name",
                "level": 1
              }
            },
            {
              "type": "paragraph",
              "data": {
                "text": "test description"
              }
            }
          ]
        },
        "visibility": { "isDefault": false, "isPublic": false, "team": team._id }
      });

      expect(currentURL()).to.equal('/manage/icons/edit/' + iconSet._id);
    });

    it('can create an icon set without changing the default description', async function () {
      this.timeout(20000);
      authenticateSession();
      await visit('/manage/icons/add');
      await waitFor(".editor-is-ready", {timeout: 3000});

      expect(currentURL()).to.equal('/manage/icons/add');
      await fillIn('.row-name input.form-control', "test name");

      this.element.querySelector(".row-team select").value = team._id;
      await triggerEvent(".row-team select", "change");

      await PageElements.wait(1000);

      await click(".btn-submit");

      expect(receivedIconSet.iconSet.description.blocks).to.deep.equal([{
          "type": "header",
          "data": {
            "text": "test name",
            "level": 1
          }
        },{
          "type": "paragraph",
          "data": {
            "text": "This is our new icon set designed to be used in our new projects."
          }
        }
      ]);

      expect(receivedIconSet.iconSet).to.deep.equal({
        "_id": iconSet._id,
        "cover": null,
        "name": "test name",
        "description": {
          blocks: [
            {
              "type": "header",
              "data": {
                "text": "test name",
                "level": 1
              }
            },
            {
              "type": "paragraph",
              "data": {
                "text": "This is our new icon set designed to be used in our new projects."
              }
            }
          ]
        },
        "visibility": { "isDefault": false, "isPublic": false, "team": team._id }
      });

      expect(currentURL()).to.equal('/manage/icons/edit/' + iconSet._id);
    });

    it('has the Add button disabled when there is no name set', async function () {
      this.timeout(4000);
      authenticateSession();
      await visit('/manage/icons/add');

      expect(currentURL()).to.equal('/manage/icons/add');

      this.element.querySelector(".row-team select").value = team._id;
      await triggerEvent(".row-team select", "change");

      expect(this.element.querySelector('.btn.btn-submit')).to.have.class("disabled");
      expect(this.element.querySelector('.btn.btn-submit')).to.have.attribute("disabled", "");
    });

    it('has the Add button disabled when there is no team selected', async function () {
      this.timeout(4000);
      authenticateSession();
      await visit('/manage/icons/add');

      expect(currentURL()).to.equal('/manage/icons/add');

      await fillIn('.row-name input.form-control', "test name");

      expect(this.element.querySelector('.btn.btn-submit')).to.have.class("disabled");
      expect(this.element.querySelector('.btn.btn-submit')).to.have.attribute("disabled", "");
    });

    it('should load only teams that the user can edit', async function () {
      authenticateSession();
      await visit('/manage/icons/add');
      await waitUntil(() => server.history.filter(a => a.indexOf("teams") != -1).length > 0);
      expect(server.history).to.contain("GET /mock-server/teams?all=false&edit=true");
    });

  });

  describe("when no team is available", function() {
    it('should show a button to guide the user to create a team first', async function () {
      authenticateSession();
      await visit('/manage/icons/add');

      expect(this.element.querySelector(".row-team .btn-add-team")).to.exist;
      expect(this.element.querySelector(".alert-danger .btn-add-team")).to.exist;
    });

    it('the button in the alert should redirect the user to the add a team form', async function () {
      authenticateSession();
      await visit('/manage/icons/add');
      await click(".alert-danger .btn-add-team");

      expect(currentURL()).to.equal('/manage/teams/add?next=manage.icons.add');
    });

    it('the button in the Team section should also redirect to the add a team form', async function() {
      authenticateSession();
      await visit('/manage/icons/add');

      await click(".row-team .btn-add-team");

      expect(currentURL()).to.equal('/manage/teams/add?next=manage.icons.add');
    });
  });

});
