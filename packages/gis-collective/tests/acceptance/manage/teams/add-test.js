import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  fillIn,
  waitUntil,
  waitFor,
} from '@ember/test-helpers';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';
import PageElements from '../../../helpers/page-elements';

describe('Acceptance | manage/teams/add', function () {
  setupApplicationTest();
  let server;
  let receivedTeam;

  beforeEach(function () {
    server = new TestServer();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();

    server.post('/mock-server/teams', (request) => {
      receivedTeam = JSON.parse(request.requestBody);
      receivedTeam.team['_id'] = '5ca78e2160780601008f69e6';

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedTeam),
      ];
    });

    receivedTeam = null;
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token it', async function () {
    invalidateSession();

    await visit('/manage/teams/add');
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fteams%2Fadd');
  });

  it('can visit /manage/teams/add', async function () {
    authenticateSession();
    await visit('/manage/teams/add');
    await waitFor('.editor-is-ready', { timeout: 3000 });

    expect(currentURL()).to.equal('/manage/teams/add');
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'New team'
    );
    expect(this.element.querySelector('.breadcrumb')).not.to.exist;

    expect(
      this.element.querySelector('.row-name h3').textContent.trim()
    ).to.contain('Name');
    expect(this.element.querySelector('.row-name input.form-control')).to.exist;

    expect(
      this.element.querySelector('.row-about h3').textContent.trim()
    ).to.contain('About');
    expect(this.element.querySelector('.row-about .editor-js')).to.exist;
    expect(this.element.querySelector('.ce-paragraph').textContent).to.contain(
      'This is our team, dedicated to map all our projects.'
    );

    expect(this.element.querySelector('.btn.btn-primary.btn-submit')).to.exist;
  });

  it('can create a team', async function () {
    this.timeout(15000);
    authenticateSession();
    await visit('/manage/teams/add');
    await waitFor('.editor-is-ready', { timeout: 3000 });

    expect(currentURL()).to.equal('/manage/teams/add');
    await fillIn('.row-name input.form-control', 'test name');
    await fillIn('.ce-paragraph', 'test description');

    await PageElements.wait(1000);

    await click('.btn-submit');
    await waitUntil(() => receivedTeam);

    expect(receivedTeam.team.about.blocks).to.deep.equal([
      { type: 'header', data: { text: 'test name', level: 1 } },
      { type: 'paragraph', data: { text: 'test description' } },
    ]);

    expect(receivedTeam).to.deep.equal({
      team: {
        _id: '5ca78e2160780601008f69e6',
        about: {
          blocks: [
            { type: 'header', data: { text: 'test name', level: 1 } },
            { type: 'paragraph', data: { text: 'test description' } },
          ],
        },
        guests: [],
        isPublic: false,
        isPublisher: false,
        leaders: [],
        logo: null,
        members: [],
        name: 'test name',
        owners: [],
      },
    });

    expect(currentURL()).to.equal(`/manage/teams/${receivedTeam.team._id}`);
  });

  it('has the submit button disabled when there is no name', async function () {
    this.timeout(4000);
    authenticateSession();
    await visit('/manage/teams/add');

    expect(currentURL()).to.equal('/manage/teams/add');

    expect(this.element.querySelector('.btn.btn-submit')).to.have.class(
      'disabled'
    );
    expect(this.element.querySelector('.btn.btn-submit')).to.have.attribute(
      'disabled',
      ''
    );
  });

  it('redirects the user to the route provided as query param value if the `next` query param is present', async function () {
    authenticateSession();
    await visit('/manage/teams/add?next=manage.maps.add');

    await fillIn('.row-name input.form-control', 'test name');
    await click('.btn-submit');
    await waitUntil(() => receivedTeam);

    expect(currentURL()).to.equal(`/manage/maps/add`);
  });
});
