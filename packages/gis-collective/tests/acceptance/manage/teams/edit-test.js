import {
  describe,
  it,
  beforeEach,
  afterEach
} from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  waitUntil,
  triggerEvent,
  click,
  waitFor,
  fillIn
} from '@ember/test-helpers';
import {
  authenticateSession,
  invalidateSession
} from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';
import PageElements from '../../../helpers/page-elements';

describe('Acceptance | manage/teams/edit', function() {
  setupApplicationTest();
  let server;
  let team;
  let receivedTeam;
  let defaultPicture;

  beforeEach(function() {
    server = new TestServer();
    team = server.testData.create.team();
    defaultPicture = server.testData.create.picture();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addTeam(team);

    server.post(`/mock-server/pictures`, () => {
      return [200, { "Content-Type": "application/json" }, JSON.stringify({
        picture: defaultPicture
      })];
    });

    server.put(`/mock-server/teams/${team._id}`, (request) => {
      receivedTeam = JSON.parse(request.requestBody);
      receivedTeam.team["_id"] = team._id;

      return [200, { "Content-Type": "application/json" }, JSON.stringify(receivedTeam)];
    });

    receivedTeam = null;
  });

  afterEach(function() {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token it', async function() {
    invalidateSession();

    await visit(`/manage/teams/${team._id}`);
    expect(currentURL()).to.equal('/login?redirect=%2Fmanage%2Fteams%2F5ca78e2160780601008f69e6');
  });

  it('should show the team page', async function() {
    authenticateSession();

    await visit(`/manage/teams/${team._id}`);
    expect(currentURL()).to.equal(`/manage/teams/${team._id}`);

    await waitFor(".editor-is-ready", {timeout: 3000});

    expect(PageElements.breadcrumbs()).to.deep.equal(['Your teams', 'Edit Open Green Map']);
    expect(PageElements.containerGroupTitles()).to.deep.equal([ 'logo', 'gallery', 'members', 'existing']);
  });

  it('should be able to update the article', async function() {
    authenticateSession();

    await visit(`/manage/teams/${team._id}`);
    expect(currentURL()).to.equal(`/manage/teams/${team._id}`);

    await waitFor(".editor-is-ready", {timeout: 3000});

    await fillIn(".ce-header", "new name");
    await fillIn(".ce-paragraph", "new about");
    await waitUntil(() => !this.element.querySelector(".btn-publish").hasAttribute("disabled") );
    await click(".btn-publish");

    await waitUntil(() => receivedTeam );

    expect(receivedTeam.team.name).to.equal("new name");
    expect(receivedTeam.team.about.blocks).to.deep.equal([
      { type: 'header',    data: { text: 'new name', level: 1 }},
      { type: 'paragraph', data: { text: 'new about' }}]);
  });

  it('should be able to update the logo', async function() {
    authenticateSession();

    await visit(`/manage/teams/${team._id}`);
    expect(currentURL()).to.equal(`/manage/teams/${team._id}`);

    const blob = server.testData.create.pngBlob();

    await triggerEvent(
      ".row-logo input[type='file']",
      "change",
      { files: [ blob ]}
    );

    await waitUntil(() => receivedTeam );

    expect(receivedTeam.team.logo).to.equal(defaultPicture._id);
  });

  it('should be able to update two gallery pictures', async function() {
    authenticateSession();

    await visit(`/manage/teams/${team._id}`);
    expect(currentURL()).to.equal(`/manage/teams/${team._id}`);

    const blob = server.testData.create.pngBlob();

    await triggerEvent(
      ".row-gallery input.image-list-input[type='file']",
      "change",
      { files: [ blob ]}
    );

    await waitUntil(() => receivedTeam );
    receivedTeam = null;

    await triggerEvent(
      ".row-gallery input.image-list-input[type='file']",
      "change",
      { files: [ blob ]}
    );

    await waitUntil(() => receivedTeam );

    expect(receivedTeam.team.pictures).to.deep.equal([defaultPicture._id, defaultPicture._id]);
  });

  it('should render the "is public" label', async function() {
    authenticateSession();

    await visit(`/manage/teams/${team._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    expect(this.element.querySelector(".side-bar").textContent).to.contain("is public");
    expect(this.element.querySelector(".container-group-is-public label").textContent).to.contain("is public");

  });

  it('should be able to make the team not public', async function() {
    this.timeout(5000);
    authenticateSession();

    await visit(`/manage/teams/${team._id}`);

    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector(".side-bar .btn-open");
    if(openBtn) {
      await click(openBtn);
    }

    await click(this.element.querySelector(".side-bar .container-group-is-public .btn-switch"));

    await waitUntil(() => receivedTeam != null, { timeout: 3000 });

    expect(receivedTeam.team.isPublic).to.equal(false);
  });

});
