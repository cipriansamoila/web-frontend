import {
  describe,
  it,
  beforeEach,
  afterEach
} from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  waitUntil,
  fillIn,
  click
} from '@ember/test-helpers';
import {
  authenticateSession,
  invalidateSession
} from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';

describe('Acceptance | manage/teams/index', function() {
  setupApplicationTest();
  let server;

  beforeEach(function() {
    this.timeout(10000);
    server = new TestServer();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultPicture();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultIcon();
  });

  afterEach(function() {
    server.shutdown();
    Modal.clear();
  });

  it("should redirect to login without an auth token it", async function() {
    invalidateSession();

    await visit(`/manage/teams`);
    expect(currentURL()).to.contain('/login');
  });

  it("should navigate to home page if the user is not an admin", async function() {
    server.testData.storage.addDefaultUser(false);
    authenticateSession();

    await visit(`/manage/teams`);
    expect(currentURL()).to.equal('/browse/maps/_/map-view');

    const table = this.element.querySelector("table");
    expect(table).not.to.exist;
  });


  describe("for an admin", function() {
    beforeEach(function() {
      server.testData.storage.addDefaultUser(true);
    });

    it("should show the team list with at least 2 columns", async function() {
      authenticateSession();

      await visit(`/manage/teams`);

      expect(currentURL()).to.equal(`/manage/teams`);

      expect(this.element.querySelector(".error")).to.not.exist;
      expect(this.element.querySelector("h1").textContent.trim()).to.equal("All teams");

      const buckets = this.element.querySelector(".team-buckets");
      this.element.querySelector('.infinity-loader').scrollIntoView();

      await waitUntil(() => buckets.querySelectorAll(".team-buckets tr").length > 1, { timeout: 1000 });
      expect(buckets.querySelectorAll(".team-buckets tr").length).to.be.gt(1);
    });

    it("should search teams by name", async function() {
      this.timeout(5000);
      authenticateSession();

      await visit(`/manage/teams`);
      expect(currentURL()).to.equal(`/manage/teams`);

      await fillIn(".main-container .input-search", "search me");

      server.history = [];
      await click(".btn-search");

      expect(currentURL()).to.equal(`/manage/teams?search=search%20me`);
      expect(server.history.filter(a => a.indexOf(" /mock-server/team") != -1)[0]).to.contain("&term=search+me");
    });
  })
});
