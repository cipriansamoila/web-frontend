import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  fillIn,
  triggerEvent,
  click,
  waitUntil,
} from '@ember/test-helpers';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';
import TestServer from '../../../helpers/test-server';
import Modal from '../../../helpers/modal';

describe('Acceptance | manage/basemaps/add', function () {
  setupApplicationTest();
  let server;
  let receivedBaseMap;

  beforeEach(function () {
    server = new TestServer();

    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();

    server.get('/mock-server/basemaps/1', () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedBaseMap),
      ];
    });

    receivedBaseMap = null;
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  describe('when there is a team available', function () {
    beforeEach(function () {
      server.testData.storage.addDefaultTeam();
    });

    it('should redirect to login without an auth token it', async function () {
      invalidateSession();

      await visit('/manage/basemaps/add');
      expect(currentURL()).to.equal(
        '/login?redirect=%2Fmanage%2Fbasemaps%2Fadd'
      );
    });

    it('should not redirect to login without an auth token', async function () {
      authenticateSession();

      await visit('/manage/basemaps/add');

      expect(currentURL()).to.equal('/manage/basemaps/add');
      expect(this.element.querySelector('.input-base-map-name')).to.exist;
      expect(this.element.querySelector('.input-base-map-icon')).to.exist;
      expect(this.element.querySelector('.input-base-map-team')).to.exist;
      expect(this.element.querySelector('.btn.btn-primary')).to.exist;

      expect(this.element.querySelector('.alert-danger .btn-add-team')).not.to
        .exist;
      expect(this.element.querySelector('.alert-danger')).not.to.exist;
    });

    it('should disable the add button by default', async function () {
      authenticateSession();

      await visit('/manage/basemaps/add');

      expect(currentURL()).to.equal('/manage/basemaps/add');
      expect(this.element.querySelector('.btn.btn-primary')).to.have.class(
        'disabled'
      );
      expect(this.element.querySelector('.btn.btn-primary')).to.have.attribute(
        'disabled',
        ''
      );
    });

    it('should enable the add button when all fields are filled', async function () {
      authenticateSession();

      await visit('/manage/basemaps/add');
      expect(currentURL()).to.equal('/manage/basemaps/add');

      await fillIn('.input-base-map-name', 'test');
      await fillIn('.input-base-map-icon', 'box');

      this.element.querySelector('.input-base-map-team').value =
        '5ca78e2160780601008f69e6';
      await triggerEvent('.input-base-map-team', 'change');

      expect(this.element.querySelector('.btn.btn-primary')).not.to.have.class(
        'disabled'
      );
      expect(
        this.element.querySelector('.btn.btn-primary')
      ).not.to.have.attribute('disabled', '');
    });

    it('should send all values to the server and redirect to the index page', async function () {
      server.post('/mock-server/basemaps', (request) => {
        receivedBaseMap = JSON.parse(request.requestBody);
        receivedBaseMap['baseMap']['_id'] = '1';

        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(receivedBaseMap),
        ];
      });

      authenticateSession();

      await visit('/manage/basemaps/add');

      await fillIn('.input-base-map-name', 'test');
      await fillIn('.input-base-map-icon', 'box');

      this.element.querySelector('.input-base-map-team').value =
        '5ca78e2160780601008f69e6';
      await triggerEvent('.input-base-map-team', 'change');

      await waitUntil(
        () =>
          this.element.querySelectorAll('.btn.btn-primary.disabled').length == 0
      );

      await click(this.element.querySelector('.btn.btn-primary'));

      expect(currentURL()).to.equal('/manage/basemaps/1');

      expect(receivedBaseMap).to.deep.equal({
        baseMap: {
          name: 'test',
          attributions: [],
          icon: 'box',
          layers: [],
          visibility: {
            isPublic: false,
            isDefault: false,
            team: '5ca78e2160780601008f69e6',
          },
          defaultOrder: 100,
          cover: null,
          _id: '1',
        },
      });
    });

    it('should show a modal with an error on failure', async function () {
      await this.timeout(20000);

      server.post('/mock-server/basemaps', () => {
        return [
          400,
          { 'Content-Type': 'application/json' },
          JSON.stringify({
            errors: [
              {
                title: 'Some error',
                description: 'some message',
                status: 400,
              },
            ],
          }),
        ];
      });

      authenticateSession();

      await visit('/manage/basemaps/add');

      await fillIn('.input-base-map-name', 'test');
      await fillIn('.input-base-map-icon', 'box');

      this.element.querySelector('.input-base-map-team').value =
        '5ca78e2160780601008f69e6';
      await triggerEvent('.input-base-map-team', 'change');

      await waitUntil(
        () =>
          this.element.querySelectorAll('.btn.btn-primary.disabled').length == 0
      );
      await click(this.element.querySelector('.btn.btn-primary'));

      await Modal.waitToDisplay();
      expect(
        document.querySelector('.modal-title').textContent.trim()
      ).to.equal('Some error');
      expect(document.querySelector('.modal-body').textContent.trim()).to.equal(
        'some message'
      );

      await click('.btn-resolve');
      await Modal.waitToHide();
    });
  });

  describe('when there is no team available', function () {
    it('should show a button to guide the user to create a team first', async function () {
      authenticateSession();
      await visit('/manage/basemaps/add');

      expect(this.element.querySelector('.row-team .btn-add-team')).to.exist;
      expect(this.element.querySelector('.alert-danger .btn-add-team')).to
        .exist;
    });

    it('the button in the alert should redirect the user to the add a team form', async function () {
      authenticateSession();
      await visit('/manage/basemaps/add');
      await click('.alert-danger .btn-add-team');

      expect(currentURL()).to.equal(
        '/manage/teams/add?next=manage.basemaps.add'
      );
    });

    it('the button in the Team section should also redirect to the add a team form', async function () {
      authenticateSession();
      await visit('/manage/basemaps/add');

      await click('.row-team .btn-add-team');

      expect(currentURL()).to.equal(
        '/manage/teams/add?next=manage.basemaps.add'
      );
    });
  });
});
