import { describe, it, beforeEach, afterEach } from 'mocha';
import { expect } from 'chai';
import { setupApplicationTest } from 'ember-mocha';
import {
  visit,
  currentURL,
  click,
  waitUntil,
  triggerEvent,
} from '@ember/test-helpers';
import TestServer from '../../../helpers/test-server';
import PageElements from '../../../helpers/page-elements';
import Modal from '../../../helpers/modal';
import {
  authenticateSession,
  invalidateSession,
} from 'ember-simple-auth/test-support';

describe('Acceptance | manage/basemaps/edit', function () {
  setupApplicationTest();
  let server;
  let receivedBaseMap;
  let baseMap;
  let deletedSite;

  beforeEach(function () {
    server = new TestServer();
    server.testData.storage.addDefaultPreferences();
    server.testData.storage.addDefaultUser();
    server.testData.storage.addDefaultTeam();
    server.testData.storage.addDefaultTeam('000000000000000000000001');
    server.testData.storage.addDefaultPicture();
    baseMap = server.testData.create.baseMap();
    server.testData.storage.addDefaultBaseMap(baseMap._id);

    server.get('/mock-server/basemaps/1', () => {
      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedBaseMap),
      ];
    });

    receivedBaseMap = null;
    server.put(`/mock-server/basemaps/${baseMap._id}`, (request) => {
      receivedBaseMap = JSON.parse(request.requestBody);

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(receivedBaseMap),
      ];
    });

    deletedSite = false;
    server.delete(`/mock-server/basemaps/${baseMap._id}`, () => {
      deletedSite = true;

      return [204, {}, ''];
    });
  });

  afterEach(function () {
    server.shutdown();
    Modal.clear();
  });

  it('should redirect to login without an auth token', async function () {
    invalidateSession();

    await visit(`/manage/basemaps/${baseMap._id}`);
    expect(currentURL()).to.equal(
      '/login?redirect=%2Fmanage%2Fbasemaps%2F000000111111222222333333'
    );
  });

  it('can visit the edit page of an existing basemap', async function () {
    authenticateSession();
    await visit(`/manage/basemaps/${baseMap._id}`);
    expect(currentURL()).to.equal(`/manage/basemaps/${baseMap._id}`);

    expect(this.element.querySelector('.error')).not.to.exist;
    expect(PageElements.breadcrumbs()).to.deep.equal([
      'Dashboard',
      `Edit ${baseMap.name}`,
    ]);
    expect(this.element.querySelector('h1').textContent.trim()).to.equal(
      'Edit base map'
    );
    expect(PageElements.rowTitles()).to.deep.equal([
      'Name',
      'attributions',
      'icon',
      'layers',
      'cover',
    ]);
  });

  it('can delete a base map', async function () {
    authenticateSession();
    await visit(`/manage/basemaps/` + baseMap._id);
    expect(currentURL()).to.equal('/manage/basemaps/' + baseMap._id);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.btn-delete');
    await Modal.waitToDisplay();
    await Modal.clickDangerButton();

    expect(deletedSite).to.equal(true);
  });

  it('should be able to change the team', async function () {
    authenticateSession();

    await visit(`/manage/basemaps/${baseMap._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-team .btn-edit');

    this.element.querySelector('.value.editing select').value =
      '000000000000000000000001';
    await triggerEvent('.value.editing select', 'change');

    await waitUntil(() => receivedBaseMap != null, { timeout: 3000 });

    expect(receivedBaseMap.baseMap.visibility.team).to.equal(
      '000000000000000000000001'
    );
  });

  it('should render the "is public" label', async function () {
    authenticateSession();

    await visit(`/manage/basemaps/${baseMap._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    expect(this.element.querySelector('.side-bar').textContent).to.contain(
      'is public'
    );
    expect(
      this.element.querySelector('.container-group-is-public label').textContent
    ).to.contain('is public');
  });

  it('should render the "is default" label', async function () {
    authenticateSession();

    await visit(`/manage/basemaps/${baseMap._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    expect(this.element.querySelector('.side-bar').textContent).to.contain(
      'is default'
    );
    expect(
      this.element.querySelector('.container-group-is-default label')
        .textContent
    ).to.contain('is default');
  });

  it('should be able to unpublish the baseMap', async function () {
    authenticateSession();

    await visit(`/manage/basemaps/${baseMap._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(
      this.element.querySelector(
        '.side-bar .container-group-is-public .btn-switch'
      )
    );

    await waitUntil(() => receivedBaseMap != null, { timeout: 3000 });

    expect(receivedBaseMap.baseMap.visibility.isPublic).to.equal(false);
  });

  it('should be able to set the baseMap as default', async function () {
    authenticateSession();

    await visit(`/manage/basemaps/${baseMap._id}`);
    expect(this.element.querySelector('.main-container > .error')).not.to.exist;

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click(
      this.element.querySelector(
        '.side-bar .container-group-is-default .btn-switch'
      )
    );

    await waitUntil(() => receivedBaseMap != null, { timeout: 3000 });

    expect(receivedBaseMap.baseMap.visibility.isDefault).to.equal(true);
  });

  it('should not show the show all teams button', async function () {
    authenticateSession();
    await visit(`/manage/basemaps/${baseMap._id}`);

    const openBtn = this.element.querySelector('.side-bar .btn-open');
    if (openBtn) {
      await click(openBtn);
    }

    await click('.container-group-team .btn-edit');

    expect(
      this.element.querySelector('.container-group-team .btn-show-all-teams')
    ).not.to.exist;
  });

  describe('when the authenticated user is an administrator', function () {
    beforeEach(function () {
      server.testData.storage.addDefaultUser(true);
    });

    it('should show all teams on pressing the show all teams button', async function () {
      authenticateSession();
      await visit(`/manage/basemaps/${baseMap._id}`);

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      expect(server.history).not.to.contain(`GET /mock-server/teams?all=true`);

      await click('.container-group-team .btn-edit');
      await click('.container-group-team .btn-show-all-teams');

      expect(currentURL()).to.equal(
        `/manage/basemaps/${baseMap._id}?allTeams=true`
      );
      expect(server.history).to.contain(`GET /mock-server/teams?all=true`);

      expect(
        this.element.querySelector('.container-group-team .btn-show-all-teams')
      ).to.exist;
    });

    it('should show owned teams on pressing the show all teams button', async function () {
      authenticateSession();
      await visit(`/manage/basemaps/${baseMap._id}?allTeams=true`);

      const openBtn = this.element.querySelector('.side-bar .btn-open');
      if (openBtn) {
        await click(openBtn);
      }

      expect(server.history).not.to.contain(`GET /mock-server/teams?edit=true`);

      await click('.container-group-team .btn-edit');
      await click('.container-group-team .btn-show-all-teams');

      expect(currentURL()).to.equal(`/manage/basemaps/${baseMap._id}`);
      expect(server.history).to.contain(`GET /mock-server/teams?edit=true`);

      expect(
        this.element.querySelector('.container-group-team .btn-show-all-teams')
      ).to.exist;
    });
  });
});
