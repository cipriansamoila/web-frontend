import Application from 'ogm/app';
import config from 'ogm/config/environment';
import { setApplication } from '@ember/test-helpers';
import { start } from 'ember-mocha';
import { mocha } from 'mocha';
import { find } from 'ember-test-helpers';

import chai from 'chai';
import chaiString from 'chai-string';
import chaiDom from 'chai-dom';

chai.use(chaiDom);
chai.use(chaiString);
chai.config.truncateThreshold = 0;

const elementFrom = (target) => {
  if (typeof target === 'object') return target;
  return find(target);
};

chai.Assertion.addMethod('attribute', function (attribute, expectedAttribute) {
  let actualAttribute = elementFrom(this._obj).getAttribute(attribute);

  if (typeof actualAttribute === 'string') {
    actualAttribute = actualAttribute
      .split(' ')
      .map((a) => a.trim())
      .filter((a) => a != '')
      .join(' ');
  }

  this.assert(
    actualAttribute === expectedAttribute,
    `expected #{this} to have ${attribute} to #{exp}, but got #{act}`,
    `expected #{this} to not have ${attribute} to #{act}`,
    expectedAttribute,
    actualAttribute
  );
});

chai.Assertion.addMethod('class', function (expectedAttribute) {
  const classList = elementFrom(this._obj)
    .getAttribute('class')
    .split(' ')
    .map((a) => a.trim())
    .filter((a) => a);

  this.assert(
    classList.find((a) => a == expectedAttribute),
    `expected #{this} to have class to #{exp}, but got #{act}`,
    `expected #{this} to not have class to #{act}`,
    expectedAttribute,
    classList.join(', ')
  );
});

mocha.timeout(15000);

setApplication(Application.create(config.APP));

start();
