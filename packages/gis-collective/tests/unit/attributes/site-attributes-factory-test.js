import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupTest } from 'ember-mocha';
import EmberObject from '@ember/object';
import { A } from '@ember/array';
import AttributesContainer from 'ogm/attributes/containers/attributes';

describe("Unit | Attributes | Containers | Attributes", function() {
  setupTest();

  var factory;

  beforeEach(function() {
    factory = new AttributesContainer({ t(val) { return val; } });
  });

  it("should have an empty group list", function() {
    expect(factory.groups.length).to.equal(0);
  });

  describe("when there is a site with attributes and icons", function() {
    var site;

    beforeEach(function() {
      site = EmberObject.create({
        "attributes": EmberObject.create({
          "Some Attributes": {
            "key": "value"
          }
        }),

        "icons": A([ EmberObject.create({ "name": "Some Attributes", "attributes": [
          {name: "key 1", localName: "local key 1"},
          {name: "key 2", localName: "local key 2"}
        ]}) ])
      });

      factory.icons = site.icons;
      factory.feature = site;
    });


    it("should contain a group with icon for the existing attribute set", function() {
      expect(factory.groups.length).to.equal(1);

      expect(factory.groups[0].name).to.equal("Some Attributes");
      expect(factory.groups[0].icon).to.equal(site.icons.objectAt(0));
    });

    it("should contain a group for the existing attribute set", function() {
      expect(factory.groups.length).to.equal(1);

      expect(factory.groups[0].name).to.equal("Some Attributes");
      expect(factory.groups[0].type).to.equal("list");

      expect(factory.groups[0].list.length).to.equal(2);
    });

    it("should update the group values", function() {
      site.set("attributes", {"Some Attributes": { "key": "other value"}});

      expect(factory.groups.length).to.equal(1);

      expect(factory.groups.objectAt(0).name).to.equal("Some Attributes");
      expect(factory.groups.objectAt(0).type).to.equal("list");

      expect(factory.groups.objectAt(0).list.length).to.equal(2);
    });

    it("should show a the list of attributes", function() {
      expect(factory.groups.length).to.equal(1);

      expect(factory.groups[0].name).to.equal("Some Attributes");
      expect(factory.groups[0].order).to.be.undefined;
      expect(factory.groups[0].indexed).to.equal(false);
      expect(factory.groups[0].addButton).to.equal(false);
      expect(factory.groups[0].list.length).to.equal(2);
    });
  });
});
