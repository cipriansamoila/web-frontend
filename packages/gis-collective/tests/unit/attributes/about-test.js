import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupTest } from 'ember-mocha';
import EmberObject from '@ember/object';
import AboutAttributes from 'ogm/attributes/containers/about';

describe("Unit | Attributes | Containers | About", function() {
  setupTest();

  var about;

  beforeEach(function() {
    about = new AboutAttributes({ t(val) { return val; } });
  });

  describe("group when there is no site set", function() {
    it("should have no groups", function() {
      expect(about.groups.length).to.equal(0);
    });
  });

  describe("group when a site with no fields is set", function() {
    let site;

    beforeEach(function() {
      site = EmberObject.create({});
      about.feature = site;
    });

    it("should have the name About", function() {
      expect(about.groups[0].name).to.equal("about");
    });

    it("should have the type list", function() {
      expect(about.groups[0].type).to.equal("list");
    });

    it("should have 3 elements", function() {
      expect(about.groups[0].list.length).to.equal(3);
    });

    it("should have the first element the `maps` field", function() {
      expect(about.groups[0].list[0].name).to.equal("maps");
      expect(about.groups[0].list[0].value).to.be.an("undefined");
    });

    it("should have the second element the `name` field", function() {
      expect(about.groups[0].list[1].name).to.equal("name");
      expect(about.groups[0].list[1].value).to.be.an("undefined");
    });

    it("should have the third element the `description` field", function() {
      expect(about.groups[0].list[2].name).to.equal("description");
      expect(about.groups[0].list[2].value).to.be.an("undefined");
    });
  });

  describe("groups list is no site set", function() {
    it("should be empty", function() {
      expect(about.groups.length).to.equal(0);
    });
  });

  describe("groups list when a site is set", function() {
    let site;

    beforeEach(function() {
      site = EmberObject.create({});

      about.feature = site;
    });

    it("should have one element", function() {
      expect(about.groups.length).to.equal(1);
    });

    it("should have first element the generated group", function() {
      expect(about.groups[0]).to.equal(about.group);
    });
  });

  describe("group when a site with all fields", function() {
    var site;

    beforeEach(function() {
      const map = EmberObject.create({ "name": "map1" });

      site = EmberObject.create({
        name: "some name",
        description: "some description",
        maps: [ map ]
      });

      about.feature = site;
    });

    it("should have 3 elements", function() {
      expect(about.group.list.length).to.equal(3);
    });

    it("should have the first element the `maps` field", function() {
      expect(about.group.list[0].name).to.equal("maps");
      expect(about.group.list[0].value.length).to.equal(1);
      expect(about.group.list[0].value[0].name).to.equal("map1");
    });

    it("should have the second element the `name` field", function() {
      expect(about.group.list[1].name).to.equal("name");
      expect(about.group.list[1].value).to.equal("some name");
    });

    it("should have the third element the `description` field", function() {
      expect(about.group.list[2].name).to.equal("description");
      expect(about.group.list[2].value).to.equal("some description");
    });
  });

  describe("when there is a site with 2 maps", function() {
    var site;
    let map1;
    let map2;

    beforeEach(function() {
      map1 = EmberObject.create({ "name": "map1" });
      map2 = EmberObject.create({ "name": "map2" });

      site = EmberObject.create({
        maps: [ map1, map2 ]
      });

      about.feature = site;
    });


    it("should have the first element the `maps` field", function() {
      expect(about.group.list[0].name).to.equal("maps");
      expect(about.group.list[0].value.map(a => a.name)[0]).to.eq("map1");
      expect(about.group.list[0].value.map(a => a.name)[1]).to.eq("map2");
    });
  });
});
