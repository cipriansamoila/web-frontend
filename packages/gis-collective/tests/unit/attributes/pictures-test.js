import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupTest } from 'ember-mocha';
import EmberObject from '@ember/object';
import PicturesAttributes from 'ogm/attributes/containers/pictures';

describe("Unit | Attributes | Containers | Pictures", function() {
  setupTest();

  var picturesContainer;

  beforeEach(function() {
    picturesContainer = new PicturesAttributes({ t(val) { return val; } });
  });

  describe("when there is no site set", function() {
    it("should have no groups", function() {
      expect(picturesContainer.groups.length).to.equal(0);
    });
  });

  describe("when there is a site with a picture", function() {
    var site;

    beforeEach(function() {
      site = EmberObject.create({
        "pictures": [ EmberObject.create() ]
      });

      picturesContainer.feature = site;
    });

    it("should have the name Pictures", function() {
      expect(picturesContainer.groups[0].name).to.equal("pictures");
    });

    it("should have the type pictureList", function() {
      expect(picturesContainer.groups[0].type).to.equal("pictureList");
    });

    it("should have a list with the name attribute", function() {
      expect(picturesContainer.groups[0].list.length).to.equal(1);
    });

    it("should remove the pictures from the list", function() {
      site.pictures.clear();
      expect(picturesContainer.groups[0].list.length).to.equal(0);
    });
  });
});
