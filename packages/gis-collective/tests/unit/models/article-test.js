import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Model | article', function () {
  setupTest();

  // Replace this with your real tests.
  it('exists', function () {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('article', {});
    expect(model).to.be.ok;
  });

  it('fills in the meta info object', async function () {
    let store = this.owner.lookup('service:store');

    let model = store.createRecord('article', {
      slug: 'test',
      title: 'test',
      info: {
        changeIndex: 9,
        createdOn: '2021-02-22T18:47:01Z',
        lastChangeOn: '2021-03-07T17:45:21Z',
      },
      visibility: {
        isDefault: false,
        isPublic: true,
      },
      content: {
        blocks: [
          {
            type: 'header',
            data: { level: 1, text: 'Mapping Berlin Soundscapes' },
          },
          {
            type: 'paragraph',
            data: {
              text: "We'd like to collect urban nature sounds that characterize Berlin's natural life.",
            },
          },
        ],
      },
      _id: '6033fc2509977601001a959e',
    });

    model.visibility.team = store.createRecord('team', {
      _id: '5ca89e37ef1f7e010007f333',
      name: 'team name',
    });
    model.info.lastChangeOn = new Date('2021-03-07T17:45:21Z');

    const metaInfo = await model.fillMetaInfo({});

    expect(metaInfo).to.deep.equal({
      title: 'test',
      description: `We'd like to collect urban nature sounds that characterize Berlin's natural life.`,
      date: '2021-03-07T17:45:21.000Z',
    });
  });
});
