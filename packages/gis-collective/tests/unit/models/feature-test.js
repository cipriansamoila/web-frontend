import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Model | feature', function () {
  setupTest();

  it('exists', function () {
    let store = this.owner.lookup('service:store');
    let model = store.createRecord('feature', {});
    expect(model).to.be.ok;
  });

  describe('the displayIconAttributes', function () {
    it('returns an empty list there are none set', async function () {
      let store = this.owner.lookup('service:store');
      let model = await store.createRecord('feature', {});

      expect(model.displayIconAttributes).to.deep.equal([]);
    });

    it('returns a category object when there are some attributes set but no icons', async function () {
      let store = this.owner.lookup('service:store');
      let model = await store.createRecord('feature', {
        attributes: {
          icon1: {
            key: 'value',
          },
        },
      });

      const result = model.displayIconAttributes[0].toJSON();
      expect(result).to.deep.equal({
        names: ['icon1'],
        displayName: 'icon1',
        isTable: false,
        isEmpty: false,
        icon: undefined,
        attributes: [
          {
            name: 'key',
            displayName: 'key',
            value: 'value',
            type: undefined,
            isPresent: true,
          },
        ],
      });
    });

    it('returns the attribute object for an icon if it is matched by name', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', { name: 'icon1' });

      let model = await store.createRecord('feature', {
        icons: [icon],
        attributes: {
          icon1: {
            key: 'value',
          },
        },
      });

      model.icons.isFulfilled = true;

      const result = model.displayIconAttributes[0].toJSON();
      expect(result.attributes).to.deep.contain({
        name: 'key',
        displayName: 'key',
        type: undefined,
        value: 'value',
        isPresent: true,
      });
    });

    it('returns the attribute object for an icon if it is matched by one of the other name', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', {
        name: 'icon1',
        otherNames: ['old name 1', 'old name2'],
      });

      let model = await store.createRecord('feature', {
        icons: [icon],
        attributes: {
          'old name 1': {
            key: 'value',
          },
        },
      });

      const result = model.displayIconAttributes[0].toJSON();
      expect(result.attributes).to.deep.contain({
        name: 'key',
        displayName: 'key',
        type: undefined,
        value: 'value',
        isPresent: true,
      });
    });

    it('returns a list of properties when allow many is true', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', {
        name: 'icon1',
        allowMany: true,
      });

      let model = await store.createRecord('feature', {
        icons: [icon],
        attributes: {
          icon1: [
            {
              key1: 'value1',
            },
          ],
        },
      });

      model.icons.isFulfilled = true;

      const result = model.displayIconAttributes[0].toJSON();

      expect(result.attributes).to.have.length(1);
      expect(result.attributes[0]).to.deep.contain({
        name: 'key1',
        displayName: 'key1',
        type: undefined,
        value: 'value1',
        isPresent: true,
      });

      expect(result.isEmpty).to.equal(false);
    });

    it('sets isPresent = false when the attribute value is not found', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', {
        name: 'icon1',
        allowMany: true,
        attributes: [{ name: 'key1', type: 'text' }],
      });

      let model = await store.createRecord('feature', {
        icons: [icon],
        attributes: {
          icon1: [{}],
        },
      });

      model.icons.isFulfilled = true;

      const result = model.displayIconAttributes[0].toJSON();

      expect(result.attributes).to.have.length(1);

      expect(result.attributes[0]).to.deep.contain({
        name: 'key1',
        displayName: 'key1',
        type: 'text',
        isPresent: false,
        value: undefined,
      });

      expect(result.isEmpty).to.equal(false);
    });

    it('merges the attributes for an icon if it is matched by other names and name', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', {
        name: 'icon1',
        otherNames: ['old name 1', 'old name 2'],
      });

      let model = await store.createRecord('feature', {
        icons: [icon],
        attributes: {
          'old name 1': {
            key1: 'value1',
          },
          'old name 2': {
            key2: 'value2',
          },
          icon1: {
            key3: 'value3',
          },
        },
      });

      model.icons.isFulfilled = true;
      const result = model.displayIconAttributes[0].toJSON();
      expect(result.attributes).to.have.length(3);
      expect(result.attributes[0]).to.deep.contain({
        name: 'key1',
        displayName: 'key1',
        value: 'value1',
      });
      expect(result.attributes[1]).to.deep.contain({
        name: 'key2',
        displayName: 'key2',
        value: 'value2',
      });
      expect(result.attributes[2]).to.deep.contain({
        name: 'key3',
        displayName: 'key3',
        value: 'value3',
      });
    });

    it('merges array properties', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', {
        name: 'icon1',
        allowMany: null,
        otherNames: ['old name 1', 'old name 2'],
      });

      let model = await store.createRecord('feature', {
        icons: [icon],
        attributes: {
          'old name 1': [
            {
              key1: 'value1',
            },
          ],
          'old name 2': [
            {
              key2: 'value2',
            },
          ],
          icon1: [
            {
              key3: 'value3',
            },
          ],
        },
      });

      model.icons.isFulfilled = true;

      const result = model.displayIconAttributes[0].toJSON();

      expect(result.attributes).to.have.length(3);
      expect(result.attributes[0]).to.deep.contain({
        name: 'key1',
        displayName: 'key1',
        value: 'value1',
      });
      expect(result.attributes[1]).to.deep.contain({
        name: 'key2',
        displayName: 'key2',
        value: 'value2',
      });
      expect(result.attributes[2]).to.deep.contain({
        name: 'key3',
        displayName: 'key3',
        value: 'value3',
      });
    });

    it('merges array properties with object properties', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', {
        name: 'icon1',
        allowMany: null,
        otherNames: ['old name 1', 'old name 2'],
      });

      let model = await store.createRecord('feature', {
        icons: [icon],
        attributes: {
          'old name 1': {
            key1: 'value1',
          },
          'old name 2': {
            key2: 'value2',
          },
          icon1: [
            {
              key3: 'value3',
            },
          ],
        },
      });

      model.icons.isFulfilled = true;

      const result = model.displayIconAttributes[0].toJSON();

      expect(result.attributes).to.have.length(3);
      expect(result.attributes[0]).to.deep.contain({
        name: 'key1',
        displayName: 'key1',
        value: 'value1',
      });
      expect(result.attributes[1]).to.deep.contain({
        name: 'key2',
        displayName: 'key2',
        value: 'value2',
      });
      expect(result.attributes[2]).to.deep.contain({
        name: 'key3',
        displayName: 'key3',
        value: 'value3',
      });
    });

    it('merges object properties in an array when the icon has the allow many flag true', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', {
        name: 'icon1',
        allowMany: true,
        otherNames: ['old name 1', 'old name 2'],
      });

      let model = await store.createRecord('feature', {
        icons: [icon],
        attributes: {
          'old name 1': {
            key1: 'value1',
          },
          'old name 2': {
            key2: 'value2',
          },
          icon1: {
            key3: 'value3',
          },
        },
      });

      model.icons.isFulfilled = true;

      const result = model.displayIconAttributes[0].toJSON();

      expect(result.attributes).to.have.length(3);
      expect(result.attributes[0]).to.have.length(1);
      expect(result.attributes[0][0]).to.deep.contain({
        name: 'key1',
        displayName: 'key1',
        value: 'value1',
      });
      expect(result.attributes[1][0]).to.deep.contain({
        name: 'key2',
        displayName: 'key2',
        value: 'value2',
      });
      expect(result.attributes[2][0]).to.deep.contain({
        name: 'key3',
        displayName: 'key3',
        value: 'value3',
      });
    });

    it('adds the attributes from an icon with no value when values are not set', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', {
        name: 'icon1',
        attributes: [
          { name: 'name 1', displayName: 'display name 1', type: 'string' },
          { name: 'name 2', displayName: 'display name 2', type: 'number' },
        ],
      });

      let model = await store.createRecord('feature', {
        icons: [icon],
      });

      model.icons.isFulfilled = true;

      const result = model.displayIconAttributes[0];

      expect(result.attributes.length).to.equal(2);
      expect(result.isEmpty).to.equal(true);
      expect(result.attributes[0].toJSON()).to.deep.contain({
        name: 'name 1',
        displayName: 'display name 1',
        type: 'string',
      });
      expect(result.attributes[1].toJSON()).to.deep.contain({
        name: 'name 2',
        displayName: 'display name 2',
        type: 'number',
      });
    });

    it('returns an other section for string values', async function () {
      let store = this.owner.lookup('service:store');
      let model = await store.createRecord('feature', {
        attributes: {
          key1: 'value 1',
          key2: 2,
          key3: true,
        },
      });

      const result = model.displayIconAttributes[0];

      expect(result.attributes.length).to.equal(3);
      expect(result.isEmpty).to.equal(false);
      expect(result.displayName).to.equal('other');
      expect(result.attributes[0].toJSON()).to.deep.contain({
        name: 'key1',
        displayName: 'key1',
        value: 'value 1',
      });
      expect(result.attributes[1].toJSON()).to.deep.contain({
        name: 'key2',
        displayName: 'key2',
        value: 2,
      });
      expect(result.attributes[2].toJSON()).to.deep.contain({
        name: 'key3',
        displayName: 'key3',
        value: true,
      });
    });
  });

  describe('the iconAttributes', function () {
    it('returns an empty attribute object when there are none set', async function () {
      let store = this.owner.lookup('service:store');
      let model = await store.createRecord('feature', {});

      expect(model.iconAttributes).to.deep.equal({});
    });

    it('returns an empty attribute object when there are some set but no icons', async function () {
      let store = this.owner.lookup('service:store');
      let model = await store.createRecord('feature', {
        attributes: {
          icon1: {
            key: 'value',
          },
        },
      });

      expect(model.iconAttributes).to.deep.equal({});
    });

    it('returns the attribute object for an icon if it is matched by name', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', { name: 'icon1' });

      let model = await store.createRecord('feature', {
        icons: [icon],
        attributes: {
          icon1: {
            key: 'value',
          },
        },
      });

      model.icons.isFulfilled = true;

      expect(model.iconAttributes).to.deep.equal({
        icon1: { key: 'value' },
      });
    });

    it('returns the attribute object for an icon if it is matched by one of the other name', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', {
        name: 'icon1',
        otherNames: ['old name 1', 'old name2'],
      });

      let model = await store.createRecord('feature', {
        icons: [icon],
        attributes: {
          'old name 1': {
            key: 'value',
          },
        },
      });

      model.icons.isFulfilled = true;
      expect(model.iconAttributes).to.deep.equal({
        icon1: { key: 'value' },
      });
    });

    it('merges the attributes for an icon if it is matched by other names and name', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', {
        name: 'icon1',
        otherNames: ['old name 1', 'old name 2'],
      });

      let model = await store.createRecord('feature', {
        icons: [icon],
        attributes: {
          'old name 1': {
            key1: 'value1',
          },
          'old name 2': {
            key2: 'value2',
          },
          icon1: {
            key3: 'value3',
          },
        },
      });

      model.icons.isFulfilled = true;
      expect(model.iconAttributes).to.deep.equal({
        icon1: {
          key1: 'value1',
          key2: 'value2',
          key3: 'value3',
        },
      });
    });

    it('merges array properties', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', {
        name: 'icon1',
        allowMany: null,
        otherNames: ['old name 1', 'old name 2'],
      });

      let model = await store.createRecord('feature', {
        icons: [icon],
        attributes: {
          'old name 1': [
            {
              key1: 'value1',
            },
          ],
          'old name 2': [
            {
              key2: 'value2',
            },
          ],
          icon1: [
            {
              key3: 'value3',
            },
          ],
        },
      });

      model.icons.isFulfilled = true;
      expect(model.iconAttributes).to.deep.equal({
        icon1: [{ key1: 'value1' }, { key2: 'value2' }, { key3: 'value3' }],
      });
    });

    it('merges array properties with object properties', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', {
        name: 'icon1',
        allowMany: null,
        otherNames: ['old name 1', 'old name 2'],
      });

      let model = await store.createRecord('feature', {
        icons: [icon],
        attributes: {
          'old name 1': {
            key1: 'value1',
          },
          'old name 2': {
            key2: 'value2',
          },
          icon1: [
            {
              key3: 'value3',
            },
          ],
        },
      });

      model.icons.isFulfilled = true;
      expect(model.iconAttributes).to.deep.equal({
        icon1: [{ key1: 'value1' }, { key2: 'value2' }, { key3: 'value3' }],
      });
    });

    it('merges object properties in an array when the icon has the allow many flag true', async function () {
      let store = this.owner.lookup('service:store');
      let icon = await store.createRecord('icon', {
        name: 'icon1',
        allowMany: true,
        otherNames: ['old name 1', 'old name 2'],
      });

      let model = await store.createRecord('feature', {
        icons: [icon],
        attributes: {
          'old name 1': {
            key1: 'value1',
          },
          'old name 2': {
            key2: 'value2',
          },
          icon1: {
            key3: 'value3',
          },
        },
      });

      model.icons.isFulfilled = true;
      expect(model.iconAttributes).to.deep.equal({
        icon1: [{ key1: 'value1' }, { key2: 'value2' }, { key3: 'value3' }],
      });
    });
  });

  it('fills in the meta info object', async function () {
    let store = this.owner.lookup('service:store');

    let model = store.createRecord('feature', {
      _id: '5e8ab02bd7628301003ac498',
      info: {
        changeIndex: 6,
        createdOn: '2020-04-06T04:29:31Z',
        lastChangeOn: '2020-04-06T04:55:15Z',
      },
      visibility: 1,
      contributors: [],
      maps: [],
      name: 'Parque Fraigcomar',
      icons: [],
      description: 'TBD',
      attributes: { 'Significant Organization or Agency': {} },
      position: {
        type: 'Point',
        coordinates: [-66.08096096379685, 18.38402618625338],
      },
      pictures: [],
    });

    model.pictures.addObject(
      store.createRecord('picture', {
        _id: '5ca89e37ef1f7e010007f333',
        picture: 'http://some-url.com',
      })
    );

    model.info.lastChangeOn = new Date('2021-03-07T17:45:21Z');
    const metaInfo = await model.fillMetaInfo({});

    expect(metaInfo).to.deep.equal({
      title: 'Parque Fraigcomar',
      description: 'TBD',
      imgSrc: 'http://some-url.com/lg',
      date: '2021-03-07T17:45:21.000Z',
    });
  });
});
