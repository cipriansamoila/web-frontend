import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Transform | map file options', function() {
  setupTest();

  // Replace this with your real tests.
  it('exists', function() {
    let transform = this.owner.lookup('transform:map-file-options');
    expect(transform).to.be.ok;
  });
});
