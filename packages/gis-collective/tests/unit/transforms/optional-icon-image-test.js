import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Transform | optional icon image', function() {
  setupTest();

  it('exists', function() {
    let transform = this.owner.lookup('transform:optional-icon-image');
    expect(transform).to.be.ok;
  });

  it('deserialize a null to a optional-icon-image object', function() {
    let transform = this.owner.lookup('transform:optional-icon-image');
    const deserialized = transform.deserialize(null).toJSON();

    expect(deserialized).to.deep.equal({
      useParent: false,
      value: ""
    });
  });
});
