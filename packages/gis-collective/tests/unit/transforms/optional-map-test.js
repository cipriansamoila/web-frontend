import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Transform | optional-map', function() {
  setupTest();

  it('exists', function() {
    let transform = this.owner.lookup('transform:optional-map');
    expect(transform).to.be.ok;
  });

  it('deserialize a null to a optional-map object', function() {
    let transform = this.owner.lookup('transform:optional-map');
    const deserialized = transform.deserialize(null).toJSON();

    expect(deserialized).to.deep.equal({
      isEnabled: false,
      map: ""
    });
  });

  it('deserialize a null map to a optional-map object', function() {
    let transform = this.owner.lookup('transform:optional-map');
    const deserialized = transform.deserialize({map: null}).toJSON();

    expect(deserialized).to.deep.equal({
      isEnabled: false,
      map: ""
    });
  });

  it('deserialize a value to a optional-map object', function() {
    let transform = this.owner.lookup('transform:optional-map');
    const deserialized = transform.deserialize({isEnabled: true, map: "some map"}).toJSON();

    expect(deserialized).to.deep.equal({
      isEnabled: true,
      map: "some map"
    });
  });
});
