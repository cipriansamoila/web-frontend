import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Transform | page col list', function () {
  setupTest();

  let transform;

  beforeEach(function () {
    transform = this.owner.lookup('transform:page-col-list');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);
    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an empty object', function () {
    let deserialized = transform.deserialize({});

    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an object with all fields', function () {
    let deserialized = transform.deserialize([
      {
        options: ['option 1', 'option 2'],
        cols: [
          {
            type: 'type',
            data: {},
            options: ['option 1', 'option 2'],
          },
        ],
      },
    ]);

    expect(deserialized.length).to.deep.equal(1);
  });

  it('can serialize an object', function () {
    let deserialized = transform.deserialize([
      { col: '1', row: '1', type: 'article', data: {} },
    ]);

    expect(transform.serialize(deserialized)).to.deep.equal([
      { container: 0, col: 1, row: 1, type: 'article', data: {} },
    ]);
  });
});
