import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Transform | icon style', function() {
  setupTest();

  it('exists', function() {
    let transform = this.owner.lookup('transform:icon-style');
    expect(transform).to.be.ok;
  });

  it('deserialize a null to a style object', function() {
    let transform = this.owner.lookup('transform:icon-style');
    const deserialized = transform.deserialize(null).toJSON();

    expect(deserialized.site).to.deep.equal({
      isVisible: true, shape: 'circle', borderColor: 'white', backgroundColor: 'rgba(255,255,255,0.5)', borderWidth: 1, size: 21 });

    expect(deserialized.siteLabel).to.deep.equal({
      isVisible: false, text: 'wrap', align: 'center', baseline: 'bottom', weight: 'bold', color: '#000', borderColor: '#fff', borderWidth: 2, size: 10, lineHeight: 1, offsetX: 0, offsetY: 0 });

    expect(deserialized.line).to.deep.equal({
      borderColor: 'blue', backgroundColor: 'transparent', borderWidth: 1, lineDash: [ ] });

    expect(deserialized.lineMarker).to.deep.equal({
      isVisible: true, shape: 'circle', borderColor: 'white', backgroundColor: 'rgba(255,255,255,0.5)', borderWidth: 1, size: 21 });

    expect(deserialized.lineLabel).to.deep.equal({
      isVisible: false, text: 'wrap', align: 'center', baseline: 'bottom', weight: 'bold', color: '#000', borderColor: '#fff', borderWidth: 2, size: 10, lineHeight: 1, offsetX: 0, offsetY: 0 });

    expect(deserialized.polygon).to.deep.equal({
      borderColor: 'blue', backgroundColor: 'transparent', borderWidth: 1, lineDash: [ ], hideBackgroundOnZoom: true });

    expect(deserialized.polygonMarker).to.deep.equal({
      isVisible: true, shape: 'circle', borderColor: 'white', backgroundColor: 'rgba(255,255,255,0.5)', borderWidth: 1, size: 21 });

    expect(deserialized.polygonLabel).to.deep.equal({
      isVisible: false, text: 'wrap', align: 'center', baseline: 'bottom', weight: 'bold', color: '#000', borderColor: '#fff', borderWidth: 2, size: 10, lineHeight: 1, offsetX: 0, offsetY: 0 });
  });

  it('deserialize a plain object with all properties to a style object', function() {
    let transform = this.owner.lookup('transform:icon-style');
    const value = {
      "lineMarker": { isVisible: true, "backgroundColor": "white2", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue" },
      "polygonMarker": { isVisible: true, "backgroundColor": "white3", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue" },
      "site": { isVisible: true, "backgroundColor": "green", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue" },
      "polygon": { hideBackgroundOnZoom: true, borderColor: "polygon blue", backgroundColor: "polygon color", borderWidth: 3, lineDash: [1,2,3] },
      "line": { borderColor: "line blue", backgroundColor: "line color", borderWidth: 2, lineDash: [1,2] }
    };

    const deserialized = transform.deserialize(value).toJSON();

    expect(deserialized.site).to.deep.equal({
      isVisible: true, shape: 'circle', borderColor: 'blue', backgroundColor: 'green', borderWidth: 1, size: 21 });

    expect(deserialized.siteLabel).to.deep.equal({
      isVisible: false, text: 'wrap', align: 'center', baseline: 'bottom', weight: 'bold', color: '#000', borderColor: '#fff', borderWidth: 2, size: 10, lineHeight: 1, offsetX: 0, offsetY: 0 });

    expect(deserialized.line).to.deep.equal({ borderColor: "line blue", backgroundColor: "line color", borderWidth: 2, lineDash: [1,2] });

    expect(deserialized.lineMarker).to.deep.equal({ isVisible: true, "backgroundColor": "white2", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue" });

    expect(deserialized.lineLabel).to.deep.equal({
      isVisible: false, text: 'wrap', align: 'center', baseline: 'bottom', weight: 'bold', color: '#000', borderColor: '#fff', borderWidth: 2, size: 10, lineHeight: 1, offsetX: 0, offsetY: 0 });

    expect(deserialized.polygon).to.deep.equal({ hideBackgroundOnZoom: true, borderColor: "polygon blue", backgroundColor: "polygon color", borderWidth: 3, lineDash: [1,2,3] });

    expect(deserialized.polygonMarker).to.deep.equal({ isVisible: true, "backgroundColor": "white3", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue" });

    expect(deserialized.polygonLabel).to.deep.equal({
      isVisible: false, text: 'wrap', align: 'center', baseline: 'bottom', weight: 'bold', color: '#000', borderColor: '#fff', borderWidth: 2, size: 10, lineHeight: 1, offsetX: 0, offsetY: 0 });
  });
});
