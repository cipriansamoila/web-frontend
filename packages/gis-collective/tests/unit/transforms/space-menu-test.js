import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Transform | space menu', function () {
  setupTest();
  let transform;

  this.beforeEach(function () {
    transform = this.owner.lookup('transform:space-menu');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);
    expect(deserialized.items).to.deep.equal([]);
  });

  it('can deserialize an empty object', function () {
    let deserialized = transform.deserialize({});

    expect(deserialized.items).to.deep.equal([]);
  });

  it('can deserialize an object with all fields', function () {
    let deserialized = transform.deserialize({
      items: [
        {
          link: {
            pageId: '61292c4c7bdf9301008fd7be',
          },
          name: 'About',
          dropDown: [
            {
              name: 'name',
              link: {
                pageId: '61292c4c7bdf9301008fd7be',
              },
            },
          ],
        },
        {
          link: {},
          name: 'Browse',
        },
        {
          link: {},
          name: 'Propose a site',
        },
      ],
    });

    expect(deserialized.items.length).to.deep.equal(3);
  });

  it('can serialize an object', function () {
    const menu = {
      items: [
        {
          link: {
            pageId: '61292c4c7bdf9301008fd7be',
          },
          name: 'About',
          dropDown: [
            {
              name: 'name',
              link: {
                pageId: '61292c4c7bdf9301008fd7be',
              },
            },
          ],
        },
        {
          link: {},
          name: 'Browse',
        },
        {
          link: {},
          name: 'Propose a site',
        },
      ],
    };

    let deserialized = transform.deserialize(menu);

    expect(transform.serialize(deserialized)).to.deep.equal(menu);
  });
});
