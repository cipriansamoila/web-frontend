import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Transform | inherited style', function() {
  setupTest();

  it('exists', function() {
    let transform = this.owner.lookup('transform:inherited-style');
    expect(transform).to.be.ok;
  });

  it('deserialize a null to a style object', function() {
    let transform = this.owner.lookup('transform:inherited-style');
    const deserialized = transform.deserialize(null).toJSON();

    expect(deserialized).to.deep.equal({
      hasCustomStyle: false
    });
  });

  it('deserialize a plain object with all properties to a style object', function() {
    let transform = this.owner.lookup('transform:inherited-style');
    const deserialized = transform.deserialize({
      hasCustomStyle: true,
      types: {
        lineMarker: { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "2" },
        polygonMarker: { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "3" },
        polygon: { hideBackgroundOnZoom: true, "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "4" },
        site: { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "5" },
        line: { borderColor: '6', backgroundColor: 'transparent', borderWidth: 1, "lineDash": [] }
      }
    }).toJSON();

    expect(deserialized.hasCustomStyle).to.equal(true);

    expect(deserialized.types.site).to.deep.equal({ isVisible: true, shape: 'circle', borderColor: '5', backgroundColor: 'white', borderWidth: 1, size: 21 });
    expect(deserialized.types.lineMarker).to.deep.equal({ isVisible: true, shape: 'circle', borderColor: '2', backgroundColor: 'transparent', borderWidth: 1, size: 21 });
    expect(deserialized.types.polygonMarker).to.deep.equal({ isVisible: true, shape: 'circle', borderColor: '3', backgroundColor: 'white', borderWidth: 1, size: 21 });
    expect(deserialized.types.polygon).to.deep.equal({ hideBackgroundOnZoom: true, borderColor: '4', backgroundColor: 'transparent', borderWidth: 1, "lineDash": [] });
    expect(deserialized.types.line).to.deep.equal({ borderColor: '6', backgroundColor: 'transparent', borderWidth: 1, "lineDash": [] });
  });

  it('deserialize a plain object with hasCustomStyle = false and ignores the types', function() {
    let transform = this.owner.lookup('transform:inherited-style');
    const deserialized = transform.deserialize({
      hasCustomStyle: false,
      types: {
        "polygonBorderMarker": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue" },
        "lineMarker": { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue" },
        "polygonMarker": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue" },
        "polygon": { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue" },
        "site": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue" },
        "line": { "backgroundColor": "white", "size": 21, "shape": "circle", "borderWidth": 1, "borderColor": "blue" },
        "polygonBorder": { "backgroundColor": "transparent", "lineDash": [], "borderWidth": 1, "borderColor": "blue" }
      }
    }).toJSON();

    expect(deserialized).to.deep.equal({
      hasCustomStyle: false
    });
  });
});
