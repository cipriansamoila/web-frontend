import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Transform | model list with default', function() {
  setupTest();

  it('exists', function() {
    let transform = this.owner.lookup('transform:model-list-with-default');
    expect(transform).to.be.ok;
  });

  it('deserialize to an object with the default values when the value is a string', function() {
    let transform = this.owner.lookup('transform:model-list-with-default');

    expect(transform.deserialize("", { model: "base-map" }).toJSON()).to.deep.equal({
      useCustomList: false,
      list: []
    });
  });

  it('deserialize to an object with the default values when the value is a null', function() {
    let transform = this.owner.lookup('transform:model-list-with-default');

    expect(transform.deserialize(null, { model: "base-map" }).toJSON()).to.deep.equal({
      useCustomList: false,
      list: []
    });
  });

  it('deserialize to an object with the default values when the value is an empty object', function() {
    let transform = this.owner.lookup('transform:model-list-with-default');

    expect(transform.deserialize(null, { model: "base-map" }).toJSON()).to.deep.equal({
      useCustomList: false,
      list: []
    });
  });

  it('deserialize to an object with the useCustomList=true when the value has useCustomList = true', function() {
    let transform = this.owner.lookup('transform:model-list-with-default');

    expect(transform.deserialize({ useCustomList: true }, { model: "base-map" }).toJSON()).to.deep.equal({
      useCustomList: true,
      list: []
    });
  });

  it('deserialize to an object with the fetched list when the value has a list with an id', async function() {
    let transform = this.owner.lookup('transform:model-list-with-default');
    let store = this.owner.lookup('service:store');
    store.createRecord("base-map", { id: 1 })

    const baseMapList = transform.deserialize({ list: [ 1 ] }, { model: "base-map" });

    expect(baseMapList.list.length).to.equal(1);
    expect(baseMapList.list[0].id).to.equal("1");

    expect(baseMapList.toJSON()).to.deep.equal({
      useCustomList: false,
      list: [ "1" ]
    });
  });

  it('serialize the changed list', async function() {
    let transform = this.owner.lookup('transform:model-list-with-default');
    let store = this.owner.lookup('service:store');

    const baseMapList = transform.deserialize({ list: [ ] }, { model: "base-map" });
    baseMapList.list.addObject(store.createRecord("base-map", { id: 1 }));

    expect(baseMapList.list.length).to.equal(1);
    expect(baseMapList.list[0].id).to.equal("1");

    expect(baseMapList.toJSON()).to.deep.equal({
      useCustomList: false,
      list: [ "1" ]
    });
  });
});
