import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Transform | page container list', function () {
  setupTest();
  let transform;

  // Replace this with your real tests.
  beforeEach(function () {
    transform = this.owner.lookup('transform:page-container-list');
  });

  it('exists', function () {
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function () {
    let deserialized = transform.deserialize(null);
    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an empty object', function () {
    let deserialized = transform.deserialize({});

    expect(deserialized).to.deep.equal([]);
  });

  it('can deserialize an object with all fields', function () {
    let deserialized = transform.deserialize([
      {
        backgroundColor: 'info',
      },
    ]);

    expect(deserialized.length).to.deep.equal(1);
  });

  it('can serialize an object', function () {
    let deserialized = transform.deserialize([{ backgroundColor: 'info' }]);

    expect(transform.serialize(deserialized)).to.deep.equal([
      {
        backgroundColor: 'info',
      },
    ]);
  });
});
