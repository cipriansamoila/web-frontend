import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Transform | cluster', function () {
  setupTest();

  // Replace this with your real tests.
  it('exists', function () {
    let transform = this.owner.lookup('transform:cluster');
    expect(transform).to.be.ok;
  });

  it('deserialize a null to a cluster object', function () {
    let transform = this.owner.lookup('transform:cluster');
    const deserialized = transform.deserialize(null).toJSON();

    expect(deserialized).to.deep.equal({
      mode: 1,
      map: '',
    });
  });

  it('deserialize a cluster object', function () {
    let transform = this.owner.lookup('transform:cluster');
    const value = {
      mode: 2,
      map: 'map',
    };

    const deserialized = transform.deserialize(value).toJSON();

    expect(deserialized).to.deep.equal({
      mode: 2,
      map: 'map',
    });
  });
});
