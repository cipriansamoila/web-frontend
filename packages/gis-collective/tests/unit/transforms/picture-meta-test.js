import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Transform | picture meta', function() {
  setupTest();

  it('exists', function() {
    let transform = this.owner.lookup('transform:picture-meta');
    expect(transform).to.be.ok;
  });

  it('can deserialize a null value', function() {
    let transform = this.owner.lookup('transform:picture-meta');

    let deserialized = transform.deserialize(null);

    expect(deserialized.renderMode).to.equal("");
  });

  it('can deserialize an empty object', function() {
    let transform = this.owner.lookup('transform:picture-meta');

    let deserialized = transform.deserialize({});

    expect(deserialized.renderMode).to.equal("");
  });

  it('can deserialize an object with the renderMode property', function() {
    let transform = this.owner.lookup('transform:picture-meta');

    let deserialized = transform.deserialize({ renderMode: "360"});

    expect(deserialized.renderMode).to.equal("360");
  });

  it('can serialize an object', function() {
    let transform = this.owner.lookup('transform:picture-meta');

    let deserialized = transform.deserialize({ renderMode: "360"});

    expect(transform.serialize(deserialized)).to.deep.equal({
      renderMode: "360"
    });
  });
});
