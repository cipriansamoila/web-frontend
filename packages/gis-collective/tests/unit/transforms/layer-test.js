import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Transform | layer', function() {
  setupTest();

  // Replace this with your real tests.
  it('exists', function() {
    let transform = this.owner.lookup('transform:layer');
    expect(transform).to.be.ok;
  });
});
