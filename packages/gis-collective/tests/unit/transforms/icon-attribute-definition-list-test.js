import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Transform | icon attribute definition list', function () {
  setupTest();

  it('exists', function () {
    let transform = this.owner.lookup('transform:icon-attribute-definition-list');
    expect(transform).to.be.ok;
  });

  it('deserialize to an empty list when the value is a string', function () {
    let transform = this.owner.lookup('transform:icon-attribute-definition-list');
    expect(transform.deserialize("")).to.deep.equal([]);
  });

  it('deserialize to an empty list when the value is undefined', function () {
    let transform = this.owner.lookup('transform:icon-attribute-definition-list');
    expect(transform.deserialize()).to.deep.equal([]);
  });

  it('deserialize to an list with name, type and options when there is an item', function () {
    let transform = this.owner.lookup('transform:icon-attribute-definition-list');

    let item = {
      a: "a",
      name: "name",
      type: "short text",
      options: "",
      isPrivate: false,
      isRequired: false,
    }

    expect(transform.deserialize([item])).to.deep.equal([{
      name: "name",
      displayName: "",
      help: "",
      type: "short text",
      options: "",
      isPrivate: false,
      isRequired: false,
      isInherited: false,
      from: {}
    }]);
  });

  it('serializes to a list with name, type and options when there is an item', function () {
    let transform = this.owner.lookup('transform:icon-attribute-definition-list');

    let item = {
      a: "a",
      name: "name",
      displayName: "local name",
      help: "help",
      type: "short text",
      options: "",
      isPrivate: false,
      isRequired: false,
      from: {}
    }

    const list = transform.deserialize([item]);

    expect(transform.serialize(list)).to.deep.equal([{
      name: "name",
      displayName: "local name",
      help: "help",
      type: "short text",
      options: "",
      isPrivate: false,
      isRequired: false,
      isInherited: false,
      from: {}
    }]);
  });

  it('serializes to an empty list for an undefined', function () {
    let transform = this.owner.lookup('transform:icon-attribute-definition-list');

    expect(transform.serialize()).to.deep.equal([]);
  });
});
