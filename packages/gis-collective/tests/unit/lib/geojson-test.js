import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupTest } from 'ember-mocha';
import GeoJson from 'ogm/lib/geoJson';

describe('Unit | Lib | geojson', function () {
  setupTest();
  let factory;

  beforeEach(function () {
    factory = this.owner.lookup('service:map-styles').styleFactory;
  });

  it('exists', function () {
    expect(factory).to.be.ok;
  });

  it('creates an empty geojson Json', function () {
    let value = new GeoJson();

    expect(value.type).to.equal('Point');
    expect(value.coordinates).to.deep.equal([]);
  });

  describe('getting the center', function () {
    it('should return the same point when is a point', function () {
      let value = new GeoJson({
        type: 'Point',
        coordinates: [9.140625, 50.736455137010665],
      });

      expect(value.center.type).to.equal('Point');
      expect(value.center.coordinates).to.deep.equal([
        9.140625, 50.736455137010665,
      ]);
    });

    it('should return the first element of a multi point', function () {
      let value = new GeoJson({
        type: 'MultiPoint',
        coordinates: [
          [9.140625, 50.736455137010665],
          [1, 2],
        ],
      });

      expect(value.center.type).to.equal('Point');
      expect(value.center.coordinates).to.deep.equal([
        9.140625, 50.736455137010665,
      ]);
    });

    it('should return the middle point of a line', function () {
      let value = new GeoJson({
        type: 'LineString',
        coordinates: [
          [9.140625, 50.736455137010665],
          [1, 2],
          [3, 4],
        ],
      });

      expect(value.center.type).to.equal('Point');
      expect(value.center.coordinates).to.deep.equal([1, 2]);
    });

    it('should return the middle point of a the first line of a multi line', function () {
      let value = new GeoJson({
        type: 'MultiLineString',
        coordinates: [
          [
            [9.140625, 50.736455137010665],
            [1, 2],
            [3, 4],
          ],
        ],
      });

      expect(value.center.type).to.equal('Point');
      expect(value.center.coordinates).to.deep.equal([1, 2]);
    });

    it('should return the center of a polygon extent', function () {
      let value = new GeoJson({
        type: 'Polygon',
        coordinates: [
          [
            [0, 1],
            [1, 2],
            [3, 4],
          ],
        ],
      });

      expect(value.center.type).to.equal('Point');
      expect(value.center.coordinates).to.deep.equal([1.5, 2.5]);
    });

    it('should return the center of a multi polygon extent', function () {
      let value = new GeoJson({
        type: 'MultiPolygon',
        coordinates: [
          [
            [
              [13.088345, 52.4196325],
              [13.0907635, 52.4115602],
              [13.0973918, 52.4094186],
              [13.0958391, 52.421975],
              [13.088345, 52.4196325],
            ],
          ],
          [
            [
              [13.5034382, 52.6189926],
              [13.5037765, 52.6192121],
              [13.5034382, 52.6189926],
            ],
          ],
        ],
      });

      expect(value.center.type).to.equal('Point');
      expect(value.center.coordinates).to.deep.equal([
        13.29606075, 52.514315350000004,
      ]);
    });
  });
});
