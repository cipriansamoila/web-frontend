import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Lib | map/style-factory', function() {
  setupTest();
  let factory;

  beforeEach(function() {
    factory = this.owner.lookup('service:map-styles').styleFactory;
  });

  it('exists', function() {
    expect(factory).to.be.ok;
  });

  it('creates a shape style', function() {
    let style = factory.shape({
      borderColor: "blue",
      lineDash: [1,2],
      borderWidth: 2,
      backgroundColor: "red"
    });

    expect(style.getStroke().getColor()).to.equal("blue");
    expect(style.getStroke().getLineDash()).to.deep.equal([1,2]);
    expect(style.getStroke().getWidth()).to.equal(2);
    expect(style.getFill().getColor()).to.equal("red");
    expect(Object.keys(factory.cache.styles)).to.deep.equal([]);
  });

  it('caches a shape style by id', function() {
    let style1 = factory.shape({
      id: "test",
      borderColor: "blue",
      lineDash: [1,2],
      borderWidth: 2,
      backgroundColor: "red"
    });

    let style2 = factory.shape({
      id: "test"
    });

    expect(style1).to.equal(factory.cache.styles["shape.test"]);
    expect(style2).to.equal(style1);
  });

  it('creates a regularShape style', function() {
    let style = factory.regularShape({
      borderColor: "blue",
      lineDash: [1,2],
      borderWidth: 2,
      backgroundColor: "red"
    });

    expect(style.getStroke().getColor()).to.equal("blue");
    expect(style.getStroke().getLineDash()).to.deep.equal([1,2]);
    expect(style.getStroke().getWidth()).to.equal(2);
    expect(style.getFill().getColor()).to.equal("red");

    expect(style.getPoints()).to.equal(4);
    expect(style.getRotation()).to.equal(0);
    expect(style.getRadius()).to.equal(factory.defaultRadius);

    expect(Object.keys(factory.cache.styles)).to.deep.equal([]);
  });

  it('caches a regularShape style by id', function() {
    let style1 = factory.regularShape({
      id: "test",
      borderColor: "blue",
      lineDash: [1,2],
      borderWidth: 2,
      backgroundColor: "red"
    });

    let style2 = factory.regularShape({
      id: "test"
    });

    expect(style1).to.equal(factory.cache.styles["regularShape.4.0.test"]);
    expect(style2).to.equal(style1);
  });

  it('creates a square style', function() {
    let style = factory.square({
      borderColor: "blue",
      lineDash: [1,2],
      borderWidth: 2,
      backgroundColor: "red",
      size: 50
    });

    expect(style.getStroke().getColor()).to.equal("blue");
    expect(style.getStroke().getLineDash()).to.deep.equal([1,2]);
    expect(style.getStroke().getWidth()).to.equal(2);
    expect(style.getFill().getColor()).to.equal("red");

    expect(style.getPoints()).to.equal(4);
    expect(style.getRotation()).to.equal(2.356194490192345);
    expect(style.getRadius()).to.equal(50);

    expect(Object.keys(factory.cache.styles)).to.deep.equal([]);
  });

  it('creates a pentagon style', function() {
    let style = factory.pentagon({
      borderColor: "green",
      lineDash: [1,2],
      borderWidth: 2,
      backgroundColor: "pink",
      size: 40
    });

    expect(style.getStroke().getColor()).to.equal("green");
    expect(style.getStroke().getLineDash()).to.deep.equal([1,2]);
    expect(style.getStroke().getWidth()).to.equal(2);
    expect(style.getFill().getColor()).to.equal("pink");

    expect(style.getPoints()).to.equal(5);
    expect(style.getRotation()).to.equal(6.283185307179586);
    expect(style.getRadius()).to.equal(40);

    expect(Object.keys(factory.cache.styles)).to.deep.equal([]);
  });

  it('creates a hexagon style', function() {
    let style = factory.hexagon({
      borderColor: "green",
      lineDash: [1,2],
      borderWidth: 2,
      backgroundColor: "pink",
      size: 40
    });

    expect(style.getStroke().getColor()).to.equal("green");
    expect(style.getStroke().getLineDash()).to.deep.equal([1,2]);
    expect(style.getStroke().getWidth()).to.equal(2);
    expect(style.getFill().getColor()).to.equal("pink");

    expect(style.getPoints()).to.equal(6);
    expect(style.getRotation()).to.equal(1.5707963267948966);
    expect(style.getRadius()).to.equal(40);

    expect(Object.keys(factory.cache.styles)).to.deep.equal([]);
  });

  it('creates a circle style', function() {
    let style = factory.circle({
      borderColor: "green",
      borderWidth: 2,
      backgroundColor: "pink",
      lineDash: [1,2,3],
      size: 40
    });

    expect(style.getStroke().getColor()).to.equal("green");
    expect(style.getStroke().getLineDash()).to.deep.equal([1,2,3]);
    expect(style.getStroke().getWidth()).to.equal(2);
    expect(style.getFill().getColor()).to.equal("pink");
    expect(style.getRadius()).to.equal(40);

    expect(Object.keys(factory.cache.styles)).to.deep.equal([]);
  });

  it('caches a circle style by id', function() {
    let style1 = factory.circle({
      id: "test",
      borderColor: "blue",
      lineDash: [1,2],
      borderWidth: 2,
      backgroundColor: "red"
    });

    let style2 = factory.circle({
      id: "test"
    });

    expect(style1).to.equal(factory.cache.styles["circle.test"]);
    expect(style2).to.equal(style1);
  });

  it('creates a square icon frame', function() {
    let style = factory.iconFrame({
      shape: "square",
      id: "test",
      borderColor: "blue",
      lineDash: [1,2],
      borderWidth: 2,
      backgroundColor: "red"
    });

    expect(style.getImage().getPoints()).to.equal(4);
  });

  it('creates a pentagon icon frame', function() {
    let style = factory.iconFrame({
      shape: "pentagon",
      id: "test",
      borderColor: "blue",
      lineDash: [1,2],
      borderWidth: 2,
      backgroundColor: "red"
    });

    expect(style.getImage().getPoints()).to.equal(5);
  });

  it('creates a hexagon icon frame', function() {
    let style = factory.iconFrame({
      shape: "hexagon",
      id: "test",
      borderColor: "blue",
      lineDash: [1,2],
      borderWidth: 2,
      backgroundColor: "red"
    });

    expect(style.getImage().getPoints()).to.equal(6);
  });

  it('creates a circle icon frame', function() {
    let style = factory.iconFrame({
      shape: "circle",
      id: "test",
      borderColor: "blue",
      lineDash: [1,2],
      borderWidth: 2,
      backgroundColor: "red"
    });

    expect(style.getImage().getPoints()).to.equal(Infinity);
  });

  it('creates a circle icon frame for undefined shapes', function() {
    let style = factory.iconFrame({
      id: "test",
      borderColor: "blue",
      lineDash: [1,2],
      borderWidth: 2,
      backgroundColor: "red"
    });

    expect(style.getImage().getPoints()).to.equal(Infinity);
  });

  it('caches an icon frame style by id', function() {
    let style1 = factory.iconFrame({
      id: "test",
      borderColor: "blue",
      lineDash: [1,2],
      borderWidth: 2,
      backgroundColor: "red"
    });

    let style2 = factory.iconFrame({
      id: "test"
    });

    expect(style1).to.equal(factory.cache.styles["iconFrame.test"]);
    expect(style2).to.equal(style1);
  });
});
