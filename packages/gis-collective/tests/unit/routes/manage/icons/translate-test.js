import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | manage/icons/translate', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:manage/icons/translate');
    expect(route).to.be.ok;
  });
});
