import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | manage/languages/edit', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:manage/languages/edit');
    expect(route).to.be.ok;
  });
});
