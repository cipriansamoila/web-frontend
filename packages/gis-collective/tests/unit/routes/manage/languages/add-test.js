import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | manage/languages/add', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:manage/languages/add');
    expect(route).to.be.ok;
  });
});
