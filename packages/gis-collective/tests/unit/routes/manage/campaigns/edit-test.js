import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | manage/campaigns/edit', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:manage/campaigns/edit');
    expect(route).to.be.ok;
  });
});
