import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | manage/pages/layout', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:manage/pages/layout');
    expect(route).to.be.ok;
  });
});
