import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | manage/pages/edit', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:manage/pages/edit');
    expect(route).to.be.ok;
  });
});
