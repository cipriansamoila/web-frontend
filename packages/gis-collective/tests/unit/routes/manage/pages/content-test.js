import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | manage/pages/content', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:manage/pages/content');
    expect(route).to.be.ok;
  });
});
