import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | browse/icons/index', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:browse/icons/index');
    expect(route).to.be.ok;
  });
});
