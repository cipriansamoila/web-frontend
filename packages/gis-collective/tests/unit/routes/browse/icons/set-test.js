import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | browse/icons/set', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:browse/icons/set');
    expect(route).to.be.ok;
  });
});
