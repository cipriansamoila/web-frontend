import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | browse/profiles/profile', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:browse/profiles/profile');
    expect(route).to.be.ok;
  });
});
