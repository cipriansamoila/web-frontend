import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | browse/maps/map-view/index', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:browse/maps/map-view/index');
    expect(route).to.be.ok;
  });
});
