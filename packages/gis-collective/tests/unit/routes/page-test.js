import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | page', function () {
  setupTest();

  it('exists', function () {
    let route = this.owner.lookup('route:page');
    expect(route).to.be.ok;
  });

  it('ignores the query params from page query', function () {
    let route = this.owner.lookup('route:page');
    expect(route.toPageSlug('help?lang=2')).to.equal('help');
  });

  it('converts the sashes to double dashes', async function () {
    let route = this.owner.lookup('route:page');
    expect(route.toPageSlug('a/b/c')).to.equal('a--b--c');
  });
});
