import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | login/index', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:login/index');
    expect(route).to.be.ok;
  });
});
