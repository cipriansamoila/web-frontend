import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | campaigns/success', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:campaigns/success');
    expect(route).to.be.ok;
  });
});
