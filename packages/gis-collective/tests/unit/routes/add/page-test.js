import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | add/page', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:add/page');
    expect(route).to.be.ok;
  });
});
