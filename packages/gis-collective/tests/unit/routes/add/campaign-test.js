import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | add/campaign', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:add/campaign');
    expect(route).to.be.ok;
  });
});
