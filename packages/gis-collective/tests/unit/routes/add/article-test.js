import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | add/article', function () {
  setupTest();

  it('exists', function () {
    let route = this.owner.lookup('route:add/article');
    expect(route).to.be.ok;
  });
});
