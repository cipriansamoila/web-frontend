import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | application', function () {
  setupTest();

  it('exists', function () {
    let route = this.owner.lookup('route:application');
    expect(route).to.be.ok;
  });

  describe('error handling', function () {
    let lastError;
    let route;
    let routeName;

    beforeEach(function () {
      lastError = this.owner.lookup('service:last-error');
      route = this.owner.lookup('route:application');

      routeName = '';
      route.intermediateTransitionTo = (name) => {
        routeName = name;
      };

      lastError.message = '';
      lastError.offerLogin = false;
    });

    it('handles an error', function () {
      route.error({
        message: 'some message',
      });

      expect(lastError.message).to.equal('some message');
      expect(lastError.offerLogin).to.equal(false);
      expect(routeName).to.equal('error');
    });

    it("ignores an 'Aborted' DOM error", function () {
      route.error(new DOMException('Aborted', 'AbortError'));

      expect(lastError.message).to.equal('');
      expect(lastError.offerLogin).to.equal(false);
      expect(routeName).not.to.equal('error');
    });

    it('redirects to a page not found when the adapter returns 404', function () {
      const error = {
        isAdapterError: true,
        fileName: 'http://localhost:4200/assets/vendor.js',
        lineNumber: 99812,
        message:
          'Ember Data Request GET http://192.168.100.138:9091/features/5ca8b355e returned a 404',
        name: 'Error',
        errors: [
          {
            description: 'Invalid id `5ca8b355e` inside `sites`',
            status: 404,
            title: 'Crate not found',
          },
        ],
      };

      route.error(error);

      expect(lastError.message).to.equal('');
      expect(lastError.offerLogin).to.equal(false);
      expect(routeName).to.equal('/page');
    });

    it('ignores an adapter error with status 0', function () {
      const error = {
        isAdapterError: true,
        fileName: 'http://localhost:4200/assets/vendor.js',
        lineNumber: 99812,
        message: '',
        name: 'Error',
        errors: [
          {
            description: '',
            status: 0,
            title: 'Error',
          },
        ],
      };

      route.error(error);

      expect(lastError.message).to.equal('');
      expect(lastError.offerLogin).to.equal(false);
      expect(routeName).not.to.equal('error');
    });
  });
});
