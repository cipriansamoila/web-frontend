import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | admin/location-services', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:admin/location-services');
    expect(route).to.be.ok;
  });
});
