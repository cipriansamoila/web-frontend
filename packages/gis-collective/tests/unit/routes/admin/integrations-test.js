import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | admin/integrations', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:admin/integrations');
    expect(route).to.be.ok;
  });
});
