import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Route | wizard/import/file-preview', function() {
  setupTest();

  it('exists', function() {
    let route = this.owner.lookup('route:wizard/import/file-preview');
    expect(route).to.be.ok;
  });
});
