import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Service | position', function() {
  setupTest();

  let service;
  let position;

  beforeEach(function() {
    service = this.owner.lookup('service:position');
    position = {
      coords: {
        latitude: 12,
        longitude: 13,
        accuracy: 14,
        altitude: 15,
        altitudeAccuracy: 16
      }
    };
  });

  // Replace this with your real tests.
  it('exists', function() {
    expect(service).to.be.ok;
  });

  describe("when it is in fastboot mode", function() {
    beforeEach(function() {
      service.fastboot.isFastBoot = true;
    });

    it("should not have geolocation support", function() {
      expect(service.hasGeolocationSupport).to.equal(false);
    });

    it("should return NaN for all position properties", function() {
      expect(service.latitude).to.be.NaN;
      expect(service.longitude).to.be.NaN;
      expect(service.accuracy).to.be.NaN;
      expect(service.altitude).to.be.NaN;
      expect(service.altitudeAccuracy).to.be.NaN;
    });

    it("should return false for all accuracy values", function() {
      expect(service.isAccurate).to.equal(false);
      expect(service.isAlmostAccurate).to.equal(false);
      expect(service.isNotAccurate).to.equal(false);
    });

    it("should return null as the center", function() {
      expect(service.center).to.equal(null);
    });

    it("should return an error on watching the position", async function() {
      let error = {};

      try {
        await service.watchPosition();
      } catch(err) {
        error = err;
      }

      expect(error.message).to.equal(`Your device does not support geo location.`);
    });

    it("should not have position", function() {
      expect(service.hasPosition).to.equal(false);
    });

    it("has the status NOT_AVAILABLE", async function() {
      expect(service.status).to.equal("PENDING");
      try {
        await service.watchPosition();
      // eslint-disable-next-line no-empty
      } catch(err) {}
      expect(service.status).to.equal("NOT_AVAILABLE");
    });
  });

  describe("when there is no geolocation support", function() {
    beforeEach(function() {
      service.geolocation = null;
    });

    it("should not have geolocation support", function() {
      expect(service.hasGeolocationSupport).to.equal(false);
    });

    it("should return NaN for all position properties", function() {
      expect(service.latitude).to.be.NaN;
      expect(service.longitude).to.be.NaN;
      expect(service.accuracy).to.be.NaN;
      expect(service.altitude).to.be.NaN;
      expect(service.altitudeAccuracy).to.be.NaN;
    });

    it("should return false for all accuracy values", function() {
      expect(service.isAccurate).to.equal(false);
      expect(service.isAlmostAccurate).to.equal(false);
      expect(service.isNotAccurate).to.equal(false);
    });

    it("should return null as the center", function() {
      expect(service.center).to.equal(null);
    });

    it("should return an error on watching the position", async function() {
      let error = {};

      try {
        await service.watchPosition();
      } catch(err) {
        error = err;
      }

      expect(error.message).to.equal(`Your device does not support geo location.`);
    });

    it("should not have position", function() {
      expect(service.hasPosition).to.equal(false);
    });

    it("has the status NOT_AVAILABLE", async function() {
      expect(service.status).to.equal("PENDING");
      try {
        await service.watchPosition();
      // eslint-disable-next-line no-empty
      } catch(err) {}
      expect(service.status).to.equal("NOT_AVAILABLE");
    });
  });

  describe("when there is a geolocation error", function() {
    beforeEach(function() {
      service.geolocation = {
        watchPosition(_, error) {
          error({
            code: 1,
            message: "Some error message"
          });
        },
        clearWatch() {}
      };
    });

    it("has the status NOT_AVAILABLE", async function() {
      expect(service.status).to.equal("PENDING");
      try {
        await service.watchPosition();
      // eslint-disable-next-line no-empty
      } catch(err) {}
      expect(service.status).to.equal("NOT_AVAILABLE");
    });

    it("should have geolocation support", function() {
      expect(service.hasGeolocationSupport).to.equal(true);
    });

    it("should return NaN for all position properties", function() {
      expect(service.latitude).to.be.NaN;
      expect(service.longitude).to.be.NaN;
      expect(service.accuracy).to.be.NaN;
      expect(service.altitude).to.be.NaN;
      expect(service.altitudeAccuracy).to.be.NaN;
    });

    it("should return false for all accuracy values", function() {
      expect(service.isAccurate).to.equal(false);
      expect(service.isAlmostAccurate).to.equal(false);
      expect(service.isNotAccurate).to.equal(false);
    });

    it("should return null as the center", function() {
      expect(service.center).to.equal(null);
    });

    it("should return the new position when watch does not return a new error", async function() {
      try {
        await service.watchPosition();
      } catch(err) {
        expect(err).to.equal("Your location is not available due to an unexpected error.")
      }

      await expect(service.status).to.equal("NOT_AVAILABLE");
    });

    it("should not have position", function() {
      expect(service.hasPosition).to.equal(false);
    });
  });

  describe("when there is a valid geolocation", function() {
    beforeEach(function() {
      service.reset();
      service.geolocation = {
        watchPosition(callback) {
          callback(position);

          return 1;
        }
      };
    });

    it("should have geolocation support", function() {
      expect(service.hasGeolocationSupport).to.equal(true);
    });

    it("should return NaN for all position properties before watch", function() {
      expect(service.latitude).to.be.NaN;
      expect(service.longitude).to.be.NaN;
      expect(service.accuracy).to.be.NaN;
      expect(service.altitude).to.be.NaN;
      expect(service.altitudeAccuracy).to.be.NaN;
    });

    it("should update the values", async function() {
      let callback = null;

      service.geolocation.watchPosition = (c) => {
        callback = c;

        return 1;
      }

      service.watchPosition().then(() => {});

      await callback(position);
      expect(service.latitude).to.equal(12);
      expect(service.longitude).to.equal(13);
      expect(service.accuracy).to.equal(14);
      expect(service.altitude).to.equal(15);
      expect(service.altitudeAccuracy).to.equal(16);

      await callback({ coords: {
        latitude: 22,
        longitude: 23,
        accuracy: 24,
        altitude: 25,
        altitudeAccuracy: 26
      }});
      expect(service.latitude).to.equal(22);
      expect(service.longitude).to.equal(23);
      expect(service.accuracy).to.equal(24);
      expect(service.altitude).to.equal(25);
      expect(service.altitudeAccuracy).to.equal(26);
    });

    it("should not have position before watch is called", function() {
      expect(service.hasPosition).to.equal(false);
    });

    it("should have position after watch is called", async function() {
      await service.watchPosition();
      expect(service.hasPosition).to.equal(true);
    });
  });
});
