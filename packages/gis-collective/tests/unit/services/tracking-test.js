import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Service | tracking', function () {
  setupTest();

  it('exists', function () {
    let service = this.owner.lookup('service:tracking');
    expect(service).to.be.ok;
  });
});
