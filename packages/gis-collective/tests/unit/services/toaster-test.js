import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Service | toaster', function() {
  setupTest();

  // Replace this with your real tests.
  it('exists', function() {
    let service = this.owner.lookup('service:toaster');
    expect(service).to.be.ok;
  });

  it('can add two toasts', function() {
    let service = this.owner.lookup('service:toaster');

    service.addToast({});
    service.addToast({});

    expect(service.toasts).to.deep.equal([{index:0}, {index:1}]);
  });
});
