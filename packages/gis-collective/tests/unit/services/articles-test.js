import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Service | articles', function() {
  setupTest();

  it('exists', function() {
    let service = this.owner.lookup('service:articles');
    expect(service).to.be.ok;
  });

  it('returns false when is not admin and the article is private', async function() {
    let service = this.owner.lookup('service:articles');
    service.user = {
      isAdmin: false
    }
    service.store = {
      findRecord: async () => {
        return { visibility: { isPublic: false }}
      }
    }

    const result = await service.checkAbout();

    expect(result).to.equal(false);
    expect(service.hasAbout).to.equal(false);
  });

  it('returns true when is admin', async function() {
    let service = this.owner.lookup('service:articles');
    service.fastboot = { isFastBoot: false }
    service.user = {
      isAdmin: true
    }
    service.store = {
      findRecord: async () => {
        return { visibility: { isPublic: false }}
      }
    }

    const result = await service.checkAbout();

    expect(result).to.equal(true);
    expect(service.hasAbout).to.equal(true);
  });

  it('returns true when is admin the article is not found', async function() {
    let service = this.owner.lookup('service:articles');

    service.fastboot = { isFastBoot: false }
    service.user = {
      isAdmin: true
    }
    service.store = {
      findRecord: async () => {
        throw Error();
      }
    }

    const result = await service.checkAbout();

    expect(result).to.equal(true);
    expect(service.hasAbout).to.equal(true);
  });

  it('returns true when is not admin and the article is public', async function() {
    let service = this.owner.lookup('service:articles');
    service.user = {
      isAdmin: false
    }
    service.store = {
      findRecord: async () => {
        return { visibility: { isPublic: true }}
      }
    }
    service.fastboot = { isFastBoot: false }

    const result = await service.checkAbout();

    expect(result).to.equal(true);
    expect(service.hasAbout).to.equal(true);
  });

  it('returns true when is true in the shoebox', async function() {
    let service = this.owner.lookup('service:articles');
    service.user = {
      isAdmin: false
    }
    service.fastboot = {
      isFastBoot: false,
      shoebox: {
        retrieve: () => ({
          aboutVisible: true
        })
      }
    }

    const result = await service.checkAbout();

    expect(result).to.equal(true);
    expect(service.hasAbout).to.equal(true);
  });

  it('stores the value in the shoebox', async function() {
    let service = this.owner.lookup('service:articles');
    service.user = {
      isAdmin: false
    }
    service.store = {
      findRecord: async () => {
        return { visibility: { isPublic: true }}
      }
    }
    let savedKey;
    let savedValue;
    service.fastboot = { isFastBoot: true, shoebox: { put: (a, b) => {
      savedKey = a;
      savedValue = b;
    }}};

    await service.checkAbout();

    expect(savedKey).to.equal("articles-store");
    expect(savedValue).to.deep.equal({ aboutVisible: true });
  });
});
