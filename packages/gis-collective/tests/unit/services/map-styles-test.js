import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Service | map-styles', function() {
  setupTest();

  it('exists', function() {
    let service = this.owner.lookup('service:map-styles');
    expect(service).to.be.ok;
  });
});
