import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';
import TestServer from '../../helpers/test-server';

describe('Unit | Service | space', function () {
  let service;
  let server;
  let space;

  setupTest();

  this.beforeEach(function () {
    server = new TestServer();
    service = this.owner.lookup('service:space');
    space = server.testData.storage.addDefaultSpace();
  });

  it('exists', function () {
    expect(service).to.be.ok;
    expect(service.currentSpace).not.to.exist;
  });

  it('loads the default space on setup', async function () {
    await service.setup();
    expect(service.currentSpace?.id).to.equal(space._id);
  });
});
