import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Controller | browse/maps/map-view', function() {
  setupTest();

  // TODO: Replace this with your real tests.
  it('exists', function() {
    let controller = this.owner.lookup('controller:browse/maps/map-view');
    expect(controller).to.be.ok;
  });

  it('sets the default map extent if no value set in preferences', function() {
    let controller = this.owner.lookup('controller:browse/maps/map-view');

    let worldExtent = [-118, -28, 37, 65];
    expect(controller.worldExtent.length).to.equal(4);
    controller.worldExtent.map((a, i) => expect(a == worldExtent[i]).to.be.true);
  });

  it('sets the map extent from preferences by default', function() {
    let controller = this.owner.lookup('controller:browse/maps/map-view');
    let preferenceMapExtent = [0,0,1,1];
    controller.preferences.appearanceMapExtent = preferenceMapExtent;
    controller.worldExtent.map((a, i) => expect(a == preferenceMapExtent[i]).to.be.true);
  });
});
