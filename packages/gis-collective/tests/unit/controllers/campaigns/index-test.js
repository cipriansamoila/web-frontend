import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Controller | campaigns/index', function () {
  setupTest();

  it('exists', function () {
    let controller = this.owner.lookup('controller:campaigns/index');
    expect(controller).to.be.ok;
  });
});
