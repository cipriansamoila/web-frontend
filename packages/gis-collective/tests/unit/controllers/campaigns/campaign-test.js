import { expect } from 'chai';
import { describe, it, beforeEach } from 'mocha';
import { setupTest } from 'ember-mocha';
import GeoJson from 'ogm/lib/geoJson';

describe('Unit | Controller | campaigns/campaign', function () {
  setupTest();
  let controller;

  beforeEach(function () {
    controller = this.owner.lookup('controller:campaigns/campaign');
    controller.model = { answer: { attributes: {} } };
  });

  describe('changePosition', function () {
    let model;
    let query;
    let results;

    beforeEach(function () {
      model = null;
      query = null;
      results = [];
      controller.store = {
        query: function (m, q) {
          model = m;
          query = q;

          return results;
        },
      };

      controller.geocodingSearch.store = controller.store;
    });

    it('performs a search when searchAddressTerm is empty', async function () {
      results = [
        {
          name: 'name',
          geometry: {},
        },
      ];
      let position = new GeoJson({
        type: 'Point',
        coordinates: [9.140625, 50.736455137010665],
      });
      let details = { searchTerm: 'term' };

      await controller.changePosition(position, details);

      expect(model).to.equal('geocoding');
      expect(query).to.deep.equal({ query: 'term' });
      expect(controller.geocodingSearch.searchAddressTerm).to.equal('term');
      expect(controller.geocodingSearch.results.length).to.equal(1);
    });

    it('does not query geocoding when searchAddressTerm equals to the search term', async function () {
      results = [
        {
          name: 'name',
          geometry: {},
        },
      ];
      let position = new GeoJson({
        type: 'Point',
        coordinates: [9.140625, 50.736455137010665],
      });
      let details = { searchTerm: 'term' };

      controller.geocodingSearch.searchAddressTerm = 'term';
      await controller.changePosition(position, details);

      expect(model).to.equal(null);
    });

    it('does not trigger on error when no results are returned', async function () {
      results = [];
      let position = new GeoJson({
        type: 'Point',
        coordinates: [9.140625, 50.736455137010665],
      });
      let details = { searchTerm: 'term' };

      await controller.changePosition(position, details);

      expect(controller.geocodingSearch.results.length).to.equal(0);
    });
  });

  describe('required icon attributes validation', function () {
    it("sets the submit button as disabled when the required attribute is not set", function () {
      controller.model = {
        answer: {
          icons: [{
            name: "icon1",
            attributes: [{ name: "phone", isRequired: true }]
          }],
          attributes: {}
        }
      }

      controller.checkRequiredAttributes();

      expect(controller.submitDisabled).to.equal(true);
    });

    it("sets the submit button as disabled when a second required attribute is not set", function () {
      controller.model = {
        answer: {
          icons: [{
            name: "icon1",
            attributes: [{ name: "phone", isRequired: true }, { name: "program", isRequired: true }]
          }],
          attributes: { icon1: { phone: "232332", program: "" } }
        }
      }

      controller.checkRequiredAttributes();

      expect(controller.submitDisabled).to.equal(true);
    });

    it("sets the submit button as enabled when the required attributes are set", function () {
      controller.model = {
        answer: {
          icons: [{
            name: "icon1",
            attributes: [{ name: "phone", isRequired: true }, { name: "program", isRequired: true }]
          }],
          attributes: { icon1: { phone: "12334", program: "Luni-Vineri:9-18" } }
        }
      }

      controller.checkRequiredAttributes();

      expect(controller.submitDisabled).to.equal(false);
    });

    it("sets the submit button as disabled when one instance of a required attribute of an icon is not set", function() {
      controller.model = {
        answer: {
          icons: [{
            allowMany: true,
            name: "icon1",
            attributes: [{ name: "phone", isRequired: true }, { name: "program", isRequired: true }]
          }],
          attributes: { icon1: [{ phone: "", program: "" }, {phone: "232332", program: "Luni-Vineri:9-18"}] }
        }
      }

      controller.checkRequiredAttributes();

      expect(controller.submitDisabled).to.equal(true);
    });

  });
});
