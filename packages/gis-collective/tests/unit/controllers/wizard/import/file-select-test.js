import { expect } from 'chai';
import { describe, it } from 'mocha';
import { setupTest } from 'ember-mocha';

describe('Unit | Controller | wizard/import/file-select', function () {
  setupTest();

  it('exists', function () {
    let controller = this.owner.lookup('controller:wizard/import/file-select');
    expect(controller).to.be.ok;
  });
});
