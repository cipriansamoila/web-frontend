export default class TestData {
  constructor(server) {
    this.server = server;

    this.plurals = {
      areas: 'areas',
      articles: 'articles',
      basemaps: 'baseMaps',
      campaigns: 'campaigns',
      features: 'features',
      geocodings: 'geocodings',
      icons: 'icons',
      iconsets: 'iconSets',
      layouts: 'layouts',
      legends: 'legends',
      mapfiles: 'mapFiles',
      maps: 'maps',
      models: 'models',
      pages: 'pages',
      presentations: 'presentations',
      pictures: 'pictures',
      preferences: 'preferences',
      sounds: 'sounds',
      teams: 'teams',
      spaces: 'spaces',
      translations: 'translations',
      userprofiles: 'userProfiles',
      users: 'users',
    };

    this.singulars = {
      areas: 'area',
      articles: 'article',
      basemaps: 'baseMap',
      campaigns: 'campaign',
      features: 'feature',
      geocodings: 'geocoding',
      icons: 'icon',
      iconsets: 'iconSet',
      layouts: 'layout',
      legends: 'legend',
      mapfiles: 'mapFile',
      maps: 'map',
      models: 'model',
      pages: 'page',
      spaces: 'space',
      presentations: 'presentation',
      pictures: 'picture',
      preferences: 'preference',
      sounds: 'sound',
      teams: 'team',
      translations: 'translation',
      userprofiles: 'userProfile',
      users: 'user',
    };

    this.create = new Factory(this);
    this.storage = new Storage(this);
    Object.keys(this.plurals)
      .map((a) => this.plurals[a])
      .forEach((a) => {
        this.storage[a] = {};
      });
  }
}

class Factory {
  feature(_id = '5ca78e2160780601008f69e6') {
    return {
      _id,
      info: {
        changeIndex: 3,
        createdOn: '2009-10-31T00:30:00Z',
        lastChangeOn: '2009-10-31T00:30:00Z',
        originalAuthor: '5b8a59caef739394031a3f67',
        author: '5b8a59caef739394031a3f67',
      },
      contributors: [],
      maps: ['5ca89e37ef1f7e010007f54c'],
      name: 'Nomadisch Grün - Local Urban Food',
      isMasked: false,
      visibility: 1,
      icons: ['5ca7bfc0ecd8490100cab980'],
      description: 'some description',
      attributes: {},
      position: {
        type: 'Point',
        coordinates: [13.433576, 52.495781],
      },
      pictures: ['5cc8dc1038e882010061545a'],
    };
  }

  article(_id = '5ca78e2160780601008f69e6') {
    return {
      _id,
      info: {
        changeIndex: 2,
        createdOn: '0001-01-01T00:00:00Z',
        lastChangeOn: '2020-02-02T19:31:07Z',
        originalAuthor: '',
        author: '5b8a59caef739394031a3f67',
      },
      slug: _id,
      title: 'some title',
      content: '# title\nsome content',
      visibility: {
        isDefault: false,
        isPublic: true,
        team: '5ca78e2160780601008f69e6',
      },
    };
  }

  user(_id = '5b8a59caef739394031a3f67') {
    return {
      _id,
      email: 'contact@szabobogdan.com',
      username: 'gedaiu',
      isAdmin: false,
      name: 'gedaiu',
    };
  }

  layout(_id = '000000000000000000000001') {
    return {
      _id,
      name: 'test',
      containers: [
        {
          options: [],
          rows: [
            {
              options: [],
              cols: [
                {
                  type: 'type',
                  data: {},
                  options: [],
                },
                {
                  type: 'type',
                  data: {},
                  options: [],
                },
              ],
            },
          ],
        },
      ],
    };
  }

  space(_id = '6227c131624b2cf1626dd029') {
    return {
      _id,
      menu: {
        items: [
          {
            link: {
              pageId: '61292c4c7bdf9301008fd7be',
            },
            name: 'About',
          },
          {
            link: {
              pageId: '61292c4c7bdf9301008fd7bf',
            },
            name: 'Browse',
          },
          {
            link: {
              pageId: '61292c4c7bdf9301008fd7bg',
            },
            name: 'Propose a site',
          },
        ],
      },
      visibility: {
        isDefault: true,
        isPublic: true,
        team: '61292c4c7bdf9301008fd7b6',
      },
      info: {
        changeIndex: 0,
        createdOn: '2022-03-08T21:48:49.1978447',
        lastChangeOn: '2022-03-08T21:48:49.1978572',
        author: '',
      },
      canEdit: true,
      name: '',
    };
  }

  page(_id = '000000000000000000000001') {
    return {
      _id,
      name: 'test',
      slug: 'page--test',
      layout: '000000000000000000000001',
      visibility: {
        isDefault: false,
        isPublic: true,
        team: '5ca88c99ecd8490100caba50',
      },
      info: {
        changeIndex: 0,
        createdOn: '2020-12-19T14:09:09Z',
        lastChangeOn: '2020-12-19T14:09:09Z',
        originalAuthor: '5b870669796da25424540deb',
        author: '5b870669796da25424540deb',
      },
      cols: [
        {
          container: 0,
          col: 0,
          row: 0,
          type: 'article',
          data: { id: '5ca78e2160780601008f69e6' },
        },
        {
          container: 0,
          col: 1,
          row: 0,
          type: 'picture',
          data: {
            id: '5cc8dc1038e882010061545a',
          },
        },
      ],
    };
  }

  presentation(_id = '000000000000000000000001') {
    return {
      _id,
      name: 'test',
      slug: 'presentation--test',
      layout: '000000000000000000000001',
      visibility: {
        isDefault: false,
        isPublic: true,
        team: '5ca88c99ecd8490100caba50',
      },
      info: {
        changeIndex: 0,
        createdOn: '2020-12-19T14:09:09Z',
        lastChangeOn: '2020-12-19T14:09:09Z',
        originalAuthor: '5b870669796da25424540deb',
        author: '5b870669796da25424540deb',
      },
      cols: [
        {
          col: 0,
          row: 0,
          type: 'article',
          data: { id: '5ca78e2160780601008f69e6' },
        },
        {
          col: 1,
          row: 0,
          type: 'picture',
          data: {
            id: '5cc8dc1038e882010061545a',
          },
        },
      ],
    };
  }

  picture(_id = '5cc8dc1038e882010061545a') {
    return {
      _id,
      owner: '5b870669796da25424540deb',
      picture:
        'https://new.opengreenmap.org/api-v1/pictures/5d5aa72acac72c010043fb59/picture',
      name: '',
    };
  }

  sound(_id = '5cc8dc1038e8820100615499') {
    return {
      _id,
      feature: '',
      info: {
        changeIndex: 0,
        createdOn: '2020-12-19T14:09:09Z',
        lastChangeOn: '2020-12-19T14:09:09Z',
        originalAuthor: '5b870669796da25424540deb',
        author: '5b870669796da25424540deb',
      },
      visibility: {
        isDefault: false,
        isPublic: true,
        team: '5ca88c99ecd8490100caba50',
      },
      sound: 'http://localhost:9091/sounds/5fde0985e20090a2c4cec43c/sound',
      name: '',
    };
  }

  geocoding(_id = '609e48af7f7737bb2db1a6bc') {
    return {
      _id,
      name: 'Berlin, Coos County, New Hampshire, 03570, United States',
      score: 1,
      icons: ['5ca7bfc0ecd8490100cab980'],
      geometry: {
        type: 'Point',
        coordinates: [13.433576, 52.495781],
      },
      license: {
        url: 'https://osm.org/copyright',
        name: 'Data © OpenStreetMap contributors, ODbL 1.0',
      },
    };
  }

  userProfile(_id = '5b8a59caef739394031a3f67') {
    return {
      _id,
      userName: 'some_username',
      organization: 'GISCollective',
      location: 'Berlin',
      bio: 'This is me',
      statusEmoji: 'earth_africa',
      picture: '5cc8dc1038e882010061545a',
      linkedin: 'szabobogdan',
      twitter: 'szabobogdan1',
      skype: 'skype_id',
      canEdit: true,
      website: 'https://szabobogdan.com',
      joinedTime: '2010-01-01T02:30:00Z',
      jobTitle: 'Developer',
      showCalendarContributions: true,
      showPrivateContributions: false,
      statusMessage: 'Hello',
      salutation: 'Dr.',
      title: 'mr',
      firstName: 'Bogdan',
      lastName: 'Szabo',
    };
  }

  translation(_id = '5ca7bfc0ecd8490100cab980') {
    return {
      _id,
      name: 'Romana',
      locale: 'ro-ro',
      file: 'https://new.opengreenmap.org/api-v1/translations/5ca7bfc0ecd8490100cab980/file',
      canEdit: 'true',
    };
  }

  icon(_id = '5ca7bfc0ecd8490100cab980') {
    return {
      _id,
      image: {
        useParent: false,
        value:
          'https://new.opengreenmap.org/api-v1/icons/5ca7bfbfecd8490100cab97d/image',
      },
      allowMany: false,
      name: 'Healthy Dining',
      localName: 'Healthy Dining',
      category: 'Sustainable Living',
      subcategory: 'Green Economy',
      description:
        'The emphasis is on wholesome, healthful, fresh foods, made with local and/or organic ingredients. Vegetarian and vegan foods (no animal products whatsoever) are served. Meat and dairy products are from ethically treated animals raised carefully to minimize environmental and health impacts, without additives, genetic modifications or factory farm practices. Endangered fish, marine and other species are not served. Cooperatively-owned cafes, cafes that grow their own food as well as "Slow Food" sites can be included, as can traditional or special local cuisines.\n',
      attributes: [],
      otherNames: [],
      iconSet: '5ca7b702ecd8490100cab96f',
    };
  }

  iconSet(_id = '5ca7b702ecd8490100cab96f') {
    return {
      _id,
      visibility: {
        isDefault: true,
        isPublic: true,
        team: '5ca78e2160780601008f69e6',
      },
      name: 'Green Map® Icons Version 3',
      style: {
        opacity: 1,
        overlayVisible: true,
        overlayOpacity: 0.5,
        scale: 1,
        overlayLineColor: '#999999',
      },
      verticalIndex: 0,
      maxRelevanceLevel: 4,
      styles: {
        hasCustomStyle: false,
        types: {
          polygonBorderMarker: {
            backgroundColor: 'white',
            size: 21,
            shape: 'circle',
            borderWidth: 1,
            borderColor: 'blue',
          },
          lineMarker: {
            backgroundColor: 'transparent',
            lineDash: [],
            borderWidth: 1,
            borderColor: 'blue',
          },
          polygonMarker: {
            backgroundColor: 'white',
            size: 21,
            shape: 'circle',
            borderWidth: 1,
            borderColor: 'blue',
          },
          polygon: {
            backgroundColor: 'transparent',
            lineDash: [],
            borderWidth: 1,
            borderColor: 'blue',
          },
          site: {
            backgroundColor: 'white',
            size: 21,
            shape: 'circle',
            borderWidth: 1,
            borderColor: 'blue',
          },
          line: {
            backgroundColor: 'white',
            size: 21,
            shape: 'circle',
            borderWidth: 1,
            borderColor: 'blue',
          },
          polygonBorder: {
            backgroundColor: 'transparent',
            lineDash: [],
            borderWidth: 1,
            borderColor: 'blue',
          },
        },
      },
      cover: '5cc8dc1038e882010061545a',
      canEdit: true,
      description: 'description',
    };
  }

  map(_id = '5ca89e37ef1f7e010007f54c') {
    return {
      _id,
      info: {
        changeIndex: 6,
        createdOn: '0001-01-01T00:00:00Z',
        lastChangeOn: '2019-08-29T21:47:26Z',
        originalAuthor: '',
        author: '5b8a59caef739394031a3f67',
      },
      baseMaps: {
        useCustomList: false,
        list: [],
      },
      visibility: {
        isDefault: false,
        isPublic: true,
        team: '5ca78e2160780601008f69e6',
      },
      area: {
        type: 'Polygon',
        coordinates: [
          [
            [-123.0621520032825, 47.66559037110829],
            [-122.5703291721397, 47.66559037110829],
            [-122.5703291721397, 48.16939418882865],
            [-123.0621520032825, 48.16939418882865],
            [-123.0621520032825, 47.66559037110829],
          ],
        ],
      },
      iconSets: {
        useCustomList: false,
        list: ['5ca7b702ecd8490100cab96f'],
      },
      name: 'Harta Verde București',
      endDate: '0001-01-01T00:00:00Z',
      startDate: '2008-01-01T00:00:00Z',
      cover: '5cc8dc1038e882010061545a',
      tagLine: 'Gândește global, carografiază local!',
      description: 'Description text',
    };
  }

  mapFile(_id = '5e3bf85528c8144486996173') {
    return {
      _id,
      size: 38700,
      file: 'http://192.168.100.138:9091/mapfiles/5e3bf85528c8144486996173/file',
      map: '5e2f42c96e290a010057dd95',
      canEdit: true,
      name: 'United Nations District Map.csv',
    };
  }

  baseMap(_id = '000000111111222222333333') {
    return {
      _id,
      name: 'name of basemap',
      attributions: [],
      layers: [],
      cover: '5cc8dc1038e882010061545a',
      visibility: {
        isDefault: false,
        isPublic: true,
        team: '5ca78e2160780601008f69e6',
      },
    };
  }

  team(_id = '5ca78e2160780601008f69e6') {
    return {
      _id,
      isPublic: true,
      members: [],
      owners: [],
      leaders: [],
      logo: '5cc8dc1038e882010061545a',
      name: 'Open Green Map',
      about: 'test description',
      guests: [],
      canEdit: true,
    };
  }

  campaign(_id = '5ca78aa160780601008f6aaa') {
    return {
      _id,
      map: {
        isEnabled: true,
        map: '5ca89e37ef1f7e010007f54c',
      },
      info: {
        changeIndex: 0,
        createdOn: '2015-01-01T00:00:00Z',
        lastChangeOn: '2015-01-01T00:00:00Z',
        originalAuthor: '',
        author: '',
      },
      visibility: {
        isDefault: false,
        isPublic: true,
        team: '000000000000000000000001',
      },
      article: 'Description text',
      startDate: '0001-01-01T00:00:00+00:00',
      endDate: '0001-01-01T00:00:00+00:00',
      name: 'Campaign 1',
      cover: '5cc8dc1038e882010061545a',
      icons: [],
    };
  }

  area(_id = '000000216078060100000000') {
    return {
      _id,
      name: 'berlin',
    };
  }

  base64Png() {
    return `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAAAsSAAALEgHS3X78AAAPVElEQVR4nO2deXBcxZ3HP/2OeaPTlmVh67I5DbG5jI8lmDsx68CmwhVgQ/YgsCwkBEJgk1SgwhGOsJXaLSrLEq4FkpTt4Cwha5trTdkQYCEcxjY2+AJfOmxL8ljWaM7XvX/0m9HIluSRNKN5k8y3SjWap/e6+71PH7/+/fq1xPaJ9ylK8o2MQhegpP4qAfGZSkB8phIQn6kExGcqAfGZrJymJhVKaStaGAKEyGnyWUsBBcp6tMoOiFSouItKJL2rTAzHAkOAAuVKVDSJqHawj62BuCS2sQPhSgzHBnOQp6O8dGNJ/RAtA2GbqIQLSakfquk1Ylf2nRMwUUkJgLANVMxFWAYIodMKmJCU+rxoEqQCIRABAxWXoBQIEAETEfDuwyc6PBClkJEE426bR81PLwAgvGwDe696DqPMRiVdzPpKJj59GcHTp/a79MDC1XTdtAzhqr4Hm5l0QlL1ndOZ8OBfA5DY3kXb2Y9zxKKrKDvjSAB6nlsHtqDykhMBiK1upe3cJ5i07O8QZQHaTn+Uo9QD7P3n50nu2E/9S9cAEHnjM3YveIYjo/em82u/5DdM+v03EWi23f/xNqEfvoIoC/imRQ09hiiFirmUXTQtDQMAV+la50qoCtC45uZDYABUfWMmk1ddi4wkdK0cKH3veMusX2BPnUDVtXNAQfSDXbTMfYSuH7+CsC0Sm/fSOu+XODMbqL5uNsrVtVzhogCiSSY8tIDIW9toO/9xys4+GvuEiQB03fcaLfMeJbpyKwII/fub7Pvxy4y76QyM5nH6XnyiIYGopMSaPpFJS64+6A/6Q8ZdJtw3Xzd7T+HnPyby1vbUKTgnN1Bx5UmohByyINEPP0dG4xgVAd0q90eIv9+K3LkfYRrIaJLo258hwzHMhipdKTIgK6kwJ1WQ/HQP8dW7ARATygBwt+0j8UEb6kBCl7uth97lGwEwG6rSlcIPGhyIUhAwqX/9+sGvTriUX3Zi+mvoZ6vY8/VFtJ/5GLF3d6SPl3/9JFTSHbIgwbnHYgQDyH0RAKyjahl/x7ko28gYpEXf54BlBkwDFUmwbcK9xN/dpfP/yglUXH0KSNl31wN0oX7QEEDAGF+GEbRRgIzEDzlFlFmYlU76e/SDXRiVDqIiQPzdnenj9rRaXaMHzEcfb3z320Tf2U7o394Ey8CaVEH5xV9AWAbDrr9KD/YpKy9wWj3OWVNg+CmNubKqJvvuWUH09c/7H1RA0Ox3SEh0JRa6C0kfd6zBn4UQKKD31Y0I24JIDGEZRP9vB61zHoWom731LAClEJU2U3ffSWBuIwCh+1fSdcNSMM2Br/GRhgQiHIueX3/A/rtXIMrsvBVCAD3PfIgzqxGjtqpvkFUKlXB1H5+UKBIIx0LFXf3gDaGPeemohO7bZNQzz1OfnmmOq7ssFXMR3p2raNJXUAYHIgRuezedNy1HOIH8TPKE0PMHIPy7DQBU3TAXgPIvHUfTth9Q89ACUOCc2siRbfcgTJPwsk+IrPwMZ04zzet/BEDsvRYiyz+l6prZNK27BQUkPt0LQO3PL6Rp2w8InncMAOPvPp+Gd76N2xsnsXa3nsT6RIPPQwQIBAS8iVo+ZAoi/7uFju4owhDs+cZiVDhO6ON2zPpqhCFIbOkk+toWepd/glCC6Hs7cDd2cWB1O3JnCGv6EXTeuAXZcoCu214i/mErZlM14SXrUXFJx3f/gEpIhG3i7grRceMLKKUQSUnvi5sQllk4j8IAyq3rZJgShkFycyfJjR0YFQEiL23qG2tSn4b3+9uekWAKhGMhUPQu3wRLN3nHTAQQXrwufZ5hW4R/taYvQ1MQXrjWyxyEaaRbqF9UUCBpCfp8YKbhTRj1caRKm73CNFDeuILhfTe8+YgSOg1T6LFFKpQrPZdKn5EhbA1AuVKfn5B98AV9rWWgvKU3ITYEwsyPr66gQJSUWEfXEDhlMpFXtlB24TR6l2/CHB/Enl5HfMMenLnNegBHEX5uPdbxtZRfMI3E1g4iSzdhT68jMLue8MJ1BM85CpJJYu+0YE2rJTCzgcirm1GhmDZ7hSD2x+0AlF9xIrG3d+DMatRgDIHsiiD3RzHryol/srcvb6EIL/oY56+acM6YQvyjNmJvbtfWY46hFLaFuIqyBdOYcN8FtH7xUeoev5TOW5chu2PUPHQB++5cQd0vLyG5pxuSkujbO2lafTOJ1m7shmq67ngFlVDU/usCep58n/F3nosMRdn91d9QfduZVP3tqez5xyWEF65l/E/ORzgmbV98HFA6r+8upfqHZ2I31aCAyIufIrtjBM+ayr57V1L3WF/e8U1d1L9+PfENuwk8uID2i39FbNU2hD2AKT0KFbYDVSrttcUzHMovnYGKJFBRN21M7Jj0ADun/JzyS2fo7433EF62gYrLT0JFtDtE4XmO4y5IF+fkyQA4M+u1yZzhVVYpL44p2Nn8IPHNewkv/oj2i57WafXL+36d95ePQcYT7JzxM5KhXoJnTs3LPNMfY0iGys46ip6FHx3ijBSWgeG5OwwsVFLp2jmgH0oRmDGZxOedOLObMwgcJEMghA22AbaJwBzQFS8CJonNHRgBm+pr59E28xGUVHq8y7FGnqIA2Zs8KDUvPqK0BZVSVpMvb8Ye37SHiktn6HiIpyntd1Bx+Yl6cncYKamwjtVe3p5fryZwehNw+OsG05T2O6j45sn0LllP74rN1D15OcEvH43sCOelfxlVkiLm4u7rTX8PnnUk8kAMFY7hnNPnjo9/snvwIFVmekB40TrK50/r68qAru+/SPT9luwCSa7EmdWEiieJvLwFwzQxaipH7GLvvOVFYu/uAtukff5/EVmxibonLiMwp1GHAHKs0TEOmIQXrU1/HXfzPCav+hYN62/FOaUxfTy8cE3Wg1/PkrWHHAsvXou7PYTItGgyPcCpAxJwFYHZDSjToOb++bqYsxu0yarQHt9huNt7f7uW5OYuqm+fR/OuH9E6/ymUklRceVK/VpwrZQ9EHPq7YVuE7n4NNxRJ/6ns7GNwph+R/h754+dEXtiY9QQssb4V2RPr1xrsEyZiHT8BtzOs860px2qsJtnRi4pqL7R9zBGYU8YhD8Rw5jQRX9NGdMVWAJxZjfrhBS3MabUYVYGsb9s+YSLWcTWQkFgN1ZjjyrXnOg8wIFsghkD1JlFx7yea0PEEU6DiLi0nP0x42YZ+l0jXZf/Db7H7wmd10Gkge92LcwMoL8glsOn5/ceIchsjoFtV0/pbafroFnqeWU30oxamdt1FcG4z+x9aRfj5DSRbQjRvuZ3AsRPp/sU7BM85mt7Fawk9sJL41g6CZx+FMAXBUxppXvc9XbvRgzWmjtEbjgmWAQEDgiaGo8vV9Mn3aVrzPXqeXY2MxDkydBdKSg48/p42BnIskdVia6m0BzUVZLIPWuQgpTYpBZiN1ZCQuC3dh19EIBUq6aKki8BEKe1zwpW6ixECXFcbCUIgAhYyHKf2ka8SnH8cAK3TH9aTt5ogar923SsltVFhGnphRuoOXW/hhGECEmGafecgdN4p7/LBedsWKp7EqClDhiLafWOaOfcUZwckWymV4YYQOS8s4K2ASepB3xAYQdurGCkfRx7yzFSe88ntPCRfEDJlCETQPjSbsfLY5jkff7k6SyoB8ZtKQHymPx8g/l9QkpV851wctqRCJlywBAKRF4ffWKq4Sy8Vbk+cuoVX0PDG9aikzMoB6WcVL5AUjN9eQcXffIHAiZOpf/26oodSnEAyYFRe3LeU1ZnZWPRQig/IIDBScmY2Ur+qeKEUF5DDwEjJOa14W0rxAMkSRkrF2n0VB5BhwkipGLsv/wORCtmboO53Vw0LRkrOaY3Uv3m99tLmIeSaa/l+Yqhcybi7zqPsjKkkd4ZGNCO3JldR+9jX6LzuBYTp71vObTwkH5LeKwkJd+SLR4ReQuq3N24Hkr+rC+j4h2PpZZt/AfL/GPIXphIQn6kExGcqAfGZCj9SKrzFwIMskFDKW7I5WmNQv8gzoJWVLgMFf72tsEA8k9aaVov72b5DX+ZXoITAOaOx31rfkUiYBvH1e6AncSgUpRCTK1AdEVTCLWiQq3BAPHfIEX+4mvgHrXQ/9MYhD0JJiTWlmsnLr8lJlruvXEj0lS0Io/86Y+VKgqc1MP7O82md858ABYNSmFwzg0sLjtdLUwfb6CGX7o4hWpmKuQSOryu4Q3LsgQzkKPSDr8BbEV9oL/HYAhmh13asVUgv8dgBGSmMXBo9w/BjFSrINTaD+ghhCCFQPQl6X9yoX4sbhUTAxG07MDwoXvfVds6TOo0xGOjzD2Q03ZQhkKEoe65YNPpd3wz9usFwH2qq+2o7d2yg5BeIt19j3XNXUvm1GSNKQpgGoqywDgXntEbq37iO9vOeSu/kkC/l9U6Vq6j8h5kjhuEnOac2Mv7uL416gno45RWIMAU9T39I91N/ymc2Y6LIyq3su+PVvG9Wk98uSwiMigCdNy4FCdX/NHd412e+LjfaiinQOwaZxrD9VZGVW2m/8FnMskDeI475H9QNgVnl0PmdpcAwoSiFqC1j8uIrkbFRWlmORdftL5NY06538slS/WAM47qRamzM3kwoBlRfmx0UpRRmmYUzqzknxTAnlpOQCrLcL2asYcBYTgxTUG5cSvcTwxhTculWGYbpXAgYMNauk4yW4ueBvlAwoBDOxcyW4kHxxZaHXiEKCQMKFQ/JgCIcC1FuD+qzGs4AfFgNsd+KCJpE/7SzoDCg0AvlvE0AzKZxyN09A0cMlcI+bsLoI4aWQXJ7COLyUNNVKowJZbidYQRGwWBAoUO43iYAcm944L3YvTB7YlNnTrIbNKZuCOS+iI4kFrj7LPwiBxh6EBEit93WSMowhiotA/KZSkB8phIQn6kExGfyx6A+lGTGfrwj/ddEgv6brvlYvgeikpJxPzmbcf9ynrc73HCh6J3oel/4mI6//2/fv2fi79KhJ3ShO1di1FZS/a05I0ojsuoz9ly9BDOYv39Kkyv5fwwxBEZlgM4b/md4XmJPkZVbaf/KM5iO7fvuCooBCIzYS1xoR+FIVBxAYEAv8VAqRhhQTEAg6yBXscKAYgMCh+2+ihkGFCMQGLSlFDsMKAKzd1BlLpywDQLTJtJ+UXHDgEIHqHKh1EwehWFbRQ0DirmFpGQIRNAqdFwpZyrOMeTPWCUgPlMJiM9UAuIzlYD4TCUgPlMJiM9UAuIzlYD4TCUgPtP/A+wL5XZ0hMpkAAAAAElFTkSuQmCC`;
  }

  pngBlob() {
    const byteArray = atob(this.base64Png().slice(22));
    let blob = new Blob([byteArray], { type: 'image/png' });
    blob.name = 'foobar.png';

    return blob;
  }

  base64Mp3() {
    return `data:audio/mpeg;base64,SUQzBAAAAAFCblRBTEIAAAATAAADcGx1bSBicmFuZHkgYmx1ZXMAVElUMgAAAA8AAANBbGwgeW91ciBsb3ZlAFRQRTEAAAANAAADTmlnaHRsb3NlcnMAVFNTRQAAAA8AAANMYXZmNTguMjkuMTAwAEFQSUMAAUF0AAADaW1hZ2UvanBlZ`;
  }

  mp3Blob() {
    const byteArray = atob(this.base64Mp3().slice(50));
    let blob = new Blob([byteArray], { type: 'audio/mpeg' });
    blob.name = 'coolsound.mp3';

    return blob;
  }
}

class Storage {
  constructor(testData) {
    this.testData = testData;
  }

  addDefaultPreferences() {
    this.addPreference('register.mandatory', false);
    this.addPreference('register.enabled', true);
    this.addPreference('appearance.name', 'Open Green Map');
    this.addPreference('profile.detailedLocation', 'false');
    this.addPreference('access.allowProposingSites', 'true');
    this.addPreference('access.allowManageWithoutTeams', 'true');
    this.addPreference('access.isMultiProjectMode', 'true');
  }

  addDefaultPicture() {
    return this.addPicture(this.testData.create.picture(...arguments));
  }

  addDefaultSound() {
    return this.addSound(this.testData.create.sound(...arguments));
  }

  addDefaultGeocoding() {
    return this.addGeocoding(this.testData.create.geocoding(...arguments));
  }

  addDefaultUser(isAdmin, _id = '5b870669796da25424540deb') {
    this.addUser(_id, {
      _id,
      isAdmin,
      email: 'contact@szabobogdan.com',
      username: 'gedaiu',
      name: 'gedaiu',
    });

    this.addDefaultProfile(_id);

    return this.addUser('me', {
      _id,
      isAdmin,
      email: 'contact@szabobogdan.com',
      username: 'gedaiu',
      name: 'gedaiu',
    });
  }

  addDefaultProfile() {
    return this.addUserProfile(this.testData.create.userProfile(...arguments));
  }

  addDefaultLayout() {
    return this.addLayout(this.testData.create.layout(...arguments));
  }

  addDefaultPage() {
    return this.addPage(this.testData.create.page(...arguments));
  }

  addDefaultPresentation() {
    return this.addPresentation(
      this.testData.create.presentation(...arguments)
    );
  }

  addDefaultArticle() {
    return this.addArticle(this.testData.create.article(...arguments));
  }

  addDefaultTeam() {
    return this.addTeam(this.testData.create.team(...arguments));
  }

  addDefaultCampaign() {
    return this.addCampaign(this.testData.create.campaign(...arguments));
  }

  addDefaultTranslation() {
    return this.addTranslation(this.testData.create.translation(...arguments));
  }

  addDefaultFeature() {
    return this.addFeature(this.testData.create.feature(...arguments));
  }

  addDefaultBaseMap() {
    return this.addBaseMap(this.testData.create.baseMap(...arguments));
  }

  addDefaultSpace() {
    return this.addSpace(this.testData.create.space(...arguments));
  }

  addDefaultMap() {
    return this.addMap(this.testData.create.map(...arguments));
  }

  addDefaultMapFile() {
    return this.addMapFile(this.testData.create.mapFile(...arguments));
  }

  addDefaultIcon() {
    return this.addIcon(this.testData.create.icon(...arguments));
  }

  addDefaultIconSet() {
    return this.addIconSet(this.testData.create.iconSet(...arguments));
  }

  addDefaultArea() {
    return this.addArea(this.testData.create.area(...arguments));
  }

  addPreference(name, value) {
    this.preferences[name] = {
      name,
      value,
      _id: name,
    };
    return value;
  }

  addUser(key, value) {
    this.users[key] = value;
    this.users[value._id] = value;
    return value;
  }

  addArticle(value) {
    this.articles[value._id] = value;
    return value;
  }

  addUserProfile(value) {
    this.userProfiles[value._id] = value;
    return value;
  }

  addTeam(value) {
    this.teams[value._id] = value;
    return value;
  }

  addPage(value) {
    this.pages[value._id] = value;
    return value;
  }

  addPresentation(value) {
    this.presentations[value._id] = value;
    return value;
  }

  addLayout(value) {
    this.layouts[value._id] = value;
    return value;
  }

  addFeature(value) {
    this.features[value._id] = value;
    return value;
  }

  addPicture(value) {
    this.pictures[value._id] = value;
    return value;
  }

  addSound(value) {
    this.sounds[value._id] = value;
    return value;
  }

  addGeocoding(value) {
    this.geocodings[value._id] = value;
    return value;
  }

  addIcon(value) {
    this.icons[value._id] = value;
    return value;
  }

  addTranslation(value) {
    this.translations[value._id] = value;
    return value;
  }

  addIconSet(value) {
    this.iconSets[value._id] = value;
    return value;
  }

  addMap(value) {
    this.maps[value._id] = value;
    return value;
  }

  addCampaign(value) {
    this.campaigns[value._id] = value;
    return value;
  }

  addMapFile(value) {
    this.mapFiles[value._id] = value;
    return value;
  }

  addBaseMap(value) {
    this.baseMaps[value._id] = value;
    return value;
  }

  addSpace(value) {
    this.spaces[value._id] = value;
    return value;
  }

  addModel(value) {
    this.models[value._id] = value;
    return value;
  }

  addArea(value) {
    this.areas[value._id] = value;
    return value;
  }
}
