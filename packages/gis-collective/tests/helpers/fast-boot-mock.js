import Service from '@ember/service';


export default class FastBootMock extends Service {

  constructor() {
    super(...arguments);
    this.request.host = `${window.location.hostname}:${window.location.port}`;
  }

  isFastBoot = true
  request = {
    host: "",
    path: "/",
    protocol: "http:",
  }
}

