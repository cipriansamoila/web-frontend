import { waitFor, waitUntil, click } from '@ember/test-helpers';
import { Modal } from 'bootstrap';

export default {
  clear() {
    const modalElements = document.querySelectorAll('.modal');
    modalElements.forEach(function (element) {
      const modal = Modal.getInstance(element);
      if (modal?._element) {
        modal.dispose?.();
      }
    });

    const elements = document.querySelectorAll('.modal-backdrop, .modal');

    elements.forEach(function (elm) {
      elm.parentNode.removeChild(elm);
    });

    document.querySelector('body').classList.remove('modal-open');
  },

  async waitToDisplay() {
    await waitFor('.modal.show', { timeout: 5000 });

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (document.querySelector('.modal.show')) {
          resolve();
        } else {
          reject();
        }
      }, 1200);
    });
  },

  waitToHide(timeout = 2000) {
    return new Promise((resolve, reject) => {
      const expected = 10;
      let index = 0;
      let total = timeout / 50;
      let success = 0;
      let timer;

      const check = () => {
        const element = document.querySelector('.modal');
        if (!element || !element.classList.contains('show')) {
          success++;
        }

        if (success > expected) {
          clearInterval(timer);
          resolve();
        }
      };

      timer = setInterval(() => {
        index++;

        if (success > expected) {
          return;
        }

        if (index < total) {
          check();
        } else {
          clearInterval(timer);
          reject('timeout! the modal is not closed.');
        }
      }, 50);
    });
  },

  async clickDangerButton() {
    return await waitUntil(async () => {
      await click('.modal .btn-danger');
      await this.waitToHide();
    });
  },

  isVisible() {
    const element = document.querySelector('.modal');

    if (!element) {
      return false;
    }

    return element.classList.contains('show');
  },

  title() {
    return document.querySelector('.modal-title').textContent.trim();
  },

  message() {
    return document.querySelector('.modal-body').textContent.trim();
  },
};
