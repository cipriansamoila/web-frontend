import Pretender from 'pretender';
import TestData from './test-data';

export default class TestServer {
  constructor() {
    this.testData = new TestData(this);

    this.history = [];

    this.server = new Pretender(function () {
      this.post('/write-coverage', this.passthrough);
    });

    this.server.handledRequest = (verb, path) => {
      this.history.push(`${verb} ${path}`);
    };
    this.server.get('/mock-server/service/logo', () => {
      return [200];
    });

    this.server.get('/api-v1/tiles/:z/:x/:y', () => {
      return [200, {}];
    });

    this.setupModelRoutes();

    const article = this.testData.create.article('about');
    article.visibility.isPublic = false;
    this.testData.storage.addArticle(article);
  }

  groupedIcons() {
    const result = {
      icons: [],
      categories: {},
    };

    Object.keys(this.testData.storage.icons)
      .map((a) => this.testData.storage.icons[a])
      .forEach((icon) => {
        if (!result['categories'][icon.category]) {
          result['categories'][icon.category] = {
            icons: [],
            categories: {},
          };
        }

        if (
          !result['categories'][icon.category]['categories'][icon.subcategory]
        ) {
          result['categories'][icon.category]['categories'][icon.subcategory] =
            {
              icons: [],
              categories: {},
            };
        }

        result['categories'][icon.category]['categories'][icon.subcategory][
          'icons'
        ].push(icon);
      });

    return result;
  }

  setupModelRoutes() {
    this.server.post('/mock-server/metrics', (request) => {
      const body = JSON.parse(request.requestBody);
      body.metric._id = '1';

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(body),
      ];
    });
    this.server.get('/mock-server/:plural', (request) => {
      const plural = this.testData.plurals[request.params.plural.toLowerCase()];

      if (
        request.queryParams.name &&
        !this.testData.storage[plural][request.queryParams.name]
      ) {
        return [404];
      }

      let value = [];

      if (plural == 'icons' && request.queryParams.group) {
        return [
          200,
          { 'Content-Type': 'application/json' },
          JSON.stringify(this.groupedIcons()),
        ];
      }

      if (request.queryParams.name) {
        value = [this.testData.storage[plural][request.queryParams.name]];
      } else {
        value = Object.keys(this.testData.storage[plural]).map(
          (key) => this.testData.storage[plural][key]
        );
      }

      const response = {};
      response[plural] = value;

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(response),
      ];
    });

    this.server.get('/mock-server/:plural/:id', (request) => {
      const plural = this.testData.plurals[request.params.plural.toLowerCase()];
      const singular =
        this.testData.singulars[request.params.plural.toLowerCase()];

      if (!this.testData.storage[plural][request.params.id]) {
        // eslint-disable-next-line no-console
        console.log(plural, request.params.id, 'not found');

        return [404];
      }

      const value = this.testData.storage[plural][request.params.id];
      const response = {};
      response[singular] = value;

      return [
        200,
        { 'Content-Type': 'application/json' },
        JSON.stringify(response),
      ];
    });
  }

  shutdown() {
    this.server.shutdown();
  }

  get() {
    return this.server.get(...arguments);
  }

  delete_() {
    return this.server.delete(...arguments);
  }

  delete() {
    return this.server.delete(...arguments);
  }

  post() {
    return this.server.post(...arguments);
  }

  put() {
    return this.server.put(...arguments);
  }
}
