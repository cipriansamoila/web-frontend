import MapController from '../../base/map-controller';
import { inject as service } from '@ember/service';
import config from '../../../config/environment';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import GeoJson from '../../../lib/geoJson';
import AlertMessage from '../../../lib/alert-message';
import { Cluster } from '../../../transforms/cluster';

export default class ManageMapEditController extends MapController {
  @service notifications;
  @service fastboot;
  @service intl;
  @service user;
  @service preferences;

  @tracked allTeams = false;
  @tracked savingField;
  @tracked editableField;

  constructor() {
    super(...arguments);
    this.queryParams.push('allTeams');
  }

  get alertList() {
    const list = [];
    const baseMaps = this.model.map.baseMaps?.list ?? [];
    const unpublishedBaseMaps =
      baseMaps
        .filter((a) => a.isLoaded)
        .filter((a) => !a.visibility?.isPublic)
        .map((a) => a.name) ?? [];

    if (this.model.map.visibility.isPublic && unpublishedBaseMaps.length > 0) {
      list.push(
        new AlertMessage(
          this.intl.t('message-alert-map-with-unpublished-basemaps', {
            names: unpublishedBaseMaps.join(', '),
          }),
          'danger',
          'alert-map-with-unpublished-basemaps'
        )
      );
    }

    return list;
  }

  get breadcrumbs() {
    return [
      {
        route: 'browse',
        text: this.intl.t('browse'),
        capitalize: true,
      },
      {
        route: 'browse.maps',
        text: this.intl.t('maps'),
        capitalize: true,
      },
      {
        route: 'browse.sites',
        query: { map: this.model.map.id },
        text: this.model.map.name,
      },
      {
        text: this.intl.t('edit-label'),
        capitalize: true,
      },
    ];
  }

  get hasClusteringEnabled() {
    return this.model.map.cluster?.mode ? true : false;
  }

  @action
  saveHasClusteringEnabled(value) {
    if (!this.model.map.cluster) {
      this.model.map.cluster = new Cluster();
    }

    this.model.map.cluster.mode = value ? 1 : 0;

    return this.save();
  }

  @action
  save() {
    this.savingField = this.editableField;
    this.editableField = '';

    return this.model.map.save().then(
      () => {
        this.savingField = '';
      },
      (err) => {
        this.notifications.handleError(err);
        this.savingField = '';
      }
    );
  }

  get apiUrl() {
    if (config.apiUrl.indexOf('http') == 0) {
      return config.apiUrl;
    }

    return 'https://' + this.domain + config.apiUrl;
  }

  get domain() {
    return this.preferences.domain;
  }

  @action
  changeAll(newValue) {
    this.allTeams = newValue;
  }

  @action
  createImage() {
    return this.store.createRecord('picture', { name: '', picture: '' });
  }

  @action
  saveIsPublic(newValue) {
    this.model.map.visibility.isPublic = newValue;
    this.editableField = this.intl.t('is public');

    return this.save();
  }

  @action
  saveAddFeaturesAsPending(newValue) {
    this.model.map.addFeaturesAsPending = newValue;
    this.editableField = this.intl.t('add features as pending');

    return this.save();
  }

  @action
  saveHideOnMainMap(newValue) {
    this.model.map.hideOnMainMap = newValue;
    this.editableField = this.intl.t('hide on main map');

    return this.save();
  }

  @action
  saveIsIndex(newValue) {
    this.model.map.isIndex = newValue;
    this.editableField = this.intl.t('is index');

    return this.save();
  }

  @action
  saveStartDate(value) {
    this.model.map.startDate = value;
    return this.save();
  }

  @action
  saveEndDate(value) {
    this.model.map.endDate = value;
    return this.save();
  }

  @action
  saveTagLine(newValue) {
    this.model.map.tagLine = newValue;
    return this.save();
  }

  @action
  saveTeam(newValue) {
    this.model.map.visibility.team = newValue;
    this.editableField = this.intl.t('team');

    return this.save();
  }

  @action
  saveIsDefault(newValue) {
    this.model.map.visibility.isDefault = newValue;
    this.editableField = this.intl.t('is default');

    return this.save();
  }

  @action
  saveLicense(newValue) {
    this.model.map.license = newValue;
    return this.save();
  }

  @action
  saveMask(newValue) {
    this.model.map.set('mask', newValue);
    return this.save();
  }

  @action
  saveShowPublicDownloadLinks(newValue) {
    this.model.map.showPublicDownloadLinks = newValue;
    this.editableField = this.intl.t('show download links');

    return this.save();
  }

  @action
  saveCover(index, value) {
    this.model.map.cover = value;
    return this.save();
  }

  @action
  saveSquareCover(index, value) {
    this.model.map.squareCover = value;
    return this.save();
  }

  @action
  saveIconSets(title, value, useCustomList) {
    this.editableField = title;
    this.model.map.iconSets.list = value;
    this.model.map.iconSets.useCustomList = useCustomList;

    return this.save();
  }

  @action
  saveBaseMaps(title, value, useCustomList) {
    this.editableField = title;
    this.model.map.baseMaps.list = value;
    this.model.map.baseMaps.useCustomList = useCustomList;

    return this.save();
  }

  @action
  saveArticle(title, description) {
    this.model.map.name = title;
    this.model.map.description = description;

    return this.save();
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.editableField = '';
    return this.model.map.rollbackAttributes();
  }

  @action
  changeExtent(value) {
    this.model.map.area = new GeoJson(value);
    return this.save();
  }

  @action
  delete() {
    const teamId = this.model.map.visibility.teamId;
    return this.model.map
      .destroyRecord()
      .then(() => {
        return this.transitionToRoute('manage.dashboards.dashboard', teamId);
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }
}
