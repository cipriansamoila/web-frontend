import BrowseController from '../../base/browse-controller';
import config from '../../../config/environment';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class ManageMapsImportController extends BrowseController {
  @service modal;
  @service store;
  @service router;
  @service session;

  host = config.apiUrl;
  viewMode = 'list';

  @tracked panel = '';
  @tracked file = '';
  @tracked log = '';
  @tracked _files;
  @tracked chunkProgress = 0;

  constructor() {
    super(...arguments);

    this.queryParams.push('panel');
    this.queryParams.push('file');
  }

  get logVisible() {
    return this.panel == 'log';
  }

  get breadcrumbs() {
    return [
      {
        route: 'browse',
        text: this.intl.t('browse'),
        capitalize: true,
      },
      {
        route: 'browse.maps',
        text: this.intl.t('maps'),
        capitalize: true,
      },
      {
        route: 'browse.sites',
        query: { map: this.model.map.id },
        text: this.model.map.name,
      },
      {
        text: this.intl.t('map files'),
        capitalize: true,
      },
    ];
  }

  get panelVisible() {
    return !!this.panel;
  }

  @action
  updateMapping(key, value) {
    this.model.file.options.fields[key] = value;
    this.model.file.hasDirtyAttributes = true;
  }

  get files() {
    if (!this._files) {
      return this.model.files;
    }

    return this._files;
  }

  @action
  async fileSelect() {
    this.reloadModel();
  }

  @action
  saveFile(file) {
    return file.save();
  }

  @action
  reloadModel() {
    this._files = [];

    later(() => {
      this._files = this.store.query(
        'map-file',
        { map: this.model.map.id },
        { reload: true }
      );
    });
  }

  @action
  import(itemId) {
    this.router.transitionTo('wizard.import.file-fields', itemId);
  }

  @action
  cancelImport(itemId) {
    const item = this.store.peekRecord('map-file', itemId);

    if (!item) {
      return;
    }

    return item.cancelImport().catch((err) => {
      this.notifications.handleError(err);
    });
  }

  @action
  closePanel() {
    this.panel = '';
    this.file = '';
    this.log = '';
  }

  @action
  showLog(item) {
    this.panel = 'log';
    this.file = item.id;
    this.log = '';
    this.set('model.file', item);

    item.getLog().then((result) => {
      if (!this.file || this.file != result.id) return;

      this.log = result.log;
    });

    return item.reloadMeta();
  }

  @action
  delete(item) {
    return item.destroyRecord().catch((err) => {
      this.notifications.handleError(err);
    });
  }
}
