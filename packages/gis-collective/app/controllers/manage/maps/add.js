import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageMapsAddController extends Controller {
  @service notifications;
  @service intl;
  @tracked _description;

  get description() {
    if (this._description) {
      return this._description;
    }

    return {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: this.intl.t('article-placeholders.add-map'),
          },
        },
      ],
    };
  }

  set description(value) {
    this._description = value;
  }

  @action
  changeTeam(team) {
    this.model.map.visibility.team = team;
  }

  @action
  changeDescriptionValue(title, description) {
    this.description = description;
  }

  @action
  save() {
    const blocks = [
      {
        type: 'header',
        data: {
          text: this.model.map.name,
          level: 1,
        },
      },
      ...this.description.blocks,
    ];

    this.model.map.description = {
      blocks,
    };

    return this.model.map
      .save()
      .then((result) => {
        return this.transitionToRoute('manage.maps.edit', result.id);
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }
}
