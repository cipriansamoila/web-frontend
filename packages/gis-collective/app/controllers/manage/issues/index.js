import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Controller.extend({
  modal: service(),

  issues: computed('model.buckets.[]', function () {
    const items = this.model.buckets.map((item) => {
      if (item.type == 'newImage') {
        item.set('hasImage', true);
      }

      return item;
    });

    return items;
  }),

  actions: {
    resolve: function (item) {
      item
        .resolve()
        .then(() => {
          item.reload();
        })
        .catch((error) => {
          var description = error.message;

          if (error.errors && error.errors.length > 0) {
            description = error.errors[0].description;
          }

          this.modal.alert('Resolve issue error', description);
        });
    },

    reject: function (item) {
      item
        .reject()
        .then(() => {
          item.reload();
        })
        .catch((error) => {
          var description = error.message;

          if (error.errors && error.errors.length > 0) {
            description = error.errors[0].description;
          }

          this.modal.alert('Reject issue error', description);
        });
    },
  },
});
