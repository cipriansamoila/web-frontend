import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';
import IconStyle from '../../../lib/icon-style';

export default class ManageIconsEditSetController extends Controller {
  @service notifications;
  @service intl;
  @service user;

  @tracked editableField = '';
  @tracked savingField = '';
  @tracked allTeams = false;

  queryParams = ['allTeams'];

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.team.get('id'),
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        text: this.intl.t('edit-model.title', {
          name: this.model.iconSet.name,
        }),
      },
    ];
  }

  @action
  changeAll(newValue) {
    this.allTeams = newValue;
  }

  @action
  save() {
    this.savingField = this.editableField;
    this.editableField = '';

    return this.model.iconSet.save().then(
      () => {
        this.savingField = '';
      },
      (err) => {
        this.notifications.handleError(err);
        this.savingField = '';
      }
    );
  }

  @action
  saveIsPublic(newValue) {
    this.model.iconSet.visibility.isPublic = newValue;
    this.set('editableField', this.intl.t('is public'));

    return this.save();
  }

  @action
  saveIsDefault(newValue) {
    this.model.iconSet.visibility.isDefault = newValue;
    this.set('editableField', this.intl.t('is default'));

    return this.save();
  }

  @action
  saveTeam(newValue) {
    this.model.iconSet.visibility.team = newValue;
    this.set('editableField', this.intl.t('team'));

    return this.save();
  }

  @action
  saveStyle(newValue) {
    this.model.iconSet.styles = new IconStyle(newValue);
    return this.save();
  }

  @action
  createImage() {
    return this.store.createRecord('picture', { name: '', picture: '' });
  }

  @action
  saveArticle(name, description) {
    this.model.iconSet.set('name', name);
    this.model.iconSet.set('description', description);

    return this.save();
  }

  @action
  saveCover(index, cover) {
    this.model.iconSet.set('cover', cover);

    return this.save();
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.editableField = '';
    return this.model.iconSet.rollbackAttributes();
  }

  @action
  delete() {
    return this.model.iconSet.destroyRecord().then(() => {
      return this.transitionToRoute(
        'manage.dashboards.dashboard',
        this.model.team.id
      );
    });
  }
}
