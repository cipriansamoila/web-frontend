import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageIconsAddSetController extends Controller {
  @service notifications;
  @service intl;

  _description = null;
  @tracked team;

  get description() {
    if (this._description) {
      return this._description;
    }

    return {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: this.intl.t('article-placeholders.add-icon-set'),
          },
        },
      ],
    };
  }

  set description(value) {
    this._description = value;
  }

  get isInvalid() {
    const hasTeam = this.model.iconSet.visibility?.team;

    return String(this.model.iconSet.name).trim() == '' || !hasTeam;
  }

  @action
  changeTeam(team) {
    this.model.iconSet.visibility.team = team;
    this.team = team;
    this.notifyPropertyChange('isInvalid');
  }

  @action
  changeDescriptionValue(title, description) {
    this.description = description;
  }

  @action
  save() {
    const blocks = [
      {
        type: 'header',
        data: {
          text: this.model.iconSet.name,
          level: 1,
        },
      },
      ...this.description.blocks,
    ];
    this.model.iconSet.description = {
      blocks,
    };

    return this.model.iconSet
      .save()
      .then((result) => {
        this.transitionToRoute('manage.icons.editset', result.id);
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }
}
