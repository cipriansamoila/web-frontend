import Controller from '@ember/controller';
import { action } from '@ember/object';
import { later } from '@ember/runloop';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import InheritedStyle from '../../../lib/inherited-style';
import AlertMessage from '../../../lib/alert-message';

export default class EditIconManageController extends Controller {
  @service notifications;
  @service intl;
  @service store;
  @service fastboot;

  @tracked savingField;
  @tracked editableField;

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.team.get('id'),
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        route: 'browse.icons.set',
        model: this.model.iconSet.get('id'),
        text: this.model.iconSet.get('name'),
      },
      { text: this.intl.t('edit-model.title', { name: this.model.icon.name }) },
    ];
  }

  get alertList() {
    const list = [];
    const iconSet = this.model.icon.get('iconSet');
    const parentIconSet = this.model.parentIconSet;

    if (iconSet && !iconSet?.get('visibility')?.isPublic) {
      list.push(
        new AlertMessage(
          this.intl.t('message-alert-icon-set-private'),
          'warning',
          'alert-icon-set-private'
        )
      );
    }

    const isParentIconSetVisible = parentIconSet?.get('visibility')?.isPublic;
    if (parentIconSet && !isParentIconSetVisible) {
      list.push(
        new AlertMessage(
          this.intl.t('message-alert-parent-icon-private'),
          'warning',
          'alert-parent-icon-private'
        )
      );
    }

    return list;
  }

  get isSavingIcon() {
    return this.savingField == 'icon';
  }

  get isSavingAttributes() {
    return this.savingField == 'attributes';
  }

  get categories() {
    var result = [];

    this.model.icons.forEach((element) => {
      const category = element.get('category');

      if (result.indexOf(category) == -1) {
        result.push(category);
      }
    });

    return result;
  }

  get subcategories() {
    var result = [];
    const category = this.model.icon.category;

    this.model.icons
      .filter((a) => a != category)
      .forEach((element) => {
        const subcategory = element.get('subcategory');

        if (result.indexOf(subcategory) == -1) {
          result.push(subcategory);
        }
      });

    return result;
  }

  get attributes() {
    return this.model.icon.attributes;
  }

  performIconSearch(term, resolve, reject) {
    if (!term || term.length <= 3) {
      return reject('Term is too short');
    }

    this.store
      .query('icon', { term: term })
      .then((data) => {
        resolve(data);
      })
      .catch(reject);
  }

  @action
  parentIconChanged(icon) {
    this.model.icon.set('parent', icon?.id ?? '');
    this.model.parentIcon = icon;

    return this.save();
  }

  @action
  changeIconSet(iconSet) {
    this.model.icon.set('iconSet', iconSet);
    return this.save();
  }

  @action
  changeIconImage(image) {
    this.editableField = 'icon';
    this.model.icon.image = image;
    return this.save();
  }

  @action
  saveAttributes(value) {
    this.model.icon.attributes = value;

    return this.save();
  }

  @action
  saveCategory(value) {
    this.model.icon.category = value;

    return this.save();
  }

  @action
  saveSubcategory(value) {
    this.model.icon.subcategory = value;

    return this.save();
  }

  @action
  save() {
    this.savingField = this.editableField;

    later(() => {
      this.editableField = '';

      this.model.icon.save().then(
        () => {
          this.savingField = '';
        },
        (err) => {
          this.notifications.handleError(err);
          this.savingField = '';
        }
      );
    }, 200);
  }

  @action
  saveAllowMany(newValue) {
    this.model.icon.set('allowMany', newValue);
    this.editableField = this.intl.t('allow many instances');

    return this.save();
  }

  @action
  saveArticle(name, description) {
    this.model.icon.set('name', name);
    this.model.icon.set('description', description);

    return this.save();
  }

  @action
  saveStyles(value) {
    this.model.icon.styles = new InheritedStyle(value);
    return this.save();
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.editableField = '';
    return this.model.icon.rollbackAttributes();
  }

  @action
  delete() {
    return this.model.icon.destroyRecord().then(() => {
      return this.transitionToRoute(
        'browse.icons.set',
        this.model.iconSet.get('id')
      );
    });
  }
}
