import { action } from '@ember/object';
import config from '../../../config/environment';
import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class ManageIconsAddSetController extends Controller {
  @service notifications;
  @service intl;
  @service store;

  host = config.apiUrl;
  _description = null;

  @tracked iconSet;
  @tracked parentIcon;

  get description() {
    if (this._description) {
      return this._description;
    }

    return {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: this.intl.t('article-placeholders.add-icon'),
          },
        },
      ],
    };
  }

  set description(value) {
    this._description = value;
  }

  get imageUrl() {
    if (!this.model.image.value) {
      return '';
    }

    if (this.model.image.value.indexOf('data:') == 0) {
      return this.model.image.value;
    }

    return this.host + '/' + this.model.image.value;
  }

  get categories() {
    var result = [];

    this.model.icons.forEach((element) => {
      const category = element.get('category');

      if (result.indexOf(category) == -1) {
        result.push(category);
      }
    });

    return result;
  }

  get subcategories() {
    var result = [];
    const category = this.model.icon.category;

    this.model.icons
      .filter((a) => a != category)
      .forEach((element) => {
        const subcategory = element.get('subcategory');

        if (result.indexOf(subcategory) == -1) {
          result.push(subcategory);
        }
      });

    return result;
  }

  get isInvalid() {
    const image = this.model.icon?.image ?? {};
    const hasImage = image.useParent || image.value;

    return (
      `${this.model.icon.name}`.trim() == '' ||
      !hasImage ||
      !this.model.icon.iconSet?.get?.('id')
    );
  }

  @action
  parentIconChanged(icon) {
    this.parentIcon = icon;
    this.model.icon.set('parent', icon?.id ?? '');
  }

  @action
  changeImage(image) {
    this.model.icon.image = image;
    this.notifyPropertyChange('isInvalid');
  }

  @action
  changeIconSet(iconSet) {
    this.model.icon.iconSet = iconSet;
    this.iconSet = iconSet;
    this.notifyPropertyChange('isInvalid');
  }

  @action
  changeDescriptionValue(title, description) {
    this.description = description;
  }

  @action
  save() {
    const blocks = [
      {
        type: 'header',
        data: {
          text: this.model.icon.name,
          level: 1,
        },
      },
      ...this.description.blocks,
    ];

    this.model.icon.description = {
      blocks,
    };

    return this.model.icon.save().then(
      (icon) => {
        return this.transitionToRoute(
          'manage.icons.edit',
          icon.iconSet.get('id'),
          icon.get('id')
        );
      },
      (err) => {
        this.notifications.handleError(err);
      }
    );
  }
}
