import Controller from '../../base/manage-edit-controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import PictureMeta from '../../../lib/picture-meta';

export default class ManageArticlesEditController extends Controller {
  @service store;

  get editableModel() {
    return this.model.article;
  }

  @action
  onPictureUpload(value, type) {
    if (type == 'url') {
      return {
        success: 1,
        file: {
          url: value,
        },
      };
    }

    return new Promise((resolve) => {
      var reader = new FileReader();
      reader.readAsDataURL(value);

      reader.onload = (event) => {
        const picture = this.store.createRecord('picture', {
          name: value.name,
          picture: event.target.result,
          meta: new PictureMeta(),
        });

        return picture
          .save()
          .then(() => {
            resolve({
              success: 1,
              file: {
                url: picture.picture + '/lg',
              },
            });
          })
          .catch(() => {
            resolve({
              success: 0,
              file: {
                url: '',
              },
            });
          });
      };

      reader.onerror = function () {
        resolve({
          success: 0,
          file: {
            url: '',
          },
        });
      };
    });
  }
}
