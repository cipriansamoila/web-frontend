import MapController from '../../base/map-controller';
import { debounce } from '@ember/runloop';
import { Promise } from 'rsvp';
import IconsSubSet from '../../../subsets/icons';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageSitesEditController extends MapController {
  queryParams = ['parentMap'];

  @service store;
  @service notifications;
  @service intl;
  @service fastboot;

  @tracked editableField;
  @tracked savingField;
  @tracked iconsSubset;

  setupIconsSubsets() {
    if (this.fastboot.isFastBoot) {
      return;
    }

    if (!this.iconsSubset) {
      this.iconsSubset = IconsSubSet.create();
    }

    this.iconsSubset.set('feature', this.model.feature);
    this.iconsSubset.set('store', this.store);
    this.iconsSubset.update();
  }

  get disabled() {
    return this.fastboot.isFastBoot || this.model.feature.isSaving;
  }

  get extent() {
    if (!this.model.feature.maps || !this.model.feature.maps.length) {
      return null;
    }

    return this.model.feature.maps.objectAt(0).area;
  }

  get breadcrumbs() {
    if (!this.model.parentMap) {
      return [
        {
          route: 'browse',
          text: this.intl.t('browse'),
          capitalize: true,
        },
        {
          route: 'browse.maps',
          text: this.intl.t('maps'),
          capitalize: true,
        },
        {
          route: 'browse.sites.site',
          model: this.model.feature.id,
          text: this.model.feature.name,
        },
        {
          text: this.intl.t('edit-label'),
          capitalize: true,
        },
      ];
    }

    return [
      {
        route: 'browse',
        text: this.intl.t('browse'),
        capitalize: true,
      },
      {
        route: 'browse.maps',
        text: this.intl.t('maps'),
        capitalize: true,
      },
      {
        route: 'browse.sites',
        query: { map: this.model.parentMap.id },
        text: this.model.parentMap.name,
      },
      {
        route: 'browse.sites.site',
        model: this.model.feature.id,
        query: { map: this.model.parentMap.id },
        text: this.model.feature.name,
      },
      {
        text: this.intl.t('edit-label'),
        capitalize: true,
      },
    ];
  }

  progress(picture, percentage) {
    picture.progress = percentage;
  }

  @action
  updatePictures(pictures) {
    this.savingField = 'photos';

    const promises = pictures
      .filter((a) => a.isNew)
      .map((picture) => {
        return picture.save({
          adapterOptions: {
            progress: (percentage) => {
              this.progress?.(picture, percentage);
            },
          },
        });
      });

    this.model.feature.pictures.setObjects(pictures);

    return Promise.all(promises)
      .then(() => {
        return this.model.feature.save();
      })
      .finally(() => {
        this.savingField = '';
      });
  }

  @action
  updateSounds(sounds) {
    this.savingField = 'sounds';

    const promises = sounds
      .filter((a) => a.isNew)
      .map((sound) => {
        return sound.save({
          adapterOptions: {
            progress: (percentage) => {
              this.progress(sound, percentage);
            },
          },
        });
      });

    this.model.feature.sounds.setObjects(sounds);

    return Promise.all(promises)
      .then(() => {
        return this.model.feature.save();
      })
      .finally(() => {
        this.savingField = '';
      });
  }

  @action
  imageChange(picture) {
    if (picture.isNew) return;

    return picture.save();
  }

  get editTitleAndDescription() {
    return this.editableField == 'titleAndDescription';
  }

  get editLocation() {
    return this.editableField == 'location';
  }

  get savingTitleAndDescription() {
    return this.savingField == 'titleAndDescription';
  }

  get savingLocation() {
    return this.savingField == 'location';
  }

  get savingIcons() {
    return this.savingField == 'icons';
  }

  get niceMapList() {
    return this.model.feature.maps.map((a) => a.get('name')).join(', ');
  }

  get niceIsPublished() {
    return this.model.feature.isPublished
      ? this.intl.t('yes')
      : this.intl.t('no');
  }

  performIconSearch(term, resolve, reject) {
    this.store
      .query('icon', { term: term })
      .then((data) => {
        resolve(data);
      })
      .catch(reject);
  }

  @action
  save() {
    this.savingField = this.editableField;
    this.editableField = '';

    return this.model.feature
      .save({
        adapterOptions: {
          validation: false,
        },
      })
      .then(
        () => {
          if (this.savingField == 'Maps') {
            this.iconsSubset.update();
          }

          this.savingField = '';
        },
        (err) => {
          this.model.feature.rollbackAttributes();
          this.notifications.handleError(err);
          this.savingField = '';
        }
      );
  }

  @action
  saveMaps(title, value) {
    this.model.feature.maps = value;
    return this.save();
  }

  @action
  createImage() {
    return this.store.createRecord('picture', { name: '', picture: '' });
  }

  @action
  createSound() {
    return this.store.createRecord('sound', {
      name: '',
      feature: this.model.feature.get('id'),
      sound: '',
    });
  }

  @action
  edit(key) {
    this.editableField = key;
  }

  @action
  searchIcons(term) {
    return new Promise((resolve, reject) => {
      debounce(this, this.performIconSearch, term, resolve, reject, 600);
    });
  }

  @action
  iconsChanged(value) {
    this.model.feature.icons = value;
    this.editableField = 'icons';

    return this.save();
  }

  @action
  cancel() {
    this.model.feature.restore();
    this.editableField = '';
  }

  @action
  saveVisibility(newValue) {
    this.model.feature.visibility = newValue;
    this.editableField = this.intl.t('visibility');

    return this.save();
  }

  @action
  saveArticle(name, description) {
    this.model.feature.name = name;
    this.model.feature.description = description;

    return this.save();
  }

  @action
  savePosition(value) {
    this.model.feature.position = value;
    return this.save();
  }

  @action
  saveAttributes(value) {
    this.model.feature.attributes = value;
    return this.save();
  }

  @action
  onDelete() {
    return this.model.feature
      .destroyRecord()
      .then(() => {
        if (this.parentMap) {
          this.transitionToRoute('browse.sites', {
            queryParams: {
              map: this.model.parentMap.id,
            },
          });

          return true;
        }

        this.transitionToRoute('browse.sites');
        return true;
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }
}
