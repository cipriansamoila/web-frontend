import Controller from '@ember/controller';
import { action } from '@ember/object';

export default class ManageDashboardsDashboardController extends Controller {
  @action
  browseIconSet(id) {
    this.transitionToRoute('browse.icons.set', id);
  }

  @action
  browseMap(map) {
    this.transitionToRoute('browse.sites.index', { queryParams: { map } });
  }

  @action
  browseCampaign(id) {
    this.transitionToRoute('campaigns.campaign', id);
  }

  @action
  deleteRecord(record) {
    return record.destroyRecord();
  }

  get showCreateMapAlert() {
    if (this.model.team.isPublisher) {
      return false;
    }

    return this.model.maps.length == 0;
  }

  get articles() {
    let groups = {};

    this.model.articles?.forEach((article) => {
      let key;

      if (article.slug.indexOf('notification-') == 0) {
        key = 'notifications';
      } else {
        key = article.slug.split('--')[0] ?? 'other';
      }

      if (!groups[key]) {
        groups[key] = [];
      }

      groups[key].push(article);
    });

    return groups;
  }
}
