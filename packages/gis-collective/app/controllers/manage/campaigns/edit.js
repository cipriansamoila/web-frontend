import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import PictureMeta from '../../../lib/picture-meta';
import AlertMessage from '../../../lib/alert-message';

export default class ManageCampaignsEditController extends Controller {
  @service notifications;
  @service intl;
  @service user;
  @service store;

  @tracked editableField;
  @tracked savingField;
  @tracked allTeams = false;

  queryParams = ['allTeams'];

  get breadcrumbs() {
    return [
      { text: this.intl.t('campaigns'), capitalize: true, route: 'campaigns' },
      {
        route: 'campaigns.campaign',
        model: this.model.campaign.id,
        text: this.model.campaign.name,
      },
      {
        text: this.intl.t('edit-label'),
        capitalize: true,
      },
    ];
  }

  checkDuplicateNames(icons, otherList, alertId) {
    const list = [];

    if (!alertId) {
      alertId = 'alert-duplicate-icon-name';
    }

    icons.forEach((icon) => {
      const others = otherList ?? icons.filter((b) => b.id != icon.id);
      const duplicate = others.find((b) => b.name == icon.name);

      if (duplicate) {
        list.push(
          new AlertMessage(
            this.intl.t(`message-${alertId}`, {
              name: icon.name,
              iconSet: icon.get('iconSet.name'),
            }),
            'danger',
            alertId,
            () => {
              icons.removeObject(icon);
              this.save();
            }
          )
        );
      }
    });

    return list;
  }

  get alertList() {
    const list = [];
    const icons = this.model.campaign.icons;
    const optionalIcons = this.model.campaign.optionalIcons;

    list.addObjects(this.checkDuplicateNames(icons));
    list.addObjects(this.checkDuplicateNames(optionalIcons));
    list.addObjects(
      this.checkDuplicateNames(
        icons,
        optionalIcons,
        'alert-duplicate-icon-name-default'
      )
    );
    list.addObjects(
      this.checkDuplicateNames(
        optionalIcons,
        icons,
        'alert-duplicate-icon-name-optional'
      )
    );

    return list;
  }

  @action
  saveFeatureNamePrefix(value) {
    if (!this.model.campaign.options) {
      this.model.campaign.options = {};
    }

    this.model.campaign.options.featureNamePrefix = value;
    return this.save();
  }

  @action
  saveRegistrationMandatory(value) {
    const options = this.model.campaign.options ?? {};
    options.registrationMandatory = value;

    return this.saveQuestions(options);
  }

  @action
  createImage() {
    return this.store.createRecord('picture', {
      name: '',
      picture: '',
      meta: new PictureMeta(),
    });
  }

  @action
  saveMap(newValue) {
    this.model.campaign.set('map', newValue);
    return this.save();
  }

  @action
  saveCover(index, value) {
    this.model.campaign.cover = value;
    return this.save();
  }

  @action
  saveStartDate(newValue) {
    this.model.campaign.startDate = newValue;
    return this.save();
  }

  @action
  saveEndDate(newValue) {
    this.model.campaign.endDate = newValue;
    return this.save();
  }

  @action
  saveTeam(newValue) {
    this.model.campaign.visibility.team = newValue;
    this.set('editableField', this.intl.t('team'));

    return this.save();
  }

  @action
  saveIsPublic(newValue) {
    this.model.campaign.visibility.isPublic = newValue;
    this.editableField = this.intl.t('is public');

    return this.save();
  }

  @action
  saveIsDefault(newValue) {
    this.model.campaign.visibility.isDefault = newValue;
    this.editableField = this.intl.t('is default');

    return this.save();
  }

  @action
  saveArticle(name, article) {
    this.model.campaign.name = name;
    this.model.campaign.article = article;

    return this.save();
  }

  @action
  iconsChanged(newValue) {
    this.model.campaign.icons = newValue;

    return this.save();
  }

  @action
  saveQuestions(newValue, icons) {
    if (!this.model.campaign.options) {
      this.model.campaign.options = {};
    }

    Object.keys(newValue).forEach((key) => {
      this.model.campaign.options[key] = newValue[key];
    });

    if (icons) {
      this.model.campaign.optionalIcons = icons;
    }
    return this.save();
  }

  @action
  save() {
    this.savingField = this.editableField;
    this.editableField = '';

    return this.model.campaign.save().then(
      () => {
        this.savingField = '';
      },
      (err) => {
        this.notifications.handleError(err);
        this.savingField = '';
      }
    );
  }

  @action
  delete() {
    return this.model.campaign
      .destroyRecord()
      .then(this.transitionToRoute('campaigns'))
      .catch(this.notifications.handleError);
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.editableField = '';
    this._parentIcon = null;
    return this.model.campaign.rollbackAttributes();
  }
}
