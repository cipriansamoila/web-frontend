import MembersAdmin from '../../base/members-admin';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageTeamsAddController extends MembersAdmin {
  queryParams = ['next'];

  @service notifications;
  @service user;
  @service intl;

  @tracked isSaving = false;
  @tracked _description;

  get description() {
    if (this._description) {
      return this._description;
    }

    return {
      blocks: [
        {
          type: 'paragraph',
          data: {
            text: this.intl.t('article-placeholders.add-team'),
          },
        },
      ],
    };
  }

  set description(value) {
    this._description = value;
  }

  get isInvalid() {
    return String(this.model.name).trim() == '';
  }

  @action
  changeAboutValue(title, description) {
    this.description = description;
  }

  @action
  async save() {
    const blocks = [
      {
        type: 'header',
        data: {
          text: this.model.name,
          level: 1,
        },
      },
      ...this.description.blocks,
    ];

    this.model.about = {
      blocks,
    };

    try {
      const result = await this.model.save();
      await this.user.loadTeams();

      if (this.next) {
        return this.transitionToRoute(this.next);
      }

      return this.transitionToRoute('manage.teams.edit', result.id);
    } catch (err) {
      this.notifications.handleError(err);
    }
  }
}
