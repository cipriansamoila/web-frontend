import MembersAdmin from '../../base/members-admin';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { all } from 'rsvp';

export default class ManageTeamsAddController extends MembersAdmin {
  @service notifications;
  @service intl;

  roleList = ['owners', 'leaders', 'members', 'guests'];
  @tracked selectedUsers = [];
  @tracked newRole = 'guests';
  @tracked editableField = '';
  @tracked savingField = '';

  get breadcrumbs() {
    return [
      {
        route: 'manage.teams',
        text: this.intl.t('your teams'),
        capitalize: true,
      },
      { text: this.intl.t('edit-model.title', { name: this.model.name }) },
    ];
  }

  save() {
    this.savingField = this.editableField;
    this.editableField = '';

    this.model.save().then(
      () => {
        this.savingField = '';
      },
      (err) => {
        this.notifications.handleError(err);
        this.savingField = '';
      }
    );
  }

  removeMemberFrom(listName, item) {
    const list = this.model.get(listName);

    list.setObjects(list.filter((a) => a.email != item.email));
  }

  removeMember(item) {
    this.roleList.forEach((role) => {
      this.removeMemberFrom(role, item);
    });
  }

  get members() {
    let result = [];

    this.roleList.forEach((role) => {
      const items = this.model.get(role).map((user) => ({ user, role }));
      result = result.concat(items);
    });

    return result;
  }

  @action
  remove(item) {
    return this.notifications
      .ask({
        title: this.intl.t('remove member'),
        question: this.intl.t('remove-member-message', { name: item.name }),
      })
      .then(() => {
        this.removeMember(item);
        this.save();
      })
      .catch(() => {});
  }

  @action
  updateRole(item, role) {
    this.removeMember(item);
    this.model.get(role).addObject(item);
    return this.save();
  }

  @action
  selectNewRole(event) {
    const value = event.target.value;
    this.newRole = value;
  }

  @action
  createImage() {
    return this.store.createRecord('picture', {
      name: 'team logo',
      picture: '',
    });
  }

  @action
  selectedUsersChanged(value) {
    this.selectedUsers = value;
  }

  @action
  addUsers() {
    this.selectedUsers.forEach((user) => {
      this.removeMember(user);
    });

    if (!this.newRole) {
      return;
    }

    this.model.get(this.newRole).addObjects(this.selectedUsers);
    this.selectedUsers = [];
    this.save();
  }

  @action
  togglePublished() {
    this.model.set('isPublic', !this.model.isPublic);
  }

  @action
  saveIsPublic(newValue) {
    this.model.set('isPublic', newValue);
    this.set('editableField', this.intl.t('is public'));

    return this.save();
  }

  @action
  requestProfile(id) {
    return this.store.findRecord('user-profile', id);
  }

  @action
  updatePictures(pictures) {
    var promises = [];

    pictures
      .filter((a) => a.isNew)
      .forEach((picture) => {
        promises.push(
          picture.save({
            adapterOptions: {
              progress: (percentage) => {
                this.progress?.(picture, percentage);
              },
            },
          })
        );
      });

    this.model.pictures.setObjects(pictures);

    return all(promises).then(
      () => {
        return this.model.save().then(
          () => {},
          (err) => {
            this.notifications.handleError(err);
          }
        );
      },
      (err) => {
        return this.notifications.handleError(err);
      }
    );
  }

  @action
  imageChange(picture) {
    if (picture.isNew) return;

    return picture.save();
  }

  @action
  saveLogo(index, value) {
    this.model.set('logo', value);
    return this.save();
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.editableField = '';
    return this.model.rollbackAttributes();
  }

  @action
  saveArticle(name, about) {
    this.model.set('name', name);
    this.model.set('about', about);

    return this.save();
  }

  @action
  delete() {
    return this.model
      .destroyRecord()
      .then(() => {
        return this.transitionToRoute('manage.teams.index');
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }
}
