import BrowseController from '../../base/browse-controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageTeams extends BrowseController {
  @tracked teamSuggestions = [];
  @tracked search = '';

  performSuggestions(term, resolve, reject) {
    if (term.length <= 4) {
      this.mapSuggestions = [];
      return resolve([]);
    }

    return this.store
      .query('team', { limit: 5, term })
      .then((result) => {
        this.teamSuggestions = result.map((a) => a.name);
        resolve(result);
      })
      .catch(reject);
  }

  @action
  delete(team) {
    return team.destroyRecord();
  }

  resetController(controller) {
    controller.set('search', '');
    controller.isSearching = false;
  }

  didTransition() {
    this.controller.isSearching = false;
  }
}
