import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageBaseMapsAddController extends Controller {
  @service notifications;
  @service intl;

  @tracked isInvalid = true;

  get name() {
    return this.model.baseMap.name;
  }

  set name(value) {
    this.model.baseMap.set('name', value);
    this.updateIsInvalid();
  }

  get icon() {
    return this.model.baseMap.icon;
  }

  set icon(value) {
    this.model.baseMap.set('icon', value);
    this.updateIsInvalid();
  }

  updateIsInvalid() {
    const hasTeam = this.model.baseMap.visibility.team;

    this.isInvalid =
      String(this.name).trim() == '' ||
      String(this.icon).trim() == '' ||
      !hasTeam;
  }

  @action
  changeTeam(team) {
    this.model.baseMap.set('visibility.team', team);
    this.updateIsInvalid();
  }

  @action
  save() {
    this.model.baseMap
      .save()
      .then((result) => {
        return this.transitionToRoute('manage.basemaps.edit', result.id);
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }
}
