import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageBaseMapsEditController extends Controller {
  @service store;
  @service notifications;
  @service ol;
  @service intl;
  @service user;

  @tracked editableField;
  @tracked savingField;
  @tracked allTeams = false;

  queryParams = ['allTeams'];

  get breadcrumbs() {
    return [
      {
        route: 'manage.dashboards.dashboard',
        model: this.model.team.get('id'),
        text: this.intl.t('dashboard'),
        capitalize: true,
      },
      {
        text: this.intl.t('edit-model.title', {
          name: this.model.baseMap.name,
        }),
        capitalize: true,
      },
    ];
  }

  get attributions() {
    try {
      return this.model.baseMap.attributions.slice();
    } catch (err) {
      return [];
    }
  }

  @action
  changeAll(newValue) {
    this.allTeams = newValue;
  }

  @action
  save() {
    this.savingField = this.editableField;
    this.editableField = '';

    return this.model.baseMap.save().then(
      () => {
        this.savingField = '';
      },
      (err) => {
        this.notifications.handleError(err);
        this.savingField = '';
      }
    );
  }

  @action
  createImage() {
    return this.store.createRecord('picture', {
      name: 'base map',
      picture: '',
    });
  }

  @action
  saveAttributions(value) {
    this.model.baseMap.set('attributions', value);
    return this.save();
  }

  @action
  edit(title) {
    this.editableField = title;
  }

  @action
  cancel() {
    this.model.baseMap.rollbackAttributes();
    this.editableField = '';
  }

  @action
  mapScreenshot() {
    return this.ol
      .screenshot()
      .then((data) => {
        const picture = this.createImage();
        picture.set('picture', data);

        return picture.save().then((result) => {
          this.model.baseMap.set('cover', result);
        });
      })
      .then(() => {
        this.save();
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }

  @action
  delete() {
    return this.model.baseMap.destroyRecord().then(() => {
      return this.transitionToRoute(
        'manage.dashboards.dashboard',
        this.model.team.id
      );
    });
  }

  @action
  saveCover(index, value) {
    this.model.baseMap.set('cover', value);
    return this.save();
  }

  @action
  saveLayers(value) {
    this.model.baseMap.set('layers', value);

    return this.save();
  }

  @action
  saveIsPublic(newValue) {
    this.model.baseMap.visibility.isPublic = newValue;
    this.set('editableField', this.intl.t('is public'));

    return this.save();
  }

  @action
  saveTeam(newValue) {
    this.model.baseMap.visibility.team = newValue;
    this.set('editableField', this.intl.t('team'));

    return this.save();
  }

  @action
  saveIsDefault(newValue) {
    this.model.baseMap.visibility.isDefault = newValue;
    this.set('editableField', this.intl.t('is default'));

    return this.save();
  }

  @action
  saveName(newValue) {
    this.model.baseMap.set('name', newValue);

    return this.save();
  }
}
