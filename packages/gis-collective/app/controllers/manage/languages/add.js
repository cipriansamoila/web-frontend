import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class ManageLanguagesAddController extends Controller {
  @tracked fileSelectorLabel;
  @service notifications;
  @service intl;

  get isInvalid() {
    return (
      !this.model.hasDirtyAttributes ||
      this.model.name.trim() == '' ||
      this.model.locale.trim() == '' ||
      this.model.file.trim() == ''
    );
  }

  @action
  save() {
    this.model.save().then(
      () => {
        return this.transitionToRoute('manage.languages.index');
      },
      (err) => {
        this.notifications.handleError(err);
      }
    );
  }

  @action
  parseFile(fileName, content) {
    this.model.file = content;
  }
}
