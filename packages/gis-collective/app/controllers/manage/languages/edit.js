import ManageLanguagesAddController from './add';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class ManageLanguagesEditController extends ManageLanguagesAddController {
  @service intl;

  get breadcrumbs() {
    return [
      {
        route: 'manage.languages',
        text: this.intl.t('languages'),
        capitalize: true,
      },
      { text: this.intl.t('edit-model.title', { name: this.model.name }) },
    ];
  }

  @action
  onChangeCustomTranslations(value) {
    this.model.customTranslations = value;
  }
}
