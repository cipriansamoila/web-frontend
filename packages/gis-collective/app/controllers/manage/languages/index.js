import Controller from '@ember/controller';
import { action } from '@ember/object';

export default class ManageLanguagesIndexController extends Controller {
  @action
  delete(item) {
    return item.destroyRecord();
  }
}
