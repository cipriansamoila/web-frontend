import Controller from '../../base/manage-edit-controller';

export default class ManagePresentationsEditController extends Controller {
  get editableModel() {
    return this.model.presentation;
  }
}
