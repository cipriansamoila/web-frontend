import Controller from './base';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { PageContainer } from '../../../transforms/page-container-list';

export default class ManagePagesContentController extends Controller {
  @tracked selectedCol;
  @tracked selectedContainer = false;
  @tracked configurePage = false;

  get pageContainer() {
    if (this.selectedContainer === false) {
      return null;
    }

    const containers = this.model.page.containers ?? [];

    return containers[this.selectedContainer] ?? new PageContainer();
  }

  @action
  hideOfCanvas() {
    this.configurePage = false;
    this.selectedContainer = false;
  }

  @action
  selectContainer(index) {
    this.selectedContainer = index;
  }

  @action
  changeContainer(newContainer) {
    let containers = this.model.page.get('containers');

    if (!containers?.length) {
      containers = this.model.layout.containers?.map(() => ({})) ?? [];
    }

    containers.replace(this.selectedContainer, 1, [newContainer]);

    this.model.page.containers = containers;
  }

  @action
  configPage() {
    this.configurePage = true;
  }

  @action
  saveContent() {
    return this.model.page.save();
  }

  @action
  async cloneContent() {
    const newPage = await this.model.page.clone();

    this.model.layout.unloadRecord();
    this.model.page.unloadRecord();

    this.router.transitionTo('manage.pages.content', newPage.id);
  }

  @action
  selectCol(col) {
    this.selectedCol = col;
  }

  @action
  async changeCol(newData, newType) {
    this.selectedCol.data = newData;
    this.selectedCol.type = newType;

    let exists = false;

    this.model.page.cols.forEach((col, index) => {
      if (
        col.container === this.selectedCol.container &&
        col.row === this.selectedCol.row &&
        col.col === this.selectedCol.col
      ) {
        exists = index;
      }
    });

    let updateModel;

    if (exists === false && newType) {
      this.model.page.cols.addObject(this.selectedCol);
      updateModel = true;
    }

    if (exists !== false && exists >= 0 && newType) {
      this.model.page.cols = this.model.page.cols.replace(exists, 1, [
        this.selectedCol,
      ]);
      updateModel = true;
    }

    if (exists !== false && exists >= 0 && !newType) {
      this.model.page.cols = this.model.page.cols.removeAt(exists);
    }

    if (updateModel) {
      this.model[this.selectedCol.modelKey] = await this.selectedCol.fetch?.();
    }
  }
}
