import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class ManagePagesBaseController extends Controller {
  @tracked deviceSize;
  @service router;

  get deviceSizeClass() {
    if (!this.deviceSize) {
      return '';
    }

    return `enforce-${this.deviceSize}-size`;
  }

  @action
  changeDeviceSize(value) {
    this.deviceSize = value;
  }
}
