import Controller from '../../base/manage-edit-controller';

export default class ManagePagesEditController extends Controller {
  get editableModel() {
    return this.model.page;
  }
}
