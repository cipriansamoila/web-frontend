import Controller from './base';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';
import { inject as service } from '@ember/service';

export default class ManagePagesLayoutController extends Controller {
  @service store;
  @tracked _containers;
  @tracked _layout;
  @tracked selectedContainer;
  @tracked selectedCol;
  @tracked selectedRow;
  @tracked configureLayout;
  updatePage = false;

  get containers() {
    if (this._containers) {
      return this._containers;
    }

    return this.layout.containers;
  }

  get layout() {
    if (this._layout) {
      return this._layout;
    }

    return this.model.layout;
  }

  @action
  hideOfCanvas() {
    later(() => {
      this.selectedContainer = null;
      this.selectedCol = null;
      this.selectedRow = null;
      this.configureLayout = null;
    });
  }

  @action
  configLayout() {
    this.configureLayout = false;
    this.configureLayout = true;
  }

  @action
  async saveLayout() {
    if (this._containers) {
      this.layout.containers = this._containers;
    }

    await this.layout.save();

    if (this.updatePage) {
      await this.model.page.save();
      this.updatePage = false;
    }
  }

  @action
  async cloneLayout() {
    if (this._containers) {
      this.layout.containers = this._containers;
    }

    this.model.page.layout = this.layout;
    const newPage = await this.model.page.clone();

    this.layout.rollbackAttributes();
    this.model.page.rollbackAttributes();

    this.router.transitionTo('manage.pages.layout', newPage.id);
  }

  @action
  onColSelect(containerIndex, rowIndex, colIndex) {
    this.containerIndex = containerIndex;
    this.rowIndex = rowIndex;
    this.colIndex = colIndex;
    this.selectedCol =
      this.containers[containerIndex].rows[rowIndex].cols[colIndex];
    this.selectedRow = null;
    this.selectedContainer = null;
    this.configureLayout = null;

    if (!this._containers) {
      this._containers = this.containers;
    }
  }

  @action
  onRowEdit(containerIndex, rowIndex) {
    this.containerIndex = containerIndex;
    this.rowIndex = rowIndex;
    this.selectedCol = null;
    this.selectedContainer = null;
    this.selectedRow = this.containers[containerIndex].rows[rowIndex];
    this.configureLayout = null;

    if (!this._containers) {
      this._containers = this.containers;
    }
  }

  @action
  onContainerEdit(containerIndex) {
    this.containerIndex = containerIndex;
    this.rowIndex = null;
    this.selectedCol = null;
    this.selectedRow = null;
    this.selectedContainer = this.containers[containerIndex];
    this.configureLayout = null;

    if (!this._containers) {
      this._containers = this.containers;
    }
  }

  @action
  changeColOptions(options) {
    this.selectedCol.options = options;
  }

  @action
  changeRowOptions(options) {
    this.selectedRow.options = options;
  }

  @action
  changeContainerOptions(options) {
    this.selectedContainer.options = options;
  }

  @action
  layoutChange(
    newContainers,
    operation,
    containerIndex,
    rowOperation,
    rowIndex,
    colIndex
  ) {
    this._containers = newContainers;

    if (operation == 'containerBefore') {
      for (let col of this.model.page.cols) {
        if (col.container < containerIndex) {
          continue;
        }

        col.container++;
      }

      // eslint-disable-next-line no-self-assign
      this.model.page.cols = this.model.page.cols;
      this.updatePage = true;
    }

    if (operation == 'removeContainer') {
      this.model.page.cols = this.model.page.cols.filter(
        (a) => a.container != containerIndex
      );

      for (let col of this.model.page.cols) {
        if (col.container < containerIndex) {
          continue;
        }

        col.container--;
      }

      // eslint-disable-next-line no-self-assign
      this.model.page.cols = this.model.page.cols;
      this.updatePage = true;
    }

    if (rowOperation == 'addRowBefore') {
      for (let col of this.model.page.cols) {
        if (col.container != containerIndex || col.row < rowIndex) {
          continue;
        }

        col.row++;
      }

      // eslint-disable-next-line no-self-assign
      this.model.page.cols = this.model.page.cols;
      this.updatePage = true;
    }

    if (rowOperation == 'removeRow') {
      for (let col of this.model.page.cols) {
        if (col.container != containerIndex || col.row < rowIndex) {
          continue;
        }

        col.row--;
      }

      // eslint-disable-next-line no-self-assign
      this.model.page.cols = this.model.page.cols;
      this.updatePage = true;
    }

    if (rowOperation == 'deleteCol') {
      this.model.page.cols = this.model.page.cols.filter(
        (a) =>
          a.container != containerIndex ||
          a.row != rowIndex ||
          (a.container == containerIndex &&
            a.row == rowIndex &&
            a.col != colIndex)
      );
      this.updatePage = true;
    }
  }
}
