import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
  modal: service(),
  session: service(),
  notifications: service(),
  intl: service(),

  actions: {
    delete() {
      this.modal
        .confirmWithPassword(
          this.intl.t('delete account', { size: 1 }),
          this.intl.t('Are you sure you want to delete this account?')
        )
        .then((password) => {
          this.model
            .removeUser(password)
            .then(() => {
              this.notifications.showMessage(
                this.intl.t('Remove user'),
                this.intl.t('You have successfully removed your user.')
              );
              this.session.invalidate();
              this.transitionToRoute('/');
            })
            .catch((err) => {
              this.notifications.handleError(err);
            });
        });
    },

    changePassword() {
      this.model
        .changePassword(...arguments)
        .then(() => {
          this.notifications.showMessage(
            this.intl.t('change password'),
            this.intl.t('You have successfully changed your password.')
          );
        })
        .catch((err) => {
          this.notifications.handleError(err);
        });
    },
  },
});
