import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class PreferencesTokensController extends Controller {
  @service notifications;
  @service intl;
  @service fastboot;

  @tracked name = null;
  @tracked expire = new Date();
  @tracked isLoading;
  @tracked newToken;

  get isDisabled() {
    if (this.isLoading) {
      return true;
    }

    if (!this.name || this.name.trim() == '') {
      return true;
    }

    const now = new Date();

    if (!this.expire || now.getTime() >= this.expire.getTime()) {
      return true;
    }

    return false;
  }

  @action
  async revoke(item) {
    await this.notifications.ask({
      title: this.intl.t('delete token', { size: 1 }),
      question: this.intl.t('delete-confirmation-message', {
        name: item.name,
        size: 1,
      }),
    });

    return this.model.user
      .revokeApiTokenByName(item.name)
      .then(() => {
        return this.model.user.updateTokens();
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }

  @action
  clearToken() {
    this.newToken = null;
  }

  @action
  updateDate(date) {
    this.expire = date;
  }

  @action
  async createToken() {
    let expire = this.expire;
    expire.setHours(0, 0, 0, 0);
    this.isLoading = true;

    try {
      const result = await this.model.user.createToken(
        this.name,
        expire.toISOString()
      );

      this.isLoading = false;
      this.newToken = result['token'];

      return await this.model.user.updateTokens();
    } catch (err) {
      this.notifications.handleError(err);
      this.isLoading = false;
    }

    return false;
  }
}
