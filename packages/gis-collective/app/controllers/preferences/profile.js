import Controller from '@ember/controller';
import { action } from '@ember/object';
import PictureMeta from '../../lib/picture-meta';
import { inject as service } from '@ember/service';

export default class PreferencesProfileController extends Controller {
  @service fastboot;
  @service store;

  @action
  update() {
    return this.model.profile.save();
  }

  get detailedLocation() {
    if (typeof this.model.detailedLocation?.value == 'string') {
      return this.model.detailedLocation?.value.toLowerCase() == 'true';
    }

    return this.model.detailedLocation?.value;
  }

  @action
  async createImage() {
    this.model.profile.picture = this.store.createRecord('picture', {
      name: '',
      picture: '',
      meta: new PictureMeta(),
    });

    return await this.model.profile.picture;
  }
}
