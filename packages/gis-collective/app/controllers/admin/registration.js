import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { all } from 'rsvp';
import { inject as service } from '@ember/service';

export default Controller.extend({
  notifications: service(),
  store: service(),
  isSaving: false,

  isDirty: computed.or(
    'model.registerEnabled.hasDirtyAttributes',
    'model.registerEmailDomains.hasDirtyAttributes',
    'model.registerMandatory.hasDirtyAttributes'
  ),

  isDisabled: computed('isDirty', 'isSaving', function () {
    return !this.isDirty || this.isSaving;
  }),

  actions: {
    save() {
      this.set('isSaving', true);
      this.set('isSavingError', false);

      all([
        this.model.registerEnabled.save(),
        this.model.registerEmailDomains.save(),
        this.model.registerMandatory.save(),
      ])
        .then(() => {
          this.set('isSaving', false);
          this.set('isSavingError', false);
        })
        .catch((err) => {
          this.set('isSaving', false);
          this.set('isSavingError', true);
          this.notifications.handleError(err);
        });
    },
  },
});
