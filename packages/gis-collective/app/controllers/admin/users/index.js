import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class AdminUsersIndexController extends Controller {
  queryParams = ['search'];
  @tracked search = '';

  @action
  performSearch(value) {
    this.search = value;
  }
}
