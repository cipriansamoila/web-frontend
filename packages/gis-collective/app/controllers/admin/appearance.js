import Controller from '@ember/controller';
import { all } from 'rsvp';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import GeoJson from '../../lib/geoJson';

export default class AdminAppearanceController extends Controller {
  @service notifications;
  @service store;
  @tracked isSaving = false;
  @tracked _logoImage = false;
  @tracked _coverImage = false;
  @tracked _selectedBaseMap;

  get logoImage() {
    return this._logoImage || this.model.logoImage;
  }

  set logoImage(value) {
    this._logoImage = value;
  }

  get coverImage() {
    return this._coverImage || this.model.coverImage;
  }

  set coverImage(value) {
    this._coverImage = value;
  }

  get isDirty() {
    return (
      this.model.appearanceLogo.hasDirtyAttributes ||
      this.model.appearanceCover.hasDirtyAttributes ||
      this.model.appearanceName.hasDirtyAttributes ||
      this.model.appearanceExtent.hasDirtyAttributes ||
      this.model.appearanceShowWelcomePresentation.hasDirtyAttributes ||
      this._logoImage ||
      this._coverImage
    );
  }

  get appearanceShowWelcomePresentation() {
    return this.model.appearanceShowWelcomePresentation.value == 'true';
  }

  set appearanceShowWelcomePresentation(value) {
    this.model.appearanceShowWelcomePresentation.value = value
      ? 'true'
      : 'false';
  }

  get isDisabled() {
    return !this.isDirty || this.isSaving;
  }

  get selectedBaseMap() {
    if (this._selectedBaseMap) {
      return this._selectedBaseMap;
    }

    if (!this.model.baseMaps || this.model.baseMaps.length == 0) {
      return null;
    }

    return this.model.baseMaps.firstObject;
  }

  get mapExtent() {
    const extentArray = JSON.parse(this.model.appearanceExtent.value);
    const extentPolygon = {
      type: 'Polygon',
      coordinates: [
        [
          [extentArray[0], extentArray[1]],
          [extentArray[0], extentArray[3]],
          [extentArray[2], extentArray[3]],
          [extentArray[2], extentArray[1]],
        ],
      ],
    };
    return new GeoJson(extentPolygon);
  }

  set mapExtent(value) {
    this.model.appearanceExtent.value = JSON.stringify(value.toExtent());
  }

  set selectedBaseMap(value) {
    this._selectedBaseMap = value;
  }

  @action
  updateMapExtent(value) {
    this.mapExtent = value;
  }

  @action
  selectBaseMap(value) {
    this.selectedBaseMap = value;
  }

  @action
  createLogoImage() {
    const image = this.store.createRecord('picture', {
      name: 'Service Logo',
      picture: '',
    });

    this.logoImage = image;

    return image;
  }

  @action
  createCoverImage() {
    const image = this.store.createRecord('picture', {
      name: 'Service Cover',
      picture: '',
    });

    this.coverImage = image;

    return image;
  }

  @action
  async save() {
    this.isSaving = true;
    this.isSavingError = false;

    try {
      if (this._logoImage) {
        await this._logoImage.save();
        this.model.appearanceLogo.value = this._logoImage.id;
      }

      if (this._coverImage) {
        await this._coverImage.save();
        this.model.appearanceCover.value = this._coverImage.id;
      }

      await all([
        this.model.appearanceLogo.save(),
        this.model.appearanceCover.save(),
        this.model.appearanceName.save(),
        this.model.appearanceExtent.save(),
        this.model.appearanceShowWelcomePresentation.save(),
      ]);
    } catch (err) {
      this.notifications.handleError(err);
      this.isSavingError = true;
    }

    this.isSaving = false;
  }
}
