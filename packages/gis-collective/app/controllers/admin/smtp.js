import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { all } from 'rsvp';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class AdminSmtpController extends Controller {
  @service notifications;
  @service user;

  @tracked isSaving;
  @tracked isError;

  @tracked SMTPAuthType = ['plain', 'none', 'login', 'cramMd5'];
  @tracked SMTPConnectionType = ['plain', 'startTLS', 'tls'];
  @tracked TLSVersion = [
    { name: 'Accept SSL v3 or TLS v1.0 and greater', value: 'any' },
    { name: 'DTLS v1.0', value: 'dtls1' },
    { name: 'Accept only SSL v3', value: 'ssl3' },
    { name: 'Accept only TLS v1.0', value: 'tls1' },
    { name: 'Accept only TLS v1.1', value: 'tls1_1' },
    { name: 'Accept only TLS v1.2', value: 'tls1_2' },
  ];
  @tracked TLSPeerValidationMode = [
    { name: 'Check the certificate for basic validity', value: 'checkCert' },
    {
      name: 'Validate the actual peer name/address against the certificate',
      value: 'checkPeer',
    },
    {
      name: 'Requires that the certificate or any parent certificate is trusted',
      value: 'checkTrust',
    },
    {
      name: 'Accept any peer regardless if and which certificate is presented',
      value: 'none',
    },
    {
      name: 'Require the peer to always present a certificate',
      value: 'requireCert',
    },
    {
      name: 'Require a valid and trusted certificate (strongly recommended)',
      value: 'trustedCert',
    },
    {
      name: 'Require a valid certificate matching the peer name',
      value: 'validCert',
    },
  ];

  get isDirty() {
    return (
      Object.keys(this.model).filter((a) => this.model[a].hasDirtyAttributes)
        .length > 0
    );
  }

  get isDisabled() {
    return !this.isDirty || this.isSaving;
  }

  @action
  sendTestMessage() {
    return this.model.user.testNotification();
  }

  @action
  save() {
    this.isSaving = true;

    return all(
      Object.keys(this.model)
        .filter((a) => this.model[a].hasDirtyAttributes)
        .map((a) => this.model[a].save())
    )
      .then(() => {
        this.isSaving = false;
      })
      .catch((err) => {
        this.isSaving = false;
        this.isError = true;

        this.notifications.handleError(err);
      });
  }
}
