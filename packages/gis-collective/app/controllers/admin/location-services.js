import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { all } from 'rsvp';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class AdminLocationServicesController extends Controller {
  @service notifications;
  @tracked isSaving = false;
  @tracked isError = false;

  maskingPrecision = [
    {
      value: '0',
    },
    {
      value: '1',
    },
    {
      value: '2',
    },
    {
      value: '3',
    },
    {
      value: '4',
    },
    {
      value: '5',
    },
  ];

  get isDirty() {
    return (
      Object.keys(this.model).filter((a) => this.model[a].hasDirtyAttributes)
        .length > 0
    );
  }

  get isDisabled() {
    return !this.isDirty || this.isSaving;
  }

  @action
  save() {
    this.isSaving = true;

    return all(
      Object.keys(this.model)
        .filter((a) => this.model[a].hasDirtyAttributes)
        .map((a) => this.model[a].save())
    )
      .then(() => {
        this.isSaving = false;
      })
      .catch((err) => {
        this.isSaving = false;
        this.notifications.handleError(err);
      });
  }
}
