import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { all } from 'rsvp';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class AdminCaptchaController extends Controller {
  @service notifications;
  @tracked isSaving;
  @tracked isError;

  status = [
    { name: 'disabled', value: 'false' },
    { name: 'reCaptcha', value: 'reCAPTCHA' },
    { name: 'mtCaptcha', value: 'mtCAPTCHA' },
  ];

  get isDirty() {
    return (
      Object.keys(this.model).filter((a) => this.model[a].hasDirtyAttributes)
        .length > 0
    );
  }

  get isDisabled() {
    return !this.isDirty || this.isSaving;
  }

  @action
  save() {
    this.isSaving = true;

    all(
      Object.keys(this.model)
        .filter((a) => this.model[a].hasDirtyAttributes)
        .map((a) => this.model[a].save())
    )
      .then(() => {
        this.isSaving = false;
      })
      .catch((err) => {
        this.isSaving = false;
        this.isError = true;

        this.notifications.handleError(err);
      });
  }
}
