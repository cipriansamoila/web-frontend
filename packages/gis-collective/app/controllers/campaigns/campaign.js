import Controller from '@ember/controller';
import { htmlSafe } from '@ember/template';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import {
  addIconSection,
  handleAttributeChange,
  handlePicturesChange,
  handlePositionChange,
  handleGroupRemove,
} from '../../attributes/actions';
import { all } from 'rsvp';
import AttributesGroup from '../../attributes/groups/attributes';
import { tracked } from '@glimmer/tracking';
import GeocodingSearch from '../../lib/geocoding-search';
import RequiredAttributes from '../../lib/required-attributes';

export default class ControllerIndexController extends Controller {
  @service intl;
  @service fastboot;
  @service fullscreen;
  @service session;
  @service position;
  @service store;

  @tracked submitDisabled = false;

  constructor() {
    super(...arguments);
    this.geocodingSearch = new GeocodingSearch(this.store);
    this.requiredAttributes = new RequiredAttributes();
  }

  get canContribute() {
    const registrationMandatory =
      this.model.campaign?.options?.registrationMandatory || false;
    return registrationMandatory ? this.session.isAuthenticated : true;
  }

  get optionalIconsGroup() {
    let name = this.model.campaign?.options?.iconsLabel ?? this.intl.t('icons');

    return {
      name,
      className: 'optional-icons',
    };
  }

  get descriptionLabel() {
    return (
      this.model.campaign?.options?.descriptionLabel ??
      this.intl.t('description')
    );
  }

  get nameLabel() {
    return this.model.campaign?.options?.nameLabel ?? this.intl.t('name');
  }

  get aboutGroup() {
    const attributes = [];

    if (this.model.campaign?.options?.showNameQuestion) {
      attributes.push({
        name: this.nameLabel,
        key: 'name',
        value: '',
        type: 'text',
      });
    }

    if (this.model.campaign?.options?.showDescriptionQuestion) {
      attributes.push({
        name: this.descriptionLabel,
        key: 'description',
        value: '',
        type: 'long text',
      });
    }

    if (attributes.length == 0) {
      return null;
    }

    const group = new AttributesGroup(this.intl);
    group.icon = { name: 'about', attributes };
    group.key = 'about';

    return group;
  }

  get contentBlocks() {
    const blocks = this.model.campaign.contentBlocks.blocks.filter(
      (a) => a.type != 'header' && a.data?.level != 1
    );

    return {
      blocks,
    };
  }

  get title() {
    return (
      this.model.campaign.contentBlocks.blocks.find(
        (a) => a.type == 'header' && a.data?.level == 1
      )?.data?.text ?? '---'
    );
  }

  get coverStyle() {
    const picture = this.model.campaign?.get('cover.picture');

    if (!picture) {
      return '';
    }

    return htmlSafe(`background-image: url('${picture}/md')`);
  }

  get positionDetails() {
    const attributes = this.model.answer?.attributes;

    if (!attributes) {
      return {};
    }

    return attributes['position details'];
  }

  get breadcrumbs() {
    const result = [
      {
        route: 'campaigns',
        text: this.intl.t('campaigns'),
        capitalize: true,
      },
    ];

    result.push({
      text: this.model.campaign.name,
    });

    return result;
  }

  get icons() {
    return this.model.campaign.icons;
  }

  handleAboutChange(key, value) {
    if (!this.model.answer.attributes['about']) {
      this.model.answer.attributes['about'] = {};
    }

    if (key == this.nameLabel) {
      this.model.answer.attributes['about']['name'] = value;
    }

    if (key == this.descriptionLabel) {
      this.model.answer.attributes['about']['description'] = value;
    }
  }

  get isDisabled() {
    return this.submitDisabled;
  }

  @action
  async searchGeocoding(term) {
    if (!term) {
      return this.geocodingSearch.clear();
    }

    return await this.geocodingSearch.search(term);
  }

  @action
  async changePosition(position, details) {
    await this.searchGeocoding(details['searchTerm']);

    if (this.geocodingSearch.hasNewResults) {
      details['address'] = this.geocodingSearch.results.firstObject.name;
      position = this.geocodingSearch.results.firstObject.geometry;
    }

    this.model.answer.position = position.center;
    this.model.answer.attributes['position details'] = details;
    this.model.answer.notifyPropertyChange?.('attributes');
  }

  @action
  handleAnswer(group, key, value) {
    switch (group.key) {
      case 'about':
        this.handleAboutChange(key, value);
        break;

      case 'position':
        handlePositionChange(this.model.answer, key, value);
        break;

      case 'pictures':
        handlePicturesChange(this.model.answer, key, value);
        break;

      default:
        handleAttributeChange(
          this.model.answer,
          group.key,
          group.index,
          key,
          value
        );
        this.checkRequiredAttributes();
        break;
    }
  }

  @action
  handleRemove(key, index) {
    handleGroupRemove(this.model.answer, key, index);
    this.model.iconAttributes.feature = this.model.answer;
    this.checkRequiredAttributes();
  }

  @action
  addSection(icon) {
    addIconSection(this.model.answer, icon);
    this.model.iconAttributes.feature = this.model.answer;
    this.checkRequiredAttributes();
  }

  @action
  publish() {
    this.model.campaign.visibility.isPublic = true;
    return this.model.campaign.save();
  }

  @action
  unpublish() {
    this.model.campaign.visibility.isPublic = false;
    return this.model.campaign.save();
  }

  @action
  changeIcons(icons) {
    this.model.answer.icons = icons;

    const names = icons.map((a) => a.name);
    names.push('position details');
    names.push('about');

    icons.forEach((icon) => {
      if (!this.model.answer.attributes[icon.name]) {
        addIconSection(this.model.answer, icon);
      }
    });

    Object.keys(this.model.answer.attributes)
      .filter((key) => names.indexOf(key) == -1)
      .forEach((key) => {
        delete this.model.answer.attributes[key];
      });

    this.model.iconAttributes.feature = this.model.answer;
  }

  resizePictures() {
    if (!this.model.answer.pictures.pictureOptimization) {
      return Promise.resolve([]);
    }

    return all(
      this.model.answer.pictures
        .filter((a) => a.isNew)
        .map((a) => a.localResize(1920, 1920))
    );
  }

  @action
  checkRequiredAttributes() {
    this.submitDisabled = this.requiredAttributes.validate(this.model.answer);
  }

  @action
  async submit() {
    await this.resizePictures();
    await all(this.model.answer.pictures.map((a) => a.save()));

    return this.model.answer.save().then(() => {
      return this.transitionToRoute(
        'campaigns.success',
        this.model.campaign.id
      );
    });
  }
}
