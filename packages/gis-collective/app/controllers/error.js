import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
  lastError: service(),
  session: service(),

  actions: {
    login() {
      this.session.invalidate().then(() => {
        this.transitionToRoute('login');
      });
    },
  },
});
