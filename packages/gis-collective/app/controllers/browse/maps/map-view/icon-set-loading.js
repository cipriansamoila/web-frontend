import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';

export default class BrowseMapsMapViewSpotlightController extends Controller {
  @tracked loadingIconSet;
}
