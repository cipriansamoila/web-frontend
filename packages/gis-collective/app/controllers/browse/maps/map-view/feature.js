import Controller from '@ember/controller';
import { htmlSafe } from '@ember/template';
import { inject as service } from '@ember/service';

export default class BrowseMapsMapViewFeatureController extends Controller {
  @service embed;

  get linkTarget() {
    return this.embed.isEnabled ? '_blank' : '_self';
  }

  get coverStyle() {
    if (
      !this.model.feature ||
      !this.model.feature.pictures ||
      this.model.feature.pictures.length == 0
    ) {
      return null;
    }

    const cover =
      this.model.feature.pictures.objectAt(0).get('picture') + '/lg';

    return htmlSafe(`background-image: url('${cover}')`);
  }
}
