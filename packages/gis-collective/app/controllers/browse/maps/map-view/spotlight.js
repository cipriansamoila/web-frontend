import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class BrowseMapsMapViewSpotlightController extends Controller {
  @service router;

  get hasData() {
    return Object.keys(this.icons).length > 0;
  }

  get icons() {
    const map = {};

    Object.keys(this.model.icons).forEach((name) => {
      const list = this.model.icons[name].toArray();

      if (list.length) {
        map[name] = list;
      }
    });

    return map;
  }

  @action
  onSelectIcon(icon) {
    this.router.transitionTo(
      'browse.maps.map-view',
      this.router.currentRoute.parent.params.id,
      {
        queryParams: {
          icons: icon.id,
          search: '',
        },
      }
    );
  }

  @action
  onDeselectIcon() {
    this.router.transitionTo(
      'browse.maps.map-view',
      this.router.currentRoute.parent.params.id,
      {
        queryParams: {
          icons: '',
          search: '',
        },
      }
    );
  }
}
