import Controller from './spotlight';
import { tracked } from '@glimmer/tracking';
import { iconGroupToList } from '../../../../lib/icons';

export default class BrowseMapsMapViewSpotlightController extends Controller {
  @tracked parent;

  get hasTerm() {
    return this.parent?.search?.length > 0;
  }

  get icons() {
    const list = iconGroupToList(this.model.icons);

    const lowerCaseTerm = this.parent?.search?.trim().toLowerCase();

    return list.filter(
      (a) =>
        a.name.toLowerCase().indexOf(lowerCaseTerm) != -1 ||
        a.localName.toLowerCase().indexOf(lowerCaseTerm) != -1
    );
  }
}
