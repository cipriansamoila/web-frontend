import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class BrowseMapsMapViewFilterIconsController extends Controller {
  @tracked parent;
  @service router;

  @action
  onSelectIcon(icon) {
    this.router.transitionTo(
      'browse.maps.map-view',
      this.router.currentRoute.parent.params.id,
      {
        queryParams: {
          icons: icon.id,
        },
      }
    );
  }

  @action
  onDeselectIcon() {
    this.router.transitionTo(
      'browse.maps.map-view',
      this.router.currentRoute.parent.params.id,
      {
        queryParams: {
          icons: '',
        },
      }
    );
  }
}
