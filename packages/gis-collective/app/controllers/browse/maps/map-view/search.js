import Controller from '@ember/controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class BrowseMapsMapViewSearchController extends Controller {
  queryParams = ['allFeatures'];

  @tracked allFeatures = null;

  @action
  showMap(map) {
    this.transitionToRoute('browse.maps.map-view', map.id);

    if (map.area && map.area.toOlFeature) {
      this.model.mapViewController.fitOlFeature(
        map.area.toOlFeature('EPSG:3857')
      );
    }
  }

  @action
  showPlace(place) {
    // eslint-disable-next-line no-console
    console.log('show place:', place);
    this.model.mapViewController.fit(place);
  }

  get licenses() {
    let result = {};

    this.model.places?.forEach((place) => {
      result[place.license.name] = place.license.url;
    });

    return Object.keys(result).map((name) => ({
      name,
      url: result[name],
    }));
  }

  get hasNoResults() {
    return (
      !this.model.featureBuckets?.columns?.firstObject?.length &&
      !this.model.mapBuckets?.columns?.firstObject?.length &&
      !this.model.places?.length
    );
  }

  get shortFeatureList() {
    if (this.model.mapId || this.allFeatures) {
      return null;
    }

    if (this.model.featureBuckets.columns.length == 0) {
      return null;
    }

    return this.model.featureBuckets.columns[0];
  }

  get featureCardOptions() {
    return {
      route: 'browse.maps.map-view.feature',
      onClick: (feature) => {
        this.showPlace(feature);
        this.transitionToRoute('browse.maps.map-view.feature', feature.id);
      },
    };
  }
}
