import MapController from '../../base/map-controller';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import Polygon from 'ol/geom/Polygon';
import { transform } from 'ol/proj';
import { debounce, cancel, later } from '@ember/runloop';
import { FeatureSelection } from '../../../lib/map/feature-selection';

export default class BrowseMapsMapViewController extends MapController {
  @service session;
  @service position;
  @service fastboot;
  @service router;
  @service mapStyles;
  @service embed;
  @service preferences;
  @service loading;

  @tracked showMapInfo = false;
  @tracked _term = null;
  @tracked _lon = null;
  @tracked _lat = null;
  @tracked _hasContent = null;
  @tracked isSearching;
  @tracked isFullscreen;

  @tracked featureSelection;

  _panelElement = null;
  _mapView = null;
  _mapLayer = null;
  mapViewAnimationDuration = 1000;

  constructor() {
    super(...arguments);

    this.featureSelection = new FeatureSelection();
  }

  get term() {
    if (this._term === null) {
      return this.search;
    }

    return this._term ?? this.search;
  }

  set term(value) {
    this._term = value;
  }

  @action
  fullscreenRequest(value) {
    this.isFullscreen = value;
  }

  @action
  toggleMapInfo() {
    this.showMapInfo = !this.showMapInfo;
  }

  @action
  setupLayer(map, layer) {
    this._mapLayer = layer;
    this._map = map;
  }

  redraw() {
    this._map.getLayers().forEach((layer) => {
      layer.changed();
    });
  }

  changeView() {
    if (!this._map) {
      return;
    }

    debounce(this, this.redraw, 500, true);
  }

  get hasFeatureContent() {
    return (
      this.router.currentRoute.name === 'browse.maps.map-view.feature' ||
      this.router.currentRoute.name === 'browse.maps.map-view.feature_loading'
    );
  }

  get hasContent() {
    if (this._hasContent !== null) {
      return this._hasContent;
    }

    if (!this.router.currentRoute) {
      return false;
    }

    return (
      this.router.currentRoute.name.indexOf('browse.maps.map-view.') == 0 &&
      this.router.currentRoute.name !== 'browse.maps.map-view.index'
    );
  }

  get tilesUrl() {
    let url = `${this.featureTilesUrl}/{z}/{x}/{y}?format=vt`;

    if (this.model.map) {
      url = `${url}&map=${this.model.map.id}`;
    }

    if (this.icons && this.icons !== '') {
      url = `${url}&icons=${this.icons}`;
    }

    return url;
  }

  get worldExtent() {
    try {
      if (this.position.longitude && this.position.latitude) {
        const radius = 0.005;
        const topRight = [
          this.position.longitude - radius,
          this.position.latitude - radius,
        ];
        const bottomLeft = [
          this.position.longitude + radius,
          this.position.latitude + radius,
        ];

        return topRight.concat(bottomLeft);
      }
      // eslint-disable-next-line no-empty
    } catch (err) {}

    return this.preferences.appearanceMapExtent;
  }

  get defaultExtent() {
    if (!this.model.map?.area?.coordinates?.length) {
      return this.worldExtent;
    }

    const polygon = new Polygon([this.model.map.area.coordinates[0]]);
    return polygon.getExtent();
  }

  get padding() {
    if (this._panelElement) {
      var width = this._panelElement.clientWidth;
      var x = this._panelElement.offsetLeft;
      var y = this._panelElement.offsetTop;
    }

    if (x === 0 && y === 0) {
      return [0, 0, 0, width];
    }

    return [0, 0, 0, 0];
  }

  get lon() {
    if (this._lon) {
      return this._lon;
    }

    return 0;
  }

  get lat() {
    if (this._lat) {
      return this._lat;
    }

    return 0;
  }

  fit(feature) {
    const geometry = feature.position || feature.geometry;

    return this.fitOlFeature(geometry.toOlFeature('EPSG:3857'));
  }

  fitOlFeature(feature) {
    if (!this._mapView || !feature) {
      return;
    }

    const geometry = feature.getGeometry?.();

    this._mapView.cancelAnimations();

    if (geometry.getType?.() == 'Point') {
      let center = geometry.getFlatCoordinates?.();

      if (!center) {
        return;
      }

      return this._mapView.animate({
        padding: this.padding,
        center,
        zoom: 16,
        duration: this.mapViewAnimationDuration,
      });
    }

    this._mapView.fit(geometry.getExtent(), {
      padding: this.padding,
      duration: this.mapViewAnimationDuration,
    });
  }

  get hasShortTerm() {
    return this.term.length < 4;
  }

  get canGoBack() {
    return !this.hasShortTerm && this.hasFeatureContent;
  }

  @action
  checkSearch() {
    if (this.term == '' || this.hasShortTerm) {
      return this.enableSpotlight();
    }

    return this.transitionToRoute('browse.maps.map-view.search', this.term, {
      queryParams: { allFeatures: null },
    });
  }

  @action
  updateSearch(value, isClosing) {
    if (this.router.currentRoute.name === 'browse.maps.map-view.icon-set') {
      this._term = null;
      this.search = value;
      return;
    }

    this.term = value;

    cancel(this.searchTimer);

    if (this.term == '' && isClosing) {
      return;
    }

    this.searchTimer = later(() => {
      debounce(this, this.checkSearch, 500, true);
    }, 500);
  }

  @action
  setupPanel(element) {
    this._panelElement = element;
  }

  @action
  filterIcons() {
    this.transitionToRoute('browse.maps.map-view.filter-icons');
  }

  @action
  enableSpotlight() {
    const isIndex =
      this.router.currentRouteName == 'browse.maps.map-view.index';

    const isIcons =
      this.router.currentRouteName == 'browse.maps.map-view.icon-set';

    const isSearch =
      this.router.currentRouteName == 'browse.maps.map-view.search' &&
      !this.hasShortTerm;

    if (!this.hasShortTerm && !isIcons) {
      return this.transitionToRoute('browse.maps.map-view.search', this.term);
    }

    if (isIndex || (!isSearch && !isIcons)) {
      this.transitionToRoute('browse.maps.map-view.spotlight');
    }
  }

  @action
  disableSpotlight() {
    this.transitionToRoute('browse.maps.map-view');
  }

  @action
  selectFeatureFromGroup(olFeature, feature) {
    this.featureSelection.update(olFeature);

    this.fit(feature);
    return this.transitionToRoute(
      'browse.maps.map-view.feature',
      this.featureSelection.featureId
    );
  }

  @action
  selectFeature(feature) {
    this.featureSelection.update(feature);

    if (this.featureSelection.type == 'feature') {
      return this.transitionToRoute(
        'browse.maps.map-view.feature',
        this.featureSelection.featureId
      );
    }
  }

  get selectedFeature() {
    return this.router.currentRoute?.params?.feature ?? null;
  }

  @action
  changeCenter(view) {
    this._mapView = view;
    const center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');
    this._lon = center[0];
    this._lat = center[1];
  }

  @action
  backToResults() {
    return this.transitionToRoute('browse.maps.map-view.search', this.term);
  }

  @action
  selectIcon(icon) {
    this.icons = icon.id;
  }

  @action
  deselectIcon() {
    this.icons = '';
  }
}
