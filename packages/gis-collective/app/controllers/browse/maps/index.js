import BrowseController from '../../base/browse-controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import AreaFilter from '../../base/filters/area-filter';
import TeamFilter from '../../base/filters/team-filter';
import { all } from 'rsvp';

export default class BrowseMapsController extends BrowseController {
  @tracked areaFilter;
  @tracked teamFilter;

  @tracked areas = [];
  @tracked teams = [];

  viewMode = 'card-deck';

  setupFilters() {
    this.areaFilter = new AreaFilter(this.store, this.intl);
    this.areaFilter.updateArea = (newValue) => {
      this.set('area', newValue);
    };
    this.areaFilter.updateAreas = (newValue) => {
      this.areas = newValue;
    };

    this.teamFilter = new TeamFilter(this.store, this.intl);
    this.teamFilter.updateTeam = (newValue) => {
      this.set('team', newValue);
    };
    this.teamFilter.updateTeams = (newValue) => {
      this.teams = newValue;
    };
  }

  get breadcrumbs() {
    const list = [
      { route: 'browse.index', text: this.intl.t('browse'), capitalize: true },
    ];

    list.push({ text: this.intl.t('maps'), capitalize: true });

    if (list.length > 2) {
      list[1].route = 'browse.maps.index';
      list[1].query = { area: '' };
    }

    return list;
  }

  get teamSuggestions() {
    if (this.teams && this.teams.length > 0) {
      return this.teams;
    }

    return this.model.editableTeams;
  }

  @action
  removeTag() {
    this.set('tag', null);
  }

  @action
  select(items) {
    const records = items
      .map((id) => this.store.peekRecord('map', id))
      .filter((a) => a);

    this.hasPublic = records.filter((a) => a.visibility.isPublic).length > 0;
    this.hasPrivate = records.filter((a) => !a.visibility.isPublic).length > 0;
  }

  @action
  handleAction(actionName, idList) {
    if (actionName == 'publish') {
      return this.updatePublish(true, idList);
    }

    if (actionName == 'unpublish') {
      return this.updatePublish(false, idList);
    }
  }

  getRecord(id) {
    return new Promise((resolve, reject) => {
      const cache = this.store.peekRecord('map', id);

      if (cache) {
        return resolve(cache);
      }

      this.findRecord('map', id).then(resolve).catch(reject);
    });
  }

  updatePublish(newValue, idList) {
    const promises = idList.map((id) =>
      this.getRecord(id).then((map) => {
        if (map.get('visibility.isPublic') == newValue) {
          return;
        }

        map.set('visibility.isPublic', newValue);
        this.select(idList);
        return map.save();
      })
    );

    return all(promises);
  }
}
