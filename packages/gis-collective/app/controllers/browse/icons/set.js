import BrowseController from '../../base/browse-controller';
import { action } from '@ember/object';

export default class BrowseMapsController extends BrowseController {
  viewMode = 'card-deck';

  get breadcrumbs() {
    const list = [
      { route: 'browse.index', text: this.intl.t('browse'), capitalize: true },
      {
        route: 'browse.icons.index',
        text: this.intl.t('icons'),
        capitalize: true,
      },
    ];

    list.push({ text: this.model.iconSet.name });

    return list;
  }

  @action
  select(items) {
    const records = items
      .map((id) => this.store.peekRecord('map', id))
      .filter((a) => a);

    this.hasPublic = records.filter((a) => a.visibility.isPublic).length > 0;
    this.hasPrivate = records.filter((a) => !a.visibility.isPublic).length > 0;
  }

  @action
  handleAction(actionName, idList) {
    if (actionName == 'publish') {
      return this.updatePublish(true, idList);
    }

    if (actionName == 'unpublish') {
      return this.updatePublish(false, idList);
    }
  }
}
