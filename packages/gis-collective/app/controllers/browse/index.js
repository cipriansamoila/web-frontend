import BrowseController from '../base/browse-controller';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class BrowseIndex extends BrowseController {
  @service position;

  get maps() {
    return [this.model.maps];
  }

  get features() {
    return [this.model.features];
  }

  get iconSets() {
    return [this.model.iconSets];
  }

  get hasMaps() {
    return this.model.maps.length > 0;
  }

  get hasFeatures() {
    return this.model.features.length > 0;
  }

  get hasIconSets() {
    return this.model.iconSets.length > 0;
  }

  get hasResults() {
    return this.hasMaps || this.hasFeatures || this.hasIconSets;
  }

  get hasManyMaps() {
    return this.countItems('Map') > this.maxLoadedMaps;
  }

  get hasManyIconSets() {
    return this.countItems('IconSet') > this.maxLoadedMaps;
  }

  get hasManyFeatures() {
    return this.countItems('Feature') > this.maxLoadedFeatures;
  }

  get featureCount() {
    return this.niceNumber('Feature');
  }

  get mapCount() {
    return this.niceNumber('Map');
  }

  get iconSetCount() {
    return this.niceNumber('IconSet');
  }

  countItems(name) {
    return this.model.models
      .filter((a) => a.get('name') == name)
      .map((a) => a.get('itemCount'))
      .pop();
  }

  niceNumber(name) {
    var count = this.countItems(name);

    if (!count) {
      return '';
    }

    if (count < 100) {
      return count;
    }

    var size = Math.pow(10, count.toString().length - 1);

    return parseInt(count / size) * size;
  }

  @action
  removeTag() {
    this.set('tag', null);
  }
}
