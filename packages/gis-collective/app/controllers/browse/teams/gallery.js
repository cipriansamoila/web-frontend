import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { htmlSafe } from '@ember/template';

export default Controller.extend({
  arePicturesLoaded: computed('model.pictures.{isLoaded,[]}', function () {
    if (this.model.pictures.isLoaded) {
      return true;
    }

    return false;
  }),

  currentSlide: computed('index', 'superDuperSwiper', {
    get() {
      return this.index;
    },
    set(key, val) {
      this.set('index', val);

      return val;
    },
  }),

  pictures: computed('model.pictures', function () {
    return this.model.pictures.map((a) =>
      htmlSafe(`background-image: url("${a.picture}")`)
    );
  }),
});
