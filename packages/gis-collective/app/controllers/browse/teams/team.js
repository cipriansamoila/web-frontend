import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class BrowseMapsController extends Controller {
  @service accessConfig;
  @service notifications;
  @service intl;

  get contentBlocks() {
    const blocks = this.model.team.contentBlocks.blocks.filter(
      (a) => !(a.type == 'header' && a.data.level == 1)
    );

    return {
      blocks,
    };
  }

  @action
  goToMap(id) {
    this.transitionToRoute('browse.map', id);
  }

  @action
  deleteCampaign(item) {
    this.notifications
      .ask({
        title: this.intl.t(`delete campaign`, { size: 1 }),
        question: this.intl.t('delete-confirmation-message', {
          name: item.name,
          size: 1,
        }),
      })
      .then(() => {
        return item.destroyRecord();
      })
      .catch(() => {
        if (!this.isDestroying) this.isDeleting = false;
      });
  }
}
