import Controller from '../../base/map-controller';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class BrowseSitesController extends Controller {
  @service store;
  @service modal;
  @service user;
  @service intl;
  @service fastboot;
  @service accessConfig;

  @tracked issue = null;
  @tracked photoIssue = null;
  @tracked isFullscreen;
  @tracked picture;

  @tracked suggestPhotoVisible = false;
  @tracked reportProblemVisible = false;

  get breadcrumbs() {
    const result = [];

    if (this.accessConfig.isMultiProjectMode) {
      result.push({
        route: 'browse',
        text: this.intl.t('browse'),
        capitalize: true,
      });

      result.push({
        route: 'browse.maps',
        text: this.intl.t('maps'),
        capitalize: true,
      });
    }

    if (this.model.map) {
      result.push({
        route: 'browse.sites',
        query: { map: this.model.map.id },
        text: this.model.map.name,
      });
    }

    result.push({
      text: this.model.feature.name,
    });

    return result;
  }

  get extent() {
    if (!this.model.feature) {
      return null;
    }

    if (!this.model.feature.maps.length) {
      return null;
    }

    return this.model.feature.maps.objectAt(0).area;
  }

  handleIssueError(error) {
    // eslint-disable-next-line no-console
    console.error(error);

    let title = 'Can not submit your request.';
    let message = 'There was a problem on sending your request.';

    if (error['message']) {
      message = error['message'];
    }

    if (error['errors'] && error['errors'].length > 0) {
      if (error['errors'][0].title) {
        title = error['errors'][0].title;
      }

      if (error['errors'][0].description) {
        message = error['errors'][0].description;
      }
    }

    this.modal.alert(title, message);
  }

  @action
  presentPicture(index) {
    this.isFullscreen = true;

    later(() => {
      this.picture = this.model.feature.pictures.objectAt(index);
    }, 10);
  }

  @action
  fullScreen(value) {
    if (!value) {
      this.picture = null;
    }

    this.isFullscreen = value;
  }

  @action
  unpublish() {
    this.model.feature.visibility = 0;
    return this.model.feature.save();
  }

  @action
  publish() {
    this.model.feature.visibility = 1;
    return this.model.feature.save();
  }

  @action
  goToMap(id) {
    this.transitionToRoute('browse.map', id);
  }

  @action
  showSuggestPhotoModal() {
    this.suggestPhotoVisible = true;

    if (this.photoIssue) {
      return;
    }

    this.photoIssue = this.store.createRecord('issue', {
      id: null,
      title: '',
      description: '',
      feature: this.model.feature,
      type: 'newImage',
    });
  }

  @action
  async suggestPhoto() {
    if (this.model.feature.canEdit) {
      this.currentPhoto.set('name', this.photoIssue.title);

      try {
        const picture = await this.currentPhoto.save();
        this.model.feature.pictures.pushObject(picture);

        return this.model.feature.save();
      } catch (error) {
        this.handleIssueError(error);
      }
    }

    return new Promise((resolve, reject) => {
      if (this.user.id) {
        this.photoIssue.set('author', this.user.id);
      }

      this.photoIssue
        .save()
        .then(() => {
          resolve();

          this.modal.alert('Send photo', 'Your photo has been sent.');

          this.photoIssue = null;
          this.photoError = '';
        })
        .catch((error) => {
          reject(error);

          // eslint-disable-next-line no-console
          console.error(error);

          if (
            error.errors &&
            error.errors.length > 0 &&
            error.errors[0].description
          ) {
            return this.set('photoError', error.errors[0].description);
          }

          this.set(
            'photoError',
            'The file could not be uploaded. Please try again later.'
          );
        });
    });
  }

  @action
  setIssuePhoto(index, photo) {
    this.currentPhoto = photo;

    if (!this.photoIssue) {
      return;
    }

    this.photoIssue.set('file', photo.picture);
  }

  @action
  showReportProblemModal() {
    this.reportProblemVisible = true;

    if (!this.issue) {
      this.issue = this.store.createRecord('issue', {
        id: null,
        title: '',
        description: '',
        feature: this.model.feature,
      });
    }
  }

  @action
  reportProblem() {
    return new Promise((resolve, reject) => {
      this.issue.save().then(
        () => {
          resolve();
          this.modal.alert('Send issue', 'Your issue has been sent.');
          this.issue = null;
        },
        (error) => {
          reject();
          this.handleIssueError(error);
        }
      );
    });
  }

  @action
  createPhoto() {
    this.currentPhoto = this.store.createRecord('picture', {
      name: '',
      picture: '',
    });

    return this.currentPhoto;
  }
}
