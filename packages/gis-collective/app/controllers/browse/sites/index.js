import BrowseController from '../../base/browse-controller';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { all } from 'rsvp';
import AuthorFilter from '../../base/filters/author-filter';
import AreaFilter from '../../base/filters/area-filter';
import MapFilter from '../../base/filters/map-filter';
import { inject as service } from '@ember/service';

export default class BrowseMapsController extends BrowseController {
  @tracked authorFilter;
  @tracked areaFilter;
  @tracked mapFilter;
  @tracked visibility;
  @tracked hasPending;

  @tracked users = [];
  @tracked icons = [];
  @tracked areas = [];
  @tracked maps = [];

  @service infinity;
  @service store;
  @service accessConfig;

  viewMode = 'card-deck';

  get iconAdapter() {
    return this.store.adapterFor('icon');
  }

  setupFilters() {
    this.authorFilter = new AuthorFilter(this.store, this.intl);
    this.authorFilter.user = this.user;
    this.authorFilter.updateAuthor = (newValue) => {
      this.set('author', newValue);
      this.model.buckets.columns = [];
    };
    this.authorFilter.updateUsers = (newValue) => {
      this.users = newValue;
    };

    this.areaFilter = new AreaFilter(this.store, this.intl);
    this.areaFilter.updateArea = (newValue) => {
      this.set('area', newValue);
      this.model.buckets.columns = [];
    };
    this.areaFilter.updateAreas = (newValue) => {
      this.areas = newValue;
    };

    this.mapFilter = new MapFilter(this.store, this.intl);
    this.mapFilter.updateMap = (newValue) => {
      this.map = newValue;
      this.model.buckets.columns = [];
    };
    this.mapFilter.updateMaps = (newValue) => {
      this.maps = newValue;
    };
  }

  get breadcrumbs() {
    const list = [
      { route: 'browse.index', text: this.intl.t('browse'), capitalize: true },
    ];

    list.push({ text: this.intl.t('sites'), capitalize: true });

    if (this.model.map) {
      list.push({ text: this.model.map.name, capitalize: true });
    }

    if (this.model.icon) {
      list.push({ text: this.model.icon.localName, capitalize: true });
    }

    if (list.length > 2) {
      list[1].route = 'browse.sites.index';
      list[1].query = { user: '', icon: '', area: '', map: '' };
    }

    return list;
  }

  get iconSets() {
    if (
      !this.model.map ||
      !this.model.map.iconSets ||
      !this.model.map.iconSets.idList
    ) {
      return null;
    }

    return this.model.map.iconSets.idList;
  }

  get selectAll() {
    if (!this.isEditable) {
      return null;
    }

    return this.model.map ? 'enabled' : 'disabled';
  }

  get modelActions() {
    const result = [];

    if (this.hasPrivate || this.hasPublic) {
      result.pushObject({
        name: this.intl.t('set as pending'),
        key: 'set-pending',
        icon: 'clock',
        class: 'btn-secondary',
      });
    }

    if (this.hasPending || this.hasPrivate) {
      result.pushObject({
        name: this.intl.t('publish'),
        key: 'publish',
        icon: 'eye',
        class: 'btn-secondary',
      });
    }

    if (this.hasPending || this.hasPublic) {
      result.pushObject({
        name: this.intl.t('unpublish'),
        key: 'unpublish',
        icon: 'eye-slash',
        class: 'btn-secondary',
      });
    }

    return result;
  }

  getRecord(id) {
    return new Promise((resolve, reject) => {
      const cache = this.store.peekRecord('feature', id);

      if (cache) {
        return resolve(cache);
      }

      this.findRecord('feature', id).then(resolve).catch(reject);
    });
  }

  @action
  iconSelect(icon) {
    if (!icon) {
      this.icon = '';
      return;
    }
    const id = icon.id || icon._id;

    this.icon = id;
  }

  @action
  changeVisibility(newValue) {
    this.visibility = newValue;
    this.model.buckets.columns = [];
  }

  @action
  select(items) {
    if (items == 'all') {
      this.hasPublic = true;
      this.hasPrivate = true;
      this.hasPending = true;

      return;
    }

    const records = items
      .map((id) => this.store.peekRecord('feature', id))
      .filter((a) => a);

    this.hasPublic = records.filter((a) => a.isPublished).length > 0;
    this.hasPrivate = records.filter((a) => a.isPrivate).length > 0;
    this.hasPending = records.filter((a) => a.isPending).length > 0;
  }

  @action
  handleAction(actionName, idList) {
    if (idList == 'all' && actionName == 'publish') {
      return this.publishAll();
    }

    if (idList == 'all' && actionName == 'unpublish') {
      return this.unpublishAll();
    }

    if (idList == 'all' && actionName == 'set-pending') {
      return this.setAllPending();
    }

    if (idList == 'all' && actionName == 'deleteAll') {
      return this.deleteAll();
    }

    if (actionName == 'publish') {
      return this.updateVisibility(1, idList);
    }

    if (actionName == 'unpublish') {
      return this.updateVisibility(0, idList);
    }

    if (actionName == 'set-pending') {
      return this.updateVisibility(-1, idList);
    }
  }

  get bulkQuery() {
    let query = {
      map: this.map,
    };

    if (this.search) {
      query.search = this.search;
    }

    if (this.tag) {
      query.tag = this.tag;
    }

    if (this.author) {
      query.author = this.author;
    }

    if (this.team) {
      query.team = this.team;
    }

    if (this.icon) {
      query.icon = this.icon;
    }

    if (this.area) {
      query.area = this.area;
    }

    if (this.visibility) {
      query.visibility = this.visibility;
    }

    return query;
  }

  async resetList() {
    this.model.buckets = await this.model.getNewBuckets();
  }

  deleteAll() {
    return this.store
      .adapterFor('feature')
      .bulkDelete(this.bulkQuery)
      .then(() => {
        this.resetList();
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }

  publishAll() {
    return this.store
      .adapterFor('feature')
      .bulkPublish(this.bulkQuery)
      .then(() => {
        return this.resetList();
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }

  unpublishAll() {
    return this.store
      .adapterFor('feature')
      .bulkUnpublish(this.bulkQuery)
      .then(() => {
        return this.resetList();
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }

  setAllPending() {
    return this.store
      .adapterFor('feature')
      .bulkSetPending(this.bulkQuery)
      .then(() => {
        return this.resetList();
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }

  updateVisibility(newValue, idList) {
    const promises = idList.map((id) =>
      this.getRecord(id).then((feature) => {
        if (feature.visibility == newValue) {
          return;
        }

        feature.visibility = newValue;

        this.select(idList);
        return feature.save();
      })
    );

    return all(promises);
  }
}
