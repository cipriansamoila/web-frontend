import Controller from '@ember/controller';
import AlertMessage from '../lib/alert-message';
import { inject as service } from '@ember/service';

export default class PageController extends Controller {
  @service intl;

  get alertList() {
    const list = [];

    if (!this.model?.page?.get('visibility')?.isPublic) {
      list.push(
        new AlertMessage(
          this.intl.t('message-alert-page-private'),
          'warning',
          'alert-page-private'
        )
      );
    }

    return list;
  }
}
