import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class ApplicationController extends Controller {
  @service session;
  @service searchStorage;
  @service store;
  @service router;
  @service preferences;
  @service fullscreen;
  @service mapStyles;
  @service user;
  @service articles;
  @service accessConfig;

  queryParams = ['embed'];

  @tracked embed;

  get isEmbedded() {
    return this.embed === 'true' || this.embed === true;
  }

  get showCampaigns() {
    const item = this.model?.models?.find((a) => a.name === 'Campaign');

    return item?.itemCount;
  }

  get showManage() {
    return (
      this.preferences.allowManageWithoutTeams || this.user.teams?.length > 0
    );
  }

  get showBrowse() {
    if (!this.model?.models?.filter) {
      return false;
    }

    if (!this.accessConfig.isMultiProjectMode) {
      return false;
    }

    const items = this.model.models
      .filter((a) => a.name !== 'Campaign')
      .map((a) => a.itemCount)
      .reduce((a, b) => a + b, 0);

    return items > 0;
  }

  get isMenuRendered() {
    if (this.isEmbedded) {
      return false;
    }

    return this.session.isAuthenticated || !this.preferences.isPrivate;
  }

  get isMenuVisible() {
    if (this.fullscreen.isEnabled) {
      return false;
    }

    if (this.session.isAuthenticated) {
      return true;
    }

    return !this.preferences.isPrivate;
  }

  @action
  async onSignOut() {
    await this.session.invalidate();
    this.store.unloadAll();
    return this.transitionToRoute('login.index');
  }

  @action
  onSearch(value) {
    this.transitionToRoute({ queryParams: { search: value, pages: null } });
  }

  @action
  goTo(path) {
    if (path === 'index') {
      this.transitionToRoute('/');
      return;
    }

    this.transitionToRoute(path);
  }

  @action
  onSelectLocale(locale) {
    this.transitionToRoute({ queryParams: { locale } });
  }

  @action
  onMapStyle(element) {
    this.mapStyles.mapStyleElement = element;
  }
}
