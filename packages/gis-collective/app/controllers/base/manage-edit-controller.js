import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class ManageEditController extends Controller {
  @service notifications;
  @service user;
  @service intl;

  @tracked editableField;
  @tracked savingField;
  @tracked showArticle = true;
  @tracked allTeams = false;

  queryParams = ['allTeams'];

  get isDisabled() {
    return (
      !this.editableModel.hasDirtyAttributes || this.editableModel.isSaving
    );
  }

  @action
  changeAll(newValue) {
    this.allTeams = newValue;
  }

  @action
  saveIsPublic(newValue) {
    this.editableModel.visibility.isPublic = newValue;
    this.editableField = this.intl.t('is public');

    return this.save();
  }

  @action
  saveTeam(newValue) {
    this.editableModel.visibility.team = newValue;
    this.editableField = this.intl.t('team');

    return this.save();
  }

  @action
  saveIsDefault(newValue) {
    this.editableModel.visibility.isDefault = newValue;
    this.editableField = this.intl.t('is default');

    return this.save();
  }

  @action
  saveField(field, value) {
    this.editableModel[field] = value;

    return this.save();
  }

  @action
  async save() {
    this.savingField = this.editableField;
    this.editableField = '';

    try {
      await this.editableModel.save();
    } catch (err) {
      this.notifications.handleError(err);
    }

    this.savingField = '';
  }

  @action
  edit(key) {
    this.editableField = key;
  }

  @action
  cancel() {
    if (this.editableModel.restore) {
      this.editableModel.restore();
    } else {
      this.editableModel.rollbackAttributes?.();
    }

    this.editableField = '';
  }

  @action
  restore() {
    this.showArticle = false;
    this.editableModel.rollbackAttributes();
    this.editableField = '';

    later(() => {
      this.showArticle = true;
    }, 100);
  }

  @action
  saveArticle(title, content) {
    this.editableModel.title = title;
    this.editableModel.content = content;

    return this.save();
  }
}
