import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class TeamFilter {
  @tracked team = {};
  @tracked teams = [];

  constructor(store, intl) {
    this.store = store;
    this.intl = intl;
  }

  @action
  hide() {}

  @action
  reset() {
    return this.updateTeam('');
  }

  @action
  select(team) {
    if (team.id) {
      this.team = team;
      return this.updateTeam(team.id);
    }

    if (team == this.team.id) {
      return this.updateTeam(team);
    }

    if (team !== '' && this.teams && this.teams.length) {
      return this.updateTeam(this.teams.firstObject.id);
    }

    return this.reset();
  }

  get name() {
    if (this.team && this.team.name) {
      return this.team.name;
    }

    return '';
  }

  get label() {
    if (this.name) {
      return this.intl.t('created by', { name: this.name });
    }

    return '';
  }

  @action
  search(term) {
    if (!term || term.length < 3) {
      this.updateTeams([]);
      return;
    }

    return new Promise((resolve, reject) => {
      this.store
        .query('team', { term, limit: 5 })
        .then((teams) => {
          this.teams = teams;
          this.updateTeams(teams);

          resolve();
        })
        .catch(reject);
    });
  }
}
