import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class AreaFilter {
  @tracked area = null;

  constructor(store, intl) {
    this.store = store;
    this.intl = intl;
  }

  @action
  hide() {}

  @action
  reset() {
    return this.updateArea('');
  }

  @action
  select(area) {
    if (area.name) {
      this.area = area.name;
      return this.updateArea(area.name);
    }

    if (area !== '' && this.areas && this.areas.length) {
      return this.updateArea(this.areas.firstObject.name);
    }

    return this.reset();
  }

  get name() {
    return this.area;
  }

  get label() {
    if (this.name) {
      return this.intl.t('inside area', { name: this.name });
    }

    return '';
  }

  @action
  search(term) {
    if (!term || term.length < 3) {
      this.updateAreas([]);
      return;
    }

    return new Promise((resolve, reject) => {
      this.store
        .query('area', { term, limit: 5 })
        .then((areas) => {
          this.areas = areas;
          this.updateAreas(areas);

          resolve();
        })
        .catch(reject);
    });
  }
}
