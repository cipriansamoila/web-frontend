import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class MapFilter {
  @tracked map = {};

  constructor(store, intl) {
    this.store = store;
    this.intl = intl;
  }

  @action
  hide() {}

  @action
  reset() {
    return this.updateMap('');
  }

  @action
  select(map) {
    if (map.id) {
      this.map = map;
      return this.updateMap(map.id);
    }

    if (map !== '' && this.maps?.length) {
      return this.updateMap(this.maps.firstObject.id);
    }

    return this.reset();
  }

  get name() {
    if (this.map && this.map.name) {
      return this.map.name;
    }

    return '';
  }

  get label() {
    if (this.name) {
      return this.intl.t('with map', { name: this.name });
    }

    return '';
  }

  @action
  search(term) {
    if (!term || term.length < 3) {
      this.updateMaps([]);
      return;
    }

    return new Promise((resolve, reject) => {
      this.store
        .query('map', { term, limit: 5 })
        .then((maps) => {
          this.maps = maps;
          this.updateMaps(maps);

          resolve();
        })
        .catch(reject);
    });
  }
}
