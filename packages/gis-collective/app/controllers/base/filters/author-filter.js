import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class AuthorFilter {
  @tracked author = {};
  @tracked user = {};

  constructor(store, intl) {
    this.store = store;
    this.intl = intl;
  }

  @action
  hide() {}

  @action
  reset() {
    return this.updateAuthor('');
  }

  @action
  select(user) {
    if (user.id) {
      this.author = user;
      return this.updateAuthor(user.id);
    }

    if (user == this.user.id) {
      return this.updateAuthor(user);
    }

    if (user !== '' && this.users && this.users.length) {
      return this.updateAuthor(this.users.firstObject.id);
    }

    return this.reset();
  }

  get name() {
    if (this.author && this.author.name) {
      return this.author.name;
    }

    return '';
  }

  get label() {
    if (this.author && this.author.id === this.user.id) {
      return this.intl.t('created by you');
    }

    if (this.name) {
      return this.intl.t('created by', { name: this.name });
    }

    return '';
  }

  @action
  search(term) {
    if (!term || term.length < 3) {
      this.updateUsers([]);
      return;
    }

    return new Promise((resolve, reject) => {
      this.store
        .query('user', { term, limit: 5 })
        .then((users) => {
          this.users = users;
          this.updateUsers(users);

          resolve();
        })
        .catch(reject);
    });
  }
}
