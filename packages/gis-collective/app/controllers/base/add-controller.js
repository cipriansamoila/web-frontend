import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class AddController extends Controller {
  @service notifications;
  @service user;

  queryParams = ['all'];

  @tracked all = false;
  @tracked hasTeam;

  @action
  toggleAll(value) {
    this.all = value;
  }

  @action
  change(field, value) {
    this.editableModel.set(field, value);
    this.hasTeam = !!this.editableModel.visibility.team;
  }

  @action
  async save() {
    let result;

    try {
      result = await this.editableModel.save();
    } catch (err) {
      return this.notifications.handleError(err);
    }

    return this.transitionToRoute(this.destination, result.id);
  }
}
