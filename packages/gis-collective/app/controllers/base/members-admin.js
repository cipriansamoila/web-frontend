import Controller from '@ember/controller';
import { Promise } from 'rsvp';
import { action } from '@ember/object';
import { debounce } from '@ember/runloop';

export default class MembersAdminController extends Controller {
  performUserSearch(term, resolve, reject) {
    if (!term || term.length <= 3) {
      return reject('Term is too short');
    }

    this.store
      .query('user', { term: term })
      .then((data) => {
        resolve(data);
      })
      .catch(reject);
  }

  @action
  searchUsers(term) {
    return new Promise((resolve, reject) => {
      debounce(this, this.performUserSearch, term, resolve, reject, 600);
    });
  }

  @action
  membersChanged(value) {
    this.set('model.members', value);
  }

  @action
  save() {
    const map = this.model;

    map.get('cover').then((cover) => {
      cover.save().then(() => {
        map.save().then(() => {
          this.set('isSaving', false);
          this.transitionToRoute('maps');
        });
      });
    });
  }
}
