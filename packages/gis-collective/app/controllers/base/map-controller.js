import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import config from '../../config/environment';
import { debounce } from '@ember/runloop';

export default class MapController extends Controller {
  @service session;
  @service fastboot;
  @service position;
  @service router;
  @service store;
  @service searchStorage;
  @service fullscreen;
  @service embed;
  @service mapStyles;
  @service preferences;

  @tracked viewbox = null;
  @tracked _selectedBaseMap;
  @tracked hoveredFeature;
  @tracked viewMode = '';
  @tracked isHover = false;
  @tracked extentChangeCounter = -1;

  @tracked contextX = 0;
  @tracked contextY = 0;

  @tracked featureStyle;
  @tracked icons = '';
  @tracked search = '';
  @tracked hideLoading = false;
  @tracked baseMap;

  queryParams = ['viewbox', 'icons', 'search', 'hideLoading', 'baseMap'];
  _extentM = null;

  featureTilesUrl = config.siteTilesUrl;

  get linkTarget() {
    return this.embed.isEnabled ? '_blank' : '_self';
  }

  get bearer() {
    let { access_token } = this.session.get('data.authenticated');

    if (!access_token) {
      return null;
    }

    return `Bearer ${access_token}`;
  }

  get extentM() {
    if (this._extentM) {
      return this._extentM;
    }

    try {
      return this.viewbox.split(',').map((a) => parseFloat(a));
      // eslint-disable-next-line no-empty
    } catch (err) {}

    return this.defaultExtent;
  }

  get selectedBaseMap() {
    if (this.baseMap) {
      return this.model.baseMaps.find((a) => a.id == this.baseMap);
    }

    if (this._selectedBaseMap) {
      return this._selectedBaseMap;
    }

    if (!this.model.baseMaps || this.model.baseMaps.length == 0) {
      return null;
    }

    return this.model.baseMaps.firstObject;
  }

  set selectedBaseMap(value) {
    this._selectedBaseMap = value;
    this.baseMap = undefined;
  }

  @action
  localize() {
    return this.position.watchPosition();
  }

  @action
  selectBaseMap(baseMap) {
    this.selectedBaseMap = baseMap;
  }

  @action
  loadIcon(iconId) {
    const icon = this.store.peekRecord('icon', iconId);

    if (icon) {
      return icon.get('iconSet').then(() => {
        return icon;
      });
    }

    return this.store.findRecord('icon', iconId).then((icon) => {
      return icon.get('iconSet').then(() => {
        return icon;
      });
    });
  }

  updateViewbox(value) {
    this.extentChangeCounter++;
    const viewbox = value.join(',');

    if (viewbox != this.viewbox) {
      this.viewbox = viewbox;
    }
  }

  @action
  handleExtentChange(value) {
    this.extentChangeCounter++;

    if (this.extentChangeCounter == 0) {
      return;
    }

    this._extentM = value;

    debounce(this, this.updateViewbox, value, 1000);
  }

  get selectedIconList() {
    if (!this.icons) {
      return [];
    }

    const idList = this.icons.split(',');
    const list = idList
      .filter((a) => a)
      .map((a) => {
        try {
          return this.store.peekRecord('icon', a);
        } catch (err) {
          return null;
        }
      })
      .filter((a) => a);

    return list;
  }

  get mapExtent() {
    if (this.extentChangeCounter > 0) {
      return null;
    }

    if (!this.viewbox) {
      return this.defaultExtent;
    }

    return this.extentM;
  }

  get isContextVisible() {
    return this.contextX && this.contextY;
  }

  get contextOptions() {
    if (
      this.hoveredFeature &&
      this.hoveredFeature.getProperties &&
      this.hoveredFeature.getProperties()._id
    ) {
      return 'open';
    }

    if (!this.preferences.allowProposingSites) {
      return 'what is nearby?';
    }

    return 'propose a site,what is nearby?';
  }

  @action
  handleMapContextAction(actionName, lon, lat) {
    let map;

    if (this.model.map) {
      map = this.model.map.id;
    }

    if (actionName == 'propose a site') {
      this.transitionToRoute('add.site', {
        queryParams: {
          ignoreGps: true,
          restore: false,
          map,
          lat,
          lon,
        },
      });
    }

    if (actionName == 'what is nearby?') {
      this.transitionToRoute('browse.sites', {
        queryParams: {
          map,
          lat,
          lon,
          viewMode: 'list',
        },
      });
    }

    if (actionName == 'open') {
      const id = this.hoveredFeature.getProperties()._id;

      this.transitionToRoute('browse.sites.site', id, {
        queryParams: {
          map: this.model.map ? this.model.map.id : null,
        },
      });
    }
  }

  @action
  hoverFeature(feature) {
    this.isHover = !!feature;

    if (feature) {
      const properties = feature.getProperties();
      if (properties['canView'] !== 'true' && properties['visibility'] != '1') {
        this.isHover = false;
        feature = null;
      }
    }

    if (!this.isContextVisible) {
      this.hoveredFeature = feature;
      this.pendingHoveredFeature = false;
    } else {
      this.pendingHoveredFeature = feature;
    }
  }

  @action
  showMapContextMenu(ev) {
    ev.preventDefault();

    if (ev.target.tagName.toLowerCase() != 'canvas') {
      return false;
    }

    if (this.pendingHoveredFeature !== false) {
      this.hoveredFeature = this.pendingHoveredFeature;
    }

    this.contextX = ev.clientX;
    this.contextY = ev.clientY;

    return false;
  }

  @action
  hideMapContextMenu() {
    this.contextX = 0;
    this.contextY = 0;
  }

  transitionToRoute(routeName) {
    if (routeName.indexOf('browse.maps.map-view') == 0) {
      return super.transitionToRoute(...arguments);
    }

    if (this.linkTarget == '_blank') {
      const url = this.router.urlFor(...arguments);

      window.open(url);
      return;
    }

    return super.transitionToRoute(...arguments);
  }
}
