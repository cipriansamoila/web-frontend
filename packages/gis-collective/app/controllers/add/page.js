import AddController from '../base/add-controller';
import { action } from '@ember/object';

export default class AddPageController extends AddController {
  destination = 'manage.pages.content';

  get canSubmit() {
    return this.editableModel.name && this.editableModel.slug && this.hasTeam;
  }

  get editableModel() {
    return this.model.page;
  }

  @action
  async save() {
    const layout = this.store.createRecord('layout', {
      name: `${this.model.page.name} layout`,
    });

    try {
      await layout.save();
      this.model.page.layout = layout;
      await this.model.page.save();
    } catch (err) {
      return this.notifications.handleError(err);
    }

    this.transitionToRoute(this.destination, this.model.page.id);
  }
}
