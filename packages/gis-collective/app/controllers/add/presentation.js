import AddController from '../base/add-controller';

export default class AddPresentationController extends AddController {
  destination = 'manage.presentations.edit';

  get canSubmit() {
    return (
      this.editableModel.name &&
      this.editableModel.slug &&
      this.editableModel.layout &&
      this.hasTeam
    );
  }

  get editableModel() {
    return this.model.presentation;
  }
}
