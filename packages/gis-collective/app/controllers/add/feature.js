import Controller from '../base/map-controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class AddFeatureController extends Controller {
  @service notifications;
  @service user;

  queryParams = ['all'];

  @tracked all = false;

  get isInvalid() {
    return (
      !this.model.feature.name ||
      this.model.feature.name.trim() == '' ||
      this.model.feature.maps.length == 0 ||
      !this.model.feature.position
    );
  }

  get extent() {
    if (!this.model.feature.maps.length) {
      return null;
    }

    return this.model.feature.maps.firstObject.area;
  }

  get map() {
    if (
      !this.model.feature ||
      !this.model.feature.maps ||
      !this.model.feature.maps.length
    ) {
      return null;
    }

    return this.model.feature.maps.firstObject;
  }

  @action
  toggleAll(value) {
    this.all = value;
  }

  @action
  updateMap(value) {
    this.model.feature.maps.clear();
    this.model.feature.maps.pushObject(value);
  }

  @action
  changePositionValue(value) {
    this.model.feature.position = value;
    this.notifyPropertyChange('isInvalid');
  }

  @action
  async save() {
    return this.model.feature
      .save()
      .then((feature) => {
        this.replaceRoute('manage.sites.edit', feature.id);
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }
}
