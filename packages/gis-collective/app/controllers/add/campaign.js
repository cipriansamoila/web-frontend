import Controller from '@ember/controller';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class AddCampaignController extends Controller {
  @service notifications;
  @service user;

  queryParams = ['all'];

  @tracked all = false;
  @tracked isInvalid = false;
  @tracked article;
  @tracked hasTeam;

  get canSubmit() {
    return this.model.campaign.name && this.hasTeam;
  }

  @action
  toggleAll(value) {
    this.all = value;
  }

  @action
  changeArticleValue(title, article) {
    this.article = article;
  }

  @action
  changeTeam(team) {
    this.model.campaign.set('visibility.team', team);
    this.hasTeam = !!this.model.campaign.visibility.team;
  }

  @action
  save() {
    const articleBlocks = this.article?.blocks ?? [];
    const blocks = [
      {
        type: 'header',
        data: {
          text: this.model.campaign.name,
          level: 1,
        },
      },
      ...articleBlocks,
    ];

    this.model.campaign.article = {
      blocks,
    };

    return this.model.campaign
      .save()
      .then((result) => {
        return this.transitionToRoute('manage.campaigns.edit', result.id);
      })
      .catch((err) => {
        this.notifications.handleError(err);
      });
  }
}
