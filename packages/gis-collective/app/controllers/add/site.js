import Controller from '../base/map-controller';
import { inject as service } from '@ember/service';
import { all } from 'rsvp';
import { transform } from 'ol/proj';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { debounce } from '@ember/runloop';
import ModelInfo from '../../lib/model-info';
import GeoJson from '../../lib/geoJson';
import PositionDetails from '../../lib/positionDetails';
import GeocodingSearch from '../../lib/geocoding-search';
import RequiredAttributes from '../../lib/required-attributes';
import {
  addIconSection,
  handleAttributeChange,
  handlePicturesChange,
  handleGroupRemove,
} from '../../attributes/actions';
import { addIconGroupToStore } from '../../lib/icons';

export default class AddSiteController extends Controller {
  queryParams = [
    {
      lon: { type: 'number' },
      lat: { type: 'number' },
      map: { type: 'string' },
      icons: { type: 'string' },
      ignoreGps: { type: 'boolean' },
      preserveScrollPosition: { type: 'boolean' },
    },
  ];

  @service notifications;
  @service position;
  @service store;
  @service pendingData;
  @service intl;
  @service user;
  @service('loading') loadingService;

  @tracked pictureOptimization = false;
  @tracked currentlyLoading = false;
  @tracked map = '';

  @tracked maps = [];
  @tracked iconGroup = {};
  @tracked areMapsLoading;
  @tracked areIconsLoading;
  @tracked ignoreGps;
  @tracked submitDisabled;

  preserveScrollPosition = true;

  constructor() {
    super(...arguments);
    this.geocodingSearch = new GeocodingSearch(this.store);
    this.requiredAttributes = new RequiredAttributes();
  }

  progress(picture, percentage) {
    picture.set('progress', percentage);
  }

  resizePictures() {
    if (!this.model.feature.pictures.pictureOptimization) {
      return Promise.resolve([]);
    }

    return all(
      this.model.feature.pictures
        .filter((a) => a.isNew)
        .map((a) => a.localResize(1920, 1920))
    );
  }

  get loadingList() {
    if (!this.loadingService.isLoading) {
      return [];
    }

    return ['about.maps', 'icons'];
  }

  get isEmpty() {
    if (!this.model.feature) {
      return true;
    }

    if (this.hasInvalidPosition) {
      return true;
    }

    if (this.model.feature.name && this.model.feature.name.trim() != '') {
      return false;
    }

    if (
      this.model.feature.description &&
      this.model.feature.description.trim() != ''
    ) {
      return false;
    }

    if (this.model.feature.pictures && this.model.feature.pictures.length > 0) {
      return false;
    }

    if (this.model.feature.icons && this.model.feature.icons.length > 0) {
      return false;
    }

    return true;
  }

  get isDisabled() {
    return this.submitDisabled;
  }

  handlePositionChange(key, value) {
    if (key == 'position') {
      if (!value) {
        value = this.tmpCenter || this.center;
      }

      this.set('lon', value[0]);
      this.set('lat', value[1]);

      this.model.feature.set('position.coordinates', value);

      this.areMapsLoading = true;
      debounce(this, this.updateMaps, 1000);

      return;
    }

    if (key == 'capturedUsingGPS') {
      this.ignoreGps = !value;
    } else {
      value = parseFloat(value);
    }

    handleAttributeChange(
      this.model.feature,
      'position details',
      null,
      key,
      value
    );
  }

  handleAboutChange(key, value) {
    if (key == 'maps') {
      this.set('map', '');
      this.model.feature.maps.setObjects(value);
      return this.updateIcons();
    }

    if (key == 'name') {
      this.model.feature.name = value;
    }

    if (key == 'description') {
      this.model.feature.description = value;
    }
  }

  handleIconsChange(key, value) {
    if (key == 'icons') {
      this.model.feature.icons.setObjects(value);
    }
  }

  storeToLocalData() {
    const newFeature = Object.assign({}, this.model.feature.serialize());

    newFeature.icons = this.model.feature.icons.map((a) => a.id);
    newFeature.maps = this.model.feature.maps.map((a) => a.id);

    this.pendingData.pendingSite.set('lastKnownValue', newFeature);

    const pendingPictures =
      this.pendingData.pendingPictures.get('lastKnownValue');
    const storedPictureCount =
      pendingPictures.new.length + pendingPictures.stored.length;

    if (this.model.feature.pictures.length != storedPictureCount) {
      const pictures = {
        new: this.model.feature.pictures
          .filter((a) => a.isNew)
          .map((a) => a.picture),
        stored: this.model.feature.pictures
          .filter((a) => !a.isNew)
          .map((a) => a.id),
      };

      this.pendingData.pendingPictures.set('lastKnownValue', pictures);
    }
  }

  async createNewFeature(params) {
    let coordinates = [
      parseFloat(params?.lon) || 0,
      parseFloat(params?.lat) || 0,
    ];
    let attributes = this.defaultAttributes(params);

    if (
      attributes['position details']['type'] == 'gps' &&
      this.position.hasPosition
    ) {
      coordinates = this.position.center.slice();
    }

    const maps = [];

    if (params && params['map']) {
      const map = await this.store.findRecord('map', params['map']);
      maps.pushObject(map);
    }

    return this.store.createRecord('feature', {
      id: null,
      name: '',
      maps,
      description: '',
      position: new GeoJson({ coordinates, type: 'Point' }),
      pictures: [],
      icons: [],
      info: new ModelInfo(),
      attributes,
      visibility: 0,
    });
  }

  defaultAttributes(params) {
    let altitude = '';
    let accuracy = '';
    let altitudeAccuracy = '';

    let type = this.position.hasPosition ? 'gps' : 'manual';

    if (
      params &&
      (params['ignoreGps'] == 'true' || params['ignoreGps'] == true)
    ) {
      type = 'manual';
    }

    if (params && !isNaN(params.lon) && !isNaN(params.lat)) {
      type = 'manual';
    } else if (type == 'gps' && this.position.hasPosition) {
      altitude = this.position.altitude;
      accuracy = this.position.accuracy;
      altitudeAccuracy = this.position.altitudeAccuracy;
    }

    return {
      'position details': new PositionDetails({
        altitude,
        accuracy,
        type,
        altitudeAccuracy,
      }),
    };
  }

  get defaultCenter() {
    return [0, 0];
  }

  updateMaps() {
    if (!this.model || !this.model.feature) {
      return;
    }

    this.areMapsLoading = true;
    const coordinates = this.model.feature.position.coordinates;
    const containing = `${coordinates[0]},${coordinates[1]}`;

    return this.store
      .query('map', { containing })
      .then((result) => {
        this.maps = result;
        this.areMapsLoading = false;
      })
      .catch(() => {
        this.maps = [];
        this.areMapsLoading = false;
      });
  }

  updateIcons() {
    if (!this.model || !this.model.feature) {
      return;
    }

    if (this.model.feature.maps.length == 0) {
      this.iconGroup = this.model.defaultIcons;
      return;
    }

    this.areIconsLoading = true;
    const iconSet = this.model.feature.maps.firstObject.iconSets.idList;

    let iconQuery = {
      iconSet: iconSet.join(),
      locale: this.intl.primaryLocale,
    };

    return this.store
      .adapterFor('icon')
      .group(iconQuery)
      .then((icons) => {
        addIconGroupToStore(icons, this.store);
        this.iconGroup = icons;

        this.areIconsLoading = false;
      })
      .catch(() => {
        this.iconGroup = this.model.defaultIcons;
        this.areIconsLoading = false;
      });
  }

  @action
  changeMapView(view) {
    const center = transform(view.getCenter(), 'EPSG:3857', 'EPSG:4326');
    this.set('tmpCenter', center);
  }

  @action
  async searchGeocoding(term) {
    if (!term) {
      return this.geocodingSearch.clear();
    }

    return await this.geocodingSearch.search(term);
  }

  @action
  async changePosition(position, details) {
    await this.searchGeocoding(details['searchTerm']);

    if (this.geocodingSearch.hasNewResults) {
      details['address'] = this.geocodingSearch.results.firstObject.name;
      position = this.geocodingSearch.results.firstObject.geometry;
    }

    if (
      JSON.stringify(this.model.feature.position) !=
      JSON.stringify(position.center)
    ) {
      this.areMapsLoading = true;
      debounce(this, this.updateMaps, 1000);
    }

    this.model.feature.position = position.center;
    this.model.feature.attributes['position details'] = details;
    this.model.feature.notifyPropertyChange?.('attributes');
  }

  @action
  remove(key, index) {
    handleGroupRemove(this.model.feature, key, index);
    this.checkRequiredAttributes();

    this.storeToLocalData();
  }

  @action
  addSection(icon) {
    addIconSection(this.model.feature, icon);
    this.checkRequiredAttributes();

    this.model.feature.notifyPropertyChange('attributes');
    this.storeToLocalData();
  }

  @action
  checkRequiredAttributes() {
    this.submitDisabled = this.requiredAttributes.validate(this.model.feature);
  }

  @action
  submit() {
    this.set('isSaving', true);
    this.set('isError', false);

    return this.resizePictures()
      .then(() => {
        const picturePromises = all(
          this.model.feature.pictures
            .filter((a) => a.isNew)
            .map((a) =>
              a.save({
                adapterOptions: {
                  progress: (percentage) => {
                    this.progress(a, percentage);
                  },
                },
              })
            )
        );

        return picturePromises.then(() => {
          return this.model.feature.save().then((newFeature) => {
            this.set('isSaving', false);
            this.set('isError', false);
            this.set('maps', null);
            this.set('icons', null);
            this.set('lon', null);
            this.set('lat', null);
            this.set('ignoreGps', null);

            let map;
            let feature;

            if (newFeature.maps.length > 0) {
              map = newFeature.maps.firstObject.id;
            }

            if (newFeature.canEdit) {
              feature = newFeature.id;
            }

            return this.transitionToRoute('add.site-success', {
              queryParams: { map, feature },
            });
          });
        });
      })
      .catch((err) => {
        this.set('isError', true);
        this.set('isSaving', false);

        this.notifications.handleError(err);
      });
  }

  @action
  async reset() {
    this.pendingData.reset();

    this.model.feature.name = '';
    this.model.feature.description = '';
    this.model.feature.attributes = this.defaultAttributes();
    this.model.feature.pictures = [];
    this.model.feature.icons = [];
    this.model.feature.maps = [];
  }

  @action
  editSection(name) {
    if (name == 'position') {
      this.transitionToRoute('add.site');
    }
  }

  @action
  changePictures(value) {
    this.handlePicturesChange('pictures', value);
    this.storeToLocalData();
  }

  @action
  change(group, key, value) {
    switch (group.key) {
      case 'position':
        this.handlePositionChange(key, value);
        break;

      case 'about':
        this.handleAboutChange(key, value);
        break;

      case 'pictures':
        handlePicturesChange(this.model.feature, key, value);
        break;

      case 'icons':
        this.handleIconsChange(key, value);
        break;

      default:
        handleAttributeChange(
          this.model.feature,
          group.key,
          group.index,
          key,
          value
        );
        this.checkRequiredAttributes();
    }

    this.storeToLocalData();
  }
}
