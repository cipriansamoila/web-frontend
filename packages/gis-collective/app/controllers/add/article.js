import AddController from '../base/add-controller';
import { action } from '@ember/object';

export default class AddArticleController extends AddController {
  destination = 'manage.articles.edit';

  get canSubmit() {
    return this.editableModel.slug && this.hasTeam;
  }

  @action
  changeArticleValue(title, article) {
    this.model.article.title = title;
    this.model.article.content = article;
  }

  get editableModel() {
    return this.model.article;
  }
}
