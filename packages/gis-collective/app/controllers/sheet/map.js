import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { SheetColData, attributeToSheetColData } from '../../lib/SheetColData';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { debounce } from '@ember/runloop';

export default class SheetMapController extends Controller {
  @service router;
  @service fastboot;
  @service notifications;
  @tracked start = 0;
  @tracked end = 100;

  queryParams = ['sortField', 'isAscending', 'icon'];

  @tracked sortBy = 'name';
  @tracked isAscending = true;
  @tracked icon = null;

  constructor() {
    super(...arguments);

    this.cache = {};

    this.cache['idCol'] = new SheetColData({
      name: 'id',
      valuePath: 'id',
      key: 'id',
      width: 300,
    });

    this.cache['nameCol'] = new SheetColData({
      name: 'name',
      key: 'name',
      valuePath: 'name',
      editorType: 'inline-text',
    });

    this.cache['descriptionCol'] = new SheetColData({
      name: 'description',
      valuePath: 'descriptionContentBlocks',
      type: 'blocks-preview',
      editorType: 'blocks',
      key: 'description',
      width: 400,
    });

    this.cache['positionCol'] = new SheetColData({
      name: 'position',
      key: 'position',
      type: 'position-text',
      editorType: 'geometry',
      valuePath: 'position',
      width: 200,
    });

    this.cache['mapsCol'] = new SheetColData({
      name: 'maps',
      key: 'maps',
      type: 'map-name-list',
      editorType: 'map-name-list',
      valuePath: 'maps',
    });

    this.cache['infoCreatedOnCol'] = new SheetColData({
      name: 'info.createdOn',
      type: 'local-date-time',
      valuePath: 'info.created-on',
    });

    this.cache['infoLastChangeOnCol'] = new SheetColData({
      name: 'info.lastChangeOn',
      type: 'local-date-time',
      valuePath: 'info.lastChangeOn',
    });

    this.cache['infoChangeIndexOnCol'] = new SheetColData({
      name: 'info.changeIndex',
      valuePath: 'info.changeIndex',
    });

    this.cache['infoOriginalAuthorOnCol'] = new SheetColData({
      name: 'info.originalAuthor',
      key: 'originalAuthor',
      type: 'profile-link',
      valuePath: 'info.originalAuthor',
      editorType: 'user',
    });

    this.cache['infoPicturesCol'] = new SheetColData({
      name: 'pictures',
      key: 'pictures',
      type: 'list-length',
      valuePath: 'pictures',
      editorType: 'pictures',
    });

    this.cache['infoSoundsCol'] = new SheetColData({
      name: 'sounds',
      key: 'sounds',
      type: 'list-length',
      valuePath: 'sounds',
      editorType: 'sounds',
    });

    this.cache['infoIconsCol'] = new SheetColData({
      name: 'icons',
      key: 'icons',
      type: 'list-length',
      valuePath: 'icons',
      editorType: 'icons',
    });

    this.cache['infoVisibilityCol'] = new SheetColData({
      name: 'visibility',
      key: 'visibility',
      type: 'feature-visibility',
      valuePath: 'visibility',
      editorType: 'feature-visibility',
    });
  }

  get sorts() {
    return {
      field: this.sortBy,
      isAscending: this.isAscending,
    };
  }

  get cols() {
    if (!this.icon) {
      return this.generalCols;
    }

    return this.iconCols;
  }

  get generalCols() {
    const result = [];

    result.push(this.cache['idCol']);
    result.push(this.cache['nameCol']);
    result.push(this.cache['descriptionCol']);
    result.push(this.cache['positionCol']);
    result.push(this.cache['mapsCol']);
    result.push(this.cache['infoCreatedOnCol']);
    result.push(this.cache['infoLastChangeOnCol']);
    result.push(this.cache['infoChangeIndexOnCol']);
    result.push(this.cache['infoOriginalAuthorOnCol']);
    result.push(this.cache['infoPicturesCol']);
    result.push(this.cache['infoSoundsCol']);
    result.push(this.cache['infoIconsCol']);
    result.push(this.cache['infoVisibilityCol']);

    return result;
  }

  get iconCols() {
    let result = [];

    result.push(this.cache['idCol']);
    result.push(this.cache['nameCol']);

    const icon = this.selectedIcon;
    const attributes = icon.attributes;

    result = result.concat(
      attributes.map((a) => {
        const key = `${icon.id}-${a.name}`;

        if (!this.cache[key]) {
          this.cache[key] = attributeToSheetColData(icon, a);
        }

        return this.cache[key];
      })
    );

    return result;
  }

  get featureCount() {
    return this.model.meta.totalFeatures || this.model.meta.publicFeatures || 0;
  }

  get iconsWithAttributes() {
    return this.model.icons.filter((a) => a.attributes?.length);
  }

  get selectedIcon() {
    if (!this.icon) {
      return null;
    }

    return this.store.peekRecord('icon', this.icon);
  }

  async loadFeatures() {
    let skip = this.start;
    let limit = 0;

    for (let index = this.start; index <= this.end; index++) {
      if (!this.model.records[index].id) {
        skip = index;
        break;
      }
    }

    for (let index = this.end; index >= this.start; index--) {
      if (!this.model.records[index].id) {
        limit = index - skip;
        break;
      }
    }

    if (limit == 0) {
      return;
    }

    limit = Math.max(10, limit);

    let features = await this.store.query('feature', {
      map: this.model.map.id,
      skip,
      limit,
      sortBy: this.sortBy,
      sortOrder: this.isAscending ? 'asc' : 'desc',
    });

    features.forEach((feature, index) => {
      this.model.records.replace(skip + index, 1, [feature]);
    });
  }

  @action
  selectTab(value) {
    this.icon = value?.id ?? null;
  }

  @action
  updateSorting(field, isAscending) {
    this.sortBy = field;
    this.isAscending = isAscending;
  }

  @action
  async edit(record, col, value) {
    if (!record) {
      return;
    }

    switch (col.valuePath) {
      case 'name':
        record.name = value;
        break;

      case 'descriptionContentBlocks':
        record.descriptionContentBlocks = value;
        break;

      case 'position':
        record.position = value;
        break;

      case 'maps':
        record.maps = value;
        break;

      case 'info.originalAuthor':
        record.info.originalAuthor = value;
        break;

      case 'pictures':
        record.pictures = value;
        break;

      case 'sounds':
        record.sounds = value;
        break;

      case 'icons':
        record.icons = value;
        break;

      case 'visibility':
        record.visibility = value;
        break;

      default:
        record.setAttribute(col.icon, col.name, value);
    }

    try {
      await record.save();
    } catch (err) {
      this.notifications.handleError(err);
    }
  }

  @action
  async scroll(start, end) {
    this.start = start;
    this.end = end;
    debounce(this, this.loadFeatures, 20);
  }
}
