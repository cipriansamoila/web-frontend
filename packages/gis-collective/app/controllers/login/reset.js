import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class LoginResetController extends Controller {
  @service notifications;
  @service store;
  @service intl;
  @service fastboot;

  queryParams = ['email', 'token'];
  email = null;
  token = null;

  @tracked isNotValid = true;
  @tracked newPassword = '';
  @tracked userEmail = '';
  @tracked isWaiting = false;

  get hasEmailAndToken() {
    return this.email && this.token;
  }

  @action
  validationChange(isValid) {
    this.isNotValid = !isValid;
  }

  @action
  change(value) {
    this.newPassword = value;
  }

  @action
  async changePassword() {
    try {
      await this.store
        .adapterFor('user')
        .forgotPassword(this.email, this.token, this.newPassword);

      this.notifications.showMessage(
        'change password',
        'Your password has been changed!'
      );
      this.transitionToRoute('login.index');
    } catch (err) {
      this.notifications.handleError(err);
    }

    this.isWaiting = false;
  }

  @action
  resetPassword() {
    return this.store
      .adapterFor('user')
      .forgotPassword(this.userEmail)
      .then(() => {
        return this.notifications.showMessage(
          'Forgot password',
          'If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes.'
        );
      })
      .catch((err) => {
        this.isWaiting = false;
        this.notifications.handleError(err);
      });
  }
}
