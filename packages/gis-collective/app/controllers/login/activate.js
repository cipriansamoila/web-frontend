import Controller from '@ember/controller';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class ActivateController extends Controller {
  @service notifications;
  @service fastboot;

  queryParams = ['email', 'token'];
  @tracked email = null;
  @tracked token = null;
  @tracked userEmail = '';
  @tracked isWaiting = false;

  get isActivationResponse() {
    return !this.model.error && this.email && this.token;
  }

  get errorCode() {
    return this.model.error ? 'activation-expire-error' : '';
  }

  @action
  activate() {
    this.isWaiting = true;

    return this.store
      .adapterFor('user')
      .activate(this.userEmail)
      .then(() => {
        this.isWaiting = false;
        this.notifications.showMessage(
          'Activate account',
          'If your email is in our database, a new link was sent.'
        );

        this.transitionToRoute('login.index');
      })
      .catch((err) => {
        this.isWaiting = false;
        this.notifications.handleError(err);
      });
  }
}
