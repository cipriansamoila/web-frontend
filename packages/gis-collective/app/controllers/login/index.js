import Controller from '@ember/controller';
import config from '../../config/environment';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class LoginIndexController extends Controller {
  @service session;
  @service intl;
  @service fastboot;
  @service store;
  @service user;
  @service preferences;

  @tracked identification = '';
  @tracked password = '';
  @tracked isLoading = false;
  @tracked errorMessage;

  host = config.apiUrl;
  queryParams = ['redirect'];

  @tracked redirect = null;

  get hasArticle() {
    if (this.model.article.title.trim() == '') {
      return false;
    }

    if (
      !this.model.article.contentBlocks ||
      !this.model.article.contentBlocks.blocks ||
      !this.model.article.contentBlocks.blocks.length
    ) {
      return false;
    }

    return true;
  }

  get redirectPath() {
    const profile = this.user.profileData;

    if (
      profile?.showWelcomePresentation &&
      this.preferences.showWelcomePresentation
    ) {
      this.store.findRecord('user-profile', this.user.id).then((profile) => {
        profile.showWelcomePresentation = false;
        profile.save();
      });
      return `/presentation/welcome`;
    }

    return `${this.redirect ?? ''}/`;
  }

  @action
  async authenticate() {
    this.errorMessage = null;
    this.isLoading = true;

    try {
      await this.session.authenticate(
        'authenticator:oauth2',
        this.identification.toLowerCase(),
        this.password
      );

      await this.user.getUserData();
      await this.user.loadProfile();

      this.replaceRoute(`${this.redirectPath}?locale=${this.intl.locale[0]}`);
    } catch (reason) {
      this.errorMessage = reason?.responseJSON?.error || reason;
    }

    this.isLoading = false;

    return false;
  }
}
