import Controller from './base';
import AlertMessage from '../../../lib/alert-message';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class WizardImportFilePreviewController extends Controller {
  @tracked currentIndex = 0;
  @tracked isAnalyzing = false;
  @tracked meta;

  get title() {
    return 'preview';
  }

  get previewList() {
    if (this.isAnalyzing) {
      return [];
    }

    return this.meta?.preview ?? [];
  }

  get alertList() {
    const list = [];

    if (this.previewList.length == 0) {
      list.push(
        new AlertMessage(
          this.intl.t('message-alert-file-without-preview'),
          'danger',
          'message-alert-file-without-preview'
        )
      );
    }

    return list;
  }

  get canImport() {
    return !this.fastboot.isFastBoot && this.alertList.length == 0;
  }

  get selectedItem() {
    return this.previewList[this.currentIndex];
  }

  get currentVisibleIndex() {
    return this.currentIndex + 1;
  }

  @action
  async preview() {
    this.isAnalyzing = true;

    try {
      this.meta = await await this.model.file.metaPreview();
      // eslint-disable-next-line no-empty
    } catch {}

    this.isAnalyzing = false;
  }

  @action
  nextPage() {
    this.currentIndex++;

    if (this.currentIndex >= this.previewList.length) {
      this.currentIndex = 0;
    }
  }

  @action
  prevPage() {
    this.currentIndex--;

    if (this.currentIndex < 0) {
      this.currentIndex = this.previewList.length - 1;
    }
  }

  @action
  queryItem(model, id) {
    try {
      return this.store.find(model, id);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }
  }

  @action
  async submit() {
    await this.model.file.import();
    this.transitionToRoute('wizard.import.file-log', this.model.file.get('id'));
  }
}
