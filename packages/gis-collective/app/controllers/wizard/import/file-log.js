import Controller from './base';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class WizardImportFileLogController extends Controller {
  @tracked log = '';
  @tracked meta;
  @tracked interval = 3000;

  get title() {
    return 'fields matching';
  }

  @action
  async update() {
    this.meta = await this.model?.file?.reloadMeta();
    const storedLog = await this.model?.file?.getLog();

    this.log = storedLog.log;

    if (this.meta.status == 'success') {
      this.interval = 0;
    }
  }
}
