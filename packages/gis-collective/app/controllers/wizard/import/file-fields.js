import Controller from './base';
import AlertMessage from '../../../lib/alert-message';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class WizardImportFileController extends Controller {
  @tracked isAnalyzing;

  get title() {
    return 'fields matching';
  }

  get alertList() {
    const list = [];
    const file = this.model?.file ?? {};

    if (!this.isAnalyzing && this.fields.length == 0) {
      list.push(
        new AlertMessage(
          this.intl.t('message-alert-file-not-analyzed-yet'),
          'danger',
          'file-not-analyzed-yet'
        )
      );
    }

    if (file.meta?.status == 'success') {
      list.push(
        new AlertMessage(
          this.intl.t('message-alert-file-already-imported'),
          'warning',
          'file-already-imported'
        )
      );
    }

    return list;
  }

  get fields() {
    const fields = this.model?.file?.options?.fields ?? [];

    if (!fields.length) {
      return [];
    }

    return fields;
  }

  get destinationMap() {
    const map = this.model.file?.options?.destinationMap;

    if (!map || map == '') {
      return 'undefined';
    }

    return this.model.file.options.destinationMap;
  }

  get teamMaps() {
    const name = this.intl.t('match based on a field');
    const list = [{ id: 'undefined', name }];

    this.model.teamMaps.forEach((a) => {
      list.push({
        id: a.id,
        name: a.name,
      });
    });

    return list;
  }

  get updateFieldList() {
    return [
      { id: '-', name: this.intl.t('do not update') },
      { id: '_id', name: '_id' },
      { id: 'name', name: this.intl.t('name') },
    ];
  }

  get updateBy() {
    const updateBy = this.model.file?.options?.updateBy;

    if (['_id', 'name'].indexOf(updateBy) == -1) {
      return '-';
    }

    return updateBy;
  }

  get extraIcons() {
    return (
      this.model?.file?.options?.extraIcons?.map((id) =>
        this.store.peekRecord('icon', id)
      ) ?? []
    );
  }

  @action
  iconsChanged(value) {
    this.model.file.options.extraIcons = value.map((a) => a.id);
  }

  @action
  async analyze() {
    this.isAnalyzing = true;

    try {
      await this.model.file.analyze();
      await this.model.file.reload();
      // eslint-disable-next-line no-empty
    } catch {}

    this.isAnalyzing = false;
  }

  @action
  selectUpdateBy(value) {
    const newValue = value.id == '-' ? '' : value.id;

    this.model.file.options.updateBy = newValue;
  }

  @action
  selectChanged(map) {
    if (map?.id == 'undefined') {
      this.model.file.options.destinationMap = '';
    } else {
      this.model.file.options.destinationMap = map?.id ?? '';
    }
  }

  @action
  updateMapping(key, value) {
    this.model.file.options.fields.forEach((field, index) => {
      if (field.key != key) return;

      this.model.file.options.fields[index].destination = value;
    });
  }

  @action
  async submit() {
    await this.model.file.save();
    this.transitionToRoute('wizard.import.file-preview', this.model.file.id);
  }
}
