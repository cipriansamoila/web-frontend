import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default class WizardImportFileBaseController extends Controller {
  @service intl;
  @service fastboot;
  @service store;
  @service session;
  @service router;

  get breadcrumbs() {
    return [
      {
        route: 'browse',
        text: this.intl.t('browse'),
        capitalize: true,
      },
      {
        route: 'browse.maps',
        text: this.intl.t('maps'),
        capitalize: true,
      },
      {
        route: 'browse.sites',
        query: { map: this.model.map.id },
        text: this.model.map.name,
      },
      {
        route: 'manage.maps.import',
        model: this.model.map.id,
        text: this.intl.t('map files'),
        capitalize: true,
      },
      {
        text: this.intl.t(this.title),
        capitalize: true,
      },
    ];
  }
}
