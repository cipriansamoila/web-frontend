import Controller from './base';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { rawBlobToBase64 } from '../../../lib/base64';
import config from '../../../config/environment';
import { htmlSafe } from '@ember/template';

export default class WizardImportFileSelectController extends Controller {
  @tracked file;
  @tracked chunkProgress = 0;

  host = config.apiUrl;
  chunk = 1024 * 1024;

  @tracked isUploading;
  @tracked currentChunk;
  @tracked chunkProgress;
  @tracked begin;

  get title() {
    return 'select a file';
  }

  get uploadProgress() {
    const progress = (this.begin + this.chunkProgress) / this.file.size;
    return parseInt(progress * 10000) / 100;
  }

  get uploadProgressStyle() {
    return htmlSafe(`width: ${this.uploadProgress}%;`);
  }

  slice(file, start, end) {
    var slice = file.mozSlice
      ? file.mozSlice
      : file.webkitSlice
      ? file.webkitSlice
      : file.slice
      ? file.slice
      : () => {};

    return slice.bind(file)(start, end);
  }

  upload(message) {
    let { access_token } = this.session.data.authenticated;

    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();

      xhr.onreadystatechange = () => {
        this.chunkProgress = 0;
        let response = '';

        if (xhr.readyState == 4) {
          response = xhr.response;

          try {
            response = JSON.parse(response);
            // eslint-disable-next-line no-empty
          } catch {}
        }

        if (xhr.status && xhr.readyState == 4) {
          if (xhr.status >= 200 && xhr.status < 300) {
            resolve(response);
          } else {
            reject(response);
          }
        }
      };

      xhr.onerror = reject;

      xhr.upload.addEventListener('progress', (event) => {
        this.chunkProgress = event.loaded;
      });

      xhr.open('POST', this.uploadUrl, true);
      xhr.setRequestHeader('Authorization', `Bearer ${access_token}`);
      xhr.setRequestHeader('Content-Type', `application/json`);
      xhr.send(JSON.stringify(message));
    });
  }

  get hasNoSelection() {
    return !this.file;
  }

  get uploadUrl() {
    return `${this.host}/maps/${this.model.map.id}/files`;
  }

  @action
  fileSelect(file) {
    this.file = file;
  }

  @action
  async startUpload() {
    this.begin = 0;
    let end = this.chunk;

    let message = {
      name: this.file.name,
      contentType: this.file.type,
      data: 'aGVsbG8K',
      size: this.chunk,
      chunk: 0,
    };

    this.isUploading = true;
    this.currentChunk = 0;

    do {
      this.chunkProgress = 0;
      let blobData = this.slice(this.file, this.begin, end);
      message.data = await rawBlobToBase64(blobData);
      message.chunk = this.currentChunk;

      const result = await this.upload(message);
      message._id = result.file;

      this.currentChunk++;
      this.begin = end;
      end = this.begin + this.chunk;
    } while (this.begin < this.file.size);

    this.router.transitionTo('wizard.import.file-fields', message._id);

    this.isUploading = false;
  }
}
