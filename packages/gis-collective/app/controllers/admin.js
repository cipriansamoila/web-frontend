import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default class AdminController extends Controller {
  @service fullscreen;
}
