import RESTSerializer from '@ember-data/serializer/rest';

export default class FeatureSerializer extends RESTSerializer {
  primaryKey = '_id';
  attrs = {
    issueCount: { serialize: false },
    canEdit: { serialize: false },
    isMasked: { serialize: false },
    contributors: { serialize: false },
  };
}
