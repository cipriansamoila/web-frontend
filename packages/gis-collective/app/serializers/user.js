import RESTSerializer from '@ember-data/serializer/rest';

export default class ApplicationSerializer extends RESTSerializer {
  primaryKey = '_id';
  attrs = {
    isActive: { serialize: false },
    name: { serialize: false },
  };
}
