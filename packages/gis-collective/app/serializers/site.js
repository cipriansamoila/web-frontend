import RESTSerializer from '@ember-data/serializer/rest';

export default class SiteSerializer extends RESTSerializer {
  primaryKey = '_id';
  attrs = {
    pictures: { serialize: 'ids' },
    canEdit: { serialize: false },
    issueCount: { serialize: false },
  };
}
