import RESTSerializer from '@ember-data/serializer/rest';

export default class PictureSerializer extends RESTSerializer {
  primaryKey = '_id';
  attrs = {
    hash: { serialize: false },
  };
}
