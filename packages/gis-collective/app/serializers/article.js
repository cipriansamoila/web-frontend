import RESTSerializer from '@ember-data/serializer/rest';

export default class ArticleSerializer extends RESTSerializer {
  primaryKey = '_id';
  attrs = {
    canEdit: { serialize: false },
  };
}
