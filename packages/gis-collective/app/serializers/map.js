import RESTSerializer from '@ember-data/serializer/rest';

export default class MapSerializer extends RESTSerializer {
  primaryKey = '_id';
  attrs = {
    canEdit: { serialize: false },
    meta: { serialize: false },
  };
}
