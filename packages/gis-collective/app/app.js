import Application from '@ember/application';
import Resolver from 'ember-resolver';
import loadInitializers from 'ember-load-initializers';
import config from './config/environment';
import { startSentry } from './sentry';
import Version from './version';

if (!Intl) {
  // eslint-disable-next-line no-console
  console.error('Intl is not supported in this environment');
}

import '@formatjs/intl-getcanonicallocales/polyfill';
import '@formatjs/intl-locale/polyfill';

import '@formatjs/intl-relativetimeformat/polyfill';
import '@formatjs/intl-relativetimeformat/locale-data/en'; // Add English data
import '@formatjs/intl-relativetimeformat/locale-data/ro'; // Add Romanian data
import '@formatjs/intl-relativetimeformat/locale-data/fr'; // Add French data
import '@formatjs/intl-relativetimeformat/locale-data/es'; // Add Spanish data

import '@formatjs/intl-pluralrules/polyfill';
import '@formatjs/intl-pluralrules/locale-data/en';
import '@formatjs/intl-pluralrules/locale-data/ro';
import '@formatjs/intl-pluralrules/locale-data/fr';
import '@formatjs/intl-pluralrules/locale-data/es';

import '@formatjs/intl-numberformat/polyfill';
import '@formatjs/intl-numberformat/locale-data/en';
import '@formatjs/intl-numberformat/locale-data/ro';
import '@formatjs/intl-numberformat/locale-data/fr';
import '@formatjs/intl-numberformat/locale-data/es';

export default class App extends Application {
  modulePrefix = config.modulePrefix;
  podModulePrefix = config.podModulePrefix;
  Resolver = Resolver;
}

// eslint-disable-next-line no-console
console.log('web-frontend version:', Version.version || 'unknown');

if (config.sentryDsn) {
  startSentry(config.sentryDsn, Version.version || 'unknown');
}

loadInitializers(App, config.modulePrefix);
