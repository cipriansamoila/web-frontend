import EmberObject from '@ember/object';
import { A } from '@ember/array';

const SubSet = EmberObject.extend({
  site: null,

  init() {
    this._super(...arguments);
    this.set('icons', A());
  },

  update() {
    this.icons.clear();

    return this.feature.iconSets.then((iconSets) => {
      return this.store
        .query('icon', { iconSet: iconSets.join() })
        .then((icons) => {
          this.icons.addObjects(icons);

          return this.icons;
        });
    });
  },
});

export default SubSet;
