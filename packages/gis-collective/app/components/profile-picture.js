import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';

export default class ProfilePictureComponent extends Component {
  get style() {
    if (!this.args.picture || !this.args.picture.get('picture')) {
      return htmlSafe(`background-image: url('/img/user.svg')`);
    }

    return htmlSafe(
      `background-image: url('${this.args.picture.get('picture')}')`
    );
  }
}
