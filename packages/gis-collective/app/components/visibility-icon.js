import Component from '@glimmer/component';

export default class VisibilityIconComponent extends Component {
  get isPending() {
    return this.args.value === -1;
  }

  get isPrivate() {
    return this.args.value === false || this.args.value === 0;
  }
}
