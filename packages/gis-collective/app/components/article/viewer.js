import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class ArticleViewerComponent extends Component {
  @service router;

  get value() {
    if (!this.args.value || !this.args.value.blocks) {
      return { blocks: [] };
    }

    return this.args.value;
  }

  @action
  updateLinks(element) {
    if (!this.args.target) {
      return;
    }

    const links = element.querySelectorAll('a');

    links.forEach((link) => {
      link.setAttribute('target', this.args.target);
    });
  }

  addHashTagLinks(content) {
    const pieces = content.split('#');

    pieces.forEach((element, index) => {
      if (index == 0) {
        return;
      }

      if (element.length == 0) {
        pieces[index] = '#';
        return;
      }

      const prev = pieces[index - 1];

      if ((prev.endsWith('&') || !prev.endsWith(' ')) && prev.length > 0) {
        pieces[index - 1] = pieces[index - 1] + '#';
        return;
      }

      if (element.substring(0, 1) == ' ') {
        pieces[index] = '#' + element;
        return;
      }

      const sectionPieces = element.split(' ');
      let url;

      try {
        url = this.router.urlFor('browse.index') + '?tag=' + sectionPieces[0];
      } catch (err) {
        url = `/test/${sectionPieces[0]}`;
      }
      sectionPieces[0] = `<a href="${url}">#${sectionPieces[0]}</a>`;

      pieces[index] = sectionPieces.join(' ');
    });

    return pieces.join('');
  }

  getTextBlock(block) {
    return {
      type: block.type,
      data: {
        ...block.data,
        text: htmlSafe(this.addHashTagLinks(block.data.text)),
      },
    };
  }

  getList(block) {
    return {
      type: block.type,
      data: {
        ...block.data,
        items: block.data.items.map((a) => htmlSafe(this.addHashTagLinks(a))),
      },
    };
  }

  getQuote(block) {
    return {
      type: block.type,
      data: {
        ...block.data,
        text: htmlSafe(block.data.text),
        caption: htmlSafe(block.data.caption),
      },
    };
  }

  getTable(block) {
    const header = block.data.content[0].map((a) =>
      htmlSafe(this.addHashTagLinks(a))
    );
    const body = [];

    for (let i = 1; i < block.data.content.length; i++) {
      body.push(
        block.data.content[i].map((a) => htmlSafe(this.addHashTagLinks(a)))
      );
    }

    return {
      type: block.type,
      data: {
        header,
        body,
      },
    };
  }

  get plainBlocks() {
    return this.value.blocks
      .map((block) => {
        if (block.type === 'header') {
          return block.data?.text ?? '';
        }

        if (block.type === 'paragraph') {
          return block.data?.text ?? '';
        }

        if (block.type === 'list') {
          return block.data.items.join(', ');
        }

        if (block.type === 'quote') {
          return `"${block.data?.text ?? ''}"`;
        }

        return `-- ${block.type} --`;
      })
      .join('\n\n');
  }

  get blocks() {
    return this.value.blocks.map((block) => {
      if (block.type === 'header') {
        return this.getTextBlock(block);
      }

      if (block.type === 'paragraph') {
        return this.getTextBlock(block);
      }

      if (block.type === 'list') {
        return this.getList(block);
      }

      if (block.type === 'quote') {
        return this.getQuote(block);
      }

      if (block.type === 'table') {
        return this.getTable(block);
      }

      return block;
    });
  }
}
