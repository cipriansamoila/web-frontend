import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import sanitizeHtml from 'sanitize-html';

export default class ArticleFirstParagraphComponent extends Component {
  get max() {
    if (this.args.max) {
      return parseInt(this.args.max);
    }

    return 200;
  }

  get firstParagraph() {
    if (!this.args.contentBlocks?.blocks) {
      return '';
    }

    const paragraph = this.args.contentBlocks.blocks.find(
      (a) => a.type == 'paragraph'
    );

    if (!paragraph) {
      return '';
    }

    if (paragraph.data.text.length < this.max - 5) {
      const clean = sanitizeHtml(paragraph.data.text);
      return htmlSafe(clean);
    }

    const clean = paragraph.data.text.replace(/(<([^>]+)>)/gi, '');
    return htmlSafe(clean.substr(0, this.max) + '...');
  }
}
