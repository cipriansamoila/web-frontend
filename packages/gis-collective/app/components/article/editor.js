import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import sanitizeHtml from 'sanitize-html';
import { inject as service } from '@ember/service';

export default class ArticleEditorComponent extends Component {
  @tracked _value;
  @tracked isSaving;
  @tracked isChanged;

  @service intl;

  get hasNoChanges() {
    return this._value ? false : true;
  }

  get value() {
    if (!this._value) {
      return this.args.value;
    }

    return this._value;
  }

  get title() {
    return this.value.blocks.find(
      (a) => a.type == 'header' && a.data.level == 1 && a.data.text.trim() != ''
    );
  }

  get hasMissingTitle() {
    if (this.args.allowNoHeading) {
      return false;
    }

    if (!this.value || !this.value.blocks) {
      return true;
    }

    return !this.title;
  }

  get hasNoParagraph() {
    if (!this.value?.blocks) {
      return true;
    }

    const paragraph = this.value.blocks.find((a) => a.type == 'paragraph');

    if (!paragraph) {
      return true;
    }

    return false;
  }

  @action
  change(value) {
    this._value = value;
    this._value.blocks = value.blocks.map((a) => ({
      type: a.type,
      data: a.data,
    }));

    if (this.args.onChange) {
      let title = '';

      if (this.title?.data?.text) {
        title = this.title.data.text;
      }

      const cleanTitle = sanitizeHtml(title, {
        allowedTags: [],
        allowedAttributes: {},
      });

      this.args.onChange(htmlDecode(cleanTitle), this._value);
    }

    this.isChanged = true;
  }

  @action
  ready(editor) {
    this.editor = editor;
  }

  @action
  setTitlePlaceholder() {
    this.editor?.blocks?.insert(
      'header',
      {
        level: 1,
        text: this.intl.t('new title'),
      },
      undefined,
      0,
      true
    );
  }

  @action
  setParagraphPlaceholder() {
    let pos = 0;

    if (this.value && this.value.blocks.length) {
      pos = this.value.blocks.length;
    }

    this.editor?.blocks?.insert(
      'paragraph',
      {
        text: this.intl.t('new paragraph'),
      },
      undefined,
      pos,
      true
    );
  }
}

function htmlDecode(input) {
  var e = document.createElement('textarea');
  e.innerHTML = input;
  // handle case of empty input
  return e.childNodes.length === 0 ? '' : e.childNodes[0].nodeValue.trim();
}
