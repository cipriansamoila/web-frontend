import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';
import { later } from '@ember/runloop';

export default class ArticleManageComponent extends Component {
  @service fastboot;

  @tracked isSaving;
  @tracked _value;
  @tracked title;
  @tracked showEditor = true;

  get isDisabled() {
    return this.fastboot.isFastBoot || !this._value || this.isSaving;
  }

  get value() {
    if (this._value) {
      return this._value;
    }

    if (this.args.value) {
      return this.args.value;
    }

    return {};
  }

  @action
  reset() {
    this.showEditor = false;
    this._value = null;

    later(() => {
      this.showEditor = true;
    });
  }

  @action
  async publish() {
    this.isSaving = true;
    if (this.args.onPublish) {
      await this.args.onPublish(this.title, this._value);
    }
    this.isSaving = false;
  }

  @action
  change(title, value) {
    this.title = title;
    this._value = value;
  }
}
