import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import sanitizeHtml from 'sanitize-html';

export default class ArticleTitleComponent extends Component {
  get title() {
    if (
      !this.args.value ||
      !this.args.value.blocks ||
      !this.args.value.blocks.length
    ) {
      return '';
    }

    const header = this.args.value.blocks.find(
      (a) => a.type == 'header' && a.data.level == 1
    );

    if (header?.data?.text) {
      const clean = sanitizeHtml(header.data.text);
      return htmlSafe(clean);
    }

    return '';
  }
}
