import Component from '@glimmer/component';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { tracked } from '@glimmer/tracking';

export default class CollapseComponent extends Component {
  @tracked isShowing;

  elementId = 'editor-js-' + guidFor(this);

  @action
  toggle() {
    this.isShowing = !this.isShowing;
  }
}
