import Component from '@glimmer/component';
import { action } from '@ember/object';
import { Toast } from 'bootstrap';
import { inject as service } from '@ember/service';

export default class ToastComponent extends Component {
  @service fastboot;

  @action
  setup(element) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    this.toast = new Toast(element, {
      delay: 10000,
    });

    this.toast.show();

    element.addEventListener('hidden.bs.toast', () => {
      if (this.isDestroyed || this.isDestroying) {
        return;
      }

      if (this.args.onClose) {
        this.args.onClose(this.args.toastId);
      }
    });
  }
}
