import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  fastboot: service(),
  isEdit: false,

  actions: {
    cancel() {
      if (this.onCancel) {
        this.onCancel(this.value);
      }
    },

    save() {
      if (this.onSave) {
        this.onSave(this.value);
      }
    },

    edit() {
      if (this.onEdit) {
        this.onEdit(this.value);
      }
    },
  },
});
