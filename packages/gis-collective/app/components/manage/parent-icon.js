import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageParentIconComponent extends Component {
  @service store;
  @tracked _value = null;

  get value() {
    if (this._value === null) {
      return this.args.value;
    }

    return this._value;
  }

  set value(newValue) {
    this._value = newValue;
  }

  @action
  save() {
    this.args.onSave?.(this.value);
  }

  @action
  cancel() {
    this._value = null;
    this.args.onCancel?.();
  }

  @action
  parentIconChanged(icon) {
    this._value = icon;
  }
}
