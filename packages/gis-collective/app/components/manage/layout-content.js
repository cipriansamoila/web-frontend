import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { PageCol } from '../../transforms/page-col-list';

export default class ManageLayoutContentComponent extends Component {
  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  @action
  changeCol(newElement) {
    const value = this.value.slice(0);
    let found = false;

    value.forEach((a, i) => {
      if (!a.fromJson) {
        a = new PageCol(a);
        value.replace(i, 1, [a]);
      }
    });

    value.forEach((a) => {
      if (a.col != newElement.col || a.row != newElement.row) {
        return;
      }

      if (
        JSON.stringify(a.data) != JSON.stringify(newElement.data) ||
        a.type != newElement.type
      ) {
        a.fromJson(newElement);
        found = true;
      }
    });

    if (!found) {
      value.addObject(newElement);
    }

    this._value = value;
  }

  @action
  save() {
    this.args.onSave?.(this.value);
    this._value = null;
  }
}
