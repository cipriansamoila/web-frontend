import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManageIconStyleComponent extends Component {
  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  get hasCustomStyle() {
    if (!this.value) {
      return false;
    }

    return this.value.hasCustomStyle;
  }

  set hasCustomStyle(value) {
    let types = {};

    if (this.value && this.value.types) {
      types = this.value.types;
    }

    this._value = {
      hasCustomStyle: value,
      types,
    };
  }

  get rawValue() {
    if (
      this.value &&
      this.value.types &&
      this.value.types &&
      this.value.types.toJSON
    ) {
      return this.value.types.toJSON();
    }

    if (!this.value || !this.value.types) {
      return {};
    }

    return this.value.types;
  }

  @action
  onChange(value) {
    this._value = {
      hasCustomStyle: this.hasCustomStyle,
      types: value,
    };
  }

  @action
  cancel() {
    if (this.args.onCancel) {
      this.args.onCancel(...arguments);
    }

    this._value = null;
  }

  @action
  save() {
    if (this.args.onSave) {
      this.args.onSave(this.value);
    }

    this._value = null;
  }

  @action
  edit() {
    if (this.args.onEdit) {
      this.args.onEdit(...arguments);
    }

    this._value = null;
  }
}
