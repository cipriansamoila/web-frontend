import Component from '@glimmer/component';
import { action } from '@ember/object';
import { later } from '@ember/runloop';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';

class WrappedItem {
  @tracked selectedIds;
  @tracked item;

  constructor(selectedIds, item) {
    this.selectedIds = selectedIds;
    this.item = item;
  }

  get selected() {
    if (this.selectedIds == 'all') {
      return true;
    }

    if (!this.selectedIds || !this.selectedIds.length) {
      return false;
    }

    return this.selectedIds.indexOf(this.item.id) != -1;
  }
}

export default class ManageListTableComponent extends Component {
  elementId = 'tlist-table-' + guidFor(this);
  _columns = [];

  get allSelected() {
    return this.args.selectedIds == 'all';
  }

  get hasDisabledSelectAll() {
    if (this.args.selectAll == 'enabled') {
      return false;
    }

    return true;
  }

  get showSelectAll() {
    if (this.args.selectAll === undefined) {
      return false;
    }

    return true;
  }

  get columns() {
    if (!this.args.columns) {
      this._columns = [];

      return this._columns;
    }

    if (this._columns.length > this.args.columns.length) {
      this._columns = [];
    }

    this._columns.forEach((list) => {
      list.forEach((item) => {
        item.selectedIds = this.args.selectedIds;
      });
    });

    if (this._columns.length < this.args.columns.length) {
      for (let i = this._columns.length; i < this.args.columns.length; i++) {
        this.addColumn(i, this.args.selectedIds);
      }
    }

    return this._columns;
  }

  addColumn(index, selections) {
    if (!this.args.columns[index]) {
      return;
    }

    this._columns.push(
      this.args.columns[index].map((item) => {
        return new WrappedItem(selections, item);
      })
    );
  }

  get headItems() {
    if (!this.args.header) {
      return [];
    }

    return this.args.header.split(',');
  }

  isSelected(id) {
    if (this.args.selectedIds == 'all') {
      return true;
    }

    if (!this.args.selectedIds || !this.args.selectedIds.length) {
      return false;
    }

    return this.args.selectedIds.indexOf(id) != -1;
  }

  toggleId(id) {
    let list = [];

    if (this.args.selectedIds == 'all') {
      list = this.args.columns.flatMap((a) => a.map((b) => b.id));
    } else if (this.args.selectedIds && this.args.selectedIds.length) {
      list = this.args.selectedIds;
    }

    if (this.isSelected(id)) {
      list = list.filter((a) => a != id);
    } else {
      list.push(id);
    }

    this.args.onSelect?.(list);
  }

  toggleAll() {
    this.args.onSelect?.(!this.allSelected ? 'all' : []);
  }

  @action
  delete(item) {
    return this.args.onDelete(item);
  }

  @action
  select(id) {
    later(() => {
      this.toggleId(id);
    }, 10);
  }

  @action
  selectAll() {
    later(() => {
      this.toggleAll();
    }, 10);
  }
}
