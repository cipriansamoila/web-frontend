import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { PageCol } from '../../transforms/page-col-list';

export default class ManagePageRowsComponent extends Component {
  @tracked rows = [];

  ensureRows(length) {
    if (length < this.rows.length) {
      return;
    }

    for (let i = this.rows.length; i <= length; i++) {
      this.rows.push([]);
    }
  }

  ensureCols(row, length) {
    if (length < this.rows[row].length) {
      return;
    }

    for (let i = this.rows[row].length; i <= length; i++) {
      this.rows[row].push(null);
    }
  }

  @action
  setupRows() {
    this.rows = [];

    this.args.value?.forEach?.((pageCol) => {
      this.ensureRows(pageCol.row);
      this.ensureCols(pageCol.row, pageCol.col);
      this.rows[pageCol.row][pageCol.col] = pageCol;
    });
  }

  @action
  changeCol(pageCol) {
    this.rows[pageCol.row][pageCol.col] = pageCol;
  }

  @action
  addRow() {
    const row = this.rows.length;

    this.rows.push([
      new PageCol({
        col: 0,
        row,
        type: 'article',
        data: {},
      }),
      new PageCol({ col: 1, row, type: 'picture', data: {} }),
    ]);

    // eslint-disable-next-line no-self-assign
    this.rows = this.rows;
  }

  @action
  deleteRow(index) {
    this.rows.removeAt(index);

    this.rows.forEach((row, rowIndex) => {
      row.forEach((pageCol, colIndex) => {
        pageCol.col = colIndex;
        pageCol.row = rowIndex;
      });
    });
  }

  @action
  save() {
    return this.args.onSave?.(this.rows.flatMap((a) => a));
  }
}
