import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { PageCol } from '../../transforms/page-col-list';
import { inject as service } from '@ember/service';

export default class ManagePageColComponent extends Component {
  @service pageCols;
  @tracked _type = null;

  get type() {
    if (this._type) {
      return this._type;
    }

    return this.args.value?.type;
  }

  @action
  typeChanged(type) {
    if (this.type == type) {
      return;
    }

    this._type = type;
    const col = new PageCol(this.args.value);
    col.data = {};
    col.type = type;

    this.args.onChange?.(col);
  }

  @action
  dataChanged(data) {
    if (JSON.stringify(this.args.value?.data) == JSON.stringify(data)) {
      return;
    }

    const col = new PageCol(this.args.value);
    col.data = data;

    this.args.onChange?.(col);
  }
}
