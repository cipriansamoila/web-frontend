import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { Dropdown } from 'bootstrap';

export default class ManageListCardComponent extends Component {
  @service intl;
  @service notifications;

  get answers() {
    const deleteOptions = this.deleteOptions || 'yes:' + this.intl.t('yes');

    return deleteOptions.split(',').map((a) => {
      const pieces = a.split(':');

      return { text: pieces[1], key: pieces[0], class: 'btn-danger' };
    });
  }

  get deleteOptions() {
    return this.args.deleteOptions;
  }

  @action
  setupDropdown(element) {
    this.dropdown = new Dropdown(element, {
      flip: true,
      boundary: 'window',
    });
  }

  @action
  delete(item) {
    return this.notifications
      .ask({
        title: this.intl.t('delete ' + item.constructor.modelName, { size: 1 }),
        question: this.intl.t('delete-confirmation-message', {
          name: item.name,
          size: 1,
        }),
        answers: this.answers,
      })
      .then((response) => {
        return this.args.onDelete(item, response);
      })
      .catch(() => {});
  }
}
