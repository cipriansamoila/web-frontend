import Component from '@glimmer/component';
import { all } from 'rsvp';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class ManageMapsFileListComponent extends Component {
  @service toaster;
  @service intl;

  interval = 3000;
  viewMode = 'table';
  queryParams = ['viewMode'];
  cachedFiles = [];

  get files() {
    this.cachedFiles.clear();

    if (this.args.files?.length) {
      this.cachedFiles.pushObject(this.args.files);
    }

    return this.cachedFiles;
  }

  get fileActions() {
    return [];
  }

  updateFileMetadata() {
    if (!this.args.files) {
      return;
    }

    all(this.args.files.map((a) => a.reloadMeta()))
      .then(() => {
        this.fileWatchInterval = setTimeout(() => {
          if (this.isDestroyed) {
            return;
          }

          this.updateFileMetadata();
        }, this.interval);
      })
      .catch((err) => this.toaster.handleError(err));
  }

  willDestroyElement() {
    this.stopFileWatch();
  }

  @action
  setupTimer() {
    this.stopFileWatch();
    this.updateFileMetadata();
  }

  @action
  stopFileWatch() {
    if (this.fileWatchInterval) {
      clearTimeout(this.fileWatchInterval);
    }

    this.fileWatchInterval = null;
  }

  @action
  import() {
    this.args.onImport(...arguments);
  }

  @action
  showLog() {
    this.args.onShowLog(...arguments);
  }

  @action
  delete() {
    this.args.onDelete(...arguments);
  }

  @action
  fileAction(key, items) {
    if (key == 'import') {
      return all(items.map((a) => this.import(a)));
    }
  }
}
