import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import GeoJson from '../../lib/geoJson';

export default class ManageAreaComponent extends Component {
  @service store;
  @service fastboot;
  @tracked editMode = 'box';
  @tracked _value;

  get title() {
    return this.args.title || 'geometry';
  }

  get value() {
    if (!this._value) {
      return this.args.value;
    }

    return this._value;
  }

  set value(v) {
    this._value = v;
  }

  performAreaSearch() {}

  @action
  changeEditMode(value) {
    this.editMode = value;
  }

  @action
  change(value) {
    this._value = value;
  }

  @action
  selectArea() {}

  @action
  search() {}

  @action
  onLocalize() {
    return this.position.refreshCoordinates();
  }

  @action
  cancel() {
    this._value = null;
    this.args.onCancel?.();
  }

  @action
  save() {
    this.args.onChange?.(new GeoJson(this.value));
    this._value = null;
  }
}
