import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageActionsSelectedComponent extends Component {
  @tracked isBusy;
  @tracked isDeleting;
  @tracked currentAction;

  @action
  handleDelete() {
    const result =
      this.args.selected == 'all'
        ? this.args.onAction('deleteAll', 'all')
        : this.args.onDelete(this.args.selected);

    if (result && result.then) {
      this.isBusy = true;
      this.isDeleting = true;

      result.then(
        () => {
          this.isBusy = false;
          this.isDeleting = false;

          if (this.args.onClearSelection) {
            return this.args.onClearSelection();
          }
        },
        () => {
          this.isBusy = false;
          this.isDeleting = false;
        }
      );
    } else if (this.args.onClearSelection) {
      this.args.onClearSelection();
    }

    return result;
  }

  @action
  handleAction(key) {
    const result = this.args.onAction(key, this.args.selected);

    if (result && result.then) {
      this.isBusy = true;
      this.currentAction = key;

      result.then(
        () => {
          this.isBusy = false;
          this.currentAction = '';
        },
        () => {
          this.isBusy = false;
          this.currentAction = '';
        }
      );
    }

    return result;
  }

  @action
  clearSelection() {
    if (this.args.onClearSelection) {
      this.args.onClearSelection();
    }
  }
}
