import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class ManageRowSaveCancelComponent extends Component {
  get isDisabled() {
    if (this.args.ignoreValidation) {
      return false;
    }

    if (this.args.isDisabled) {
      return this.args.isDisabled;
    }

    if (!this.args.model) {
      return false;
    }

    return !this.args.model.hasDirtyAttributes;
  }

  @action
  saveModel() {
    return this.args.onSave();
  }

  @action
  cancelModel() {
    return this.args.onCancel();
  }
}
