import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageSelectRecordComponent extends Component {
  @tracked _value = null;

  get value() {
    if (this._value === null) {
      return this.args.value;
    }

    return this._value;
  }

  @action
  change(value) {
    this._value = value;
  }

  @action
  save() {
    this.args.onSave?.(this.value);
  }
}
