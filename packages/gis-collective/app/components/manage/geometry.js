import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ManageGeometryComponent extends Component {
  @tracked tmpValue;

  get value() {
    if (this.tmpValue) {
      return this.tmpValue;
    }

    return this.args.value;
  }

  @action change(value) {
    this.tmpValue = value;
  }

  @action onSave() {
    let result;

    if (this.args.onSave) {
      result = this.args.onSave(this.value);
    }

    this.tmpValue = null;

    return result;
  }

  @action onCancel() {
    this.tmpValue = null;
    return this.args.onCancel(...arguments);
  }

  @action onEdit() {
    this.tmpValue = null;
    return this.args.onEdit(...arguments);
  }
}
