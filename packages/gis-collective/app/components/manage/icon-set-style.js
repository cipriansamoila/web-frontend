import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManageIconSetStyleComponent extends Component {
  @tracked _value;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  get rawValue() {
    if (this.value && this.value.toJSON) {
      return this.value.toJSON();
    }

    return this.value;
  }

  @action
  onChange(value) {
    this._value = value;
  }

  @action
  cancel() {
    if (this.args.onCancel) {
      this.args.onCancel(...arguments);
    }

    this._value = null;
  }

  @action
  save() {
    if (this.args.onSave) {
      this.args.onSave(this.value);
    }

    this._value = null;
  }

  @action
  edit() {
    if (this.args.onEdit) {
      this.args.onEdit(...arguments);
    }

    this._value = null;
  }
}
