import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManageMemberTableComponent extends Component {
  roleList = ['owners', 'leaders', 'members', 'guests'];
  cache = {};

  get members() {
    const list = this.args.members ?? [];
    const result = [];

    list.forEach((a) => {
      const id = a.user.id;
      const isPending = a.user.id.indexOf('@') != -1;

      if (!this.cache[id] && !isPending) {
        this.cache[id] = new UserWithProfile(
          a.user,
          this.args.requestProfile?.(id),
          a.role
        );
      }

      if (!this.cache[id] && isPending) {
        this.cache[id] = new UserWithProfile(
          { email: id },
          { fullName: '---' },
          a.role
        );
      }

      if (this.cache[id].role != a.role) {
        this.cache[id].role = a.role;
      }

      result.push(this.cache[id]);
    });

    return result;
  }

  @action
  remove() {
    return this.args.onRemove?.(...arguments);
  }

  @action
  updateRole(member, role) {
    return this.args.updateRole?.(member.user, role);
  }
}

class UserWithProfile {
  @tracked user;
  @tracked profile;
  @tracked role;

  constructor(user, profile, role) {
    this.user = user;
    this.profile = profile;
    this.role = role;
  }
}
