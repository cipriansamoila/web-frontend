import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import AttributeDefinition from '../../lib/attribute-definition';
import { guidFor } from '@ember/object/internals';

export default class ManageIconAttributesComponent extends Component {
  elementId = 'icon-attributes-' + guidFor(this);
  @tracked value = [];
  @tracked types = [
    'short text',
    'long text',
    'integer',
    'decimal',
    'boolean',
    'options',
    'options with other',
  ];

  @action
  attributeTypeChanged(index, value) {
    if (this.value[index].type == value) {
      return;
    }

    const newItem = new AttributeDefinition(this.value[index]);
    newItem.type = value;

    this.value.replace(index, 1, [newItem]);
  }

  @action
  attributeOptionsChanged(index, value) {
    if (this.value[index].options == value) {
      return;
    }

    const newItem = new AttributeDefinition(this.value[index]);
    newItem.options = value;

    this.value.replace(index, 1, [newItem]);
  }

  @action
  addAttribute() {
    this.value.pushObject(
      new AttributeDefinition({
        name: 'new attribute',
        type: 'short text',
      })
    );
  }

  @action
  edit() {
    const originalList = this.args.value || [];

    this.value = originalList.map((a) => new AttributeDefinition(a));

    return this.args.onEdit(...arguments);
  }

  @action
  cancel() {
    return this.args.onCancel(...arguments);
  }

  @action
  save() {
    return this.args.onSave(this.value || []);
  }

  @action
  deleteItem(index) {
    this.value.removeAt(index);
  }

  @action
  dragEnd({ sourceList, sourceIndex, targetList, targetIndex }) {
    if (sourceIndex === targetIndex || sourceList !== targetList) return;

    const item = sourceList.objectAt(sourceIndex);
    sourceList.removeAt(sourceIndex);
    sourceList.insertAt(targetIndex, item);
  }
}
