import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class ManageVisibilityComponent extends Component {
  @action
  toggleAll() {
    if (!this.args.onChangeAll) {
      return;
    }

    this.args.onChangeAll(!this.args.areAllShowing);
  }
}
