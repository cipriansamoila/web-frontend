import Component from '../../page-col/icon-attribute';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManageEditorsIconAttributeComponent extends Component {
  @tracked _value;
  @tracked _listValue;

  get listValue() {
    if (Array.isArray(this._listValue)) {
      return this._listValue;
    }

    return super.listValue;
  }

  set listValue(newValue) {
    this._listValue = newValue;
    this.args.onChange?.(newValue);
  }

  get value() {
    if (typeof this._value == 'string') {
      return this._value;
    }

    return super.value;
  }

  set value(newValue) {
    this._value = this.convertToType(newValue);
    this.args.onChange?.(this._value);
  }

  convertToType(value) {
    if (this.type == 'integer') {
      return parseInt(value);
    }

    if (this.type == 'decimal') {
      return parseFloat(value);
    }

    return value;
  }

  get type() {
    return this.attribute.type;
  }

  get attribute() {
    if (!this.args.options?.icon?.attributes?.length) {
      return {};
    }

    for (let attribute of this.args.options?.icon?.attributes) {
      if (attribute.name == this.key) {
        return attribute;
      }
    }

    return {};
  }

  get isBoolean() {
    return this.type == 'boolean';
  }

  get isOptions() {
    return this.type == 'options';
  }

  get isOptionsWithOther() {
    return this.type == 'options with other';
  }

  get options() {
    const values =
      this.attribute.options?.split(',').map((a) => a.trim()) ?? [];

    return { values };
  }

  @action
  listSetup() {
    this._listValue = super.listValue;
  }

  @action
  updateArrayValue(index, event) {
    const value = event.target ? event.target.value : event;

    this._listValue[index] = this.convertToType(value);
    this.args.onChange?.(this._listValue);
  }

  @action
  addValue() {
    const list = this.listValue;

    list.push('');
    this.listValue = list;
  }

  @action
  removeValue(index) {
    const list = this.listValue;

    list.removeAt(index);
    this.listValue = list;
  }
}
