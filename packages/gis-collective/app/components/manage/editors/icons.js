import Component from './base/modelList';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { addIconGroupToStore } from '../../../lib/icons';

export default class ManageEditorsIconsComponent extends Component {
  @tracked icons;
  @tracked showIconPicker;

  @tracked _viewMode;
  viewModeOptions = ['small', 'medium', 'cards'];

  @tracked _links;
  linksOptions = ['disabled', 'enabled'];

  @tracked _iconsPerLine;
  iconsPerLineOptions = ['auto', '1', '2', '3', '4', '5', '6'];

  @tracked _gutter;
  gutterOptions = ['default', '0', '1', '2', '3', '4', '5'];

  get gutter() {
    if (this._gutter) {
      return this._gutter;
    }

    return this.args.value?.data?.gutter ?? this.gutterOptions[0];
  }

  @action
  gutterChanged(value) {
    this._gutter = value;

    return this.triggerChange();
  }

  get iconsPerLine() {
    if (this._iconsPerLine) {
      return this._iconsPerLine;
    }

    return this.args.value?.data?.iconsPerLine ?? this.iconsPerLineOptions[0];
  }

  @action
  iconsPerLineChanged(value) {
    this._iconsPerLine = value;

    return this.triggerChange();
  }

  get viewMode() {
    if (this._viewMode) {
      return this._viewMode;
    }

    return this.args.value?.data?.viewMode ?? this.viewModeOptions[0];
  }

  @action
  viewModeChanged(value) {
    this._viewMode = value;

    return this.triggerChange();
  }

  get links() {
    if (this._links) {
      return this._links;
    }

    return this.args.value?.data?.links ?? this.linksOptions[0];
  }

  triggerChange() {
    return this.args.onChange?.({
      ids: this.records.map((a) => a.id || a._id),
      model: 'icon',
      viewMode: this.viewMode,
      links: this.links,
      gutter: this.gutter,
      rowColSize: this.iconsPerLine,
    });
  }

  @action
  linksChanged(value) {
    this._links = value;

    return this.triggerChange();
  }

  @action
  async setup() {
    let iconSet;

    try {
      if (this.args.options?.parent) {
        const feature = this.store.peekRecord(
          'feature',
          this.args.options.parent
        );
        const iconSets = await feature?.iconSets;

        iconSet = iconSets?.join();
      }
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }

    this.icons = await this.store.adapterFor('icon').group({ iconSet });

    addIconGroupToStore(this.icons, this.store);

    if (this.args.value?.data?.ids && !this.args.value?.data?.records) {
      this._records = this.args.value?.data?.ids
        .map((id) => this.store.peekRecord('icon', id))
        .filter((a) => Boolean(a));
    }
  }

  @action
  toggleShowIconPicker() {
    this.showIconPicker = !this.showIconPicker;
  }

  @action
  change(value) {
    this._records = value;

    if (!this.hasData) {
      return this.args.onChange?.(value);
    }

    return this.triggerChange();
  }
}
