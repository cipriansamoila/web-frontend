import Component from '@glimmer/component';
import { action } from '@ember/object';
import { toContentBlocks } from '../../../lib/content-blocks';
import { guidFor } from '@ember/object/internals';
import { tracked } from '@glimmer/tracking';

export default class ManageEditorsBlocksComponent extends Component {
  @tracked _blocks;
  @tracked _alignment;
  @tracked _lineHeight;
  @tracked _font;
  elementId = `manage-editors-banner-${guidFor(this)}`;
  alignmentOptions = ['start', 'center', 'end'];
  lineHeightOptions = ['base', '1', 'sm', 'lg'];
  fontOptions = ['default', 'monospace'];

  get alignment() {
    if (this._alignment) {
      return this._alignment;
    }

    if (this.hasAdvancedOptions && this.args.value?.data?.alignment) {
      return this.args.value.data.alignment;
    }

    return this.alignmentOptions[0];
  }

  get lineHeight() {
    if (this._lineHeight) {
      return this._lineHeight;
    }

    if (this.hasAdvancedOptions && this.args.value?.data?.lineHeight) {
      return this.args.value.data.lineHeight;
    }

    return this.lineHeightOptions[0];
  }

  get font() {
    if (this._font) {
      return this._font;
    }

    if (this.hasAdvancedOptions && this.args.value?.data?.font) {
      return this.args.value.data.font;
    }

    return this.fontOptions[0];
  }

  change() {
    const data = {
      ...this.blocks,
    };

    if (this.hasAdvancedOptions) {
      data['alignment'] = this.alignment;
      data['lineHeight'] = this.lineHeight;
      data['font'] = this.font;
    }

    this.args.onChange(data);
  }

  get hasAdvancedOptions() {
    return !!this.args.value?.data;
  }

  get blocks() {
    if (this._blocks) {
      return this._blocks;
    }

    if (this.args.value?.data) {
      return this.args.value?.data;
    }

    if (typeof this.args.value == 'string') {
      return toContentBlocks(this.args.value);
    }

    return this.args.value;
  }

  @action
  blocksChanged(title, value) {
    this._blocks = value;
    this.change();
  }

  @action
  alignmentChanged(value) {
    this._alignment = value;
    this.change();
  }

  @action
  lineHeightChanged(value) {
    this._lineHeight = value;
    this.change();
  }

  @action
  fontChanged(value) {
    this._font = value;
    this.change();
  }
}
