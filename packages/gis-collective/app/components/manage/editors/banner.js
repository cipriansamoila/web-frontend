import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { toContentBlocks } from '../../../lib/content-blocks';
import { inject as service } from '@ember/service';
import PictureMeta from '../../../lib/picture-meta';

export default class ManageEditorsBannerComponent extends Component {
  @service store;

  elementId = `manage-editors-banner-${guidFor(this)}`;
  sidePictureMobileOptions = ['hidden', 'before', 'after'];

  @tracked _heading = null;
  @tracked _subHeading = null;
  @tracked _description = null;
  @tracked _buttonLabel = null;
  @tracked _buttonLink = null;
  @tracked _sidePictureMobile = null;
  @tracked headingPicture = null;
  @tracked sidePicture = null;

  get sidePictureMobile() {
    if (this._sidePictureMobile) {
      return this._sidePictureMobile;
    }

    return this.args.value?.data?.sidePictureMobile;
  }

  get heading() {
    if (this._heading !== null) {
      return this._heading;
    }

    return this.args.value?.data?.heading;
  }

  set heading(value) {
    this._heading = value;
    this.change('heading', value);
  }

  get subHeading() {
    if (this._subHeading !== null) {
      return this._subHeading;
    }

    return this.args.value?.data?.subHeading;
  }

  set subHeading(value) {
    this._subHeading = value;
    this.change('subHeading', value);
  }

  get description() {
    if (this._description !== null) {
      return this._description;
    }

    return toContentBlocks(this.args.value?.data?.description);
  }

  set description(value) {
    this._description = value;
    this.change('description', value);
  }

  get buttonLabel() {
    if (this._buttonLabel !== null) {
      return this._buttonLabel;
    }

    return this.args.value?.data?.buttonLabel;
  }

  set buttonLabel(value) {
    this._buttonLabel = value;
    this.change('buttonLabel', value);
  }

  get buttonLink() {
    if (this._buttonLink !== null) {
      return this._buttonLink;
    }

    return this.args.value?.data?.buttonLink;
  }

  @action
  changeButtonLink(value) {
    this._buttonLink = value;
    this.change('buttonLink', value);
  }

  @action
  async setupPictures() {
    if (this.args.value?.data?.headingPicture) {
      this.headingPicture = this.store.findRecord(
        'picture',
        this.args.value?.data?.headingPicture
      );
    }

    if (this.args.value?.data?.sidePicture) {
      this.sidePicture = this.store.findRecord(
        'picture',
        this.args.value?.data?.sidePicture
      );
    }
  }

  change(key, value) {
    const data = {
      heading: this.heading,
      subHeading: this.subHeading,
      description: this.description,
      buttonLabel: this.buttonLabel,
      buttonLink: this.buttonLink,
      sidePictureMobile: this.sidePictureMobile,
      headingPicture: this.headingPicture?.get?.('id'),
      sidePicture: this.sidePicture?.get?.('id'),
    };

    if (key != 'headingPicture' && key != 'sidePicture') {
      data[key] = value;
    }

    this.args.onChange(data);
  }

  @action
  changeDescription(title, value) {
    this._description = value;
    this.change('description', value);
  }

  @action
  sidePictureMobileChanged(value) {
    this._sidePictureMobile = value;
    this.change('sidePictureMobile', value);
  }

  @action
  sidePictureChanged(index, value) {
    this.sidePicture = value;
    this.change('sidePicture', value);
  }

  @action
  headingPictureChanged(index, value) {
    this.headingPicture = value;
    this.change('headingPicture', value);
  }

  @action
  createImage() {
    return this.store.createRecord('picture', {
      name: '',
      picture: '',
      meta: new PictureMeta(),
    });
  }
}
