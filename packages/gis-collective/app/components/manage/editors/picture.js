import Component from './base/modelList';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import PictureMeta from '../../../lib/picture-meta';

export default class ManageEditorsPictureComponent extends Component {
  @service store;
  @tracked _picture;
  @tracked _options;

  get value() {
    return this.args.value?.data ?? {};
  }

  get options() {
    if (this._options) {
      return this._options;
    }

    return this.value.options ?? [];
  }

  get picture() {
    if (this._picture) {
      return this._picture;
    }

    return this.value.record;
  }

  get recordType() {
    return 'icon';
  }

  @action
  setupPicture() {
    if (this.args.value?.data?.id) {
      this._picture = this.store.findRecord('picture', this.args.value.data.id);
    }
  }

  @action
  createImage() {
    return this.store.createRecord('picture', {
      name: '',
      picture: '',
      meta: new PictureMeta(),
    });
  }

  @action
  pictureChanged(_, value) {
    this.args.onChange?.({
      id: value.id,
      model: 'picture',
      options: this.options,
    });
  }

  @action
  changeOptions(options) {
    this._options = options;
    this.args.onChange?.({
      id: this.value.id,
      model: 'picture',
      options: this.options,
    });
  }
}
