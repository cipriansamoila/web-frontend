import Component from './base/modelList';
import { action } from '@ember/object';

export default class ManageEditorsSoundsComponent extends Component {
  @action
  createSound() {
    return this.store.createRecord('sound', {
      name: '',
      feature: this.args.options?.parent,
      sound: '',
    });
  }
}
