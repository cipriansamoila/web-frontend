import Component from './base/modelList';
import { action } from '@ember/object';
import PictureMeta from '../../../lib/picture-meta';
import { tracked } from '@glimmer/tracking';

export default class ManageEditorsPicturesComponent extends Component {
  @tracked _options;
  @tracked _containerOptions;
  @tracked _heightSm;
  @tracked _heightMd;
  @tracked _heightLg;

  get heightSm() {
    if (this._heightSm) {
      return this._heightSm;
    }

    return this.value.heightSm;
  }

  get heightMd() {
    if (this._heightMd) {
      return this._heightMd;
    }

    return this.value.heightMd;
  }

  get heightLg() {
    if (this._heightLg) {
      return this._heightLg;
    }

    return this.value.heightLg;
  }

  get value() {
    return this.args.value?.data ?? {};
  }

  get options() {
    if (this._options) {
      return this._options;
    }

    return this.value.options ?? [];
  }

  get containerOptions() {
    if (this._containerOptions) {
      return this._containerOptions;
    }

    return this.value.containerOptions ?? [];
  }

  triggerChange() {
    return this.args.onChange?.({
      ids: this.records.map((a) => a.id || a._id),
      model: 'picture',
      options: this.options,
      containerOptions: this.containerOptions,
      heightSm: this.heightSm,
      heightMd: this.heightMd,
      heightLg: this.heightLg,
    });
  }

  get recordType() {
    return 'picture';
  }

  @action
  changeHeight(size, value) {
    this[`_height${size}`] = value;
    return this.triggerChange();
  }

  @action
  createImage() {
    return this.store.createRecord('picture', {
      name: '',
      picture: '',
      meta: new PictureMeta(),
    });
  }

  @action
  changeOptions(options) {
    this._options = options;
    this.triggerChange();
  }

  @action
  changeContainerOptions(options) {
    this._containerOptions = options;
    this.triggerChange();
  }

  @action
  change(value) {
    this._records = value;

    if (!this.hasData) {
      return this.args.onChange?.(value);
    }

    return this.triggerChange();
  }
}
