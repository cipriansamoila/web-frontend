import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManageEditorsArticleComponent extends Component {
  @service store;
  @tracked _allArticles = [];

  @action
  async setup() {
    this._allArticles = await this.store.findAll('article');
  }

  get allArticles() {
    return this._allArticles.map((a) => ({
      id: a.id,
      slug: a.slug,
      name: a.title,
    }));
  }

  get selectedArticle() {
    return this.allArticles.find((a) => {
      return (
        a.id == this.args.value?.data?.id || a.slug == this.args.value?.data?.id
      );
    });
  }

  @action
  itemChanged(value) {
    this.args.onChange?.({ id: value?.id, model: 'article' });
  }
}
