import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class ManageEditorsMapNameListComponent extends Component {
  @service store;
  _value = null;
  @tracked list;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  @action
  async loadMaps() {
    const result = await this.store.query('map', { edit: true });
    this.list = result.filter(
      (a) => this.args.value.map((b) => b.id).indexOf(a.id) == -1
    );

    this.list.addObjects(this.args.value);
  }

  @action
  change(isEnabled, value) {
    this._value = value;
    return this.args.onChange?.(value);
  }
}
