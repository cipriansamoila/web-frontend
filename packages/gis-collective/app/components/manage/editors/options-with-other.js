import Component from '../../attributes/value-options-other';

export default class ManageEditorsOptionsWithOtherComponent extends Component {
  get optionsList() {
    return this.args.options?.values ?? [];
  }

  get options() {
    return super.options.split(',');
  }
}
