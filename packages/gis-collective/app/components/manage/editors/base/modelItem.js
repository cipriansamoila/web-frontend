import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';

export default class ManageEditorsBaseModelItemComponent extends Component {
  elementId = `manage-editors-${guidFor(this)}`;

  @service store;

  @tracked _records;
  @tracked allItems;

  @action
  itemChanged(value) {
    return this.args.onChange?.({ id: value.id, model: 'map' });
  }

  get selectedItem() {
    return this.allItems?.find((a) => a.id == this.args.value?.data?.id);
  }

  @action
  async setup() {
    this.allItems = await this.store.query(this.recordType, {});
  }
}
