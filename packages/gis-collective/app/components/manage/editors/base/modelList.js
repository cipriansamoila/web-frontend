import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';

export default class ManageEditorsBaseModelListComponent extends Component {
  elementId = `manage-editors-banner-${guidFor(this)}`;

  @service store;
  @service notifications;

  @tracked _records;

  get hasData() {
    return Boolean(this.args.value?.data);
  }

  get records() {
    if (this._records) {
      return this._records;
    }

    if (this.args.value?.toArray) {
      return this.args.value.toArray();
    }

    if (Array.isArray(this.args.value?.data?.records)) {
      return this.args.value.data.records;
    }

    if (Array.isArray(this.args.value)) {
      return this.args.value;
    }

    return [];
  }

  @action
  async update(value) {
    let error;

    for (let model of value) {
      if (model.isNew) {
        try {
          await model.save();
        } catch (err) {
          error = true;
          this.notifications.handleError(err);
        }
      }
    }

    if (!error) {
      this._records = value;
    }

    if (!this.hasData) {
      return this.args.onChange?.(this.records);
    }

    this.args.onChange?.({
      ...this.args.value.data,
      ids: this.records.map((a) => a.id),
    });
  }

  @action
  async setup() {
    if (this.args.value?.data?.ids && !this.args.value?.data?.records) {
      this._records = this.args.value?.data?.ids
        .map(
          (id) =>
            this.store.peekRecord(this.recordType, id) ||
            this.store.findRecord(this.recordType, id)
        )
        .filter((a) => Boolean(a));
    }
  }
}
