import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { debounce } from '@ember/runloop';
import { tracked } from '@glimmer/tracking';

export default class ManageEditorsUserComponent extends Component {
  @service store;
  @tracked _user = null;

  get user() {
    if (this._user) {
      return this._user;
    }

    if (!this._user && typeof this.args.value == 'string') {
      const user = this.store.peekRecord('user', this.args.value);

      if (user) {
        return user;
      }
    }

    if (typeof this.args.value == 'object') {
      return this.args.value;
    }

    if (this.args.value.indexOf('@') != -1) {
      return {};
    }

    this.store
      .findRecord('user', this.args.value)
      .then((result) => {
        this._user = result;
      })
      .catch(() => {
        this._user = {
          name: '@unknown',
        };
      });

    return {};
  }

  @action
  change(value) {
    this._user = value;
    this.args.onChange?.(value.id);
  }

  performUserSearch(term, resolve, reject) {
    if (!term || term.length <= 3) {
      return reject('Term is too short');
    }

    this.store
      .query('user', { term: term })
      .then((data) => {
        resolve(data);
      })
      .catch(reject);
  }

  @action
  searchUsers(term) {
    return new Promise((resolve, reject) => {
      debounce(this, this.performUserSearch, term, resolve, reject, 600);
    });
  }
}
