import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class ManageEditorsFeatureVisibilityComponent extends Component {
  @service intl;
  @tracked _value;

  get values() {
    return [
      { id: '0', name: this.intl.t('visibility-private') },
      { id: '1', name: this.intl.t('visibility-public') },
      { id: '-1', name: this.intl.t('visibility-pending') },
    ];
  }

  get value() {
    return `${this._value || this.args.value || 0}`;
  }

  @action
  change(value) {
    this.args.onChange?.(parseInt(value.id));
  }
}
