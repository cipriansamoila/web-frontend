import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
  notifications: service(),
  intl: service(),

  classNames: ['btn', 'btn-delete'],
  tagName: 'button',

  classNameBindings: ['isDeleting::btn-danger'],
  attributeBindings: ['isDeleting:disabled'],

  deleteOptions: null,
  isDeleting: false,

  answers: computed('deleteOptions', function () {
    let deleteOptions = this.deleteOptions || 'yes:' + this.intl.t('yes');

    return deleteOptions.split(',').map((a) => {
      const pieces = a.split(':');

      return { text: pieces[1], key: pieces[0], class: 'btn-danger' };
    });
  }),

  click() {
    this.set('isDeleting', false);

    this.notifications
      .ask({
        title: this.intl.t(`delete ${this.modelName}`, { size: 1 }),
        question: this.intl.t('delete-confirmation-message', {
          name: this.name,
          size: 1,
        }),
        answers: this.answers,
      })
      .then((response) => {
        this.set('isDeleting', true);

        return this.onDelete(response).then(() => {
          if (!this.isDestroying) this.set('isDeleting', false);
          return true;
        });
      })
      .catch(() => {
        if (!this.isDestroying) this.set('isDeleting', false);
      });
  },
});
