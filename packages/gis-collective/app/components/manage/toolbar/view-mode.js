import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class ManageToolbarViewModeComponent extends Component {
  get default() {
    return this.args.default || 'card-columns';
  }

  get value() {
    return this.args.value || this.default;
  }

  @action
  select(value) {
    if (this.args.onChange) {
      this.args.onChange(value);
    }
  }
}
