import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';
import { Offcanvas } from 'bootstrap';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class ManagePageColSheetComponent extends Component {
  @service pageCols;
  @tracked newValue;
  @tracked newType;
  @tracked isVisible;

  elementId = `sheet-${guidFor(this)}`;

  get type() {
    if (this.newType === null) {
      return false;
    }

    let type = this.newType || this.args.type;

    if (this.pageCols.availableTypes.indexOf(type) == -1) {
      // eslint-disable-next-line no-console
      console.log(`The type "${type}" is an invalid editor type.`);
      return false;
    }

    return type;
  }

  get hasUpdate() {
    return (
      typeof this.newValue != 'undefined' ||
      this.newType ||
      this.newType === null
    );
  }

  @action
  typeChanged(value) {
    this.newType = value;
  }

  @action
  dataChanged(value) {
    this.newValue = value;
  }

  @action
  close() {
    this.isVisible = false;
    this.newValue = undefined;
    this.newType = undefined;
    this.offcanvas.hide();
  }

  @action
  setup(element) {
    this.offcanvas = new Offcanvas(element);

    element.addEventListener('hidden.bs.offcanvas', () => {
      this.args.onHide?.();
    });

    this.setStates();
  }

  @action
  setStates() {
    if (this.args.show) {
      this.newType = undefined;
      this.newValue = undefined;
      this.offcanvas.show();
      this.isVisible = true;
    }

    if (!this.args.show && this.isVisible) {
      this.close();
    }
  }

  @action
  async save() {
    await this.args.onChange(this.newValue, this.type);
    this.close();
  }
}
