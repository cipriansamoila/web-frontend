import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { debounce } from '@ember/runloop';

export default class ManageToggleComponent extends Component {
  @service intl;

  checkbox = null;

  get isSaving() {
    return this.args.title && this.args.title == this.args.savingField;
  }

  get className() {
    if (!this.args.title || !this.args.title) {
      return 'unknown';
    }

    return this.args.title.replace(/[^0-9a-z]/gi, '-').toLowerCase();
  }

  @action
  setupCheckbox(element) {
    if (this.args.value) {
      element.setAttribute('checked', 'true');
    }

    this.checkbox = element;
  }

  saveState() {
    this.args.onSave(this.checkbox.checked);
  }

  @action
  switchChanged() {
    debounce(this, this.saveState, 200);
  }
}
