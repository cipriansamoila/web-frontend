import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { all } from 'rsvp';

export default class ManageListComponent extends Component {
  @service intl;
  @service notifications;

  @tracked selectedItems;

  get answers() {
    const deleteOptions =
      this.args.deleteOptions || 'yes:' + this.intl.t('yes');

    return deleteOptions.split(',').map((a) => {
      const pieces = a.split(':');
      return { text: pieces[1], key: pieces[0], class: 'btn-danger' };
    });
  }

  @action delete(item) {
    return this.notifications
      .ask({
        title: this.intl.t(`delete ${item.constructor.modelName}`, { size: 1 }),
        question: this.intl.t('delete-confirmation-message', {
          name: item.name,
          size: 1,
        }),
        answers: this.answers,
      })
      .then((response) => {
        return this.args.onDelete(item, response);
      })
      .catch((err) => {
        if (err) {
          // eslint-disable-next-line no-console
          console.error(err);
        }
      });
  }

  @action deleteMany(itemsId) {
    const items = [];

    this.args.columns.forEach((list) => {
      list.forEach((item) => {
        if (itemsId.indexOf(item.id) != -1) {
          items.push(item);
        }
      });
    });

    if (items.length == 0) {
      return;
    }

    if (items.length == 1) {
      return this.delete(items[0]);
    }

    return this.notifications
      .ask({
        title: this.intl.t(`delete ${items[0].constructor.modelName}`, {
          size: items.length,
        }),
        question: this.intl.t('delete-confirmation-message', {
          size: items.length,
        }),
        answers: this.answers,
      })
      .then((response) => {
        return all(items.map((item) => this.args.onDelete(item, response)));
      })
      .catch(() => {});
  }

  @action select(items) {
    this.selectedItems = items;

    if (this.args.onSelect) {
      return this.args.onSelect(items);
    }
  }

  @action clearSelection() {
    this.selectedItems = [];
  }
}
