import Component from '@glimmer/component';
import { action } from '@ember/object';
import { later } from '@ember/runloop';
import { inject as service } from '@ember/service';

export default class ManageSideBarGroupComponent extends Component {
  @service clickOutside;

  get canEdit() {
    if (this.args.canEdit === undefined) {
      return true;
    }

    return this.args.canEdit;
  }

  get isEditMode() {
    return this.args.editablePanel == this.args.title;
  }

  get isSaving() {
    return this.args.savingPanel == this.args.title;
  }

  get niceClass() {
    if (!this.args.title) {
      return 'unknown';
    }

    return this.args.title.toLowerCase().replace(/\s/gi, '-');
  }

  @action
  handleClickOutside() {
    if (!this.isEditMode) {
      return;
    }

    return this.args.onClickOutside?.();
  }

  @action
  enableEdit() {
    this.args.onEdit?.(this.args.title);

    later(() => {
      this.element.querySelector('select,input,textarea,button').focus();
    }, 50);

    this.clickOutside.subscribe(this.element, () => {
      this.handleClickOutside();
    });
  }

  @action
  setup(element) {
    this.element = element;
  }
}
