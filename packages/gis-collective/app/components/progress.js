import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';

export default class ProgressComponent extends Component {
  getWidth(value) {
    const percentage = parseInt(
      (parseFloat(value) / parseFloat(this.args.max)) * 100
    );

    return htmlSafe(`width: ${percentage}%`);
  }

  get primaryStyle() {
    return this.getWidth(this.args.primary);
  }

  get warningStyle() {
    return this.getWidth(this.args.warning);
  }

  get successStyle() {
    return this.getWidth(this.args.success);
  }

  get infoStyle() {
    return this.getWidth(this.args.info);
  }

  get dangerStyle() {
    return this.getWidth(this.args.danger);
  }
}
