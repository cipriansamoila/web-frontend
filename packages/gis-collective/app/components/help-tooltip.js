import Component from '@glimmer/component';
import { action } from '@ember/object';
import { Popover } from 'bootstrap';

export default class HelpTooltipComponent extends Component {
  @action
  setup(element) {
    const content = element.querySelector('.content');

    this.popover = new Popover(element, {
      animation: true,
      placement: 'top',
      trigger: 'hover',
      html: true,
      content: content.innerHTML,
    });
  }
}
