import Component from '@glimmer/component';
import create360Viewer from '360-image-viewer';
import canvasFit from 'canvas-fit';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ImageSphereComponent extends Component {
  @tracked isLoading;

  @action
  create360Viewer(element) {
    const picture = this.args.picture;

    const image = new Image();
    const resizedImage = new Image();
    image.crossOrigin = 'Anonymous';
    image.src = picture.picture;
    this.isLoading = true;

    image.onload = () => {
      try {
        const viewer = create360Viewer({
          canvas: element,
          fov: 90 * (Math.PI / 180),
          rotateSpeed: -0.15,
          damping: 0.3,
        });

        const w = image.naturalWidth;
        const h = image.naturalHeight;

        const canvas = document.createElement('canvas');
        document.querySelector('body').appendChild(canvas);

        canvas.width = 4000;
        canvas.height = 4000;
        const ctx = canvas.getContext('2d');
        ctx.drawImage(image, 0, 0, w, h, 0, 0, canvas.width, canvas.height);

        resizedImage.src = canvas.toDataURL('image/jpeg', 1);

        resizedImage.onload = () => {
          viewer.texture(resizedImage);

          if (this.args.fitParent) {
            this.fit = canvasFit(
              viewer.canvas,
              element.parentElement,
              window.devicePixelRatio
            );
            window.addEventListener('resize', this.fit, false);
            this.fit();
          } else {
            viewer.canvas.width = 2000;
            viewer.canvas.height = 1500;
          }

          viewer.start();
          this.isLoading = false;
        };
      } catch (err) {
        alert(err);
      }
    };
  }
}
