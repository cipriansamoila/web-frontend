import Component from '@ember/component';
import { htmlSafe } from '@ember/template';
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ['d-inline-block', 'position-relative', 'picture-progress'],
  classNameBindings: ['isUploading:uploading'],

  isUploading: computed('picture.progress', function () {
    return this.picture.progress > 0 && this.picture.progress < 100;
  }),

  leftStyle: computed('picture.progress', function () {
    const value =
      this.picture.progress < 50
        ? 0
        : parseInt((this.picture.progress / 100) * 360) - 180;

    return htmlSafe(`transform: rotate(${value}deg)`);
  }),

  rightStyle: computed('picture.progress', function () {
    const value =
      this.picture.progress > 50
        ? 180
        : parseInt((this.picture.progress / 100) * 360);

    return htmlSafe(`transform: rotate(${value}deg)`);
  }),
});
