import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  isSet: computed('value.toDateString', function () {
    if (!this.value || !this.value.toDateString) {
      return false;
    }

    if (this.value.getFullYear() <= 1) {
      return false;
    }

    return true;
  }),

  text: computed('isSet', 'value', function () {
    if (!this.isSet) {
      return '';
    }

    return this.value.toDateString();
  }),
});
