import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';
import { later } from '@ember/runloop';

export default class FullscreenComponent extends Component {
  @tracked hasError;
  @tracked isFullscreen;

  @service fastboot;

  @action
  watchFullscreen() {
    this._fullscreenChange = () => {
      this.isFullscreen =
        document.fullscreen || document.fullscreenElement !== null;

      if (this.args.onFullScreen) {
        this.args.onFullScreen(this.isFullscreen);
      }
    };

    if (!this.fastboot.isFastBoot) {
      document.addEventListener(
        'fullscreenchange',
        this._fullscreenChange,
        false
      );
      document.addEventListener(
        'mozfullscreenchange',
        this._fullscreenChange,
        false
      );
      document.addEventListener(
        'MSFullscreenChange',
        this._fullscreenChange,
        false
      );
      document.addEventListener(
        'webkitfullscreenchange',
        this._fullscreenChange,
        false
      );
    }
  }

  willDestroy() {
    super.willDestroy(...arguments);
    if (!this.fastboot.isFastBoot) {
      document.removeEventListener(
        'fullscreenchange',
        this._fullscreenChange,
        false
      );
      document.removeEventListener(
        'mozfullscreenchange',
        this._fullscreenChange,
        false
      );
      document.removeEventListener(
        'MSFullscreenChange',
        this._fullscreenChange,
        false
      );
      document.removeEventListener(
        'webkitfullscreenchange',
        this._fullscreenChange,
        false
      );
    }
  }

  @action
  setup(element) {
    this.element = element;
    this.watchFullscreen();
  }

  @action
  updateState() {
    this.fullscreen(this.args.isFullscreen);
  }

  @action
  exitFullscreen() {
    return document.exitFullscreen();
  }

  @action
  requestFullscreen() {
    const elem = this.element;

    if (elem.requestFullscreen) {
      return elem.requestFullscreen();
    }
  }

  @action
  async fullscreen(value) {
    if (this.isFullscreen == value) {
      return;
    }

    later(async () => {
      this.isFullscreen = value;

      try {
        if (value) {
          await this.requestFullscreen();
        } else {
          await this.exitFullscreen();
        }
      } catch (err) {
        // eslint-disable-next-line no-console
        console.log(err);

        if (this.args.onFullScreen) {
          this.args.onFullScreen(this.isFullscreen);
        }
      }
    });
  }
}
