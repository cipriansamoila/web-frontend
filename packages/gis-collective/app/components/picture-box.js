import Component from '@ember/component';
import { htmlSafe } from '@ember/template';
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ['picture-box'],

  image: computed('src', function () {
    if (!this.src) {
      return htmlSafe('');
    }

    return htmlSafe("background-image: url('" + this.src + "')");
  }),
});
