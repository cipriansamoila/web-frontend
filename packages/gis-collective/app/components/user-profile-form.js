import Component from '@glimmer/component';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { Location } from '../transforms/location';

export default class UserProfileFormComponent extends Component {
  elementId = 'user-profile-' + guidFor(this);

  get name() {
    return {
      salutation: this.args.profile?.salutation,
      title: this.args.profile?.title,
      firstName: this.args.profile?.firstName,
      lastName: this.args.profile?.lastName,
    };
  }

  get simpleLocation() {
    return this.args.profile?.location?.simple ?? '';
  }

  set simpleLocation(value) {
    this.args.profile.location = new Location(value);
  }

  @action
  changeBio(value) {
    this.args.profile.bio = value;
  }

  @action
  handleNameChange(value) {
    this.args.profile.title = value.title;
    this.args.profile.salutation = value.salutation;
    this.args.profile.firstName = value.firstName;
    this.args.profile.lastName = value.lastName;
  }

  @action
  changeDetailedLocation(value) {
    this.args.profile.location = new Location({
      isDetailed: true,
      detailedLocation: value,
    });
  }

  @action
  update() {
    if (!this.args.onUpdate) {
      return;
    }

    return this.args.onUpdate();
  }
}
