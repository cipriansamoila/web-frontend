import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import config from '../config/environment';

export default class MainBarComponent extends Component {
  @service loading;
  @service session;
  @service user;
  @service position;
  @service fastboot;
  @tracked showingOptions;

  apiUrl = config.apiUrl;
  serviceDesk = config.serviceDesk;

  get selectedLocale() {
    if (this.args.selectedLocale) {
      return this.args.selectedLocale;
    }

    return 'en-us';
  }

  get currentAction() {
    return this.args.currentAction || '';
  }

  get allowProposingSites() {
    return (
      this.args.allowProposingSites !== 'false' &&
      this.args.allowProposingSites !== false
    );
  }

  get allowManageWithoutTeams() {
    return (
      this.args.allowManageWithoutTeams !== 'false' &&
      this.args.allowManageWithoutTeams !== false
    );
  }

  get allowManage() {
    if (!this.session.isAuthenticated) {
      return false;
    }

    if (this.args.teams?.length || this.user?.isAdmin) {
      return true;
    }

    return false;
  }

  get isPublisher() {
    return this.user?.isAdmin || this.args.teams?.find((a) => a.isPublisher);
  }

  get allowAdd() {
    if (this.allowManage) {
      return true;
    }

    return this.session.isAuthenticated && this.allowManageWithoutTeams;
  }

  get isMapPage() {
    if (this.currentAction == 'index') {
      return true;
    }

    if (this.currentAction == 'browse.maps.map-view') {
      return true;
    }

    return false;
  }

  get isBrowsing() {
    if (this.args.currentURL?.indexOf('/browse/maps/_') == 0) {
      return false;
    }

    return this.currentAction.indexOf('browse') === 0;
  }

  get isIndex() {
    return this.currentAction === 'index';
  }

  get isCampaign() {
    return (
      this.currentAction.indexOf('campaigns.') == 0 &&
      this.currentAction.indexOf('.add') == -1
    );
  }

  get isManage() {
    return (
      this.currentAction.indexOf('manage.') == 0 &&
      this.currentAction.indexOf('.add') == -1
    );
  }

  get isAdd() {
    return (
      this.currentAction.indexOf('manage.') == 0 &&
      this.currentAction.indexOf('.add') != -1
    );
  }

  get isIconsAll() {
    return this.currentAction.indexOf('icons') === 0;
  }

  get isTeamsAll() {
    return this.currentAction.indexOf('teams') === 0;
  }

  get isMapsAll() {
    return this.currentAction.indexOf('maps') === 0;
  }

  get isContribute() {
    return this.isAddSite;
  }

  get isAddSite() {
    return this.currentAction.indexOf('add') === 0;
  }

  get isAbout() {
    return this.currentAction.indexOf('about') == 0;
  }

  get isHelp() {
    return this.currentAction.indexOf('help') == 0;
  }

  @action
  toggle() {
    this.showingOptions = !this.showingOptions;
  }

  @action
  close(element) {
    const isLink = element.target?.attributes?.getNamedItem?.('href');

    if (isLink) {
      this.showingOptions = false;
    }
  }

  @action
  invalidateSession() {
    return this.session.invalidate();
  }

  @action
  signOutClick() {
    return this.args.onSignOut();
  }
}
