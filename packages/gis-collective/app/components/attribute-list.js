import Component from '@ember/component';

export default Component.extend({
  actions: {
    editValue() {
      if (!this.editValue) {
        alert('editValue is not set');
      }

      this.editValue(...arguments);
    },
    editSection() {
      if (!this.editSection) {
        alert('editSection is not set');
      }

      this.editSection(...arguments);
    },

    addSection() {
      if (!this.addSection) {
        alert('addSection is not set');
      }

      this.addSection(...arguments);
    },

    onLocalize() {
      if (!this.onLocalize) {
        alert('onLocalize is not set');
      }
      this.onLocalize(...arguments);
    },
  },
});
