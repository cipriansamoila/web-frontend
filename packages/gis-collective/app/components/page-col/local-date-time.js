import Component from '@glimmer/component';

export default class PageColLocalDateTimeComponent extends Component {
  get value() {
    if (
      typeof this.args.value == 'number' ||
      typeof this.args.value == 'string'
    ) {
      return this.args.value;
    }

    if (!this.args.value || typeof this.args.value?.getMonth != 'function') {
      return null;
    }

    return this.args.value;
  }
}
