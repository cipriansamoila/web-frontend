import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class PageColIconsComponent extends Component {
  @service store;
  @tracked _records;

  get gutter() {
    return this.args.value?.data?.gutter ?? '1';
  }

  get rowColSize() {
    return this.args.value?.data?.rowColSize ?? '0';
  }

  get size() {
    return this.args.value?.data?.viewMode == 'medium' ? 'lg' : 'sm';
  }

  get showLinks() {
    return this.args.value?.data?.links == 'enabled';
  }

  get showCards() {
    return this.args.value?.data?.viewMode == 'cards';
  }

  get records() {
    if (this._records) {
      return this._records;
    }

    if (Array.isArray(this.args.value?.data?.records)) {
      return this.args.value.data.records;
    }

    if (Array.isArray(this.args.value)) {
      return this.args.value;
    }

    return [];
  }

  @action
  setup() {
    if (this.args.value?.data?.ids) {
      this._records = this.args.value?.data?.ids
        .map(
          (id) =>
            this.store.peekRecord('icon', id) ||
            this.store.findRecord('icon', id)
        )
        .filter((a) => Boolean(a));
    }
  }
}
