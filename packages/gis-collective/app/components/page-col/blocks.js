import Component from '@glimmer/component';
import { toContentBlocks } from '../../lib/content-blocks';

export default class BlocksComponent extends Component {
  get cls() {
    let result = [];

    if (this.args.value?.data?.alignment) {
      result.push(`text-${this.args.value?.data?.alignment}`);
    }

    if (this.args.value?.data?.lineHeight) {
      result.push(`lh-${this.args.value?.data?.lineHeight}`);
    }

    if (this.args.value?.data?.font) {
      result.push(`font-${this.args.value?.data?.font}`);
    }

    return result.join(' ');
  }

  get value() {
    if (this.args.value?.data) {
      return this.args.value?.data;
    }

    if (typeof this.args.value == 'string') {
      return toContentBlocks(this.args.value);
    }

    return this.args.value;
  }
}
