import Component from './base-list';
import { guidFor } from '@ember/object/internals';

export default class PageColPictureListComponent extends Component {
  elementId = `page-col-picture-list-${guidFor(this)}`;

  get recordType() {
    return 'picture';
  }

  get options() {
    return this.args.value?.data?.options?.join(' ');
  }

  get containerOptions() {
    return this.args.value?.data?.containerOptions?.join(' ');
  }

  get properties() {
    const result = {};

    if (this.args.value?.data?.heightSm) {
      result.sm = {
        height: this.args.value?.data?.heightSm + 'px',
      };
    }

    if (this.args.value?.data?.heightMd) {
      result.md = {
        height: this.args.value?.data?.heightMd + 'px',
      };
    }

    if (this.args.value?.data?.heightLg) {
      result.lg = {
        height: this.args.value.data?.heightLg + 'px',
      };
    }

    return result;
  }
}
