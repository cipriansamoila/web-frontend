import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { Collapse } from 'bootstrap';
import { tracked } from '@glimmer/tracking';
import { htmlSafe } from '@ember/template';
import sanitizeHtml from 'sanitize-html';

export default class PageColTrayComponent extends Component {
  @service router;
  @tracked isOpen = false;

  get link() {
    return this.args.value?.data?.link ?? '';
  }

  get linkTitle() {
    const data = this.args.value?.data ?? {};
    return data['link-title'] ?? '';
  }

  get hasExternalLink() {
    if (!this.link) {
      return false;
    }

    return this.link.indexOf('http') == 0;
  }

  get hasInternalLink() {
    if (!this.link) {
      return false;
    }

    try {
      return this.router.recognize(this.link);
    } catch {
      return false;
    }
  }

  get title() {
    return htmlSafe(sanitizeHtml(this.args.value?.data?.title ?? ''));
  }

  get paragraph() {
    return htmlSafe(sanitizeHtml(this.args.value?.data?.paragraph ?? ''));
  }

  @action
  setupCollapse(element) {
    const collapseElement = element.querySelector('.collapse');

    this.collapse = new Collapse(collapseElement, {
      toggle: false,
    });
  }

  @action
  toggle() {
    this.isOpen = !this.isOpen;
    this.collapse?.toggle();
  }
}
