import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class BaseListComponent extends Component {
  @service store;
  @tracked _records;

  get records() {
    if (this._records) {
      return this._records;
    }

    if (Array.isArray(this.args.value?.data?.records)) {
      return this.args.value.data.records;
    }

    if (Array.isArray(this.args.value)) {
      return this.args.value;
    }

    return [];
  }

  get size() {
    return 'lg';
  }

  @action
  setup() {
    if (this.args.value?.data?.ids) {
      this._records = this.args.value?.data?.ids
        .map(
          (id) =>
            this.store.peekRecord(this.recordType, id) ||
            this.store.findRecord(this.recordType, id)
        )
        .filter((a) => Boolean(a));
    }
  }
}
