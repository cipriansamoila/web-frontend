import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class PageColArticleComponent extends Component {
  @service store;
  @tracked _article;

  get article() {
    if (this._article) {
      return this._article;
    }

    return this.args.value?.record;
  }

  @action
  setupArticle() {
    if (!this.args.value?.data?.id) {
      return null;
    }

    if (this.args.value.record) {
      this._article = this.args.value.record;
      return;
    }

    this._article = this.store.peekRecord('article', this.args.value.data.id);

    if (!this._article) {
      this._article = this.store.findRecord('article', this.args.value.data.id);
    }
  }
}
