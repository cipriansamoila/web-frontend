import BasePageColListComponent from './base/modelList';

export default class PageColCampaignsComponent extends BasePageColListComponent {
  modelName = 'campaign';
}
