import BasePageColListComponent from './base/modelList';

export default class PageColMapsComponent extends BasePageColListComponent {
  modelName = 'map';
}
