import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class PageColCampaignsPreviewComponent extends Component {
  @service store;
  @service intl;
  @tracked team;

  @action
  async setup() {
    if (this.args.value?.data?.query?.team) {
      try {
        this.team = await this.store.findRecord(
          'team',
          this.args.value.data.query.team
        );
      } catch {
        this.team = '';
      }
    }
  }

  get name() {
    if (this.team?.name) {
      return this.team.name;
    }

    return this.intl.t('all teams');
  }
}
