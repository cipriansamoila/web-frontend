import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class PageColProfileLinkComponent extends Component {
  @service store;
  @tracked profile;
  @tracked isLoaded;

  get hasAt() {
    if (typeof this.id != 'string') {
      return false;
    }

    return this.id.indexOf('@') != -1;
  }

  get fullName() {
    if (!this.args.value) {
      return null;
    }

    if (this.profile?.fullName) {
      return this.profile.fullName;
    }

    if (typeof this.args.value == 'object' && this.args.value.fullName) {
      return this.args.value.fullName;
    }

    return null;
  }

  get id() {
    if (this.isLoaded && this.profile?.id) {
      return this.profile.id;
    }

    if (!this.args.value) {
      return null;
    }

    if (typeof this.args.value == 'object' && this.args.value.id) {
      return this.args.value.id;
    }

    if (typeof this.args.value == 'string') {
      return this.args.value;
    }

    return null;
  }

  @action
  async setupProfile() {
    if (typeof this.args.value != 'string' || this.hasAt) {
      this.isLoaded = true;
      return;
    }

    if (!this.args.value || this.args.value == '') {
      this.isLoaded = true;
      return;
    }

    this.profile = this.store.peekRecord('user-profile', this.args.value);

    if (!this.profile) {
      try {
        this.profile = await this.store.findRecord(
          'user-profile',
          this.args.value
        );
        this.isLoaded = true;
      } catch {
        this.profile = null;
      }
    } else {
      this.isLoaded = true;
    }
  }
}
