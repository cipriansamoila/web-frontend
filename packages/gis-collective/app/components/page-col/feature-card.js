import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

export default class PageColFeatureCardComponent extends Component {
  @service store;

  get feature() {
    return this.args.value;
  }

  get options() {
    return this.args.options ?? {};
  }
}
