import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { getOwner } from '@ember/application';

export default class PageColViewComponent extends Component {
  @service registry;

  get hasType() {
    return (this.args.type || this.args.value?.type) && this.type;
  }

  get type() {
    const type = this.args.type || this.args.value?.type || 'text';

    let applicationInstance = getOwner(this);
    if (
      !applicationInstance.resolveRegistration(`component:page-col/${type}`)
    ) {
      return null;
    }

    return type;
  }
}
