import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class BasePageColListComponent extends Component {
  @service store;

  get buckets() {
    const key = this.args.value.modelKey;

    if (!this.args.model) {
      return null;
    }

    return this.args.model[key];
  }

  @action
  deleteRecord(record) {
    return record.destroyRecord();
  }
}
