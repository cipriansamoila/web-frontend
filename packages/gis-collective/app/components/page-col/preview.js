import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { getOwner } from '@ember/application';

export default class PageColPreviewComponent extends Component {
  @service store;

  get component() {
    return `page-col/${this.args.value.type}`;
  }

  get componentPreview() {
    return `page-col/${this.args.value.type}-preview`;
  }

  get hasPreview() {
    const owner = getOwner(this);
    const lookup = owner.lookup('component-lookup:main');

    if (!lookup.componentFor) {
      return !!lookup.lookupFactory(this.componentPreview);
    }

    return !!(
      lookup.componentFor(this.componentPreview, owner) ||
      lookup.layoutFor(this.componentPreview, owner)
    );
  }

  get hasComponent() {
    const owner = getOwner(this);
    const lookup = owner.lookup('component-lookup:main');

    if (!lookup.componentFor) {
      return !!lookup.lookupFactory(this.component);
    }

    return !!(
      lookup.componentFor(this.component, owner) ||
      lookup.layoutFor(this.component, owner)
    );
  }
}
