import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class PageColBannerComponent extends Component {
  @service store;
  @service router;
  @service fastboot;
  @tracked headingPicture;
  @tracked sidePicture;

  get isInternalLink() {
    if (!this.args.value?.data?.buttonLink) {
      return false;
    }

    if (this.args.value?.data?.buttonLink?.substr(1) != '/') {
      return false;
    }

    return (
      !this.fastboot.isFastBoot &&
      this.router.recognize(this.args.value?.data?.buttonLink)
    );
  }

  get buttonLink() {
    if (this.isInternalLink) {
      return this.args.value?.data?.buttonLink?.substr(1);
    }

    return this.args.value?.data?.buttonLink ?? '';
  }

  @action
  transitionToLink(event) {
    event.preventDefault();
    this.router.transitionTo(this.args.value?.data?.buttonLink);

    return false;
  }

  @action
  async setupPictures() {
    if (this.args.value?.data?.headingPicture) {
      this.headingPicture = await this.store.findRecord(
        'picture',
        this.args.value?.data?.headingPicture
      );
    }

    if (this.args.value?.data?.sidePicture) {
      this.sidePicture = await this.store.findRecord(
        'picture',
        this.args.value?.data?.sidePicture
      );
    }
  }

  get headingPictureStyle() {
    if (!this.headingPicture) {
      return null;
    }

    const url = this.headingPicture.picture;

    if (!url) {
      return htmlSafe('');
    }

    return htmlSafe(`background-image: url(${url})`);
  }

  get sidePictureStyle() {
    if (!this.sidePicture) {
      return null;
    }

    const url = this.sidePicture.picture;

    if (!url) {
      return htmlSafe('');
    }

    return htmlSafe(`background-image: url(${url})`);
  }
}
