import Component from '@glimmer/component';
import { action } from '@ember/object';
import config from '../../../config/environment';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import Polygon from 'ol/geom/Polygon';
import { htmlSafe } from '@ember/template';
import { guidFor } from '@ember/object/internals';
import { later } from '@ember/runloop';
import { FeatureSelection } from '../../../lib/map/feature-selection';

export default class PageColMapMapViewComponent extends Component {
  elementId = `map-view-${guidFor(this)}`;

  @service mapStyles;
  @service position;
  @service router;

  @tracked _selectedBaseMap;
  @tracked isHover;
  @tracked hoveredFeature;
  @tracked pendingHoveredFeature;
  @tracked isTransitioning;
  @tracked style;
  @tracked extent;
  @tracked olMap;

  @tracked isSpotlight;
  @tracked featureSelection;

  featureTilesUrl = config.siteTilesUrl;

  constructor() {
    super(...arguments);

    this.featureSelection = new FeatureSelection();
  }

  get extentM() {
    const polygon = new Polygon([this.map.area.coordinates[0]]);
    return polygon.getExtent();
  }

  get map() {
    const key = this.args.value?.modelKey;

    if (!this.args.model || !key) {
      return null;
    }

    return this.args.model[key];
  }

  get mapId() {
    return this.map.id || this.map._id || '_';
  }

  get selectedBaseMap() {
    if (this._selectedBaseMap) {
      return this._selectedBaseMap;
    }

    return this.map?.baseMaps?.list.firstObject ?? null;
  }

  get baseMaps() {
    return this.map?.baseMaps?.list;
  }

  get attributions() {
    const list = [];

    if (this.map?.license?.name) {
      list.push(this.map.license);
    }

    if (this.selectedBaseMap?.attributions?.length) {
      list.addObjects(this.selectedBaseMap?.attributions);
    }

    return list;
  }

  get shouldTransition() {
    return this.isSpotlight || this.featureSelection.featureId;
  }

  redraw() {
    this.olMap.getLayers().forEach((layer) => {
      layer.changed();
    });
  }

  @action
  setupLayer(map) {
    this.olMap = map;
    this.redraw();
  }

  @action
  startTransition() {
    if (!this.rect) {
      this.rect = this.element.getBoundingClientRect();

      this.style = htmlSafe(
        `width: ${this.rect.width}px; height: ${this.rect.height}px; top: ${this.rect.top}px; left: ${this.rect.left}px; position: fixed;`
      );
    }

    later(() => {
      const currentRect = this.element.getBoundingClientRect();

      if (currentRect.x != this.rect.x || currentRect.y != this.rect.y) {
        this.startTransition();
        return;
      }

      this.isTransitioning = true;

      later(() => {
        this.style = null;
        this.rect = null;
      }, 20);
    }, 5);
  }

  @action
  transitionEnd() {
    if (!this.shouldTransition) {
      return;
    }

    later(() => {
      if (this.isSpotlight) {
        this.router.transitionTo('browse.maps.map-view.spotlight', this.mapId);
        return;
      }

      this.router.transitionTo(
        'browse.maps.map-view.feature',
        this.mapId,
        this.featureSelection.featureId,
        {
          queryParams: {
            viewbox: this.extent,
            hideLoading: true,
            baseMap: this.selectedBaseMap?.id,
          },
        }
      );
    }, 10);
  }

  @action
  setup(element) {
    this.element = element;

    this.mapStyles.watchChange(this.elementId, () => {
      this.redraw();
    });
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.mapStyles.unwatchChange(this.elementId);
  }

  @action
  localize() {
    return this.position.watchPosition();
  }

  @action
  handleExtentChange(value) {
    this.extent = value.join(',');
  }

  @action
  selectBaseMap(value) {
    this._selectedBaseMap = value;
  }

  @action
  showMapContextMenu() {}

  @action
  hideMapContextMenu() {}

  @action
  hoverFeature(feature) {
    this.isHover = !!feature;

    if (feature) {
      const properties = feature.getProperties();
      if (properties['canView'] !== 'true' && properties['visibility'] != '1') {
        this.isHover = false;
        feature = null;
      }
    }

    if (!this.isContextVisible) {
      this.hoveredFeature = feature;
      this.pendingHoveredFeature = false;
    } else {
      this.pendingHoveredFeature = feature;
    }
  }

  @action
  selectFeature(feature) {
    this.featureSelection.update(feature);

    if (this.featureSelection.type == 'feature') {
      this.startTransition();
    }
  }

  @action
  enableSpotlight() {
    this.isSpotlight = true;
    this.startTransition();
  }

  get tilesUrl() {
    let url = `${this.featureTilesUrl}/{z}/{x}/{y}?format=vt`;

    if (this.map) {
      url = `${url}&map=${this.map.id}`;
    }

    return url;
  }
}
