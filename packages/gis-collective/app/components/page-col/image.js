import Component from '@glimmer/component';
import sanitizeHtml from 'sanitize-html';

export default class PageColImageComponent extends Component {
  get data() {
    return this.args.value?.data ?? {};
  }

  get caption() {
    return sanitizeHtml(this.data.caption ?? '', {
      allowedTags: [],
      allowedAttributes: [],
    });
  }

  get src() {
    return this.data.file?.url;
  }
}
