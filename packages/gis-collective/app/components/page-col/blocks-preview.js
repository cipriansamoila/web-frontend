import Component from '@glimmer/component';
import { toContentBlocks, firstParagraph } from '../../lib/content-blocks';

export default class BlocksComponent extends Component {
  get blocks() {
    if (typeof this.args.value == 'string') {
      return toContentBlocks(this.args.value);
    }

    return this.args.value;
  }

  get value() {
    return firstParagraph(this.blocks ?? { blocks: [] });
  }
}
