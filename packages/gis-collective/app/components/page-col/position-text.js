import Component from '@glimmer/component';

export default class PageColPositionTextComponent extends Component {
  get icon() {
    if (!this.args.value?.type) {
      return null;
    }

    switch (this.args.value?.type) {
      case 'MultiPoint':
        return 'braille';

      case 'LineString':
      case 'MultiLineString':
        return 'wave-square';

      case 'Polygon':
      case 'MultiPolygon':
        return 'draw-polygon';

      default:
        return 'map-marker-alt';
    }
  }

  get flatCoordinates() {
    if (this.args.value?.type == 'Point') {
      return [this.args.value?.coordinates];
    }

    if (
      this.args.value?.type == 'MultiPoint' ||
      this.args.value?.type == 'LineString'
    ) {
      return this.args.value?.coordinates;
    }

    if (
      this.args.value?.type == 'MultiLineString' ||
      this.args.value?.type == 'Polygon'
    ) {
      return this.args.value?.coordinates.flatMap((a) => a);
    }

    if (this.args.value?.type == 'MultiPolygon') {
      return this.args.value?.coordinates.flatMap((a) => a.flatMap((b) => b));
    }

    return [];
  }

  get coordinates() {
    if (this.flatCoordinates.length == 0) {
      return '';
    }

    let point = this.flatCoordinates[0];

    if (!point?.length) {
      return '';
    }

    return `${point[0]}, ${point[1]}`;
  }

  get len() {
    if (this.flatCoordinates.length <= 1) {
      return '';
    }

    return this.flatCoordinates.length - 1;
  }
}
