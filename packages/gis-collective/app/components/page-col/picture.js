import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class PageColPictureComponent extends Component {
  @service store;
  @tracked _picture;

  get options() {
    const list = this.args.value?.data?.options ?? [];

    return list.join(' ');
  }

  get picture() {
    if (this._picture) {
      return this._picture;
    }

    return this.args.value?.data?.record;
  }

  @action
  setupPicture() {
    if (!this.args.value?.data?.id) {
      return null;
    }

    this._picture = this.store.findRecord('picture', this.args.value.data.id);
  }
}
