import Component from '@glimmer/component';

export default class LoadingGroupComponent extends Component {
  _height = 270;
  _width = 250;

  get scale() {
    return this.args.scale || 1;
  }

  get width() {
    return this._width * this.scale;
  }

  get height() {
    return this._height * this.scale;
  }
}
