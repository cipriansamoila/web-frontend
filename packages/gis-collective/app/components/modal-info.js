import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { Modal } from 'bootstrap';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ModalInfoComponent extends Component {
  @service modal;
  @service fastboot;

  @tracked resolved = null;
  @tracked args = null;

  @action
  setupModal(element) {
    if (this.fastboot.isFastBoot) {
      return;
    }
    this.element = element;
    this.modalInstance = new Modal(this.element, { show: false });

    this.modal.openModal = this.openModal;

    this.element.addEventListener('hidden.bs.modal', () => {
      this.modal.clear();
      this.modal.visible = false;
    });

    this.element.addEventListener('show.bs.modal', () => {
      this.resolved = null;
      this.args = null;

      this.modal.visible = true;
    });
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.modal.openModal = null;

    if (this.modalInstance?._element) {
      this.modalInstance.dispose();
    }
  }

  @action
  async openModal() {
    if (this.modalInstance?._element) {
      await this.modalInstance.show();
    }
  }

  @action
  async resolve() {
    if (this.modalInstance?._element) {
      await this.modalInstance.hide();
    }

    if (this.modal.password) {
      return this.modal.resolve(this.modal.password);
    }

    return this.modal.resolve(...arguments);
  }

  @action
  async reject() {
    if (this.modalInstance?._element) {
      await this.modalInstance.hide();
    }

    return this.modal?.reject?.();
  }
}
