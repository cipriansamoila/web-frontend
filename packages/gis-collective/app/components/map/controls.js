import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { htmlSafe } from '@ember/template';
import { later } from '@ember/runloop';
import { fromLonLat } from 'ol/proj';

export default class MapControlsComponent extends Component {
  @tracked localize = true;
  @tracked enableTooltips = false;

  @tracked localizationError = false;
  @tracked localizationWaiting = false;
  @tracked rotation = 0;

  refreshMap() {
    later(
      this,
      function () {
        this.args.map.updateSize();
      },
      10
    );
  }

  get showRotation() {
    return this.rotation != 0;
  }

  get arrowRotation() {
    return htmlSafe('transform: rotate(' + this.rotation + 'rad);');
  }

  @action
  watchRotation() {
    if (!this.args.map) {
      return;
    }

    this._rotationChange = () => {
      this.rotation = this.args.map.getView().getRotation();
    };

    this.args.map.getView().on('change:rotation', this._rotationChange);
  }

  @action
  async handleLocalize() {
    this.localizationWaiting = true;

    try {
      const center = await this.args.onLocalize(this.args.map);
      this.localizationError = false;

      if (center) {
        this.args.map
          .getView()
          .animate({ zoom: 16, center: fromLonLat(center) });
      }
    } catch (err) {
      this.localizationError = true;
    }

    this.localizationWaiting = false;
  }

  willDestroy() {
    super.willDestroy(...arguments);
    if (!this.args.map) {
      return;
    }

    this.args.map.getView().un('change:rotation', this._rotationChange);
  }

  @action
  zoomIn() {
    const view = this.args.map.getView();
    if (view.getAnimating()) return;

    const zoom = parseInt(view.getZoom()) + 1;
    view.animate({ zoom });
  }

  @action
  zoomOut() {
    const view = this.args.map.getView();
    if (view.getAnimating()) return;

    const zoom = parseInt(view.getZoom()) - 1;
    view.animate({ zoom });
  }

  @action
  resetRotation() {
    this.args.map.getView().setRotation(0);
  }
}
