import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

import { transform } from 'ol/proj';
import { transformExtent } from 'ol/proj';
import { createCenterConstraint } from 'ol/View';
import { later } from '@ember/runloop';

export default class MapViewComponent extends Component {
  @tracked exchangeSetCount = 0;
  @tracked exchangeUpdateCount = 0;
  @tracked extentView = '';

  constructor() {
    super(...arguments);

    this._changeExtent = () => {
      if (this.isDestroyed) {
        return this.removeFromMap();
      }

      return this.changeExtent();
    };

    this._changeCenter = () => {
      return this.changeCenter();
    };

    this._changeResolution = () => {
      return this.changeResolution();
    };

    this._changeRotation = () => {
      return this.changeRotation();
    };

    this._change = () => {
      return this.change();
    };
  }

  @action
  update() {
    if (!this.args.map) {
      return this.removeFromMap();
    }

    if (this.oldMap != this.args.map) {
      this.removeFromMap();

      this.view = this.args.map.getView();
      this.oldMap = this.args.map;

      this.args.map.on('moveend', this._changeExtent);
      this.view.on('change:center', this._changeCenter);
      this.view.on('change:resolution', this._changeResolution);
      this.view.on('change:rotation', this._changeRotation);
      this.view.on('change', this._change);
    }

    this.updateZoom();
    this.updateCenterM();
    this.updateCenter();
    this.updateConstrainResolution();
    this.updateEnableRotation();
    this.updateConstrainOnlyCenter();
    this.updateSmoothExtentConstraint();
    this.updateMaxResolution();
    this.updateMinResolution();
    this.updateMaxZoom();
    this.updateMinZoom();
    this.updateMultiWorld();
    this.updateSmoothResolutionConstraint();
    this.updateProjection();
    this.updateRotation();
    this.updateZoomFactor();
    this.updateExtent();
    this.updateExtentM();
  }

  isSet(value) {
    if (Array.isArray(value)) {
      return (
        value.map((a) => parseFloat(a)).filter((a) => isNaN(a)).length == 0
      );
    }

    return value !== undefined && value !== null;
  }

  @action
  updateZoom() {
    if (!this.isSet(this.args.zoom)) return;
    this.view.setZoom(this.args.zoom);
  }

  @action
  updateCenter() {
    if (!this.isSet(this.args.center)) return;
    this.view.setCenter(this.args.center);
  }

  @action
  updateCenterM() {
    if (!this.isSet(this.args.centerM)) return;
    this.centerM = this.args.centerM;
  }

  @action
  updateConstrainResolution() {
    if (!this.isSet(this.args.constrainResolution)) return;
    this.view.setConstrainResolution(this.args.constrainResolution);
  }

  @action
  updateEnableRotation() {
    if (!this.isSet(this.args.enableRotation)) return;
    this.view.setEnableRotation(this.args.enableRotation);
  }

  @action
  updateConstrainOnlyCenter() {
    if (!this.isSet(this.args.constrainOnlyCenter)) return;
    this.setConstrainOnlyCenter(this.args.constrainOnlyCenter);
  }

  @action
  updateSmoothExtentConstraint() {
    if (!this.isSet(this.args.smoothExtentConstraint)) return;
    this.view.setSmoothExtentConstraint(this.args.smoothExtentConstraint);
  }

  @action
  updateMaxResolution() {
    if (!this.isSet(this.args.maxResolution)) return;
    this.view.setMaxResolution(this.args.maxResolution);
  }

  @action
  updateMinResolution() {
    if (!this.isSet(this.args.minResolution)) return;
    this.view.setMinResolution(this.args.minResolution);
  }

  @action
  updateMaxZoom() {
    if (!this.isSet(this.args.maxZoom)) return;
    this.view.setMaxZoom(this.args.maxZoom);
  }

  @action
  updateMinZoom() {
    if (!this.isSet(this.args.minZoom)) return;
    this.view.setMinZoom(this.args.minZoom);
  }

  @action
  updateMultiWorld() {
    if (!this.isSet(this.args.multiWorld)) return;
    this.view.setMultiWorld(this.args.multiWorld);
  }

  @action
  updateSmoothResolutionConstraint() {
    if (!this.isSet(this.args.smoothExtentConstraint)) return;
    this.view.setSmoothResolutionConstraint(this.args.smoothExtentConstraint);
  }

  @action
  updateProjection() {
    if (!this.isSet(this.args.projection)) return;
    this.view.setProjection(this.args.projection);
  }

  @action
  updateRotation() {
    if (!this.isSet(this.args.rotation)) return;
    this.view.setRotation(this.args.rotation);
  }

  @action
  updateZoomFactor() {
    if (!this.isSet(this.args.zoomFactor)) return;
    this.view.setZoomFactor(this.args.zoomFactor);
  }

  @action
  updateExtent() {
    this.extent = this.args.extent;
  }

  @action
  updateExtentM() {
    this.extentM = this.args.extentM;
  }

  removeFromMap() {
    if (this.oldMap) {
      this.oldMap.un('moveend', this._changeExtent);
    }

    if (this.view) {
      this.view.un('change:center', this._changeCenter);
      this.view.un('change:resolution', this._changeResolution);
      this.view.un('change:rotation', this._changeRotation);
      this.view.un('change', this._change);
      this.view = null;
    }
  }

  changeExtent() {
    this.exchangeUpdateCount++;

    this.extentView = this.extentM.join(',');

    if (this.args.onChangeExtent) {
      return this.args.onChangeExtent(this.extentM);
    }
  }

  changeCenter() {
    if (this.args.onChangeCenter) {
      return this.args.onChangeCenter(this.view);
    }
  }

  changeResolution() {
    if (this.args.onChangeResolution) {
      return this.args.onChangeResolution(this.view);
    }
  }

  changeRotation() {
    if (this.args.onChangeRotation) {
      return this.args.onChangeRotation(this.view);
    }
  }

  change() {
    if (this.args.onChange) {
      return this.args.onChange(this.view);
    }
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.removeFromMap();
  }

  get centerM() {
    return transform(this.view.getCenter(), 'EPSG:3857', 'EPSG:4326');
  }

  set centerM(value) {
    if (!this.isSet(value)) return;

    const newValue = transform(value, 'EPSG:4326', 'EPSG:3857');
    this.view.setCenter(newValue);
  }

  get constraintExtent() {
    return null;
  }

  set constraintExtent(value) {
    if (!this.view) {
      return;
    }

    const constraints = this.map.getView().getConstraints();
    constraints['center'] = createCenterConstraint({ extent: value });

    this.view.set('extent', value);
  }

  get extent() {
    return this.view.calculateExtent(this.oldMap.getSize());
  }

  set extent(value) {
    later(() => {
      this.exchangeSetCount++;
    });

    if (
      !value ||
      value.length != 4 ||
      value.map((a) => parseFloat(a)).filter((a) => isNaN(a)).length > 0
    ) {
      return;
    }

    let options = {};

    if (Array.isArray(this.args.padding)) {
      options.padding = this.args.padding;
    }

    if (value) {
      this.view.fit(value, options);
    }
  }

  get extentM() {
    const extent = this.view.calculateExtent(this.oldMap.getSize());
    return transformExtent(extent, 'EPSG:3857', 'EPSG:4326');
  }

  set extentM(value) {
    if (
      !value ||
      value.length != 4 ||
      value.map((a) => parseFloat(a)).filter((a) => isNaN(a)).length > 0
    ) {
      return;
    }

    this.extent = transformExtent(value, 'EPSG:4326', 'EPSG:3857');
  }
}
