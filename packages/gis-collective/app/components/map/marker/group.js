import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { getCenter } from 'ol/extent';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { all } from 'rsvp';

export default class MapMarkerGroupComponent extends Component {
  @service store;
  @tracked features = [];
  @tracked pendingCount = 0;
  @tracked isLoading;

  get isZoomVisible() {
    return this.features?.length >= 3;
  }

  @action
  updateFeatures() {
    if (!this.args.feature) {
      this.features = null;
      return;
    }

    const properties = this.args.feature.getProperties();
    this.pendingCount = properties['pendingCount'];

    if (!properties['idList']) {
      this.features = null;
      return;
    }

    this.isLoading = true;
    this.features = all(
      properties['idList']
        .split(',')
        .map((a) => this.store.findRecord('feature', a))
    ).then((items) => {
      this.isLoading = false;
      this.features = items;
    });
  }

  @action
  select(feature) {
    const geometry = this.args.feature.getGeometry();
    const olFeature = {
      properties: { _id: feature.id },
      getProperties() {
        return this.properties;
      },
      getGeometry() {
        return geometry;
      },
    };

    this.args?.onSelect(olFeature, feature);
  }

  @action
  zoomIn() {
    const view = this.args.map.getView();
    const center = getCenter(this.args.feature.getGeometry().getExtent());

    const zoom = view.getZoom() + 1;
    view.animate({ zoom, center });

    this.args?.onZoom();
  }
}
