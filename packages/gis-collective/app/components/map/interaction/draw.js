import Component from '@glimmer/component';
import Draw from 'ol/interaction/Draw';
import { action } from '@ember/object';

export default class MapInteractionDrawComponent extends Component {
  @action
  setupOl() {
    if (!this.args.map) {
      return;
    }

    this.draw = new Draw({
      source: this.args.source,
      type: this.args.type,
      geometryFunction: this.args.geometryFunction,
    });

    this.args.map.addInteraction(this.draw);
    this.currentInteractions = [this.draw];

    this.draw.on('drawend', (ev) => {
      this.args.onDrawEnd?.(ev);
    });
  }

  willDestroy() {
    super.willDestroy(...arguments);
    if (this.currentInteractions && this.args.map) {
      this.currentInteractions.forEach((item) => {
        this.args.map.removeInteraction(item);
      });
    }
  }
}
