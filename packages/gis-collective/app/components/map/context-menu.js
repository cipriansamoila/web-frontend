import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { action } from '@ember/object';
import { transform } from 'ol/proj';

export default class MapContextMenuComponent extends Component {
  get options() {
    if (!this.args.options) {
      return [];
    }

    return this.args.options.split(',');
  }

  get style() {
    const left = this.args.x || 0;
    const top = this.args.y || 0;

    return htmlSafe(`top: ${top}px; left: ${left}px;`);
  }

  @action
  selectOption(value) {
    if (this.args.onSelect) {
      let lonLat = [0, 0];

      if (this.args.map) {
        const coordinates = this.args.map.getCoordinateFromPixel([
          this.args.x,
          this.args.y,
        ]);
        lonLat = transform(coordinates, 'EPSG:3857', 'EPSG:4326');
      }

      this.args.onSelect(value, lonLat[0], lonLat[1]);
    }
  }
}
