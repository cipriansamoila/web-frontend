import { fromLonLat, transformExtent, transform } from 'ol/proj';
import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import Map from 'ol/Map';
import * as olInteraction from 'ol/interaction';
import { later, cancel } from '@ember/runloop';

export default class MapPlainComponent extends Component {
  @tracked disabled = false;
  @tracked firstZoom = 14;
  @tracked map;

  get viewbox() {
    if (!this.map) {
      return null;
    }

    var extent = this.map.getView().calculateExtent(this.map.getSize());
    extent = transformExtent(extent, 'EPSG:3857', 'EPSG:4326');

    if (extent.filter((a) => isNaN(a)).length > 0) {
      return null;
    }

    return extent.join(',');
  }

  set viewbox(value) {
    if (!this.map) {
      return;
    }

    let currentExtent = this.map.getView().calculateExtent(this.map.getSize());

    try {
      value = value.split(',');
      value = value.map((a) => parseFloat(a));
    } catch (err) {
      value = value.map((a) => parseFloat(a));
    }

    const transformedValue = transformExtent(value, 'EPSG:4326', 'EPSG:3857');

    currentExtent = transformExtent(currentExtent, 'EPSG:3857', 'EPSG:4326');

    const order = 1000000;
    const a = currentExtent.map((a) => Math.round(a * order) / order).join('|');
    const b = value.map((a) => Math.round(a * order) / order).join('|');

    if (a != b) {
      this.map.getView().fit(transformedValue, this.map.getSize());
    }
  }

  get zoom() {
    if (!this.map) {
      return null;
    }

    return this.map.getView().getZoom();
  }

  set zoom(value) {
    this.map.getView?.().setZoom?.(value);
  }

  @action
  updateInteractions() {
    if (this.args.disabled) {
      this.interactions?.forEach((interaction) => {
        this.map.removeInteraction(interaction);
      });
    } else {
      this.interactions?.forEach((interaction) => {
        this.map.addInteraction(interaction);
      });
    }
  }

  spyResizeWithTimer() {
    this.oldW = this.mapElement.offsetWidth;
    this.oldH = this.mapElement.offsetHeight;
    cancel(this.resizeTimer);
    const _this = this;

    function sizeCheck() {
      const resizeTimer = later(() => {
        sizeCheck();
      }, 1000);

      _this.resizeTimer = resizeTimer;

      if (
        _this.oldW == _this.mapElement.offsetWidth &&
        _this.oldH == _this.mapElement.offsetHeight
      ) {
        return;
      }

      _this.oldW = _this.mapElement.offsetWidth;
      _this.oldH = _this.mapElement.offsetHeight;

      _this.args.map.updateSize();
    }

    const resizeTimer = later(() => {
      sizeCheck();
    }, 1000);

    _this.resizeTimer = resizeTimer;
  }

  spyResize() {
    try {
      if (this.sizeObserver) {
        this.sizeObserver.disconnect();
      }

      this.sizeObserver = new ResizeObserver(() => {
        this.map?.updateSize();
      });

      this.sizeObserver.observe(this.mapElement);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
      return this.spyResizeWithTimer(this.mapElement);
    }
  }

  @action
  async setupMapElement(element) {
    this.mapElement = element;

    this.map = new Map({
      controls: [],
      target: element,
      loadTilesWhileAnimating: true,
      loadTilesWhileInteracting: true,
      interactions: olInteraction.defaults({
        doubleClickZoom: false,
      }),
    });

    if (this.isDestroyed || this.isDestroying) {
      return;
    }

    this.spyResize();

    this.mapElement.olMap = this.map;

    let useCenter = true;

    if (this.initialExtent) {
      this.map.getView().set('extent', this.initialExtent);
    } else {
      this.map.getView().set('extent', null);
    }

    if (this.firstViewbox) {
      this.viewbox = this.args.firstViewbox;
      useCenter = false;
    }

    if (useCenter) {
      this.map.getView().setZoom(this.firstZoom);

      if (this.firstCenter) {
        this.map.getView().setCenter(fromLonLat(this.firstCenter));
      }
    }

    this._moveend = () => {
      // eslint-disable-next-line no-self-assign
      this.viewbox = this.viewbox;

      if (this.onCenterMove) {
        const center = transform(
          this.map.getView().getCenter(),
          'EPSG:3857',
          'EPSG:4326'
        );
        this.onCenterMove(center);
      }
    };

    this.map.on('moveend', this._moveend);
    this.interactions = this.map.getInteractions().getArray().slice();

    this.updateInteractions();
  }

  willDestroy() {
    super.willDestroy(...arguments);

    if (!this.map) {
      return;
    }

    this.map?.un?.('moveend', this._moveend);

    this.interactions?.forEach((interaction) => {
      this.map.addInteraction(interaction);
    });

    for (let layer of this.map.getAllLayers()) {
      this.map.removeLayer(layer);
    }
  }
}
