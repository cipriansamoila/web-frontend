import Select from 'ol/interaction/Select';

export default class FeatureSelect extends Select {
  constructor() {
    super(...arguments);

    this.on('select', function (e) {
      this.notify(e.selected.length > 0 ? e.selected[0] : null);
    });
  }

  restorePreviousStyle_() {}

  applySelectedStyle_() {}
}
