import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class VectorTilesComponent extends Component {
  @service layer;
  @tracked currentLayers = [];

  hasChanges(newLayers) {
    if (this.currentLayers.length != newLayers.length) {
      return true;
    }

    let result = false;

    this.currentLayers.forEach((layer, index) => {
      if (layer.__id === undefined || layer.__id != newLayers[index].__id) {
        result = true;
      }
    });

    return result;
  }

  @action
  update() {
    if (!this.args.value?.layers?.length || !this.args.map) {
      return;
    }

    const result = this.args.value.layers
      .slice()
      .reverse()
      .map((layer) => this.layer.toOl(layer, this.args.cacheId, this.args.map));

    if (!this.hasChanges(result)) {
      return;
    }

    for (let layer of this.currentLayers) {
      this.args.map.removeLayer(layer);
    }

    const mapLayers = this.args.map.getLayers();

    for (let layer of result) {
      mapLayers.insertAt(0, layer);
    }

    this.currentLayers = result;

    this.args.map.setLayers(mapLayers);
  }

  willDestroy() {
    super.willDestroy(...arguments);
    if (!this.args.value?.layers?.length || !this.args.map) {
      return;
    }

    for (let layer of this.currentLayers) {
      this.args.map.removeLayer(layer);
    }
  }
}
