import { inject as service } from '@ember/service';

import MVT from 'ol/format/MVT';
import VectorTile from 'ol/layer/VectorTile';
import VectorTileSource from 'ol/source/VectorTile';

import { action } from '@ember/object';
import FeatureSelect from './interactions/FeatureSelect';
import config from '../../../config/environment';

import Component from '@glimmer/component';

export default class VectorTilesComponent extends Component {
  @service layer;
  @service featureStyle;

  debugTiles = config.debugTiles;
  iconCache = {};

  constructor() {
    super(...arguments);

    this.source = new VectorTileSource({
      format: new MVT(),
      tileLoadFunction: (tile, url) => {
        tile.setLoader((extent, resolution, projection) => {
          const xhr = new XMLHttpRequest();
          xhr.open('GET', url);
          xhr.responseType = 'arraybuffer';

          if (this.args.bearer) {
            xhr.setRequestHeader('Authorization', this.args.bearer);
          }

          xhr.onload = function () {
            const format = tile.getFormat();

            tile.setFeatures(
              format.readFeatures(xhr.response, {
                extent: extent,
                featureProjection: projection,
              })
            );
          };

          xhr.send();
        });
      },
    });
  }

  setupLayerObject() {
    if (this.olLayer) {
      return this.olLayer;
    }

    this.olLayer = this.layer.getCached(
      this.args.cacheId,
      'vt',
      this.args.map,
      () =>
        new VectorTile({
          source: this.source,
          declutter: false,
          renderMode: 'vector',
          zIndex: 9999,
        })
    );

    this.olLayer.setStyle((feature, resolution) => {
      return this.args.style?.(feature, resolution, this.args.selectedId);
    });

    this.featureSelect = new FeatureSelect({
      layers: [this.olLayer],
    });

    this.featureSelect.notify = (feature) => {
      this.args.onSelect?.(feature, this.olLayer);
    };

    this.pointerMove = (event) => {
      this.handlePointerMove(event);
    };

    return this.olLayer;
  }

  @action
  update() {
    this.setupLayerObject();

    if (this.oldMap != this.args.map) {
      this.removeFromMap();
      this.oldMap = this.args.map;
      this.args.map.getLayers().push(this.olLayer);
      this.args.map.addInteraction(this.featureSelect);

      this.args.map.on('pointermove', this.pointerMove);
    }

    if (this.oldUrl != this.args.url) {
      this.source.setUrl(this.args.url);
      this.oldUrl = this.args.url;
    }

    if (this.oldSelectedId != this.args.selectedId) {
      this.oldSelectedId = this.args.selectedId;
      this.olLayer.changed();
    }

    if (this.args.onLayerUpdate) {
      this.args.onLayerUpdate(this.olLayer);
    }
  }

  removeFromMap() {
    if (!this.oldMap) {
      return;
    }

    this.oldUrl = null;
    this.oldMap.removeInteraction(this.featureSelect);
    this.oldMap.un('pointermove', this.pointerMove);
    this.oldMap.removeLayer(this.olLayer);
  }

  handlePointerMove(evt) {
    if (
      this.handlingMouseMove ||
      evt.dragging ||
      !this.olLayer ||
      !this.args.map
    ) {
      return;
    }

    const pixel = this.args.map.getEventPixel(evt.originalEvent);

    this.handlingMouseMove = true;
    this.olLayer.getFeatures(pixel).then(
      (features) => {
        this.handlingMouseMove = false;

        const feature = features.length ? features[0] : undefined;

        if (this.feature != feature) {
          this.feature = feature;

          if (this.args.onHover) {
            this.args.onHover(feature);
          }
        }
      },
      () => {
        if (this.feature != null) {
          this.feature = null;

          if (this.args.onHover) {
            this.args.onHover(null);
          }
        }

        this.handlingMouseMove = false;
      }
    );
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.removeFromMap();
  }
}
