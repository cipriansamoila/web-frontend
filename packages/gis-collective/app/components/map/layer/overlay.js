import Component from '@glimmer/component';
import { fromLonLat } from 'ol/proj';
import { action } from '@ember/object';
import Overlay from 'ol/Overlay';

export default class MapLayerPointComponent extends Component {
  get position() {
    return fromLonLat(this.args.position);
  }

  @action
  setup(element) {
    this._element = element;

    this.marker = new Overlay({
      position: this.position,
      positioning: this.args.positioning,
      element,
      stopEvent: this.args.stopEvent,
      html: true,
      insertFirst: true,
    });

    this.args.map?.addOverlay(this.marker);
  }

  @action
  update() {
    this.marker.setPosition(this.position);
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.args.map?.removeOverlay?.(this.marker);
  }
}
