import Component from '@glimmer/component';
import { action } from '@ember/object';
import { htmlSafe } from '@ember/template';

export default class MapBaseMapSelectComponent extends Component {
  get covers() {
    if (!this.args.list) {
      return [];
    }

    const selectedId = this.args.selected?.get
      ? this.args.selected.get('id')
      : this.args.selected?._id;

    return this.args.list
      .filter((a) => {
        const id = a.get ? a.get('id') : a._id;

        return id != selectedId;
      })
      .map((a) => a.cover);
  }

  get styles() {
    return this.covers.map((a) => {
      const picture = a.get ? a.get('picture') : a.picture;

      return {
        cover: a,
        style: htmlSafe(`background-image: url(${picture}/md);`),
      };
    });
  }

  @action
  select(cover) {
    const coverId = cover.get ? cover.get('id') : cover._id;

    const selected = this.args.list.find((a) => {
      const id = a.cover?.get ? a.cover?.get('id') : a.cover?._id;

      return id == coverId;
    });

    this.args.onSelect?.(selected);
  }
}
