import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  showFull: false,
  maxChars: 800,

  hasLongValue: computed('maxChars', 'value.length', function () {
    return this.value && this.value.length > (this.maxChars || 800);
  }),

  actions: {
    showMore: function () {
      this.set('showFull', true);
    },

    showLess: function () {
      this.set('showFull', false);
    },
  },
});
