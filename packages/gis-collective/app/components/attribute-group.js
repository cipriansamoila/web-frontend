import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
  classNames: ['card', 'mt-3', 'shadow-sm'],
  classNameBindings: ['isPosition:card-position', 'isIconList:icon-list'],

  position: service(),

  isPosition: computed('group.type', function () {
    return this.group.type == 'position';
  }),

  latitude: computed('group.value.coordinates', 'isPosition', function () {
    if (
      !this.isPosition ||
      !this.group ||
      !this.group.value ||
      !this.group.value.coordinates
    ) {
      return 'NaN';
    }

    return this.group.value.coordinates[1];
  }),

  longitude: computed('group.value.coordinates', 'isPosition', function () {
    if (
      !this.isPosition ||
      !this.group ||
      !this.group.value ||
      !this.group.value.coordinates
    ) {
      return 'NaN';
    }

    return this.group.value.coordinates[0];
  }),

  isList: computed('group.type', function () {
    return this.group.type == 'list';
  }),

  containsIcon: computed('group.icon', 'isList', function () {
    return !!this.group.icon;
  }),

  isPictureList: computed('group.type', function () {
    return this.group.type == 'pictureList';
  }),

  isIconList: computed('group.type', function () {
    return this.group.type == 'iconList';
  }),

  actions: {
    onLocalize() {
      if (!this.onLocalize) {
        alert('onLocalize is not set');
      }
      this.onLocalize(...arguments);
    },

    editValue() {
      if (!this.editValue) {
        alert('editValue is not set');
      }

      this.editValue(...arguments);
    },
    editSection(name) {
      if (!this.editSection) {
        alert('editSection is not set');
      }

      this.editSection(name);
    },
  },
});
