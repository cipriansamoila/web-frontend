import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class LayoutComponent extends Component {
  _cols = [];

  get cols() {
    return this._cols;
  }

  computeCols(cols) {
    if (!cols?.length) {
      this._cols.clear();
    }

    if (cols?.length < this._cols.length) {
      this._cols.removeAt(cols.length - 1, this._cols.length - cols.length);
      this._cols.arrayContentDidChange(
        cols.length - 1,
        this._cols.length - cols.length,
        0
      );
    }

    cols?.forEach((col, i) => {
      if (!this._cols[i]) {
        this._cols.push(col);
        this._cols.arrayContentDidChange(i, 0, 1);
        return;
      }

      if (this._cols[i].type != col.type) {
        this._cols.replace(i, 1, [col]);
        this._cols.arrayContentDidChange(i, 1, 1);
      }

      if (JSON.stringify(this._cols[i].data) != JSON.stringify(col.data)) {
        this._cols[i].data = col.data;
      }
    });
  }

  @action
  setup() {
    this.computeCols(this.args.cols);
  }

  @action
  select(col) {
    this.args.onSelect?.(col);
  }
}
