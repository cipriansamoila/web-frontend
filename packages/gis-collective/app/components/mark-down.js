import Component from '@glimmer/component';
import { marked } from 'marked';
import { action } from '@ember/object';
import { htmlSafe } from '@ember/template';
import { inject as service } from '@ember/service';
import sanitizeHtml from 'sanitize-html';

export default class MarkDownComponent extends Component {
  @service router;

  constructor() {
    super(...arguments);

    this.mdeRenderer = new marked.Renderer();

    this.mdeRenderer.link = (href, title, text) => {
      if (href.indexOf('https://youtu.be/') == 0) {
        return this.youtubeShortUrl(href);
      }

      let cls = 'secure';

      if (href.indexOf('http://') == 0) {
        cls = 'unsecure';
      }

      if (!text) {
        text = href;
      }

      let strTarget = '';

      if (this.args.target) {
        strTarget = `target="${this.args.target}"`;
      }

      return `<a href="${href}" ${strTarget} class="${cls}">${text}</a>`;
    };
  }

  get renderedText() {
    if (!this.args.value) {
      return '';
    }

    const sanitizedValue = sanitizeHtml(this.args.value, {
      allowedTags: [],
      allowedAttributes: [],
    });
    return htmlSafe(
      marked(this.updateTags(sanitizedValue), { renderer: this.mdeRenderer })
    );
  }

  youtubeShortUrl(url) {
    const pieces = url.split('/');
    const id = pieces[pieces.length - 1];

    return `<iframe src="https://www.youtube.com/embed/${id}" frameborder="0" allow="autoplay; encrypted-media; picture-in-picture" class="youtube" allowfullscreen></iframe>`;
  }

  @action
  didRenderContent(element) {
    this.fixLinks(element);
  }

  fixLinks(element) {
    var router = this.router;

    const links = element.querySelectorAll('a');

    links.forEach((element) => {
      element.addEventListener('click', function () {
        if (!this.attributes.href || !this.attributes.href.value) {
          return true;
        }

        if (
          this.attributes.target &&
          this.attributes.target.value == '_blank'
        ) {
          return true;
        }

        if (this.attributes.href.value.indexOf('mailto:') === 0) {
          return true;
        }

        if (
          this.attributes.href.value.indexOf('http:') === 0 ||
          this.attributes.href.value.indexOf('https:') === 0
        ) {
          return true;
        }

        router.transitionTo(this.attributes.href.value);

        return false;
      });
    });
  }

  updateTags(text) {
    if (!text) {
      return '';
    }

    text = text.trim();

    if (text == '') {
      return text;
    }

    return text
      .split('\n')
      .map((line) => {
        const pieces = line.split('#');

        pieces.forEach((element, index) => {
          if (index == 0) {
            return '';
          }

          if (element.length == 0) {
            pieces[index] = '#';
            return;
          }

          if (element.substring(0, 1) == ' ') {
            pieces[index] = '#' + element;
            return;
          }

          const sectionPieces = element.split(' ');
          let url;

          try {
            url =
              this.router.urlFor('browse.index') + '?tag=' + sectionPieces[0];
          } catch (err) {
            url = '/test/' + sectionPieces[0];
          }
          sectionPieces[0] = `[#${sectionPieces[0]}](${url} "#${sectionPieces[0]}")`;

          pieces[index] = sectionPieces.join(' ');
        });

        return pieces.join('');
      })
      .join('\n');
  }
}
