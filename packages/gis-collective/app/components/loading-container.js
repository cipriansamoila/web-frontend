import Component from '@glimmer/component';

export default class LoadingContainerComponent extends Component {
  get animate() {
    if (this._value !== undefined) {
      return this._value;
    }

    if (this.args.animate === undefined) {
      this._value = true;
    } else {
      this._value = !!this.args.animate;
    }

    return this._value;
  }
}
