import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class LanguageSelectorComponent extends Component {
  @service clickOutside;
  @tracked showingOptions;

  @action
  setup(element) {
    this.element = element;
  }

  @action
  show() {
    this.clickOutside.subscribe(this.element, () => {
      this.hide();
    });

    this.showingOptions = true;
  }

  @action
  hide() {
    this.showingOptions = false;
  }

  @action
  selectLanguage(item) {
    this.args.onSelect?.(item);
    this.showingOptions = false;
  }

  @action
  handleClick() {
    this.hide();
  }
}
