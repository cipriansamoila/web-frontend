import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class SlideWelcomeEndComponent extends Component {
  @service session;

  @action
  back() {
    this.args.onPrevPage?.();
  }
}
