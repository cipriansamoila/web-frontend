import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ['d-inline', 'button-upload-file'],
  session: service(),
  hasError: false,
  working: false,
  progress: 0,

  getFormData() {
    let fileInput = this.element.querySelector('input[type=file]');
    var formData = new FormData();

    formData.append('file', fileInput.files[0], fileInput.files[0].name);

    return formData;
  },

  buttonClass: computed('hasError', function () {
    return this.hasError ? 'btn-danger' : 'btn-primary';
  }),

  actions: {
    selectFile: function () {
      this.element.querySelector('input[type=file]').click();
    },

    upload: function () {
      let { access_token } = this.session.data.authenticated;

      var xhr = new XMLHttpRequest();

      xhr.onreadystatechange = () => {
        this.set('hasError', false);
        this.set('working', true);
        this.set('progress', 0);

        if (xhr.readyState == 4) {
          this.set('working', false);
          const action = this.onUpload;

          if (action) {
            action();
          }
        }

        if (xhr.status && xhr.readyState == 4) {
          if (xhr.status >= 200 && xhr.status < 300) {
            this.set('hasError', false);
          } else {
            this.set('hasError', true);
          }
        }
      };

      xhr.onerror = () => {
        this.set('hasError', true);
        this.modal.alert(
          'File upload error',
          'The server does not accept the file.'
        );
      };

      xhr.upload.addEventListener('progress', (event) => {
        this.set('progress', (event.loaded / event.total) * 100);
      });

      const url = this.url;

      xhr.open('POST', url, true);
      xhr.setRequestHeader('Authorization', `Bearer ${access_token}`);
      xhr.send(this.getFormData());
    },
  },
});
