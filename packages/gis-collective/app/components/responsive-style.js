import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';

export default class ResponsiveStyleComponent extends Component {
  styles(values) {
    let result = `${this.args.selector} {`;

    Object.keys(values).forEach((key) => {
      result += `${key}: ${values[key]};`;
    });

    return htmlSafe(result + '}');
  }

  get sm() {
    return this.styles(this.args.style?.sm ?? {});
  }

  get md() {
    return this.styles(this.args.style?.md ?? {});
  }

  get lg() {
    return this.styles(this.args.style?.lg ?? {});
  }
}
