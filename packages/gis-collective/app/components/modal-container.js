import Component from '@glimmer/component';
import { Modal } from 'bootstrap';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class ModalContainerComponent extends Component {
  @service fastboot;
  @tracked _visible = false;
  @tracked waitAction;

  @action
  updateVisible() {
    this.visible = this.args.visible;
  }

  @action
  setup(element) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    this.modal = new Modal(element, { show: false });

    element.addEventListener('hidden.bs.modal', () => {
      this.clear();
    });

    element.addEventListener('hide.bs.modal', () => {
      this._visible = false;
    });

    element.addEventListener('show.bs.modal', () => {
      this._visible = true;
    });
  }

  willDestroy() {
    super.willDestroy();
    if (this.modal) {
      this.modal.dispose();
    }
  }

  clear() {}

  get visible() {
    return this._visible;
  }

  set visible(value) {
    if (value) {
      this.modal.show();
    }

    this._visible = value;
  }

  @action
  hide() {
    try {
      return this.modal.hide();
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }
  }

  @action
  resolve() {
    if (!this.args.resolve) {
      return;
    }

    var actionResult = this.args.resolve(...arguments);

    if (actionResult.then) {
      this.waitAction = true;

      return actionResult
        .then(() => {
          this.waitAction = false;
          return this.hide();
        })
        .catch(() => {
          this.waitAction = false;
        });
    }
  }

  @action
  reject() {
    this.hide();
    return this.args.reject?.(...arguments);
  }
}
