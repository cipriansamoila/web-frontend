import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class LanguageSelectorComponent extends Component {
  @service clickOutside;
  @service router;
  @tracked showingOptions;

  get selectedLanguage() {
    let selected = [];

    if (this.args.languages) {
      selected = this.args.languages.filter(
        (a) => a.locale == this.args.selectedLocale
      );
    }

    if (selected.length == 0) {
      return 'Language not available';
    }

    return selected[0].name;
  }

  @action
  selectLanguage(locale) {
    this.router.transitionTo({ queryParams: { locale } });
  }
}
