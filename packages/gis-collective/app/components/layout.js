import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class LayoutComponent extends Component {
  get containers() {
    const options = this.args.containerOptions ?? [];

    return (
      this.args.containers?.map(
        (a, index) => new ContainerWithOptions(a, options[index])
      ) ?? []
    );
  }

  @action
  selectCol(pageCol) {
    this.args.onSelect?.(pageCol);
  }

  @action
  configContainer(index) {
    this.args.onConfigContainer?.(index);
  }
}

class ContainerWithOptions {
  @tracked layout;

  constructor(layout, options) {
    this.layout = layout;
    this.options = options;
  }
}
