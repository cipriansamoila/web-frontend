import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { Tooltip } from 'bootstrap';

export default class IconComponent extends Component {
  @service fastboot;
  @tracked resolvedIcon;

  @action
  setupTooltip(element) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    this.tooltip = new Tooltip(element, {
      title: () => {
        return this.localName;
      },
    });
  }

  @action
  destroyTooltip() {
    if (this.fastboot.isFastBoot) {
      return;
    }

    this.tooltip?.dispose();
  }

  get hasIcon() {
    if (this.resolvedIcon) {
      return true;
    }

    if (this.args.src && this.args.src.then) {
      if (this.args.waiting) {
        return false;
      }

      this.args.src.then(
        (result) => {
          this.resolvedIcon = result;
        },
        () => {
          this.resolvedIcon = {};
        }
      );

      return false;
    }

    return !!this.args.src;
  }

  get isLoading() {
    if (this.args.waiting && !this.args.src?.image?.value) {
      return true;
    }

    let isLoading = false;
    if (typeof this.args.src?.get == 'function') {
      this.args.src.get('isLoading');
    }

    if (this.args.src && isLoading) {
      return true;
    }

    if (this.args.src && this.args.src.then && !this.hasIcon) {
      return true;
    }

    return false;
  }

  get image() {
    if (!this.hasIcon) {
      return null;
    }

    return this.icon.image;
  }

  get name() {
    if (!this.hasIcon) {
      return '';
    }

    return this.icon.localName;
  }

  get icon() {
    if (this.resolvedIcon) {
      return this.resolvedIcon;
    }

    return this.args.src;
  }

  get hasFailedIcon() {
    if (this.isLoading) {
      return false;
    }

    return this.hasIcon && !this.image?.value;
  }
}
