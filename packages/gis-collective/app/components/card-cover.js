import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class SiteCoverComponent extends Component {
  @tracked isLoaded;

  get hashStyle() {
    if (this.firstValidPicture) {
      return htmlSafe(this.firstValidPicture.get('hashBg') ?? '');
    }

    return htmlSafe(this.args.fallbackHash ?? '');
  }

  get firstValidPicture() {
    const pictures = this.args.pictures;

    if (!pictures || pictures.isRejected) {
      return null;
    }

    const remaining = pictures.filter(
      (a) => a && !a.isRejected && a.get && a.get('picture')
    );

    return remaining.length > 0 ? remaining[0] : null;
  }

  get url() {
    const picture = this.firstValidPicture;

    if (!picture) {
      return null;
    }

    let url = picture.get ? picture.get('picture') : picture.picture;

    if (this.args.size) {
      url += '/' + this.args.size;
    }

    return url;
  }

  get style() {
    let url = this.url;

    if (!this.url) {
      return htmlSafe('');
    }

    return htmlSafe(`background-image: url(${url})`);
  }

  @action
  handleClick() {
    this.args.onClick?.();
  }

  @action
  loaded() {
    this.isLoaded = true;
  }
}
