import Component from '@ember/component';
import { computed } from '@ember/object';
import { htmlSafe } from '@ember/template';

export default Component.extend({
  classNames: ['cover'],
  attributeBindings: ['style'],
  style: '',

  didInsertElement: function () {
    this._super(...arguments);
    this.updateCover();
  },

  updateCover: function () {
    const picture = this.cover.get('picture');

    if (!picture) {
      return;
    }

    this.set('style', htmlSafe("background-image: url('" + picture + "')"));
  },

  cover: computed('_cover', {
    get() {
      return this._cover;
    },
    set(name, value) {
      if (this.value && this.value.then) {
        this.value
          .then(() => {
            this.updateCover();
          })
          .catch(() => {
            this.updateCover();
          });
      }

      this.set('_cover', value);

      return value;
    },
  }),

  click() {
    let action = this.onClick;

    if (action) {
      action();
    }
  },
});
