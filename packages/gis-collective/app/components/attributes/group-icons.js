import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class AttributesGroupIconsComponent extends Component {
  @service intl;

  get group() {
    return {
      key: 'icons',
      name: this.intl.t('icons'),
      isLoading: this.args.areIconsLoading,
    };
  }

  @action
  change(value) {
    if (this.args.onChange) {
      this.args.onChange(this.group, 'icons', value);
    }
  }
}
