import Component from '@ember/component';

export default Component.extend({
  classNames: ['text-center', 'mt-3', 'mb-2'],

  actions: {
    editValue() {
      if (!this.editValue) {
        alert('editValue is not set');
      }

      this.editValue(...arguments);
    },

    editSection(name) {
      if (!this.editSection) {
        alert('editSection is not set');
      }

      this.editSection(name);
    },
  },
});
