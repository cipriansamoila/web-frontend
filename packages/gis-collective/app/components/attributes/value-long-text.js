import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class AttributesValueLongTextComponent extends Component {
  @action
  updateValue(value) {
    if (this.args.onChange) {
      this.args.onChange(value);
    }
  }
}
