import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  init() {
    this._super(...arguments);
    this.set('defaultDefinition', { type: 'short text' });
  },

  fields: computed('group', 'icon.attributes', 'listAttributes', function () {
    let result = [];

    if (this.icon && this.icon.attributes) {
      this.icon.attributes.forEach((attribute) => {
        const value = this.getValue(attribute.name);
        const key = this.getKey(attribute.name);

        result.push({
          key,
          niceKey: attribute.name.trim(),
          value,
          type: attribute.type,
          options: attribute.options,
          isValid: this.validate(attribute.type, value),
          isIconAttribute: true,
          isRequired: attribute.isRequired,
        });
      });
    }

    const existingKeys = result.map((a) => a.key.trim().toLowerCase());

    this.listAttributes.forEach((item) => {
      const lowerKey = item.key.trim().toLowerCase();
      const exists = existingKeys.indexOf(lowerKey) != -1;

      if (exists) {
        return;
      }

      result.push({
        key: item.key,
        niceKey: item.key.trim(),
        value: item.value,
        type: 'short text',
        isValid: true,
        isIconAttribute: false,
      });
    });

    return result;
  }),

  listAttributes: computed('attributes', function () {
    if (!this.attributes) {
      return [];
    }

    if (Array.isArray(this.attributes)) {
      return this.attributes;
    }

    return Object.keys(this.attributes).map((key) => {
      return { key, value: this.attributes[key] };
    });
  }),

  icon: computed('group', 'icons.@each.name', function () {
    if (!this.icons) {
      return;
    }

    if (!this.group) {
      return;
    }

    const group = this.group.trim().toLowerCase();

    const selectedIcon = this.icons.filter((icon) => {
      const other = icon.otherNames ? icon.otherNames : [];
      const allNames = [icon.name, ...other]
        .map((a) => a + '')
        .map((a) => a.trim().toLowerCase());

      return allNames.indexOf(group) != -1;
    });

    return selectedIcon[0];
  }),

  getDefinition(key) {
    if (!this.icon) {
      return this.defaultDefinition;
    }

    const definition = this.icon.attributes.filter(
      (a) => a.name.trim().toLowerCase() == key.trim().toLowerCase()
    );

    if (definition.length == 0) {
      return this.defaultDefinition;
    }

    return definition[0];
  },

  validate: function (type, value) {
    if (type == 'boolean') {
      if (typeof value == 'string') {
        return (
          value.toLowerCase() === 'true' ||
          value.toLowerCase() === 'false' ||
          value.toLowerCase() === 'yes' ||
          value.toLowerCase() === 'no'
        );
      }

      return (
        value === 0 ||
        value === 1 ||
        value === true ||
        value === false ||
        value == undefined
      );
    }

    if (type == 'decimal') {
      if (value) {
        return !isNaN(parseFloat(value));
      }
    }

    if (type == 'integer') {
      if (value) {
        return !isNaN(parseInt(value)) && parseInt(value) == parseFloat(value);
      }
    }

    return true;
  },

  getValue(key) {
    if (!this.attributes) {
      return null;
    }

    const attributeKey = this.getKey(key);

    const items = this.listAttributes.filter((a) => a.key == attributeKey);

    if (items.length == 0) {
      return null;
    }

    return items[0].value;
  },

  getKey(attributeName) {
    if (this.listAttributes.map((a) => a.key).indexOf(attributeName) != -1) {
      return attributeName;
    }

    let result = attributeName;

    const lowerKey = attributeName.trim().toLowerCase();

    this.listAttributes
      .map((item) => {
        return { key: item.key, lowerKey: item.key.trim().toLocaleLowerCase() };
      })
      .forEach((item) => {
        if (item.lowerKey == lowerKey) {
          result = item.key;
        }
      });

    return result;
  },
});
