import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class AttributesGroupAboutComponent extends Component {
  @service intl;

  get group() {
    return {
      key: 'about',
      name: this.intl.t('about'),
    };
  }

  get map() {
    if (!this.args.feature) {
      return null;
    }

    if (!this.args.feature.maps || this.args.feature.maps.length == 0) {
      return null;
    }

    return this.args.feature.maps.firstObject;
  }

  get name() {
    if (!this.args.feature) {
      return '';
    }

    return this.args.feature.name;
  }

  get description() {
    if (!this.args.feature) {
      return '';
    }

    return this.args.feature.description;
  }

  @action
  change(key, value) {
    this.args.onChange(this.group, key, value);
  }

  @action
  changeMap(key, value) {
    if (!value) {
      return this.change('maps', []);
    }

    this.change('maps', [value]);
  }
}
