import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class AttributesValueOptionsComponent extends Component {
  @tracked areOptionsVisible = false;
  @tracked hasMouseOver = false;
  @tracked hasFocus = false;
  @tracked _value;

  emptyMessage = 'there is nothing to choose';
  selectMessage = 'select a value from the list';

  @action
  setMouseOver(value) {
    if (this.hasMouseOver == value) {
      return;
    }

    this.hasMouseOver = value;
    this.updateAreOptionsVisible();
  }

  get value() {
    if (this._value) {
      return this._value;
    }

    if (!this.args.value) {
      return '';
    }

    if (this.args.value.toArray) {
      return this.args.value.toArray();
    }

    return this.args.value;
  }

  get hasValues() {
    return this.value && this.value != '';
  }

  get options() {
    if (!this.args.options) {
      return [];
    }

    if (typeof this.args.options == 'string') {
      return this.args.options.split(',');
    }

    if (this.args.options.toArray) {
      return this.args.options.toArray();
    }

    if (Array.isArray(this.args.options)) {
      return this.args.options;
    }

    return [];
  }

  get hasOptions() {
    return this.optionsList.length > 0;
  }

  isSelected(option, value) {
    if (!value) {
      return false;
    }

    if (value.id) {
      value = value.id;
    }

    if (option.id) {
      return option.id == value;
    }

    return option == value;
  }

  get optionsList() {
    return this.options.map((a) => {
      let name = a.name ? a.name : a;

      return {
        name,
        value: a,
        selected: this.isSelected(a, this.value),
      };
    });
  }

  get stringValues() {
    let list = [];

    if (Array.isArray(this.value)) {
      list = this.value;
    } else {
      list = [this.value];
    }

    return list.map((a) => (typeof a == 'string' ? a : a.name));
  }

  get stringValue() {
    return this.stringValues.join(', ');
  }

  @action
  resetValue() {
    if (this.areOptionsVisible) {
      return;
    }

    this._value = null;
  }

  @action
  focusOption(element) {
    if (element.previousElementSibling) {
      return;
    }

    later(() => {
      element.focus();
      this.hasFocus = true;
    });
  }

  @action
  blurOption() {
    if (!document.hasFocus()) {
      return;
    }

    later(() => {
      if (
        !document.activeElement.classList.contains('list-group-item-action')
      ) {
        this.hasFocus = false;
        this.updateAreOptionsVisible();
      }
    }, 10);
  }

  updateAreOptionsVisible() {
    if (this.hasMouseOver || this.hasFocus) {
      return;
    }

    this.hideOptions();
  }

  raiseChange(newValue) {
    this._value = newValue;

    if (!this.args.onChange) {
      return;
    }

    this.args.onChange(newValue);
  }

  @action
  select(option) {
    this.raiseChange(option.value);
  }

  @action
  showOptions() {
    this.areOptionsVisible = true;

    if (this.args.onFocusIn) {
      this.args.onFocusIn();
    }
  }

  @action
  hideOptions() {
    this.areOptionsVisible = false;

    if (this.args.onFocusOut) {
      this.args.onFocusOut();
    }
  }
}
