import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  items: computed(
    'allIconNames',
    'definedAttributes',
    'otherAttributes',
    'parent',
    'undefinedAttributes',
    function () {
      let result = this.definedAttributes.slice();
      result = result.concat(this.undefinedAttributes);

      if (this.otherAttributes) {
        result.push(this.otherAttributes);
      }

      return result;
    }
  ),

  otherAttributes: computed(
    'allIconNames',
    'attributes',
    'parent',
    function () {
      if (this.parent || !this.attributes) {
        return null;
      }

      const values = this.extractValues(this.attributes, []).filter(
        (a) => this.allIconNames.indexOf(a.key) == -1
      );

      if (values.length == 0) {
        return null;
      }

      return {
        name: 'Other',
        stringPath: '',
        content: {
          objects: {},
          values,
        },
        path: [],
        type: 'attributes',
      };
    }
  ),

  path: computed('parent.path', function () {
    if (!this.parent) {
      return [];
    }

    return this.parent.path;
  }),

  allIconNames: computed('icons', function () {
    if (!this.icons) {
      return [];
    }

    return this.icons
      .map((a) => {
        if (!a.otherNames) {
          return [a.name];
        }

        return [a.name, ...a.otherNames];
      })
      .reduce((acc, x) => acc.concat(x), []);
  }),

  definedAttributes: computed(
    'attributes',
    'icons',
    'icons.@each.name',
    'parent',
    function () {
      const result = [];

      if (!this.icons) {
        return result;
      }

      this.icons.forEach((icon) => {
        let otherNames = [];
        const values = this.getValues([icon.name, ...otherNames]);

        if (!icon.allowMany && values.length == 0) {
          result.push(this.createRecord(icon.name, {}, null, 'newAttribute'));
        }

        values.forEach((value, index) => {
          const record = this.createRecord(
            icon.name,
            value,
            icon.allowMany ? index : null,
            'attribute'
          );

          result.push(record);
        });

        if (icon.allowMany) {
          result.push(
            this.createRecord(icon.name, {}, null, 'newArrayAttribute')
          );
        }
      });

      return result;
    }
  ),

  undefinedAttributes: computed(
    'attributes',
    'allIconNames',
    'parent',
    function () {
      const result = [];

      if (!this.attributes) {
        return result;
      }

      Object.keys(this.attributes)
        .filter((a) => this.allIconNames.indexOf(a) == -1)
        .forEach((name) => {
          const attribute = this.attributes[name];

          if (Array.isArray(attribute)) {
            attribute.forEach((element, index) => {
              if (!element) {
                return;
              }

              result.push(this.createRecord(name, element, index, 'attribute'));
            });

            result.push(this.createRecord(name, {}, null, 'newArrayAttribute'));
          } else if (this.hasNested(attribute)) {
            result.push(this.createRecord(name, attribute, null, 'attribute'));
          }
        });

      return result;
    }
  ),

  createRecord(name, allContent, index, type) {
    const path = this.path.slice();
    path.push(name);
    if (index != null) {
      path.push(index);
    }

    const stringPath = path.join('.');

    const content = {
      objects: this.extractObjects(allContent),
      values: this.extractValues(allContent, path),
    };

    return {
      name,
      stringPath,
      content,
      path,
      type,
    };
  },

  getValues(possibleNames) {
    if (!this.attributes) {
      return [];
    }

    const existingKeys = Object.keys(this.attributes).filter(
      (a) => possibleNames.indexOf(a) != -1
    );
    if (existingKeys.length == 0) {
      return [];
    }

    const attribute = this.attributes[existingKeys[0]];

    if (!attribute) {
      return [];
    }

    if (!Array.isArray(attribute)) {
      return [attribute];
    }

    return attribute;
  },

  hasNested(value) {
    if (typeof value !== 'object' || value === null) {
      return false;
    }

    return true;
  },

  extractValues(allContent, path) {
    if (!allContent) {
      return [];
    }

    return Object.keys(allContent)
      .filter((a) => !this.hasNested(allContent[a]))
      .map((key) => {
        return {
          key,
          value: allContent[key],
          path: [...path, key],
        };
      });
  },

  extractObjects(allContent) {
    const result = {};

    if (!allContent) {
      return result;
    }

    Object.keys(allContent)
      .filter((a) => this.hasNested(allContent[a]))
      .forEach((key) => {
        result[key] = allContent[key];
      });

    return result;
  },

  addArray(key, parent) {
    let stringPath = parent ? `${parent.key}.${key}` : key;
    const path = this.path.slice();
    path.push(key);

    return {
      name,
      stringPath,
      content: {
        objects: {},
        values: [],
      },
      path,
      type: 'newArrayItem',
    };
  },
});
