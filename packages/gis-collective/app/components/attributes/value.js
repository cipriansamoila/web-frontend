import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class AttributesValueComponent extends Component {
  @tracked hasFocus = false;
  @tracked _disabled = null;
  @tracked _tmpValue = null;
  @tracked _isLoading = false;

  click() {
    if (!this.disabled) {
      const elm = this.element.querySelector('input');

      if (!elm) {
        return;
      }

      elm.focus();
    }
  }

  get isLoading() {
    return this.args.isLoading || this._isLoading;
  }

  get disabled() {
    if (this._disabled === null) {
      return this.args.disabled;
    }

    return this._disabled;
  }

  set disabled(value) {
    this._disabled = value;

    if (this._disabled) {
      this._tmpValue = null;
    }
  }

  get tmpValue() {
    if (this._tmpValue === null) {
      return this.args.value;
    }

    return this._tmpValue;
  }

  set tmpValue(value) {
    this._tmpValue = value;
  }

  @action
  resetValue() {
    this._tmpValue = null;
  }

  @action
  focusIn() {
    later(() => {
      if (this.isDestroyed) return;
      this.hasFocus = true;
    });
  }

  @action
  focusOut() {
    later(() => {
      if (this.isDestroyed) return;
      this.hasFocus = false;
    });
  }

  @action
  longTextFocusOut() {
    later(() => {
      if (this.isDestroyed) return;
      this.hasFocus = false;
    });
  }

  @action
  async change(value) {
    switch (this.args.format) {
      case 'integer':
        this.tmpValue = parseInt(value);
        break;
      case 'decimal':
        this.tmpValue = parseFloat(value);
        break;
      default:
        this.tmpValue = value;
    }

    this._isLoading = true;
    try {
      await this.args.onChange?.(
        this.args.key || this.args.name,
        this.tmpValue
      );
    } catch (err) {
      // eslint-disable-next-line no-console
      console.warn(err);
    }
    this._isLoading = false;
  }

  @action
  changeInput(event) {
    return this.change(event.target.value);
  }
}
