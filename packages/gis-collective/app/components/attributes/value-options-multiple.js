import AttributesValueOptionsComponent from './value-options';
import { action } from '@ember/object';

export default class AttributesValueOptionsMultipleComponent extends AttributesValueOptionsComponent {
  selectMessage = 'select one or more values from the list';

  get value() {
    if (!this.args.value) {
      return [];
    }

    if (this.args.value.toArray) {
      return this.args.value.toArray();
    }

    return this.args.value;
  }

  get hasValues() {
    if (!Array.isArray(this.value)) {
      return false;
    }

    return this.value.length > 0;
  }

  isSelected(option, value) {
    let values = [];

    if (value) {
      values = this.value;
    }

    values = values.map((a) => {
      if (a.id) {
        return a.id;
      }

      return a;
    });

    if (option.id) {
      return values.filter((a) => a == option.id).length > 0;
    }

    return values.filter((a) => a == option).length > 0;
  }

  raiseChange(newValue) {
    if (!this.args.onChange) {
      return;
    }

    this.args.onChange(newValue);
  }

  removeValue(value) {
    let newValues = [];

    if (this.value) {
      newValues = this.value;
    }

    if (value.id) {
      newValues = newValues.filter((a) => a.id != value.id);
    }

    this.raiseChange(newValues);
  }

  addValue(value) {
    let newValues = [];

    if (this.args.value) {
      newValues.pushObjects(this.value);
    }

    newValues.push(value);

    this.raiseChange(newValues);
  }

  @action
  select(option) {
    if (option.selected) {
      return this.removeValue(option.value);
    }

    return this.addValue(option.value);
  }
}
