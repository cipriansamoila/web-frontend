import AttributesValueOptionsComponent from './value-options';

export default class AttributesValueMapListComponent extends AttributesValueOptionsComponent {
  emptyMessage = 'no maps available in this area.';
  selectMessage = 'click to select from list';
}
