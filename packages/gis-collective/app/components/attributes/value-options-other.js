import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class AttributesValueOptionsOtherComponent extends Component {
  @service intl;
  @tracked _optionsValue = null;
  @tracked _inputValue = null;

  get optionsList() {
    return this.args.options?.split(',') ?? [];
  }

  get optionsValue() {
    let selectedOption = this.optionsList.find((a) => a === this.args.value);

    if (this._optionsValue == null && selectedOption) {
      return selectedOption;
    }

    if (this._optionsValue == null && typeof this.args.value === 'string') {
      return this.otherOption;
    }

    return this._optionsValue;
  }

  get inputValue() {
    if (
      this._inputValue == null &&
      !this.optionsList.find((a) => a === this.args.value)
    ) {
      return this.args.value;
    }

    return this._inputValue;
  }

  get otherOption() {
    return this.intl.t('other-option');
  }

  get options() {
    const list = this.optionsList;

    return [...list, this.otherOption].join(',');
  }

  get isOther() {
    return this.optionsValue === this.otherOption;
  }

  @action
  handleOptionsChange(newValue) {
    this._optionsValue = newValue;
    let value = this._optionsValue;

    if (this.isOther) {
      value = '';
    }

    return this.args.onChange?.(value);
  }

  @action
  handleInput(key, value) {
    this._inputValue = value;
    return this.args.onChange?.(value);
  }
}
