import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class AttributesValueBooleanComponent extends Component {
  @tracked _value;
  @tracked hasFocus;

  get value() {
    if (typeof this._value != 'undefined') {
      return this._value;
    }

    return this.args.value;
  }

  updateValue(value) {
    this._value = value;

    if (this.args.onChange) {
      this.args.onChange(value);
    }
  }

  get isFalse() {
    if (this.isUnknown) {
      return false;
    }

    return this.value === false;
  }

  get isTrue() {
    if (this.isUnknown) {
      return false;
    }

    return this.value === true;
  }

  get isUnknown() {
    return typeof this.value == 'undefined';
  }

  @action
  setUnknown() {
    this.updateValue(undefined);
  }

  @action
  setTrue() {
    this.updateValue(true);
  }

  @action
  setFalse() {
    this.updateValue(false);
  }

  @action
  focusIn() {
    this.hasFocus = true;
  }

  @action
  focusOut() {
    this.hasFocus = false;
  }
}
