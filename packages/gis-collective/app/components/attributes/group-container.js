import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class AttributesGroupContainerComponent extends Component {
  @action
  onPositionDetailsChange(key, value) {
    this.args.onChangeAttribute(
      'position details',
      key.toLowerCase(),
      null,
      value
    );
  }

  @action
  onChange(key, value) {
    this.args.onChange(this.args.group, key, value);
  }

  get className() {
    if (!this.args.group || !this.args.group.key) {
      return 'unknown';
    }

    return this.args.group.key.replace(/[^0-9a-z]/gi, '-').toLowerCase();
  }
}
