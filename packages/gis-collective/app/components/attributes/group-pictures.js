import Component from '@glimmer/component';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { inject as service } from '@ember/service';
import PictureMeta from '../../lib/picture-meta';

export default class AttributesGroupPicturesComponent extends Component {
  elementId = 'pictures-attributes-' + guidFor(this);

  @service intl;
  @service store;

  get group() {
    return {
      key: 'pictures',
      name: this.intl.t('pictures'),
    };
  }

  @action
  change(newPictures) {
    this.args.onChange(this.group, 'pictures', newPictures);
  }

  @action
  createImage() {
    return this.store.createRecord('picture', {
      name: '',
      picture: '',
      meta: new PictureMeta(),
    });
  }

  @action
  optimizeChange(ev) {
    if (this.args.onChange) {
      this.args.onChange(this.group, 'pictureOptimization', ev.target.checked);
    }
  }
}
