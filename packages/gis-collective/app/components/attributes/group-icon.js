import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  tagName: '',

  icon: computed('icons', 'icons.@each.name', 'group', function () {
    if (!this.group || !this.icons || this.icons.length == 0) {
      return;
    }

    const lowerGroup = this.group.trim().toLowerCase();
    const result = this.icons.filter((a) => {
      const otherNames = a.otherNames ? a.otherNames : [];

      return (
        [a.name, ...otherNames]
          .map((a) => a.trim().toLowerCase())
          .indexOf(lowerGroup) != -1
      );
    });

    if (result.length == 0) {
      return;
    }

    return result[0];
  }),
});
