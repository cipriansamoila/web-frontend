import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { transform } from 'ol/proj';
import GeoJson from '../../lib/geoJson';
import PositionDetails from '../../lib/positionDetails';
import { later } from '@ember/runloop';

export default class AttributesGroupMapComponent extends Component {
  @service intl;
  @service fullscreen;
  @service mapStyles;

  @tracked editMode = false;
  @tracked _selectedBaseMap;

  get group() {
    return {
      key: 'location',
      name: this.intl.t('location'),
    };
  }

  get extentLabelPosition() {
    const extent = this.args.extent.toExtent();
    const center = this.args.extent.center.coordinates;

    center[1] = extent[3];
    return center;
  }

  get extentStyle() {
    return this.mapStyles.extent(this.mapStyleElement);
  }

  get baseMaps() {
    return this.args.defaultBaseMaps || [];
  }

  get selectedBaseMap() {
    if (this._selectedBaseMap) {
      return this._selectedBaseMap;
    }

    return this.args.baseMaps?.firstObject ?? null;
  }

  get hasValidPosition() {
    const coordinates = this.args.position?.coordinates;

    if (!coordinates?.length > 0) {
      return false;
    }

    return coordinates[0] && coordinates[1];
  }

  get mapExtent() {
    if (this.hasValidPosition) {
      const radius = 0.005;
      const position = this.args.position;
      const topRight = [
        position?.longitude - radius,
        position?.latitude - radius,
      ];
      const bottomLeft = [
        position?.longitude + radius,
        position?.latitude + radius,
      ];

      return topRight.concat(bottomLeft);
    }

    if (this.args.extent) {
      const geoJson = new GeoJson(this.args.extent);
      const extent = geoJson.toOlFeature().getGeometry().getExtent();

      return extent;
    }

    return [-114, -41, 42, 72];
  }

  get details() {
    return this.args.details ?? {};
  }

  get position() {
    return this.args.position ?? {};
  }

  @action
  updateCenter() {
    const center = transform(
      this.args.position?.coordinates ?? [0, 0],
      'EPSG:4326',
      'EPSG:3857'
    );

    this._mapView?.setCenter?.(center);
    this.updateMapSize();
  }

  @action
  edit() {
    this.editMode = true;
    this.fullscreen.isEnabled = true;

    this.prevPosition = new GeoJson(this.position);
    this.prevDetails = new PositionDetails(this.details);

    this.args.onChange(this.position, {
      ...this.details,
      type: 'manual',
    });
  }

  @action
  selectResult(result) {
    try {
      const feature = result.geometry.toOlFeature('EPSG:3857').getGeometry();
      this._mapView?.fit?.(feature);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }

    this.search(null);
  }

  @action
  selectBaseMap(value) {
    this._selectedBaseMap = value;
  }

  @action
  search(value) {
    return this.args.onSearch?.(value);
  }

  @action
  confirm() {
    if (this._mapView) {
      const center = transform(
        this._mapView.getCenter(),
        'EPSG:3857',
        'EPSG:4326'
      );

      this.args.onChange(
        new GeoJson({ type: 'Point', coordinates: center }),
        new PositionDetails({ ...this.details, type: 'manual' })
      );
    }

    this.editMode = false;
    this.fullscreen.isEnabled = false;

    this.updateMapSize();
  }

  @action
  dismiss() {
    this.editMode = false;
    this.fullscreen.isEnabled = false;

    this.args.onChange?.(this.prevPosition, this.prevDetails);

    later(() => {
      this.updateMapSize();
    }, 100);
  }

  @action
  updateMapSize() {
    this._map?.updateSize?.();
  }

  @action
  updatePosition(map, view) {
    this._map = map;
    this._mapView = view;
  }
}
