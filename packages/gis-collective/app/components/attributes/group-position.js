import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { transform } from 'ol/proj';
import GeoJSON from 'ol/format/GeoJSON';

export default class AttributesGroupPositionComponent extends Component {
  @service intl;
  @service position;
  @service fullscreen;

  @tracked showDetails = false;
  @tracked editMode = false;
  @tracked _selectedBaseMap;

  get hasPosition() {
    return !isNaN(this.longitude) && !isNaN(this.latitude);
  }

  get group() {
    return {
      key: 'position',
      name: this.intl.t('position'),
    };
  }

  get details() {
    if (this.args.positionDetails) {
      return this.args.positionDetails;
    }

    return {};
  }

  get capturedUsingGPS() {
    return this.getGroupAttribute('capturedUsingGPS') ?? false;
  }

  get longitude() {
    return this.args.position?.coordinates[0];
  }

  get latitude() {
    return this.args.position?.coordinates[1];
  }

  get altitude() {
    return this.getGroupAttribute('altitude');
  }

  get accuracy() {
    return this.getGroupAttribute('accuracy');
  }

  get altitudeAccuracy() {
    return this.getGroupAttribute('altitude accuracy');
  }

  get selectedBaseMap() {
    if (this._selectedBaseMap) {
      return this._selectedBaseMap;
    }

    if (this.baseMaps.length) {
      return this.baseMaps.firstObject;
    }

    return null;
  }

  get center() {
    return [this.longitude, this.latitude];
  }

  get mapExtent() {
    if (!this.hasInvalidPosition) {
      const radius = 0.005;
      const position = this.args.position;
      const topRight = [
        position?.longitude - radius,
        position?.latitude - radius,
      ];
      const bottomLeft = [
        position?.longitude + radius,
        position?.latitude + radius,
      ];

      return topRight.concat(bottomLeft);
    }

    if (this.args.extent) {
      const geoJson = new GeoJSON();
      const feature = geoJson.readFeature(this.args.extent);
      const extent = feature.getGeometry().getExtent();

      return extent;
    }

    return [-114, -41, 42, 72];
  }

  get hasInvalidPosition() {
    return (
      !this.args.position?.coordinates[0] || !this.args.position?.coordinates[1]
    );
  }

  get baseMaps() {
    return this.args.defaultBaseMaps || [];
  }

  _schedule(context, func, args) {
    return setTimeout(() => {
      this._currentlyExecutedFunction = this._schedule(context, func, args);
      func.apply(context, args);
    }, 1000);
  }

  @action
  localize() {
    return this.position.watchPosition();
  }

  @action
  changeMapView(map, view) {
    this._map = map;
    this._mapView = view;
  }

  @action
  setupPositionTimer() {
    this.position.watchPosition();
    this._currentlyExecutedFunction = this._schedule(this, this.updatePosition);
  }

  updatePosition() {
    if (!this.capturedUsingGPS || this.position.hasPosition === false) {
      return;
    }

    this.updateAttribute('longitude', this.position.longitude);
    this.updateAttribute('latitude', this.position.latitude);
    this.updateAttribute('altitude', this.position.altitude);
    this.updateAttribute('accuracy', this.position.accuracy);
    this.updateAttribute('altitude accuracy', this.position.altitudeAccuracy);
  }

  getGroupAttribute(key) {
    if (key == 'longitude') {
      return this.args.position.coordinates[0];
    }

    if (key == 'latitude') {
      return this.args.position.coordinates[1];
    }

    return this.details[key];
  }

  updateAttribute(key, value) {
    const currentValue = this.getGroupAttribute(key);

    if (
      !this.args.onChange ||
      currentValue == value ||
      (isNaN(currentValue) && isNaN(value))
    ) {
      return;
    }

    if (key.toLowerCase() == 'longitude') {
      return this.args.onChange(this.group, 'position', [
        parseFloat(value),
        parseFloat(this.getGroupAttribute('latitude')),
      ]);
    }

    if (key.toLowerCase() == 'latitude') {
      return this.args.onChange(this.group, 'position', [
        parseFloat(this.getGroupAttribute('longitude')),
        parseFloat(value),
      ]);
    }

    this.args.onChange(this.group, key, value);
  }

  willDestroy() {
    super.willDestroy(...arguments);
    clearTimeout(this._currentlyExecutedFunction);
  }

  @action
  selectBaseMap(value) {
    this._selectedBaseMap = value;
  }

  @action
  change(key, value) {
    this.updateAttribute(key, value);
  }

  @action
  confirm() {
    this.updateAttribute('capturedUsingGPS', false);

    if (this._mapView) {
      const center = transform(
        this._mapView.getCenter(),
        'EPSG:3857',
        'EPSG:4326'
      );
      this.updateAttribute('longitude', center[0]);
      this.updateAttribute('latitude', center[1]);
    }

    this.editMode = false;
    this.fullscreen.set('isEnabled', false);

    if (this._map) {
      this._map.updateSize();
    }
  }

  @action
  dismiss() {
    this.editMode = false;
    this.fullscreen.set('isEnabled', false);

    this.updateAttribute('capturedUsingGPS', this.prevCapturedUsingGpsAttr);

    if (this._map) {
      this._map.updateSize();
    }
  }

  @action
  edit() {
    this.prevCapturedUsingGpsAttr = this.capturedUsingGPS;
    this.updateAttribute('capturedUsingGPS', false);
    this.editMode = true;
    this.fullscreen.set('isEnabled', true);
  }

  @action
  updateMapSize() {
    this._map?.updateSize?.();
  }

  @action
  toggleDetails() {
    this.showDetails = !this.showDetails;
  }
}
