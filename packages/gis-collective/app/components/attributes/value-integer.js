import Component from '@glimmer/component';
import { action } from '@ember/object';
import { later } from '@ember/runloop';
import { tracked } from '@glimmer/tracking';

export default class AttributesValueIntegerComponent extends Component {
  @tracked _value = null;

  get value() {
    if (this._value != null) {
      return this._value;
    }

    return this.args.value;
  }

  set value(newValue) {
    this._value = newValue;

    if (isNaN(newValue)) {
      this.args.onChange?.();
      return;
    }

    let intValue = parseInt(newValue);

    if (!Number.isInteger(intValue)) {
      intValue = undefined;
    }

    this.args.onChange?.(intValue);
  }

  resetValue() {
    this._value = null;
  }

  @action
  updateValue() {
    if (this._value === null) {
      return;
    }

    if (this._value != this.args.value) {
      later(() => {
        this.resetValue();
      }, 5);
    }
  }

  @action
  validateValue(event) {
    if (event.key == '-' || event.key == '+') return;

    var value = parseInt(event.key);

    if (!Number.isInteger(value)) {
      event.preventDefault();
    }
  }
}
