import Component from '@glimmer/component';
import AttributesForm from '../../attributes';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class AttributesFormComponent extends Component {
  @service intl;
  @service position;

  @tracked oldFeatureId = null;
  @tracked _icons = null;
  @tracked attributes = null;

  @action
  setupFeature() {
    // eslint-disable-next-line ember/no-observers
    this.args.feature.removeObserver('attributes', this, 'attributesUpdated');
    // eslint-disable-next-line ember/no-observers
    this.args.feature.removeObserver('icons', this, 'attributesUpdated');
    // eslint-disable-next-line ember/no-observers
    this.args.feature.addObserver('attributes', this, 'attributesUpdated');
    // eslint-disable-next-line ember/no-observers
    this.args.feature.addObserver('icons', this, 'attributesUpdated');

    if (!this.attributes || this.oldFeatureId != this.args.feature.id) {
      this.attributes = new AttributesForm(this.intl, this.position);
      this.oldFeatureId = this.args.feature.id;
    }

    this.attributes.feature = this.args.feature;
  }

  willDestroy() {
    super.willDestroy(...arguments);
    if (this.args.feature) {
      this.args.feature.removeObserver('attributes', this, 'attributesUpdated');
    }
  }

  attributesUpdated() {
    this.attributes.feature = this.args.feature;
    this.attributes.updateGroups();
  }

  get positionDetails() {
    return this.args.feature?.attributes['position details'] ?? {};
  }

  get icons() {
    return this._icons;
  }

  set icons(value) {
    this._icons = value;

    if (!this.attributes) {
      this.attributes = new AttributesForm(this.intl, this.position);
    }

    this.attributes.icons = value;
  }

  get aboutGroup() {
    if (!this.attributes) {
      return null;
    }

    return this.attributes.about.groups[0];
  }

  get picturesGroup() {
    if (!this.attributes) {
      return null;
    }

    return this.attributes.pictures.groups[0];
  }

  get iconsGroup() {
    if (!this.attributes) {
      return null;
    }

    return this.attributes.iconGroups.groups[0];
  }

  get iconAttributes() {
    if (!this.attributes) {
      return null;
    }

    return this.attributes.attributeFactory.groups;
  }
}
