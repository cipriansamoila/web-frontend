import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class BrowsePlacesPreviewComponent extends Component {
  @action
  onClick() {
    return this.args.onClick?.(this.args.place);
  }
}
