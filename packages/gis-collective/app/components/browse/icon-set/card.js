import Component from '@glimmer/component';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class BrowseIconSetCardComponent extends Component {
  @service clickOutside;
  @tracked showingOptions = false;

  elementId = `icon-set-${guidFor(this)}`;

  get pictures() {
    if (!this.args.iconSet) {
      return [];
    }

    return [this.args.iconSet.cover];
  }

  @action
  setupOptions(element) {
    this.optionsElement = element;
  }

  @action
  showOptions() {
    this.clickOutside.subscribe(this.optionsElement, () => {
      this.hideOptions();
    });

    this.showingOptions = true;
  }

  @action
  hideOptions() {
    this.showingOptions = false;
  }

  @action
  linkRendered(element) {
    this.link = element;
  }

  @action
  click() {
    return this.link.click();
  }
}
