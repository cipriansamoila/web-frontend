import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class BrowseIconSuggestionsComponent extends Component {
  @action
  showAll() {
    return this.args.onShowAll?.();
  }

  @action
  selectIcon(icon) {
    return this.args.onSelectIcon?.(icon);
  }

  @action
  deselectIcon() {
    return this.args.onDeselectIcon?.();
  }
}
