import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';

export default class BrowseIconListComponent extends Component {
  @tracked _icons;

  get icons() {
    if (this.args.icons != this.oldIcons) {
      this._icons = null;
      this.oldIcons = this.args.icons;
    }

    if (!this.args.icons) {
      this._icons = [];
    }

    if (this._icons) {
      return this._icons;
    }

    if (this.args.icons && this.args.icons.then) {
      if (!this.args.waiting) {
        this.args.icons.then((result) => {
          this._icons = result;
        });

        return this.args.icons;
      }

      if (!this.args.icons.length) {
        return [];
      }

      let result = [];

      for (let i = 0; i < this.args.icons.length; i++) {
        result.push({});
      }

      return result;
    }

    return this.args.icons;
  }
}
