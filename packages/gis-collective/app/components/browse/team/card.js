import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class BrowseTeamCardComponent extends Component {
  get pictures() {
    if (!this.args.team) {
      return [];
    }

    return [this.args.team.logo];
  }

  get memberCount() {
    if (!this.args.team) {
      return 0;
    }

    return (
      this.args.team.owners.length +
      this.args.team.leaders.length +
      this.args.team.members.length
    );
  }

  get guestCount() {
    if (!this.args.team) {
      return 0;
    }

    return this.args.team.guests.length;
  }

  @action
  click() {
    if (!this.args.onClick) {
      return;
    }

    this.args.onClick(this.args.team.id);
  }
}
