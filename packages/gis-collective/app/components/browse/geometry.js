import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import Polygon from 'ol/geom/Polygon';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { getCenter, getWidth, getHeight } from 'ol/extent';
import { transformExtent } from 'ol/proj';
import { getGeometryExtent } from '../../lib/extents';
import GeoJson from '../../lib/geoJson';

export default class BrowseGeometryComponent extends Component {
  @service fullscreen;
  @service intl;
  @service mapStyles;

  @tracked _selectedBaseMap;
  @tracked viewExtent;
  @tracked isFullscreen;
  @tracked extentM;

  get type() {
    if (!this.args.value) {
      return 'Point';
    }

    return this.args.value.type;
  }

  get extentStyle() {
    return this.mapStyles.extent(this.mapStyleElement);
  }

  get featureStyle() {
    return (feature) => {
      const type = feature.getGeometry().getType();
      const properties = {};

      if (typeof this.args.icon?.get == 'function') {
        properties['icon'] = this.args.icon.get('id');
      } else {
        properties['icon'] = this.args.icon?.id;
      }

      if (type == 'Polygon' || type == 'MultiPolygon') {
        properties.type = 'polygon';
      }

      if (type == 'Polygon') {
        properties.type = 'polygon';
      }

      if (type == 'MultiLineString') {
        properties.type = 'line';
      }

      if (type == 'LineString') {
        properties.type = 'line';
      }

      if (type == 'Point') {
        properties.type = 'point';
      }

      return this.mapStyles.fromProperties(properties);
    };
  }

  get geometryCenter() {
    return {
      type: 'Point',
      coordinates: getCenter(this.extentM),
    };
  }

  get selectedBaseMap() {
    if (this._selectedBaseMap) {
      return this._selectedBaseMap;
    }

    if (this.args.baseMaps && this.args.baseMaps.length > 0) {
      return this.args.baseMaps.firstObject;
    }

    return null;
  }

  get isPoint() {
    return this.type == 'Point';
  }

  get jsonValue() {
    let value = this.args.value;

    if (typeof value == 'string') {
      try {
        value = JSON.parse(value);
        // eslint-disable-next-line no-empty
      } catch (err) {}
    }

    if (typeof value != 'object' || !this.args.value) {
      return null;
    }

    return JSON.parse(JSON.stringify(value));
  }

  get isLine() {
    return this.type == 'LineString';
  }

  get isMultiLine() {
    return this.type == 'MultiLineString';
  }

  get isPolygon() {
    return this.type == 'Polygon';
  }

  get isMultiPolygon() {
    return this.type == 'MultiPolygon';
  }

  get geometryExtent() {
    if (!this.args.value || !this.args.value.coordinates) {
      return null;
    }

    return getGeometryExtent(this.args.value);
  }

  get value() {
    return new GeoJson(this.args.value);
  }

  setupExtentM() {
    if (this.value.toOlFeature) {
      let extent = this.value?.toOlFeature?.().getGeometry().getExtent();

      if (getWidth(extent) < 0.0001 || getHeight(extent) < 0.0001) {
        const center = getCenter(extent);
        const delta = 0.005;
        extent = [
          center[0] - delta,
          center[1] - delta,
          center[0] + delta,
          center[1] + delta,
        ];
      }

      this.extentM = extent;
      return;
    }

    if (!this.args.extent || !this.args.extent.coordinates) {
      this.extentM = [-30, -30, 30, 30];
      return;
    }

    const polygon = new Polygon(this.args.extent.coordinates.slice());
    this.extentM = polygon.getExtent();
  }

  @action
  setup() {
    this.setupExtentM();
  }

  @action
  fullscreenRequest(value) {
    this.isFullscreen = value;
  }

  @action
  iconUpdate() {
    this.featureStyle.icon = this.args.icon;
  }

  @action
  mapSetup(map) {
    this.olMap = map;
  }

  @action
  changeExtent(value) {
    this.viewExtent = value;
  }

  @action
  positionFullscreen(value) {
    this.fullscreen.isEnabled = value;
  }

  @action
  selectBaseMap(value) {
    this._selectedBaseMap = value;
  }

  @action
  fitGeometry(map) {
    const extent = transformExtent(
      this.geometryExtent,
      'EPSG:4326',
      'EPSG:3857'
    );
    map.getView().fit(extent, { padding: [20, 20, 20, 20], duration: 1000 });
  }
}
