import Component from '@glimmer/component';

export default class BrowseAttributesTableComponent extends Component {
  get header() {
    if (!this.args.value || !this.args.value.length) {
      return [];
    }

    const result = this.args.value
      .map((a) => a.map((b) => b.displayName))
      .reduce((a, b) => a.concat(b), []);

    return result.uniq();
  }

  get rows() {
    if (!this.args.value || !this.args.value.length) {
      return [];
    }

    const result = [];
    this.args.value.forEach((attributeList, index) => {
      const values = this.header
        .map((displayName) =>
          attributeList.find((a) => a.displayName == displayName)
        )
        .map((a) => (a ? a.value : '---'));

      result.push({
        index: index + 1,
        values,
      });
    });

    return result;
  }
}
