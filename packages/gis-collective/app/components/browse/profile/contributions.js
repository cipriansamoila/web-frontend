import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';

export default class BrowseProfileContributionsComponent extends Component {
  squareSize = 18;

  get relativeDate() {
    if (this.args.relativeDate) {
      return this.args.relativeDate;
    }

    const date = new Date();
    date.setDate(date.getDate() + 1);

    return date;
  }

  get relativeWeek() {
    var date = new Date(this.relativeDate);
    date.setUTCHours(0, 0, 0, 0);

    // Thursday in current week decides the year.
    date.setUTCDate(date.getUTCDate() + 3 - ((date.getUTCDay() + 6) % 7));

    // January 4 is always in week 1.
    var week1 = new Date(date.getUTCFullYear(), 0, 4);

    // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    return (
      1 +
      Math.round(
        ((date.getTime() - week1.getTime()) / 86400000 -
          3 +
          ((week1.getUTCDay() + 6) % 7)) /
          7
      )
    );
  }

  get relativeYear() {
    return this.relativeDate.getFullUTCYear();
  }

  get currentDay() {
    let result = this.relativeDate.getDay();

    if (result == 0) {
      result = 7;
    }

    return result - 1;
  }

  get width() {
    return this.squareSize * 53;
  }

  get height() {
    return this.squareSize * 9;
  }

  get weeks() {
    const result = [];
    let startWeek = this.relativeWeek;

    for (let i = 0; i < 52; i++) {
      result.push(this.getWeek(startWeek));
      startWeek--;
    }

    return result;
  }

  get contributions() {
    if (!this.args.contributions || !this.args.contributions.forEach) {
      return [];
    }

    const map = {};

    this.args.contributions.forEach((contribution) => {
      const week = parseInt(contribution.week);
      const day = parseInt(contribution.day);
      const count = parseInt(contribution.count);

      if (!map[week]) {
        map[week] = {};
      }

      if (!map[week][day]) {
        map[week][day] = {
          count: 0,
          level: 0,
          x: this.getX(week),
          y: this.getY(day + 1),
          size: this.squareSize - 2,
        };
      }

      map[week][day].count += count;
      map[week][day].level = parseInt(map[week][day].count / 10) + 1;

      if (map[week][day].level > 5) {
        map[week][day].level = 5;
      }
    });

    return map;
  }

  get dayLabels() {
    return [
      {
        x: this.squareSize / 2,
        y: this.squareSize * 2 - this.squareSize * 0.2,
        label: 'days.letter.monday',
      },
      {
        x: this.squareSize / 2,
        y: this.squareSize * 4 - this.squareSize * 0.2,
        label: 'days.letter.wednesday',
      },
      {
        x: this.squareSize / 2,
        y: this.squareSize * 6 - this.squareSize * 0.2,
        label: 'days.letter.friday',
      },
      {
        x: this.squareSize / 2,
        y: this.squareSize * 8 - this.squareSize * 0.2,
        label: 'days.letter.sunday',
      },
    ];
  }

  get monthLabels() {
    const allMonths = [
      'month.short.february',
      'month.short.march',
      'month.short.april',
      'month.short.may',
      'month.short.june',
      'month.short.july',
      'month.short.august',
      'month.short.september',
      'month.short.october',
      'month.short.november',
      'month.short.december',
      'month.short.january',
    ];

    const result = [];

    const now = this.relativeDate;
    let x = this.width + this.squareSize / 2;
    let y = this.squareSize / 2 + 3;
    let currentMonth = now.getUTCMonth();

    for (let i = 52; i > 0; i--) {
      now.setDate(now.getDate() - 7);

      x -= this.squareSize;

      if (currentMonth != now.getUTCMonth()) {
        currentMonth = now.getUTCMonth();

        result.push({ y, x, label: allMonths[currentMonth] });
      }
    }

    return result;
  }

  getWeek(col) {
    const x = this.getX(col);
    const y = this.squareSize;

    let days = this.weekCoordinates(col);

    if (col == this.relativeWeek - 51) {
      days = days.slice(this.currentDay);
    }

    if (col == this.relativeWeek) {
      days = days.slice(0, this.currentDay);
    }

    return {
      index: col,
      transform: htmlSafe(`translate(${x}, ${y})`),
      days,
    };
  }

  weekCoordinates() {
    let result = [];

    for (let index = 0; index < 7; index++) {
      result.push({
        index,
        x: 0,
        y: this.getY(index),
        size: this.squareSize - 2,
      });
    }

    return result;
  }

  getY(index) {
    return index * this.squareSize;
  }

  getX(weekNumber) {
    const index = 52 - (this.relativeWeek - weekNumber);

    return index * this.squareSize;
  }
}
