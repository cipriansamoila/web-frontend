import Component from '@glimmer/component';
import config from '../../../config/environment';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';
import { inject as service } from '@ember/service';

export default class BrowseMapDownloadComponent extends Component {
  @service clickOutside;
  @service fastboot;
  @tracked isOpened;

  apiUrl = config.apiUrl;
  elementId = 'map-download-' + guidFor(this);

  get isDisabled() {
    return this.fastboot.isFastBoot;
  }

  @action
  setupOptions(element) {
    this.optionsElement = element;
  }

  @action
  showOptions() {
    this.clickOutside.subscribe(this.optionsElement, () => {
      this.isOpened = false;
    });

    this.isOpened = true;
  }
}
