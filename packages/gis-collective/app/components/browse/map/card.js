import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class BrowseMapCardComponent extends Component {
  @tracked isAdminView;
  @service clickOutside;

  get pictures() {
    if (!this.args.map || !this.args.map.hasCover) {
      return [];
    }

    return [this.args.map.squareCover, this.args.map.cover];
  }

  @action
  titleRendered(element) {
    this.titleElement = element;
  }

  @action
  click() {
    if (!this.titleElement) {
      return;
    }

    this.titleElement.click();
  }

  @action
  setupOptions(element) {
    this.optionsElement = element;
  }

  @action
  showAdmin() {
    this.clickOutside.subscribe(this.optionsElement, () => {
      this.hideAdmin();
    });

    this.isAdminView = true;
  }

  @action
  hideAdmin() {
    this.isAdminView = false;
  }
}
