import Component from '@glimmer/component';

export default class BrowseMapArticleComponent extends Component {
  get meta() {
    if (!this.args.map || !this.args.map.meta) {
      return [];
    }

    return Object.keys(this.args.map.meta).map((key) => {
      return { title: key, value: this.args.map.meta[key] };
    });
  }

  get contentBlocks() {
    if (!this.args.map) {
      return { blocks: [] };
    }

    const blocks =
      this.args.map.contentBlocks?.blocks.filter(
        (a) => !(a.type == 'header' && a.data.level == 1)
      ) ?? [];

    return { blocks };
  }

  get hasTeam() {
    try {
      return (
        this.args.map &&
        this.args.map.visibility &&
        this.args.map.visibility.team
      );
    } catch (err) {
      return false;
    }
  }
}
