import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';

export default class BrowseMapTitleComponent extends Component {
  get coverStyle() {
    const picture = this.args.map.coverPicture;

    return htmlSafe(`background-image: url(${picture}/lg);`);
  }

  get squareCoverStyle() {
    const picture = this.args.map.squareCoverPicture;

    return htmlSafe(`background-image: url(${picture}/lg);`);
  }
}
