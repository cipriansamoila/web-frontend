import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class BrowseMapOptionsComponent extends Component {
  @service embed;

  get linkTarget() {
    return this.embed.isEnabled ? '_blank' : '_self';
  }

  @action
  onDelete() {
    return this.args.onDelete(this.args.map);
  }
}
