import Component from '@glimmer/component';
import { action } from '@ember/object';
import { htmlSafe } from '@ember/template';

export default class BrowseMapPreviewComponent extends Component {
  get style() {
    if (!this.args.map.hasCover) {
      return htmlSafe('');
    }

    const picture = this.args.map.coverPicture;

    if (!picture) {
      return htmlSafe('');
    }

    return htmlSafe(`background-image: url('${picture}/sm')`);
  }

  @action
  handleClick() {
    if (this.args.onClick) {
      return this.args.onClick(this.args.map);
    }
  }
}
