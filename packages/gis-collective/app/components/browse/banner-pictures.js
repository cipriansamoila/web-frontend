import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import create360Viewer from '360-image-viewer';
import canvasFit from 'canvas-fit';
import { later } from '@ember/runloop';

export default class BrowseBannerPicturesComponent extends Component {
  @tracked index360;
  @tracked isFullscreen;
  @tracked superDuperSwiper;

  get is360() {
    return !isNaN(this.index360);
  }

  get pictures() {
    return this.args.pictures?.map((a) => this.preparePicture(a));
  }

  preparePicture(picture) {
    return {
      is360: picture.is360,
      style: htmlSafe(`background-image: url(${picture.picture}/lg);`),
    };
  }

  @action
  create360Viewer(element) {
    const picture = this.args.pictures.objectAt(this.index360);

    const image = new Image();
    const resizedImage = new Image();
    image.crossOrigin = 'Anonymous';
    image.src = picture.picture;

    image.onload = () => {
      try {
        const viewer = create360Viewer({
          canvas: element,
          fov: 90 * (Math.PI / 180),
          rotateSpeed: -0.15,
          damping: 0.3,
        });

        const w = image.naturalWidth;
        const h = image.naturalHeight;

        const canvas = document.createElement('canvas');
        document.querySelector('body').appendChild(canvas);

        canvas.width = 4000;
        canvas.height = 4000;
        const ctx = canvas.getContext('2d');
        ctx.drawImage(image, 0, 0, w, h, 0, 0, canvas.width, canvas.height);

        resizedImage.src = canvas.toDataURL('image/jpeg', 1);

        resizedImage.onload = () => {
          viewer.texture(resizedImage);
          this.fit = canvasFit(viewer.canvas, window, window.devicePixelRatio);
          window.addEventListener('resize', this.fit, false);
          this.fit();

          viewer.start();
        };
      } catch (err) {
        alert(err);
      }
    };
  }

  @action
  fullscreenRequest(isEnabled) {
    this.isFullscreen = isEnabled;
  }

  @action
  fullScreen(isEnabled) {
    if (this.superDuperSwiper && this.superDuperSwiper.update) {
      later(() => {
        this.superDuperSwiper();
      }, 500);
    }

    if (!isEnabled) {
      this.toggle360();
      window.removeEventListener('resize', this.fit, false);
    }
  }

  @action
  toggle360(index) {
    this.index360 = index;

    if (this.isFullscreen != this.is360) {
      this.isFullscreen = this.is360;
    }
  }
}
