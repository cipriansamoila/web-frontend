import Component from '@glimmer/component';

export default class BrowseAttributesComponent extends Component {
  get value() {
    if (!this.args.value || !this.args.value.length) {
      return [];
    }

    return this.args.value
      .filter((a) => a.isPresent || a.value)
      .map((a) => {
        if (
          typeof a.value == 'string' &&
          (a.value.indexOf('http:') == 0 || a.value.indexOf('https:') == 0)
        ) {
          a.type = 'link';
        }

        return a;
      });
  }

  get hasIndex() {
    return typeof this.args.index == 'number';
  }

  get index() {
    return this.args.index + 1;
  }
}
