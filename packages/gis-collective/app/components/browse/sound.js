import Component from '@glimmer/component';
import { action } from '@ember/object';
import { later } from '@ember/runloop';
import { tracked } from '@glimmer/tracking';

export default class BrowseSoundComponent extends Component {
  @tracked isPlaying;
  @tracked isLoading;
  @tracked isError;

  @action
  setupAudio(element) {
    this.audioElement = element;

    this.audioElement.addEventListener('play', () => {
      this.isPlaying = true;
    });

    this.audioElement.addEventListener('pause', () => {
      this.isPlaying = false;
    });

    this.audioElement.addEventListener('playing', () => {
      this.isLoading = false;
      this.isError = false;
    });

    this.audioElement.addEventListener('error', () => {
      this.isError = true;
    });
  }

  @action
  pause() {
    this.audioElement.pause();
  }

  @action
  play() {
    this.audioElement.load();
    this.isLoading = true;

    later(() => {
      this.audioElement.volume = 1;
      this.audioElement.play();
    });
  }
}
