import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class BrowseCampaignOptionsComponent extends Component {
  @action
  onDelete() {
    return this.args.onDelete(this.args.campaign);
  }
}
