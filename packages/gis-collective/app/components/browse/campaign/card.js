import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class BrowseCampaignCardComponent extends Component {
  @service clickOutside;
  @tracked isAdminView;

  get style() {
    if (!this.picture) {
      return htmlSafe('');
    }

    return htmlSafe(`background-image: url('${this.picture}/md')`);
  }

  get picture() {
    if (this.args.campaign?.get) {
      return this.args.campaign?.get?.('cover.picture');
    }

    return this.args.campaign?.cover?.picture;
  }

  @action
  linkSetup(element) {
    this.link = element;
  }

  @action
  transitionToCampaign() {
    return this.link.click();
  }

  @action
  setupOptions(element) {
    this.optionsElement = element;
  }

  @action
  showAdmin() {
    this.clickOutside.subscribe(this.optionsElement, () => {
      this.hideAdmin();
    });

    this.isAdminView = true;
  }

  @action
  hideAdmin() {
    this.isAdminView = false;
  }
}
