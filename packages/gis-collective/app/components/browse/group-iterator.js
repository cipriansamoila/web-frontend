import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class BrowseGroupIteratorComponent extends Component {
  @tracked list = [];
  collapseList = [];

  get key() {
    return this.cssId(this.args.key);
  }

  get itemList() {
    if (
      this.args.items &&
      Array.isArray(this.args.items.icons) &&
      this.args.items.icons.length > 0
    ) {
      return this.args.items.icons;
    }

    return null;
  }

  getTmpCategories() {
    const list = [];

    if (
      this.args.items.categories &&
      Object.keys(this.args.items.categories).length > 0
    ) {
      Object.keys(this.args.items.categories).forEach((a) => {
        const key = this.args.key ? this.args.key + '.' + a : a;

        list.push(
          new Item(
            this.cssId(key),
            a,
            key,
            null,
            this.args.items.categories[a],
            true
          )
        );
      });
    }

    return list;
  }

  @action
  update() {
    if (!this.args.items) {
      return;
    }

    const tmpList = this.getTmpCategories();

    const newIds = tmpList.map((a) => a.id);

    this.list.forEach((item) => {
      item.isVisible = newIds.indexOf(item.id) != -1;
    });

    const existingIds = this.list.map((a) => a.id);

    tmpList.forEach((item) => {
      const index = existingIds.indexOf(item.id);

      if (index == -1) {
        this.list.pushObject(item);
        return;
      }

      this.list[index].list = item.list;
      this.list[index].obj = item.obj;
      this.list[index].isVisible = item.isVisible;
    });
  }

  cssId(value) {
    if (!value) {
      return 'unknown';
    }

    return value.replace(/[^0-9a-z]/gi, '-').toLowerCase();
  }
}

class Item {
  @tracked id;
  @tracked name;
  @tracked key;
  @tracked list;
  @tracked obj;
  @tracked isVisible;

  constructor(id, name, key, list, obj, isVisible) {
    this.id = id;
    this.name = name;
    this.key = key;
    this.list = list;
    this.obj = obj;
    this.isVisible = isVisible;
  }
}
