import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class BrowseUserInfoComponent extends Component {
  @service store;
  @tracked _user;

  get name() {
    if (this.user?.username) {
      return this.user.username;
    }

    if (this.user?.name) {
      return this.user.name;
    }

    if (this._user?.name) {
      return this._user.name;
    }

    return 'loading...';
  }

  get user() {
    if (!this.args.userId) {
      return {
        name: '@unknown',
      };
    }

    if (this.args.userId.indexOf('@') != -1) {
      return {
        name: this.args.userId,
      };
    }

    if (this._user && this.args.userId == this._user.id) {
      return this._user;
    }

    if (this._user?.name == '@unknown') {
      return this._user;
    }

    this.store
      .findRecord('user', this.args.userId)
      .then((result) => {
        this._user = result;
      })
      .catch(() => {
        this._user = {
          name: '@unknown',
        };
      });

    return {
      name: '@unknown',
    };
  }
}
