import Component from '@glimmer/component';

export default class BrowseTableComponent extends Component {
  get header() {
    if (!this.args.value || !Array.isArray(this.args.value)) {
      return [];
    }

    const result = [];

    this.args.value
      .filter((a) => a && typeof a === 'object')
      .forEach((row) => {
        result.addObjects(
          Object.keys(row).filter((a) => result.indexOf(a) == -1)
        );
      });

    return result;
  }

  get rows() {
    const header = this.header;

    return this.args.value
      .filter((a) => a && typeof a === 'object')
      .map((value, index) => {
        const values = header.map((key) =>
          value[key] === undefined ? '-' : value[key]
        );

        return {
          index: index + 1,
          values,
        };
      });
  }
}
