import Component from '@glimmer/component';
import { inject as service } from '@ember/service';

export default class BrowsePrimaryPictureComponent extends Component {
  @service fastboot;

  get pictures() {
    if (!this.args.pictures || this.args.pictures.length == 0) {
      return [];
    }

    if (this.args.pictures.toArray) {
      return this.args.pictures.toArray();
    }

    return this.args.pictures;
  }

  get primaryImage() {
    if (this.pictures.length == 0) {
      return null;
    }

    return this.pictures[0];
  }
}
