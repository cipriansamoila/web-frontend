import Component from '@glimmer/component';
import { toContentBlocks } from '../../../lib/content-blocks';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import displayIconAttributes from '../../../lib/display-icon-attributes';
import AlertMessage from '../../../lib/alert-message';
import { inject as service } from '@ember/service';

export default class BrowseSitePreviewImportComponent extends Component {
  @service intl;

  @tracked maps;
  @tracked contributors;
  @tracked icons;
  @tracked pictures;
  @tracked externalPictures;
  @tracked sounds;
  @tracked externalSounds;
  @tracked originalAuthor;
  @tracked author;
  @tracked createdOn;
  @tracked attributes;
  @tracked feature;

  get description() {
    return this.record.description ?? '';
  }

  get name() {
    return this.record.name ?? '';
  }

  get descriptionBlocks() {
    return toContentBlocks(this.description, this.name);
  }

  get position() {
    return this.record.position;
  }

  get visibility() {
    const value = this.record?.visibility;

    if (value == -1) {
      return `visibility-pending`;
    }

    if (value == 1) {
      return `visibility-public`;
    }

    return `visibility-private`;
  }

  get record() {
    return this.args.value?.result ?? {};
  }

  get alertList() {
    const list = this.args.value?.errors.map((a, i) => {
      const message = this.intl.t('preview-import.alert', {
        key: a.key,
        destination: a.destination,
        error: a.error,
      });
      return new AlertMessage(message, 'warning', `error-${i}`);
    });

    if (this.args.value?.validationError) {
      const error = this.args.value?.validationError;

      const options = {};
      if (error.vars) {
        options.vars = error.vars.join(', ');
      }

      const message = this.intl.t(`preview-import.${error.code}`, options);
      list.push(new AlertMessage(message, 'danger', `error-validation`));
    }

    return list;
  }

  @action
  async setupRelations() {
    const mapIds = this.record.maps ?? [];
    const contributorIds = this.record.contributors ?? [];
    const iconIds = this.record.icons ?? [];
    const pictureIds = this.record.pictures ?? [];
    const soundIds = this.record.sounds ?? [];
    const isLink = (a) =>
      a.toLowerCase().indexOf('http://') == 0 ||
      a.toLowerCase().indexOf('https://') == 0;

    this.feature = null;
    this.author = null;
    this.originalAuthor = null;

    if (this.record._id) {
      this.feature = this.args.queryItem?.('feature', this.record._id);
    }

    if (this.record.info?.author) {
      this.author = this.args.queryItem?.(
        'user-profile',
        this.record.info?.author
      );
    }

    if (this.record.info?.originalAuthor) {
      this.originalAuthor = this.args.queryItem?.(
        'user-profile',
        this.record.info?.originalAuthor
      );
    }

    this.createdOn = this.record.info?.createdOn;

    this.maps = mapIds
      .map((a) => this.args.queryItem?.('map', a))
      .filter((a) => a);

    this.contributors = contributorIds
      .map((a) => this.args.queryItem?.('user-profile', a))
      .filter((a) => a);

    this.icons = iconIds
      .map((a) => this.args.queryItem?.('icon', a))
      .filter((a) => a);

    await Promise.all(this.icons);

    this.pictures = pictureIds
      .filter((a) => typeof a == 'string')
      .filter((a) => !isLink(a))
      .map((a) => this.args.queryItem?.('picture', a));

    this.externalPictures = pictureIds
      .filter((a) => typeof a == 'string')
      .filter(isLink);

    this.sounds = soundIds
      .filter((a) => typeof a == 'string')
      .filter((a) => !isLink(a))
      .map((a) => this.args.queryItem?.('sound', a));

    this.externalSounds = soundIds
      .filter((a) => typeof a == 'string')
      .filter((a) => isLink(a));

    this.attributes = displayIconAttributes(this.record.attributes, this.icons);
  }
}
