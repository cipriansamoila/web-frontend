import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class BrowseSiteCoverComponent extends Component {
  @tracked isLoaded;

  get route() {
    return this.args.route ?? 'browse.sites.site';
  }

  get picture() {
    return this.args.feature?.pictures?.firstObject;
  }

  get pictureUrl() {
    if (!this.hasPicture) {
      return '';
    }

    return `${this.picture.picture}/md`;
  }

  get hashStyle() {
    if (this.hasPicture) {
      return htmlSafe(this.picture.hashBg ?? '');
    }

    return htmlSafe(this.args.feature?.hashBg ?? '');
  }

  get style() {
    if (!this.hasPicture) {
      return htmlSafe('');
    }

    return htmlSafe(`background-image: url('${this.pictureUrl}')`);
  }

  get hasPicture() {
    return !!this.picture;
  }

  @action
  linkRendered(element) {
    this.link = element;
  }

  @action
  transitionToSite(event) {
    event.preventDefault();

    if (!this.args.onClick) {
      this.link.click();
      return;
    }

    if (this.args.onClick) {
      later(() => {
        this.args.onClick?.(this.args.feature);
      });
    }

    return false;
  }

  @action
  loaded() {
    this.isLoaded = true;
  }
}
