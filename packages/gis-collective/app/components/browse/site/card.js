import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class BrowseSiteCardComponent extends Component {
  @service clickOutside;
  @service position;

  @tracked isAdminView = false;

  @action
  setupOptions(element) {
    this.optionsElement = element;
  }

  @action
  showAdmin() {
    this.clickOutside.subscribe(this.optionsElement, () => {
      this.hideAdmin();
    });

    this.isAdminView = true;
  }

  @action
  hideAdmin() {
    this.isAdminView = false;
  }

  @action
  deleteItem() {
    this.args.onDelete?.(this.args.feature);
  }
}
