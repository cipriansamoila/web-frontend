import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class UserAccountFormComponent extends Component {
  @tracked _joinedTime;
  @tracked _email;
  @tracked _userName;

  get hasUpdate() {
    return this._joinedTime || this._email || this._userName;
  }

  get joinedTime() {
    if (this._joinedTime) {
      return this._joinedTime;
    }

    return this.args.profile?.joinedTime;
  }

  get email() {
    if (this._email) {
      return this._email;
    }

    return this.args.user?.email;
  }

  set email(value) {
    this._email = value;
  }

  get userName() {
    if (this._userName) {
      return this._userName;
    }

    return this.args.user?.username;
  }

  set userName(value) {
    this._userName = value;
  }

  @action
  changeJoinedTime(value) {
    this._joinedTime = value;
  }

  @action
  async update() {
    await this.args.onUpdate?.(this.joinedTime, this.email, this.userName);

    this._joinedTime = null;
    this._email = null;
    this._userName = null;

    return true;
  }
}
