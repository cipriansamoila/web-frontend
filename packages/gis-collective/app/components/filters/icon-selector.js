import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class FiltersIconSelectorComponent extends Component {
  cache = {};
  @tracked list = [];

  get icons() {
    if (!this.args.icons || !Array.isArray(this.args.icons)) {
      return [];
    }

    return this.args.icons;
  }

  get emptyList() {
    const result = [];

    for (let i = 0; i <= this.list.length / 4; i++) {
      result.push({ index: i });
    }

    return result;
  }

  @action
  update() {
    const visibleIds = this.icons.map((a) => a.id || a._id);
    const selectedIds = this.args.value
      ? this.args.value.map((a) => a.id || a._id)
      : [];

    this.icons.forEach((icon) => {
      const id = icon.id || icon._id;

      if (!this.cache[id]) {
        this.cache[id] = new Item(icon);
        this.list.pushObject(this.cache[id]);
      }
    });

    this.list.forEach((a) => {
      const id = a.icon.id || a.icon._id;

      a.isVisible = visibleIds.indexOf(id) != -1;
      a.isSelected = selectedIds.indexOf(id) != -1;
    });
  }

  isSelected(icon) {
    if (!this.args.value || this.args.value.length == 0) {
      return false;
    }

    return this.args.value.filter((a) => a.id == icon.id).length > 0;
  }

  @action
  toggle(item) {
    const isSelected = this.isSelected(item.icon);

    if (isSelected && this.args.onDeselect) {
      this.args.onDeselect(item.icon);
    }

    if (!isSelected && this.args.onSelect) {
      this.args.onSelect(item.icon);
    }
  }
}

class Item {
  @tracked isSelected = false;
  @tracked isVisible = true;
  @tracked icon;

  constructor(icon) {
    this.icon = icon;
  }
}
