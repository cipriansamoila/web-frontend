import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { later } from '@ember/runloop';

export default Component.extend({
  store: service(),
  classNames: ['popup-filter', 'list-filter'],
  isFilterVisible: false,
  cls: 'btn-outline-primary',
  clsValue: 'btn-primary',

  didInsertElement() {
    this._super(...arguments);
    this.set('hideOnBlur', (e) => {
      if (
        e.relatedTarget &&
        e.relatedTarget.classList.contains('list-group-item')
      ) {
        return;
      }

      if (this.isFilterVisible) {
        later(() => {
          this.set('isFilterVisible', false);
        }, 10);
      }
    });

    this.element
      .querySelector('.btn-pill')
      .addEventListener('blur', this.hideOnBlur, false);
  },

  willRemoveElement() {
    this.element
      .querySelector('.btn-pill')
      .removeEventListener('blur', this.hideOnBlur);
  },

  actions: {
    toggleVisible() {
      this.set('isFilterVisible', !this.isFilterVisible);
    },

    reset() {
      if (!this.onSelect) {
        return;
      }

      this.onSelect();
    },

    select(value) {
      this.set('isFilterVisible', false);

      if (!this.onSelect) {
        return;
      }

      if (value == this.value) {
        this.onSelect();
      } else {
        this.onSelect(value);
      }
    },
  },
});
