import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { later } from '@ember/runloop';
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ['popup-filter', 'icon-category-filter'],
  store: service(),
  isFilterVisible: false,
  cls: 'btn-outline-primary',
  clsValue: 'btn-primary',

  didInsertElement() {
    this._super(...arguments);
    this.set('hideOnBlur', (e) => {
      if (
        e.relatedTarget &&
        e.relatedTarget.classList.contains('list-group-item')
      ) {
        return;
      }

      if (this.isFilterVisible) {
        later(() => {
          this.set('isFilterVisible', false);
        }, 10);
      }
    });

    this.element
      .querySelector('.btn-pill')
      .addEventListener('blur', this.hideOnBlur, false);
  },

  willRemoveElement() {
    this.element
      .querySelector('.btn-select-icon-category')
      .removeEventListener('blur', this.hideOnBlur);
  },

  iconSetName: computed('iconSet', 'iconSets', function () {
    const list = this.iconSets.filter((a) => a.id == this.iconSet);

    if (list.length == 0) {
      return '';
    }

    return list[0].name;
  }),

  actions: {
    show() {
      this.set('isFilterVisible', !this.isFilterVisible);
    },

    reset() {
      if (!this.onSelect) {
        return;
      }

      this.onSelect();
    },

    select(iconSet, category) {
      this.set('isFilterVisible', false);

      if (!this.onSelect) {
        return;
      }

      if (iconSet == this.iconSet && category == this.category) {
        this.onSelect();
      } else {
        this.onSelect(iconSet, category);
      }
    },
  },
});
