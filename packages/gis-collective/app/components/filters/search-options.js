import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { debounce } from '@ember/runloop';
import { inject as service } from '@ember/service';
import { guidFor } from '@ember/object/internals';

export default class FiltersSearchOptionsComponent extends Component {
  elementId = `search-options-${guidFor(this)}`;

  @service clickOutside;

  @tracked showSuggestions = false;
  @tracked isSearching = false;
  @tracked _value = null;

  get value() {
    if (this._value === '') {
      return '';
    }

    return this._value || this.args.value;
  }

  set value(newValue) {
    this._value = newValue;
    debounce(this, this.search, 500);
  }

  get title() {
    return this.args.title || 'not set';
  }

  @action
  handleKeyPress(event) {
    if (event.key == 'Escape' || event.keyCode == 27) {
      return this.hide();
    }

    if (event.key == 'Enter' || event.keyCode == 13) {
      this.search();

      if (this.args.onSelect) {
        this.args.onSelect(this.value);
      }

      return this.hide();
    }
  }

  @action
  reset() {
    this.hide();

    if (this.args.onReset) {
      this.args.onReset();
    }
  }

  @action
  setup(element) {
    this.element = element;
  }

  @action
  hide() {
    if (!this.showSuggestions) {
      return;
    }

    this.showSuggestions = false;
    this._value = null;

    if (this.args.onHide) {
      return this.args.onHide();
    }
  }

  show() {
    if (this.showSuggestions) {
      return;
    }

    this.clickOutside.subscribe(this.element, () => {
      this.showSuggestions = false;
    });

    this.showSuggestions = true;

    return this.search();
  }

  @action
  toggleVisibility() {
    if (this.showSuggestions) {
      return this.hide();
    }

    return this.show();
  }

  search() {
    if (!this.args.onSearch) {
      return;
    }

    const result = this.args.onSearch(this.value);

    if (result && result.then) {
      this.isSearching = true;

      result.then(() => {
        this.isSearching = false;
      });
    }

    if (result && result.catch) {
      this.isSearching = true;

      result.catch(() => {
        this.isSearching = false;
      });
    }

    return result;
  }
}
