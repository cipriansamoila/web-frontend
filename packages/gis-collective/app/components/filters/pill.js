import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class FiltersPillComponent extends Component {
  get hasValue() {
    if (!this.args.value) {
      return false;
    }

    if (this.args.value.trim() == '') {
      return false;
    }

    return true;
  }

  get className() {
    if (!this.args.title || !this.args.title) {
      return 'unknown';
    }

    return this.args.title.replace(/[^0-9a-z]/gi, '-').toLowerCase();
  }

  get title() {
    return this.args.title || 'no title';
  }

  get displayValue() {
    return this.hasValue ? this.args.value : this.title;
  }

  @action
  reset() {
    if (this.args.onReset) {
      this.args.onReset();
    }
  }

  @action
  select() {
    if (this.args.onSelect) {
      this.args.onSelect();
    }
  }
}
