import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { Modal } from 'bootstrap';
import { inject as service } from '@ember/service';

export default class FiltersIconsComponent extends Component {
  @tracked modalVisible = false;
  @tracked icons = {};
  @tracked promise;
  @tracked isLoading;
  @tracked destinationElement;

  @service fastboot;
  @service intl;

  get label() {
    if (!this.args.icon) {
      return '';
    }
    if (this.args.icon.localName) {
      return this.intl.t('with icon', { name: this.args.icon.localName });
    }

    if (this.args.icon.name) {
      return this.intl.t('with icon', { name: this.args.icon.name });
    }

    return '';
  }

  get iconsQuery() {
    const params = {};

    if (this.args.iconSets && this.args.iconSets.length) {
      params['iconSet'] = this.args.iconSets.join(',');
    }

    return params;
  }

  willDestroy() {
    super.willDestroy();
    this.modal?.dispose?.();
  }

  @action
  close() {
    this.modal.hide();
  }

  @action
  setupModal(element) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    this.modal = new Modal(element, { show: false });

    element.addEventListener('hidden.bs.modal', function () {
      this.modalVisible = false;
    });
  }

  @action
  reset() {
    if (this.args.onChange) {
      return this.args.onChange(null);
    }
  }

  @action
  selectionChanged(value) {
    this.modal.hide();

    if (!value || !value.length) {
      return;
    }

    if (this.args.onChange) {
      return this.args.onChange(value[0]);
    }
  }

  @action prepareModal() {
    this.destinationElement = document.querySelector('#modal-target');
  }

  @action
  openModal() {
    this.selection = this.args.value ?? [];
    this.modalVisible = true;

    this.modal.show();

    this.isLoading = true;
    this.promise = this.args.adapter.group(this.iconsQuery).then((result) => {
      this.isLoading = false;
      this.icons = result;
    });
  }
}
