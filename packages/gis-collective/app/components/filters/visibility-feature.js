import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class FiltersVisibilityFeatureComponent extends Component {
  @service intl;
  @service clickOutside;
  @tracked showSuggestions = false;

  get isPublished() {
    return this.args.value == 1;
  }

  get isUnpublished() {
    return this.args.value == '0';
  }

  get isPending() {
    return this.args.value == -1;
  }

  get niceValue() {
    if (this.isPublished) {
      return this.intl.t('only published');
    }

    if (this.isPending) {
      return this.intl.t('only pending');
    }

    if (this.isUnpublished) {
      return this.intl.t('only unpublished');
    }

    return null;
  }

  @action
  setup(element) {
    this.element = element;
  }

  @action
  hide() {
    if (this.showSuggestions) {
      this.showSuggestions = false;
    }
  }

  @action
  toggleVisibility() {
    this.showSuggestions = !this.showSuggestions;

    if (this.showSuggestions) {
      this.clickOutside.subscribe(this.element, () => {
        this.showSuggestions = false;
      });
    }
  }

  @action
  change(newValue) {
    if (this.args.onChange) {
      this.args.onChange(newValue);
    }
  }

  @action
  reset() {
    if (this.args.onChange) {
      this.args.onChange(null);
    }
  }
}
