import Component from '@glimmer/component';
import { action } from '@ember/object';
import { defaultBsProperty, sizedBsProperty } from '../lib/bs-cols';

export default class BsColWidthsComponent extends Component {
  get options() {
    return this.args.options ?? [];
  }

  get tabletSize() {
    const v = sizedBsProperty('col', 'md', this.options);

    if (isNaN(v)) {
      return '-';
    }

    return v;
  }

  get desktopSize() {
    const v = sizedBsProperty('col', 'lg', this.options);

    if (isNaN(v)) {
      return '-';
    }

    return v;
  }

  get mobileSize() {
    const v = defaultBsProperty('col', this.options);

    if (isNaN(v)) {
      return '-';
    }

    return v;
  }

  @action
  click() {
    this.args.onClick?.(...arguments);
  }
}
