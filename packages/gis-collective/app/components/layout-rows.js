import Component from '@glimmer/component';
import { PageCol } from '../transforms/page-col-list';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class LayoutComponent extends Component {
  get rows() {
    let result = [];

    if (!this.args.layout?.length) {
      return [];
    }

    if (this.args.layout.length < result.length) {
      result.removeAt(
        this.args.layout.length - 1,
        result.length - this.args.layout.length
      );
    }

    this.args.layout?.forEach((row, rowIndex) => {
      const cols = row.cols.map((col, i) => this.getValue(rowIndex, i, col));

      if (!result[rowIndex]) {
        result.push(new RowData(cols, row, rowIndex));
      } else {
        result[rowIndex].cols = cols;
      }
    });

    return result;
  }

  getValue(rowIndex, colIndex, col) {
    let value = this.args.cols?.find(
      (item) =>
        (item.container ?? 0) == (this.args.container ?? 0) &&
        item.row == rowIndex &&
        item.col == colIndex
    );

    if (!value) {
      value = new PageCol({
        container: this.args.container,
        col: colIndex,
        row: rowIndex,
        cls: col.cls,
      });
    }

    return value;
  }

  @action
  select(col) {
    if (!this.args.selectable) {
      return;
    }

    this.args.onSelect?.(col);
  }
}

class RowData {
  @tracked index;
  @tracked cols = [];
  @tracked layout = [];
  @tracked cls = '';

  constructor(cols, row, index) {
    this.cols = cols;
    this.layout = row.cols;
    this.cls = row.cls;
    this.index = index;
  }
}
