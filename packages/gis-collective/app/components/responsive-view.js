import Component from '@glimmer/component';
import { later, cancel } from '@ember/runloop';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class ResponsiveViewComponent extends Component {
  @tracked hasSetup = false;
  @tracked isSmall = false;
  break = 510;
  oldW = 0;
  oldH = 0;
  resizeTimer = null;
  container = null;

  spyResizeWithTimer(element) {
    this.oldW = element.offsetWidth;
    this.oldH = element.offsetHeight;
    cancel(this.resizeTimer);
    const _this = this;

    function sizeCheck() {
      _this.resizeTimer = later(() => {
        sizeCheck();
      }, 1000);

      if (
        _this.oldW == element.offsetWidth &&
        _this.oldH == element.offsetHeight
      ) {
        return;
      }

      _this.oldW = element.offsetWidth;
      _this.oldH = element.offsetHeight;

      _this.onResize();
    }

    _this.resizeTimer = later(() => {
      sizeCheck();
    }, 1000);
  }

  spyResize() {
    try {
      if (this.sizeObserver) {
        this.sizeObserver.disconnect();
      }

      this.sizeObserver = new ResizeObserver(() => {
        this.onResize();
      });
      this.sizeObserver.observe(this.container);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
      return this.spyResizeWithTimer(this.container);
    }
  }

  @action
  setupTimer(element) {
    this.container = element;

    this.onResize();
    this.spyResize(element);

    this.hasSetup = true;
  }

  willDestroyElement() {
    if (this.resizeTimer) {
      cancel(this.resizeTimer);
    }

    if (this.sizeObserver) {
      this.sizeObserver.disconnect();
    }
  }

  onResize() {
    const isSmall = this.container.offsetWidth <= this.break;

    if (this.isSmall != isSmall) {
      this.isSmall = isSmall;
    }
  }
}
