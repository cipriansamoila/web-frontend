import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  modal: service(),
  newPassword: '',
  newPassword2: '',
  isValid: false,

  actions: {
    change(value, newPassword, newPassword2) {
      this.set('newPassword', newPassword);
      this.set('newPassword2', newPassword2);
    },

    validationChanged(newValue) {
      this.set('isValid', newValue);
    },

    update() {
      this.modal
        .confirmWithPassword(
          'change password',
          'Enter your current password to continue.'
        )
        .then((password) => {
          this.onUpdate?.(password, this.newPassword);
        });
    },
  },
});
