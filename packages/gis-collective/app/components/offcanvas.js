import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';
import { Offcanvas } from 'bootstrap';
import { tracked } from '@glimmer/tracking';

export default class OffcanvasComponent extends Component {
  elementId = `offcanvas-${guidFor(this)}`;

  @tracked visible;

  @action
  close() {
    this.offcanvas.hide();
  }

  @action
  setup(element) {
    this.offcanvas = new Offcanvas(element);

    element.addEventListener('hidden.bs.offcanvas', () => {
      this.visible = false;
      this.args.onHide?.();
    });

    element.addEventListener('show.bs.offcanvas', () => {
      this.visible = true;
    });

    this.setStates();
  }

  @action
  setStates() {
    if (this.args.show) {
      this.offcanvas.show();
    }

    if (!this.args.show) {
      this.close();
    }
  }
}
