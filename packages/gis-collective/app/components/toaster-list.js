import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
  classNames: [
    'toaster-list',
    'd-flex',
    'justify-content-center',
    'align-items-center',
  ],
  toaster: service(),

  actions: {
    close(index) {
      this.toaster.remove(index);
    },
  },
});
