import Component from '@glimmer/component';
import { htmlSafe } from '@ember/string';
import { action } from '@ember/object';

export default class NativeScrollbarComponent extends Component {
  get hasSize() {
    return (
      typeof this.args.total == 'number' && typeof this.args.visible == 'number'
    );
  }

  get size() {
    return (this.args.total / this.args.visible) * 100;
  }

  get innerStyle() {
    return htmlSafe(`height: ${this.size}%`);
  }

  updateScroll(position, height) {
    const percentage = position / height;
    const start = Math.ceil(percentage * this.args.total);

    this.args.onScroll?.(start);
  }

  @action
  setup(element) {
    element.addEventListener('scroll', () => {
      this.updateScroll(element.scrollTop, element.scrollHeight);
    });
  }
}
