import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class PresentationComponent extends Component {
  @tracked selectedPage = -1;

  get slides() {
    const result = [];

    if (!this.args.cols?.length) {
      return result;
    }

    let currentRow = {};
    let rowNr = this.args.cols[0].row;
    result.push(currentRow);

    this.args.cols?.forEach((col) => {
      if (rowNr != col.row) {
        rowNr = col.row;
        currentRow = {};
        result.push(currentRow);
      }

      if (col.type == 'article') {
        currentRow.article = col;
      } else {
        currentRow.content = col;
      }
    });

    return result;
  }

  get currentLabel() {
    return this.selectedPage + 1;
  }

  @action
  setupFirstPage() {
    if (!this.args.hasIntro) {
      this.selectedPage = 0;
    }
  }

  @action
  nextPage() {
    if (!this.args.hasEnd && this.selectedPage == this.slides.length - 1) {
      return;
    }

    this.selectedPage = this.selectedPage + 1;
  }

  @action
  prevPage() {
    if (!this.args.hasIntro && this.selectedPage == 0) {
      return;
    }

    this.selectedPage = this.selectedPage - 1;
  }
}
