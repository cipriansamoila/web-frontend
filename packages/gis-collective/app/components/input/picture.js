import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { htmlSafe } from '@ember/template';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class InputPictureComponent extends Component {
  @tracked isRotating = false;
  @tracked _value;
  @tracked rnd;

  @service toaster;

  get value() {
    if (this._value || this._value === null) {
      return this._value;
    }

    if (!this.args.value) {
      return null;
    }

    if (!this._loadingPromise && this.args.value && this.args.value.then) {
      this._loadingPromise = true;
      this.args.value.then((val) => {
        this._loadingPromise = false;
        this._value = val;
      });
    }

    if (this._loadingPromise) {
      return null;
    }

    return this.args.value;
  }

  set value(newValue) {
    this._value = newValue;
  }

  get canRotate() {
    if (this.isEmpty) {
      return false;
    }

    return this.value.get('canRotate');
  }

  get canEnable360() {
    return !this.args.disable365 && !this.isEmpty;
  }

  get progressStyle() {
    return htmlSafe(`width: ${this.progress}%`);
  }

  get canDelete() {
    if (this.isEmpty) {
      return false;
    }

    return this.args.onRemove ? true : false;
  }

  get isEmpty() {
    if (!this.value || !this.value.get('picture')) {
      return true;
    }

    if (this.value.get('name') == 'default') {
      return true;
    }

    return false;
  }

  get isSaving() {
    if (this.isEmpty) {
      return false;
    }

    if (this.isRotating) {
      return true;
    }

    if (this.progress > 0 && this.progress < 100) {
      return true;
    }

    return this.value && this.value.get('isSaving');
  }

  get progress() {
    if (this.isEmpty) {
      return false;
    }

    if (this.args.progress > 0 && this.args.progress < 100) {
      return parseInt(this.args.progress);
    }

    if (this.value.get('progress') > 0 && this.value.get('progress') < 100) {
      return parseInt(this.value.get('progress'));
    }

    return 0;
  }

  get image() {
    if (this.isEmpty) {
      return htmlSafe('');
    }

    const picture = this.value?.get('picture');

    let rnd = '';

    if (!picture.startsWith('data:')) {
      rnd = '/md?rnd=' + this.rnd;
    }

    if (!picture) {
      return htmlSafe('');
    }

    return htmlSafe(`background-image: url('${picture}${rnd}')`);
  }

  triggerOnChange() {
    const onChange = this.args.onchange || this.args.onChange;

    if (onChange) {
      onChange(this.args.index, this.value);
    }
  }

  async cameraSuccess(result) {
    if (!this.args.createImage) {
      return alert('create image is not set');
    }

    const value = await this.args.createImage();
    if (!value) {
      return;
    }

    value.set('picture', result);

    const maxSize = 1024 * 2;

    return value
      .localResize(maxSize, maxSize)
      .then(() => {
        this.value = value;
        return this.triggerOnChange();
      })
      .catch((err) => {
        this.toaster.handleError(err);
      });
  }

  @action
  toggle360() {
    this.value.is360 = !this.value.is360;
    this.triggerOnChange();
  }

  @action
  inputInserted(element) {
    this.inputElement = element;
  }

  @action
  selectFile() {
    if (this.isSaving) {
      return;
    }

    this.inputElement.click();
  }

  @action
  fileSelected(event) {
    const file = event.target.files[0];

    if (!file) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = (event) => {
      this.cameraSuccess(event.target.result);
    };

    reader.onerror = function () {};
  }

  @action
  rotate() {
    if (this.isSaving) {
      return;
    }

    this.isRotating = true;

    return this.value
      .rotate()
      .then(() => {
        this.isRotating = false;
        this.rnd = Math.random();
      })
      .catch((err) => {
        this.isRotating = false;
        this.toaster.handleError(err);
      });
  }

  @action
  remove() {
    if (this.isSaving) {
      return;
    }

    return this.args.onRemove(this.args.index);
  }
}
