import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class InputDropdownComponent extends Component {
  @action
  select(value, event) {
    this.args.onChange?.(value);
    event.preventDefault();
  }
}
