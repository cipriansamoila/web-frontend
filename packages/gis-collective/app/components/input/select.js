import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

class Option {
  @tracked id;
  @tracked name;
  @tracked selected;
}

export default class InputSelectComponent extends Component {
  @tracked _value;

  get value() {
    if (this._value || this._value === 0) {
      return this._value;
    }

    if (this.args.value === 0) {
      return 0;
    }

    return this.getValueFromObject(this.args.value);
  }

  getValueFromObject(obj) {
    let value = obj || '';

    if (value) {
      if (obj.get && obj.get('id')) {
        value = this.args.value.get('id');
      }

      if (!obj.get && obj.id) {
        value = this.args.value.id;
      }
    }

    return value;
  }

  get hasNoSelectedValues() {
    return !this.value;
  }

  get selectionList() {
    if (!this.args.list || !this.args.list.length) {
      return [];
    }

    return this.args.list.map((a) => {
      const option = new Option();
      if (typeof a == 'string') {
        option.id = a;
        option.name = a;
      } else if (a?.id && a?.name) {
        option.id = a.id;
        option.name = a.name;
      }

      return option;
    });
  }

  @action
  renderSelect(element) {
    this.element = element;
    this.element.value = this.value;
  }

  @action
  updateValue() {
    this.element.value = this.getValueFromObject(this.args.value);
  }

  @action
  change() {
    this._value = this.element.value;

    this.args.onChange?.(this.getById(this.element.value));
  }

  getById(id) {
    var result = null;

    this.args.list?.forEach(function (item) {
      if (item === id || item.id === id) {
        result = item;
      }
    });

    return result;
  }
}
