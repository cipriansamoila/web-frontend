import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';

export default class InputListComponent extends Component {
  elementId = 'select-list-' + guidFor(this);
  @tracked _values;
  @tracked _isEnabled = null;

  get haveEnabledSwitch() {
    return this.args.isEnabled !== undefined;
  }

  get isEnabled() {
    if (!this.haveEnabledSwitch) {
      return true;
    }

    if (this._isEnabled === null) {
      return this.args.isEnabled;
    }

    return this._isEnabled;
  }

  set isEnabled(value) {
    this._isEnabled = value;
    this.raiseChange();
  }

  get values() {
    if (this._values) {
      return this._values;
    }

    if (this.args.values && this.args.values.toArray) {
      return this.args.values.toArray();
    }

    if (this.args.values && this.args.values.length) {
      return this.args.values;
    }

    return [];
  }

  get list() {
    return this.args.list ?? [];
  }

  get canAdd() {
    return this.list.length > this.values.length;
  }

  get firstObject() {
    const items = this.list.filter((a) => this.values.indexOf(a) == -1);

    return items.pop();
  }

  raiseChange() {
    let values = this.isEnabled ? this.values : [];

    const validValues = values.filter((a) => a !== null && a !== undefined);

    this.args.onChange?.(this.isEnabled, validValues);
  }

  @action
  removeItem(index) {
    if (!this._values) {
      this._values = this.args.values.slice();
    }

    this._values.removeAt(index);
    this.raiseChange();
  }

  @action
  updateItem(index, value) {
    if (!this._values) {
      this._values = this.args.values.slice();
    }

    this._values[index] = value;
    this._values = [...this._values];
    this.raiseChange();
  }

  @action
  addItem() {
    if (!this._values) {
      this._values = this.args.values.slice();
    }

    this._values.addObject(this.firstObject);
    this.raiseChange();
  }
}
