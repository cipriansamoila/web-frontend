import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import IconStyle from '../../lib/icon-style';

export default class InputIconStyleAllComponent extends Component {
  @tracked tmpValue;

  @action
  onUpdate() {
    this.tmpValue = new IconStyle(this.args.value);
  }

  @action
  updateStyle(key, value) {
    this.tmpValue[key] = value;
    this.raiseChange();
  }

  raiseChange() {
    if (!this.args.onChange) {
      return;
    }

    this.args.onChange(new IconStyle(this.tmpValue));
  }
}
