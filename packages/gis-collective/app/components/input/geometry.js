import BrowseGeometryComponent from '../browse/geometry';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { debounce } from '@ember/runloop';
import { getCenter } from 'ol/extent';
import { inject as service } from '@ember/service';
import { transform } from 'ol/proj';

export default class InputGeometryComponent extends BrowseGeometryComponent {
  @tracked mode;
  @tracked gpxType;
  @tracked fileError;
  @service fastboot;
  tooltips = [];

  raiseChange(value) {
    this.args.onChange?.(value);
  }

  get showToolbar() {
    return !this.args.disabled;
  }

  get extent() {
    if (
      !this.args.extent ||
      !this.args.extent.type ||
      this.args.extent.type != 'Polygon'
    ) {
      return null;
    }

    return this.args.extent;
  }

  updateCoordinates() {
    if (!this._coordinates || !this.isPoint) {
      return;
    }

    this.raiseChange({
      type: this.value.type,
      coordinates: this._coordinates,
    });
  }

  get isNewLine() {
    return this.type == 'LineString' && this.args.value.coordinates.length == 0;
  }

  get isNewPolygon() {
    return this.type == 'Polygon' && this.args.value.coordinates.length == 0;
  }

  get isEditPath() {
    const value = this.args.value;
    return (
      this.type != 'Point' &&
      value &&
      value.coordinates &&
      value.coordinates.length > 0
    );
  }

  get isEdit() {
    return !this.isPaste && !this.isFile;
  }

  get isPaste() {
    return this.mode == 'paste';
  }

  get isFile() {
    return this.mode == 'file';
  }

  get value() {
    if (this.args.value) {
      return this.args.value;
    }

    return this.defaultPoint;
  }

  get defaultPoint() {
    let coordinates = [0, 0];

    if (this.extentM) {
      coordinates = getCenter(this.extentM);
    }

    if (this._coordinates) {
      coordinates = this._coordinates;
    }

    return {
      type: 'Point',
      coordinates,
    };
  }

  @action
  checkPointCoordinates() {
    if (!this.extentM || !this.isPoint) {
      return;
    }

    if (this._coordinates?.length != 2) {
      return this.createPoint();
    }

    const x = this._coordinates[0];
    const y = this._coordinates[1];
    const minX = Math.min(this.extentM[0], this.extentM[2]);
    const maxX = Math.max(this.extentM[0], this.extentM[2]);
    const minY = Math.min(this.extentM[1], this.extentM[3]);
    const maxY = Math.max(this.extentM[1], this.extentM[3]);

    if (x < minX || x > maxX || y < minY || y > maxY) {
      this.createPoint();
    }
  }

  @action
  changeMapView(map) {
    if (this.args.disabled) {
      return;
    }

    const center = transform(
      map.getView().getCenter(),
      'EPSG:3857',
      'EPSG:4326'
    );

    this.updatePointCoordinates(center);
  }

  @action
  newPolygon(ev) {
    const coordinates = ev.feature
      .getGeometry()
      .transform('EPSG:3857', 'EPSG:4326')
      .getCoordinates();

    this.raiseChange({
      type: 'Polygon',
      coordinates,
    });
  }

  @action
  featureChange(ev) {
    const feature = ev.features.item(0);
    const coordinates = feature
      .getGeometry()
      .transform('EPSG:3857', 'EPSG:4326')
      .getCoordinates();

    if (JSON.stringify(this._coordinates) == JSON.stringify(coordinates)) {
      return;
    }

    this._coordinates = coordinates;
    debounce(this, this.updateCoordinates, 100);
  }

  @action
  updatePointCoordinates(coordinates) {
    if (JSON.stringify(this._coordinates) == JSON.stringify(coordinates)) {
      return;
    }

    this._coordinates = coordinates;

    debounce(this, this.updateCoordinates, 500);
  }

  @action
  newLine(ev) {
    const coordinates = ev.feature
      .getGeometry()
      .transform('EPSG:3857', 'EPSG:4326')
      .getCoordinates();

    this.raiseChange({
      type: 'LineString',
      coordinates,
    });
  }

  @action
  selectMode(mode) {
    this.mode = mode;
  }

  @action
  createPoint() {
    this.raiseChange(this.defaultPoint);
  }

  @action
  createLine() {
    this.raiseChange({
      type: 'LineString',
      coordinates: [],
    });
  }

  @action
  createPolygon() {
    this.raiseChange({
      type: 'Polygon',
      coordinates: [],
    });
  }

  @action
  textContent(value) {
    this.raiseChange(value);
  }

  @action
  fileContent(name, value) {
    this.fileError = '';

    const pieces = name.toLowerCase().split('.');
    const ext = pieces[pieces.length - 1];

    if (ext == 'gpx') {
      const hasTrack = value.indexOf('trk') != -1;
      const hasRoute = value.indexOf('rte') != -1;

      if ((hasTrack && !hasRoute) || (hasTrack && hasRoute)) {
        this.gpxType = 'gpx.track';
      }

      if (!hasTrack && hasRoute) {
        this.gpxType = 'gpx.route';
      }

      if (!hasTrack && !hasRoute) {
        this.gpxType = 'gpx.route';
      }

      if (!hasTrack && !hasRoute) {
        this.fileError = this.intl.t('invalid-gpx-missing-track-or-route');
        return this.raiseChange(null);
      }

      return this.raiseChange({ type: this.gpxType, value });
    }

    if (ext == 'json' || ext == 'geojson') {
      try {
        var jsonValue = JSON.parse(value);
      } catch (err) {
        this.fileError = this.intl.t('invalid-json-file-message');
        return this.raiseChange(null);
      }

      const validGeometries = [
        'Point',
        'LineString',
        'MultiLineString',
        'Polygon',
      ];
      let geometry = jsonValue;

      if (geometry.type == 'Feature') {
        geometry = jsonValue.geometry;
      }

      if (!geometry || !geometry.type) {
        this.fileError = this.intl.t('invalid-json-feature-message');
        return this.raiseChange(null);
      }

      let type = geometry.type;
      if (validGeometries.indexOf(type) == -1) {
        this.fileError = this.intl.t('invalid-json-feature-type-message', {
          type,
        });
        return this.raiseChange(null);
      }

      return this.raiseChange(jsonValue);
    }

    this.raiseChange(null);
    this.fileError = this.intl.t('invalid-file-type-message');
  }
}
