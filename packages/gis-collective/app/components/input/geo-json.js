import Component from '@glimmer/component';
import { action } from '@ember/object';
import JSONEditor from 'jsoneditor';

export default class InputGeoJsonComponent extends Component {
  options = {
    mode: 'code',
    modes: ['tree', 'form', 'code', 'text'],
    enableSort: false,
    enableTransform: false,
  };
  element = null;

  @action
  update() {
    if (this.args.value === null) {
      return;
    }

    this.element.editor.update(this.args.value);
  }

  @action
  setupEditor(element) {
    this.options.onChange = () => {
      if (this.args.onChange)
        try {
          this.args.onChange(this.element.editor.get());
          // eslint-disable-next-line no-empty
        } catch (err) {}
    };

    const editor = new JSONEditor(element, this.options);

    if (this.args.value) {
      editor.set(this.args.value);
    }

    this.element = element;
    this.element.editor = editor;
  }
}
