import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class InputPictureAutoComponent extends Component {
  @tracked isError = false;
  @tracked progress = 100;
  @tracked _value;

  @service toaster;

  updateProgress(value) {
    this.progress = value;
    this.value.progress = value;
  }

  get value() {
    if (!this._value) {
      return this.args.value;
    }

    return this._value;
  }

  @action
  createImage() {
    this._value = this.args.createImage();

    return this._value;
  }

  @action
  isCreated() {
    if (!this.args.value) {
      return;
    }

    if (this.args.value.get('isNew')) {
      this.save(this.args.index, this.args.value);
    }
  }

  @action
  async save(index, value) {
    if (!value.save) {
      return;
    }

    this.isError = false;

    if (value.get('hasDirtyAttributes') || value.get('isNew')) {
      try {
        await value.save({
          adapterOptions: {
            progress: (value) => {
              this.updateProgress(value);
            },
          },
        });
        this.args.save?.(index, value);
      } catch (err) {
        this.isError = true;
        this.toaster.handleError(err);
      }
    }
  }
}
