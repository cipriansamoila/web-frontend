import Component from '@glimmer/component';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { OptionalIconImage } from '../../transforms/optional-icon-image';

export default class InputIconComponent extends Component {
  elementId = 'input-icon-' + guidFor(this);

  get useParent() {
    return this.args.value?.useParent ?? false;
  }

  set useParent(newValue) {
    this.args.onChange?.(
      new OptionalIconImage({
        useParent: newValue,
        value: this.args?.value.value,
      })
    );
  }

  get isEmpty() {
    if (
      !this.useParent &&
      this.args.parentValue &&
      this.args.value?.value == this.args.parentValue?.value
    ) {
      return true;
    }

    if (this.useParent) {
      return !this.args.parentValue?.value;
    }

    return !this.args.value?.value;
  }

  get image() {
    const value = this.args.value?.value;

    if (this.useParent) {
      return this.args.parentValue.value;
    }

    if (typeof value != 'string') {
      return '';
    }

    if (value.indexOf('data:') == 0) {
      return value;
    }

    return `${value}?rnd=${Math.random()}`;
  }

  get isParentImage() {
    return !this.args.value || this.args.value == this.args.parentValue;
  }

  @action
  render(element) {
    this.inputElement = element;
  }

  @action
  selectFile() {
    this.inputElement.click();
  }

  @action
  fileSelected(event) {
    const file = event.target.files[0];

    if (!file || !this.args.onChange) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(file);

    reader.onload = (event) => {
      this.args.onChange?.(
        new OptionalIconImage({
          useParent: false,
          value: event.target.result,
        })
      );
    };
  }
}
