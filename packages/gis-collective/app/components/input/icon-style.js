import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';

export default class InputIconStyleComponent extends Component {
  id = guidFor(this);

  shapeList = ['circle', 'square', 'pentagon', 'hexagon'];
  textList = ['normal', 'shorten', 'wrap'];
  alignList = ['center', 'end', 'left', 'right', 'start'];
  baselineList = [
    'alphabetic',
    'bottom',
    'hanging',
    'ideographic',
    'middle',
    'top',
  ];
  weightList = ['bold', 'normal'];

  get lineDash() {
    if (this._lineDash != null) {
      return this._lineDash;
    }

    if (!this.args.value || !this.args.value.lineDash) {
      return '';
    }

    return this.args.value.lineDash.join(', ');
  }

  @action
  changeLineDash(event) {
    let lineDash = [];

    try {
      lineDash = event.target.value
        .split(',')
        .map((a) => parseInt(a.trim()))
        .filter((a) => Number.isInteger(a));
      // eslint-disable-next-line no-empty
    } catch (err) {}

    this.change('lineDash', lineDash);
  }

  get hasText() {
    return this.hasStyle('text');
  }

  get hasAlign() {
    return this.hasStyle('align');
  }

  get hasBaseline() {
    return this.hasStyle('baseline');
  }

  get hasWeight() {
    return this.hasStyle('weight');
  }

  get hasColor() {
    return this.hasStyle('color');
  }

  get hasSize() {
    return this.hasStyle('size');
  }

  get hasLineHeight() {
    return this.hasStyle('lineHeight');
  }

  get hasOffsetX() {
    return this.hasStyle('offsetX');
  }

  get hasOffsetY() {
    return this.hasStyle('offsetY');
  }

  get hasIsVisible() {
    return this.hasStyle('isVisible');
  }

  get hasHideBackgroundOnZoom() {
    return this.hasStyle('hideBackgroundOnZoom');
  }

  get hasShape() {
    return this.hasStyle('shape');
  }

  get hasBorderColor() {
    return this.hasStyle('borderColor');
  }

  get hasBackgroundColor() {
    return this.hasStyle('backgroundColor');
  }

  get hasBorderWidth() {
    return this.hasStyle('borderWidth');
  }

  get hasLineDash() {
    return this.hasStyle('lineDash');
  }

  hasStyle(name) {
    if (!this.args.value || typeof this.args.value != 'object') {
      return false;
    }

    return this.args.value[name] !== undefined;
  }

  @action
  changeVisibility(event) {
    this._isVisible = event.target.value == 'yes';
    this.change('isVisible', this._isVisible);

    return true;
  }

  @action
  changeHideBackgroundOnZoom(event) {
    this._hideBackgroundOnZoom = event.target.value == 'yes';
    this.change('hideBackgroundOnZoom', this._hideBackgroundOnZoom);

    return true;
  }

  @action
  changeIntInput(key, event) {
    const value = parseInt(event.target.value);

    if (!isNaN(value)) {
      this.change(key, value);
    }
  }

  @action
  changeFloatInput(key, event) {
    const value = parseFloat(event.target.value);

    if (!isNaN(value)) {
      this.change(key, value);
    }
  }

  @action
  change(key, value) {
    if (!this.args.onChange) {
      return;
    }

    this.args.value[key] = value;
    this.args.onChange(this.args.value);
  }
}
