import Component from '@glimmer/component';
import SimpleMDE from 'simplemde';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class InputSimpleMdeComponent extends Component {
  @service fullscreen;

  @tracked isFullscreen = false;
  @tracked isPreview = false;
  @tracked _value = '';

  @tracked isBold;
  @tracked isItalic;
  @tracked isStrike;
  @tracked isHeading;
  @tracked isQuote;
  @tracked isUnorderedList;
  @tracked isOrderedList;
  @tracked isPreview;

  options = {
    autoDownloadFontAwesome: false,
    status: false,
    spellChecker: false,
  };

  @action
  updateValue() {
    if (!this.currentEditor) {
      return;
    }

    if (this.currentEditor.value() != this.args.value) {
      this.currentEditor.value(this.args.value);
    }
  }

  @action
  setupEditor(element) {
    this.options.element = element.querySelector('textarea');
    this.options.initialValue = this.args.value;
    this.options.toolbar = false;

    this.currentEditor = new SimpleMDE(this.options);

    this.currentEditor.codemirror.on('change', () => {
      if (this.isDestroyed || this.isDestroying) {
        return;
      }

      let value = this.currentEditor.value();

      if (this.args.maxLen && value.length > this.args.maxLen) {
        value = value.substr(0, this.args.maxLen);
        this.currentEditor.value(value);
      }

      if (this.args.onChange) {
        this.args.onChange(value);
      }
    });

    this.currentEditor.codemirror.on('cursorActivity', () => {
      this.updateToolbar();
    });

    this.currentEditor.codemirror.on('focus', () => {
      if (this.isDestroyed || this.isDestroying) {
        return;
      }

      this.args.focusIn?.();
      this.args.onFocusIn?.();
    });

    this.currentEditor.codemirror.on('blur', () => {
      if (this.isDestroyed || this.isDestroying) {
        return;
      }

      this.args.focusOut?.();
      this.args.onFocusOut?.();
    });

    this.updateToolbar();
  }

  updateToolbar() {
    const state = this.currentEditor.getState();

    this.isBold = state.bold;
    this.isItalic = state.italic;
    this.isStrike = state.strikethrough;

    this.isHeading = state.heading;

    this.isQuote = state.quote;
    this.isUnorderedList = state['unordered-list'];
    this.isOrderedList = state['ordered-list'];
  }

  @action
  toggleBold() {
    this.currentEditor.toggleBold();
  }

  @action
  toggleItalic() {
    this.currentEditor.toggleItalic();
  }

  @action
  toggleStrike() {
    this.currentEditor.toggleStrikethrough();
  }

  @action
  toggleHeading() {
    const state = this.currentEditor.getState();

    if (state.heading && !state['heading-2']) {
      this.currentEditor.toggleHeading2();
    }

    this.currentEditor.toggleHeading2();
  }

  @action
  toggleHPlus() {
    this.currentEditor.toggleHeadingSmaller();
  }

  @action
  toggleHMinus() {
    this.currentEditor.toggleHeadingBigger();
  }

  @action
  toggleQuote() {
    this.currentEditor.toggleBlockquote();
  }

  @action
  toggleUnorderedList() {
    this.currentEditor.toggleUnorderedList();
  }

  @action
  toggleOrderedList() {
    this.currentEditor.toggleOrderedList();
  }

  @action
  drawLink() {
    this.currentEditor.drawLink();
  }

  @action
  drawImage() {
    this.currentEditor.drawImage();
  }

  @action
  preview() {
    this.currentEditor.togglePreview();
    this.isPreview = !this.isPreview;
  }

  @action
  toggleFullscreen() {
    this.isFullscreen = !this.isFullscreen;
    this.fullscreen.set('isEnabled', this.isFullscreen);
  }
}
