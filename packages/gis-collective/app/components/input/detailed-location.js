import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';

export default class InputDetailedLocationComponent extends Component {
  elementId = 'user-profile-' + guidFor(this);

  @tracked _country;
  @tracked _province;
  @tracked _city;
  @tracked _postalCode;

  get country() {
    if (this._country === '') {
      return '';
    }

    return this._country || this.args.value?.country || '';
  }

  set country(value) {
    this._country = value;
    this.triggerChange();
  }

  get province() {
    if (this._province === '') {
      return '';
    }

    return this._province || this.args.value?.province || '';
  }

  set province(value) {
    this._province = value;
    this.triggerChange();
  }

  get city() {
    if (this._city === '') {
      return '';
    }

    return this._city || this.args.value?.city || '';
  }

  set city(value) {
    this._city = value;
    this.triggerChange();
  }

  get postalCode() {
    if (this._postalCode === '') {
      return '';
    }

    return this._postalCode || this.args.value?.postalCode || '';
  }

  set postalCode(value) {
    this._postalCode = value;
    this.triggerChange();
  }

  triggerChange() {
    this.args.onChange?.({
      country: this.country,
      province: this.province,
      city: this.city,
      postalCode: this.postalCode,
    });
  }
}
