import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class InputSiteAttributesComponent extends Component {
  @service fastboot;

  @tracked deletedPath;
  @tracked delete;
  @tracked newAttributes;
  @tracked queue;
  @tracked rename;
  @tracked selectedPath;
  @tracked editablePanel;
  @tracked newGroupName;

  updateArrays(attributes) {
    const result = {};

    Object.keys(attributes).forEach((key) => {
      if (attributes[key] == null) {
        return;
      }

      if (Array.isArray(attributes[key])) {
        result[key] = attributes[key].filter((a) => a);
      } else {
        result[key] = attributes[key];
      }
    });

    return result;
  }

  enableEdit(path) {
    this.deletedPath = [];
    this.delete = [];
    this.newAttributes = [];
    this.queue = {};
    this.rename = {};
    this.selectedPath = path;
    this.editablePanel = path ? `${path.join('.')}` : null;
    this.newGroupName = '';
  }

  get attributes() {
    return JSON.parse(JSON.stringify(this.args.attributes));
  }

  @action
  renameGroup(event) {
    this.newGroupName = event.target.value;
  }

  @action
  addAttribute() {
    this.newAttributes.addObject({
      key: 'new attribute',
      value: '',
    });
  }

  @action
  updateNewAttribute(index, key, value) {
    this.newAttributes.set(`${index}.value`, value);
  }

  @action
  renameNewAttribute(index, key, newKey) {
    this.newAttributes.set(`${index}.key`, newKey);
  }

  @action
  deleteNewAttribute(index) {
    this.newAttributes.removeAt(index);
  }

  @action
  updateAttribute(path, key, value) {
    this.queue[key] = value;
  }

  @action
  renameAttribute(path, key, newKey) {
    this.rename[key] = [newKey];
  }

  @action
  deleteAttribute(path, key) {
    this.delete.push(key);
  }

  @action
  deletePath(path) {
    this.deletedPath = path;
  }

  @action
  add(path) {
    this.enableEdit(path);

    this.isArray = false;
    this.isNewAttribute = true;
  }

  @action
  addArrayItem(path) {
    path.push(this.args.attributes[path[0]]?.length ?? 0);
    this.enableEdit(path);
  }

  @action
  edit(path) {
    this.enableEdit(path);
  }

  @action
  restore() {
    this.enableEdit();
  }

  @action
  addNewGroup() {
    let index = 1;

    while (this.args.attributes[`New Group ${index}`]) {
      index++;
    }

    this.newAttributeName = `New Group ${index}`;
    this.newAttributes = [];
    this.selectedPath = 'new attribute';
  }

  @action
  saveNewGroup() {
    let attributes = this.attributes;

    attributes[this.newAttributeName] = {};

    this.newAttributes.forEach((attribute) => {
      attributes[this.newAttributeName][attribute.key] = attribute.value;
    });

    this.enableEdit();
    return this.args.onSave?.(attributes);
  }

  @action
  restoreNewGroup() {
    this.enableEdit();
  }

  @action
  async save() {
    let attributes = this.attributes;

    if (this.selectedPath?.length == 1) {
      attributes = this.attributes[this.selectedPath[0]] ?? {};
    }

    if (this.selectedPath?.length == 2) {
      let group = this.attributes[this.selectedPath[0]] ?? {};

      attributes = group[this.selectedPath[1]] ?? {};
    }

    Object.keys(this.queue).forEach((key) => {
      const value = this.queue[key];
      attributes[key] = value;
    });

    Object.keys(this.rename).forEach((key) => {
      const newKey = this.rename[key];

      attributes[newKey] = attributes[key];
      delete attributes[key];
    });

    this.newAttributes.forEach((attribute) => {
      attributes[attribute.key] = attribute?.value;
    });

    this.delete.forEach((key) => {
      delete attributes[key];
    });

    let allAttributes = this.attributes;

    if (this.newGroupName != '' && !allAttributes[this.newGroupName]) {
      allAttributes[this.newGroupName] = allAttributes[this.selectedPath[0]];
      delete allAttributes[this.selectedPath[0]];
      this.selectedPath[0] = this.newGroupName;
    }

    if (this.selectedPath?.length == 1) {
      allAttributes[this.selectedPath[0]] = attributes;
    }

    if (this.selectedPath?.length == 2) {
      if (!allAttributes[this.selectedPath[0]]) {
        allAttributes[this.selectedPath[0]] = [{}];
      }
      allAttributes[this.selectedPath[0]][this.selectedPath[1]] = attributes;
    }

    if (this.deletedPath?.length == 1) {
      delete allAttributes[this.deletedPath[0]];
    }

    if (this.deletedPath?.length == 2) {
      delete allAttributes[this.deletedPath[0]][this.deletedPath[1]];
    }

    this.enableEdit();

    allAttributes = this.updateArrays(allAttributes);
    try {
      return await this.args.onSave?.(allAttributes);
    } catch (err) {
      // eslint-disable-next-line no-empty
    }
  }
}
