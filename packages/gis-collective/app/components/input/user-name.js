import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputUserNameComponent extends Component {
  elementId = 'input-user-name-' + guidFor(this);

  @tracked _salutation = null;
  @tracked _title = null;
  @tracked _firstName = null;
  @tracked _lastName = null;

  get salutation() {
    if (this._salutation == '') {
      return '';
    }

    return this._salutation || this.args.value?.salutation || '';
  }

  get title() {
    if (this._title == '') {
      return '';
    }

    return this._title || this.args.value?.title || '';
  }

  set title(value) {
    this._title = value;
    this.triggerChange();
  }

  get firstName() {
    if (this._firstName == '') {
      return '';
    }

    return this._firstName || this.args.value?.firstName || '';
  }

  set firstName(value) {
    this._firstName = value;
    this.triggerChange();
  }

  get lastName() {
    if (this._lastName == '') {
      return '';
    }

    return this._lastName || this.args.value?.lastName || '';
  }

  set lastName(value) {
    this._lastName = value;
    this.triggerChange();
  }

  @action
  onChangeSalutation(event) {
    this._salutation = event.target.value;

    this.triggerChange();
  }

  triggerChange() {
    return this.args.onChange?.({
      salutation: this.salutation,
      title: this.title,
      firstName: this.firstName,
      lastName: this.lastName,
    });
  }
}
