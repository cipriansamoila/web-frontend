import Component from '@glimmer/component';
import { htmlSafe } from '@ember/template';
import { tracked } from '@glimmer/tracking';

export default class InputStyleColorComponent extends Component {
  @tracked _value = null;

  get colorStyle() {
    return htmlSafe(`background-color: ${this.value}`);
  }

  get value() {
    if (this._value !== null) {
      return this._value;
    }

    return this.args.value;
  }

  set value(newValue) {
    this._value = newValue;

    if (this.args.onChange) {
      this.args.onChange(newValue);
    }
  }
}
