import Component from '@glimmer/component';
import { later } from '@ember/runloop';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import { tracked } from '@glimmer/tracking';

export default class InputDateComponent extends Component {
  @tracked elementId = `icon-set-${guidFor(this)}`;

  @action
  setup(element) {
    this.containerElement = element;
  }

  get beginYear() {
    return 1990;
  }

  get endYear() {
    return 2030;
  }

  get years() {
    const list = [];

    const fullYear = this.args.value.getFullYear();

    for (let year = this.beginYear; year <= this.endYear; year++) {
      list.push({ year, selected: year == fullYear });
    }

    later(() => {
      this.containerElement.querySelector('.year').value = fullYear;
    }, 1);

    return list;
  }

  get months() {
    const list = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];

    return list.map((month, index) => {
      return {
        month,
        value: index,
        selected: this.args.value.getMonth() == index,
      };
    });
  }

  get days() {
    const list = [];

    for (let day = 1; day <= 31; day++) {
      list.push({ day, selected: this.args.value.getDate() == day });
    }

    return list;
  }

  get isNotSet() {
    return !this.isSet;
  }

  get isSet() {
    if (!this.args.value || !this.args.value.toDateString) {
      return false;
    }

    return this.args.value.getFullYear() > 1;
  }

  set isSet(value) {
    this.args.value.setFullYear(value ? this.beginYear : 0);

    this.args.onChange?.(this.args.value);
  }

  @action
  onChangeValue() {
    const date = new Date(this.args.value);
    this.containerElement.querySelector('.year').value = date.getFullYear();
    this.containerElement.querySelector('.month').value = date.getMonth();
    this.containerElement.querySelector('.day').value = date.getDate();
  }

  @action
  updateValue() {
    const year = parseInt(this.containerElement.querySelector('.year').value);
    const month = parseInt(this.containerElement.querySelector('.month').value);
    const day = parseInt(this.containerElement.querySelector('.day').value);

    const date = new Date(this.args.value);

    date.setFullYear(year);
    date.setMonth(month);
    date.setDate(day);

    this.args.onChange?.(date);
  }
}
