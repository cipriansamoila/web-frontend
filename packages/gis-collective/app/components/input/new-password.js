import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputNewPasswordComponent extends Component {
  @service intl;

  @tracked _newPassword1 = null;
  @tracked _newPassword2 = null;

  get newPassword1() {
    if (this._newPassword1 === null) {
      return this.args.newPassword1 ?? '';
    }

    return this._newPassword1;
  }

  set newPassword1(value) {
    this._newPassword1 = value;
    this.handleValidationEvent();
  }

  get newPassword2() {
    if (this._newPassword2 === null) {
      return this.args.newPassword2 ?? '';
    }

    return this._newPassword2;
  }

  set newPassword2(value) {
    this._newPassword2 = value;
    this.handleValidationEvent();
  }

  get validationMessage() {
    if (this.newPassword1 != this.newPassword2) {
      return this.intl.t('the passwords do not match');
    }

    if (this.newPassword1.trim() == '') {
      return this.intl.t("the new password can't be empty");
    }

    if (this.newPassword1.length < 10) {
      return this.intl.t('the new password must have at least 10 chars');
    }

    return '';
  }

  get isValid() {
    return !this.validationMessage;
  }

  get isNotValid() {
    return !!this.validationMessage;
  }

  handleValidationEvent() {
    this.args.onValidationChange?.(this.isValid);
    this.args.onChange?.(
      this.isValid ? this._newPassword1 : null,
      this.newPassword1,
      this.newPassword2
    );
  }

  @action
  updateValidation() {
    this.handleValidationEvent();
  }

  @action
  setup() {
    this.handleValidationEvent();
  }
}
