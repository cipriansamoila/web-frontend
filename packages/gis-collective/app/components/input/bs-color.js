import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class InputBsColorComponent extends Component {
  predefinedVariables = [
    'primary',
    'secondary',
    'success',
    'danger',
    'warning',
    'info',
    'light',
    'dark',
    'white',
    'black',
  ];

  colors = [
    'blue',
    'indigo',
    'purple',
    'pink',
    'red',
    'orange',
    'yellow',
    'green',
    'teal',
    'cyan',
    'gray',
  ];

  get currentColor() {
    if (this.predefinedVariables.indexOf(this.args.value) != -1) {
      return this.args.value;
    }

    if (this.colors.indexOf(this.args.value) != -1) {
      return this.args.value;
    }

    if (this.hasShades) {
      return this.args.value;
    }

    return 'transparent';
  }

  get baseColor() {
    if (!this.args.value) {
      return false;
    }

    const value = this.colors.filter((a) => this.args.value.indexOf(a) == 0);

    return value[0] ?? '';
  }

  get hasShades() {
    if (!this.args.value) {
      return false;
    }

    return (
      this.colors.filter((a) => this.args.value.indexOf(a) == 0).length == 1
    );
  }

  get selectedColor() {
    return this.currentColor;
  }

  get shades() {
    if (!this.hasShades) {
      return null;
    }

    let shades = [];

    for (let i = 1; i <= 9; i++) {
      shades.push(`${this.baseColor}-${i}00`);
    }

    return shades;
  }

  @action
  select(value) {
    if (value == this.currentColor) {
      return this.args.onChange?.('');
    }

    this.args.onChange?.(value);
  }
}
