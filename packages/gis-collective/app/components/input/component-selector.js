import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class InputComponentSelectorComponent extends Component {
  @service pageCols;

  get defaultComponents() {
    return this.pageCols.defaultComponents.map((a) => this.toButton(a));
  }

  get articleComponents() {
    return this.pageCols.articleComponents.map((a) => this.toButton(a));
  }

  get campaignComponents() {
    return this.pageCols.campaignComponents.map((a) => this.toButton(a));
  }

  get iconComponents() {
    return this.pageCols.iconComponents.map((a) => this.toButton(a));
  }

  get mapComponents() {
    return this.pageCols.mapComponents.map((a) => this.toButton(a));
  }

  get groups() {
    return [
      {
        list: this.defaultComponents,
      },
      {
        name: 'article',
        list: this.articleComponents,
      },
      {
        name: 'map',
        list: this.mapComponents,
      },
      {
        name: 'campaign',
        list: this.campaignComponents,
      },
      {
        name: 'icon',
        list: this.iconComponents,
      },
    ];
  }

  toButton(name) {
    const icons = {
      banner: 'newspaper',
      blocks: 'paragraph',
      picture: 'image',
      pictures: 'images',
      sounds: 'file-audio',
      article: 'stream',
      icons: 'chess-pawn',
      'campaign-card-list': 'pencil-alt',
      'map-card-list': 'map',
      menu: 'bars',
    };

    const label = name.split('-').join(' ');

    return {
      name,
      label,
      icon: icons[name],
      isSelected: name == this.args.value,
    };
  }

  @action
  change(value) {
    this.args.onChange(value);
  }
}
