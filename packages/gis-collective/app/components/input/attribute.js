import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  classNameBindings: ['isDeleted:d-none'],
  isDeleted: false,

  isBoolType: computed('type', function () {
    return this.type == 'boolean';
  }),

  isOptionType: computed('type', function () {
    return this.type == 'options';
  }),

  isDefaultType: computed('isBoolType', 'isOptionType', function () {
    return !this.isBoolType && !this.isOptionType;
  }),

  list: computed('options', 'value', function () {
    const value = this.value;
    const options = this.options;

    var pieces = options.split(',');

    if (pieces.indexOf(value) == -1 && value != '' && value != undefined) {
      pieces.push(value);
    }

    return pieces;
  }),

  tmpValue: computed('value', {
    get: function () {
      return this.value;
    },
    set: function (name, value) {
      this.onUpdate?.(this.category, this.key, value);

      return value;
    },
  }),

  tmpKey: computed('key', {
    get: function () {
      return this.key;
    },

    set: function (name, value) {
      this.onRename?.(this.category, this.key, value);

      return value;
    },
  }),

  actions: {
    change(newValue) {
      this.set('tmpValue', newValue);
    },

    delete() {
      this.set('isDeleted', true);
      this.onDelete?.(this.category, this.key);
    },
  },
});
