import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { debounce, cancel } from '@ember/runloop';

export default class InputSoundListComponent extends Component {
  @tracked isLoading;
  @tracked isError;
  nameUpdates = {};

  get value() {
    if (!this.args.value) {
      return [{ isAdd: true }];
    }

    return this.args.value.toArray().concat([{ isAdd: true }]);
  }

  @action
  dragEnd({ sourceList, sourceIndex, targetList, targetIndex }) {
    if (sourceIndex === targetIndex || sourceList !== targetList) return;

    const item = sourceList.objectAt(sourceIndex);
    sourceList.removeAt(sourceIndex);
    sourceList.insertAt(targetIndex, item);

    this.triggerOnChange(targetList);
  }

  @action
  changeName(index) {
    const item = this.value.objectAt(index);

    if (item && item.save) {
      if (this.nameUpdates[item.id]) {
        cancel(this.nameUpdates[item.id]);
      }
      this.nameUpdates[item.id] = debounce(item, item.save, 500);
    }
  }

  @action
  remove(index) {
    const newValue = this.args.value.toArray();
    newValue.removeAt(index);

    this.triggerOnChange(newValue);
  }

  async triggerOnChange(newValue) {
    this.isLoading = true;

    try {
      if (!this.args.onChange) {
        this.isLoading = false;
        return;
      }

      await this.args.onChange(newValue.filter((a) => !a.isAdd));
    } catch (err) {
      this.isError = true;
    }

    this.isLoading = false;
  }

  @action
  selectFile() {
    this.fileElement.click();
  }

  @action
  setupFileInput(element) {
    this.fileElement = element;
  }

  @action
  fileSelected(event) {
    const file = event.target.files[0];

    if (!file) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event) => {
      if (!this.args.create) return;

      const sound = this.args.create();
      sound.sound = event.target.result;
      sound.name = file.name || '';

      const newValue = this.value.concat([sound]);
      this.triggerOnChange(newValue);
    };
  }
}
