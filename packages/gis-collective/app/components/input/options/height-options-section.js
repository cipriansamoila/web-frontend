import Component from './base-options';

export default class InputHeightOptionsSectionComponent extends Component {
  availableHeights = [
    'default',
    '15',
    '20',
    '25',
    '30',
    '40',
    '50',
    '60',
    '70',
    '75',
    '80',
    '90',
    '100',
  ];

  get minHeight() {
    return this.getProperty('min-vh', this.options);
  }
}
