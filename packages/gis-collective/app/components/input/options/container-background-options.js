import Component from '@glimmer/component';
import { action } from '@ember/object';
import { PageContainer } from '../../../transforms/page-container-list';

export default class InputOptionsContainerBackgroundOptionsComponent extends Component {
  @action
  changeBgColor(value) {
    const pageContainer = new PageContainer(this.args.value);
    pageContainer.backgroundColor = value;

    this.args.onChange?.(pageContainer);
  }
}
