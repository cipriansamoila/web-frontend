import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputRowOptionsComponent extends Component {
  @tracked _options;

  get options() {
    return this._options || this.args.row?.options || [];
  }

  @action
  onChangeOptions(options) {
    this._options = options;

    this.args.onChangeOptions?.(this.options);
  }
}
