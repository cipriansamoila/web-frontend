import Component from './base-options';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';

export default class InputRowOptionsSectionComponent extends Component {
  elementId = `input-row-options-section-${guidFor(this)}`;

  availableGutters = ['default', '0', '1', '2', '3', '4', '5'];
  availableJustifyContent = [
    'default',
    'start',
    'end',
    'center',
    'between',
    'around',
    'evenly',
  ];
  availableAlignItems = [
    'default',
    'start',
    'end',
    'center',
    'baseline',
    'stretch',
  ];

  get verticalGutters() {
    return this.getProperty('gy', this.options);
  }

  get horizontalGutters() {
    return this.getProperty('gx', this.options);
  }

  get alignItems() {
    return this.getProperty('align-items', this.options);
  }

  get justifyContent() {
    return this.getProperty('justify-content', this.options);
  }

  @action
  verticalGuttersChange(value) {
    this.changeProperty('gy', value);
  }

  @action
  horizontalGuttersChange(value) {
    this.changeProperty('gx', value);
  }

  @action
  justifyContentChange(value) {
    this.changeProperty('justify-content', value);
  }

  @action
  alignItemsChange(value) {
    this.changeProperty('align-items', value);
  }
}
