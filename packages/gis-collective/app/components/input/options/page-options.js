import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';

export default class InputOptionsPageOptionsComponent extends Component {
  elementId = `layout-options-${guidFor(this)}`;

  get path() {
    const slug = this.args.page?.slug ?? '';
    const pieces = slug
      .split('--')
      .map((a) => a.trim())
      .filter((a) => a != '');

    return `/${pieces.join('/')}`;
  }

  set path(value) {
    const pieces = value
      .split('/')
      .map((a) => a.trim())
      .filter((a) => a != '');

    this.args.page.slug = pieces.join('--');
  }

  @action
  changeLayout(value) {
    this.args.page.layout = value;
  }

  @action
  saveIsPublic(value) {
    this.args.page.visibility.isPublic = value;
  }
}
