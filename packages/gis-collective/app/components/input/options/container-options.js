import Component from './base-options';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
export default class InputOptionsContainerOptionsComponent extends Component {
  availableWidths = ['default', 'fluid'];
  elementId = `input-container-options-section-${guidFor(this)}`;

  get options() {
    return this._options || this.args.container?.options || [];
  }

  @action
  onChangeOptions(options) {
    this.args.onChangeOptions?.(options);
  }

  get width() {
    return this.getProperty('container', this.options);
  }

  @action
  widthChange(value) {
    this.changeProperty('container', value);
  }
}
