import Component from './base-options';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';

export default class InputOptionsMarginsComponent extends Component {
  elementId = `input-margin-options-${guidFor(this)}`;

  availableMargins = ['default', '0', '1', '2', '3', '4', '5'];

  get topMargin() {
    return this.getProperty('mt');
  }

  get bottomMargin() {
    return this.getProperty('mb');
  }

  get startMargin() {
    return this.getProperty('ms');
  }

  get endMargin() {
    return this.getProperty('me');
  }

  @action
  topMarginChange(value) {
    this.changeProperty('mt', value);
  }

  @action
  bottomMarginChange(value) {
    this.changeProperty('mb', value);
  }

  @action
  startMarginChange(value) {
    this.changeProperty('ms', value);
  }

  @action
  endMarginChange(value) {
    this.changeProperty('me', value);
  }
}
