import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class InputOptionsFrameComponent extends Component {
  @tracked isClosed;

  @action
  toggleClose() {
    this.isClosed = !this.isClosed;
  }
}
