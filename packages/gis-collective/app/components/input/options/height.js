import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';

export default class InputOptionsHeightComponent extends Component {
  @tracked _value;

  elementId = `input-container-options-section-${guidFor(this)}`;

  get value() {
    if (this._value) {
      return this._value;
    }

    let value = this.args.value;

    if (value <= 0) {
      return '';
    }

    return !isNaN(value) ? value : '';
  }

  set value(newValue) {
    this._value = newValue;
    this.args.onChange?.(parseInt(newValue));
  }

  get type() {
    return this.args.type ?? 'type';
  }
}
