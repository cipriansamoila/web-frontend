import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';
import { defaultBsProperty, sizedBsProperty } from '../../../lib/bs-cols';

export default class InputColOptionsComponent extends Component {
  elementId = `input-col-options-${guidFor(this)}`;
  availableWidths = [
    'default',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
    '11',
    '12',
  ];

  get mobileWidth() {
    return defaultBsProperty('col', this.options);
  }

  get tabletWidth() {
    return sizedBsProperty('col', 'md', this.options);
  }

  get desktopWidth() {
    return sizedBsProperty('col', 'lg', this.options);
  }

  get options() {
    return this.args.col?.options ?? [];
  }

  @action
  mobileWidthChange(value) {
    const newOptions = this.options
      .slice()
      .filter(
        (a) => a.indexOf('col-') != 0 || a.indexOf('-') != a.lastIndexOf('-')
      );

    if (!isNaN(value)) {
      newOptions.push(`col-${value}`);
    }

    this.args.onChangeOptions?.(newOptions);
  }

  @action
  tabletWidthChange(value) {
    const newOptions = this.options.slice().filter((a) => a.indexOf('col-md-'));

    if (!isNaN(value)) {
      newOptions.push(`col-md-${value}`);
    }

    this.args.onChangeOptions?.(newOptions);
  }

  @action
  desktopWidthChange(value) {
    const newOptions = this.options.slice().filter((a) => a.indexOf('col-lg-'));

    if (!isNaN(value)) {
      newOptions.push(`col-lg-${value}`);
    }

    this.args.onChangeOptions?.(newOptions);
  }
}
