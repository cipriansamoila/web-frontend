import Component from './base-options';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';

export default class InputOptionsAlignmentComponent extends Component {
  @tracked _value;

  elementId = `input-options-alignment-${guidFor(this)}`;

  availableAlignments = ['default', 'start', 'center', 'end'];

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value ?? this.availableAlignments[0];
  }

  @action
  onChange(value) {
    this.changeProperty('text', value);
  }
}
