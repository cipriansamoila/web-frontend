import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { later } from '@ember/runloop';
import { guidFor } from '@ember/object/internals';

export default class InputOptionsModelItemsComponent extends Component {
  elementId = `model-items-${guidFor(this)}`;

  @service store;
  @service intl;
  @tracked _allTeams = [];
  @tracked items = [];
  @tracked _value;
  @tracked isLoading = true;

  get value() {
    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  get isAll() {
    return this.queryType === 'all';
  }

  set isAll(value) {
    if (value) {
      this.changed({});
    }
  }

  get isTeam() {
    return this.queryType === 'team';
  }

  set isTeam(value) {
    if (value) {
      this.changed({ team: '' });
    }
  }

  get isIds() {
    return this.queryType === 'ids';
  }

  set isIds(value) {
    if (value) {
      this.changed({ ids: [] });
    }
  }

  get allTeams() {
    const teams = this._allTeams.map((a) => ({
      id: a.id,
      name: a.name,
    }));

    const result = [{ id: 'all', name: this.intl.t('all teams') }];
    result.addObjects(teams);

    return result;
  }

  get teamId() {
    if (typeof this.value?.data?.query?.team == 'string') {
      return this.value?.data.query?.team;
    }

    return this.value?.query?.team;
  }

  get ids() {
    if (Array.isArray(this.value?.data?.query?.ids)) {
      return this.value.data.query.ids;
    }

    return this.value?.query?.ids;
  }

  get selectedTeam() {
    const teamId = this.teamId;

    return (
      this.allTeams.find((a) => a.id == teamId || a.name == teamId) ??
      this.allTeams[0]
    );
  }

  get model() {
    return `${this.args.model}`;
  }

  get queryType() {
    if (typeof this.teamId == 'string') {
      return 'team';
    }

    if (Array.isArray(this.ids)) {
      return 'ids';
    }

    return 'all';
  }

  getValue(id) {
    return this.items.find((a) => a.id == id);
  }

  get values() {
    return this.ids.map((a) => this.getValue(a)).filter((a) => a);
  }

  @action
  itemChanged(value) {
    if (value?.id == 'all') {
      return this.changed({});
    }

    this.changed({ team: value?.id });
  }

  @action
  changeValues(isEnabled, values) {
    const ids = values.map((a) => a.id);

    this.changed({ ids });
  }

  @action
  changed(query) {
    this.args.onChange?.({ query, model: this.model });

    this._value = { query, model: this.model };
  }

  @action
  async setup() {
    this._allTeams = await this.store.findAll('team');
    this.items = await this.store.findAll(this.model);

    later(() => {
      this.isLoading = false;
    }, 300);
  }
}
