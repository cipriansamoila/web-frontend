import Component from './base-options';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';

export default class InputOptionsPaddingOptionsComponent extends Component {
  elementId = `input-padding-options-${guidFor(this)}`;

  availablePaddings = ['default', '0', '1', '2', '3', '4', '5'];

  get topPadding() {
    return this.getProperty('pt');
  }

  get bottomPadding() {
    return this.getProperty('pb');
  }

  get startPadding() {
    return this.getProperty('ps');
  }

  get endPadding() {
    return this.getProperty('pe');
  }

  @action
  topPaddingChange(value) {
    this.changeProperty('pt', value);
  }

  @action
  bottomPaddingChange(value) {
    this.changeProperty('pb', value);
  }

  @action
  startPaddingChange(value) {
    this.changeProperty('ps', value);
  }

  @action
  endPaddingChange(value) {
    this.changeProperty('pe', value);
  }
}
