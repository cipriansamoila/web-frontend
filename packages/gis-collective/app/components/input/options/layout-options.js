import Component from './base-options';
import { guidFor } from '@ember/object/internals';

export default class LayoutOptionsComponent extends Component {
  elementId = `layout-options-${guidFor(this)}`;
}
