import Component from '@glimmer/component';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';
import {
  defaultBsProperty,
  setDefaultBsProperty,
  sizedBsProperty,
  setSizedBsProperty,
} from '../../../lib/bs-cols';

export default class InputBaseOptionsComponent extends Component {
  elementId = `options-section-${guidFor(this)}`;

  get type() {
    return this.args.type ?? 'type';
  }

  get options() {
    return this.args.options ?? [];
  }

  getProperty(property) {
    return this.args.size
      ? sizedBsProperty(property, this.args.size, this.options)
      : defaultBsProperty(property, this.options);
  }

  @action
  changeProperty(property, value) {
    const newOptions = this.args.size
      ? setSizedBsProperty(property, this.args.size, value, this.options)
      : setDefaultBsProperty(property, value, this.options);

    this.args.onChangeOptions?.(newOptions);
  }
}
