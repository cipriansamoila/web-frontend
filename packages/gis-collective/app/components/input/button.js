import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import * as Sentry from '@sentry/browser';

export default class InputButtonComponent extends Component {
  @tracked isWaiting;
  @tracked isError;

  @action
  async handleAction() {
    if (!this.args.action) {
      return;
    }

    this.isWaiting = true;
    this.isError = false;

    let result;

    try {
      result = await this.args.action();
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err);
      try {
        Sentry.captureEvent(err);
        // eslint-disable-next-line no-empty
      } catch (e) {}
      this.isError = true;
    }

    this.isWaiting = false;

    return result;
  }
}
