import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class InputIconsOrderComponent extends Component {
  @service fastboot;

  get showIconTypeLabel() {
    if (this.args.showIconTypeLabel === undefined) {
      return true;
    }

    return this.args.showIconTypeLabel;
  }

  get value() {
    if (!this.args.value && this.args.onSelect) {
      return [{ isAdd: true }];
    }

    let list = this.args.value?.toArray() ?? [];

    if (this.args.onSelect) {
      list = list.concat([{ isAdd: true }]);
    }

    return list;
  }

  @action
  dragEnd({ sourceList, sourceIndex, targetList, targetIndex }) {
    if (sourceIndex === targetIndex || sourceList !== targetList) return;

    const item = sourceList.objectAt(sourceIndex);
    sourceList.removeAt(sourceIndex);
    sourceList.insertAt(targetIndex, item);

    return this.triggerOnChange(sourceList);
  }

  @action
  primary(index) {
    if (index == 0) {
      return;
    }

    const list = this.value;

    return this.dragEnd({
      sourceList: list,
      sourceIndex: index,
      targetList: list,
      targetIndex: 0,
    });
  }

  @action
  remove(index) {
    let value = this.value;

    value.removeAt(index);
    this.triggerOnChange(value);
  }

  @action
  select() {
    if (this.args.onSelect) {
      return this.args.onSelect();
    }
  }

  async triggerOnChange(newValue) {
    if (!this.args.onChange) {
      return;
    }

    this.isLoading = true;
    let result;

    try {
      result = await this.args.onChange(newValue.filter((a) => !a.isAdd));
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err);
    }

    this.isLoading = false;

    return result;
  }
}
