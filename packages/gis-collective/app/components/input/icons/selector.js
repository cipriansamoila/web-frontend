import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputIconSelectorComponent extends Component {
  @tracked filterValue = '';

  get allIcons() {
    return this.flatten(this.args.icons);
  }

  flatten(group) {
    const icons = [];

    if (group && Array.isArray(group['icons']) && group['icons'].length > 0) {
      group['icons'].forEach((icon) => {
        icons.push(icon);
      });
    }

    if (group && group['categories']) {
      Object.keys(group['categories']).forEach((a) => {
        this.flatten(group['categories'][a]).forEach((icon) => {
          icons.push(icon);
        });
      });
    }

    return icons;
  }

  get hasSearchValue() {
    return this.filterValue.length > 2;
  }

  get hasNoResults() {
    return this.hasSearchValue && this.filteredIcons.length == 0;
  }

  get filteredIcons() {
    if (!this.hasSearchValue) {
      return [];
    }

    const value = this.filterValue.toLowerCase();

    return this.allIcons.filter(
      (a) => a.localName.toLowerCase().indexOf(value) != -1
    );
  }

  @action
  onSelectIcon(icon) {
    if (!this.args.onSelect) {
      return;
    }

    let value = this.args.value ? this.args.value.toArray() : [];

    const newList = value.filter((a) => a.id !== icon.id);

    newList.push(icon);

    return this.args.onSelect(newList);
  }

  @action
  onDeselectIcon(icon) {
    if (!this.args.onSelect) {
      return;
    }

    let value = this.args.value ? this.args.value.toArray() : [];
    const newList = value.filter((a) => a.id !== icon.id);

    return this.args.onSelect(newList);
  }
}
