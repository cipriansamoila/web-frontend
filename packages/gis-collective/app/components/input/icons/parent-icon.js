import Component from '@glimmer/component';
import { action } from '@ember/object';
import { task, timeout } from 'ember-concurrency';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class InputIconsParentIconComponent extends Component {
  @tracked _value;
  @tracked renderList = true;

  @task(function* (term) {
    yield timeout(600);

    if (!term || term.length <= 3) {
      return [];
    }

    return this.args.store.query('icon', { term: term });
  })
  searchTask;

  get value() {
    if (this._value === null) {
      return null;
    }

    if (this._value) {
      return this._value;
    }

    return this.args.value;
  }

  @action
  handleChange(icon) {
    this._value = icon;
    this.args.onChange?.(icon);
  }

  @action
  removeIcon() {
    this._value = null;
    this.args.onChange?.();
    this.renderList = false;

    later(() => {
      this.renderList = true;
    });
  }
}
