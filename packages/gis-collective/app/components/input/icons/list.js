import Component from '@glimmer/component';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { Modal } from 'bootstrap';

export default class InputIconsListComponent extends Component {
  @service fastboot;
  @tracked selectedCategory = '';
  @tracked selectedSubcategory = '';
  @tracked modalVisible;
  @tracked selection;

  @action
  setupModal(element) {
    this.modal = new Modal(element, { show: false });

    element.addEventListener('hidden.bs.modal', function () {
      this.modalVisible = false;
    });
  }

  willDestroy() {
    super.willDestroy();

    if (this.modal?._element) {
      this.modal?.dispose();
    }
  }

  @action
  openModal() {
    this.selection = this.args.value ?? [];
    this.modalVisible = true;

    if (this.modal?._element) {
      this.modal.show();
    }
  }

  @action
  hide() {
    if (this.modal?._element) {
      return this.modal.hide();
    }
  }

  @action
  selectionChanged(value) {
    this.selection = value;
  }

  @action
  orderChanged(value) {
    this.selection = value;
    this.args.onChange?.(value);
  }

  @action
  save() {
    this.args.onChange?.(this.selection);
    if (this.modal?._element) {
      return this.modal.hide();
    }
  }
}
