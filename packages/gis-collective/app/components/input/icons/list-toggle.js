import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputIconsListToggleComponent extends Component {
  @tracked list = [];

  get value() {
    return this.args.value || [];
  }

  @action
  updateList() {
    const selectedIds = this.args.value?.map((a) => a.id) ?? [];

    if (this.list.length == 0) {
      this.list = this.args.list?.map((icon) => new RenderedItem(icon)) ?? [];
    }

    this.list.forEach((item) => {
      item.isSelected = selectedIds.indexOf(item.icon.id) >= 0;
    });
  }

  @action
  toggle(icon) {
    let value = this.value;
    const exists = value.find((a) => a.id == icon.id);

    if (exists) {
      value = value.filter((a) => a.id != icon.id);
    } else {
      value.pushObject(icon);
    }

    this.args.onChange?.(value);
  }
}

class RenderedItem {
  @tracked icon;
  @tracked isSelected = false;

  constructor(icon) {
    this.icon = icon;
  }
}
