import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

export default class InputTextboxSuggestionsComponent extends Component {
  @tracked isListOpen = false;

  get filteredList() {
    const list = this.args.list;

    if (!this.args.value) {
      return [];
    }

    var result = list.filter(
      (a) =>
        a.toLowerCase().indexOf(this.args.value.toLowerCase()) >= 0 &&
        a != this.args.value
    );

    return result;
  }

  get hasVisibleList() {
    return this.filteredList.length > 0 && this.isListOpen;
  }

  @action
  closeList() {
    later(() => {
      this.isListOpen = false;
    }, 200);
  }

  @action
  openList() {
    this.isListOpen = true;
  }

  @action
  select(value) {
    this.args.onChange(value.target.innerText);
  }

  @action
  handleKeyPress(event) {
    if (event.key == 'Enter' || event.keyCode == 13) {
      const value = event.target.value.trim();

      this.args.onChange?.(value);
    }
  }
}
