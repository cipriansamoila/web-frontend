import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  checked: computed('value', function () {
    const value = this.value;

    if (typeof value == 'string') {
      return value.toLowerCase() === 'true' || value.toLowerCase() === 'yes';
    }

    return value === 1 || value === true;
  }),

  notChecked: computed('value', function () {
    const value = this.value;

    if (typeof value == 'string') {
      return value.toLowerCase() === 'false' || value.toLowerCase() === 'no';
    }

    return value === 0 || value === false;
  }),

  disabled: computed('value', function () {
    const value = this.value;
    return value === undefined || value === null;
  }),

  actions: {
    check: function () {
      this.set('value', true);
    },

    uncheck: function () {
      this.set('value', false);
    },

    disable: function () {
      this.set('value', undefined);
    },
  },
});
