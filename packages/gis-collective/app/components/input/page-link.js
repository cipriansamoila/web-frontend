import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputPageLinkComponent extends Component {
  @service store;
  @tracked internalPages;
  @tracked isExternal;
  elementId = 'input-page-link-' + guidFor(this);

  getPath(slug) {
    return `/` + slug.split('--').join('/');
  }

  get value() {
    return this.pages.find((a) => a.path == this.args.value);
  }

  get textValue() {
    return this.args.value ?? '';
  }

  set textValue(value) {
    this.isExternal = true;
    this.args.onChange?.(value);
  }

  get pages() {
    return (
      this.internalPages?.map((a) => ({
        id: a.id,
        path: this.getPath(a.slug),
        name: `${a.name} (${this.getPath(a.slug)})`,
      })) ?? []
    );
  }

  get hasExternalLink() {
    if (this.isExternal === true || this.isExternal === false) {
      return this.isExternal;
    }

    const value = this.args.value ?? '';

    return value.indexOf('http') == 0;
  }

  set hasExternalLink(value) {
    this.isExternal = value;
  }

  @action
  async setup() {
    this.internalPages = await this.store.findAll('page');
  }

  @action
  change(value) {
    this.args.onChange?.(value?.path);
  }
}
