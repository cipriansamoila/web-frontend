import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class InputContainerGroupComponent extends Component {
  @service fastboot;

  get isDisabled() {
    return this.fastboot.isFastBoot;
  }

  get isEditMode() {
    return this.args.title == this.args.editablePanel;
  }

  get isSaving() {
    return this.args.title == this.args.savingPanel;
  }

  get canEdit() {
    return !this.isEditMode && this.args.canEdit !== false;
  }

  get className() {
    if (!this.args.title) {
      return 'unknown';
    }

    return this.args.title.replace(/[^0-9a-z]/gi, '-').toLowerCase();
  }

  @action
  edit() {
    if (!this.args.onEdit) {
      return;
    }

    this.args.onEdit(this.args.title);
  }

  @action
  save() {
    if (!this.args.onSave) {
      return;
    }

    return this.args.onSave(this.args.title);
  }

  @action
  cancel() {
    if (!this.args.onCancel) {
      return;
    }

    return this.args.onCancel(this.args.title);
  }
}
