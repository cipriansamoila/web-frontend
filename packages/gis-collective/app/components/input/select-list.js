import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { guidFor } from '@ember/object/internals';

export default class InputSelectListComponent extends Component {
  elementId = 'select-list-' + guidFor(this);
  @tracked _values;
  @tracked _isEnabled = null;

  get values() {
    if (this._values) {
      return this._values;
    }

    if (this.args.values && this.args.values.toArray) {
      return this.args.values.toArray();
    }

    if (this.args.values && this.args.values.length) {
      return this.args.values;
    }

    return [];
  }

  get haveEnabledSwitch() {
    return this.args.isEnabled !== undefined;
  }

  get isEnabled() {
    if (!this.haveEnabledSwitch) {
      return true;
    }

    if (this._isEnabled === null) {
      return this.args.isEnabled;
    }

    return this._isEnabled;
  }

  set isEnabled(value) {
    this._isEnabled = value;
  }

  @action
  change(isEnabled, values) {
    this._isEnabled = isEnabled;
    this._values = values;
  }

  @action
  save() {
    if (!this.args.onSave) {
      return;
    }

    const validValues = this.values.filter(
      (a) => a !== null && a !== undefined
    );

    if (!this.isEnabled) {
      return this.args.onSave(this.args.title, [], false);
    }

    return this.args.onSave(this.args.title, validValues, true);
  }

  @action
  cancel() {
    this._values = null;
    this._isEnabled = null;

    if (!this.args.onCancel) {
      return;
    }

    this.args.onCancel(...arguments);
  }
}
