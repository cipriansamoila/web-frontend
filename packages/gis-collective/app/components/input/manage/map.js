import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import OptionalMap from '../../../lib/optional-map';

export default class InputManageMapComponent extends Component {
  elementId = 'map-' + guidFor(this);

  @tracked _isEnabled;
  @tracked _map;

  get enabledText() {
    if (this.args.enabledText) {
      return this.args.enabledText;
    }

    return 'manage-section.map-campaign';
  }

  get disabledText() {
    if (this.args.disabledText) {
      return this.args.disabledText;
    }

    return 'manage-section.no-map-campaign';
  }

  get isEnabled() {
    if (this._isEnabled !== null && this._isEnabled !== undefined) {
      return this._isEnabled;
    }

    if (!this.args.value || !this.args.value.isEnabled) {
      return false;
    }

    return this.args.value.isEnabled;
  }

  set isEnabled(value) {
    this._isEnabled = value;
  }

  get map() {
    if (this._map) {
      return this._map;
    }

    if (!this.args.value || !this.args.value.map) {
      return null;
    }

    return this.args.value.map;
  }

  @action
  updateMap(map) {
    this._map = map;
  }

  @action
  save() {
    if (!this.args.onSave) {
      return;
    }

    return this.args.onSave(
      new OptionalMap({
        isEnabled: this.isEnabled,
        map: this.map,
      })
    );
  }

  @action
  cancel() {
    this._map = null;
    this._isEnabled = null;

    if (this.args.onCancel) {
      return this.args.onCancel();
    }
  }
}
