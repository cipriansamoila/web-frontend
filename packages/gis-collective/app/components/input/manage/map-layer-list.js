import Component from '@glimmer/component';
import { action } from '@ember/object';

import BaseMapLayer from '../../../lib/base-map-layer';
import { tracked } from '@glimmer/tracking';

export default class InputManageMapLayerListComponent extends Component {
  @tracked values;

  @action
  copyValues() {
    this.values = [];

    if (!this.args.values) {
      return;
    }

    this.args.values
      .slice()
      .reverse()
      .forEach((layer) => {
        this.values.addObject(new BaseMapLayer(layer));
      });
  }

  @action
  addLayer() {
    this.values.addObject(new BaseMapLayer());
  }

  @action
  save() {
    if (this.args.onSave) {
      return this.args.onSave(this.values.slice().reverse());
    }
  }

  @action
  changeLayerType(type, index) {
    this.values.objectAt(index).type = type;
  }

  @action
  delete(index) {
    this.values.removeAt(index);
  }

  @action
  moveUp(index) {
    const tmp = this.values.objectsAt([index, index - 1]);
    const raw = this.values;
    raw[index] = tmp[1];
    raw[index - 1] = tmp[0];

    this.values.setObjects(raw);
  }

  @action
  moveDown(index) {
    const tmp = this.values.objectsAt([index, index + 1]);
    const raw = this.values;
    raw[index] = tmp[1];
    raw[index + 1] = tmp[0];

    this.values.setObjects(raw);
  }

  @action
  changeOption(index, options) {
    this.values.objectAt(index).options = options;
  }
}
