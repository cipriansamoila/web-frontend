import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  initList(value) {
    if (!Array.isArray(value)) {
      this.set('_list', []);
      return;
    }

    if (!Array.isArray(value)) {
      this.set('_list', []);
      return;
    }

    this.set('_list', JSON.parse(JSON.stringify(value)));
  },

  tmpList: computed('_list', 'list.[]', {
    get() {
      if (!this._list) {
        this.initList(this.list);
      }

      return this._list;
    },

    set(name, value) {
      this.initList(value);

      return this._list;
    },
  }),

  actions: {
    save() {
      if (this.onSave) {
        this.onSave(this.tmpList);
      }
    },

    remove(index) {
      this.tmpList.removeAt(index);
    },

    cancel() {
      this.initList(this.list);

      if (this.onCancel) {
        this.onCancel();
      }
    },

    addItem() {
      this.tmpList.push({
        text: '',
        url: '',
      });

      this.notifyPropertyChange('tmpList');
    },
  },
});
