import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class InputManageSelectWithVisibilityComponent extends Component {
  @action
  toggleAll() {
    if (this.args.toggleAll) {
      this.args.toggleAll(!this.args.areAllShowing);
    }
  }
}
