import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class InputManageCampaignsQuestionsComponent extends Component {
  @tracked _showNameQuestion = null;
  @tracked _nameLabel = null;
  @tracked _showDescriptionQuestion = null;
  @tracked _descriptionLabel = null;
  @tracked _iconsLabel = null;
  @tracked _icons = null;

  get icons() {
    let records = this._icons || this.args.icons?.slice() || [];

    return {
      data: {
        ids: records?.map((a) => a.id ?? a._id),
        records,
      },
    };
  }

  get showNameQuestion() {
    if (typeof this._showNameQuestion == 'boolean') {
      return this._showNameQuestion;
    }

    return this._showNameQuestion !== null
      ? this._showNameQuestion
      : this.args.value?.showNameQuestion || false;
  }

  set showNameQuestion(value) {
    this._showNameQuestion = value;
  }

  get nameLabel() {
    if (this._nameLabel === '') {
      return '';
    }

    return this._nameLabel || this.args.value?.nameLabel || '';
  }

  set nameLabel(value) {
    this._nameLabel = value;
  }

  get showDescriptionQuestion() {
    if (typeof this._showDescriptionQuestion == 'boolean') {
      return this._showDescriptionQuestion;
    }

    return this._showDescriptionQuestion !== null
      ? this._showDescriptionQuestion
      : this.args.value?.showDescriptionQuestion || false;
  }

  set showDescriptionQuestion(value) {
    this._showDescriptionQuestion = value;
  }

  get descriptionLabel() {
    if (this._descriptionLabel === '') {
      return '';
    }
    return this._descriptionLabel || this.args.value?.descriptionLabel || '';
  }

  set descriptionLabel(value) {
    this._descriptionLabel = value;
  }

  get iconsLabel() {
    if (this._iconsLabel === '') {
      return '';
    }
    return this._iconsLabel || this.args.value?.iconsLabel || '';
  }

  set iconsLabel(value) {
    this._iconsLabel = value;
  }

  @action
  iconsChanged(value) {
    this._icons = value;
  }

  @action
  save() {
    return this.args.onSave(
      {
        showNameQuestion: this.showNameQuestion,
        nameLabel: this.nameLabel,
        showDescriptionQuestion: this.showDescriptionQuestion,
        descriptionLabel: this.descriptionLabel,
        iconsLabel: this.iconsLabel,
      },
      this.icons.data.records
    );
  }

  @action
  cancel() {
    this._showNameQuestion = null;
    this._nameLabel = null;
    this._showDescriptionQuestion = null;
    this._descriptionLabel = null;
    this._iconsLabel = null;

    return this.args.onCancel();
  }
}
