import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputManageTextComponent extends Component {
  @tracked value = '';

  @action
  setupValue() {
    this.value = this.args.value;
  }

  @action
  save() {
    return this.args.onSave(this.value);
  }
}
