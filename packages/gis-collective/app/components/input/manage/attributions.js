import Component from '@ember/component';
import { later, cancel } from '@ember/runloop';

export default Component.extend({
  raiseFocusOut() {
    if (this.focusOut) {
      this.focusOut();
    }
  },

  actions: {
    focusIn() {
      cancel(this.outTimer);
    },

    focusOut() {
      const timer = later(() => {
        this.raiseFocusOut();
      }, 5);

      this.set('outTimer', timer);
    },
  },
});
