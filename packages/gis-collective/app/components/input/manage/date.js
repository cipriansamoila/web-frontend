import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';

export default class InputManageDateComponent extends Component {
  @tracked _value = null;

  get value() {
    if (this._value === null) {
      return new Date(this.args.value.getTime());
    }

    return this._value;
  }

  @action
  onChange(value) {
    this._value = value;
  }

  @action
  save() {
    if (this.args.onSave) {
      return this.args.onSave(this.value);
    }
  }

  @action
  cancel() {
    this._value = null;

    if (this.args.onCancel) {
      return this.args.onCancel();
    }
  }
}
