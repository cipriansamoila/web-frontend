import Component from '@glimmer/component';
import { action } from '@ember/object';
import { LayoutRow, LayoutCol } from '../../../transforms/layout-row-list';
import { LayoutContainer } from '../../../transforms/layout-container-list';
import { tracked } from '@glimmer/tracking';

export default class InputLayoutComponent extends Component {
  @tracked selectedContainer;

  getNewContainer() {
    const newRow = new LayoutRow({
      cols: [new LayoutCol()],
    });

    return new LayoutContainer({ rows: [newRow] });
  }

  @action
  removeContainer(index) {
    const newLayout = this.args.containers.slice();
    newLayout.removeAt(index);
    this.args.onLayoutChange?.(newLayout, 'removeContainer', index);
  }

  @action
  configContainer(index) {
    this.args.onContainerEdit?.(index);
  }

  @action
  addContainerBefore(index) {
    const newLayout = this.args.containers.slice();
    newLayout.insertAt(index, this.getNewContainer());

    this.args.onLayoutChange?.(newLayout, 'containerBefore', index);
  }

  @action
  addContainerEnd() {
    const newLayout = this.args.containers.slice();

    newLayout.insertAt(this.args.containers.length, this.getNewContainer());
    this.args.onLayoutChange?.(newLayout, 'containerEnd');
  }

  @action
  onLayoutChange(index, rows, operation, rowIndex, colIndex) {
    const newLayout = this.args.containers.slice();
    newLayout.objectAt(index).rows = rows;

    return this.args.onLayoutChange?.(
      newLayout,
      'rowChange',
      index,
      operation,
      rowIndex,
      colIndex
    );
  }

  @action
  onColSelect(index, row, col) {
    return this.args.onColSelect?.(index, row, col);
  }

  @action
  onRowEdit(index, row) {
    return this.args.onRowEdit?.(index, row);
  }

  @action
  onRowSelect(index) {
    this.selectedContainer = index;
  }
}
