import Component from '@glimmer/component';
import { action } from '@ember/object';
import { LayoutCol } from '../../../transforms/layout-row-list';
import { tracked } from '@glimmer/tracking';

export default class InputLayoutColsComponent extends Component {
  @tracked _cols;

  get cols() {
    const cols = this._cols || this.args.cols || [];

    return cols.map((a) => (a.isLayoutCol ? a : new LayoutCol(a)));
  }

  get lastIndex() {
    if (!this.cols) {
      return 0;
    }

    return this.cols.length - 1;
  }

  @action
  select(index, event) {
    this.args.onSelect?.(index);
    event.preventDefault();
  }

  @action
  addCol(index, event) {
    const newLayout = this.cols?.slice() ?? [];
    const newRow = new LayoutCol();

    newLayout.insertAt(index, newRow);

    this.args.onLayoutChange?.(newLayout);
    this._cols = newLayout;

    event.preventDefault();
  }

  @action
  deleteCol(index, event) {
    const newLayout = this.cols?.slice() ?? [];

    newLayout.removeAt(index);

    this.args.onLayoutChange?.(newLayout, 'deleteCol', index);
    this._cols = newLayout;

    event.preventDefault();
  }
}
