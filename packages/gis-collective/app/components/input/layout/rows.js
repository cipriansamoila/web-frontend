import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { LayoutRow, LayoutCol } from '../../../transforms/layout-row-list';
import { getSizeFromWidth } from '../../../lib/responsive';

export default class InputLayoutComponent extends Component {
  @tracked editingStartEdge;
  @tracked editingEndEdge;
  @tracked selectedCol;
  @tracked width;

  get size() {
    return getSizeFromWidth(this.width);
  }

  willDestroy() {
    super.willDestroy(...arguments);
    this.resizeObserver?.disconnect?.();
  }

  @action
  setupResizeObserver(element) {
    this.resizeObserver = new ResizeObserver(() => {
      this.width = element.clientWidth;
    });

    this.resizeObserver.observe(element);
  }

  @action
  selectCol(index, colIndex) {
    this.enableRowEdit(index);
    this.selectedCol = colIndex;

    this.args.onColSelect?.(index, colIndex);
    this.args.onRowSelect?.();
  }

  @action
  rowEdit(index) {
    this.args.onRowEdit?.(index);
    this.args.onRowSelect?.();
  }

  @action
  enableRowEdit(index) {
    this.editingStartEdge = index;
    this.editingEndEdge = index + 1;
    this.selectedCol = -1;
    this.args.onRowSelect?.();
  }

  @action
  addRowBefore(index) {
    const newLayout = this.args.rows.slice();
    const newRow = new LayoutRow({
      cols: [new LayoutCol()],
    });

    newLayout.insertAt(index, newRow);

    this.args.onLayoutChange?.(newLayout, 'addRowBefore', index);
    this.args.onRowSelect?.();
  }

  @action
  addRowEnd() {
    const newLayout = this.args.rows.slice();
    const newRow = new LayoutRow({
      cols: [new LayoutCol()],
    });

    newLayout.insertAt(this.args.rows.length, newRow);

    this.args.onLayoutChange?.(newLayout, 'addRowEnd');
    this.args.onRowSelect?.();
  }

  @action
  colsChange(index, cols, operation, colIndex) {
    const newLayout = this.args.rows.slice();
    newLayout.objectAt(index).cols = cols;

    if (cols.length == 0) {
      newLayout.removeAt(index);
    }

    this.args.onLayoutChange?.(newLayout, operation, index, colIndex);
    if (cols.length == 0) {
      this.args.onLayoutChange?.(newLayout, 'removeRow', index);
    }
    this.args.onRowSelect?.();
  }
}
