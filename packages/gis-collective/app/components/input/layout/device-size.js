import Component from '@glimmer/component';
import { action } from '@ember/object';

export default class InputLayoutDeviceSizeComponent extends Component {
  @action
  select(value) {
    if (value == this.args.value) {
      return this.args.onChange?.(null);
    }
    this.args.onChange?.(value);
  }
}
