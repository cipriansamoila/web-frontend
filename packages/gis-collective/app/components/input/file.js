import Component from '@glimmer/component';
import { guidFor } from '@ember/object/internals';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputFileComponent extends Component {
  inputId = 'fileInput-' + guidFor(this);

  @tracked fileName;

  triggerMimeChange(value) {
    if (this.args.onMimeSelect) {
      this.args.onMimeSelect(value);
    }
  }

  triggerNameChange(value) {
    this.fileName = value;

    if (this.args.onNameChange) {
      this.args.onNameChange(value);
    }
  }

  triggerTextContentChange(name, value) {
    if (this.args.onTextContentChange) {
      this.args.onTextContentChange(name, value);
    }
  }

  readTextContent(blob) {
    const reader = new FileReader();

    reader.onload = () => {
      this.triggerTextContentChange(blob.name, reader.result);
    };

    reader.readAsText(blob);
  }

  triggerBaseUrlContentChange(name, value) {
    if (this.args.onBaseUrlContentChange) {
      this.args.onBaseUrlContentChange(name, value);
    }
  }

  readBaseUrlContent(blob) {
    const reader = new FileReader();

    reader.onload = () => {
      this.triggerBaseUrlContentChange(blob.name, reader.result);
    };

    reader.readAsDataURL(blob);
  }

  @action
  handleFile(event) {
    if (event.target.files.length == 0) {
      this.triggerNameChange(null);
      this.triggerMimeChange(null);
      this.triggerTextContentChange(null, null);
      this.triggerBaseUrlContentChange(null, null);
      return;
    }

    const file = event.target.files[0];

    if (file.type) {
      this.triggerMimeChange(file.type);
    }

    if (file.name) {
      this.triggerNameChange(file.name);
    }

    if (this.args.onChange) {
      this.args.onChange(file);
    }

    if (this.args.onTextContentChange) {
      this.readTextContent(file);
    }

    if (this.args.onBaseUrlContentChange) {
      this.readBaseUrlContent(file);
    }
  }
}
