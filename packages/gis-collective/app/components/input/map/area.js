import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';
import { createBox } from 'ol/interaction/Draw';
import Polygon from 'ol/geom/Polygon';
import { fromLonLat } from 'ol/proj';
import { tracked } from '@glimmer/tracking';
import { getSize } from 'ol/extent';
import GeoJson from '../../../lib/geoJson';

export default class InputMapAreaComponent extends Component {
  @service fullscreen;

  @tracked _tmpValue;
  @tracked _baseMap;
  @tracked isFullscreen;

  mapPadding = [50, 50, 50, 50];

  get tmpValue() {
    if (!this._tmpValue || this.disabled) {
      return new GeoJson(this.args.value);
    }

    return this._tmpValue;
  }

  set tmpValue(value) {
    this._tmpValue = value;

    this.args.onChange?.(value);
  }

  get polygon() {
    if (!this.args.value?.coordinates[0]) {
      return null;
    }

    const coordinates = this.args.value.coordinates[0].map((a) =>
      fromLonLat(a)
    );
    const polygon = new Polygon([coordinates]);

    return polygon;
  }

  get extent() {
    if (!this.polygon || this._tmpValue) {
      return null;
    }

    return this.polygon.getExtent();
  }

  get createBox() {
    return createBox();
  }

  get selectedBaseMap() {
    return this._baseMap || this.args.baseMaps?.firstObject;
  }

  @action
  selectBaseMap(value) {
    this._baseMap = value;
  }

  @action
  resetValue() {
    this._tmpValue = undefined;
  }

  @action
  featureChange(ev) {
    const coordinates = ev.features
      .item(0)
      .getGeometry()
      .transform('EPSG:3857', 'EPSG:4326')
      .getCoordinates();

    const newGeometry = this.tmpValue;

    if (newGeometry.set) {
      newGeometry.set('coordinates', coordinates);
    } else {
      newGeometry.coordinates = coordinates;
    }

    this.tmpValue = newGeometry;
  }

  @action
  onNewPolygon(ev) {
    const coordinates = ev.feature
      .getGeometry()
      .transform('EPSG:3857', 'EPSG:4326')
      .getCoordinates();

    const extentSize = getSize(ev.feature.getGeometry().getExtent());

    if (extentSize[0] <= 0.001 || extentSize[1] <= 0.001) {
      return;
    }

    const newGeometry = this.tmpValue;

    if (newGeometry.set) {
      newGeometry.set('coordinates', coordinates);
    } else {
      newGeometry.coordinates = coordinates;
    }

    this.tmpValue = newGeometry;
  }

  @action
  polygonChange(value) {
    this.tmpValue = value;
  }

  @action
  positionFullscreen(value) {
    this.fullscreen.isEnabled = value;
    this.isFullscreen = value;
  }
}
