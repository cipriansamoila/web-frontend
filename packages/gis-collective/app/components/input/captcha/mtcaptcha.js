import Component from '@glimmer/component';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class InputCaptchaMtcaptchaComponent extends Component {
  @service fastboot;

  @action
  setupMtCaptcha() {
    if (this.fastboot.isFastBoot) {
      return;
    }

    const s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.innerHTML = `var mtcaptchaConfig = {"sitekey": "${this.args.siteKey}"};`;
    document.body.appendChild(s);

    const mt_service = document.createElement('script');
    mt_service.src =
      'https://service.mtcaptcha.com/mtcv1/client/mtcaptcha.min.js';
    mt_service.async = true;
    document.body.appendChild(mt_service);

    const mt_service2 = document.createElement('script');
    mt_service2.src =
      'https://service2.mtcaptcha.com/mtcv1/client/mtcaptcha2.min.js';
    mt_service2.async = true;
    document.body.appendChild(mt_service2);

    // eslint-disable-next-line no-undef
    mtcaptchaConfig['verified-callback'] = (state) => {
      this.args.onResponse?.(state.verifiedToken);
    };
  }

  //updateResponse
}
