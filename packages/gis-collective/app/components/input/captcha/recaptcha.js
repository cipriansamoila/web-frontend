import Component from '@glimmer/component';
import { run } from '@ember/runloop';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

/* global grecaptcha */
export default class InputRecaptchaComponent extends Component {
  @service fastboot;
  @tracked token = '';
  @tracked hasToken = false;

  raiseResponse(token) {
    this.args.onResponse?.(token);
  }

  receiveToken() {
    grecaptcha.ready(() => {
      grecaptcha
        .execute(this.args.siteKey, { action: 'login' })
        .then((token) => {
          this.hasToken = true;
          this.raiseResponse(token);
        });
    });
  }

  getScript(element, scriptUrl, callback) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    this.script = document.createElement('script');
    this.script.src = scriptUrl;
    this.script.onload = callback;

    element.appendChild(this.script);
  }

  @action
  elementInsert(element) {
    this.element = element;

    run(() => {
      this.getScript(
        element,
        `https://www.google.com/recaptcha/api.js?render=${this.args.siteKey}`,
        () => {
          run(() => {
            this.receiveToken();
          });
        }
      );
    });
  }

  willDestroy() {
    super.willDestroy(...arguments);

    if (this.fastboot.isFastBoot) {
      return;
    }

    const elem = document.body.querySelector('.grecaptcha-badge');
    elem.parentNode.removeChild(elem);
  }
}
