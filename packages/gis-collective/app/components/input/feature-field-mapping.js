import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { guidFor } from '@ember/object/internals';

export default class InputFeatureFieldMappingComponent extends Component {
  elementId = `editor-js-${guidFor(this)}`;

  options = [
    '',
    '_id',
    'name',
    'description',
    'visibility',
    'info.createdOn',
    'info.lastChangeOn',
    'info.changeIndex',
    'info.author',
    'info.originalAuthor',
    'position',
    'position.lon',
    'position.lat',
    'pictures',
    'sounds',
    'icons',
    'maps',
    'contributors',
  ];

  @tracked _value = null;
  @tracked _groupName = null;
  @tracked _propertyName = null;
  @tracked _isCustom = null;

  get hasInvalidGroupName() {
    if (this.groupName.trim() === '') {
      return true;
    }

    if (this.groupName.indexOf('.') >= 0) {
      return true;
    }

    return false;
  }

  get hasInvalidPropertyName() {
    if (this.propertyName.trim() === '') {
      return true;
    }

    if (this.propertyName.indexOf('.') >= 0) {
      return true;
    }

    return false;
  }

  get isCustom() {
    if (this._isCustom !== null) {
      return this._isCustom;
    }

    if (!this.args.value) {
      return false;
    }

    return this.options.indexOf(this.args.value) == -1;
  }

  set isCustom(value) {
    this._isCustom = value;

    if (value) {
      this.raiseAttributeValue();
    } else {
      this.selectChanged('');
    }
  }

  raiseAttributeValue() {
    if (this.hasInvalidGroupName || this.hasInvalidPropertyName) {
      return this.args.onChange?.(``);
    }

    this.args.onChange?.(`attributes.${this.groupName}.${this.propertyName}`);
  }

  @action
  selectChanged(value) {
    this.value = value;
    this.raiseValue(value);
  }

  @action
  handleBlur() {
    this.raiseAttributeValue();
  }

  raiseValue(value) {
    if (!this.args.onChange) {
      return;
    }

    this.args.onChange(value);
  }

  get groupName() {
    if (this._groupName !== null) {
      return this._groupName;
    }

    if (this.pieces.length < 2) {
      return 'other';
    }

    return this.pieces[this.pieces.length - 2];
  }

  set groupName(value) {
    this._groupName = value;
  }

  get propertyName() {
    if (this._propertyName !== null) {
      return this._propertyName;
    }
    if (this.pieces.length == 0) {
      return (this.args.key ?? '_unknown').replace('.', '_');
    }

    return this.pieces[this.pieces.length - 1];
  }

  set propertyName(value) {
    this._propertyName = value;
  }

  get pieces() {
    const value = this.args.value ?? '';

    return value
      .split('.')
      .map((a) => a.trim())
      .filter((a) => a != '' && a != 'attributes');
  }

  get value() {
    if (this._value === null) {
      return this.args.value;
    }

    return this._value;
  }

  set value(newValue) {
    this._value = newValue;
  }
}
