import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class InputImageListComponent extends Component {
  @tracked isLoading = false;
  @tracked rnd = [];

  get value() {
    if (!this.args.value) {
      return [{ isAdd: true }];
    }

    return this.args.value.toArray().concat([{ isAdd: true }]);
  }

  @action
  remove(index) {
    const values = this.value;
    values.removeAt(index);

    this.triggerOnChange(values);
  }

  @action
  async rotate(index) {
    const values = this.value;
    await values[index].rotate();

    return this.triggerOnChange(values);
  }

  @action
  toggle360(index) {
    const values = this.value;
    values[index].is360 = !values[index].is360;

    return values[index].save();
  }

  @action
  dragEnd({ sourceList, sourceIndex, targetList, targetIndex }) {
    if (sourceIndex === targetIndex || sourceList !== targetList) return;

    const item = sourceList.objectAt(sourceIndex);
    sourceList.removeAt(sourceIndex);
    sourceList.insertAt(targetIndex, item);

    this.triggerOnChange(targetList);
  }

  async triggerOnChange(newValue) {
    const onChange = this.args.onChange || this.args.onchange;

    if (!onChange) {
      return;
    }

    this.isLoading = true;

    newValue = newValue.filter((a) => a.picture);

    let result;

    try {
      result = await onChange(newValue);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err);
    }

    this.isLoading = false;

    return result;
  }

  cameraSuccess(imageURI) {
    if (!this.args.createImage) {
      return alert('`createImage` action is not set');
    }

    const picture = this.args.createImage();
    picture.set('picture', imageURI);

    const newValue = this.value.concat([picture]);
    this.triggerOnChange(newValue);
  }

  @action
  imageRemoved(index) {
    this.args.value.removeAt(index);
    this.triggerOnChange();
  }

  @action
  setupFileInput(element) {
    this.fileElement = element;
  }

  @action
  selectFile() {
    this.fileElement.click();
  }

  @action
  fileSelected(event) {
    var file = event.target.files[0];

    if (!file) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = (event) => {
      this.cameraSuccess(event.target.result);
    };
  }
}
