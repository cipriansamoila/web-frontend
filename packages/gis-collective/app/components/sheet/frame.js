import Component from '@glimmer/component';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';

export default class SheetFrameComponent extends Component {
  firstVisible = -1;
  lastVisible = -1;
  @tracked selectedCustomField;
  @tracked _row;
  @tracked options;
  @tracked hasInlineEditor;

  get columns() {
    return this.args.cols;
  }

  get rows() {
    return [];
  }

  get sortField() {
    return this.args.sorts?.field;
  }

  get sortOrder() {
    return this.args.sorts?.isAscending ?? false;
  }

  selectRow(row, event) {
    this.deselectCell();

    this._row?.cellMeta?.set('selected', false);
    this._row?.columnMeta?.set('selected', false);
    this._row?.rowMeta?.set('selected', false);

    this._row = row;

    this._row.cellMeta.set('selected', true);
    this._row.columnMeta.set('selected', true);
    this._row.rowMeta.set('selected', true);

    this._selectedCell = event.target;

    this.options = {
      ...row.columnValue.options,
      parent: this._row?.rowValue?.id,
    };
  }

  @action
  fixInterface(element) {
    if (navigator?.userAgent.toLowerCase().indexOf('firefox') != -1) {
      return;
    }

    element.querySelector('table').style.height = '100%';
  }

  @action
  onUpdateSorts() {
    return this.args.onUpdateSorts?.(...arguments);
  }

  @action
  selectCustomCell(row, event) {
    if (row.columnValue.typeClass != 'editor-custom') {
      return;
    }

    this.selectRow(row, event);

    this.selectedCustomField =
      this._row.columnValue.typeClass == 'editor-custom'
        ? this._row.columnValue.name
        : undefined;
  }

  @action
  selectCell(row, event) {
    this.selectRow(row, event);
  }

  raiseOnScroll() {
    if (this.firstVisible == -1 || this.lastVisible == -1) {
      return;
    }

    this.args.onScroll?.(this.firstVisible, this.lastVisible);
  }

  @action
  deselectCell() {
    this.selectedCustomField = undefined;

    if (!this._row || this._row.columnValue.typeClass != 'editor-inline') {
      return;
    }

    let value = this._selectedCell.innerText.trim();

    if (this._row.columnValue.editorType == 'inline-integer') {
      value = parseInt(value);
    }

    if (this._row.columnValue.editorType == 'inline-decimal') {
      value = parseFloat(value);
    }

    this.args.onEdit?.(this._row.rowValue, this._row.columnValue, value);
  }

  @action
  changeCustomValue(value) {
    this.args.onEdit?.(this._row.rowValue, this._row.columnValue, value);
  }

  @action
  firstVisibleChanged(api, value) {
    this.firstVisible = value;
    this.raiseOnScroll();
  }

  @action
  lastVisibleChanged(api, value) {
    this.lastVisible = value;
    this.raiseOnScroll();
  }
}
