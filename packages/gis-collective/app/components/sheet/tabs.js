import Component from '@glimmer/component';
import { action } from '@ember/object';
import { later, cancel } from '@ember/runloop';
import { tracked } from '@glimmer/tracking';

export default class SheetTabsComponent extends Component {
  destination = 0;
  step = 20;
  @tracked element;

  get width() {
    return this.element.clientWidth - 50;
  }

  get showScroll() {
    if (!this.element) {
      return false;
    }

    return this.args.icons?.length > 0 && this.element.scrollWidth > this.width;
  }

  handleAnimation() {
    cancel(this.timer);

    this.timer = later(() => {
      const diff = this.destination - this.element.scrollLeft;

      if (Math.abs(diff) > this.step) {
        this.element.scrollLeft += Math.ceil(diff / this.step);
      } else {
        this.element.scrollLeft += diff / Math.abs(diff);
      }

      if (Math.abs(diff) > 3) {
        this.handleAnimation();
      } else {
        this.element.scrollLeft = this.destination;
        cancel(this.timer);
      }
    }, 15);
  }

  @action
  setup(element) {
    this.element = element;
  }

  @action
  select(value) {
    this.args.onSelect?.(value);
  }

  @action
  nextPage() {
    this.step = 20;
    this.destination += this.width;
    this.destination = Math.min(this.destination, this.element.scrollWidth);

    this.handleAnimation();
  }

  @action
  prevPage() {
    this.step = 20;
    this.destination -= this.width;
    this.destination = Math.max(0, this.destination);

    this.handleAnimation();
  }
}
