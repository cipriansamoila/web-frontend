import Service, { inject as service } from '@ember/service';
import { storageFor } from 'ember-local-storage';
import { tracked } from '@glimmer/tracking';
import ModelInfo from '../lib/model-info';
import GeoJson from '../lib/geoJson';
import { all } from 'rsvp';
import EmberObject from '@ember/object';

export default class PositionService extends Service {
  @storageFor('pendingSite') pendingSite;
  @storageFor('pendingPictures') pendingPictures;
  @service fastboot;
  @service store;
  _status = null;

  get status() {
    if (!this._status) {
      this._status = new PendingDataStatus(
        this.pendingSite,
        this.pendingPictures
      );
    }

    return this._status;
  }

  reset() {
    try {
      this.pendingSite.reset();
      this.pendingPictures.reset();
      // eslint-disable-next-line no-empty
    } catch (err) {}

    this.status.update(this.pendingSite, this.pendingPictures);
  }

  checkStorage() {
    if (this.fastboot.isFastBoot) {
      return;
    }

    try {
      JSON.parse(window.localStorage.getItem('storage:pending-pictures'));
    } catch (err) {
      window.localStorage.removeItem('storage:pending-pictures');
      // eslint-disable-next-line no-console
      console.log('the local storage is corrupted: ', err);
      this.pendingPictures.set('isCorrupted', true);
    }

    try {
      JSON.parse(window.localStorage.getItem('storage:pending-site'));
    } catch (err) {
      window.localStorage.removeItem('storage:pending-site');
      // eslint-disable-next-line no-console
      console.log('the local storage is corrupted: ', err);
      this.pendingSite.set('isCorrupted', true);
    }
  }

  findRecord(model, id) {
    return new Promise((resolve) => {
      this.store
        .findRecord(model, id)
        .then(resolve)
        .catch(() => {
          resolve(null);
        });
    });
  }

  get feature() {
    if (!this.status.isRestored || this.status.isCorrupted) {
      return null;
    }

    const feature = Object.assign(this.pendingSite.get('lastKnownValue'));
    const pictures = Object.assign(this.pendingPictures.get('lastKnownValue'));

    return all(feature.icons.map((a) => this.findRecord('icon', a))).then(
      (icons) => {
        return all(feature.maps.map((a) => this.findRecord('map', a))).then(
          (maps) => {
            return all(
              pictures.stored.map((a) => this.findRecord('picture', a))
            ).then((existingPictures) => {
              feature.icons = icons.filter((a) => a != null);
              feature.maps = maps.filter((a) => a != null);
              feature.pictures = existingPictures
                .filter((a) => a != null)
                .concat(
                  pictures.new.map((picture) =>
                    this.store.createRecord('picture', { name: '', picture })
                  )
                );
              feature.info = new ModelInfo();
              feature.contributors = [];
              feature.position = new GeoJson(feature.position);
              feature.attributes = EmberObject.create(feature.attributes);

              return this.store.createRecord('feature', feature);
            });
          }
        );
      }
    );
  }
}

class PendingDataStatus {
  @tracked isRestored;
  @tracked isCorrupted;

  constructor(pendingSite, pendingPictures) {
    this.update(pendingSite, pendingPictures);
  }

  update(pendingSite, pendingPictures) {
    this.isCorrupted =
      pendingSite.get('isCorrupted') || pendingPictures.get('isCorrupted');
    this.isRestored = !this.isCorrupted && !pendingSite.isInitialContent();
  }
}
