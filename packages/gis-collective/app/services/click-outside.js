import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';

export default class ClickOutsideService extends Service {
  @service fastboot;

  @tracked element;
  @tracked event;

  constructor() {
    super(...arguments);

    if (this.fastboot.isFastBoot) {
      return;
    }

    document.addEventListener('click', (event) => {
      if (!this.element) {
        return;
      }

      const isClickInside =
        this.element.contains(event.target) ||
        event.target.dataset?.alwaysInside;

      if (!isClickInside) {
        this.unsubscribe();
      }
    });

    super.init(...arguments);
  }

  get hasSubscriber() {
    return Boolean(this.element && this.event);
  }

  subscribe(element, event) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    if (element == this.element) {
      return;
    }

    this.unsubscribe();

    if (!document.body.contains(element)) {
      return;
    }

    this.element = element;
    this.event = event;
  }

  unsubscribe() {
    if (this.fastboot.isFastBoot) {
      return;
    }

    if (!this.event || !document.body.contains(this.element)) {
      return;
    }

    this.event();

    this.event = null;
    this.element = null;
  }
}
