import Service, { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class LoadingService extends Service {
  @service fastboot;
  @tracked _isLoading;
  @tracked isFirst;

  get isLoading() {
    if (this.fastboot.isFastBoot) {
      return true;
    }

    return this._isLoading;
  }

  set isLoading(value) {
    this._isLoading = value;
  }
}
