import Service from '@ember/service';
import { storageFor } from 'ember-local-storage';
import { tracked } from '@glimmer/tracking';

export default class SearchStorageService extends Service {
  @storageFor('searchHistory') history;
  @tracked term = '';

  add(term) {
    if (!term) {
      return;
    }

    this.history.pushObject(term);
    this.history.setObjects(this.history.uniq());

    if (this.history.length > 5) {
      this.history.setObjects(this.history.slice(1, 6));
    }
  }
}
