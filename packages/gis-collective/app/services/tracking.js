import Service from '@ember/service';
import { inject as service } from '@ember/service';

export default class TrackingService extends Service {
  @service preferences;
  @service fastboot;

  trackers = {};

  registerTracker(name, tracker) {
    if (this.trackers[name]) {
      return;
    }

    this.trackers[name] = tracker;
  }

  addPage() {
    for (const tracker of Object.values(this.trackers)) {
      tracker.addPage?.(...arguments);
    }
  }

  addMapView() {
    for (const tracker of Object.values(this.trackers)) {
      tracker.addMapView?.(...arguments);
    }
  }
}
