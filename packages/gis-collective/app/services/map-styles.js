import Service from '@ember/service';
import { Fill, Style, Icon } from 'ol/style';
import { tracked } from '@glimmer/tracking';
import config from '../config/environment';
import { inject as service } from '@ember/service';
import StyleFactory from '../lib/map/style-factory';
import StyleCache from '../lib/map/style-cache';
import LineStyleProperties from '../lib/line-style-properties';
import PointStyleProperties from '../lib/point-style-properties';
import LabelStyleProperties from '../lib/label-style-properties';

export default class MapStylesService extends Service {
  @service store;

  debugTiles = config.debugTiles;
  selectedZIndex = 99999;
  iconSize = 21;

  @tracked _mapStyleElement;
  changeObservers = {};

  constructor() {
    super(...arguments);

    this.cache = new StyleCache();
    this.styleFactory = new StyleFactory();

    this.defaultShapeStyle = new LineStyleProperties();
    this.defaultPointStyle = new PointStyleProperties();

    this.pendingPointStyle = new PointStyleProperties();
    this.selectedPointStyle = new PointStyleProperties();

    this.debugText = new LabelStyleProperties();

    this.debugShapeStyle = {
      0: new LineStyleProperties(),
      10: new LineStyleProperties(),
      20: new LineStyleProperties(),
      30: new LineStyleProperties(),
    };

    this.groupTextStyle = [
      new LabelStyleProperties(),
      new LabelStyleProperties(),
      new LabelStyleProperties(),
      new LabelStyleProperties(),
      new LabelStyleProperties(),
      new LabelStyleProperties(),
      new LabelStyleProperties(),
      new LabelStyleProperties(),
    ];

    this._groupStyle = [
      new PointStyleProperties(),
      new PointStyleProperties(),
      new PointStyleProperties(),
      new PointStyleProperties(),
      new PointStyleProperties(),
      new PointStyleProperties(),
      new PointStyleProperties(),
      new PointStyleProperties(),
    ];

    this._groupStyle.forEach((style) => {
      style.shape = 'hexagon';
    });
  }

  get mapStyleElement() {
    return this._mapStyleElement;
  }

  set mapStyleElement(value) {
    this.styleFactory.mapStyleElement = value;
    this.styleFactory.update(`debug-0`, this.debugShapeStyle[0]);
    this.styleFactory.update(`debug-10`, this.debugShapeStyle[10]);
    this.styleFactory.update(`debug-20`, this.debugShapeStyle[20]);
    this.styleFactory.update(`debug-30`, this.debugShapeStyle[30]);

    this.styleFactory.update(`pending-point`, this.pendingPointStyle);

    this.styleFactory.update(`selected-point`, this.selectedPointStyle);

    this.groupTextStyle.forEach((style, key) => {
      this.styleFactory.update(`group-text-${key}`, style);
      style.baseline = 'middle';
      style.borderColor = 'transparent';
    });

    this._groupStyle.forEach((style, key) => {
      this.styleFactory.update(`group-${key}`, style);
    });
  }

  get styleFunction() {
    return (feature, resolution, selectedId) => {
      if (!feature) {
        return [];
      }

      let properties = {};

      if (feature.getProperties) {
        properties = feature.getProperties();
      }

      return this.fromProperties(properties, selectedId);
    };
  }

  watchChange(key, f) {
    this.changeObservers[key] = f;
  }

  unwatchChange(key) {
    delete this.changeObservers[key];
  }

  raiseChange() {
    this.onChange?.();

    Object.keys(this.changeObservers).forEach((key) => {
      const f = this.changeObservers[key];
      f?.();
    });
  }

  badgeStyle(size, type) {
    const key = `${type}-${size}`;

    if (!this.cache.styles[key]) {
      this.cache.styles[key] = new PointStyleProperties();
      this.styleFactory.update(`${type}-point-badge`, this.cache.styles[key]);
      this.cache.styles[key].size = size;
    }

    return this.cache.styles[key];
  }

  debugStyle(properties) {
    if (!this.debugTiles) {
      return [];
    }

    if (!properties.name) {
      const size = parseInt(properties.size) || 0;
      return [this.debugShapeStyle[size].toOlStyle()];
    }

    const key = `debug-text-${properties.name}`;

    if (!this.cache.styles[key]) {
      this.cache.styles[key] = this.debugText.clone().toOlStyle();
      this.cache.styles[key].getText().setText(properties.name);
    }

    return [this.cache.styles[key]];
  }

  groupStyle(properties) {
    const styles = [];

    styles.push(this.group(properties));
    styles.push(this.groupText(properties));

    return styles;
  }

  fromProperties(properties, selectedId) {
    if (properties.layer == 'debug') {
      return this.debugStyle(properties);
    }

    if (properties.layer == 'groups') {
      return this.groupStyle(properties);
    }

    const isPending = properties.visibility == '-1';

    if (properties.icons) {
      properties.icon = getIcon(properties.icons);
    }

    const isSelected =
      selectedId !== undefined &&
      (selectedId === properties.id || selectedId === properties._id);

    properties.type = getStyleType(properties.type);

    let styles = [];

    if (['line', 'polygon'].indexOf(properties.type) != -1) {
      this.shapeStyles(properties, { isSelected, isPending }, styles);
      return styles;
    }

    if (
      ['lineMarker', 'polygonMarker', 'site'].indexOf(properties.type) != -1
    ) {
      this.markerStyles(properties, { isSelected, isPending }, styles);
      return styles;
    }

    return styles;
  }

  shapeStyles(properties, variants, styles) {
    const done = () => {
      this.raiseChange();
    };
    let styleProperties = this.iconStyles(
      properties.icon,
      properties.type,
      done
    );

    if (!styleProperties) {
      styles.push(this.defaultShapeStyle.toOlStyle());
      return;
    }

    styleProperties = this.applyStyleVariants(styleProperties, variants);
    styles.push(styleProperties.toOlStyle());

    this.labelStyles(properties, styleProperties?.label, styles);
  }

  markerStyles(properties, variants, styles) {
    const done = () => {
      this.raiseChange();
    };

    let styleProperties = this.iconStyles(
      properties.icon,
      properties.type,
      done
    );

    if (!styleProperties?.isVisible) {
      styles.push(this.defaultPointStyle.toOlStyle());
      return;
    }

    styleProperties = this.applyStyleVariants(styleProperties, variants);
    styles.push(styleProperties.toOlStyle());

    let state;

    if (properties['visibility'] == '0') {
      state = 'private';
    }

    if (properties['visibility'] == '-1') {
      state = 'pending';
    }

    if (state) {
      const style = this.badgeStyle(styleProperties.size, state);
      style.zIndex = styleProperties.zIndex;
      styles.push(style.toOlBadgeStyle());

      styles.push(style.toOlBadgeIcon(`/assets/img/${state}.svg`));
    }

    const image = styleProperties.toOlImageStyle();

    if (image) {
      styles.push(image);
    }

    this.labelStyles(properties, styleProperties?.label, styles);
  }

  labelStyles(properties, label, styles) {
    if (!label?.isVisible) {
      return;
    }

    const defaultStyle =
      this.styleFactory.extractElementProperties(`group-text-1`);
    const olStyle = label.toOlStyle(defaultStyle.fontFamily);
    olStyle
      .getText()
      .setText(this.stringTransform(properties.name, label.text));

    styles.push(olStyle);
  }

  applyStyleVariants(style, variants) {
    if (variants.isSelected) {
      return this.updateStyle(style, this.selectedPointStyle);
    }

    if (variants.isPending) {
      return this.updateStyle(style, this.pendingPointStyle);
    }

    return style;
  }

  updateStyle(style, sourceStyle) {
    const styleProperties = style.clone();

    if (sourceStyle.opacity) {
      styleProperties.opacity = sourceStyle.opacity;
    }

    if (sourceStyle.shape) {
      styleProperties.shape = sourceStyle.shape;
    }

    if (sourceStyle.borderWidth) {
      styleProperties.borderWidth = sourceStyle.borderWidth;
    }

    if (sourceStyle.size) {
      styleProperties.size = sourceStyle.size;
    }

    if (sourceStyle.zIndex) {
      styleProperties.zIndex = sourceStyle.zIndex;
    }

    if (sourceStyle.borderColor != 'transparent') {
      styleProperties.borderColor = sourceStyle.borderColor;
    }

    if (sourceStyle.backgroundColor != 'transparent') {
      styleProperties.backgroundColor = sourceStyle.backgroundColor;
    }

    return styleProperties;
  }

  lazyIconImage(iconId, style, done) {
    if (!iconId) {
      return;
    }

    const record = this.getIcon(iconId, done);

    if (!record) {
      return;
    }

    let scale = style.size / this.iconSize + 0.3;
    return this.image({ url: record.image?.value + '/sm', scale });
  }

  iconStyles(iconId, name, done) {
    const record = this.getIcon(iconId, done);

    if (!record) {
      return null;
    }

    const types = record.styles.types;
    const imageStyles = record.imageStyles;

    return imageStyles[name] || types[name];
  }

  getIcon(id, done) {
    if (typeof id != 'string') {
      return null;
    }

    const record = this.store.peekRecord('icon', id);

    if (!record || !record.styles || !record.styles.types) {
      this.store.findRecord('icon', id).then(done).catch(done);
      return null;
    }

    return record;
  }

  extent() {
    return this.styleFactory.shape('extent');
  }

  image(params) {
    const className = params.url;
    const key = `image.${className}.${params.isSelected}`;

    if (this.cache.styles[key]) {
      return this.cache.styles[key];
    }

    const style = new Style({
      image: new Icon({
        scale: params.scale,
        anchor: [0.5, 0.5],
        anchorXUnits: 'fraction',
        anchorYUnits: 'fraction',
        src: `${params.url}`,
      }),
    });

    if (params.isSelected) {
      style.setZIndex(999999);
    }

    this.cache.styles[key] = style;

    return this.cache.styles[key];
  }

  group(properties) {
    const level = parseInt(properties.count);
    let classLevel = 0;

    if (level > 0 && level < 10) {
      classLevel = 0;
    } else if (level >= 10 && level < 30) {
      classLevel = 1;
    } else if (level >= 30 && level < 50) {
      classLevel = 2;
    } else if (level >= 50 && level < 100) {
      classLevel = 3;
    } else if (level >= 100 && level < 200) {
      classLevel = 4;
    } else if (level >= 200 && level < 500) {
      classLevel = 5;
    } else if (level >= 500 && level < 1000) {
      classLevel = 6;
    } else if (level >= 1000) {
      classLevel = 7;
    }

    const key = `group-style-${classLevel}`;

    if (!this.cache.styles[key]) {
      this.cache.styles[key] = new Style({
        image: this.styleFactory.hexagon(`group-${classLevel}`),
      });
    }

    return this._groupStyle[classLevel].toOlStyle();
  }

  groupText(properties) {
    const level = parseInt(properties.count) || 0;
    let classLevel = 0;
    let text = level;

    if (level > 0 && level < 10) {
      classLevel = 0;
    } else if (level >= 10 && level < 30) {
      classLevel = 1;
    } else if (level >= 30 && level < 50) {
      classLevel = 2;
    } else if (level >= 50 && level < 100) {
      classLevel = 3;
    } else if (level >= 100 && level < 200) {
      classLevel = 4;
    } else if (level >= 200 && level < 500) {
      classLevel = 5;
    } else if (level >= 500 && level < 1000) {
      classLevel = 6;
    } else if (level >= 1000) {
      classLevel = 7;
      text = parseInt(level / 1000) + 'k';
    }

    const style = this.groupTextStyle[classLevel].toOlStyle();
    style.getText().setText(text + '');

    return style;
  }

  pattern(iconModel, onRefresh) {
    if (!iconModel) {
      return this.cache.emptyStyle;
    }

    let context;
    let canvas;

    const key = `pattern.${iconModel.id}`;

    if (this.cache.styles[key]) {
      return this.cache.styles[key];
    }

    this.cache.styles[key] = new Style({
      fill: new Fill({ color: 'transparent' }),
    });

    const size = this.iconSize;
    const padding = 5;
    const fullSize = size + 2 * padding;
    const shift = (fullSize / 2) * -1;

    try {
      canvas = document.createElement('canvas');
      canvas.width = size * 2 + padding * 4;
      canvas.height = size * 2 + padding * 4;

      context = canvas.getContext('2d');

      var image = new Image();

      image.onload = () => {
        context.drawImage(image, padding, padding, size, size);
        context.drawImage(image, padding * 3 + size, padding, size, size);

        context.drawImage(image, shift, padding * 3 + size, size, size);
        context.drawImage(
          image,
          shift + fullSize + padding,
          padding * 3 + size,
          size,
          size
        );
        context.drawImage(
          image,
          shift + fullSize + fullSize,
          padding * 3 + size,
          size,
          size
        );

        if (this.cache.styles[key]) {
          this.cache.styles[key]
            .getFill()
            .setColor(context.createPattern(canvas, 'repeat'));
        }

        if (onRefresh) {
          onRefresh();
        }
      };

      image.src = `${iconModel.image}/sm`;

      // eslint-disable-next-line no-empty
    } catch (err) {}

    return this.cache.styles[key];
  }

  stringTransform(str, type) {
    if (!str) {
      return '';
    }

    if (type == 'shorten' && str.length > 12) {
      return str.substr(0, 10) + '...';
    }

    if (type == 'wrap') {
      return this.stringDivider(str, 16, '\n');
    }

    return str;
  }

  stringDivider(str, width, spaceReplacer) {
    if (str.length > width) {
      var p = width;
      while (p > 0 && str[p] != ' ' && str[p] != '-') {
        p--;
      }
      if (p > 0) {
        var left;
        if (str.substring(p, p + 1) == '-') {
          left = str.substring(0, p + 1);
        } else {
          left = str.substring(0, p);
        }
        var right = str.substring(p + 1);
        return (
          left + spaceReplacer + this.stringDivider(right, width, spaceReplacer)
        );
      }
    }
    return str;
  }
}

function getStyleType(type) {
  if (type == 'polygonBorder') {
    return 'line';
  }

  if (type == 'polygonBorderMarker') {
    return 'lineMarker';
  }

  if (type == 'point') {
    return 'site';
  }

  return type;
}

function getIcon(icons) {
  if (!icons) {
    return;
  }

  const pos = icons.indexOf(',');

  if (pos == -1) {
    return icons;
  }

  return icons.substr(0, pos);
}
