import Service, { inject as service } from '@ember/service';
import config from '../config/environment';
import { tracked } from '@glimmer/tracking';

export default class UserService extends Service {
  @service session;
  @service store;
  @service fastboot;

  @tracked _profileData;
  @tracked _userData;
  @tracked teams;

  apiUrl = config.apiUrl;

  get userData() {
    if (this._userData) {
      return this._userData;
    }

    let shoebox = this.fastboot.shoebox;
    const store = shoebox?.retrieve('user-service-store') ?? {};

    return store['user'];
  }

  set userData(value) {
    if (!this.fastboot.isFastBoot) {
      this._userData = value;
      return;
    }

    let shoebox = this.fastboot.shoebox;
    const store = shoebox?.retrieve('user-service-store') ?? {};
    store['user'] = value.toPOJO();

    shoebox.put('user-service-store', store);
  }

  get profileData() {
    if (this._profileData) {
      return this._profileData;
    }

    let shoebox = this.fastboot.shoebox;
    const store = shoebox?.retrieve('user-service-store') ?? {};

    return store['profile'];
  }

  set profileData(value) {
    if (!this.fastboot.isFastBoot) {
      this._profileData = value;
      return;
    }

    let shoebox = this.fastboot.shoebox;
    const store = shoebox?.retrieve('user-service-store') ?? {};
    store['profile'] = value.serialize();
    store['profile'].fullName = value.fullName;

    shoebox.put('user-service-store', store);
  }

  async getUserData() {
    if (!this.session.isAuthenticated) {
      this.userData = null;
      return null;
    }

    if (!this.userData) {
      this.userData = await this.store.queryRecord('user', { me: true });
    }

    return this.userData;
  }

  async loadProfile() {
    if (!this.id) {
      return;
    }

    this.profileData = await this.store.findRecord('user-profile', this.id);
  }

  async loadTeams() {
    if (!this.id) {
      return;
    }

    this.teams = await this.store.query('team', { user: this.id });
    this.teams = this.teams.slice().sort?.(function (a, b) {
      var nameA = a.name.toUpperCase(); // ignore upper and lowercase
      var nameB = b.name.toUpperCase(); // ignore upper and lowercase
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }

      return 0;
    });
  }

  get name() {
    if (!this.session.isAuthenticated) {
      return 'Not Authenticated';
    }

    return this.profileData?.fullName ?? '...';
  }

  get isAdmin() {
    if (!this.userData) {
      return false;
    }

    return this.userData.isAdmin;
  }

  get id() {
    if (!this.userData) {
      return false;
    }

    return this.userData.id;
  }

  async ready() {
    this.loadTeams();

    if (!this.session.isAuthenticated || this.userData) {
      return;
    }

    try {
      await this.getUserData();
    } catch (err) {
      this.session.invalidate();
      return;
    }
    await this.loadProfile();
  }
}
