import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class FullscreenService extends Service {
  @tracked isEnabled = false;
}
