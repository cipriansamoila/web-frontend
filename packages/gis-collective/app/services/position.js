import Service, { inject as service } from '@ember/service';
import { storageFor } from 'ember-local-storage';
import { action } from '@ember/object';
import { tracked } from '@glimmer/tracking';
import * as sphere from 'ol/sphere';

export default class PositionService extends Service {
  @service fastboot;
  @service toaster;

  @tracked errorMessage;
  @tracked status;

  @tracked latitude;
  @tracked longitude;
  @tracked accuracy;
  @tracked altitude;
  @tracked altitudeAccuracy;

  geolocation = null;

  @storageFor('position') storage;
  timeout = 3000;

  constructor() {
    super(...arguments);

    if (!this.fastboot.isFastBoot && navigator) {
      this.geolocation = navigator.geolocation;
    }

    this.reset();
  }

  get isError() {
    return !!this.errorMessage;
  }

  get isAccurate() {
    return this.accuracy <= 5;
  }

  get isAlmostAccurate() {
    return this.accuracy > 5 && this.accuracy < 20;
  }

  get isNotAccurate() {
    return this.accuracy > 20;
  }

  get center() {
    if (this.longitude && this.latitude) {
      return [this.longitude, this.latitude];
    }

    return null;
  }

  get hasGeolocationSupport() {
    if (this.fastboot.isFastBoot) {
      return false;
    }

    return !!this.geolocation && !!this.geolocation.watchPosition;
  }

  get hasPosition() {
    if (!this.watchId) {
      return false;
    }

    return this.center !== null;
  }

  get isHeadlessBrowser() {
    let isHeadless = false;

    try {
      isHeadless = /\bHeadlessChrome\//.test(navigator.userAgent);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
    }

    return isHeadless;
  }

  get isWaiting() {
    return this.status == 'WAITING';
  }

  get hasGeoLocationPosition() {
    return !!this.center;
  }

  reset() {
    this.errorMessage = undefined;
    this.watchId = null;

    this.latitude = NaN;
    this.longitude = NaN;
    this.accuracy = NaN;
    this.altitude = NaN;
    this.altitudeAccuracy = NaN;
    this.status = 'PENDING';
  }

  shouldUpdate(longitude, latitude, accuracy) {
    if (accuracy <= this.accuracy) {
      return true;
    }

    const distance = parseInt(
      sphere.getDistance(
        [longitude, latitude],
        [parseFloat(this.longitude), parseFloat(this.latitude)]
      )
    );

    return isNaN(distance) || distance > 5;
  }

  @action
  watchPosition() {
    if (this.center) {
      return Promise.resolve(this.center);
    }

    if (this.watchId) {
      return this._promise;
    }

    this._promise = new Promise((resolve, reject) => {
      if (!this.hasGeolocationSupport) {
        this.status = 'NOT_AVAILABLE';
        return reject(new Error('Your device does not support geo location.'));
      }

      this.status = 'WAITING';

      this.watchId = this.geolocation.watchPosition(
        (position) => {
          const accuracy = Math.round(position.coords.accuracy * 100) / 100;

          if (
            this.shouldUpdate(
              position.coords.longitude,
              position.coords.latitude,
              accuracy
            )
          ) {
            this.longitude = position.coords.longitude;
            this.latitude = position.coords.latitude;
            this.accuracy = accuracy;
            this.altitude = position.coords.altitude;
            this.altitudeAccuracy = position.coords.altitudeAccuracy;
          }

          this.errorMessage = '';
          this.status = 'WATCHING';

          resolve?.(this.center);
          resolve = null;
        },
        (error) => {
          this.status = 'NOT_AVAILABLE';

          if (!this.isError && this.lastCode != error?.code) {
            this.errorMessage = this.toaster.handleLocationError(error);
          }

          this.lastCode = error?.code;
          return reject(this.errorMessage);
        },
        {
          enableHighAccuracy: true,
          timeout: this.timeout,
          maximumAge: Infinity,
        }
      );
    });

    return this._promise;
  }

  @action
  cancelWatch() {
    this.status = 'PENDING';

    if (!this.hasGeolocationSupport) {
      return;
    }

    this.geolocation.clearWatch?.(this.watchId);
    this.watchId = null;
    this._promise = null;
  }

  willDestroy() {
    this.cancelWatch();
  }
}
