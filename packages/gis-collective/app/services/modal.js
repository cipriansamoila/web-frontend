import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';
import { htmlSafe } from '@ember/template';

export default class ModalService extends Service {
  @tracked visible = false;
  @tracked title = '';
  @tracked message = '';
  @tracked password = '';
  @tracked isConfirmation = false;
  @tracked answers = [];
  @tracked list = null;
  @tracked resolve = null;
  @tracked reject = null;
  @tracked disableCancel = false;
  @tracked askPassword = false;

  openModal = null;

  ask(title, message, answers, disableCancel = false) {
    this.title = title;
    this.message = message;

    this.isConfirmation = true;
    this.disableCancel = disableCancel;

    this.answers.clear();

    if (answers) {
      this.answers.addObjects(answers);
    }

    this.openModal?.();

    return new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
  }

  confirm(title, message) {
    this.title = title;
    this.message = message;
    this.isConfirmation = true;

    this.openModal?.();

    return new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
  }

  confirmWithPassword() {
    this.askPassword = true;

    return this.confirm(...arguments);
  }

  alert(title, message) {
    this.title = title;
    this.message = message;
    this.isConfirmation = false;

    this.resolve = null;
    this.reject = null;

    this.openModal?.();

    return new Promise((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
  }

  inform(title, message) {
    return this.alert(title, message);
  }

  log(title, message) {
    this.isLog = true;

    return this.inform(title, htmlSafe(`<pre class="log">${message}</pre>`));
  }

  clear() {
    this.answers.clear();

    this.isLog = false;
    this.askPassword = false;
    this.password = '';
    this.title = '';
    this.message = '';
    this.isConfirmation = false;
    this.list = [];
    this.disableCancel = false;
    this.resolve = null;
    this.reject = null;
  }
}
