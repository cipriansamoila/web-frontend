import Service, { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import md5 from 'blueimp-md5';
import { MatomoTracker } from '../lib/trackers/matomo';

export default class PreferencesService extends Service {
  @service store;
  @service fastboot;
  @service tracking;
  @service user;

  @tracked isMultiProjectMode = true;
  @tracked isPrivate = true;
  @tracked allowManageWithoutTeams = true;
  @tracked allowProposingSites = true;
  @tracked serviceName = 'GISCollective';
  @tracked appearanceMapExtent = [-118, -28, 37, 65];
  @tracked showWelcomePresentation = false;
  @tracked matomoUrl = null;
  @tracked matomoSiteId = null;

  get uid() {
    if (!this._uid) {
      this._uid = this.fastboot.shoebox?.retrieve('uid');
    }

    if (!this._uid) {
      const randomNr = Math.floor(Math.random() * 100000);
      this._uid = md5(`${Date.now()}-${randomNr}`).substring(0, 16);
    }

    if (this.fastboot.isFastBoot) {
      this.fastboot.shoebox?.put('uid', this._uid);
    }

    return this._uid;
  }

  get domain() {
    if (this.fastboot.isFastBoot) {
      return this.fastboot.request.host;
    }

    if (window && window.location && window.location.host) {
      return window.location.host;
    }

    return '';
  }

  get protocol() {
    if (!window?.location?.protocol) {
      return 'https://';
    }

    return window.location.protocol + '//';
  }

  get referer() {
    const cache = this.cache;

    if (cache['referer']) {
      return cache['referer'].value;
    }

    return null;
  }

  setupReferer() {
    const cache = this.cache;

    if (!this.fastboot.isFastBoot || cache['referer']) {
      return;
    }

    let headers = this.fastboot?.request?.headers ?? {};
    let value = null;

    if (headers.get?.('Referer')) {
      value = headers.get('Referer');
    }

    cache['referer'] = { name: 'referer', value };

    this.cache = cache;
  }

  checkTrackers() {
    if (this.matomoSiteId && this.matomoSiteId) {
      this.tracking.registerTracker(
        'matomo',
        new MatomoTracker(this.user, this, this.fastboot)
      );
    }
  }

  updateProperty(name, value) {
    const strValue = `${value}`;

    if (name == 'access.isMultiProjectMode') {
      this.isMultiProjectMode = strValue !== 'false';
    }

    if (name == 'integrations.matomo.url') {
      this.matomoUrl = strValue;
      this.checkTrackers();
    }

    if (name == 'integrations.matomo.siteId') {
      this.matomoSiteId = strValue;
      this.checkTrackers();
    }

    if (name == 'register.mandatory') {
      this.isPrivate = strValue !== 'false';
    }

    if (name == 'access.allowManageWithoutTeams') {
      this.allowManageWithoutTeams = strValue !== 'false';
    }

    if (name == 'access.allowProposingSites') {
      this.allowProposingSites = strValue !== 'false';
    }

    if (name == 'appearance.name') {
      this.serviceName = strValue;
    }

    if (name == 'appearance.showWelcomePresentation') {
      this.showWelcomePresentation = strValue !== 'false';
    }

    if (name == 'appearance.mapExtent') {
      try {
        this.appearanceMapExtent = JSON.parse(strValue);
      } catch (err) {
        this.appearanceMapExtent = [-118, -28, 37, 65];
      }
    }
  }

  get cache() {
    return this._cache || this.fastboot.shoebox?.retrieve('cache') || {};
  }

  set cache(value) {
    if (!this.fastboot.isFastBoot) {
      this._cache = value;
      return;
    }

    this.fastboot.shoebox?.put('cache', value);
  }

  async requestPreference(name) {
    const result = await this.store.query('preference', { name });

    return result.firstObject;
  }

  async getAllPreferences() {
    if (Object.keys(this.cache).length > 0) {
      Object.keys(this.cache).forEach((key) => {
        const preference = this.cache[key];
        this.updateProperty(preference?.name, preference?.value);
      });

      return;
    }

    const result = await this.store.findAll('preference');

    this.setupReferer();
    const cache = this.cache;

    result.forEach((preference) => {
      const name = preference.name;
      const key = name.replace('.', '_');

      cache[key] = preference.serialize();
      cache[key].id = preference.id;
      this.updateProperty(name, preference.value);
    });

    this.cache = cache;
  }

  async getPreference(name) {
    const key = name.replace('.', '_');
    const cache = this.cache;

    if (cache[key]) {
      this.updateProperty(name, cache[key].value);
      return cache[key];
    }

    const result = await this.requestPreference(name);

    cache[key] = result.serialize();
    cache[key].value =
      typeof cache[key]?.value == 'boolean'
        ? `${cache[key].value}`
        : cache[key].value;
    cache[key].id = result.id;

    this.updateProperty(name, cache[key].value);
    this.cache = cache;

    return cache[key];
  }
}
