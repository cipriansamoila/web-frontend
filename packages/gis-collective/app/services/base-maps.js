import { inject as service } from '@ember/service';

import BaseMaps from 'ember-ol/services/base-maps';

export default BaseMaps.extend({
  store: service(),

  init() {
    this._super(...arguments);
    this.all();
  },

  all() {
    if (this.loadPromise) {
      return this.loadPromise;
    }

    this.list.clear();
    this.layers.clear();

    const promise = new Promise((resolve, reject) => {
      this.store
        .findAll('base-map')
        .then((list) => {
          list.forEach((baseMap) => {
            this.load(baseMap);
          });
        })
        .then(() => {
          this.set('visibleBaseMap', this.defaultBaseMap);
          this.set('loadPromise', null);
          resolve(this.layers);
        })
        .catch(() => {
          this.set('loadPromise', null);
          reject(...arguments);
        });
    });

    this.set('loadPromise', promise);

    return promise;
  },
});
