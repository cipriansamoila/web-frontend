import { tracked } from '@glimmer/tracking';
import Service, { inject as service } from '@ember/service';
import { getOwner } from '@ember/application';

export default class HeadDataService extends Service {
  @service router;

  get config() {
    const config = getOwner(this).resolveRegistration('config:environment');
    return config['ember-meta'];
  }

  get currentRouteMeta() {
    return this;
  }

  get routeName() {
    return this.router.currentRouteName;
  }

  @tracked articleTitle;
  @tracked author;
  @tracked canonical;
  @tracked categories;
  @tracked content;
  @tracked date;
  @tracked description;
  @tracked imgSrc;
  @tracked jsonld;
  @tracked keywords;
  @tracked siteName;
  @tracked slug;
  @tracked tags;
  @tracked title;
  @tracked twitterUsername;
  @tracked type;
  @tracked url;
}
