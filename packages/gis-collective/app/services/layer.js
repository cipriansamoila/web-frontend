import Service from '@ember/service';

import Tile from 'ol/layer/Tile';
import VectorTile from 'ol/layer/VectorTile';
import MVT from 'ol/format/MVT';

import OSM from 'ol/source/OSM';
import Stamen from 'ol/source/Stamen';
import TileArcGISRest from 'ol/source/TileArcGISRest';
import TileWMS from 'ol/source/TileWMS';
import XYZ from 'ol/source/XYZ';
import BingMaps from 'ol/source/BingMaps';
import VectorTileSource from 'ol/source/VectorTile';
import config from '../config/environment';

import { createXYZ } from 'ol/tilegrid';
import stylefunction from 'ol-mapbox-style/dist/stylefunction';
import { inject as service } from '@ember/service';

export default class LayerService extends Service {
  @service session;
  @service mapStyles;
  cache = [];

  featureTilesUrl = config.siteTilesUrl;

  toOl(layer, cacheId, map) {
    if (layer.type == 'Open Street Maps' || layer.type == 'Open Street Map') {
      return this.toOSMLayer(layer.options, cacheId, map);
    }

    if (layer.type == 'WMS') {
      return this.toWMSLayer(layer.options, cacheId, map);
    }

    if (layer.type == 'XYZ') {
      return this.toXYZLayer(layer.options, cacheId, map);
    }

    if (layer.type == 'Stamen') {
      return this.toStamenLayer(layer.options, cacheId, map);
    }

    if (layer.type == 'VectorTile') {
      return this.toVectorTileLayer(layer.options, cacheId, map);
    }

    if (layer.type == 'ArcGIS MapServer') {
      return this.toArcGISMapServer(layer.options, cacheId, map);
    }

    if (layer.type == 'Bing Maps') {
      return this.toBingLayer(layer.options, cacheId, map);
    }

    if (layer.type == 'GISCollective Map') {
      return this.toGisCollectiveMapLayer(layer.options, cacheId, map);
    }

    // eslint-disable-next-line no-console
    console.error('the layer type:', layer.type, 'is not supported');
  }

  updateVisibility(layer, options) {
    if (!options['visibility']) {
      return;
    }

    layer.setMinZoom(options['visibility'][0]);
    layer.setMaxZoom(options['visibility'][1]);
  }

  getCached(id, type, map, factory) {
    if (!id) {
      return factory();
    }

    const key = `${id}_${type}`;

    if (!this.cache[key]) {
      this.cache[key] = factory();
    }

    let layer = this.cache[key];

    if (layer.__map?.getLayers?.().getLength() > 0) {
      layer = factory();
    }

    layer.__map?.removeLayer?.(layer);
    layer.__map = map;
    layer.__id = key;

    return layer;
  }

  toOSMLayer(options, cacheId, map) {
    let layer = this.getCached(
      cacheId,
      `osm_${JSON.stringify(options)}`,
      map,
      () => new Tile({ source: new OSM() })
    );

    this.updateVisibility(layer, options);
    return layer;
  }

  toStamenLayer(options, cacheId, map) {
    let layer = this.getCached(
      cacheId,
      `stamen_${JSON.stringify(options)}`,
      map,
      () =>
        new Tile({
          source: new Stamen({
            layer: options.layer,
          }),
        })
    );

    this.updateVisibility(layer, options);

    return layer;
  }

  toArcGISMapServer(options, cacheId, map) {
    let layer = this.getCached(
      cacheId,
      `arcgis_${JSON.stringify(options)}`,
      map,
      () =>
        new Tile({
          source: new TileArcGISRest({
            url: options.url,
          }),
        })
    );

    this.updateVisibility(layer, options);
    return layer;
  }

  toWMSLayer(options, cacheId, map) {
    let layer = this.getCached(
      cacheId,
      `wms_${JSON.stringify(options)}`,
      map,
      () =>
        new Tile({
          source: new TileWMS({
            url: options.url,
            serverType: options['server type'],
            params: {
              LAYERS: options['layers'],
              STYLES: options['styles'],
            },
            gutter: options['gutter'],
          }),
        })
    );

    this.updateVisibility(layer, options);

    return layer;
  }

  toXYZLayer(options, cacheId, map) {
    let layer = this.getCached(
      cacheId,
      `xyz_${JSON.stringify(options)}`,
      map,
      () =>
        new Tile({
          source: new XYZ({
            url: options.url,
            tilePixelRatio: options['tilePixelRatio'],
          }),
        })
    );

    this.updateVisibility(layer, options);

    return layer;
  }

  toBingLayer(options, cacheId, map) {
    let layer = this.getCached(
      cacheId,
      `bing_${JSON.stringify(options)}`,
      map,
      () =>
        new Tile({
          source: new BingMaps({
            key: options.key,
            imagerySet: options['imagerySet'],
          }),
        })
    );

    this.updateVisibility(layer, options);

    return layer;
  }

  toVectorTileLayer(options, cacheId, map) {
    const tilegrid = createXYZ({ tileSize: 512 });

    let layer = this.getCached(
      cacheId,
      `vector_tile_${JSON.stringify(options)}`,
      map,
      () =>
        new VectorTile({
          declutter: true,
          source: new VectorTileSource({
            format: new MVT(),
            url: options.url,
            tileGrid: tilegrid,
          }),
        })
    );

    fetch(options.style).then(function (response) {
      response.json().then(function (glStyle) {
        const keys = Object.keys(glStyle.sources);
        stylefunction(layer, glStyle, keys[0]);
      });
    });

    this.updateVisibility(layer, options);
    return layer;
  }

  toGisCollectiveMapLayer(options) {
    let bearer;
    const mapId = options['GISCollectiveMap'];
    let { access_token } = this.session.get('data.authenticated');

    if (access_token) {
      bearer = `Bearer ${access_token}`;
    }

    const source = {
      format: new MVT(),
      url: `${this.featureTilesUrl}/{z}/{x}/{y}?format=vt&map=${mapId}`,
    };

    source.tileLoadFunction = (tile, url) => {
      tile.setLoader((extent, resolution, projection) => {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', url);
        xhr.responseType = 'arraybuffer';

        if (bearer) {
          xhr.setRequestHeader('Authorization', bearer);
        }

        xhr.onload = function () {
          const format = tile.getFormat();

          tile.setFeatures(
            format.readFeatures(xhr.response, {
              extent: extent,
              featureProjection: projection,
            })
          );
        };

        xhr.send();
      });
    };

    const layer = new VectorTile({
      source: new VectorTileSource(source),
      declutter: false,
      renderMode: 'image',
      zIndex: 9999,
      style: (feature) => {
        return this.mapStyles.styleFunction(feature);
      },
    });

    this.updateVisibility(layer, options);

    return layer;
  }
}
