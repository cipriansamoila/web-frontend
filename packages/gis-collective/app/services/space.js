import Service, { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class SpaceService extends Service {
  @service store;
  @service fastboot;

  @tracked currentSpace;

  async setup() {
    const spaces = await this.store.query('space', { default: true });

    this.currentSpace = spaces.firstObject;
  }
}
