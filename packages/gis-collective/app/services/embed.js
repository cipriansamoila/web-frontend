import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class EmbedService extends Service {
  @tracked isEnabled = false;
}
