import Service from '@ember/service';

export default class PageColsService extends Service {
  otherComponents = [
    'blocks-preview',
    'feature-visibility',
    'geometry',
    'icon-attribute',
    'map-name-list',
    'optional-boolean',
    'options-with-other',
    'options',
    'user',
  ];

  defaultComponents = [
    'banner',
    'blocks',
    'picture',
    'pictures',
    'sounds',
    'menu',
  ];
  articleComponents = ['article'];
  campaignComponents = ['campaign-card-list'];
  iconComponents = ['icons'];
  mapComponents = ['map-card-list', 'map/map-view'];

  get availableTypes() {
    const result = [];

    result.addObjects(this.defaultComponents);
    result.addObjects(this.articleComponents);
    result.addObjects(this.mapComponents);
    result.addObjects(this.campaignComponents);
    result.addObjects(this.iconComponents);
    result.addObjects(this.otherComponents);

    return result;
  }
}
