import Service, { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class ArticlesService extends Service {
  @service store;
  @service fastboot;
  @service user;

  @tracked _aboutVisible;
  @tracked resolved;

  async checkAbout() {
    if (this.resolved) return this.hasAbout;
    this.resolved = true;

    const shoebox = this.fastboot.shoebox;
    const store = shoebox?.retrieve?.('articles-store') ?? {};

    if (typeof store.aboutVisible == 'boolean') {
      this._aboutVisible = store.aboutVisible;
      return this.hasAbout;
    }

    try {
      const article = await this.store.findRecord('article', 'about');

      this._aboutVisible = article.visibility?.isPublic ?? false;
    } catch (err) {
      this._aboutVisible = false;
    }

    if (this.fastboot.isFastBoot && shoebox) {
      shoebox.put?.('articles-store', { aboutVisible: this._aboutVisible });
    }

    return this.hasAbout;
  }

  get hasAbout() {
    return this.user.isAdmin || this._aboutVisible;
  }
}
