import Service from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class LastErrorService extends Service {
  @tracked message = 'Error contacting the server.';
  @tracked offerLogin = false;
}
