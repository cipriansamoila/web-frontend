import Service from '@ember/service';
import { inject as service } from '@ember/service';

export default class AccessConfigService extends Service {
  @service user;
  @service preferences;

  get isMultiProjectMode() {
    if (this.user.isAdmin) {
      return true;
    }

    if (!this.preferences.isMultiProjectMode) {
      return false;
    }

    return true;
  }
}
