import Service, { inject as service } from '@ember/service';
import { later } from '@ember/runloop';

export default class NotificationsService extends Service {
  @service modal;
  @service intl;

  ask(options) {
    return this.modal.ask(
      options.title,
      options.question,
      options.answers,
      options.disableCancel
    );
  }

  handleError(err) {
    return this.showError(err);
  }

  showError(error) {
    // eslint-disable-next-line no-console
    console.error(error);

    let message = this.intl.t('There was an unexpected error.');
    let title = this.intl.t('unknown error');

    if (error && error['errors']) {
      error = error['errors'][0];
    }

    if (error.message) {
      message = error.message;
    }

    if (error.description) {
      message = error.description;
    }

    if (error.title) {
      title = error.title;
    }

    if (error.status && error.status >= 500) {
      message = this.intl.t('Server error') + ': ' + message;
    }

    if (!this.modal.visible) {
      this.modal.alert(title, message);
    } else {
      later(() => {
        this.modal.alert(title, message);
      }, 2000);
    }
  }

  showMessage(title, message) {
    return this.modal.inform(title, message);
  }
}
