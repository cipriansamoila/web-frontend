import Service, { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import * as Sentry from '@sentry/browser';

class ToastDetails {
  @tracked code;
  @tracked message;
  @tracked serialized;
  @tracked localMessage;
}

export class Toast {
  @tracked title;
  @tracked level;
  @tracked icon;
  @tracked canClose;
  @tracked message;
  @tracked details;
  @tracked index;
}

export default class ToasterService extends Service {
  @service intl;
  @tracked toasts = [];
  @tracked index = 0;

  handleLocationError(error) {
    let details = new ToastDetails();

    details.code = error.code;
    details.message = error.message;
    details.serialized = JSON.stringify(error);

    switch (error.code) {
      case error.PERMISSION_DENIED:
        details.localMessage = this.intl.t('geo-location-permission-denied');
        break;

      case error.POSITION_UNAVAILABLE:
        details.localMessage = this.intl.t(
          'geo-location-permission-unavailable'
        );
        break;

      case error.TIMEOUT:
        details.localMessage = this.intl.t('geo-location-permission-timeout');
        break;

      default:
        details.localMessage = this.intl.t('geo-location-unknown-error');
        break;
    }

    const toast = new Toast();
    (toast.title = this.intl.t('Location error')),
      (toast.level = 'danger'),
      (toast.icon = 'location-arrow'),
      (toast.canClose = true),
      (toast.message = details.localMessage),
      (toast.details = details);

    this.addToast(toast);

    return details.localMessage;
  }

  handleError(err) {
    let details = new ToastDetails();
    let message = this.intl.t('An unknown error occurred.');

    Sentry?.collect?.(err);

    if (err && err['errors']) {
      return err.errors.forEach((error) => {
        this.handleError(error);
      });
    }

    if (err.message) {
      message = err.message;
    }

    details.code = err.code;
    details.message = message;
    details.serialized = JSON.stringify(err);

    const toast = new Toast();
    toast.title = this.intl.t('error');
    toast.level = 'danger';
    toast.message = message;
    toast.details = details;

    this.addToast(toast);
  }

  addToast(toast) {
    toast.index = this.index;
    this.toasts.pushObject(toast);
    this.index++;
  }

  remove(index) {
    let matchedToasts = this.toasts.filter((a) => a.index == index);
    this.toasts.removeObjects(matchedToasts);
  }
}
