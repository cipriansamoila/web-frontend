import { containsCoordinate } from 'ol/extent';
import LineString from 'ol/geom/LineString';

function calculatePointsDistance(coord1, coord2) {
  var dx = coord1[0] - coord2[0];
  var dy = coord1[1] - coord2[1];
  return Math.sqrt(dx * dx + dy * dy);
}

function calculateSplitPointCoords(startNode, nextNode, distance) {
  const length = calculatePointsDistance(startNode, nextNode);
  const partition = distance / length;

  const x = startNode[0] * (1 - partition) + nextNode[0] * partition;
  const y = startNode[1] * (1 - partition) + nextNode[1] * partition;

  return [x, y];
}

export function multiLineSplit(geometry, segmentLength, options) {
  const list = [];

  geometry.getLineStrings().forEach((line) => {
    list.addObjects(lineSplit(line, segmentLength, options));
  });

  return list;
}

export function lineSplit(geometry, segmentLength, options) {
  var splitPoints = [];
  var coords = geometry.getCoordinates();

  let leftDistance = 0;
  let lines = [];

  segmentLength = Math.max(segmentLength, geometry.getLength() / 1000);

  const lineObj = new LineString([]);
  for (let i = 1; i < coords.length; i++) {
    const segment = [coords[i - 1], coords[i]];

    lineObj.setCoordinates(segment);

    if (lineObj.intersectsExtent(options.extent)) {
      lines.push(segment);
    }
  }

  lines.forEach((line) => {
    let point = line[0];
    let nextPoint = line[1];

    let remainingDistance = calculatePointsDistance(point, nextPoint);

    while (remainingDistance + leftDistance > segmentLength) {
      const splitPoint = calculateSplitPointCoords(
        point,
        nextPoint,
        segmentLength - leftDistance
      );
      leftDistance = 0;
      remainingDistance -= calculatePointsDistance(point, splitPoint);

      if (containsCoordinate(options.extent, splitPoint)) {
        splitPoints.push(splitPoint);
      }

      point = splitPoint;
    }

    leftDistance += remainingDistance;
  });

  return splitPoints;
}
