const missing = {};

export default function missingMessage(key, locales) {
  locales.forEach((locale) => {
    if (!missing[locale]) {
      missing[locale] = {};
    }

    missing[locale][key] = true;
  });

  let result = '';

  Object.keys(missing).forEach((locale) => {
    result += '---------- ' + locale + '.yaml\n';

    Object.keys(missing[locale]).forEach((key) => {
      result += key + ': ' + key + '\n';
    });
  });

  // eslint-disable-next-line no-console
  console.log('There are some missing translations:\n', result);

  return key;
}
