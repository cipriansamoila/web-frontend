import ApplicationAdapter from './application';

export default class IssueAdapter extends ApplicationAdapter {
  resolve(model) {
    const url = this.buildURL('issue', model.get('id')) + '/resolve';
    return this.ajax(url, 'GET');
  }

  reject(model) {
    const url = this.buildURL('issue', model.get('id')) + '/reject';
    return this.ajax(url, 'GET');
  }
}
