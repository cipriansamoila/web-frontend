import ApplicationAdapter from './application';

export default class FileAdapter extends ApplicationAdapter {
  import(model) {
    const url = this.buildURL('file', model.get('id')) + '/importAllItems';
    return this.ajax(url, 'GET');
  }
}
