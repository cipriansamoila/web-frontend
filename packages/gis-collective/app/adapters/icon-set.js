import ApplicationAdapter from './application';

export default class IssueAdapter extends ApplicationAdapter {
  categories(model) {
    const url = this.buildURL('icon-set', model.get('id')) + '/categories';

    return this.ajax(url, 'GET');
  }

  subcategories(model, category) {
    const url =
      this.buildURL('icon-set', model.get('id')) +
      '/subcategories?category=' +
      category;

    return this.ajax(url, 'GET');
  }
}
