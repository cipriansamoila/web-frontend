import ApplicationAdapter from './application';
import fetch from 'fetch';
import { inject as service } from '@ember/service';

export default class PictureAdapter extends ApplicationAdapter {
  @service fastboot;

  shouldBackgroundReloadAll() {
    return true;
  }

  shouldReloadAll() {
    return true;
  }

  rotate(model) {
    const url = this.buildURL('picture', model.get('id')) + '/rotate';
    return this.ajax(url, 'GET');
  }

  createRecord(store, type, snapshot) {
    let data = {};
    let serializer = store.serializerFor(type.modelName);
    let url = this.buildURL(type.modelName, null, snapshot, 'createRecord');

    serializer.serializeIntoHash(data, type, snapshot, { includeId: true });

    return this.ajax(url, 'POST', {
      data: data,
      adapterOptions: snapshot.adapterOptions,
    });
  }

  _xhrRequest(url, options) {
    return new Promise((success, reject) => {
      var xhr = new window.XMLHttpRequest();

      xhr.upload.addEventListener('load', function () {
        options.adapterOptions?.progress?.(100);
      });

      xhr.upload.addEventListener(
        'progress',
        function (evt) {
          if (evt.lengthComputable) {
            const percentComplete = evt.loaded / evt.total;
            options.adapterOptions?.progress?.(percentComplete * 100);
          }
        },
        false
      );

      xhr.addEventListener('load', () => {
        success({
          url: options.url,
          status: xhr.status,
          statusText: xhr.statusText,
          ok: true,
          text() {
            return xhr.responseText;
          },
          json() {
            return JSON.parse(xhr.responseText);
          },
        });
      });

      xhr.addEventListener('error', reject);
      xhr.addEventListener('abort', reject);

      xhr.open(options.method, url, true);
      Object.keys(options.headers).forEach((key) => {
        xhr.setRequestHeader(key, options.headers[key]);
      });

      xhr.send(options.body);
    });
  }

  _fetchRequest(options) {
    if (this.fastboot.isFastBoot && super._fetchRequest) {
      return super._fetchRequest(options);
    }

    if (options.method != 'POST') {
      return fetch?.(options.url, options);
    }

    return this._xhrRequest(options.url, options);
  }
}
