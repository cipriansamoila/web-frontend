import ApplicationAdapter from './application';

export default class FeatureAdapter extends ApplicationAdapter {
  getParams(query) {
    return Object.keys(query)
      .map((a) => `${a}=${encodeURIComponent(query[a])}`)
      .join('&');
  }

  bulkPublish(query) {
    const url =
      this.urlForQuery(query, 'feature') + '/publish?' + this.getParams(query);
    return this.ajax(url, 'POST');
  }

  bulkUnpublish(query) {
    const url =
      this.urlForQuery(query, 'feature') +
      '/unpublish?' +
      this.getParams(query);
    return this.ajax(url, 'POST');
  }

  bulkSetPending(query) {
    const url =
      this.urlForQuery(query, 'feature') + '/pending?' + this.getParams(query);
    return this.ajax(url, 'POST');
  }

  bulkDelete(query) {
    const url =
      this.urlForQuery(query, 'feature') + '?' + this.getParams(query);
    return this.ajax(url, 'DELETE');
  }
}
