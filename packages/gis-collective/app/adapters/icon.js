import ApplicationAdapter from './application';

export default class IconAdapter extends ApplicationAdapter {
  group(query = {}) {
    query['group'] = true;
    const url = this.buildURL('icon');

    return this.ajax(url, 'GET', { data: query });
  }
}
