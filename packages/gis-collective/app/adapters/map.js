import Adapter from './application';

export default Adapter.extend({
  getMeta(mapId) {
    const url = this.buildURL('map', mapId) + '/meta';
    return this.ajax(url, 'GET');
  },
});
