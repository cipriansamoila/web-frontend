import RESTAdapter from '@ember-data/adapter/rest';
import config from '../config/environment';
import { inject as service } from '@ember/service';

export default class ApplicationAdapter extends RESTAdapter {
  useFetch = true;
  @service session;
  @service fastboot;
  @service intl;

  get headers() {
    let headers = {};

    if (this.session.isAuthenticated) {
      headers[
        'Authorization'
      ] = `Bearer ${this.session.data.authenticated.access_token}`;
    }

    headers['Accept-Language'] = this.intl.primaryLocale;

    return headers;
  }

  get host() {
    try {
      // eslint-disable-next-line no-undef
      if (this.fastboot.isFastBoot && apiHost) {
        // eslint-disable-next-line no-undef
        return apiHost;
      }
    } catch (err) {
      if (err.message != 'apiHost is not defined') {
        // eslint-disable-next-line no-console
        console.error(err.message);
      }
    }

    if (this.fastboot.isFastBoot && config.fastbootApiUrl) {
      return config.fastbootApiUrl;
    }

    return config.apiUrl;
  }

  buildURL(modelName, id, snapshot, requestType, query) {
    let url = super
      .buildURL(modelName.replace('-', ''), id, snapshot, requestType, query)
      .toLowerCase();

    if (snapshot?.adapterOptions?.locale) {
      url += `?locale=${snapshot.adapterOptions.locale}`;
    }

    if (typeof snapshot?.adapterOptions?.validation == 'boolean') {
      url += `?validation=${snapshot.adapterOptions.validation}`;
    }

    return url;
  }
}
