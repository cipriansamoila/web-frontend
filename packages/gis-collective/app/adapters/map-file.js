import ApplicationAdapter from './application';

export default class MApFileAdapter extends ApplicationAdapter {
  import(id) {
    const url = this.buildURL('map-file', id) + '/import';
    return this.ajax(url, 'POST');
  }

  cancelImport(id) {
    const url = this.buildURL('map-file', id) + '/cancelImport';
    return this.ajax(url, 'POST');
  }

  getMeta(id) {
    const url = this.buildURL('map-file', id) + '/meta';
    return this.ajax(url, 'GET');
  }

  getLog(id) {
    const url = this.buildURL('map-file', id) + '/log';
    return this.ajax(url, 'GET');
  }

  metaPreview(id) {
    const url = this.buildURL('map-file', id) + '/preview';
    return this.ajax(url, 'GET');
  }

  analyze(id) {
    const url = this.buildURL('map-file', id) + '/analyze';
    return this.ajax(url, 'GET');
  }
}
