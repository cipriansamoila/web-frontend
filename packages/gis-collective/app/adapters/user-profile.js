import ApplicationAdapter from './application';

export default class UserProfileAdapter extends ApplicationAdapter {
  contributions(id) {
    const url = this.buildURL('user-profile', id) + `/contributions`;

    return this.ajax(url, 'GET');
  }
}
