import Adapter from './application';
import fetch from 'fetch';
import {
  isServerErrorResponse,
  isUnauthorizedResponse,
} from 'ember-fetch/errors';

export default class UserAdapter extends Adapter {
  changePassword(model, currentPassword, newPassword) {
    const url = this.buildURL('user', model.get('id')) + '/changePassword';

    return this.ajax(url, 'POST', {
      data: {
        currentPassword,
        newPassword,
      },
    });
  }

  revokeApiTokenByName(model, name) {
    const url = this.buildURL('user', model.get('id')) + '/revoke';

    return this.ajax(url, 'POST', { data: { name } });
  }

  removeUser(model, password) {
    const url = this.buildURL('user', model.get('id')) + '/remove';

    return this.ajax(url, 'POST', { data: { password } });
  }

  promote(model, password) {
    const url = this.buildURL('user', model.get('id')) + '/promote';

    return this.ajax(url, 'POST', { data: { password } });
  }

  downgrade(model, password) {
    const url = this.buildURL('user', model.get('id')) + '/downgrade';

    return this.ajax(url, 'POST', { data: { password } });
  }

  createToken(model, name, expire) {
    const url = this.buildURL('user', model.get('id')) + '/token';

    return this.ajax(url, 'POST', { data: { name, expire } });
  }

  getApiTokens(model) {
    const url = this.buildURL('user', model.get('id')) + '/listTokens';

    return this.ajax(url, 'GET');
  }

  forgotPassword(email, token, password) {
    const url = `${this.host}/users/forgotpassword`;

    return this.ajax(url, 'POST', { data: { email, token, password } });
  }

  registerChallenge() {
    const url = `${this.host}/users/registerchallenge`;
    return this.ajax(url, 'POST');
  }

  register(name, username, email, password, challenge) {
    const url = `${this.host}/users/register`;
    const data = { ...name, username, email, password };

    if (challenge) {
      data['challenge'] = challenge;
    }

    return this.ajax(url, 'POST', { data });
  }

  activate(email, token) {
    const url = `${this.host}/users/activate`;
    return this.ajax(url, 'POST', { data: { email, token } });
  }

  testNotification(model) {
    const url = this.buildURL('user', model.get('id')) + '/testNotification';
    return this.ajax(url, 'POST', {});
  }

  queryRecord(modelName, query, options) {
    if (options.me) {
      let { access_token } = this.session.data.authenticated;

      return fetch(this.host + '/users/me', {
        method: 'GET',
        headers: {
          'Content-type': 'application/json; charset=UTF-8',
          Authorization: `Bearer ${access_token}`,
        },
      })
        .then(function (response) {
          if (response.ok) {
            return response.json();
          } else if (
            isUnauthorizedResponse(response) ||
            isServerErrorResponse(response)
          ) {
            return null;
          }
        })
        .catch(function (error) {
          // eslint-disable-next-line no-console
          console.error(error);
          return null;
        });
    }

    return super.queryRecord(modelName, query, options);
  }
}
