import { tracked } from '@glimmer/tracking';

export default class OptionalMap {
  @tracked isEnabled;
  @tracked mapId;

  constructor(value, store) {
    this.store = store;

    if (!value) {
      return;
    }

    this.isEnabled = value.isEnabled;

    if (value.map?.id) {
      this.mapId = value.map.id;
    } else if (value.map?._id) {
      this.mapId = value.map._id;
    } else if (value.map) {
      this.mapId = value.map;
    }
  }

  get map() {
    if (!this.store || !this.mapId) {
      return null;
    }

    return this.store.peekRecord('map', this.mapId);
  }

  set map(value) {
    if (!value || !value.id) {
      this.mapId = '';
    }

    this.mapId = value.id;
  }

  toJSON() {
    return {
      isEnabled: this.isEnabled ?? false,
      map: this.mapId ?? '',
    };
  }
}
