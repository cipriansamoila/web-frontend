import { tracked } from '@glimmer/tracking';
import { Fill, Stroke, Style } from 'ol/style';
import TextStyle from 'ol/style/Text';

export default class LabelStyleProperties {
  @tracked isVisible = false;

  @tracked text = 'wrap';
  @tracked align = 'center';
  @tracked baseline = 'bottom';
  @tracked weight = 'bold';

  @tracked color = '#000';
  @tracked borderColor = '#fff';
  @tracked borderWidth = 2;

  @tracked size = 10;
  @tracked lineHeight = 1;
  @tracked offsetX = 0;
  @tracked offsetY = 0;

  _style = null;

  constructor(value) {
    if (!value || typeof value != 'object') {
      return;
    }

    Object.getOwnPropertyNames(LabelStyleProperties.prototype).forEach(
      (key) => {
        if (typeof value[key] == 'function' || value[key] === undefined) return;

        this[key] = value[key];
      }
    );
  }

  clone() {
    return new LabelStyleProperties(this);
  }

  toOlStyle(fontFace) {
    if (!this._style) {
      const font =
        this.weight + ' ' + this.size + '/' + this.lineHeight + ' ' + fontFace;

      this._style = new Style({
        text: new TextStyle({
          textAlign: this.align,
          textBaseline: this.baseline,
          offsetX: this.offsetX,
          offsetY: this.offsetY,
          font: font,

          stroke: new Stroke({
            color: this.borderColor,
            width: this.borderWidth,
          }),

          fill: new Fill({
            color: this.color,
          }),
        }),
      });
    }

    return this._style;
  }

  toJSON() {
    return {
      isVisible: this.isVisible,
      text: this.text,
      align: this.align,
      baseline: this.baseline,
      weight: this.weight,
      color: this.color,
      borderColor: this.borderColor,
      borderWidth: this.borderWidth,
      size: this.size,
      lineHeight: this.lineHeight,
      offsetX: this.offsetX,
      offsetY: this.offsetY,
    };
  }
}
