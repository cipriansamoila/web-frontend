import { tracked } from '@glimmer/tracking';

export default class ModelListWithDefault {
  @tracked useCustomList = false;
  _list = null;
  idList = [];
  model = '';

  store;

  constructor(serialized, store, model) {
    this.store = store;
    this.model = model;

    this.useCustomList = false;
    this.idList = [];

    if (!serialized) {
      return;
    }

    if (serialized.useCustomList) {
      this.useCustomList = serialized.useCustomList;
    }

    if (serialized.list) {
      this.idList = serialized.list;
    }
  }

  get list() {
    if (this._list === null) {
      this._list = [];
      this.fetch();
    }

    return this._list;
  }

  set list(value) {
    if (this._list === null) {
      this._list = [];
    }

    this._list.setObjects(value);
  }

  async fetch() {
    if (!Array.isArray(this._list)) {
      this._list = [];
    }

    for (let id of this.idList) {
      let item = this.store.peekRecord(this.model, id);

      if (!item) {
        item = await this.store.findRecord(this.model, id, { reload: true });
      }

      this._list.addObject(item);
    }
  }

  toJSON() {
    let list = this.idList;

    if (this._list) {
      list = this._list.map((a) => a.get('id'));
    }

    return {
      useCustomList: this.useCustomList,
      list,
    };
  }
}
