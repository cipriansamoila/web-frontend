export default class RequiredAttributes {
  hasInvalidRequiredAttribute(attribute, answerAttributes) {
    const key = attribute.name;
    const tmp = answerAttributes;
    if (tmp[key] === null || tmp[key] === undefined || tmp[key] === '') {
      return true;
    }

    return false;
  }

  validate(answer) {
    const validationList = answer.icons.map((icon) => {
      const iconName = icon.name;
      const requiredAttributes = icon.attributes.filter((a) => a.isRequired);
      const answerAttributes = answer.attributes[iconName];

      let answers = [];

      if (Array.isArray(answerAttributes)) {
        answers = answerAttributes;
      } else {
        answers.push(answerAttributes ?? {});
      }

      const invalidAttributes = requiredAttributes.filter((requiredAttribute) =>
        answers.find((a) =>
          this.hasInvalidRequiredAttribute(requiredAttribute, a)
        )
      );

      return invalidAttributes.length == 0;
    });

    return validationList.filter((a) => a == false).length > 0;
  }
}
