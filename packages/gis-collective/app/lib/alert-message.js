import { tracked } from '@glimmer/tracking';

export default class AlertMessage {
  @tracked message;
  @tracked level;
  @tracked id;
  @tracked fix;

  constructor(message, level, id, onFix) {
    this.message = message;
    this.level = level;
    this.id = id;
    this.fix = onFix;
  }

  get pieces() {
    return this.message
      .split('.')
      .map((a) => a.trim())
      .filter((a) => a != '');
  }

  get textMessage() {
    if (!this.fix || this.pieces.length <= 1) {
      return this.message;
    }

    return this.pieces.slice(0, this.pieces.length - 1).join('.') + '.';
  }

  get fixMessage() {
    if (!this.fix || this.pieces.length <= 1) {
      return null;
    }

    return this.pieces[this.pieces.length - 1];
  }
}
