import { tracked } from '@glimmer/tracking';

export default class SheetRowData {
  @tracked index;
  @tracked cols;
}
