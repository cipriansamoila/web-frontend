import { tracked } from '@glimmer/tracking';
import { htmlSafe } from '@ember/string';

export class SheetColData {
  @tracked name;
  @tracked valuePath;
  @tracked width = 100;
  @tracked type;
  @tracked editorType;

  constructor(data) {
    if (!data) {
      return;
    }

    if (data.name) {
      this.name = data.name;
    }
    if (data.valuePath) {
      this.valuePath = data.valuePath;
    }
    if (data.width) {
      this.width = data.width;
    }
    if (data.type) {
      this.type = data.type;
    }
    if (data.editorType) {
      this.editorType = data.editorType;
    }
    if (data.key) {
      this.key = data.key;
    }
    if (data.icon) {
      this.icon = data.icon;
    }
    if (data.options) {
      this.options = data.options;
    }
  }

  get style() {
    return htmlSafe(`width: ${this.width}px`);
  }

  get typeClass() {
    if (!this.editorType) {
      return `editor-none`;
    }

    if (this.editorType.indexOf('inline') != -1) {
      return `editor-inline`;
    }

    return `editor-custom`;
  }
}

export function attributeToSheetColData(icon, attribute) {
  let editorType = 'inline-text';
  let options = {};

  if (attribute.type == 'integer') {
    editorType = 'inline-integer';
  }

  if (attribute.type == 'decimal') {
    editorType = 'inline-decimal';
  }

  if (
    icon?.allowMany ||
    ['options with other', 'options', 'boolean'].indexOf(attribute.type) != -1
  ) {
    editorType = 'icon-attribute';
  }

  let type = 'icon-attribute';
  options.icon = icon;
  options.key = attribute.name;

  return new SheetColData({
    name: attribute.name,
    valuePath: `attributes`,
    key: attribute.name.replace(/[^0-9a-z]/gi, '-').toLowerCase(),
    editorType,
    icon,
    options,
    type,
  });
}
