export function getSizeFromWidth(width) {
  if (width <= 576) {
    return 'mobile';
  }

  if (width <= 768) {
    return 'tablet';
  }

  return 'desktop';
}
