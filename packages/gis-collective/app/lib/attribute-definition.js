import { tracked } from '@glimmer/tracking';

export default class AttributeDefinition {
  @tracked name = '';
  @tracked displayName = '';
  @tracked help = '';
  @tracked type = '';
  @tracked options = '';
  @tracked isPrivate = false;
  @tracked from = {};
  @tracked isInherited = false;
  @tracked isRequired = false;

  constructor(deserialized) {
    if (deserialized.name) {
      this.name = deserialized.name;
    }

    if (deserialized.displayName) {
      this.displayName = deserialized.displayName;
    }

    if (deserialized.help) {
      this.help = deserialized.help;
    }

    if (deserialized.type) {
      this.type = deserialized.type;
    }

    if (deserialized.options) {
      this.options = deserialized.options;
    }

    if (deserialized.isPrivate) {
      this.isPrivate = deserialized.isPrivate;
    }

    if (deserialized.isRequired) {
      this.isRequired = deserialized.isRequired;
    }

    if (deserialized.isInherited) {
      this.isInherited = deserialized.isInherited;
    }

    if (deserialized.from) {
      this.from = deserialized.from;
    }
  }

  get optionsType() {
    if (this.type == 'options' || this.type == 'options with other') {
      return 'text';
    }

    return '';
  }

  toJSON() {
    return {
      name: this.name,
      displayName: this.displayName,
      help: this.help,
      type: this.type,
      options: this.options,
      isPrivate: this.isPrivate,
      from: this.from,
      isInherited: this.isInherited,
      isRequired: this.isRequired,
    };
  }
}
