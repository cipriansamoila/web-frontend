import { marked } from 'marked';
import sanitizeHtml from 'sanitize-html';

export function toContentBlocks(content, optionalTitle) {
  let blocks = [];

  if (!content) {
    content = '';
  }

  if (typeof content == 'string') {
    const lexer = marked.lexer(content);

    lexer.forEach((item) => {
      if (item.type == 'space') return;

      if (item.type == 'heading') {
        blocks.push({
          type: 'header',
          data: {
            text: marked.parseInline(item.text),
            level: item.depth,
          },
        });

        return;
      }

      if (item.type == 'quote') {
        blocks.push({
          type: 'quote',
          data: {
            text: marked.parseInline(item.text),
          },
        });

        return;
      }

      if (item.type == 'paragraph') {
        blocks.push({
          type: 'paragraph',
          data: {
            text: marked.parseInline(item.text),
          },
        });

        return;
      }

      if (item.type == 'list') {
        blocks.push({
          type: 'list',
          data: {
            style: item.ordered ? 'ordered' : 'unordered',
            items: item.items.map((a) => marked.parseInline(a.text)),
          },
        });

        return;
      }

      blocks.push({
        type: 'paragraph',
        data: {
          text: item.raw,
        },
      });
    });
  } else if (content.blocks) {
    blocks = content.blocks;
  }

  if (
    optionalTitle &&
    !blocks.find((a) => a.type == 'header' && a.data.level == 1)
  ) {
    blocks = [
      {
        type: 'header',
        data: {
          text: optionalTitle,
          level: 1,
        },
      },
      ...blocks,
    ];
  }

  return { blocks };
}

export function firstParagraph(content) {
  const paragraph = content.blocks?.find(
    (a) => a.type == 'paragraph' && `${a.data.text}`.trim().length
  );

  return sanitizeHtml(paragraph?.data.text ?? '', {
    allowedTags: [],
    allowedAttributes: {},
  });
}
