import { tracked } from '@glimmer/tracking';

export default class FileMeta {
  @tracked runId;
  @tracked time;
  @tracked ping;
  @tracked ignored;
  @tracked total;
  @tracked errors;
  @tracked sent;
  @tracked duration;
  @tracked processed;
  @tracked status;
  @tracked log;

  constructor(deserialized) {
    if (typeof deserialized != 'object' || !deserialized) {
      return;
    }

    this.runId = deserialized.runId;
    this.time = deserialized.time;
    this.ping = deserialized.ping;
    this.ignored = parseInt(deserialized.ignored);
    this.total = parseInt(deserialized.total);
    this.errors = parseInt(deserialized.errors);
    this.sent = parseInt(deserialized.sent);
    this.duration = deserialized.duration;
    this.processed = parseInt(deserialized.processed);
    this.status = deserialized.status;
    this.log = deserialized.log;
  }
}
