export class DetailedAttributeCategory {
  _icon;
  names = [];
  displayName = '';
  isTable = false;

  attributes = [];

  toAttributeSet(value) {
    if (typeof value != 'object' || value === null) {
      return [];
    }

    let result = this.isTable ? [] : this.attributes;

    if (
      this.icon &&
      this.icon.attributes &&
      this.icon.attributes.length &&
      result.length == 0
    ) {
      result = this.icon.attributes.map((a) => {
        const newAttribute = new DetailedAttribute();
        newAttribute.name = a.name;
        newAttribute.displayName = a.displayName || a.name;
        newAttribute.type = a.type;
        newAttribute.isPresent = false;

        return newAttribute;
      });
    }

    Object.keys(value).forEach((key) => {
      result
        .filter((a) => a.name == key)
        .forEach((a) => {
          a.value = value[key];
          a.isPresent = true;
        });
    });

    const unmapped = Object.keys(value)
      .filter((key) => !result.find((a) => a.name == key))
      .map((key) => {
        const newAttribute = new DetailedAttribute();
        newAttribute.name = key;
        newAttribute.displayName = key;
        newAttribute.value = value[key];
        newAttribute.isPresent = true;

        return newAttribute;
      });

    return result.concat(unmapped);
  }

  get icon() {
    return this._icon;
  }

  set icon(value) {
    this._icon = value;
    this.isTable = value.allowMany;

    if (!this.isTable) {
      this.attributes = this.toAttributeSet({});
    }
  }

  get isEmpty() {
    if (this.attributes.length == 0) {
      return true;
    }

    if (this.isTable && this.attributes.length > 0) {
      return false;
    }

    return !this.attributes.find((a) => a.value);
  }

  fill(value) {
    if (Array.isArray(value) && !this.isTable) {
      return value.forEach((a) => this.fill(a));
    }

    if (this.isTable) {
      let newAttributes = [];

      if (Array.isArray(value)) {
        newAttributes = value.map((a) => this.toAttributeSet(a));
      } else if (typeof value === 'object' && value !== null) {
        newAttributes.push(this.toAttributeSet(value));
      }

      return newAttributes.forEach((a) => {
        this.attributes.push(a);
      });
    }

    this.attributes = this.toAttributeSet(value);
  }

  canFill(key) {
    if (!this.names || !this.names.length) {
      return false;
    }

    return (
      this.names
        .filter((a) => a)
        .map((a) => a.toLowerCase())
        .indexOf(key.toLowerCase()) != -1
    );
  }

  toJSON() {
    const attributes = this.attributes.map((a) => {
      if (Array.isArray(a)) {
        return a.map((b) => b.toJSON());
      }

      return a.toJSON();
    });
    return {
      icon: this.icon?.get?.('id'),
      names: this.names,
      displayName: this.displayName,
      isTable: this.isTable,
      isEmpty: this.isEmpty,
      attributes,
    };
  }
}

export class DetailedAttribute {
  name = '';
  displayName = '';
  type;
  value;
  isPresent;

  toJSON() {
    return {
      name: this.name,
      displayName: this.displayName,
      type: this.type,
      value: this.value,
      isPresent: this.isPresent,
    };
  }
}
