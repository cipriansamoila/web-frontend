import { tracked } from '@glimmer/tracking';

export default class BaseMapLayer {
  @tracked type = '';
  @tracked options = {};

  constructor(serialized) {
    if (!serialized) {
      return;
    }

    if (serialized.type) {
      this.type = serialized.type;
    }

    if (serialized.options) {
      this.options = serialized.options;
    }
  }

  toJSON() {
    return {
      type: this.type,
      options: this.options,
    };
  }
}
