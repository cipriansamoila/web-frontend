import { Fill, Stroke, Style, Circle, RegularShape } from 'ol/style';
import { tracked } from '@glimmer/tracking';
import StyleCache from './style-cache';

export default class StyleFactory {
  @tracked mapStyleElement;
  defaultRadius = 15;

  constructor() {
    this.cache = new StyleCache();
  }

  update(className, style) {
    const properties = this.extractElementProperties(className);

    style.borderColor = properties.borderColor;
    style.backgroundColor = properties.backgroundColor;
    style.color = properties.backgroundColor;
    style.borderWidth = properties.borderWidth;
    style.lineDash = properties.lineDash;
    style.font = properties.font;
    style.size = properties.size;
    style.opacity = properties.opacity;
    style.zIndex = properties.zIndex;
  }

  extractElementProperties(className) {
    const element = this.mapStyleElement;

    const properties = {
      borderColor: '#fff',
      backgroundColor: 'rgba(255,255,255, 0.6)',
      borderWidth: 1,
      lineDash: [],
      font: '',
      size: this.defaultRadius,
    };

    if (
      element &&
      element.querySelector &&
      element.querySelector(`.${className}`)
    ) {
      const elm = element.querySelector(`.${className}`);
      const styles = getComputedStyle(elm);

      properties.borderColor = styles.borderLeftColor;
      properties.borderWidth = parseFloat(styles.borderLeftWidth);
      properties.backgroundColor = styles.backgroundColor;
      properties.font = `${styles.fontWeight} ${styles.fontSize} ${styles.fontFamily}`;
      properties.fontFamily = styles.fontFamily;
      properties.fontWeight = styles.fontWeight;
      properties.fontSize = styles.fontSize;
      properties.opacity = styles.opacity;
      properties.size = parseFloat(styles.width) || parseFloat(styles.height);
      properties.zIndex = parseInt(styles.zIndex);

      try {
        properties.lineDash = JSON.parse(elm.textContent.trim());
        // eslint-disable-next-line no-empty
      } catch (err) {}
    }

    return properties;
  }

  extractProperties(className, isSelected) {
    let result = {};

    if (typeof className == 'object') {
      if (!className.size) {
        className.size = this.defaultRadius;
      }

      result = { className: className.id, properties: className };
    } else {
      const properties = this.extractElementProperties(className);

      result = { className, properties };
    }

    if (isSelected) {
      result.className = result.className + '.selected';
      const properties = this.extractElementProperties('selection');
      result.properties.borderColor = properties.borderColor;
      result.properties.backgroundColor = properties.backgroundColor;
      result.properties.borderWidth = properties.borderWidth;
    }

    return result;
  }

  shape(prop, isSelected) {
    let { className, properties } = this.extractProperties(prop, isSelected);

    const key = `shape.${className}`;

    if (className && this.cache.styles[key]) {
      return this.cache.styles[key];
    }

    const style = new Style({
      stroke: new Stroke({
        color: properties.borderColor,
        lineDash: properties.lineDash,
        width: properties.borderWidth,
      }),
      fill: new Fill({
        color: properties.backgroundColor,
      }),
    });

    if (isSelected) {
      style.setZIndex(99999);
    }

    if (className) {
      this.cache.styles[key] = style;
    }

    return style;
  }

  regularShape(prop, points = 4, rotation = 0, isSelected = false) {
    let { className, properties } = this.extractProperties(prop, isSelected);
    const key = `regularShape.${points}.${rotation}.${className}`;

    if (prop.id && this.cache.styles[key]) {
      return this.cache.styles[key];
    }

    const shape = new RegularShape({
      stroke: new Stroke({
        color: properties.borderColor,
        lineDash: properties.lineDash,
        width: properties.borderWidth,
      }),
      fill: new Fill({
        color: properties.backgroundColor,
      }),
      points,
      rotation,
      radius: properties.size,
      angle: 0,
    });

    if (prop.id) {
      this.cache.styles[key] = shape;
    }

    return shape;
  }

  square(param, isSelected) {
    return this.regularShape(param, 4, (Math.PI / 4) * 3, isSelected);
  }

  pentagon(param, isSelected) {
    return this.regularShape(param, 5, (Math.PI / 4) * 8, isSelected);
  }

  hexagon(param, isSelected) {
    return this.regularShape(param, 6, (Math.PI / 4) * 2, isSelected);
  }

  circle(prop, isSelected) {
    let { className, properties } = this.extractProperties(prop, isSelected);

    const key = `circle.${className}`;
    if (properties.id && this.cache.styles[key]) {
      return this.cache.styles[key];
    }

    const style = new Circle({
      radius: properties.size,
      stroke: new Stroke({
        color: properties.borderColor,
        lineDash: properties.lineDash,
        width: properties.borderWidth,
      }),
      fill: new Fill({
        color: properties.backgroundColor,
      }),
    });

    if (properties.id) {
      this.cache.styles[key] = style;
    }

    return style;
  }

  iconFrame(prop, isSelected) {
    let { className, properties } = this.extractProperties(prop, isSelected);

    const key = `iconFrame.${className}`;

    if (properties.id && this.cache.styles[key]) {
      return this.cache.styles[key];
    }

    let shape = 'circle';

    if (['square', 'pentagon', 'hexagon'].indexOf(properties.shape) != -1) {
      shape = properties.shape;
    }

    const style = new Style({
      image: this[shape].apply(this, [properties, isSelected]),
    });

    if (isSelected) {
      style.setZIndex(99999);
    }

    if (properties.id) {
      this.cache.styles[key] = style;
    }

    return style;
  }
}
