import { Style } from 'ol/style';

export default class StyleCache {
  styles = {};

  constructor() {
    this.emptyStyle = new Style();
  }
}
