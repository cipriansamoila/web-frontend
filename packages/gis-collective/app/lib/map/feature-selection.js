import { getCenter } from 'ol/extent';
import { tracked } from '@glimmer/tracking';

export class FeatureSelection {
  @tracked coordinates;
  @tracked type;
  @tracked group;
  @tracked featureId;

  update(feature) {
    if (!feature) {
      this.group = null;
      this.coordinate = null;
      this.type = null;
      return;
    }

    const properties = feature.getProperties();
    const layer = properties.layer;
    const idList = properties['idList'];
    const canView = properties['canView'];
    const visibility = properties['visibility'];
    const _id = properties['_id'];

    this.coordinates = getCenter(feature?.getGeometry?.().getExtent?.());

    if (layer == 'groups' && typeof idList == 'string') {
      this.group = feature;
      this.featureId = null;
      this.type = 'group';
      return;
    }

    if (canView !== 'true' && visibility == '-1') {
      this.group = null;
      this.featureId = null;
      this.type = 'pending';
      return;
    }

    if (typeof _id == 'string') {
      this.group = null;
      this.featureId = _id;
      this.type = 'feature';
    }
  }
}
