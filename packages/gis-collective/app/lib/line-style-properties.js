import { tracked } from '@glimmer/tracking';
import { Fill, Stroke, Style } from 'ol/style';

export default class LineStyleProperties {
  @tracked borderColor = 'blue';
  @tracked backgroundColor = 'transparent';
  @tracked borderWidth = 1;
  @tracked zIndex;
  @tracked lineDash = [];

  _style = null;

  constructor(value) {
    if (!value || typeof value != 'object') {
      value = {};
    }

    if (value.borderColor) {
      this.borderColor = value.borderColor;
    }

    if (value.backgroundColor) {
      this.backgroundColor = value.backgroundColor;
    }

    if (value.borderWidth) {
      this.borderWidth = value.borderWidth;
    }

    if (value.lineDash) {
      this.lineDash = value.lineDash;
    }

    if (value.label) {
      this.label = value.label;
    }

    if (value.zIndex) {
      this.zIndex = value.zIndex;
    }
  }

  clone() {
    return new LineStyleProperties(this);
  }

  toOlStyle() {
    if (!this._style) {
      this._style = new Style({
        stroke: new Stroke({
          color: this.borderColor,
          lineDash: this.lineDash,
          width: this.borderWidth,
        }),
        fill: new Fill({
          color: this.backgroundColor,
        }),
        zIndex: this.zIndex,
      });
    }

    return this._style;
  }

  toJSON() {
    return {
      borderColor: this.borderColor,
      backgroundColor: this.backgroundColor,
      borderWidth: this.borderWidth,
      lineDash: this.lineDash,
    };
  }
}
