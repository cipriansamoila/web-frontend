export function addIconGroupToStore(group, store) {
  if (Array.isArray(group['icons']) && group['icons'].length > 0) {
    group['icons'] = group['icons'].map((icon) => {
      return store.push(store.normalize('icon', icon));
    });
  }

  Object.keys(group['categories']).forEach((a) =>
    addIconGroupToStore(group['categories'][a], store)
  );
}

export function iconGroupToList(group) {
  const list = [];

  if (Array.isArray(group['icons']) && group['icons'].length > 0) {
    list.addObjects(group['icons']);
  }

  Object.keys(group['categories']).forEach((a) =>
    list.addObjects(iconGroupToList(group['categories'][a]))
  );

  return list;
}
