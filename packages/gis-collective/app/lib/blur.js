import { decode } from 'blurhash';
import md5 from 'blueimp-md5';

export function idToHash(id) {
  const mdId = md5(id);
  return 'L6Pj0^i' + mdId.substring(0, 21);
}

export function generateBlur(blurHash, width, height) {
  let hash = blurHash ?? '';

  if (hash == '') {
    hash = 'UKO2?U%2Tw=w]~RBVZRi};RPxuwHtLOtxZ%g';
  }

  const pixels = decode(hash, width, height, 1);
  const css = pixelsToCss(pixels, width, height);

  return Object.keys(css)
    .map((key) => `${key}:${css[key]};`)
    .join('');
}

export function pixelsToCss(pixels, width, height) {
  let backgroundPositions = [];
  let backgroundImages = [];

  for (let y = 0; y < height; y++) {
    let gradients = [];

    for (let x = 0; x < width; x++) {
      let r = getPixel(pixels, x, y, width, 0);
      let g = getPixel(pixels, x, y, width, 1);
      let b = getPixel(pixels, x, y, width, 2);

      let startPercent = x == 0 ? '' : getRoundedPercentageOf(x, width);
      let endPercent = x == width ? '' : getRoundedPercentageOf(x + 1, width);

      const percentage = `${startPercent} ${endPercent}`.trim();
      gradients.push(`rgb(${r},${g},${b}) ${percentage}`);
    }

    backgroundImages.push('linear-gradient(90deg,' + gradients.join(',') + ')');
    if (y == 0) {
      backgroundPositions.push('0 0');
    } else {
      backgroundPositions.push(`0 ${getRoundedPercentageOf(y, height - 1)}`);
    }
  }

  return {
    'background-image': backgroundImages.join(','),
    'background-position': backgroundPositions.join(','),
    transform: 'scale(1.2)',
    'background-repeat': 'no-repeat',
    'background-size': `100% ${100 / height}%`,
    filter: 'blur(24px)',
  };
}

export function getRoundedPercentageOf(part, whole) {
  let value = (part / whole) * 100;

  return `${Math.round(value)}%`;
}

export function getPixel(list, x, y, width, index) {
  let channels = 4;
  let bytesPerRow = width * channels;

  return list[channels * x + index + y * bytesPerRow];
}
