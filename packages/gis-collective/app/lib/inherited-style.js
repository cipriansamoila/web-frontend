import { tracked } from '@glimmer/tracking';
import IconStyle from './icon-style';

export default class InheritedStyle {
  @tracked hasCustomStyle;
  @tracked types;

  constructor(value) {
    if (!value || typeof value != 'object') {
      value = {};
    }

    this.hasCustomStyle = value.hasCustomStyle || false;
    this.types = new IconStyle(value['types']);
  }

  toJSON() {
    const result = {
      hasCustomStyle: this.hasCustomStyle,
    };

    if (this.hasCustomStyle) {
      result.types = this.types.toJSON();
    }

    return result;
  }
}
