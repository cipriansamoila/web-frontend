import { tracked } from '@glimmer/tracking';
import GeoJSON from 'ol/format/GeoJSON';
import { getCenter } from 'ol/extent';

export default class GeoJson {
  @tracked type = 'Point';
  @tracked value;
  @tracked coordinates = [];

  constructor(deserialized) {
    if (!deserialized || typeof deserialized != 'object') {
      deserialized = {};
    }

    if (deserialized.type) {
      this.type = deserialized.type;
    }

    if (deserialized.type) {
      this.value = deserialized.value;
    }

    if (deserialized.coordinates) {
      this.coordinates = deserialized.coordinates.slice();
    }
  }

  get center() {
    if (this.type == 'Point') {
      return this;
    }

    if (this.type == 'MultiPoint') {
      return new GeoJson({
        type: 'Point',
        coordinates: this.coordinates[0],
      });
    }

    if (this.type == 'LineString') {
      const middle = parseInt(this.coordinates.length / 2);
      return new GeoJson({
        type: 'Point',
        coordinates: this.coordinates[middle],
      });
    }

    if (this.type == 'LineString') {
      const middle = parseInt(this.coordinates.length / 2);
      return new GeoJson({
        type: 'Point',
        coordinates: this.coordinates[middle],
      });
    }

    if (this.type == 'MultiLineString') {
      const middle = parseInt(this.coordinates[0].length / 2);
      return new GeoJson({
        type: 'Point',
        coordinates: this.coordinates[0][middle],
      });
    }

    if (this.type == 'Polygon') {
      return new GeoJson({
        type: 'Point',
        coordinates: getCenter(this.toOlFeature().getGeometry().getExtent()),
      });
    }

    if (this.type == 'MultiPolygon') {
      return new GeoJson({
        type: 'Point',
        coordinates: getCenter(this.toOlFeature().getGeometry().getExtent()),
      });
    }

    return new GeoJson({
      type: 'Point',
      coordinates: [0, 0],
    });
  }

  toExtent(projection) {
    return this.toOlFeature(projection).getGeometry().getExtent();
  }

  toOlFeature(projection) {
    const geoJson = new GeoJSON();

    const feature = geoJson.readFeature(this.toJSON());

    if (projection) {
      feature.getGeometry().transform('EPSG:4326', projection);
    }

    return feature;
  }

  toJSON() {
    const result = {};

    if (this.type) {
      result.type = this.type;
    }

    if (this.value) {
      result.value = this.value;
    }

    if (this.coordinates) {
      result.coordinates = this.coordinates;
    }

    return result;
  }
}
