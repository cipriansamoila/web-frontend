import { tracked } from '@glimmer/tracking';

export default class PictureMeta {
  @tracked renderMode = '';

  constructor(serialized) {
    if (!serialized) {
      return;
    }

    if (serialized.renderMode) {
      this.renderMode = serialized.renderMode;
    }
  }

  toJSON() {
    return {
      renderMode: this.renderMode,
    };
  }
}
