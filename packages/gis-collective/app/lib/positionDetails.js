import { tracked } from '@glimmer/tracking';

export default class PositionDetails {
  @tracked capturedUsingGPS = false;
  @tracked type = null;
  @tracked altitude = 0;
  @tracked accuracy = 1000;
  @tracked altitudeAccuracy = 1000;
  @tracked searchTerm = null;
  @tracked address = null;

  constructor(deserialized) {
    if (!deserialized || typeof deserialized != 'object') {
      deserialized = {};
    }

    if (deserialized.capturedUsingGPS) {
      this.capturedUsingGPS = deserialized.capturedUsingGPS;
    }

    if (deserialized.type) {
      this.type = deserialized.type;
    }

    if (deserialized.searchTerm) {
      this.searchTerm = deserialized.searchTerm;
    }

    if (deserialized.address) {
      this.address = deserialized.address;
    }

    if (deserialized.altitude) {
      this.altitude = deserialized.altitude;
    }

    if (deserialized.accuracy) {
      this.accuracy = deserialized.accuracy;
    }

    if (deserialized.altitudeAccuracy) {
      this.altitudeAccuracy = deserialized.altitudeAccuracy;
    }

    if (deserialized['altitude accuracy']) {
      this.altitudeAccuracy = deserialized['altitude accuracy'];
    }
  }

  toJSON() {
    const result = {
      altitude: this.altitude,
      accuracy: this.accuracy,
      altitudeAccuracy: this.altitudeAccuracy,
    };

    if (this.type) {
      result.type = this.type;
    } else {
      result.capturedUsingGPS = this.capturedUsingGPS ?? false;
    }

    if (this.type) {
      result.type = this.type;
    }

    if (this.searchTerm) {
      result.searchTerm = this.searchTerm;
    }

    if (this.address) {
      result.address = this.address;
    }

    return result;
  }
}
