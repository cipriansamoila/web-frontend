export function blobToBase64(blob) {
  const reader = new FileReader();
  reader.readAsDataURL(blob);

  return new Promise((resolve) => {
    reader.onloadend = () => {
      resolve(reader.result);
    };
  });
}

export async function rawBlobToBase64(blob) {
  const result = await blobToBase64(blob);

  const pos = result.indexOf('base64,');
  return result.slice(pos + 7);
}
