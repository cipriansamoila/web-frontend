import { tracked } from '@glimmer/tracking';
import { Fill, Stroke, Style, Circle, RegularShape, Icon } from 'ol/style';

export default class PointStyleProperties {
  @tracked isVisible = true;
  @tracked shape = 'circle';
  @tracked borderColor = 'white';
  @tracked backgroundColor = 'rgba(255,255,255,0.5)';
  @tracked borderWidth = 1;
  @tracked size = 21;
  @tracked opacity = 1;
  @tracked zIndex;
  @tracked image;
  @tracked label;

  constructor(value) {
    if (!value || typeof value != 'object') {
      value = {};
    }

    if (value.isVisible === true || value.isVisible === false) {
      this.isVisible = value.isVisible;
    }

    if (value.shape) {
      this.shape = value.shape;
    }

    if (value.borderColor) {
      this.borderColor = value.borderColor;
    }

    if (value.backgroundColor) {
      this.backgroundColor = value.backgroundColor;
    }

    if (value.borderWidth) {
      this.borderWidth = value.borderWidth;
    }

    if (value.size) {
      this.size = parseInt(value.size) || 21;
    }

    if (value.image) {
      this.image = value.image;
    }

    if (value.label) {
      this.label = value.label;
    }

    if (value.zIndex) {
      this.zIndex = value.zIndex;
    }

    this.badgeRadius = 8;
  }

  regularShape(points = 4, rotation = 0) {
    return new RegularShape({
      stroke: new Stroke({
        color: this.borderColor,
        lineDash: this.lineDash,
        width: this.borderWidth,
      }),
      fill: new Fill({
        color: this.backgroundColor,
      }),
      points,
      rotation,
      radius: this.size,
      angle: 0,
      zIndex: this.zIndex,
    });
  }

  toOlSquare() {
    return this.regularShape(4, (Math.PI / 4) * 3);
  }

  toOlPentagon() {
    return this.regularShape(5, (Math.PI / 4) * 8);
  }

  toOlHexagon() {
    return this.regularShape(6, (Math.PI / 4) * 2);
  }

  toOlCircle() {
    return new Circle({
      radius: this.size,
      stroke: new Stroke({
        color: this.borderColor,
        lineDash: this.lineDash,
        width: this.borderWidth,
      }),
      fill: new Fill({
        color: this.backgroundColor,
      }),
    });
  }

  toOlBadgeStyle() {
    if (!this.badgeBgImage) {
      this.badgeBgImage = new Circle({
        radius: this.badgeRadius,
        stroke: new Stroke({
          color: this.borderColor,
          lineDash: this.lineDash,
          width: this.borderWidth,
        }),
        fill: new Fill({
          color: this.backgroundColor,
        }),
        displacement: [this.size, this.size - this.badgeRadius],
      });
    }

    return new Style({
      image: this.badgeBgImage,
      zIndex: this.zIndex,
    });
  }

  toOlBadgeIcon(src) {
    const size = this.badgeRadius * 2 + this.borderWidth;
    const zIndex = this.zIndex ? this.zIndex + 1 : undefined;

    if (!this.badgeCache) {
      this.badgeCache = {};
    }

    const key = `${this.size}_${src}`;
    if (!this.badgeCache[key]) {
      this.badgeCache[key] = new Icon({
        scale: 1,
        opacity: 1,
        src,
        anchor: [-this.size + size / 2, this.size],
        anchorXUnits: 'pixels',
        anchorYUnits: 'pixels',
      });
    }

    return new Style({ image: this.badgeCache[key], zIndex });
  }

  toOlImageStyle() {
    if (!this.image) {
      return;
    }

    if (!this._imageStyle) {
      const scale = this.size / 21 + 0.3;
      const zIndex = this.zIndex ? this.zIndex + 1 : undefined;
      this._imageStyle = new Style({
        image: new Icon({
          scale,
          opacity: this.opacity,
          anchor: [0.5, 0.5],
          anchorXUnits: 'fraction',
          anchorYUnits: 'fraction',
          src: `${this.image}/sm`,
        }),
        zIndex,
      });
    }

    return this._imageStyle;
  }

  toOlStyle() {
    if (!this._style) {
      let image;

      if (this.shape == 'square') {
        image = this.toOlSquare();
      }

      if (this.shape == 'pentagon') {
        image = this.toOlPentagon();
      }

      if (this.shape == 'hexagon') {
        image = this.toOlHexagon();
      }

      if (!image) {
        image = this.toOlCircle();
      }

      this._style = new Style({
        image,
        zIndex: this.zIndex,
      });
    }

    return this._style;
  }

  clone() {
    return new PointStyleProperties(this);
  }

  toJSON() {
    return {
      isVisible: this.isVisible,
      shape: this.shape,
      borderColor: this.borderColor,
      backgroundColor: this.backgroundColor,
      borderWidth: this.borderWidth,
      size: this.size,
    };
  }
}
