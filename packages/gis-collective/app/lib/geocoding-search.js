import { tracked } from '@glimmer/tracking';

export default class GeocodingSearch {
  @tracked results;
  @tracked hasNewResults;

  constructor(store) {
    this.store = store;
  }

  clear() {
    this.results = [];
    this.hasNewResults = false;
    this.searchAddressTerm = null;
  }

  async search(query) {
    this.hasNewResults = false;

    if (query?.length < 4) {
      this.results = [];
      return;
    }

    if (this.searchAddressTerm != query) {
      this.searchAddressTerm = query;
      try {
        this.results = await this.store.query('geocoding', {
          query,
        });
      } catch (err) {
        this.results = [];
      }
      this.hasNewResults = this.results?.length > 0;
    }
  }
}
