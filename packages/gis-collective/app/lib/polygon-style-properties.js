import { tracked } from '@glimmer/tracking';
import LineStyleProperties from './line-style-properties';

export default class PolygonStyleProperties extends LineStyleProperties {
  @tracked hideBackgroundOnZoom = true;

  constructor(value) {
    super(value);

    if (!value || typeof value != 'object') {
      value = {};
    }

    if (
      value.hideBackgroundOnZoom === true ||
      value.hideBackgroundOnZoom === false
    ) {
      this.hideBackgroundOnZoom = value.hideBackgroundOnZoom;
    }
  }

  clone() {
    return new PolygonStyleProperties(this);
  }

  toJSON() {
    const result = super.toJSON();
    result.hideBackgroundOnZoom = this.hideBackgroundOnZoom;

    return result;
  }
}
