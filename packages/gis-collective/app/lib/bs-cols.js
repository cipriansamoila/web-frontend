function countDashes(value) {
  return value.split('-').length - 1;
}

export function setDefaultBsProperty(property, value, options) {
  const expectedDashes = countDashes(property) + 1;
  const newOptions = options
    .slice()
    .filter(
      (a) =>
        !(a.indexOf(`${property}-`) == 0 && countDashes(a) == expectedDashes)
    );

  if (value != 'default') {
    newOptions.push(`${property}-${value}`);
  }

  return newOptions;
}

export function setSizedBsProperty(property, size, value, options) {
  const expectedDashes = countDashes(property) + 2;

  const newOptions = options
    .slice()
    .filter(
      (a) =>
        !(
          a.indexOf(`${property}-${size}-`) == 0 &&
          countDashes(a) == expectedDashes
        )
    );

  if (value != 'default') {
    newOptions.push(`${property}-${size}-${value}`);
  }

  return newOptions;
}

export function defaultBsProperty(property, options) {
  const expectedDashes = countDashes(property) + 1;
  const defaultCols = options.filter(
    (a) => a.indexOf(`${property}-`) == 0 && countDashes(a) == expectedDashes
  );

  if (!defaultCols.length) {
    return 'default';
  }

  const pieces = defaultCols[0].split('-');

  return pieces[pieces.length - 1];
}

export function sizedBsProperty(property, size, options) {
  const defaultCols = options.filter(
    (a) => a.indexOf(`${property}-${size}-`) == 0
  );

  if (!defaultCols.length) {
    return 'default';
  }

  const pieces = defaultCols[0].split('-');

  return pieces[pieces.length - 1];
}
