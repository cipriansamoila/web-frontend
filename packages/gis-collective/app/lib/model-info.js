import { tracked } from '@glimmer/tracking';

export default class LineStyleProperties {
  @tracked createdOn = null;
  @tracked author = null;
  @tracked lastChangeOn = null;
  @tracked originalAuthor = null;
  @tracked changeIndex = null;

  constructor(value) {
    if (!value || typeof value != 'object') {
      value = {};
    }

    if (value.author) {
      this.author = value.author;
    }

    if (value.changeIndex) {
      this.changeIndex = value.changeIndex;
    }

    if (value.createdOn) {
      this.createdOn = new Date(Date.parse(value.createdOn));
    }

    if (value.lastChangeOn) {
      this.lastChangeOn = new Date(value.lastChangeOn);
    }

    if (value.originalAuthor) {
      this.originalAuthor = value.originalAuthor;
    }
  }

  toJSON() {
    const result = {};

    if (this.createdOn) {
      try {
        result.createdOn = this.createdOn.toISOString();
        // eslint-disable-next-line no-empty
      } catch (e) {}
    }

    if (this.lastChangeOn) {
      try {
        result.lastChangeOn = this.lastChangeOn.toISOString();
        // eslint-disable-next-line no-empty
      } catch (e) {}
    }

    if (
      this.author &&
      (typeof this.author === 'string' || this.author instanceof String)
    ) {
      result.author = this.author;
    }

    if (
      this.originalAuthor &&
      (typeof this.originalAuthor === 'string' ||
        this.originalAuthor instanceof String)
    ) {
      result.originalAuthor = this.originalAuthor;
    }

    if (this.changeIndex && !isNaN(parseInt(this.changeIndex))) {
      result.changeIndex = parseInt(this.changeIndex);
    }

    return result;
  }
}
