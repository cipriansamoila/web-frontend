import { DetailedAttributeCategory } from './attribute-category';

export default function displayIconAttributes(attributes, icons) {
  const categories =
    icons?.map((a) => (a.get ? a.get('asCategory') : a.asCategory)) ?? [];

  if (!attributes) {
    return categories;
  }

  Object.keys(attributes)
    .filter(
      (key) =>
        !Array.isArray(attributes[key]) && typeof attributes[key] != 'object'
    )
    .forEach((key) => {
      if (!attributes['other']) {
        attributes['other'] = {};
      }

      attributes['other'][key] = attributes[key];
      delete attributes[key];
    });

  Object.keys(attributes).forEach((key) => {
    const existingCategories = categories.filter((a) => a?.canFill?.(key));

    if (existingCategories.length == 0) {
      const newCategory = new DetailedAttributeCategory();
      newCategory.displayName = key;
      newCategory.names = [key];
      newCategory.isTable = Array.isArray(attributes[key]);
      newCategory.fill(attributes[key]);

      categories.push(newCategory);
    } else {
      existingCategories[0].fill(attributes[key]);
    }
  });

  return categories;
}
