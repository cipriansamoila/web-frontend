import { tracked } from '@glimmer/tracking';
import PointStyleProperties from './point-style-properties';
import LineStyleProperties from './line-style-properties';
import PolygonStyleProperties from './polygon-style-properties';
import LabelStyleProperties from './label-style-properties';

export default class IconStyle {
  @tracked site;
  @tracked siteLabel;

  @tracked line;
  @tracked lineMarker;
  @tracked lineLabel;

  @tracked polygon;
  @tracked polygonMarker;
  @tracked polygonLabel;

  constructor(value) {
    if (!value || typeof value != 'object') {
      value = {};
    }

    this.site = new PointStyleProperties(value['site']);
    this.siteLabel = new LabelStyleProperties(value['siteLabel']);
    this.site.label = this.siteLabel;

    this.line = new LineStyleProperties(value['line']);
    this.lineMarker = new PointStyleProperties(value['lineMarker']);
    this.lineLabel = new LabelStyleProperties(value['lineLabel']);
    this.line.label = this.lineLabel;

    this.polygon = new PolygonStyleProperties(value['polygon']);
    this.polygonMarker = new PointStyleProperties(value['polygonMarker']);
    this.polygonLabel = new LabelStyleProperties(value['polygonLabel']);
    this.polygon.label = this.polygonLabel;

    this.cache = {};
  }

  imageStyles(image) {
    if (!this.cache[image]) {
      this.cache[image] = {
        site: this.site,
        lineMarker: this.lineMarker,
        polygonMarker: this.polygonMarker,
      };

      Object.keys(this.cache[image]).forEach((key) => {
        const b = this.cache[image][key].clone();
        b.image = image;

        this.cache[image][key] = b;
      });
    }

    return this.cache[image];
  }

  toJSON() {
    return {
      site: this.site.toJSON(),
      siteLabel: this.siteLabel.toJSON(),

      line: this.line.toJSON(),
      lineMarker: this.lineMarker.toJSON(),
      lineLabel: this.lineLabel.toJSON(),

      polygon: this.polygon.toJSON(),
      polygonMarker: this.polygonMarker.toJSON(),
      polygonLabel: this.polygonLabel.toJSON(),
    };
  }
}
