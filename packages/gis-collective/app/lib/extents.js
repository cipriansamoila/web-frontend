import MultiPolygon from 'ol/geom/MultiPolygon';
import LineString from 'ol/geom/LineString';
import Circle from 'ol/geom/Circle';
import Polygon from 'ol/geom/Polygon';
import MultiLineString from 'ol/geom/MultiLineString';

export function getGeometryExtent(value) {
  if (!value?.type) {
    return null;
  }

  if (value.type == 'Point') {
    const point = new Circle(value.coordinates.slice(), 0.005);
    return point.getExtent();
  }

  if (value.type == 'LineString') {
    const line = new LineString(value.coordinates.slice());
    return line.getExtent();
  }

  if (value.type == 'MultiLineString') {
    const line = new MultiLineString(value.coordinates.slice());
    return line.getExtent();
  }

  if (value.type == 'Polygon') {
    const polygon = new Polygon(value.coordinates.slice());
    return polygon.getExtent();
  }

  if (value.type == 'MultiPolygon') {
    const multiPolygon = new MultiPolygon(value.coordinates.slice());
    return multiPolygon.getExtent();
  }

  return null;
}
