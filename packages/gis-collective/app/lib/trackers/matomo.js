import fetch from 'fetch';

export class MatomoTracker {
  constructor(user, preferences, fastboot) {
    this.preferences = preferences;
    this.fastboot = fastboot;
    this.user = user;
    this.referer = preferences.referer;
  }

  addPage(path) {
    this.event(path, {
      action_name: encodeURIComponent('page view'),
    });
  }

  addMapView(path, mapId) {
    this.event(path, {
      action_name: encodeURIComponent('map view'),
      _cvar: encodeURIComponent(JSON.stringify({ mapId })),
    });
  }

  event(path, params) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    if (this.referer) {
      params['urlref'] = encodeURIComponent(this.referer);
      this.referer = null;
    }

    params['idsite'] = this.preferences.matomoSiteId;
    params['rec'] = 1;
    params['_id'] = this.preferences.uid;
    params['rand'] = Math.random();
    params['apiv'] = 1;

    if (path) {
      params['url'] = encodeURIComponent(
        this.preferences.protocol + this.preferences.domain + path
      );
    }

    let url = this.preferences.matomoUrl;

    if (!url) {
      return;
    }

    if (url.indexOf('/', url.length - 1) !== -1) {
      url = url.substring(0, url.length - 1);
    }

    const strParams = Object.keys(params)
      .map((k) => `${k}=${params[k]}`)
      .join('&');

    if (fetch) {
      fetch(`${url}/matomo.php?${strParams}`);
    }
  }
}
