export class MetricsTracker {
  constructor(store, preferences, fastboot) {
    this.preferences = preferences;
    this.fastboot = fastboot;
    this.store = store;
  }

  addPage() {}

  addMapView(path, mapId) {
    let labels = {};

    if (this.preferences.referer) {
      labels['referer'] = this.preferences.referer;
    }

    return this.store
      .createRecord('metric', {
        name: mapId,
        type: 'mapView',
        reporter: this.preferences.uid,
        value: 1,
        time: new Date(),
        labels,
      })
      .save();
  }
}
