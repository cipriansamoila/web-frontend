import StorageObject from 'ember-local-storage/local/object';

const Storage = StorageObject.extend();

Storage.reopenClass({
  initialState() {
    return {
      latitude: 0,
      longitude: 0,
      accuracy: 10000,
    };
  },
});

export default Storage;
