import StorageObject from 'ember-local-storage/local/object';

const Storage = StorageObject.extend();

Storage.reopenClass({
  initialState() {
    return {
      lastKnownValue: {
        new: [],
        stored: [],
      },
    };
  },
});

export default Storage;
