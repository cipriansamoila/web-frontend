import { tracked } from '@glimmer/tracking';
import AboutGroup from '../groups/about';

export default class AboutAttributes {
  intl;
  @tracked group;

  constructor(intl) {
    this.intl = intl;
  }

  get feature() {
    return this.group.feature;
  }

  set feature(value) {
    this.group = new AboutGroup(this.intl);
    this.group.feature = value;
  }

  get groups() {
    if (!this.group) {
      return [];
    }

    return [this.group];
  }
}
