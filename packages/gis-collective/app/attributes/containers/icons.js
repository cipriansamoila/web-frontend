import { tracked } from '@glimmer/tracking';
import IconGroup from '../groups/icons';

export default class IconAttributes {
  intl;
  @tracked group;

  constructor(intl) {
    this.intl = intl;
  }

  get feature() {
    return this.group.feature;
  }

  set feature(value) {
    this.group = new IconGroup(this.intl);
    this.group.feature = value;
  }

  get groups() {
    if (!this.group) {
      return [];
    }

    return [this.group];
  }
}
