import { tracked } from '@glimmer/tracking';

import PictureGroup from '../groups/pictures';

export default class PictureAttributes {
  intl;
  @tracked group;

  constructor(intl) {
    this.intl = intl;
  }

  get feature() {
    return this.group.feature;
  }

  set feature(value) {
    this.group = new PictureGroup(this.intl);
    this.group.feature = value;
  }

  get groups() {
    if (!this.group) {
      return [];
    }

    return [this.group];
  }
}
