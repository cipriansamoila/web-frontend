import { tracked } from '@glimmer/tracking';

import PositionGroup from '../groups/position';

export default class PositionAttributes {
  intl;
  positionService;
  @tracked group;

  constructor(intl, positionService) {
    this.intl = intl;
    this.positionService = positionService;
  }

  get feature() {
    return this.group.feature;
  }

  set feature(value) {
    this.group = new PositionGroup(this.intl, this.positionService);
    this.group.feature = value;
  }

  get groups() {
    if (!this.group) {
      return [];
    }

    return [this.group];
  }
}
