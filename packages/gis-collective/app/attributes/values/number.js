import TextValue from './text';

export default class NumberValue extends TextValue {
  constructor(name, key) {
    super(name, key, 'number');
  }
}
