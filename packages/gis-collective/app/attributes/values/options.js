import TextValue from './text';

export default class OptionsText extends TextValue {
  constructor(name, key, options, value) {
    super(name, key, 'options', value);
    this.options = options;
  }
}
