import { tracked } from '@glimmer/tracking';

export default class TextValue {
  @tracked name;
  @tracked key;
  @tracked help;
  @tracked value;
  @tracked format;
  @tracked isLoading;

  constructor(name, key, format = 'short text', value = '') {
    this.name = name;
    this.key = key;
    this.format = format;
    this.value = value;
  }
}
