import TextValue from './text';

export default class OptionsOther extends TextValue {
  constructor(name, key, options, value) {
    super(name, key, 'options with other', value);
    this.options = options;
  }
}
