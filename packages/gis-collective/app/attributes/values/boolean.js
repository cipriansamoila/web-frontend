import TextValue from './text';

export default class ValueBoolean extends TextValue {
  constructor(name, key) {
    super(name, key, 'boolean');
  }
}
