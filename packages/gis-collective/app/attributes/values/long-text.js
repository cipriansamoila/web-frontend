import TextValue from './text';

export default class ValueLongText extends TextValue {
  constructor(name, key) {
    super(name, key, 'long text');
  }
}
