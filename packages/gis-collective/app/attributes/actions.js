export function addIconSection(model, icon) {
  const name = icon.name;
  const newObject = {};

  icon.attributes.forEach((attr) => {
    newObject[attr.name] = null;
  });

  if (icon.allowMany) {
    if (!Array.isArray(model.attributes[name])) {
      model.attributes[name] = [];
    }
    model.attributes[name].push(newObject);
  } else {
    model.attributes[name] = newObject;
  }
}

export function handleAttributeChange(model, group, index, key, value) {
  let oldValue;

  if (index === null || index === undefined) {
    if (!model.attributes[group]) {
      model.attributes[group] = {};
    }

    oldValue = model.attributes[group][key];
    model.attributes[group][key] = value;
  } else {
    if (!model.attributes[group]) {
      model.attributes[group] = [];
    }

    oldValue = model.attributes[group][index][key];
    model.attributes[group][index][key] = value;
  }

  if (oldValue != value) {
    model.notifyPropertyChange?.('attributes');
  }
}

export function handlePicturesChange(model, key, value) {
  if (key == 'pictureOptimization') {
    model.pictures.pictureOptimization = value;
  }

  if (key == 'pictures') {
    model.pictures.setObjects(value.slice());
  }
}

export function handlePositionChange(model, key, value) {
  if (key == 'position') {
    if (!value) {
      value = this.tmpCenter || this.center;
    }

    model.set('position.coordinates', value);
    return;
  }

  if (key != 'capturedUsingGPS') {
    value = parseFloat(value);
  }

  handleAttributeChange(model, 'position details', null, key, value);
}

export function handleGroupRemove(model, key, index) {
  if (index === null) {
    delete model.attributes[key];
  } else {
    model.attributes[key].removeAt(index);

    if (!model.attributes[key].length) {
      delete model.attributes[key];
    }
  }

  model?.notifyPropertyChange?.('attributes');
}
