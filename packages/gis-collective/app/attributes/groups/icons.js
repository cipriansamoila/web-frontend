import { tracked } from '@glimmer/tracking';

export default class IconGroup {
  intl;
  _list = [];
  @tracked feature;
  @tracked isLoading;

  constructor(intl) {
    this.intl = intl;
    this.emptyMessage = intl.t('there are no icons');
  }

  get name() {
    return this.intl.t('icons');
  }

  get key() {
    return 'icons';
  }

  get type() {
    return 'iconList';
  }

  get list() {
    if (!this.feature || !this.feature.icons || !this.feature.icons.length) {
      return [];
    }

    return this.feature.icons;
  }
}
