import { tracked } from '@glimmer/tracking';

export default class PictureGroup {
  intl;
  _list = [];
  @tracked feature;
  @tracked isLoading;

  constructor(intl) {
    this.intl = intl;
    this.emptyMessage = intl.t('there are no pictures');
  }

  get name() {
    return this.intl.t('pictures');
  }

  get key() {
    return 'pictures';
  }

  get type() {
    return 'pictureList';
  }

  get list() {
    if (
      !this.feature ||
      !this.feature.pictures ||
      !this.feature.pictures.length
    ) {
      return [];
    }

    return this.feature.pictures;
  }
}
