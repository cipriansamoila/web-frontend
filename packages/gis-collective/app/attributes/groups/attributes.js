import TextValue from '../values/text';
import OptionsValue from '../values/options';
import OptionsOther from '../values/options-other';
import { tracked } from '@glimmer/tracking';

export default class AttributesGroup {
  intl;
  _list = [];
  @tracked key;
  @tracked feature;
  @tracked attributes = {};
  @tracked index;
  @tracked indexed;
  @tracked order;
  @tracked icon;
  @tracked isLoading;

  constructor(intl) {
    this.intl = intl;
  }

  get canBeRemoved() {
    return true;
  }

  get name() {
    if (!this.icon) {
      return this.key;
    }

    if (!this.icon.localName) {
      return this.icon.name;
    }

    return this.icon.localName;
  }

  get key() {
    return this.key;
  }

  get type() {
    return 'list';
  }

  get list() {
    if (!this.icon) {
      return [];
    }

    return this.icon.attributes.map((a) => this.getAttribute(a));
  }

  getAttribute(iconAttribute) {
    let value;

    switch (iconAttribute.type) {
      case 'options':
        value = new OptionsValue(
          iconAttribute.displayName,
          iconAttribute.name,
          iconAttribute.options,
          this.attributes[iconAttribute.name]
        );
        break;

      case 'options with other':
        value = new OptionsOther(
          iconAttribute.displayName,
          iconAttribute.name,
          iconAttribute.options,
          this.attributes[iconAttribute.name]
        );
        break;

      default:
        value = new TextValue(
          iconAttribute.displayName,
          iconAttribute.name,
          iconAttribute.type,
          this.attributes[iconAttribute.name]
        );
    }

    if (iconAttribute.help) {
      value.help = iconAttribute.help;
    }

    if (iconAttribute.isRequired) {
      value.isRequired = iconAttribute.isRequired;
    }

    return value;
  }
}
