import { tracked } from '@glimmer/tracking';

import ValueBoolean from '../values/boolean';
import ValueNumber from '../values/number';

export default class PositionGroup {
  intl;
  positionService;
  @tracked feature;
  @tracked isLoading;
  @tracked validationMessage;

  constructor(intl, positionService) {
    this.intl = intl;
    this.positionService = positionService;

    this.capturedUsingGPS = new ValueBoolean(
      intl.t('capture using GPS'),
      'capturedUsingGPS'
    );

    this.longitude = new ValueNumber(intl.t('longitude'), 'longitude');
    this.latitude = new ValueNumber(intl.t('latitude'), 'latitude');
    this.altitude = new ValueNumber(intl.t('altitude'), 'altitude');
    this.accuracy = new ValueNumber(intl.t('accuracy'), 'accuracy');
    this.altitudeAccuracy = new ValueNumber(
      intl.t('altitude accuracy'),
      'altitudeAccuracy'
    );
  }

  get name() {
    return this.intl.t('position');
  }

  get key() {
    return 'position';
  }

  get type() {
    return 'position';
  }

  get list() {
    let details = {};
    let coordinates = [0, 0];

    if (this.feature && this.feature.attributes && ['position details']) {
      details = this.feature.attributes['position details'];
    }

    if (
      this.feature &&
      this.feature.position &&
      this.feature.position.coordinates
    ) {
      coordinates = this.feature.position.coordinates;
    }

    this.capturedUsingGPS.value = details['capturedUsingGPS'];
    this.longitude.value = coordinates[0];
    this.latitude.value = coordinates[1];
    this.altitude.value = details['altitude'];
    this.accuracy.value = details['accuracy'];
    this.altitudeAccuracy.value = details['altitudeAccuracy'];

    const attributes = [];

    if (this.positionService) {
      if (this.positionService.hasGeolocationSupport) {
        attributes.push(this.capturedUsingGPS);
      } else {
        this.validationMessage = this.positionService.errorMessage;
      }
    }

    [
      this.longitude,
      this.latitude,
      this.altitude,
      this.accuracy,
      this.altitudeAccuracy,
    ].forEach((item) => {
      attributes.push(item);
    });

    return attributes;
  }
}
