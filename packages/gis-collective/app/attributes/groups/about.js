import TextValue from '../values/text';
import { tracked } from '@glimmer/tracking';

export default class AboutGroup {
  intl;
  _list = [];
  @tracked feature;
  @tracked isLoading;

  constructor(intl) {
    this.intl = intl;
    this._list.push(new TextValue(intl.t('maps'), 'maps', 'map list'));
    this._list.push(new TextValue(intl.t('name'), 'name', 'short text'));
    this._list.push(
      new TextValue(intl.t('description'), 'description', 'long text')
    );
  }

  get name() {
    return this.intl.t('about');
  }

  get key() {
    return 'about';
  }

  get type() {
    return 'list';
  }

  get list() {
    if (this.feature) {
      this._list[0].value = this.feature.maps;
      this._list[1].value = this.feature.name;
      this._list[2].value = this.feature.description;
    }

    return this._list;
  }
}
