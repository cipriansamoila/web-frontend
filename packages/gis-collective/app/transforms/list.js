import Transform from '@ember-data/serializer/transform';
import { A } from '@ember/array';

export default Transform.extend({
  deserialize(serialized) {
    return A(serialized);
  },

  serialize(deserialized) {
    return deserialized;
  },
});
