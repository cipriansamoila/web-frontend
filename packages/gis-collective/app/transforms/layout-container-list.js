import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';
import { LayoutRow } from './layout-row-list';

export default class LayoutContainerListTransform extends Transform {
  deserialize(serialized) {
    if (!Array.isArray(serialized)) {
      return [];
    }

    return serialized.map((a) => new LayoutContainer(a));
  }

  serialize(deserialized) {
    return deserialized?.map((a) => (a.toJSON ? a.toJSON() : a)) ?? [];
  }
}

export class LayoutContainer {
  @tracked options = [];
  @tracked rows = [];

  constructor(serialized) {
    if (!serialized) {
      return;
    }

    if (Array.isArray(serialized.options)) {
      this.options = serialized.options;
    }

    if (Array.isArray(serialized.rows)) {
      this.rows = serialized.rows.map((a) => new LayoutRow(a));
    }
  }

  get cls() {
    let options = this.options.slice();

    if (options.indexOf('container-fluid') != -1) {
      options = options.filter((a) => a != 'container');
    }

    if (
      options.indexOf('container-fluid') == -1 &&
      options.indexOf('container') == -1
    ) {
      options.push('container');
    }

    return options.join(' ');
  }

  toJSON() {
    return {
      rows: this.rows.map((a) => a.toJSON()),
      options: this.options,
    };
  }
}
