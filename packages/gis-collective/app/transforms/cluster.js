import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class ClusterTransform extends Transform {
  deserialize(serialized) {
    return new Cluster(serialized);
  }

  serialize(deserialized) {
    return deserialized;
  }
}

export class Cluster {
  @tracked mode = 1;
  @tracked map = '';

  constructor(value) {
    if (typeof value?.mode != 'undefined') {
      this.mode = value.mode;
    }

    if (value?.map) {
      this.map = value.map;
    }
  }

  toJSON() {
    return {
      mode: this.mode,
      map: this.map,
    };
  }
}
