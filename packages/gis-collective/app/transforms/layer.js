import Transform from '@ember-data/serializer/transform';
import BaseMapLayer from '../lib/base-map-layer';

export default Transform.extend({
  deserialize(serialized) {
    const list = [];

    serialized.forEach((element) => {
      list.addObject(new BaseMapLayer(element));
    });

    return list;
  },

  serialize(deserialized) {
    if (deserialized && deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  },
});
