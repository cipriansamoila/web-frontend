import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class LayoutRowListTransform extends Transform {
  deserialize(serialized) {
    if (!Array.isArray(serialized)) {
      return [];
    }

    return serialized.map((a) => new LayoutRow(a));
  }

  serialize(deserialized) {
    return deserialized.map((a) => a.toJSON());
  }
}

export class LayoutRow {
  @tracked options = [];
  @tracked cols = [];

  constructor(serialized) {
    if (!serialized) {
      return;
    }

    if (Array.isArray(serialized.options)) {
      this.options = serialized.options;
    }

    if (Array.isArray(serialized.cols)) {
      this.cols = serialized.cols.map((a) => new LayoutCol(a));
    }
  }

  get cls() {
    return this.options.join(' ');
  }

  toJSON() {
    return {
      cols: this.cols.map((a) => a.toJSON()),
      options: this.options,
    };
  }
}

export class LayoutCol {
  @tracked type = '';
  @tracked data;
  @tracked isSelected;
  @tracked options = [];
  isLayoutCol = true;

  constructor(serialized) {
    if (!serialized) {
      return;
    }

    if (Array.isArray(serialized.options)) {
      this.options = serialized.options;
    }

    if (serialized.type) {
      this.type = serialized.type;
    }

    if (serialized.data) {
      this.data = serialized.data;
    }
  }

  get cls() {
    return this.options.join(' ');
  }

  toJSON() {
    return {
      type: this.type,
      data: this.data,
      options: this.options,
    };
  }
}
