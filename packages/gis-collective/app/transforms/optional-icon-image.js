import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class OptionalIconImageTransform extends Transform {
  deserialize(serialized) {
    return new OptionalIconImage(serialized);
  }

  serialize(deserialized) {
    if (deserialized?.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}

export class OptionalIconImage {
  @tracked useParent;
  @tracked value;

  constructor(serialized) {
    if (typeof serialized !== 'object') {
      return;
    }

    this.useParent = serialized?.useParent ?? false;
    this.value = serialized?.value ?? '';
  }

  toJSON() {
    return {
      useParent: this.useParent,
      value: this.value,
    };
  }
}
