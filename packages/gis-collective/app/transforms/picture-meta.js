import Transform from '@ember-data/serializer/transform';
import PictureMeta from '../lib/picture-meta';

export default class PictureMetaTransform extends Transform {
  deserialize(serialized) {
    return new PictureMeta(serialized);
  }

  serialize(deserialized) {
    if (deserialized && deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}
