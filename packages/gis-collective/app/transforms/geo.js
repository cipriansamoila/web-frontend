import Transform from '@ember-data/serializer/transform';
import GeoJson from '../lib/geoJson';

export default Transform.extend({
  deserialize(serialized) {
    return new GeoJson(serialized);
  },

  serialize(deserialized) {
    if (!deserialized || typeof deserialized != 'object') {
      return null;
    }

    if (deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  },
});
