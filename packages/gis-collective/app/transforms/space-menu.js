import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class SpaceMenuTransform extends Transform {
  deserialize(serialized) {
    return new SpaceMenu(serialized);
  }

  serialize(deserialized) {
    return deserialized.toJSON ? deserialized.toJSON() : deserialized;
  }
}

export class SpaceMenu {
  @tracked items = [];

  constructor(serialized) {
    if (!Array.isArray(serialized?.items)) {
      return;
    }

    this.items = serialized.items.map((a) => new SpaceMenuItem(a));
  }

  toJSON() {
    return {
      items: this.items.map((a) => (a.toJSON ? a.toJSON() : a)),
    };
  }
}

export class SpaceMenuItem {
  @tracked name;
  @tracked link;
  @tracked dropDown;

  constructor(serialized) {
    this.name = serialized.name;
    this.link = new MenuLink(serialized.link);
    this.dropDown = serialized.dropDown?.map((a) => new SpaceMenuChildItem(a));
  }

  toJSON() {
    const result = { name: '' };

    if (this.name) {
      result.name = this.name;
    }
    if (this.link) {
      result.link = this.link.toJSON ? this.link.toJSON() : this.link;
    }

    if (this.dropDown) {
      result.dropDown = this.dropDown?.map((a) => a.toJSON());
    }

    return result;
  }
}

export class MenuLink {
  @tracked pageId;
  @tracked url;

  constructor(serialized) {
    this.pageId = serialized?.pageId;
    this.url = serialized?.url;
  }

  toJSON() {
    const result = {};

    if (this.pageId) {
      result.pageId = this.pageId;
    }
    if (this.url) {
      result.url = this.url;
    }

    return result;
  }
}

export class SpaceMenuChildItem {
  @tracked name;
  @tracked link;

  constructor(serialized) {
    this.name = serialized?.name;
    this.link = new MenuLink(serialized?.link);
  }

  toJSON() {
    return {
      name: this.name,
      link: this.link?.toJSON ? this.link?.toJSON() : this.link,
    };
  }
}
