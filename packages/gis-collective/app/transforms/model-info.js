import Transform from '@ember-data/serializer/transform';
import ModelInfo from '../lib/model-info';

export default class ModelInfoTransform extends Transform {
  deserialize(serialized) {
    return new ModelInfo(serialized);
  }

  serialize(deserialized) {
    if (deserialized && deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}
