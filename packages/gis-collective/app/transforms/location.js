import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class LocationTransform extends Transform {
  deserialize(serialized) {
    return new Location(serialized);
  }

  serialize(deserialized) {
    return deserialized?.toJSON?.();
  }
}

export class Location {
  @tracked isDetailed = false;
  @tracked _simple = '';

  @tracked _detailedLocation;

  constructor(value) {
    this._detailedLocation = new DetailedLocation(value?.detailedLocation);

    if (typeof value == 'string') {
      this.simple = value;
      return;
    }

    this.isDetailed = value?.isDetailed ?? true;
    this._simple = value?.simple ?? '';
  }

  get simple() {
    if (this.isDetailed) {
      return this.detailedLocation.niceString();
    }

    return this._simple;
  }

  set simple(val) {
    this._simple = val;
    this.isDetailed = false;
  }

  get detailedLocation() {
    return this._detailedLocation;
  }

  set detailedLocation(value) {
    this._detailedLocation = new DetailedLocation(value);
    this.isDetailed = true;
  }

  toJSON() {
    return {
      isDetailed: this.isDetailed,
      simple: this.simple,
      detailedLocation: this.detailedLocation.toJSON(),
    };
  }
}

export class DetailedLocation {
  @tracked country = '';
  @tracked province = '';
  @tracked city = '';
  @tracked postalCode = '';

  constructor(value) {
    this.country = value?.country ?? '';
    this.province = value?.province ?? '';
    this.city = value?.city ?? '';
    this.postalCode = value?.postalCode ?? '';
  }

  niceString() {
    return [this.country, this.province, this.city, this.postalCode]
      .filter((a) => typeof a == 'string' && a.trim() != '')
      .join(', ');
  }

  toJSON() {
    return {
      country: this.country,
      province: this.province,
      city: this.city,
      postalCode: this.postalCode,
    };
  }
}
