import Transform from '@ember-data/serializer/transform';
import OptionalMap from '../lib/optional-map';
import { getOwner } from '@ember/application';

export default class MaskTransform extends Transform {
  deserialize(serialized) {
    let applicationInstance = getOwner(this);
    const store = applicationInstance.lookup('service:store');

    return new OptionalMap(serialized, store);
  }

  serialize(deserialized) {
    if (deserialized && deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}
