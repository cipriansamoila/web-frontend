import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class PageContainerListTransform extends Transform {
  deserialize(serialized) {
    if (!Array.isArray(serialized)) {
      return [];
    }

    return serialized.map((a) => new PageContainer(a));
  }

  serialize(deserialized) {
    if (!Array.isArray(deserialized)) {
      return [];
    }

    return deserialized.map((a) => (a.toJSON ? a.toJSON() : a));
  }
}

export class PageContainer {
  @tracked backgroundColor;

  constructor(serialized) {
    this.fromJson(serialized);
  }

  fromJson(serialized) {
    if (serialized?.backgroundColor) {
      this.backgroundColor = serialized.backgroundColor;
    }
  }

  get cls() {
    let clsList = [];

    if (this.backgroundColor) {
      clsList.push(`bg-${this.backgroundColor}`);
    }

    return clsList.join(' ');
  }

  toJSON() {
    return {
      backgroundColor: this.backgroundColor,
    };
  }
}
