import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';

export default class MapFileOptionsTransform extends Transform {
  deserialize(serialized) {
    return new MapFileOptions(serialized);
  }

  serialize(deserialized) {
    if (deserialized && deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}

export class MapFileOptions {
  @tracked fields = [];
  @tracked destinationMap;
  @tracked updateBy;
  @tracked uuid;
  @tracked crs;
  @tracked extraIcons = [];

  constructor(serialized) {
    if (serialized.destinationMap) {
      this.destinationMap = serialized.destinationMap;
    }

    if (serialized.updateBy) {
      this.updateBy = serialized.updateBy;
    }

    if (serialized.uuid) {
      this.uuid = serialized.uuid;
    }

    if (serialized.crs) {
      this.crs = serialized.crs;
    }

    if (Array.isArray(serialized.fields)) {
      this.fields.addObjects(serialized.fields);
    }

    if (Array.isArray(serialized.extraIcons)) {
      this.extraIcons = serialized.extraIcons;
    }
  }

  toJSON() {
    const result = {
      uuid: this.uuid ?? '',
    };

    if (this.destinationMap) {
      result.destinationMap = this.destinationMap;
    }

    if (this.updateBy) {
      result.updateBy = this.updateBy;
    }

    if (this.fields) {
      result.fields = this.fields;
    }

    if (this.crs) {
      result.crs = this.crs;
    }

    if (this.extraIcons) {
      result.extraIcons = this.extraIcons;
    }

    return result;
  }
}
