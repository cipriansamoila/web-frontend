import Transform from '@ember-data/serializer/transform';
import AttributeDefinition from '../lib/attribute-definition';

export default class IconAttributeDefinitionListTransform extends Transform {
  deserialize(serialized) {
    if (!Array.isArray(serialized)) {
      return [];
    }

    return serialized.map((a) => new AttributeDefinition(a));
  }

  serialize(deserialized) {
    if (!Array.isArray(deserialized)) {
      return [];
    }

    return deserialized.map((a) =>
      a.toJSON ? a.toJSON() : JSON.parse(JSON.stringify(a))
    );
  }
}
