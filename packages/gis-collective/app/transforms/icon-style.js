import Transform from '@ember-data/serializer/transform';
import IconStyle from '../lib/icon-style';

export default class IconStyleTransform extends Transform {
  deserialize(serialized) {
    return new IconStyle(serialized);
  }

  serialize(deserialized) {
    if (deserialized && deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}
