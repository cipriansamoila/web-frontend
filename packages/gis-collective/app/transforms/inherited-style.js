import Transform from '@ember-data/serializer/transform';
import InheritedStyle from '../lib/inherited-style';

export default class InheritedStyleTransform extends Transform {
  deserialize(serialized) {
    return new InheritedStyle(serialized);
  }

  serialize(deserialized) {
    if (deserialized && deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}
