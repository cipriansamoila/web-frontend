import Transform from '@ember-data/serializer/transform';
import { tracked } from '@glimmer/tracking';
import { inject as service } from '@ember/service';
import ExtendedInfinityModel from '../routes/base/infinity-model';
import { later } from '@ember/runloop';

export default class PageColListTransform extends Transform {
  @service store;
  @service infinity;
  @service fastboot;

  deserialize(serialized) {
    if (!Array.isArray(serialized)) {
      return [];
    }

    return serialized.map(
      (a) => new PageCol(a, this.store, this.fastboot, this.infinity)
    );
  }

  serialize(deserialized) {
    if (!Array.isArray(deserialized)) {
      return [];
    }

    return deserialized.map((a) => (a.toJSON ? a.toJSON() : a));
  }
}

export class PageCol {
  @tracked container = 0;
  @tracked col = 0;
  @tracked row = 0;
  @tracked cls = '';
  @tracked type;
  @tracked data = {};
  @tracked record;
  @tracked buckets;

  constructor(serialized, store, fastboot, infinity) {
    this.store = store;
    this.fastboot = fastboot;
    this.infinity = infinity;

    this.fromJson(serialized);
  }

  fromJson(serialized) {
    this.record = undefined;
    this.type = serialized.type;

    if (serialized?.container) {
      this.container = parseInt(serialized.container);
    } else {
      this.container = 0;
    }

    if (serialized?.row) {
      this.row = parseInt(serialized.row);
    } else {
      this.row = 0;
    }

    if (serialized?.col) {
      this.col = parseInt(serialized.col);
    } else {
      this.col = 0;
    }

    if (serialized?.cls) {
      this.cls = serialized.cls;
    }

    if (serialized?.data) {
      this.data = JSON.parse(JSON.stringify(serialized.data));
    } else {
      this.data = {};
    }
  }

  get modelKey() {
    if (!this.data) {
      return null;
    }

    if (this.data.id) {
      return `${this.data.model}_${this.data.id}`;
    }

    const keys = Object.keys(this.query)
      .sort()
      .map((a) => `${a}_${this.query[a]}`);

    let postfix = 'all';

    if (keys.length > 0) {
      postfix = keys.join('_');
    }

    return `${this.data.model}_${postfix}`;
  }

  async fetchRecord() {
    try {
      this.record = this.store?.peekRecord(this.data.model, this.data.id);

      if (!this.record) {
        this.record = await this.store?.findRecord(
          this.data.model,
          this.data.id
        );

        await this.record.loadRelations?.();
      }
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
      this.record = null;
    }

    return this.record;
  }

  get query() {
    const result = JSON.parse(JSON.stringify(this.data?.query ?? {}));

    if (Array.isArray(result.ids)) {
      result.ids = result.ids.join(',');
    }

    return result;
  }

  async fetchList() {
    const query = JSON.parse(JSON.stringify(this.data.query ?? {}));
    if (Array.isArray(query.ids)) {
      query.ids = query.ids.join(',');
    }

    try {
      if (this.buckets && this.currentQuery == JSON.stringify(query)) {
        return;
      }

      this.currentQuery = JSON.stringify(query);

      this.buckets = await this.infinity.model(
        this.data.model,
        query,
        ExtendedInfinityModel.extend()
      );

      if (this.fastboot.isFastBoot) {
        this.record
          .filter((a) => a.loadRelations)
          .forEach((record) => {
            this.fastboot.deferRendering(record.loadRelations());
          });
      }
    } catch (err) {
      if (!this.buckets?.length) {
        this.buckets = null;
      }
    }

    later(() => {
      this.data = {
        ...this.data,
      };
    });

    return this.buckets;
  }

  async fetch() {
    if (!this.data?.model) {
      return;
    }

    if (this.data.id) {
      return this.fetchRecord();
    }

    return this.fetchList();
  }

  toJSON() {
    return {
      container: this.container || 0,
      col: this.col || 0,
      row: this.row || 0,
      type: this.type,
      data: JSON.parse(JSON.stringify(this.data ?? {})),
    };
  }
}
