import Transform from '@ember-data/serializer/transform';
import ModelListWithDefault from '../lib/model-list-with-default';
import { getOwner } from '@ember/application';

export default class ModelListWithDefaultTransform extends Transform {
  deserialize(serialized, options) {
    const store = getOwner(this).lookup('service:store');

    return new ModelListWithDefault(serialized, store, options.model);
  }

  serialize(deserialized) {
    if (deserialized && deserialized.toJSON) {
      return deserialized.toJSON();
    }

    return deserialized;
  }
}
