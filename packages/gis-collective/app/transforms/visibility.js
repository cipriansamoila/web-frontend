import Transform from '@ember-data/serializer/transform';
import { getOwner } from '@ember/application';
import { tracked } from '@glimmer/tracking';
import { later } from '@ember/runloop';

class Visibility {
  @tracked isPublic;
  @tracked isDefault;
  @tracked _team;
  @tracked teamId = '';

  constructor(store, isPublic, isDefault, team) {
    this.store = store;
    this.isPublic = isPublic;
    this.isDefault = isDefault;
    this.teamId = team;
  }

  fetchTeam() {
    if (!this.teamId || this.fetchError) {
      return Promise.resolve(null);
    }

    return new Promise((resolve) => {
      let result = this.store.peekRecord('team', this.teamId);

      if (result?.id) {
        later(() => {
          this._team = result;
          this.fetchError = false;
          return resolve(result);
        });
      }

      this.store
        .findRecord('team', this.teamId)
        .then((result) => {
          this._team = result;
          this.fetchError = false;
          resolve(result);
        })
        .catch(() => {
          this._team = null;
          this.fetchError = true;
          resolve(null);
        });
    });
  }

  get team() {
    if (!this._team) {
      this.fetchTeam();
    }

    return this._team;
  }

  set team(value) {
    this._team = value;

    if (value?.id) {
      this.teamId = value.id;
    }
  }
}

export default Transform.extend({
  deserialize(serialized) {
    let applicationInstance = getOwner(this);
    const store = applicationInstance.lookup('service:store');

    return new Visibility(
      store,
      serialized.isPublic || false,
      serialized.isDefault || false,
      serialized.team
    );
  },

  serialize(deserialized) {
    if (!deserialized) {
      return;
    }

    let team = deserialized.teamId;

    if (!team && deserialized.team?.id) {
      team = deserialized.team?.id;
    }

    if (!team && typeof deserialized.team == 'string') {
      team = deserialized.team;
    }

    return {
      isPublic: deserialized.isPublic || false,
      isDefault: deserialized.isDefault || false,
      team,
    };
  },
});
