import Model, { attr, belongsTo } from '@ember-data/model';
import { computed } from '@ember/object';

export default Model.extend({
  feature: belongsTo('feature'),

  title: attr('string'),
  description: attr('string'),
  file: attr('string'),
  type: attr('string'),
  status: attr('string'),
  creationDate: attr('date'),
  author: attr('string', { defaultValue: '@anonymous' }),

  authorObject: computed('author', 'store', function () {
    const id = this.author;

    if (id.indexOf('@') != -1) {
      return {
        name: id,
      };
    }

    return this.store.findRecord('user', id);
  }),

  resolve() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.resolve(this);
  },

  reject() {
    const adapter = this.store.adapterFor(this.constructor.modelName);
    return adapter.reject(this);
  },
});
