import Model, { belongsTo, attr } from '@ember-data/model';
import { toContentBlocks, firstParagraph } from '../lib/content-blocks';
import { DetailedAttributeCategory } from '../lib/attribute-category';

export default class IconModel extends Model {
  @attr('string') name;
  @attr('string') localName;
  @attr description;
  @attr('optional-icon-image') image;
  @attr('string') parent;
  @attr('inherited-style') styles;

  @belongsTo('iconSet') iconSet;
  @attr('string') category;
  @attr('string') subcategory;

  @attr('boolean', { defaultValue: false }) allowMany;
  @attr('icon-attribute-definition-list') attributes;

  @attr('boolean') canEdit;
  @attr otherNames;

  async fillMetaInfo(metaInfo) {
    metaInfo.title = this.localName;
    metaInfo.description = this.firstParagraph;
    metaInfo.imgSrc = this.image.value;
    if (metaInfo.imgSrc) {
      metaInfo.imgSrc += '/lg';
    }

    return metaInfo;
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  get imageStyles() {
    return this.styles.types.imageStyles(this.image.value);
  }

  get contentBlocks() {
    return toContentBlocks(this.description, this.localName);
  }

  get asCategory() {
    const category = new DetailedAttributeCategory();

    const otherNames = Array.isArray(this.otherNames) ? this.otherNames : [];

    category.icon = this;
    category.names = [this.name, ...otherNames];
    category.displayName = this.displayName || this.name;

    return category;
  }
}
