import Model, { attr, belongsTo } from '@ember-data/model';

export default class UserProfileModel extends Model {
  @attr('date') joinedTime;

  @belongsTo('picture') picture;

  @attr('string') statusEmoji;
  @attr('string') statusMessage;

  @attr('string') userName;

  @attr('string') salutation;
  @attr('string') title;
  @attr('string') firstName;
  @attr('string') lastName;

  @attr('string') skype;
  @attr('string') linkedin;
  @attr('string') twitter;
  @attr('string') website;
  @attr('location') location;
  @attr('string') jobTitle;
  @attr('string') organization;
  @attr('string') bio;

  @attr('boolean') showCalendarContributions;
  @attr('boolean') showPrivateContributions;
  @attr('boolean') showWelcomePresentation;

  @attr('boolean') canEdit;

  get contributions() {
    return this.store
      .adapterFor(this.constructor.modelName)
      .contributions(this.id);
  }

  get fullName() {
    if (!this.firstName && !this.lastName) {
      return '---';
    }

    return `${this.title ?? ''} ${this.firstName ?? ''} ${
      this.lastName ?? ''
    }`.trim();
  }
}
