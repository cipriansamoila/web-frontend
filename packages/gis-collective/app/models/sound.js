import Model, { attr } from '@ember-data/model';
import { tracked } from '@glimmer/tracking';

export default class SoundModel extends Model {
  @attr('string') name;
  @attr('string') sound;
  @attr('visibility') visibility;
  @attr('model-info') info;
  @attr('boolean') canEdit;
  @attr('string') feature;

  @tracked progress;
}
