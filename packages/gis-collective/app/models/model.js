import Model, { attr } from '@ember-data/model';

export default class ModelModel extends Model {
  @attr('string') name;
  @attr itemCount;
}
