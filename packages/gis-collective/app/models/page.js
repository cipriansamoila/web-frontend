import Model, { attr, belongsTo } from '@ember-data/model';

export default class PageModel extends Model {
  @attr('model-info') info;
  @attr('string') name;
  @attr('string') slug;

  @belongsTo('layout') layout;
  @attr('page-col-list') cols;
  @attr('page-container-list') containers;
  @attr('visibility') visibility;
  @attr('boolean') canEdit;

  async clone() {
    const jsonPage = this.toJSON();
    const jsonLayout = (await this.layout).toJSON();

    jsonLayout.name = `${jsonLayout.name} - copy`;

    const layout = this.store.createRecord('layout', jsonLayout);
    await layout.save();

    const page = this.store.createRecord('page', jsonPage);
    page.layout = layout;
    page.name = `${jsonPage.name} - copy`;
    page.slug = `${jsonPage.slug}-copy`;
    page.visibility.isPublic = false;

    return page.save();
  }
}
