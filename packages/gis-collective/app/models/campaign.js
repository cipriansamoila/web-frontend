/* eslint-disable ember/no-get */
import Model, { hasMany, attr, belongsTo } from '@ember-data/model';
import { toContentBlocks, firstParagraph } from '../lib/content-blocks';
import { get } from '@ember/object';

export default class CampaignModel extends Model {
  @attr('string') name;
  @attr article;
  @attr('visibility') visibility;
  @attr('model-info') info;
  @belongsTo('picture') cover;
  @attr('optional-map') map;
  @hasMany('icon') icons;
  @hasMany('icon') optionalIcons;
  @attr options;

  @attr('date', {
    defaultValue() {
      let value = new Date();
      value.setFullYear(0);
      return value;
    },
  })
  startDate;

  @attr('date', {
    defaultValue() {
      let value = new Date();
      value.setFullYear(0);
      return value;
    },
  })
  endDate;

  @attr('boolean') canEdit;

  async loadRelations() {
    try {
      await this.get('cover');
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err);
    }

    try {
      await this.visibility.fetchTeam();
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err);
    }
  }

  get contentBlocks() {
    return toContentBlocks(this.article, this.name);
  }

  get articleContentBlocks() {
    return toContentBlocks(this.article);
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  async fillMetaInfo(metaInfo) {
    const cover = await get(this, 'cover');

    metaInfo.title = this.name;
    metaInfo.description = this.firstParagraph;
    metaInfo.imgSrc = cover?.picture;
    if (metaInfo.imgSrc) {
      metaInfo.imgSrc += '/lg';
    }

    metaInfo.date = this.info?.lastChangeOn?.toISOString?.();

    return metaInfo;
  }
}
