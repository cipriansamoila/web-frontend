import Model, { attr } from '@ember-data/model';
import { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';

export default class UserModel extends Model {
  @service store;

  @attr('string') lastActivity;
  @attr('string') email;
  @attr('string') username;
  @attr('string') name;
  @attr('boolean') isAdmin;
  @attr('boolean') isActive;

  @tracked tokens = [];

  toPOJO() {
    return {
      id: this.id,
      lastActivity: this.lastActivity,
      email: this.email,
      username: this.username,
      name: this.name,
      isAdmin: this.isAdmin,
      isActive: this.isActive,
    };
  }

  get dateLastActivity() {
    if (!this.lastActivity) {
      return null;
    }

    return new Date(this.lastActivity * 1000);
  }

  updateTokens() {
    return this.store
      .adapterFor(this.constructor.modelName)
      .getApiTokens(this)
      .then((result) => {
        const tokens = result['tokens'].map((a) => {
          if (a['expire']) {
            a['expire'] = new Date(Date.parse(a['expire']));
          }

          if (a['created']) {
            a['created'] = new Date(Date.parse(a['created']));
          }

          if (a['scopes']) {
            a['scopes'] = a['scopes'].join(', ');
          }

          return a;
        });

        this.tokens.setObjects(tokens);
      });
  }

  revokeApiTokenByName() {
    return this.store
      .adapterFor(this.constructor.modelName)
      .revokeApiTokenByName(this, ...arguments);
  }

  changePassword() {
    return this.store
      .adapterFor(this.constructor.modelName)
      .changePassword(this, ...arguments);
  }

  removeUser() {
    return this.store
      .adapterFor(this.constructor.modelName)
      .removeUser(this, ...arguments);
  }

  promote() {
    return this.store
      .adapterFor(this.constructor.modelName)
      .promote(this, ...arguments);
  }

  downgrade() {
    return this.store
      .adapterFor(this.constructor.modelName)
      .downgrade(this, ...arguments);
  }

  createToken() {
    return this.store
      .adapterFor(this.constructor.modelName)
      .createToken(this, ...arguments);
  }

  activate() {
    return this.store
      .adapterFor(this.constructor.modelName)
      .activate(this, ...arguments);
  }

  testNotification() {
    return this.store
      .adapterFor(this.constructor.modelName)
      .testNotification(this, ...arguments);
  }
}
