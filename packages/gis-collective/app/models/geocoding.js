import Model, { hasMany, attr } from '@ember-data/model';

export default class GeocodingModel extends Model {
  @attr('string') name;
  @attr('number') score;
  @attr('geo') geometry;
  @hasMany('icon') icons;
  @attr license;
}
