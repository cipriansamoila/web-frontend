import Model, { attr } from '@ember-data/model';
import { toContentBlocks, firstParagraph } from '../lib/content-blocks';

export default class ArticleModel extends Model {
  @attr('string') title;
  @attr content;
  @attr('string') slug;

  @attr('model-info') info;

  @attr('boolean') canEdit;
  @attr('visibility') visibility;

  get contentBlocks() {
    return toContentBlocks(this.content, this.title);
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  async fillMetaInfo(metaInfo) {
    metaInfo.title = this.title;
    metaInfo.description = this.firstParagraph;
    metaInfo.date = this.info?.lastChangeOn?.toISOString?.();

    return metaInfo;
  }
}
