import Model, { hasMany, belongsTo, attr } from '@ember-data/model';

export default class CampaignAnswerModel extends Model {
  @belongsTo('campaign') campaign;
  @hasMany('picture') pictures;
  @attr('geo') position;
  @attr('model-info') info;
  @hasMany('icon') icons;

  @attr attributes;
}
