import Model, { attr } from '@ember-data/model';

export default class PreferenceModel extends Model {
  @attr('string') name;
  @attr value;
}
