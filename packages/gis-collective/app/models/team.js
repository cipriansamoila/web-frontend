import Model, { attr, hasMany, belongsTo } from '@ember-data/model';
import { toContentBlocks, firstParagraph } from '../lib/content-blocks';

export default class TeamModel extends Model {
  @attr('string') name;
  @attr about;

  @attr('boolean') isPublic;

  @hasMany('user') owners;
  @hasMany('user') leaders;
  @hasMany('user') members;
  @hasMany('user') guests;

  @belongsTo('picture') logo;
  @hasMany('picture') pictures;

  @attr('boolean') canEdit;
  @attr('boolean') isPublisher;

  get hasLogo() {
    if (!this.logo) {
      return false;
    }

    const name = this.logo.get('name');

    return name != 'default';
  }

  get contentBlocks() {
    return toContentBlocks(this.about, this.name);
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  async fillMetaInfo(metaInfo) {
    metaInfo.title = this.name;
    metaInfo.description = this.firstParagraph;

    if (this.hasLogo) {
      metaInfo.imgSrc = this.logo?.get('picture');
    }
    if (metaInfo.imgSrc) {
      metaInfo.imgSrc += '/lg';
    }

    return metaInfo;
  }
}
