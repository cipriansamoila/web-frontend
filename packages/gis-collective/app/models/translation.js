import Model, { attr } from '@ember-data/model';

export default class TranslationModel extends Model {
  @attr('string') name;
  @attr('string') locale;
  @attr('string') file;
  @attr customTranslations;

  @attr('boolean') canEdit;
}
