import Model, { attr } from '@ember-data/model';
import PictureMeta from '../lib/picture-meta';
import { htmlSafe } from '@ember/template';
import { tracked } from '@glimmer/tracking';
import { generateBlur } from '../lib/blur';

export default class PictureModel extends Model {
  @attr('string') name;
  @attr('string') picture;
  @attr('boolean') canEdit;
  @attr('string') owner;
  @attr('string') hash;
  @attr('picture-meta', {
    defaultValue() {
      return new PictureMeta();
    },
  })
  meta;

  @tracked progress;

  get hashBg() {
    return generateBlur(this.hash, 4, 4);
  }

  get canRotate() {
    return this.owner != '@system';
  }

  get styleLg() {
    return htmlSafe(`background-image: url('${this.picture}/lg')`);
  }

  get is360() {
    return this.meta.renderMode == '360';
  }

  set is360(value) {
    this.meta.renderMode = value ? '360' : '';
  }

  async rotate() {
    if (this.isNew) {
      return this.localRotate();
    }

    const adapter = this.store.adapterFor(this.constructor.modelName);

    await adapter.rotate(this);
  }

  localResize(maxWidth, maxHeight) {
    if (!this.isNew) {
      return;
    }

    if (this.picture.length < 1024 * 10) {
      return new Promise((resolve) => {
        resolve();
      });
    }

    let img = document.createElement('img');
    img.src = this.picture;
    img.crossOrigin = 'Anonymous';

    return new Promise((resolve, reject) => {
      img.onload = () => {
        try {
          const w = img.naturalWidth;
          const h = img.naturalHeight;

          if (maxWidth >= img.width && maxHeight >= img.height) {
            return resolve();
          }

          if (w == 0 || h == 0) {
            return reject();
          }

          var ratio = 1;
          if (img.width > maxWidth) {
            ratio = maxWidth / img.width;
          } else if (img.height > maxHeight) {
            ratio = maxHeight / img.height;
          }

          if (ratio == 1) {
            return resolve();
          }

          var canvas = document.createElement('canvas');
          canvas.width = w * ratio;
          canvas.height = h * ratio;
          var ctx = canvas.getContext('2d');
          ctx.drawImage(img, 0, 0, w, h, 0, 0, w * ratio, h * ratio);

          var data;
          try {
            data = canvas.toDataURL('image/jpeg', 0.7);
          } catch (err) {
            return reject();
          }

          this.set('picture', data);
          resolve();
        } catch (err) {
          reject(err);
        }
      };
    });
  }

  localRotate() {
    var angle = -90;

    var img = document.createElement('img');
    img.src = this.picture;

    return new Promise((resolve, reject) => {
      img.onload = () => {
        try {
          var h = img.naturalHeight;
          var w = img.naturalWidth;

          if (w == 0 || h == 0) {
            return reject();
          }

          var max = Math.max(w, h);

          //rotate image
          var canvas = document.createElement('canvas');
          canvas.width = max;
          canvas.height = max;
          var ctx = canvas.getContext('2d');

          ctx.translate(0, w);
          ctx.rotate(angle * (Math.PI / 180));
          ctx.drawImage(img, 0, 0);

          //crop image
          var canvas2 = document.createElement('canvas');
          canvas2.width = h;
          canvas2.height = w;
          var ctx2 = canvas2.getContext('2d');
          ctx2.drawImage(canvas, 0, 0);

          const data = canvas2.toDataURL('image/jpeg', 0.7);

          this.set('picture', data.slice());
          resolve();
        } catch (err) {
          reject(err);
        }
      };
    });
  }
}
