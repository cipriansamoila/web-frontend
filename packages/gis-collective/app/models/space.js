import Model, { attr } from '@ember-data/model';

export default class Space extends Model {
  @attr('string') name;
  @attr('space-menu') menu;

  @attr('model-info') info;
  @attr('visibility') visibility;
}
