import Model, { attr, belongsTo } from '@ember-data/model';
import Polygon from 'ol/geom/Polygon';
import { toContentBlocks, firstParagraph } from '../lib/content-blocks';
import { generateBlur, idToHash } from '../lib/blur';

export default class MapModel extends Model {
  @attr('string') name;
  @attr description;
  @attr('string') tagLine;
  @attr('geo') area;
  @belongsTo('picture') cover;
  @belongsTo('picture') squareCover;
  @attr('boolean') canEdit;
  @attr('boolean') hideOnMainMap;
  @attr('boolean') showPublicDownloadLinks;
  @attr('boolean') isIndex;
  @attr('boolean') addFeaturesAsPending;
  @attr license;
  @attr('optional-map') mask;
  @attr('visibility') visibility;
  @attr('model-info') info;
  @attr('cluster') cluster;
  @attr('model-list-with-default', { model: 'icon-set' }) iconSets;
  @attr('model-list-with-default', { model: 'base-map' }) baseMaps;
  @attr() meta;

  @attr('date', {
    defaultValue() {
      let value = new Date();
      value.setFullYear(0);
      return value;
    },
  })
  startDate;

  @attr('date', {
    defaultValue() {
      let value = new Date();
      value.setFullYear(0);
      return value;
    },
  })
  endDate;

  reloadMeta() {
    return this.store
      .adapterFor(this.constructor.modelName)
      .getMeta(this.id)
      .then((meta) => {
        this.set('meta', meta);
        return meta;
      });
  }

  async loadRelations() {
    const promises = [];

    promises.push(this.baseMaps.fetch());
    promises.push(this.iconSets.fetch());

    await Promise.all(promises);
  }

  get hashBg() {
    return generateBlur(idToHash(this.id), 4, 4);
  }

  get viewbox() {
    if (!this.area) {
      return '';
    }

    const geometry = new Polygon(this.area.coordinates.slice());
    return geometry.getExtent().join(',');
  }

  get hasCover() {
    if (!this.cover && !this.squareCover) {
      return false;
    }

    const coverName = this.cover ? this.cover.get('name') : 'default';
    const squareCoverName = this.squareCover
      ? this.squareCover.get('name')
      : 'default';

    const coverPicture = this.cover ? this.cover.get('picture') : null;
    const squareCoverPicture = this.squareCover
      ? this.squareCover.get('picture')
      : null;

    return (
      (coverPicture && coverName != 'default') ||
      (squareCoverPicture && squareCoverName != 'default')
    );
  }

  get coverPicture() {
    if (!this.hasCover) {
      return '';
    }

    const coverName = this.cover ? this.cover.get('name') : 'default';
    if (coverName != 'default' && this.cover.get('picture')) {
      return this.cover.get('picture');
    }

    const squareCoverName = this.squareCover
      ? this.squareCover.get('name')
      : 'default';
    if (squareCoverName != 'default' && this.squareCover.get('picture')) {
      return this.squareCover.get('picture');
    }

    return '';
  }

  get squareCoverPicture() {
    if (!this.hasCover) {
      return '';
    }
    const squareCoverName = this.squareCover
      ? this.squareCover.get('name')
      : 'default';
    if (squareCoverName != 'default' && this.squareCover.get('picture')) {
      return this.squareCover.get('picture');
    }

    const coverName = this.cover ? this.cover.get('name') : 'default';
    if (coverName != 'default' && this.cover.get('picture')) {
      return this.cover.get('picture');
    }

    return '';
  }

  get hasStartDate() {
    return this.startDate?.getFullYear() > 1;
  }

  get hasEndDate() {
    return this.endDate?.getFullYear() > 1;
  }

  get contentBlocks() {
    return toContentBlocks(this.description, this.name);
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  async fillMetaInfo(metaInfo) {
    const cover = await this.cover;
    const squareCover = await this.squareCover;

    metaInfo.title = this.name;
    metaInfo.description = this.firstParagraph;
    metaInfo.author = this.visibility?.team?.name;
    metaInfo.imgSrc = squareCover?.picture || cover?.picture;
    if (metaInfo.imgSrc) {
      metaInfo.imgSrc += '/lg';
    }

    metaInfo.date = this.info?.lastChangeOn?.toISOString?.();

    return metaInfo;
  }
}
