/* eslint-disable ember/no-get */
import Model, { hasMany, attr } from '@ember-data/model';
import { all } from 'rsvp';
import { toContentBlocks, firstParagraph } from '../lib/content-blocks';
import { DetailedAttributeCategory } from '../lib/attribute-category';
import { generateBlur, idToHash } from '../lib/blur';
import { get, set } from '@ember/object';

export default class FeatureModel extends Model {
  @attr('string') name;
  @attr description;
  @attr('geo') position;
  @attr('number') issueCount;
  @attr('number') visibility;
  @attr('boolean') isMasked;
  @attr('boolean') canEdit;
  @attr('model-info') info;
  @attr attributes;

  @hasMany('picture') pictures;
  @hasMany('sound') sounds;
  @hasMany('icon') icons;
  @hasMany('map') maps;
  @hasMany('users') contributors;

  get pictureCount() {
    return this.hasMany('pictures').ids().length;
  }

  get iconCount() {
    return this.hasMany('icons').ids().length;
  }

  get soundCount() {
    return this.hasMany('sounds').ids().length;
  }

  get hashBg() {
    return generateBlur(idToHash(this.id), 4, 4);
  }

  get isPublished() {
    return this.visibility == 1;
  }

  get isPrivate() {
    return this.visibility == 0;
  }

  get isPending() {
    return this.visibility == -1;
  }

  get niceCreatedOn() {
    if (!this.info || !this.info.createdOn) {
      return 'unknown';
    }

    return new Date(Date.parse(this.info.createdOn)).toDateString();
  }

  get originalAuthor() {
    if (!this.info || !this.info.originalAuthor) {
      return '@unknown';
    }

    return this.info.originalAuthor;
  }

  get hasPictures() {
    return this.pictures.length > 0;
  }

  get displayIconAttributes() {
    const categories = this.icons.map((a) => a.asCategory);

    if (!this.attributes) {
      return categories;
    }

    Object.keys(this.attributes)
      .filter(
        (key) =>
          !Array.isArray(this.attributes[key]) &&
          typeof this.attributes[key] != 'object'
      )
      .forEach((key) => {
        if (!this.attributes['other']) {
          this.attributes['other'] = {};
        }

        this.attributes['other'][key] = this.attributes[key];
        delete this.attributes[key];
      });

    Object.keys(this.attributes).forEach((key) => {
      const existingCategories = categories.filter((a) => a.canFill(key));

      if (existingCategories.length == 0) {
        const newCategory = new DetailedAttributeCategory();
        newCategory.displayName = key;
        newCategory.names = [key];
        newCategory.isTable = Array.isArray(this.attributes[key]);
        newCategory.fill(this.attributes[key]);

        categories.push(newCategory);
      } else {
        existingCategories[0].fill(this.attributes[key]);
      }
    });

    return categories;
  }

  get iconAttributes() {
    if (!this.attributes || typeof this.attributes !== 'object') {
      return {};
    }

    if (!this.icons || this.icons.isFulfilled === false) {
      if (this.icons.then) {
        this.icons.then(() => {}).catch(() => {});
      }

      return {};
    }

    const validIconNames = [];
    const nameMatch = {};
    const isArray = {};

    this.icons.forEach((icon) => {
      validIconNames.push(icon.name);
      nameMatch[icon.name] = icon.name;
      isArray[icon.name] = icon.allowMany;

      if (icon && icon.otherNames && icon.otherNames.length) {
        icon.otherNames.forEach((name) => {
          nameMatch[name] = icon.name;
          validIconNames.push(name);
        });
      }
    });

    const attributes = {};

    Object.keys(this.attributes)
      .filter((key) => validIconNames.indexOf(key) != -1)
      .filter((key) => isArray[nameMatch[key]] !== true)
      .forEach((key) => {
        isArray[nameMatch[key]] = Array.isArray(this.attributes[key]);
      });

    Object.keys(this.attributes)
      .filter((key) => validIconNames.indexOf(key) != -1)
      .forEach((key) => {
        const iconName = nameMatch[key];

        if (!attributes[iconName]) {
          attributes[iconName] = isArray[iconName] ? [] : {};
        }

        if (isArray[iconName] && Array.isArray(this.attributes[key])) {
          attributes[iconName].addObjects(this.attributes[key]);
        } else if (isArray[iconName] && !Array.isArray(this.attributes[key])) {
          attributes[iconName].addObject(this.attributes[key]);
        } else {
          attributes[iconName] = Object.assign(
            {},
            attributes[iconName],
            this.attributes[key]
          );
        }
      });

    return attributes;
  }

  restore() {
    if (this._original_position) {
      this.position.coordinates.clear();
      this.position.coordinates.addObjects(this._original_position);
      this.set('_original_position', false);
    }

    this.rollbackAttributes();
  }

  get firstIcon() {
    return this.icons
      .filter((a) => typeof a.get('image') != 'undefined')
      .objectAt(0);
  }

  get firstMap() {
    if (this.maps.length == 0) {
      return null;
    }

    return this.maps.objectAt(0);
  }

  get firstSound() {
    if (this.sounds.length == 0) {
      return null;
    }

    return this.sounds.objectAt(0);
  }

  get isInvalid() {
    const coordinates = this.position.coordinates;

    if (this.maps.length == 0) {
      return true;
    }

    if (!this.name) {
      return true;
    }

    if (!this.description) {
      return true;
    }

    if (this.icons.length == 0) {
      return true;
    }

    if (coordinates[0] == 0 || coordinates[1] == 0) {
      return true;
    }

    return false;
  }

  get iconsByCategory() {
    var result = {};

    this.icons.forEach(function (icon) {
      const category = icon.get('category');
      const subcategory = icon.get('subcategory');

      if (!result[category]) {
        result[category] = {};
      }

      if (!result[category][subcategory]) {
        result[category][subcategory] = [];
      }

      result[category][subcategory].push(icon);
    });

    return result;
  }

  get iconSets() {
    return this.maps.then((maps) => {
      const allSets = maps.map((a) => a.iconSets.list);
      const merged = [].concat.apply([], allSets);
      const sets = [...new Set(merged)];

      return all(sets).then((result) => {
        return result.map((map) => map.id);
      });
    });
  }

  get contentBlocks() {
    return toContentBlocks(this.description, this.name);
  }

  get descriptionContentBlocks() {
    if (!this.contentBlocks) {
      return { blocks: [] };
    }

    const blocks = this.contentBlocks.blocks.filter(
      (a) => !(a.type == 'header' && a.data.level == 1)
    );

    return { blocks };
  }

  set descriptionContentBlocks(content) {
    this.description = toContentBlocks(content, this.name);
  }

  get firstParagraph() {
    return firstParagraph(this.contentBlocks);
  }

  async fillMetaInfo(metaInfo) {
    const cover = await get(this, 'pictures.firstObject');

    metaInfo.title = this.name;
    metaInfo.description = this.firstParagraph;
    metaInfo.imgSrc = cover?.picture;
    if (metaInfo.imgSrc) {
      metaInfo.imgSrc += '/lg';
    }

    metaInfo.date = this.info?.lastChangeOn?.toISOString?.();

    return metaInfo;
  }

  setListAttribute(icon, key, value) {
    if (!Array.isArray(value)) {
      return;
    }

    if (
      this.attributes[icon.name] &&
      !Array.isArray(this.attributes[icon.name])
    ) {
      this.attributes[icon.name] = [this.attributes[icon.name]];
    }

    if (!this.attributes[icon.name]) {
      set(this.attributes, icon.name, []);
    }

    while (this.attributes[icon.name].length < value.length) {
      this.attributes[icon.name].push({});
    }

    this.attributes[icon.name].forEach((item, index) => {
      if (index < value.length) {
        set(this.attributes[icon.name][index], key, value[index]);
      } else {
        delete this.attributes[icon.name][index][key];
      }
    });

    this.attributes[icon.name] = this.attributes[icon.name].filter(
      (a) => Object.keys(a).length > 0
    );
  }

  setAttribute(icon, key, value) {
    if (icon.allowMany) {
      return this.setListAttribute(...arguments);
    }

    if (!this.attributes[icon.name]) {
      set(this.attributes, icon.name, {});
    }

    set(this.attributes[icon.name], key, value);
  }
}
