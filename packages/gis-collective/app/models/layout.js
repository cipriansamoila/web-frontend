import Model, { attr } from '@ember-data/model';

export default class LayoutModel extends Model {
  @attr('string') name;

  @attr('model-info') info;
  @attr('layout-container-list') containers;
  @attr('boolean') canEdit;
}
