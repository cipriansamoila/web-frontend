import Model, { attr, belongsTo } from '@ember-data/model';

export default class PresentationModel extends Model {
  @attr('model-info') info;
  @attr('string') name;
  @attr('string') slug;

  @belongsTo('layout') layout;
  @attr('page-col-list') cols;
  @attr('visibility') visibility;
  @attr('boolean') canEdit;
}
