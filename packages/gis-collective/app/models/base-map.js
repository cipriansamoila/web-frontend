import Model, { attr, belongsTo } from '@ember-data/model';

export default class BaseMap extends Model {
  @attr('string') name;
  @attr attributions;
  @attr('string') icon;
  @attr('layer') layers;
  @attr('boolean') canEdit;
  @belongsTo('picture') cover;
  @attr('visibility') visibility;
  @attr('number') defaultOrder;
}
