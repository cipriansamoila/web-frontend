import Model, { attr } from '@ember-data/model';

export default class MetricModel extends Model {
  @attr('string') name;
  @attr('string') type;
  @attr('string') reporter;
  @attr('number') value;
  @attr('date') time;
  @attr labels;
}
