import Model, { attr, belongsTo } from '@ember-data/model';
import { toContentBlocks } from '../lib/content-blocks';
import { generateBlur, idToHash } from '../lib/blur';

export default class IconSetModel extends Model {
  @attr('string') name;
  @attr description;
  @attr('boolean') canEdit;
  @attr('icon-style') styles;
  @belongsTo('picture') cover;
  @attr('visibility') visibility;

  get hashBg() {
    return generateBlur(idToHash(this.id), 4, 4);
  }

  get contentBlocks() {
    return toContentBlocks(this.description, this.name);
  }

  get hasCover() {
    if (!this.cover) {
      return false;
    }

    return this.cover.get('name') != 'default';
  }

  categories() {
    return this.store
      .adapterFor(this.constructor.modelName)
      .categories(this, ...arguments);
  }

  subcategories() {
    return this.store
      .adapterFor(this.constructor.modelName)
      .subcategories(this, ...arguments);
  }
}
