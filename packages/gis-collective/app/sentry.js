import * as Sentry from '@sentry/browser';
import * as Integrations from '@sentry/integrations';

let errorCount = 0;

const ignoredErrors = ['ChunkLoadError', 'TransitionAborted', 'AbortError'];

export function startSentry(dsn, version) {
  Sentry.init({
    //autoSessionTracking: false,
    dsn,
    release: `gis-collective/web-frontend@${version}`,
    integrations: [new Integrations.Ember()],

    beforeSend(event, hint) {
      let error = hint.originalException;

      // ignore aborted route transitions from the Ember.js router
      if (error && error.name && ignoredErrors.find((a) => a === error.name)) {
        return null;
      }

      if (error && error.isAdapterError) {
        return null;
      }

      errorCount++;

      if (errorCount > 50) {
        return null;
      }

      return event;
    },
  });
}
