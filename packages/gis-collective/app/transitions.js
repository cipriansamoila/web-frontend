export default function () {
  this.transition(
    this.fromRoute('manage.maps.edit_loading'),
    this.toRoute('manage.maps.edit'),
    this.use('crossFade')
  );

  this.transition(
    this.fromRoute('manage.sites.edit_loading'),
    this.toRoute('manage.sites.edit'),
    this.use('crossFade')
  );

  this.transition(
    this.fromRoute([
      'browse.maps.map-view.feature',
      'browse.maps.map-view.feature_loading',
      'browse.maps.map-view.search',
    ]),
    this.toRoute([
      'browse.maps.map-view.feature',
      'browse.maps.map-view.feature_loading',
      'browse.maps.map-view.search',
    ]),
    this.use('crossFade', { duration: 500 }),
    this.reverse('crossFade', { duration: 500 })
  );
}
