import { helper } from '@ember/component/helper';

export default helper(function colSizeLimit(positional) {
  let options = positional[1]?.split(' ') || [];
  const def = options.filter(
    (a) => a.indexOf('-md-') == -1 && a.indexOf('-lg-') == -1
  );
  const md = options
    .filter((a) => a.indexOf('md-') != -1)
    .map((a) => a.replace('md-', ''));
  const lg = options
    .filter((a) => a.indexOf('lg-') != -1)
    .map((a) => a.replace('lg-', ''));

  if (positional[0] == 'mobile') {
    return def.join(' ');
  }

  if (positional[0] == 'tablet' && md.length == 0) {
    return def.join(' ');
  }

  if (positional[0] == 'tablet' && md.length > 0) {
    return md.join(' ');
  }

  if (positional[0] == 'desktop' && md.length == 0 && lg.length == 0) {
    return def.join(' ');
  }

  if (positional[0] == 'desktop' && lg.length == 0) {
    return md.join(' ');
  }

  if (positional[0] == 'desktop' && lg.length > 0) {
    return lg.join(' ');
  }

  return options.join(' ');
});
