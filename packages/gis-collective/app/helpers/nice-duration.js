import { helper } from '@ember/component/helper';

export function niceDuration(params /*, hash*/) {
  if (!params[0]) {
    return 'unknown';
  }

  let milliseconds;

  if (params[0].getTime) {
    milliseconds = new Date().getTime() - params[0].getTime();
  } else {
    milliseconds = params[0];
  }

  let longUnits = params[1];
  let postfix = params[2] || '';
  let u = -1;
  let units = longUnits
    ? ['second', 'minute', 'hour', 'day', 'month', 'year']
    : ['s', 'm', 'h', 'd', 'm', 'y'];
  let size = [1000, 60, 60, 24, 30, 12];

  if (milliseconds == undefined) {
    return '';
  }

  if (Math.abs(milliseconds) <= 1000) {
    return `just now`;
  }

  do {
    u++;
    milliseconds /= size[u];
  } while (u < units.length - 1 && milliseconds / size[u + 1] >= 1);

  let plural = milliseconds.toFixed(0) != 1 ? 's' : '';

  if (!longUnits) {
    plural = '';
  }

  return `${milliseconds.toFixed(0)} ${units[u]}${plural} ${postfix}`;
}

export default helper(niceDuration);
