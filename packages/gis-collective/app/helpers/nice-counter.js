import { helper } from '@ember/component/helper';
import { htmlSafe } from '@ember/template';

export function niceCounter([value, noValue, oneValue, manyValues]) {
  if (!value) {
    return noValue;
  }

  if (value == 1) {
    return htmlSafe(`<b>1</b> ${oneValue}`);
  }

  return htmlSafe(`<b>${value}</b> ${manyValues}`);
}

export default helper(niceCounter);
