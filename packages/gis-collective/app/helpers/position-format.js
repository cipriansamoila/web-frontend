import { helper } from '@ember/component/helper';

export function positionFormat([value]) {
  return value.coordinates.join(', ');
}

export default helper(positionFormat);
