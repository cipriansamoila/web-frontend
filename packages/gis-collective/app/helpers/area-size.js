import { helper } from '@ember/component/helper';
import { htmlSafe } from '@ember/template';

import Polygon from 'ol/geom/Polygon';
import { fromLonLat } from 'ol/proj';
import { getArea } from 'ol/sphere';

export function areaSize([area]) {
  if (!area) {
    return 'The area is not set';
  }

  const coordinates = area.coordinates;

  if (coordinates.length == 0 || coordinates[0].length < 3) {
    return 'The area is not set';
  }

  const coordinates2 = coordinates[0].map((a) => fromLonLat(a));
  const geometry = new Polygon([coordinates2]);

  const size = getArea(geometry);

  if (size > 10000) {
    return htmlSafe(
      Math.round((size / 1000000) * 100) / 100 + ' km<sup>2</sup>'
    );
  } else {
    return htmlSafe(Math.round(size * 100) / 100 + ' m<sup>2</sup>');
  }
}

export default helper(areaSize);
