import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default class AdminIntegrationsRoute extends Route {
  @service preferences;

  model() {
    const fields = ['nominatim', 'matomo.url', 'matomo.siteId'];
    const items = {};

    fields.forEach((key) => {
      items[key.replace('.', '')] = this.preferences.requestPreference(
        `integrations.${key}`
      );
    });

    return hash(items);
  }
}
