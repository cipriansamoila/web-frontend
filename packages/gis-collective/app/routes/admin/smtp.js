import Route from '../base/authenticated-admin-route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default class AdminSmtpRoute extends Route {
  @service preferences;

  model() {
    const fields = [
      'authType',
      'connectionType',
      'tlsValidationMode',
      'tlsVersion',
      'host',
      'port',
      'password',
      'username',
      'from',
    ];
    const items = {
      user: this.store.queryRecord('user', { me: true }),
    };

    fields.forEach((key) => {
      items[key] = this.preferences.requestPreference(`secret.smtp.${key}`);
    });

    return hash(items);
  }
}
