import Route from '@ember/routing/route';

import ExtendedInfinityModel from '../../base/infinity-model';
import { inject as service } from '@ember/service';

export default class AdminUsersIndexRoute extends Route {
  @service infinity;
  @service searchStorage;

  queryParams = {
    search: {
      refreshModel: true,
      replace: true,
    },
  };

  model(params) {
    const queryParams = {
      edit: true,
    };

    params['edit'] = true;
    if (params['search'] && params['search'] != '') {
      this.set('searchStorage.term', params['search']);
      queryParams['term'] = params['search'];
    }

    return this.infinity.model(
      'user',
      queryParams,
      ExtendedInfinityModel.extend()
    );
  }
}
