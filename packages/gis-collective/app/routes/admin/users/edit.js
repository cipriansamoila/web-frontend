import AuthenticatedAdminRoute from '../../base/authenticated-admin-route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default class AdminUsersEditRoute extends AuthenticatedAdminRoute {
  @service preferences;

  model(params) {
    return hash({
      profile: this.store.findRecord('user-profile', params.id),
      user: this.store.findRecord('user', params.id),
      detailedLocation: this.preferences.requestPreference(
        `profile.detailedLocation`
      ),
    });
  }
}
