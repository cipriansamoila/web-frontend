import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend({
  preferences: service(),

  model() {
    return hash({
      enabled: this.preferences.requestPreference('captcha.enabled'),
      recaptchaSiteKey: this.preferences.requestPreference(
        'secret.recaptcha.siteKey'
      ),
      recaptchaSecretKey: this.preferences.requestPreference(
        'secret.recaptcha.secretKey'
      ),
      mtcaptchaSiteKey: this.preferences.requestPreference(
        'secret.mtcaptcha.siteKey'
      ),
      mtcaptchaPrivateKey: this.preferences.requestPreference(
        'secret.mtcaptcha.privateKey'
      ),
    });
  },
});
