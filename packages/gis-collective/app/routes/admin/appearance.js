import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend({
  user: service(),
  preferences: service(),

  async model() {
    const appearanceName = await this.preferences.requestPreference(
      'appearance.name'
    );
    const appearanceLogo = await this.preferences.requestPreference(
      'appearance.logo'
    );
    const appearanceCover = await this.preferences.requestPreference(
      'appearance.cover'
    );
    const appearanceExtent = await this.preferences.requestPreference(
      'appearance.mapExtent'
    );
    const appearanceShowWelcomePresentation =
      await this.preferences.requestPreference(
        'appearance.showWelcomePresentation'
      );

    return hash({
      appearanceName,
      appearanceLogo,
      appearanceCover,
      appearanceExtent,
      appearanceShowWelcomePresentation,
      logoImage: this.store.findRecord('picture', appearanceLogo.value),
      coverImage: this.store.findRecord('picture', appearanceCover.value),
      baseMaps: this.store.query('base-map', { default: true }),
    });
  },

  actions: {
    willTransition() {
      return this.model().then((data) => {
        data.appearanceName.rollbackAttributes();
        data.appearanceLogo.rollbackAttributes();
        data.appearanceCover.rollbackAttributes();
      });
    },
  },
});
