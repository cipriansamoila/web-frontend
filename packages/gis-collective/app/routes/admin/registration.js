import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend({
  user: service(),
  preferences: service(),

  model() {
    return hash({
      registerEnabled: this.preferences.requestPreference('register.enabled'),
      registerEmailDomains: this.preferences.requestPreference(
        'register.emailDomains'
      ),
      registerMandatory:
        this.preferences.requestPreference('register.mandatory'),
    });
  },

  actions: {
    willTransition() {
      return this.model().then((data) => {
        data.registerEmailDomains.rollbackAttributes();
        data.registerEnabled.rollbackAttributes();
      });
    },
  },
});
