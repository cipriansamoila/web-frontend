import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default class AdminLocationServicesRoute extends Route {
  @service preferences;

  model() {
    const fields = ['maskingPrecision'];
    const items = {};

    fields.forEach((key) => {
      items[key] = this.preferences.requestPreference(
        `locationServices.${key}`
      );
    });

    return hash(items);
  }
}
