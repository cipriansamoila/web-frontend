import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default class AdminAccessRoute extends Route {
  @service preferences;

  model() {
    return hash({
      allowProposingSites: this.preferences.requestPreference(
        'access.allowProposingSites'
      ),
      allowManageWithoutTeams: this.preferences.requestPreference(
        'access.allowManageWithoutTeams'
      ),
      isMultiProjectMode: this.preferences.requestPreference(
        'access.isMultiProjectMode'
      ),
    });
  }
}
