import BrowseRoute from './browse';

export default class AuthenticatedBrowseRoute extends BrowseRoute {
  beforeModel() {
    if (!this.session.isAuthenticated) {
      return this.router.transitionTo('login.index');
    }
  }
}
