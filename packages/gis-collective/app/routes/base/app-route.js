import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class AppRoute extends Route {
  @service session;
  @service fastboot;
  @service router;
  @service preferences;
  @service user;
  @service('loading') loadingService;

  get isFastBoot() {
    return this.fastboot.isFastBoot;
  }

  get isErrorPage() {
    if (this.isFastBoot) {
      return false;
    }

    if (
      this.router.location.concreteImplementation &&
      this.router.location.concreteImplementation.location.pathname == '/error'
    ) {
      return true;
    }

    return false;
  }

  get isPrivate() {
    return this.preferences.isPrivate;
  }

  checkServiceRedirect() {
    if (this.preferences.isPrivate && !this.session.isAuthenticated) {
      this.router.transitionTo('login.index');
    }
  }

  @action
  loading(transition) {
    this.loadingService.isLoading = true;
    this.loadingService.isFirst = transition.from == null;

    transition.promise.finally(() => {
      this.loadingService.isLoading = false;
      this.loadingService.isFirst = false;
    });

    return true;
  }

  async beforeModel() {
    await this.user.ready();
    return this.isErrorPage || this.checkServiceRedirect();
  }
}
