import AuthenticatedRoute from './authenticated-route';
import { inject as service } from '@ember/service';

export default class AuthenticatedAdminRoute extends AuthenticatedRoute {
  @service session;

  beforeModel() {
    if (!this.session.isAuthenticated) {
      return this.transitionTo('login.index');
    }

    return this.store.queryRecord('user', { me: true }).then((user) => {
      if (!user.isAdmin) {
        this.transitionTo('index');
      }
    });
  }
}
