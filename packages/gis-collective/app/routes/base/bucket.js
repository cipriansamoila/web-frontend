import BrowseRoute from './browse';
import { hash, all } from 'rsvp';
import { inject as service } from '@ember/service';

export default class BucketRoute extends BrowseRoute {
  @service searchStorage;
  oldSearchTerm = '';

  constructor() {
    super(...arguments);

    this.queryParams['pages'] = {
      refreshModel: true,
      replace: true,
    };
  }

  model(queryParams) {
    var term = queryParams['search'];
    var pages = queryParams['pages'] || 1;

    this.searchStorage.set('term', term);

    if (this.buckets.length > pages || pages == 1 || !pages) {
      this.buckets.clear();
    }

    let localModel = [];

    if (this.buckets.length < pages) {
      for (let i = this.buckets.length; i < pages; i++) {
        let params = this.getParams(queryParams);
        params.limit = this.itemsPerPage;
        params.skip = i * this.itemsPerPage;
        params.term = term;

        localModel.push(this.prepareModel(params));
      }
    }

    var hashModel = {
      itemsPerPage: this.itemsPerPage,
      buckets: this.buckets,
    };

    if (this.fillHashModel) {
      this.fillHashModel(hashModel, queryParams);
    }

    if (localModel.length >= 0) {
      return all(localModel).then((result) => {
        result.forEach((item) => {
          this.buckets.push(item.content);
        });

        return hash(hashModel);
      });
    }

    return hash(hashModel);
  }
}
