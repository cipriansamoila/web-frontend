import InfinityModel from 'ember-infinity/lib/infinity-model';
import { tracked } from '@glimmer/tracking';

export default class GisCollectiveInfinityModel extends InfinityModel {
  @tracked columns = [];
  @tracked loadedAny = true;

  reset() {
    this.columns.clear();
    this.content.clear();
    this.currentPage = 0;
  }

  buildParams() {
    const params = super.buildParams(...arguments);
    let deleted = 0;

    this.columns.forEach((list) => {
      list.forEach((item) => {
        if (item.isDeleted) {
          deleted++;
        }
      });
    });

    params['limit'] = params['per_page'];
    params['skip'] = (params['page'] - 1) * params['per_page'] - deleted;
    return params;
  }

  afterInfinityModel(items) {
    super.afterInfinityModel(items);
    this.loadedAny = items.length > 0;
    this.canLoadMore = this.loadedAny;

    this.columns.pushObject(items);
  }
}
