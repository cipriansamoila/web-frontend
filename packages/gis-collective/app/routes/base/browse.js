import AppRoute from './app-route';
import EmberObject from '@ember/object';
import { all } from 'rsvp';
import { inject as service } from '@ember/service';

export default class BrowseRoute extends AppRoute {
  @service searchStorage;
  @service fastboot;
  @service user;
  @service store;

  queryParams = {
    author: {
      refreshModel: true,
    },
    visibility: {
      refreshModel: true,
    },
    icon: {
      refreshModel: true,
    },
    search: {
      refreshModel: true,
    },
    area: {
      refreshModel: true,
    },
    map: {
      refreshModel: true,
    },
    team: {
      refreshModel: true,
    },
    published: {
      refreshModel: true,
    },
    category: {
      refreshModel: true,
    },
    subcategory: {
      refreshModel: true,
    },
    iconSet: {
      refreshModel: true,
    },
    tag: {
      refreshModel: true,
    },
    lon: {
      refreshModel: true,
    },
    lat: {
      refreshModel: true,
    },
    all: {
      refreshModel: true,
    },
  };

  resetController(controller) {
    controller.set('map', '');
    controller.set('area', '');
    controller.set('author', '');
    controller.set('icon', '');
    controller.set('published', null);
    controller.set('search', '');
    controller.isSearching = false;
  }

  getAuthor(params) {
    if (!params.author) {
      return;
    }

    return this.store.findRecord('user', params.author);
  }

  getIcon(params) {
    if (!params.icon) {
      return;
    }

    return this.store.findRecord('icon', params.icon);
  }

  getMap(params) {
    if (!params.map) {
      return;
    }

    return this.store.findRecord('map', params.map);
  }

  getSubcategories(iconSet, category) {
    if (!iconSet || !category) {
      return [];
    }

    return this.store.findRecord('icon-set', iconSet).then((iconSet) => {
      return iconSet.subcategories(category).then((result) => {
        return result['iconSetSubcategories'].map((a) => a['subcategory']);
      });
    });
  }

  getIconSets() {
    return this.store.findAll('icon-set').then((list) => {
      return all(
        list
          .filter((a) => !a.isNew)
          .map((iconSet) => {
            return iconSet.categories().then((categories) => {
              return EmberObject.create({
                id: iconSet.id,
                name: iconSet.name,
                categories: categories['iconSetCategories'].map(
                  (a) => a.category
                ),
              });
            });
          })
      );
    });
  }

  mapsSetup(destination, params) {
    if (params['search']) {
      destination['term'] = params['search'];
    }

    if (params['team']) {
      destination['team'] = params['team'];
    }

    if (params['tag']) {
      destination['tag'] = params['tag'];
    }

    if (params['area']) {
      destination['area'] = params['area'];
    } else if (params['lon'] & params['lat']) {
      destination['lon'] = params['lon'];
      destination['lat'] = params['lat'];
    }

    if (params['published']) {
      destination['published'] = params['published'];
    }
  }

  featuresSetup(destination, params) {
    const lon = params['lon'];
    const lat = params['lat'];

    if (params['search']) {
      destination['term'] = params['search'];
    }

    if (params['author']) {
      destination['author'] = params['author'];
    }

    if (params['tag']) {
      destination['tag'] = params['tag'];
    }

    if (params['category']) {
      destination['iconCategory'] = params['category'];
    }

    if (params['subcategory']) {
      destination['iconSubcategory'] = params['subcategory'];
    }

    if (params['icon']) {
      destination['icons'] = params['icon'];
    }

    if (params['map']) {
      destination['map'] = params['map'];
    }

    if (params['published']) {
      destination['published'] = params['published'];
    }

    if (params['visibility']) {
      destination['visibility'] = params['visibility'];
    }

    if (params['area']) {
      destination['area'] = params['area'];
    } else if (lon && lat) {
      destination['lon'] = lon;
      destination['lat'] = lat;
    }
  }

  searchResult(params) {
    if (!params.search) {
      return [];
    }

    this.searchStorage.set('term', params.search);

    let searchQuery = {
      term: params.search,
    };

    if (params.icons) {
      searchQuery.icons = params.icons;
    }

    return this.store.query('sitesubset', searchQuery);
  }
}
