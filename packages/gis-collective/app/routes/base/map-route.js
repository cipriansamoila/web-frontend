import IconGroupRoute from './icon-group-route';
import { inject as service } from '@ember/service';
import Polygon from 'ol/geom/Polygon';

export default class MapRoute extends IconGroupRoute {
  @service searchStorage;
  @service mapStyles;
  @service store;
  @service router;

  queryParams = {
    viewbox: {
      refreshModel: true,
      replace: true,
    },
    icons: {
      refreshModel: true,
    },
    search: {
      refreshModel: true,
    },
    map: {
      refreshModel: true,
    },
    feature: {
      refreshModel: true,
    },
  };

  resetController(controller) {
    controller.set('search', '');
  }

  legend(params) {
    return this.store.query('legend', {
      viewbox: params['viewbox'],
      map: params.id,
    });
  }

  viewBox(params) {
    if (!params.viewbox || params.viewbox.indexOf('NaN') != -1) {
      params.viewbox = '0,0,1,1';
    }

    return params.viewbox;
  }

  searchResult(params) {
    if (!params.search) {
      return [];
    }

    let hash;
    let query = this.baseQuery(params);

    hash = query.viewbox + query.term + query.map;

    if (params.icons) {
      query.icons = params.icons;
      hash += query.icons.join ? query.icons.join(',') : query.icons;
    }

    if (this.searchCache && this.searchCache.hash == hash) {
      return this.searchCache.result;
    }

    return this.store.query('feature', query).then((result) => {
      this.searchCache = {
        hash,
        result,
      };

      return result;
    });
  }

  updateViewbox(params, map) {
    function validate(boundary, value) {
      if (value > boundary) {
        return boundary;
      }

      if (value < boundary * -1) {
        return boundary * -1;
      }

      return value;
    }

    if (!params) {
      return {};
    }

    if (params.viewbox) {
      const pieces = params.viewbox.split(',').map((a) => parseFloat(a));

      pieces[0] = validate(180, pieces[0]);
      pieces[1] = validate(90, pieces[1]);
      pieces[2] = validate(180, pieces[2]);
      pieces[3] = validate(90, pieces[3]);

      params.viewbox = pieces.join(',');

      return params;
    }

    if (map && map.area) {
      const geometry = new Polygon(map.area.coordinates.slice());
      params.viewbox = geometry.getExtent().join(',');
    }

    return params;
  }

  icons(params) {
    const query = this.baseQuery(params);

    if (this.iconsCache && this.iconsCache.viewbox == query.viewbox) {
      return this.iconsCache.result;
    }

    return this.store
      .adapterFor('icon')
      .group(query)
      .then((iconGroup) => {
        this.addIcons(iconGroup);

        this.iconsCache = {
          viewBox: query.viewbox,
          result: iconGroup,
        };

        return iconGroup;
      });
  }

  async map(params) {
    if (!params.map) {
      return null;
    }

    let map = this.store.peekRecord('map', params.map);

    if (map) {
      return map;
    }

    return this.store.findRecord('map', params.map);
  }

  feature(params) {
    this.mapStyles.selectedId = params.feature;

    if (!params.feature) {
      return null;
    }

    let feature = this.store.peekRecord('feature', params.feature);

    if (feature) {
      return feature;
    }

    return this.store.findRecord('feature', params.feature);
  }

  baseQuery(params) {
    var query = {
      viewbox: this.viewBox(params),
    };

    if (params.map) {
      query.map = params.map;
    }

    if (params.icons) {
      query.icons = params.icons;
    }

    if (params.search) {
      query.term = params.search;
    }

    return query;
  }

  features(params) {
    var query = this.baseQuery(params);

    return this.store.query('sitesubset', query);
  }

  baseMaps() {
    if (this.cachedBaseMaps) {
      return this.cachedBaseMaps;
    }

    return this.store.query('baseMap', { default: true }).then((result) => {
      this.cachedBaseMaps = result;

      return result;
    });
  }
}
