import AuthenticatedRoute from './authenticated-route';
import { hash } from 'rsvp';

export default class ManageEditRoute extends AuthenticatedRoute {
  queryParams = {
    allTeams: {
      refreshModel: true,
    },
  };

  async model(params, queries) {
    const teamsParams = {};

    if (params.allTeams) {
      teamsParams.all = true;
    } else {
      teamsParams.edit = true;
    }

    return hash({
      ...queries,
      teams: this.store.query('team', teamsParams),
    });
  }
}
