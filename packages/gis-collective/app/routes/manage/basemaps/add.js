import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageBaseMapsAddRoute extends AuthenticatedRoute {
  model() {
    this.modelsToClear = ['base-map'];

    const baseMap = this.store.createRecord('base-map', {
      name: '',
      icon: 'map',
      attributions: [],
      layers: [],
      defaultOrder: 100,
      visibility: {
        isPublic: false,
        isDefault: false,
        team: null,
      },
    });

    return hash({
      baseMap: baseMap,
      teams: this.store.query('team', { edit: true }),
    });
  }
}
