import ManageEditRoute from '../../base/manage-edit-route';

export default class ManageArticlesEditRoute extends ManageEditRoute {
  model(params) {
    return super.model(params, {
      article: this.store.findRecord('article', params['id']),
    });
  }
}
