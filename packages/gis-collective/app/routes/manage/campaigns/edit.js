import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { addIconGroupToStore } from '../../../lib/icons';

export default class ManageCampaignsEditRoute extends AuthenticatedRoute {
  queryParams = {
    allTeams: {
      refreshModel: true,
    },
  };

  getIcons() {
    return this.store
      .adapterFor('icon')
      .group({})
      .then((icons) => {
        addIconGroupToStore(icons, this.store);
        return icons;
      });
  }

  async model(params) {
    if (this.fastboot.isFastBoot) {
      return null;
    }

    const teamsParams = {};

    teamsParams.all = !!params.allTeams;
    teamsParams.edit = !params.allTeams;

    const campaign = await this.store.findRecord('campaign', params.id);

    return hash({
      icons: this.getIcons(),
      maps: this.store.query('map', {
        edit: true,
        team: campaign.get('visibility').teamId,
      }),
      teams: this.store.query('team', teamsParams),
      campaign,
    });
  }
}
