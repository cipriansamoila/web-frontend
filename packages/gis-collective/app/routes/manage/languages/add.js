import AuthenticatedAdminRoute from '../../base/authenticated-admin-route';

export default class ManageLanguagesAddRoute extends AuthenticatedAdminRoute {
  model() {
    this.modelsToClear = ['translation'];

    return this.store.createRecord('translation', {
      name: '',
      locale: '',
      file: '',
    });
  }
}
