import AuthenticatedAdminRoute from '../../base/authenticated-admin-route';

export default class ManageLanguagesAddRoute extends AuthenticatedAdminRoute {
  model(params) {
    this.modelsToClear = ['translation'];

    return this.store.findRecord('translation', params.id);
  }
}
