import AuthenticatedAdminRoute from '../../base/authenticated-admin-route';

export default class ManageLanguagesAddRoute extends AuthenticatedAdminRoute {
  model() {
    return this.store.findAll('translation');
  }
}
