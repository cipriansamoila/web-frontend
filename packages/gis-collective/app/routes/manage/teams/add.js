import AuthenticatedRoute from '../../base/authenticated-route';

export default class ManageTeamsAddRoute extends AuthenticatedRoute {
  model() {
    if (this.isFastBoot) {
      return;
    }

    this.modelsToClear = ['team'];

    return this.store.createRecord('team', {
      name: '',
      about: '',
      isPublic: false,
      owners: [],
      leaders: [],
      members: [],
      guests: [],
    });
  }
}
