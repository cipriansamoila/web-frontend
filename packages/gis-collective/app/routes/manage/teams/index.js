import AuthenticatedAdminRoute from '../../base/authenticated-admin-route';
import ExtendedInfinityModel from '../../base/infinity-model';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default class ManageTeams extends AuthenticatedAdminRoute {
  @service infinity;
  @service user;
  queryParams = {
    search: {
      refreshModel: true,
    },
  };

  model(params) {
    const teamsParams = {
      edit: true,
      all: true,
      term: params['search'],
    };

    return hash({
      buckets: this.infinity.model(
        'team',
        teamsParams,
        ExtendedInfinityModel.extend()
      ),
    });
  }
}
