import AuthenticatedRoute from '../../base/authenticated-route';

export default class ManageTeamsEditRoute extends AuthenticatedRoute {
  model(params) {
    return this.store.findRecord('team', params.id);
  }
}
