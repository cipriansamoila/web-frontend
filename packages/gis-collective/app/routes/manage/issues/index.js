import AuthenticatedRoute from '../../base/authenticated-route';
import ExtendedInfinityModel from '../../base/infinity-model';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default class ManageIssuesRoute extends AuthenticatedRoute {
  @service infinity;

  queryParams = {
    feature: {
      refreshModel: true,
    },
  };

  model(params) {
    return hash({
      feature: this.store.findRecord('feature', params.feature),
      buckets: this.infinity.model(
        'issue',
        { feature: params.feature },
        ExtendedInfinityModel.extend()
      ),
    });
  }
}
