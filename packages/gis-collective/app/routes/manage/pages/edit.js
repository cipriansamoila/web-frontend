import ManageEditRoute from '../../base/manage-edit-route';

export default class ManagePagesEditRoute extends ManageEditRoute {
  async model(params) {
    let colData = {};
    let layouts;
    let page = await this.store.findRecord('page', params.id, { reload: true });
    let layout = await this.store.findRecord('layout', page.get('layout.id'), {
      reload: true,
    });

    try {
      layouts = await this.store.findAll('layout');
    } catch {
      layouts = [layout];
    }

    try {
      for (let col of page.cols) {
        colData[col.modelKey] = col.fetch?.();
      }
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
      page = null;
    }

    return super.model(params, {
      ...colData,
      page,
      layout,
      layouts,
    });
  }
}
