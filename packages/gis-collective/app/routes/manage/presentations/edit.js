import ManageEditRoute from '../../base/manage-edit-route';

export default class ManagePresentationsEditRoute extends ManageEditRoute {
  model(params) {
    return super.model(params, {
      presentation: this.store.findRecord('presentation', params.id),
      layouts: this.store.findAll('layout'),
    });
  }
}
