import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class ManageMapsEditRoute extends AuthenticatedRoute {
  @service('loading') loadingService;

  queryParams = {
    allTeams: {
      refreshModel: true,
    },
  };

  model(params) {
    if (this.fastboot.isFastBoot) {
      return hash({
        map: this.store.findRecord('map', params.id),
      });
    }

    this.modelsToClear = ['map'];

    const teamsParams = {};

    if (params.allTeams) {
      teamsParams.all = true;
    } else {
      teamsParams.edit = true;
    }

    return hash({
      baseMaps: this.store.findAll('base-map'),
      iconSets: this.store.findAll('icon-set'),
      teams: this.store.query('team', teamsParams),
      indexedMaps: this.store.query('map', { isIndex: true }),
      map: this.store.findRecord('map', params.id),
    });
  }

  async afterModel(model) {
    await model.map?.visibility?.fetchTeam();
  }

  @action
  loading(transition) {
    return !transition.from || transition.from.name != transition.to.name;
  }
}
