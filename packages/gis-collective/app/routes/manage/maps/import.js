import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageTeamsRoute extends AuthenticatedRoute {
  async model(params) {
    const files = await this.store.query(
      'map-file',
      { map: params.id },
      { reload: true }
    );

    let file;

    if (params.file) {
      file = this.store.peekRecord('map-file', params.file);
    }

    return hash({
      files,
      file,
      map: this.store.findRecord('map', params.id),
    });
  }

  afterModel(model) {
    if (model.file) {
      return model.file.reloadMeta();
    }
  }
}
