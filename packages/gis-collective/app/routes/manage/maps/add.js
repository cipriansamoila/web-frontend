import AuthenticatedRoute from '../../base/authenticated-route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default class ManageMapsAddRoute extends AuthenticatedRoute {
  @service store;

  model() {
    if (this.fastboot.isFastBoot) {
      return false;
    }

    this.modelsToClear = ['map'];

    const teamParams = {
      edit: true,
      all: false,
    };

    const map = this.store.createRecord('map', {
      contributors: [],
      description: '',
      cover: this.store.createRecord('picture', {
        name: '',
        picture: '',
      }),
      visibility: {
        isPublic: false,
        team: null,
      },
    });

    return hash({
      teams: this.store.query('team', teamParams),
      map: map,
    });
  }
}
