import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageDashboardRoute extends AuthenticatedRoute {
  async model(params) {
    return hash({
      team: await this.store.findRecord('team', params.id),
      maps: this.store.query('map', { team: params.id }),
      iconSets: this.store.query('iconSet', { team: params.id }),
      basemaps: this.store.query('baseMap', { team: params.id }),
      campaigns: this.store.query('campaign', { team: params.id }),
      pages: this.store.query('page', { team: params.id }),
      presentations: this.store.query('presentation', { team: params.id }),
      articles: this.store.query('article', { team: params.id }),
    });
  }
}
