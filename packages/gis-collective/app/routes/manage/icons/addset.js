import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageIconsAddSetRoute extends AuthenticatedRoute {
  model() {
    if (this.isFastBoot) {
      return;
    }

    this.modelsToClear = ['iconSet'];

    const teamParams = {
      edit: true,
      all: false,
    };

    const iconSet = this.store.createRecord('iconSet', {
      name: '',
      description: '',
      visibility: {
        team: null,
        isPublic: false,
      },
    });

    return hash({
      teams: this.store.query('team', teamParams),
      iconSet: iconSet,
    });
  }
}
