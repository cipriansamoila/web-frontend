import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class ManageIconsEditSetRoute extends AuthenticatedRoute {
  queryParams = {
    allTeams: {
      refreshModel: true,
    },
  };

  async model(params) {
    const teamsParams = {};

    if (params.allTeams) {
      teamsParams.all = true;
    } else {
      teamsParams.edit = true;
    }

    const iconSet = await this.store.findRecord('icon-set', params.id);

    return hash({
      teams: this.store.query('team', teamsParams),
      iconSet,
      team: this.store.findRecord('team', iconSet.visibility.teamId),
    });
  }
}
