import AuthenticatedRoute from '../../base/authenticated-route';
import { tracked } from '@glimmer/tracking';

export default class ManageIconEditRoute extends AuthenticatedRoute {
  async model(params) {
    this.modelsToClear = ['icon'];

    const icon = await this.store.findRecord('icon', params.id, {
      adapterOptions: { locale: 'none' },
    });
    let parentIcon;

    if (icon.parent) {
      parentIcon = await this.store.findRecord('icon', icon.parent);
    }

    const iconData = new EditIconData();
    iconData.iconSet = await this.store.findRecord('iconSet', params.set_id);
    iconData.iconSets = this.store.query('iconSet', { edit: true });
    iconData.team = this.store.findRecord(
      'team',
      iconData.iconSet.visibility.teamId
    );
    iconData.icons = this.store.query('icon', { edit: true, locale: 'none' });
    iconData.icon = icon;
    iconData.parentIcon = parentIcon;
    iconData.parentIconSet = parentIcon?.get?.('iconSet');
    iconData.translations = this.store.findAll('translation');

    return iconData;
  }

  async afterModel(model) {
    await model.all();
  }
}

class EditIconData {
  @tracked team;
  @tracked iconSet;
  @tracked iconSets;
  @tracked icons;
  @tracked icon;
  @tracked parentIcon;
  @tracked parentIconSet;
  @tracked translations;

  all() {
    return Promise.all([
      this.team,
      this.iconSet,
      this.iconSets,
      this.icons,
      this.icon,
      this.parentIcon,
      this.parentIconSet,
      this.translations,
    ]);
  }
}
