import AuthenticatedRoute from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { addIconGroupToStore } from '../../../lib/icons';
import { inject as service } from '@ember/service';

export default class ManageSitesEditRoute extends AuthenticatedRoute {
  @service user;

  queryParams = {
    parentMap: {
      refreshModel: true,
    },
  };

  async model(params) {
    if (this.fastboot.isFastBoot) {
      return hash({
        feature: this.store.findRecord('feature', params.id),
      });
    }

    const baseMaps = await this.store.query('baseMap', { default: true });
    const feature = await this.store.findRecord('feature', params.id);

    let parentMap = feature.maps.firstObject;

    if (params.parentMap) {
      parentMap = this.store.findRecord('map', params.parentMap);
    }

    let iconSets = await feature.iconSets;

    if (iconSets.length == 0) {
      iconSets = (await this.store.query('iconSet', { default: true })).map(
        (a) => a.id
      );
    }

    const icons = await this.store
      .adapterFor('icon')
      .group({ iconSet: iconSets.join() });

    addIconGroupToStore(icons, this.store);

    const teams = await this.store.query('team', { user: this.user.id });
    const mapQueries = await Promise.all(
      teams.map((a) =>
        this.store.query('map', {
          team: a.id,
        })
      )
    );

    return hash({
      maps: mapQueries.map((a) => a.toArray()).flatMap((a) => a),
      icons,
      feature,
      baseMaps,
      parentMap,
    });
  }

  getContaining(feature) {
    if (feature.position.type == 'Point') {
      return feature.position.coordinates.join(',');
    }

    if (feature.position.type == 'LineString') {
      return feature.position.coordinates[0].join(',');
    }

    if (feature.position.type == 'MultiLineString') {
      return feature.position.coordinates[0][0].join(',');
    }

    if (feature.position.type == 'Polygon') {
      return feature.position.coordinates[0][0].join(',');
    }

    if (feature.position.type == 'MultiPolygon') {
      return feature.position.coordinates[0][0][0].join(',');
    }

    return '';
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    controller.setupIconsSubsets();
  }
}
