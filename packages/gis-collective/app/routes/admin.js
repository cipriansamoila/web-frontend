import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class AdminRoute extends Route {
  @service user;

  async beforeModel() {
    await this.user.ready();

    if (!this.user.isAdmin) {
      return this.transitionTo('index');
    }
  }
}
