import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default class PageRoute extends Route {
  @service store;

  toPageSlug(path) {
    const queryIndex = path.indexOf('?');

    if (queryIndex != -1) {
      path = path.substr(0, queryIndex);
    }

    return path
      .split('/')
      .filter((a) => a != '')
      .join('--');
  }

  async model(params, transition) {
    let colData = {};
    let path = transition.intent?.url || params.wildcard;

    if (typeof path != 'string') {
      return { page: null };
    }

    const slug = this.toPageSlug(path);
    let page;

    try {
      page = await this.store.findRecord('page', slug);

      for (let col of page.cols) {
        colData[col.modelKey] = col.fetch?.();
      }
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err);
      page = null;
    }

    return hash({
      ...colData,
      page,
      layout: page?.layout,
    });
  }
}
