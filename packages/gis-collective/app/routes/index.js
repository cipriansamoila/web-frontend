import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class IndexRoute extends Route {
  @service intl;
  @service preferences;
  @service router;
  @service session;

  beforeModel() {
    let params =
      this.intl.locale[0] == 'en-us' ? '' : `?locale=${this.intl.locale[0]}`;
    return this.router.transitionTo(`/browse/maps/_/map-view${params}`);
  }
}
