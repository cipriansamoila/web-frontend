import AuthenticatedRoute from '../base/authenticated-route';
import { hash } from 'rsvp';

export default class PreferencesTokensRoute extends AuthenticatedRoute {
  model() {
    return this.store.queryRecord('user', { me: true }).then((user) => {
      return user.updateTokens().then(() => {
        return hash({
          user: user,
          tokens: user.tokens,
        });
      });
    });
  }
}
