import AuthenticatedRoute from '../base/authenticated-route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default class PreferencesProfile extends AuthenticatedRoute {
  @service user;
  @service preferences;

  model() {
    return hash({
      profile: this.store.findRecord('user-profile', this.user.id),
      detailedLocation: this.preferences.getPreference(
        'profile.detailedLocation'
      ),
    });
  }
}
