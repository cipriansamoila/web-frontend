import AuthenticatedRoute from '../base/authenticated-route';

export default class PreferencesAccountRoute extends AuthenticatedRoute {
  model() {
    return this.store.queryRecord('user', { me: true });
  }
}
