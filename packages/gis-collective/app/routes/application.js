import AppRoute from './base/app-route';
import { inject as service } from '@ember/service';
import fetch from 'fetch';
import YAML from 'yaml';
import { action } from '@ember/object';
import { hash } from 'rsvp';
import { MetricsTracker } from '../lib/trackers/metrics';
import * as Sentry from '@sentry/browser';

export default class ApplicationRoute extends AppRoute {
  @service lastError;
  @service intl;
  @service embed;
  @service fastboot;
  @service user;
  @service articles;
  @service store;
  @service headData;
  @service tracking;
  @service space;

  queryParams = {
    fastboot: {},
    embed: {},
    locale: {
      refreshModel: true,
    },
  };

  constructor() {
    super(...arguments);

    this.router.on('routeDidChange', () => {
      const page = this.router.currentURL;
      const title = this.router.currentRouteName || 'unknown';

      this.tracking.addPage(page, title);
    });
  }

  get languages() {
    return this.store.findAll('translation').then((result) => {
      result = result.sortBy('locale');

      if (result.filter((a) => a.locale.indexOf('en-') == 0).length == 0) {
        result.addObject({ name: 'English', locale: 'en-us' });
      }

      return result;
    });
  }

  async beforeModel(transition) {
    await this.space.setup();
    await this.session.setup();
    await this.preferences.getAllPreferences();

    await this.user.ready();
    await this.articles.checkAbout();

    Sentry.setUser({
      email: this.user?.userData?.email ?? 'unknown',
      isFastboot: this.fastboot.isFastBoot,
    });

    if (this.fastboot.isFastBoot) {
      const page = transition?.intent?.url;
      const title = transition?.targetName || 'unknown';

      this.tracking.addPage(page, title);
    }

    if (!this.fastboot.isFastBoot) {
      this.tracking.registerTracker(
        'metrics',
        new MetricsTracker(this.store, this.preferences, this.fastboot)
      );
    }
  }

  async model(params) {
    if (params.embed === 'true' || params.embed === true) {
      this.embed.isEnabled = true;
    }

    let languages = [];
    let selectedLanguage;

    try {
      languages = await this.languages;
      selectedLanguage = languages.find((a) => a.locale == params.locale);
      // eslint-disable-next-line no-empty
    } catch (err) {}

    if (!selectedLanguage && params.locale) {
      return this.router.transitionTo({ queryParams: { locale: undefined } });
    }

    if (params.locale && selectedLanguage?.file) {
      const translations = await fetch(selectedLanguage.file);
      const text = await translations.text();

      this.intl.addTranslations(params.locale, YAML.parse(text));
    }

    if (params.locale && selectedLanguage?.customTranslations) {
      this.intl.addTranslations(
        params.locale,
        selectedLanguage?.customTranslations
      );
    }

    this.intl.set('locale', [params.locale ?? 'en-us']);

    let article;

    if (this.fastboot.isFastBoot) {
      article = this.store.findRecord('article', `about`);
    }

    return hash({
      languages,
      models: this.store.findAll('model'),
      article,
    });
  }

  async afterModel(model) {
    if (this.fastboot.isFastBoot) {
      this.headData.url = `${this.fastboot.request.protocol}//${this.fastboot.request.host}${this.fastboot.request.path}`;
    } else {
      this.headData.url = window.location.toString();
    }

    model.article?.fillMetaInfo(this.headData);
  }

  @action
  error(error) {
    const type = Object.prototype.toString.call(error);
    let errorMessage = 'Unknown error';

    if (error && error.message && error.message.toString) {
      errorMessage = error.message.toString().toLowerCase();
    }

    // eslint-disable-next-line no-console
    console.error(type, errorMessage, error);

    const isAdapterError =
      error.isAdapterError && error.errors && error.errors.length > 0;

    if (isAdapterError && error.errors[0].status == 404) {
      this.intermediateTransitionTo('/page');
      return;
    }

    if (isAdapterError && error.errors[0].status == 0) {
      return;
    }

    if (type === '[object DOMException]' && errorMessage == 'aborted') {
      return;
    }

    if (error.offerLogin) {
      this.lastError.set('offerLogin', true);
    }

    this.lastError.set('message', error.message);
    this.intermediateTransitionTo('error');
  }
}
