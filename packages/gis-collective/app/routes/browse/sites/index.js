import BrowseRoute from '../../base/browse';
import { inject as service } from '@ember/service';
import { hash, all } from 'rsvp';
import ExtendedInfinityModel from '../../base/infinity-model';

export default class BrowseSitesRoute extends BrowseRoute {
  @service infinity;
  @service headData;

  model(params) {
    const featuresParams = {};

    this.featuresSetup(featuresParams, params);

    const getNewBuckets = async () =>
      await this.infinity.model(
        'feature',
        featuresParams,
        ExtendedInfinityModel.extend()
      );

    return hash({
      getNewBuckets,
      buckets: getNewBuckets(),
      author: this.getAuthor(params),
      icon: this.getIcon(params),
      map: this.getMap(params),
      area: params.area,
    });
  }

  async afterModel(model) {
    const items = [];

    if (model.icon) {
      await model.icon.fillMetaInfo(this.headData);
    }

    if (model.map) {
      items.push(
        model.map.reloadMeta().catch((err) => {
          // eslint-disable-next-line no-console
          console.error(err);
        })
      );

      await model.map.visibility.fetchTeam();
      await model.map.get('cover');
      await model.map.get('squareCover');
      await model.map.fillMetaInfo(this.headData);
    }

    if (!this.fastboot.isFastBoot) {
      return all(items);
    }

    model.buckets.content.forEach((feature) => {
      feature.icons.forEach((icon) => {
        items.push(
          this.store.findRecord('icon', icon.id).catch((err) => {
            // eslint-disable-next-line no-console
            console.error(err);
          })
        );
      });

      feature.pictures.forEach((picture) => {
        items.push(
          this.store.findRecord('picture', picture.id).catch((err) => {
            // eslint-disable-next-line no-console
            console.error(err);
          })
        );
      });
    });

    if (model.map?.team) {
      items.push(model.map.team.get('logo'));
    }

    return all(items);
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    controller.setupFilters();
    controller.authorFilter.author = model.author;
    controller.areaFilter.area = model.area;
    controller.mapFilter.map = model.map;
  }
}
