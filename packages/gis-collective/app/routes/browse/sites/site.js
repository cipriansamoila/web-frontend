import AppRoute from '../../base/app-route';
import { hash, all } from 'rsvp';
import { inject as service } from '@ember/service';

export default class BrowseSiteRoute extends AppRoute {
  @service headData;

  queryParams = {
    parentMap: {
      refreshModel: true,
      replace: true,
    },
  };

  async model(params) {
    const feature = await this.store.findRecord('feature', params.id);

    let map;
    let profile;

    if (feature.originalAuthor.indexOf('@') == -1) {
      try {
        profile = await this.store.findRecord(
          'user-profile',
          feature.originalAuthor
        );
      } catch (err) {
        // eslint-disable-next-line no-console
        console.error(err);
      }
    }

    if (params.parentMap) {
      map = this.store.findRecord('map', params.parentMap);
    } else {
      map = feature.maps.firstObject;
    }

    return hash({
      baseMaps: this.store.query('baseMap', { default: true }),
      feature,
      profile,
      map,
    });
  }

  async afterModel(model) {
    const items = [];

    model.feature.icons.forEach((icon) => {
      items.push(
        this.store.findRecord('icon', icon.get('id')).catch((err) => {
          // eslint-disable-next-line no-console
          console.error(err);
        })
      );
    });

    let firstPicture;
    model.feature.pictures.forEach((picture, index) => {
      let promise = this.store
        .findRecord('picture', picture.get('id'))
        .catch((err) => {
          // eslint-disable-next-line no-console
          console.error(err);
        });

      if (index == 0) {
        firstPicture = promise;
      }

      items.push(promise);
    });

    const profile = (await model.profile) ?? {};
    await firstPicture;
    await model.feature.fillMetaInfo(this.headData);
    this.headData.author = profile.fullName;
    this.headData.twitterUsername = profile.twitter;

    if (model.profile?.picture?.get?.('id')) {
      items.push(
        this.store
          .findRecord('picture', model.profile.picture.get('id'))
          .catch((err) => {
            // eslint-disable-next-line no-console
            console.error(err);
          })
      );
    }

    return all(items);
  }
}
