import Route from '@ember/routing/route';
import { hash } from 'rsvp';

export default class BrowseProfilesProfileRoute extends Route {
  async model(params) {
    const profile = await this.store.findRecord('user-profile', params.id);
    const teams = this.store.query('team', { user: params.id, all: true });

    return hash({
      profile,
      teams,
      contributions: profile.contributions,
    });
  }
}
