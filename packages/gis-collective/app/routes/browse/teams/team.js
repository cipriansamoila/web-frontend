import AppRoute from '../../base/app-route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default class BrowseTeamsTeam extends AppRoute {
  @service store;
  @service headData;

  async model(params) {
    const team = await this.store.findRecord('team', params.id);
    const campaigns = await this.store.query('campaign', { team: params.id });

    const members = [];

    team.owners.forEach((user) => {
      members.push(this.store.findRecord('user-profile', user.id));
    });

    team.leaders.forEach((user) => {
      members.push(this.store.findRecord('user-profile', user.id));
    });

    team.members.forEach((user) => {
      members.push(this.store.findRecord('user-profile', user.id));
    });

    team.guests.forEach((user) => {
      members.push(this.store.findRecord('user-profile', user.id));
    });

    return hash({
      team,
      campaigns,
      members,
      maps: this.store.query('map', { team: params.id }),
    });
  }

  async afterModel(model) {
    await model.team.get('logo');
    await model.team.fillMetaInfo(this.headData);
  }
}
