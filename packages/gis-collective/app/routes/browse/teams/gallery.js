import AppRoute from '../../base/app-route';
import { hash } from 'rsvp';

export default class BrowseTeamsRoute extends AppRoute {
  queryParams = {
    index: {
      refreshModel: false,
      replace: true,
    },
  };

  model(params) {
    return this.store.findRecord('team', params.id).then((team) => {
      return hash({
        team,
        pictures: team.get('pictures'),
      });
    });
  }
}
