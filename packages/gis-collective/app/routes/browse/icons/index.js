import BrowseRoute from '../../base/browse';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';
import ExtendedInfinityModel from '../../base/infinity-model';

export default class BrowseIconsIndexRoute extends BrowseRoute {
  @service infinity;

  model(params) {
    const iconsParams = {};

    if (params['search']) {
      iconsParams['term'] = params['search'];
    }

    return hash({
      buckets: this.infinity.model(
        'icon-set',
        iconsParams,
        ExtendedInfinityModel.extend()
      ),
    });
  }
}
