import BrowseRoute from '../../base/browse';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';
import ExtendedInfinityModel from '../../base/infinity-model';

export default class BrowseIconsSetRoute extends BrowseRoute {
  @service infinity;

  model(params) {
    const iconParams = { iconSet: params.id };

    if (params['search']) {
      iconParams['term'] = params['search'];
    }

    return hash({
      iconSet: this.store.findRecord('icon-set', params.id),
      buckets: this.infinity.model(
        'icon',
        iconParams,
        ExtendedInfinityModel.extend()
      ),
    });
  }
}
