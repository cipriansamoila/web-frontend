import MapRoute from '../../base/map-route';
import { hash, all } from 'rsvp';
import { inject as service } from '@ember/service';

export default class Route extends MapRoute {
  @service fullscreen;
  @service embed;
  @service mapStyles;
  @service preferences;
  @service headData;
  @service fastboot;
  @service tracking;

  async beforeModel() {
    try {
      await super.beforeModel();
      await this.preferences.getPreference('appearance.mapExtent');
    } catch (err) {
      // eslint-disable-next-line no-console
      console.warn('The `appearance.mapExtent` preference is not set.', err);
    }
  }

  async model(params) {
    let map;
    let baseMaps;
    let iconsQuery = {
      limit: 5,
    };

    params.map = params.id;
    if (params.id != '_') {
      map = await this.map(params);
      iconsQuery.map = map.id;
    }

    if (params.viewbox) {
      iconsQuery.viewbox = params.viewbox;
    }

    if (map?.baseMaps?.list?.length > 0) {
      baseMaps = all(map.baseMaps.list);
    } else {
      baseMaps = this.store.query('baseMap', { default: true });
    }

    let icons;

    try {
      icons = await this.store.query('icon', iconsQuery);
    } catch (err) {
      icons = [];
    }

    return hash({
      id: params?.id,
      icons,
      selectedIcon: this.selectedIcon(params),
      map,
      baseMaps,
    });
  }

  async afterModel(model, transition) {
    if (model.map && this.fastboot.isFastBoot) {
      this.tracking.addMapView(this.router.currentURL, model.id);

      await model.map.visibility.fetchTeam();
      let pictureId = model.map.get('squareCover.id');

      if (!pictureId) {
        pictureId = model.map.get('cover.id');
      }

      if (pictureId) {
        await this.store.findRecord('picture', pictureId);
      }

      await model.map.fillMetaInfo(this.headData);
    }

    const from = transition.from?.name ?? '';
    const to = transition.to?.name ?? '';

    if (
      !this.fastboot.isFastBoot &&
      from != to &&
      from.indexOf('browse.maps.map-view.') == -1
    ) {
      this.tracking.addMapView(this.router.currentURL, model.id);
    }
  }

  async selectedIcon(params) {
    const iconIds = params.icons?.split(',').filter((a) => a.length > 0);

    if (iconIds.length > 0) {
      try {
        return await this.store.findRecord('icon', iconIds[0]);
      } catch (err) {
        return null;
      }
    }
  }

  resetController(controller) {
    controller.viewMode = '';
    controller.extentChangeCounter = -1;
    controller._extentM = null;
    controller.set('viewbox', null);
  }

  activate() {
    let controller = this.controllerFor('browse.maps.map-view');
    this.mapStyles.onChange = () => {
      controller.changeView();
    };
  }

  deactivate() {
    this.fullscreen.isEnabled = false;
    this.mapStyles.onChange = null;
  }
}
