import AppRoute from '../../../base/app-route';
import { inject as service } from '@ember/service';

export default class BrowseMapsMapViewIndexRoute extends AppRoute {
  @service fullscreen;
  @service embed;

  activate() {
    if (!this.embed.isEnabled) {
      this.fullscreen.isEnabled = false;
    }
  }
}
