import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';
import { addIconGroupToStore } from '../../../../lib/icons';

export default class BrowseMapsMapViewFilterIconsRoute extends Route {
  @service fullscreen;

  iconsCache = {};

  icons(params) {
    if (
      this.iconsCache &&
      this.iconsCache.viewbox == params.viewbox &&
      params.viewbox
    ) {
      return this.iconsCache.result;
    }

    return this.store
      .adapterFor('icon')
      .group(params)
      .then((iconGroup) => {
        addIconGroupToStore(iconGroup, this.store);

        this.iconsCache = {
          viewBox: params.viewbox,
          result: iconGroup,
        };

        return iconGroup;
      });
  }

  model() {
    const params = {};
    const parentModel = this.modelFor('browse.maps.map-view');

    if (parentModel.map && parentModel.map.id) {
      params.map = parentModel.map.id;
    }

    return hash({
      icons: this.icons(params),
    });
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    // eslint-disable-next-line ember/no-controller-access-in-routes
    controller.parent = this.controllerFor('browse.maps.map-view');
  }

  activate() {
    this.fullscreen.isEnabled = true;
  }
}
