import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default class BrowseMapsMapViewSpotlightRoute extends Route {
  @service fullscreen;
  @service infinity;
  @service fastboot;

  async model() {
    const iconsQuery = {
      limit: 8,
    };
    const parentModel = this.modelFor('browse.maps.map-view');
    await parentModel.map?.iconSets?.fetch?.();

    let iconSets = [];

    if (parentModel.map?.iconSets?.list?.length) {
      iconSets = await Promise.all(parentModel.map.iconSets.list);
      iconsQuery.map = parentModel.map.id;
    } else {
      iconSets = await this.store.query('icon-set', { default: true });
    }

    const icons = {};

    const iconPromises = [];

    iconSets.forEach((iconSet) => {
      const id = iconSet.get('id');
      const name = iconSet.get('name');

      const list = this.store.query('icon', {
        ...iconsQuery,
        iconSet: id,
      });
      icons[name] = list;
      iconPromises.push(list);
    });

    await Promise.all(iconPromises);

    return hash({
      iconSets,
      icons,
    });
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    // eslint-disable-next-line ember/no-controller-access-in-routes
    controller.parent = this.controllerFor('browse.maps.map-view');
  }

  activate() {
    // eslint-disable-next-line ember/no-controller-access-in-routes
    const controller = this.controllerFor('browse.maps.map-view');
    controller.isSearching = true;
    this.fullscreen.isEnabled = true;
  }

  deactivate() {
    // eslint-disable-next-line ember/no-controller-access-in-routes
    const controller = this.controllerFor('browse.maps.map-view');
    controller.isSearching = false;
    controller.allFeatures = null;
  }
}
