import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';
import ExtendedInfinityModel from '../../../base/infinity-model';

export default class BrowseMapsMapViewSearchRoute extends Route {
  @service fullscreen;
  @service infinity;
  @service fastboot;

  queryParams = {
    allFeatures: {
      refreshModel: true,
    },
  };

  async model(params) {
    // eslint-disable-next-line ember/no-controller-access-in-routes
    let controller = this.controllerFor('browse.maps.map-view');
    controller.term = params.term;

    const parentModel = this.modelFor('browse.maps.map-view');

    if (parentModel.map) {
      params.map = parentModel.map.id;
    }

    const lon = controller.lon;
    const lat = controller.lat;

    if (lon && lat) {
      params.lon = lon;
      params.lat = lat;
    }

    params.per_page = 12;

    const featureBuckets = this.infinity.model(
      'feature',
      params,
      ExtendedInfinityModel.extend()
    );
    let maps;
    let places;

    if (!parentModel.map && !params.allFeatures && params.term.length > 3) {
      maps = this.store.query('map', { ...params, limit: 15 });
      places = await this.store.query('geocoding', { query: params.term });
    }

    return hash({
      mapViewController: controller,
      mapId: params.map,
      term: params.term,
      featureBuckets,
      maps,
      places,
    });
  }

  async afterModel(model) {
    if (!this.fastboot.isFastBoot) {
      return;
    }

    const features = await model.featureBuckets?.columns?.firstObject;
    const maps = await model.mapBuckets?.columns?.firstObject;

    const items = [];

    features?.forEach((feature) => {
      feature.icons.forEach((icon) => {
        items.push(
          this.store.findRecord('icon', icon.id).catch((err) => {
            // eslint-disable-next-line no-console
            console.error(err);
          })
        );
      });

      feature.pictures.forEach((picture) => {
        items.push(
          this.store.findRecord('picture', picture.id).catch((err) => {
            // eslint-disable-next-line no-console
            console.error(err);
          })
        );
      });
    });

    maps?.forEach((map) => {
      items.push(map.get('cover'));
    });

    return Promise.all(items);
  }

  activate() {
    const controller = this.controllerFor('browse.maps.map-view');
    controller.isSearching = true;
    this.fullscreen.isEnabled = true;
  }

  deactivate() {
    const controller = this.controllerFor('browse.maps.map-view');
    controller.isSearching = false;
    controller.allFeatures = null;
  }
}
