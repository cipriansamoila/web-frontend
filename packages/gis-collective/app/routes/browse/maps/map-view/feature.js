import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';
import { action } from '@ember/object';

export default class BrowseMapsMapViewFeatureRoute extends Route {
  @service fullscreen;
  @service mapStyles;

  async model(params) {
    // eslint-disable-next-line ember/no-controller-access-in-routes
    let controller = this.controllerFor('browse.maps.map-view');

    controller.mapStyles.selectedId = params.feature;

    let feature =
      this.store.peekRecord('feature', params.feature) ||
      (await this.store.findRecord('feature', params.feature));

    return hash({ feature });
  }

  @action
  loading(transition) {
    if (transition.to.params.feature) {
      return !this.store.peekRecord('feature', transition.to.params.feature);
    }

    return true;
  }

  activate() {
    // eslint-disable-next-line no-console
    console.log('feature active');
    this.fullscreen.isEnabled = true;
  }

  deactivate() {
    // eslint-disable-next-line no-console
    console.log('feature inactive');
    this.lastSelected = null;
    this.mapStyles.selectedId = null;
  }
}
