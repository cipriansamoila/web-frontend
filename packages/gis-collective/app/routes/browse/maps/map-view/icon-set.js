/* eslint-disable ember/no-controller-access-in-routes */
import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { addIconGroupToStore } from '../../../../lib/icons';
import { action } from '@ember/object';
import { inject as service } from '@ember/service';

export default class BrowseMapsMapViewIconSetRoute extends Route {
  @service fullscreen;

  model(params) {
    const iconsParams = {
      iconSet: params.set_id,
    };

    this.loadingIconSet =
      this.store.peekRecord('icon-set', iconsParams.iconSet) ||
      this.store.findRecord('icon-set', iconsParams.iconSet);

    const parentModel = this.modelFor('browse.maps.map-view');

    if (parentModel.map && parentModel.map.id) {
      iconsParams.map = parentModel.map.id;
    }

    if (!this.iconsCache) {
      this.iconsCache = this.store
        .adapterFor('icon')
        .group(iconsParams)
        .then((iconGroup) => {
          addIconGroupToStore(iconGroup, this.store);
          return iconGroup;
        });
    }

    return hash({
      icons: this.iconsCache,
      iconSet: this.loadingIconSet,
    });
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    // eslint-disable-next-line ember/no-controller-access-in-routes
    controller.parent = this.controllerFor('browse.maps.map-view');
  }

  @action
  loading() {
    // eslint-disable-next-line ember/no-controller-access-in-routes
    let controller = this.controllerFor(
      'browse.maps.map-view.icon-set-loading'
    );
    controller.loadingIconSet = this.loadingIconSet;

    return true;
  }

  activate() {
    this.fullscreen.isEnabled = true;
  }

  deactivate() {
    const parentController = this.controllerFor('browse.maps.map-view');
    parentController.search = '';
    this.iconsCache = null;
  }
}
