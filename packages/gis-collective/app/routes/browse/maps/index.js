import BrowseRoute from '../../base/browse';
import { inject as service } from '@ember/service';
import { hash, all } from 'rsvp';
import ExtendedInfinityModel from '../../base/infinity-model';

export default class BrowseMapsRoute extends BrowseRoute {
  @service infinity;

  model(params) {
    const mapsParams = {};

    this.mapsSetup(mapsParams, params);
    let team;
    let editableTeams;

    if (params.team) {
      team = this.store.findRecord('team', params.team);
    }

    if (this.user.id) {
      editableTeams = this.store.query('team', { edit: true });
    }

    return hash({
      buckets: this.infinity.model(
        'map',
        mapsParams,
        ExtendedInfinityModel.extend()
      ),
      area: params.area,
      editableTeams,
      team,
    });
  }

  afterModel(model) {
    if (!this.fastboot.isFastBoot) {
      return;
    }

    const items = [];

    model.buckets.content.forEach((map) => {
      items.push(
        map.get('cover').catch((err) => {
          // eslint-disable-next-line no-console
          console.error(err);
        })
      );

      items.push(
        map.get('squareCover').catch((err) => {
          // eslint-disable-next-line no-console
          console.error(err);
        })
      );
    });

    return all(items);
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    controller.setupFilters();
    controller.areaFilter.area = model.area;
    controller.teamFilter.team = model.team;
  }
}
