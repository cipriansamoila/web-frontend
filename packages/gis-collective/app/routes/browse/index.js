import { hash, all } from 'rsvp';
import { inject as service } from '@ember/service';
import BrowseRouter from '../base/browse';

export default class BrowseRoute extends BrowseRouter {
  @service searchStorage;
  @service fastboot;

  maxLoadedMaps = 4;
  maxLoadedFeatures = 8;

  modelsByPosition(params) {
    const term = params['search'];
    const lon = params['lon'] || 0;
    const lat = params['lat'] || 0;

    this.set('searchStorage.term', term);

    let mapsParams = { limit: this.maxLoadedMaps };
    let featuresParams = { limit: this.maxLoadedFeatures };
    let modelParams = {};

    this.mapsSetup(mapsParams, params, lon, lat);
    this.featuresSetup(featuresParams, params, lon, lat);
    this.featuresSetup(modelParams, params, lon, lat);

    return hash({
      features: this.store.query('feature', featuresParams),
      maps: this.store.query('map', mapsParams),
      iconSets: this.store.query('icon-set', mapsParams),
      models: this.store.query('model', modelParams),
      area: params.area,
    });
  }

  model(params) {
    return this.modelsByPosition(params);
  }

  async afterModel(model) {
    if (!this.fastboot.isFastBoot) {
      return;
    }

    const items = [];

    model.features.forEach((feature) => {
      feature.icons.forEach((icon) => {
        items.push(
          this.store.findRecord('icon', icon.id).catch((err) => {
            // eslint-disable-next-line no-console
            console.error(err);
          })
        );
      });

      feature.pictures.forEach((picture) => {
        items.push(
          this.store.findRecord('picture', picture.id).catch((err) => {
            // eslint-disable-next-line no-console
            console.error(err);
          })
        );
      });
    });

    await Promise.all(model.maps.map((a) => a.get('cover')));
    await Promise.all(model.maps.map((a) => a.get('squareCover')));
    await Promise.all(model.iconSets.map((a) => a.get('cover')));

    return all(items);
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    controller.maxLoadedMaps = this.maxLoadedMaps;
    controller.maxLoadedFeatures = this.maxLoadedFeatures;
  }
}
