import Route from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class WizardImportFileSelectRoute extends Route {
  async model(params) {
    const map = await this.store.findRecord('map', params.id);

    return hash({
      map,
    });
  }
}
