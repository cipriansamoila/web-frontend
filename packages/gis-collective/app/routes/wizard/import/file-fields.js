import Route from '../../base/authenticated-route';
import { hash } from 'rsvp';
import { addIconGroupToStore } from '../../../lib/icons';

export default class WizardImportFileRoute extends Route {
  async model(params) {
    const file = await this.store.findRecord('map-file', params.id);
    const map = await file.map;

    const icons = await this.store.adapterFor('icon').group({});

    addIconGroupToStore(icons, this.store);

    return hash({
      file,
      map,
      icons,
      teamMaps: this.store.query('map', { team: map.visibility.teamId }),
    });
  }

  afterModel(model) {
    return model.file.reloadMeta();
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    if (controller.fields.length == 0) {
      controller.analyze();
    }
  }
}
