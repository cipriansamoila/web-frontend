import Route from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class WizardImportFilePreviewRoute extends Route {
  async model(params) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    const file = await this.store.findRecord('map-file', params.id);

    return hash({
      file,
      map: file.map,
      baseMaps: this.store.query('baseMap', { default: true }),
    });
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    controller.preview();
  }
}
