import Route from '../../base/authenticated-route';
import { hash } from 'rsvp';

export default class WizardImportFileLogRoute extends Route {
  async model(params) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    const file = await this.store.findRecord('map-file', params.id);

    return hash({
      file,
      map: file.map,
    });
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    if (!this.fastboot.isFastBoot) {
      controller.update();
    }
  }
}
