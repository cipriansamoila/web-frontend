import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default class ErrorRoute extends Route {
  @service lastError;

  deactivate() {
    this.lastError.message = 'unknown error';
  }
}
