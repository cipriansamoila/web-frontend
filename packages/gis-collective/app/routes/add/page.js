import AuthenticatedRoute from '../base/authenticated-route';
import { hash } from 'rsvp';
import ModelInfo from '../../lib/model-info';

export default class AddPageRoute extends AuthenticatedRoute {
  queryParams = {
    all: { refreshModel: true },
  };

  model(params) {
    if (this.isFastBoot) {
      return;
    }

    const teamParams = { edit: true };

    if (params.all) {
      teamParams.all = params.all;
    }

    return hash({
      page: this.store.createRecord('page', {
        name: '',
        description: '',
        info: new ModelInfo(),
        visibility: {
          isPublic: false,
          team: null,
        },
      }),
      teams: this.store.query('team', teamParams),
    });
  }
}
