import IconGroupRoute from '../base/icon-group-route';
import EmberObject from '@ember/object';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default class AddSiteRoute extends IconGroupRoute {
  @service position;
  @service pendingData;
  @service intl;

  queryParams = {
    lon: { refreshModel: false },
    lat: { refreshModel: false },
    map: { refreshModel: false },
    icons: { refreshModel: false },
    ignoreGps: { refreshModel: false },
  };

  constructor() {
    super(...arguments);
    this.pendingData.checkStorage();
  }

  async createFeature(params) {
    let feature = this.store
      .peekAll('feature')
      .find((a) => a.get('id') === null);

    if (!feature) {
      feature = await this.pendingData.feature;
    }

    if (!feature || feature.position == null) {
      feature = this.controllerFor('add.site').createNewFeature(params);
    }

    return feature;
  }

  getDefaultIconSets() {
    if (this._defaultIconSets) {
      return new Promise((resolve) => {
        resolve(this._defaultIconSets);
      });
    }

    return this.store.query('icon-set', { default: true }).then((result) => {
      this.set('_defaultIconSets', result);
      return result;
    });
  }

  getDefaultBaseMaps() {
    if (this._defaultBaseMaps) {
      return new Promise((resolve) => {
        resolve(this._defaultBaseMaps);
      });
    }

    return this.store.query('baseMap', { default: true }).then((result) => {
      this.set('_defaultBaseMaps', result);
      return result;
    });
  }

  getMaps(containing) {
    if (this._maps && this._maps.containing == containing) {
      return new Promise((resolve) => {
        resolve(this._maps.result);
      });
    }

    return this.store.query('map', { containing }).then((result) => {
      this.set('_maps', { containing, result });
      return result;
    });
  }

  getIcons(iconSet) {
    if (this._icons && this._icons.iconSet == iconSet) {
      return new Promise((resolve) => {
        resolve(this._icons.result);
      });
    }

    let iconQuery = { iconSet, locale: this.intl.primaryLocale };

    return this.store
      .adapterFor('icon')
      .group(iconQuery)
      .then((icons) => {
        this.addIcons(icons);

        this.set('_icons', { iconSet, result: icons });

        return icons;
      });
  }

  fillIcons(params, feature) {
    if (!params.icons) {
      return;
    }

    const icons = params.icons
      .split(',')
      .map((a) => this.store.peekRecord('icon', a))
      .filter((a) => a);

    feature.icons.setObjects(icons);
  }

  beforeModel() {
    if (this.position.watchId || this.position.errorMessage) {
      return;
    }

    return new Promise((resolve) => {
      this.position.watchPosition().then(resolve).catch(resolve);
    });
  }

  async model(params) {
    if (this.isFastBoot) {
      return;
    }

    const feature = await this.createFeature(params);

    const initialQuery = {
      iconSets: this.getDefaultIconSets(),
      baseMaps: this.getDefaultBaseMaps(),
    };

    return hash(initialQuery).then((result) => {
      const iconSetList = [];

      result.iconSets.forEach((item) => {
        iconSetList.push(item.id);
      });

      return this.getIcons(iconSetList.join()).then((defaultIcons) => {
        if (feature.icons.length == 0) {
          this.fillIcons(params, feature);
        }

        return hash({
          feature,
          defaultIcons,
          defaultBaseMaps: result.baseMaps,
        }).then((a) => EmberObject.create(a));
      });
    });
  }

  setupController(controller, model) {
    super.setupController(controller, model);

    controller.updateMaps();
    controller.updateIcons();
  }

  deactivate() {
    if (this.isFastBoot) {
      return;
    }

    const controller = this.controllerFor('add.site');

    if (controller.model.feature && !this.store.isDestroyed) {
      this.store.unloadRecord(controller.model.feature);
      return controller.pendingData.reset();
    }
  }
}
