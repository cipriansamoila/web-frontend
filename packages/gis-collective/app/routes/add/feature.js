import AuthenticatedRoute from '../base/authenticated-route';
import { hash } from 'rsvp';
import ModelInfo from '../../lib/model-info';

export default class AddFeatureRoute extends AuthenticatedRoute {
  queryParams = {
    all: { refreshModel: true },
  };

  model(params) {
    if (this.isFastBoot) {
      return;
    }

    const mapParams = { canAdd: true };

    if (params.all) {
      mapParams.all = params.all;
    }

    const feature = this.store.createRecord('feature', {
      name: '',
      description: '',
      visibility: 0,
      info: new ModelInfo(),
    });

    return hash({
      feature,
      baseMaps: this.store.query('baseMap', { default: true }),
      maps: this.store.query('map', mapParams),
    });
  }
}
