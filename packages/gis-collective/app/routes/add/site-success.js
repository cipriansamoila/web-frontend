import Route from '@ember/routing/route';
import { hash } from 'rsvp';

export default class AddSiteSuccessRoute extends Route {
  queryParams = {
    feature: { refreshModel: false },
    map: { refreshModel: false },
  };

  model(params) {
    let map;
    let feature;

    if (params.map) {
      map = this.store.findRecord('map', params.map);
    }

    if (params.feature) {
      feature = this.store.findRecord('feature', params.feature);
    }

    return hash({ map, feature });
  }
}
