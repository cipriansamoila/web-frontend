import AuthenticatedRoute from '../base/authenticated-route';
import { hash } from 'rsvp';
import ModelInfo from '../../lib/model-info';

export default class AddPresentationRoute extends AuthenticatedRoute {
  queryParams = {
    all: { refreshModel: true },
  };

  model(params) {
    if (this.isFastBoot) {
      return;
    }

    const teamParams = { edit: true };

    if (params.all) {
      teamParams.all = params.all;
    }

    return hash({
      presentation: this.store.createRecord('presentation', {
        name: '',
        description: '',
        info: new ModelInfo(),
        visibility: {
          isPublic: false,
          team: null,
        },
      }),
      teams: this.store.query('team', teamParams),
      layouts: this.store.findAll('layout'),
    });
  }
}
