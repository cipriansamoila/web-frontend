import PageRoute from '../page';

export default class CampaignsIndexRoute extends PageRoute {
  templateName = 'page';

  model(params, transition) {
    return super.model({ wildcard: '/campaigns' }, transition);
  }
}
