import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';
import GeoJson from '../../lib/geoJson';
import PositionDetails from '../../lib/positionDetails';
import AttributesContainer from '../../attributes/containers/attributes';
import { addIconSection } from '../../attributes/actions';

export default class CampaignsCampaignRoute extends Route {
  @service fastboot;
  @service position;
  @service session;
  @service headData;
  @service store;

  async model(params) {
    const campaign = await this.store.findRecord('campaign', params.id);
    const icons = await campaign.icons;
    let answer;

    if (!this.fastboot.isFastBoot) {
      const coordinates = this.position.center?.slice().map((a) => a || 0) ?? [
        0, 0,
      ];

      answer = this.store.createRecord('campaign-answer', {
        campaign,
        icons,
        attributes: {
          'position details': new PositionDetails({
            type: 'gps',
          }),
        },
        position: new GeoJson({ coordinates, type: 'Point' }),
      });

      icons.forEach((icon) => {
        addIconSection(answer, icon);
      });
    }

    if (campaign.map?.isEnabled) {
      try {
        var map = await this.store.findRecord('map', campaign.map?.mapId);
        // eslint-disable-next-line no-empty
      } catch (e) {}
    }

    const iconAttributes = new AttributesContainer(this.intl);
    iconAttributes.feature = answer;

    return hash({
      answer,
      campaign,
      iconAttributes,
      map,
      baseMaps: this.store.query('baseMap', { default: true }),
    });
  }

  async afterModel(model) {
    if (!this.fastboot.isFastBoot) {
      if (
        !model.campaign?.options?.registrationMandatory ||
        this.session.isAuthenticated
      ) {
        try {
          await this.position.watchPosition();
        } catch (err) {
          // eslint-disable-next-line no-console
          console.error(err);
          model.answer.attributes['position details'][
            'capturedUsingGPS'
          ] = false;
        }
      }

      return;
    }

    await model.campaign.visibility.fetchTeam();
    await model.campaign.get('squareCover');
    await model.campaign.get('cover');
    await model.campaign.fillMetaInfo(this.headData);
  }
}
