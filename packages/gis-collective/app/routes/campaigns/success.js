import Route from '@ember/routing/route';
import { hash } from 'rsvp';

export default class CampaignsSuccessRoute extends Route {
  async model(params) {
    const campaign = await this.store.findRecord('campaign', params.id);

    return hash({
      campaign,
    });
  }
}
