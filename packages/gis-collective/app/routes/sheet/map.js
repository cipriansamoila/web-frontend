import AuthenticatedRoute from '../base/authenticated-route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default class PresentationIndexRoute extends AuthenticatedRoute {
  @service infinity;
  @service fastboot;

  queryParams = {
    sortBy: {
      refreshModel: true,
    },
    isAscending: {
      refreshModel: true,
    },
    icon: {
      refreshModel: true,
    },
  };

  async getIcons(params) {
    if (!this._icons || params.id != this.loadedMap) {
      try {
        this._icons = await this.store.query('icon', {
          map: params.id,
        });
      } catch (err) {
        this._icons = [];
      }
    }

    return this._icons;
  }

  async getRecords(params, meta, currentSort) {
    if (!this._records || params.id != this.loadedMap) {
      this._records = [];

      for (let i = 0; i < meta.totalFeatures ?? 0; i++) {
        this._records.pushObject({});
      }
    }

    if (currentSort != this.currentSort) {
      for (let i = 0; i < meta.totalFeatures ?? 0; i++) {
        this._records.replace(i, 1, [{}]);
      }
    }

    return this._records;
  }

  async getMap(params) {
    let map = this.store.peekRecord('map', params.id);

    if (!map) {
      map = await this.store.findRecord('map', params.id);
    }

    return map;
  }

  async getMeta(params) {
    if (!this._meta || params.id != this.loadedMap) {
      let map = this.store.peekRecord('map', params.id);
      this._meta = await map.reloadMeta();
    }

    return this._meta;
  }

  async model(params) {
    if (this.fastboot.isFastBoot) {
      return;
    }

    const map = await this.getMap(params);
    const meta = await this.getMeta(params);

    const currentSort = `${params.sortBy}-${params.isAscending}`;

    let records = this.getRecords(params, meta, currentSort);
    let icons = await this.getIcons(params);

    this.loadedMap = params.id;
    this.currentSort = currentSort;

    return hash({
      map,
      meta,
      records,
      icons,
    });
  }
}
