import AppRoute from '../base/app-route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default class RoutesLoginRoute extends AppRoute {
  @service session;
  @service preferences;
  @service router;

  beforeModel(transition) {
    if (this.session.isAuthenticated) {
      return this.router.transitionTo(
        transition.to.queryParams.redirect || 'index'
      );
    }

    return super.beforeModel();
  }

  getArticle() {
    return new Promise((resolve) => {
      this.store
        .findRecord('article', 'login')
        .then((result) => {
          resolve(result);
        })
        .catch(() => {
          resolve({ title: '', content: '' });
        });
    });
  }

  model() {
    return hash({
      article: this.getArticle(),
      locale: this.locale,
      registrationEnabled: this.preferences
        .getPreference('register.enabled')
        .then((a) => a.value == 'true' || a.value === true),
      name: this.preferences
        .getPreference('appearance.name')
        .then((a) => a.value),
    });
  }
}
