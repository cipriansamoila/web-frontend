import Route from '@ember/routing/route';
import { hash } from 'rsvp';

import { inject as service } from '@ember/service';

export default Route.extend({
  session: service(),
  preferences: service(),
  fastboot: service(),
  router: service(),

  beforeModel() {
    if (this.session.isAuthenticated) {
      return this.router.transitionTo('index');
    }

    return this.preferences
      .getPreference('register.enabled')
      .then((registerEnabled) => {
        const value = registerEnabled.value;

        if (value != 'true' && value !== true) {
          this.router.transitionTo('index');
        }
      });
  },

  model() {
    if (this.fastboot.isFastBoot) {
      return;
    }

    return this.preferences
      .getPreference('captcha.enabled')
      .then((captchaEnabled) => {
        return hash({
          captchaEnabled: captchaEnabled.value,
          challenge:
            ['reCAPTCHA', 'mtCAPTCHA'].indexOf(captchaEnabled.value) != -1
              ? this.store.adapterFor('user').registerChallenge()
              : null,
        });
      });
  },
});
