import AppRoute from '../base/app-route';
import { hash } from 'rsvp';

export default class PresentationIndexRoute extends AppRoute {
  model(params) {
    return hash({
      presentation: this.store.findRecord(
        'presentation',
        `presentation--${params.name}`
      ),
    });
  }
}
