/* eslint-disable no-console */
// eslint-disable-next-line no-undef
const FastBootAppServer = require('fastboot-app-server');
// eslint-disable-next-line no-undef
const promMid = require('express-prometheus-middleware');
// eslint-disable-next-line no-undef
const client = require('prom-client');

const userAgentGauge = new client.Gauge({
  name: 'user_agent',
  labelNames: ['name'],
  help: 'The user agents seen by the frontend server',
});

const userIpGauge = new client.Gauge({
  name: 'user_ip',
  labelNames: ['name'],
  help: 'The user ip seen by the frontend server',
});

// eslint-disable-next-line no-undef
const apiHost = process.env.apiHost;
// eslint-disable-next-line no-undef
const authHost = process.env.authHost;
// eslint-disable-next-line no-undef
const port = parseInt(process.env.frontendPort) || 80;

function validateEnvHostVar(value, name) {
  if (value) {
    if (value.indexOf('http://') != 0 && value.indexOf('https://') != 0) {
      console.log(
        `The protocol "http://" or "https://" is missing from the ENV var "apiHost"`
      );

      // eslint-disable-next-line no-undef
      process.exit(1);
    }

    console.log(`Using "${value}" as backend server`);
  } else {
    console.log(
      `The ENV var "${name}" is not set! This might cause some problems`
    );
  }
}

validateEnvHostVar(apiHost, 'apiHost');
validateEnvHostVar(authHost, 'authHost');

function userAgentStats(req, res, next) {
  if (req.path == '/stats') return next();

  const userAgent = req.get('user-agent');
  userAgentGauge.inc({ name: userAgent });

  next();
}

function userIpStats(req, res, next) {
  if (req.path == '/stats') return next();

  userIpGauge.inc({ name: req.ip });

  next();
}

setInterval(function () {
  client.register.resetMetrics();
}, 15000);

let server = new FastBootAppServer({
  beforeMiddleware: function (app) {
    app.set('trust proxy');

    app.use(userAgentStats);
    app.use(userIpStats);

    app.use(
      promMid({
        metricsPath: '/stats',
        collectDefaultMetrics: true,
        requestDurationBuckets: [0.1, 0.5, 1, 1.5],
        prefix: 'frontend_',
      })
    );
  },
  distPath: '.',
  buildSandboxGlobals(defaultGlobals) {
    // Optional - Make values available to the Ember app running in the FastBoot server, e.g. "MY_GLOBAL" will be available as "GLOBAL_VALUE"
    return Object.assign({}, defaultGlobals, { apiHost });
  },
  gzip: false, // Optional - Enables gzip compression.
  port: port, // Optional - Sets the port the server listens on (defaults to the PORT env var or 3000).
});

server.start();
