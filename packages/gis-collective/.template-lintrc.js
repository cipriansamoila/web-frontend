'use strict';

module.exports = {
  extends: 'octane',
  rules: {
    'no-bare-strings': false,
    'no-inline-styles': false,
    'no-invalid-interactive': false,
    'no-action': false,
    'no-implicit-this': { allow: ['target.value'] },
  },
};
