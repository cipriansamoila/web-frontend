'use strict';

module.exports = function (/* environment, appConfig */) {
  // See https://github.com/zonkyio/ember-web-app#documentation for a list of
  // supported properties

  return {
    name: 'Green Map',
    short_name: 'Green Map',
    icons: [
      {
        src: 'assets/icon/128x128.png',
        sizes: '128x128',
        type: 'image/png',
      },
      {
        src: 'assets/icon/152x152.png',
        sizes: '152x152',
        type: 'image/png',
      },
      {
        src: 'assets/icon/144x144.png',
        sizes: '144x144',
        type: 'image/png',
      },
      {
        src: 'assets/icon/192x192.png',
        sizes: '192x192',
        type: 'image/png',
      },
      {
        src: '/assets/icon/512x512.png',
        sizes: '512x512',
        type: 'image/png',
      },
    ],
    start_url: '/',
    display: 'fullscreen',
    background_color: '#272727',
    theme_color: '#000000',
    ms: {
      tileColor: '#000000',
    },
    apple: {
      statusBarStyle: 'black',
    },
  };
};
