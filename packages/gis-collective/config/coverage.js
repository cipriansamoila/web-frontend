module.exports = {
  useBabelInstrumenter: true,
  reporters: ['lcov', 'html', 'text-summary'],
};
