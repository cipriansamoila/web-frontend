/* jshint node: true */

module.exports = function (environment) {
  let ENV = {
    modulePrefix: 'ogm',
    environment: environment,
    rootURL: '/',
    locationType: 'auto',
    historySupportMiddleware: true,

    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false,
      },
    },

    fastboot: {
      hostWhitelist: [/^.*$/, /^localhost:\d+$/],
    },

    contentSecurityPolicy: {
      'default-src': "'none'",
      'script-src': "'self' 'unsafe-eval' *.googleapis.com maps.gstatic.com",
      'font-src': "'self' fonts.gstatic.com",
      'connect-src': "'self' maps.gstatic.com",
      'img-src': "'self' *.googleapis.com csi.gstatic.com",
      'style-src':
        "'self' 'unsafe-inline' fonts.googleapis.com maps.gstatic.com",
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },
    'ember-cli-head': {
      suppressBrowserRender: true,
    },
  };

  ENV['ember-meta'] = {
    description: '',
    imgSrc: '',
    siteName: '',
    title: '',
    twitterUsername: '',
    url: '',
  };

  ENV.serviceDesk = {
    email:
      'incoming+giscollective-help-desk-13678468-issue-@incoming.gitlab.com',
    url: 'https://gitlab.com/GISCollective/help-desk/issues',
  };

  if (environment === 'development') {
    ENV.debugTiles = true;
    ENV.apiSiteSocket = 'ws://localhost:9091/socket/feature';
    ENV.apiUrl = 'http://localhost:9091';
    ENV.fastbootApiUrl = 'http://localhost:9091';
    ENV.authUrl = 'http://localhost:9092';
    ENV.siteTilesUrl = 'http://localhost:9091/tiles';
    ENV.fastbootAuthUrl = 'http://localhost:9092';

    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'home') {
    ENV.debugTiles = true;
    ENV.apiSiteSocket = 'ws://192.168.100.138:9091/socket/feature';
    ENV.apiUrl = 'http://192.168.100.138:9091';
    ENV.authUrl = 'http://192.168.100.138:9092';
    ENV.siteTilesUrl = 'http://192.168.100.138:9091/tiles';

    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    // ENV.APP.LOG_BINDINGS = true;
  }

  if (environment === 'lan') {
    ENV.debugTiles = true;
    ENV.apiSiteSocket = 'ws://192.168.100.116:9091/socket/feature';
    ENV.apiUrl = 'http://192.168.100.116:9091';
    ENV.authUrl = 'http://192.168.100.116:9092';
    ENV.siteTilesUrl = 'http://192.168.100.116:9091/tiles';

    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    // ENV.APP.LOG_BINDINGS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';
    ENV.apiSiteSocket = '/mock-server';
    ENV.apiUrl = '/mock-server';
    ENV.authUrl = '/mock-auth-server';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;
    ENV.APP.autoboot = false;

    ENV.APP.rootElement = '#ember-testing';

    ENV.siteTilesUrl = '/api-v1/tiles';

    ENV['simple-auth'] = {
      store: 'simple-auth-session-store:ephemeral',
    };
  }

  if (environment === 'production') {
    ENV.debugTiles = false;
    ENV.apiSiteSocket = '/api-v1/socket/feature';
    ENV.apiUrl = '/api-v1';
    ENV.fastbootApiUrl = 'http://gisc-api-service';
    ENV.fastbootAuthUrl = 'http://gis-collective-auth';
    ENV.fileUrl = '/api-v1';
    ENV.issueUrl = '/api-v1';
    ENV.siteTilesUrl = '/api-v1/tiles';
    ENV.authUrl = '/auth';
    ENV.sentryDsn =
      'https://08bdf8fb0a0a42ce8085605c1bf2f6a1@sentry.io/2825877';
  }

  if (environment === 'bundle') {
    ENV.debugTiles = false;
    ENV.apiSiteSocket = '/api-v1/socket/feature';
    ENV.apiUrl = '/api-v1';
    ENV.fastbootApiUrl = 'http://127.0.0.1:9091';
    ENV.fastbootAuthUrl = 'http://127.0.0.1:9092';
    ENV.fileUrl = '/api-v1';
    ENV.issueUrl = '/api-v1';
    ENV.siteTilesUrl = '/api-v1/tiles';
    ENV.authUrl = '/auth';
  }

  return ENV;
};
