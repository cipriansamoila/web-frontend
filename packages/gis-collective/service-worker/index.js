import cleanupCaches from 'ember-service-worker/service-worker/cleanup-caches';

const CACHE_KEY_PREFIX = 'giscoll-fallback';
const VERSION = 1;
const CACHE_NAME = `${CACHE_KEY_PREFIX}-${VERSION}`;

self.addEventListener('fetch', (event) => {
  let request = event.request;

  const isHttps = !/^https?/.test(request.url);
  const isLocalHost = /^http:\/\/localhost?/.test(request.url);

  if (request.method !== 'GET' || !isHttps || isLocalHost) {
    return;
  }

  if (urlMatchesAnyPattern(request.url)) {
    event.respondWith(
      caches.open(CACHE_NAME).then((cache) => {
        return fetch(request)
          .then((response) => {
            cache.put(request, response.clone());

            return caches
              .match(event.request)
              .then((cachedResponse) => {
                return cachedResponse || response.clone();
              })
              .catch(() => response.clone());
          })
          .catch(() => caches.match(event.request));
      })
    );
  }
});

self.addEventListener('activate', (event) => {
  event.waitUntil(cleanupCaches(CACHE_KEY_PREFIX, CACHE_NAME));
});

function urlMatchesAnyPattern(url) {
  if (url.indexOf('rnd') != -1) {
    return false;
  }

  if (url.indexOf('/icons/') != -1) {
    return true;
  }

  if (url.indexOf('/pictures/') != -1) {
    return true;
  }

  return false;
}
