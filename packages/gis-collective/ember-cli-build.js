'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const Funnel = require('broccoli-funnel');

module.exports = function (defaults) {
  let app = new EmberApp(defaults, {
    sassOptions: {
      includePaths: [
        '../../node_modules/bootstrap/scss',
        '../../node_modules/simplemde/dist',
        '../../node_modules/jsoneditor/src/scss',
        '../../node_modules/jsoneditor/src/js/assets/selectr',
        '../../node_modules/swiper',
        '../../node_modules/@fontsource'
      ],
    },
    'ember-power-select': {
      theme: 'bootstrap',
    },
    sourcemaps: {
      enabled: true,
    },
    'asset-cache': {
      include: ['assets/**/*', 'assets/*', 'img/*'],
    },
    'ember-service-worker': {
      versionStrategy: 'every-build',
    },
    'esw-index': {
      // Bypass esw-index and don't serve cached index file for matching URLs
      excludeScope: [
        /\/api(\/.*)?$/,
        /\/api-files(\/.*)?$/,
        /\/api-issues(\/.*)?$/,
        /\/stats(\/.*)?$/,
        /\/admin(\/.*)?$/,
        /\/auth(\/.*)?$/,
      ],
    },
    typefaceOptions: {
      disableAuto: true,
      typefaces: ['lato'],
    },
    autoImport: {
      webpack: {
        devtool: 'eval',
        node: { global: true }, // Fix: "Uncaught ReferenceError: global is not defined"
      },
    },
    gisCollective: {
      version: process.env.VERSION || 'develop',
    },
    babel: {
      plugins: [
        require.resolve('ember-auto-import/babel-plugin'),
        'transform-optional-chaining',
      ],
    },
  });

  app.import('node_modules/bootstrap/dist/js/bootstrap.bundle.min.js', {
    outputFile: 'assets/bootstrap.min.js',
  });
  app.import('node_modules/ol/ol.css', { outputFile: 'assets/ol.css' });
  app.import('node_modules/ol/ol.css', { outputFile: 'assets/ol.css' });
  app.import('node_modules/jsoneditor/dist/img/jsoneditor-icons.svg', {
    destDir: 'assets/img',
  });

  const lato = new Funnel('../../node_modules/@fontsource/lato/files', {
    srcDir: '/',
    include: ['**/*.*'],
    destDir: '/assets/files',
  });

  return app.toTree([lato]);
};
