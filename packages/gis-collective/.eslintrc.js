'use strict';

module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
    ecmaFeatures: {
      legacyDecorators: true,
    },
  },
  plugins: ['ember'],
  extends: [
    'eslint:recommended',
    'plugin:ember/recommended',
    'plugin:prettier/recommended',
  ],
  env: {
    browser: true,
  },
  rules: {
    'ember/no-classic-components': 'off',
    'ember/no-classic-classes': 'off',
    'ember/classic-decorator-no-classic-methods': 'off',
    'ember/no-actions-hash': 'off',
    'ember/no-component-lifecycle-hooks': 'off',
    'ember/no-side-effects': 'off',
    'ember/require-tagless-components': 'off',
  },
};
