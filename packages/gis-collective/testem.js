module.exports = {
  test_page: 'tests/index.html?hidepassed',
  disable_watching: true,
  launch_in_ci: ['Firefox'],
  launch_in_dev: ['Firefox'],
  browser_start_timeout: 220,
  tap_quiet_logs: true,
  browser_args: {
    chromium: {
      mode: 'ci',
      args: [
        '--headless',
        '--disable-gl-drawing-for-tests',
        '--no-sandbox',
        '--remote-debugging-port=0',
        '--window-size=1440,900',
        '--no-default-browser-check',
        '--no-first-run',
        '--ignore-certificate-errors',
        '--test-type',
        '--disable-renderer-backgrounding',
        '--disable-background-timer-throttling',
        '--ignore-gpu-blacklist',
        '--enable-webgl',
      ],
    },
    Firefox: {
      mode: 'ci',
      args: ['--headless', '--window-size=1440,900'],
    },
  },
};
