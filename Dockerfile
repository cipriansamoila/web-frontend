FROM node:14-alpine

LABEL mantainer="Szabo Bogdan <contact@szabobogdan.com>"

WORKDIR /home/node/app
USER root

# Copy the app files
COPY dist/ ./

RUN npm install

EXPOSE 80

CMD [ "node", "server.js" ]
